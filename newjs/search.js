// Search Form

$j(function() {
    var button = $j('#searchButton');
    var box = $j('#searchBox');
    var form = $j('#searchForm');
    button.removeAttr('href');
    button.mouseup(function(search) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
        return false;
    });
    $j(this).mouseup(function(search) {
        if(!($j(search.target).parent('#searchButton').length > 0)) {
            button.removeClass('active');
            box.hide();
        }
    });
});

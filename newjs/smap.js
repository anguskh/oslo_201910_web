function make_smap(cityname, dom_id){
	const map = {"福州市":"#","南京市":"#","无锡市":"#","苏州市":"#","杭州市":"#","宁波市":"#","广州市":"#","深圳市":"#","南宁市":"#","北海市":"#","武汉市":"#","成都市":"#",};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
		"backgroundColor": "#f8f8f8",
        "title": [
            {
	            "textStyle": {
					"color": "#000",
					"fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""				
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "series": [
            {
	            "mapType": cityname,
	            "data": [
					{name: '福州市', value: 1},
					{name: '南京市', value: 1},
					{name: '无锡市', value: 1},
					{name: '苏州市', value: 1},
					{name: '杭州市', value: 1},
					{name: '宁波市', value: 1},
					{name: '广州市', value: 1},
					{name: '深圳市', value: 1},
					{name: '南宁市', value: 1},
					{name: '北海市', value: 1},
					{name: '武汉市', value: 1},
					{name: '成都市', value: 1}
	            ],
	            "name": "",
				"symbol": "circle",
				"showLegendSymbol": false,
	            "type": "map",
				"roam": false,
				"label": {
					"normal": {
					  "show": false
					}
				  }
            }
		],
		"visualMap": {
			type: "piecewise",
			pieces: [
				{value: 0, label: '未营运', color: "#eee"},
				{value: 1, label: '营运中', color: "#a1d69e"},
				{value: 2, label: '即时告警错误', color: "#f29394"}
			],
			"textStyle": {
				'color': '#333'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
        window.location.href = filename + ".html";
    });
}

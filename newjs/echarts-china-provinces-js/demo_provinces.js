function make_map(cityname, dom_id){
	const map = {"福州市":"fu2jian4_fu2zhou1",};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
        "title": [
            {
	            "textStyle": {
	            "color": "#000",
	            "fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "backgroundColor": "#fff",
        "series": [
            {
	            "mapType": cityname,
	            "data": [
		            {name: '福州市', value: 1},
	            ],
	            "name": "",
	            "symbol": "circle",
	            "type": "map",
	            "roam": false
            }
		],
		"visualMap": {
			"min": 0,
			"max": 1,
			"splitNumber": 2,
			"color": ['#d94e5d','#eac736','#50a3ba'],
			"textStyle": {
				'color': '#fff'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
        window.location.href = filename + ".html";
    });
}

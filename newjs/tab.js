$(function(){
    // 预设显示第一个 Tab
    var _showTab = 0;
    var $defaultLi = $('ul.tabs li').eq(_showTab).addClass('active');
    $($defaultLi.find('a').attr('href')).siblings().hide();
    
    // 当 li 页签被点击时...
    // 若要改成滑鼠移到 li 页签就切换时, 把 click 改成 mouseover
    $('ul.tabs li').click(function() {
        // 找出 li 中的超连结 href(#id)
        var $this = $(this),
            _clickTab = $this.find('a').attr('href');
        // 把目前点击到的 li 页签加上 .active
        // 并把兄弟元素中有 .active 的都移除 class
        $this.addClass('active').siblings('.active').removeClass('active');
        // 淡入相对应的内容并隐藏兄弟元素
        $(_clickTab).stop(false, true).fadeIn().siblings().hide();

        return false;
    }).find('a').focus(function(){
        this.blur();
    });
});
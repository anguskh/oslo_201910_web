function make_map(){
    const map = {"云南":"#","内蒙古":"#","吉林":"#","四川":"#","宁夏":"#","安徽":"#","山东":"#","山西":"#","广东":"#","广西":"#","新疆":"#","江苏":"#","江西":"#","河北":"#","河南":"#","浙江":"#","海南":"#","湖北":"#","湖南":"#","甘肃":"#","直辖市":"#","福建":"#福建","西藏":"#","贵州":"#","辽宁":"#","陕西":"#","青海":"#","黑龙江":"#","天津":"#","北京":"#","上海":"#","重庆":"#","香港":"#","澳门":"#","北京":"#"};
    const achart = echarts.init(document.getElementById("china"));
    var option =  {
        "backgroundColor": "#f8f8f8", 
        "title": [
        {
            "textStyle": {
            "color": "#000",
            "fontSize": 18
            },
            "subtext": "",
            "text": "中国",
            "top": "auto",
            "subtextStyle": {
            "color": "#aaa",
            "fontSize": 12
            },
            "left": "auto"
        }
        ],
        "legend": [
        {
            "selectedMode": "multiple",
            "top": "top",
            "orient": "horizontal",
            "data": [
            ""
            ],
            "left": "center",
            "show": true
        }
        ],
        "series": [
            {
                "mapType": "china",
                "data": [ //0为未营运，1为营运中，2为错误
                    {name: '广东', value: 1},
                    {name: '广西', value: 1},
                    {name: '江苏', value: 1},
                    {name: '浙江', value: 1},
                    {name: '福建', value: 2},
                    {name: '四川', value: 1},
                    {name: '上海', value: 1},
                    {name: '湖北', value: 1},
                    {name: '新疆', value: 1},
                    {name: '西藏', value: 0},
                    {name: '青海', value: 0},
                    {name: '甘肃', value: 0},
                    {name: '陕西', value: 0},
                    {name: '宁夏', value: 0},
                    {name: '内蒙古', value: 0},
                    {name: '黑龙江', value: 0},
                    {name: '吉林', value: 0},
                    {name: '辽宁', value: 0},
                    {name: '北京', value: 1},
                    {name: '天津', value: 0},
                    {name: '河北', value: 0},
                    {name: '河南', value: 0},
                    {name: '江西', value: 0},
                    {name: '安徽', value: 0},
                    {name: '山西', value: 0},
                    {name: '湖南', value: 0},
                    {name: '山东', value: 0},
                    {name: '重庆', value: 0},
                    {name: '贵州', value: 0},
                    {name: '云南', value: 0},
                    {name: '海南', value: 0},
                    {name: '南海诸岛', value: 0},
                    {name: '台湾', value: 0}
                ],
                "name": "",
                "symbol": "circle",
                "showLegendSymbol": false,
                "type": "map",
                "roam": false,
                "label": {
                    "normal": {
                        "show": false
                    }
                }
            }
        ],
        "visualMap": {
            type: "piecewise",
            pieces: [
                {value: 0, label: '未营运', color: "#eee"},
                {value: 1, label: '营运中', color: "#a1d69e"},
                {value: 2, label: '即时告警错误', color: "#f29394"}
            ],
            "textStyle": {
                'color': '#333'
            }
        }
    };
    achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
		//alert(params.name);
		make_map();
		make_smap(params.name , 'p_rovince');
		// make_smap('福州', 'fu2_zhou1');
       // window.location.href = filename;
        //alert(filename)
    });
};

function make_smap(cityname, dom_id){
	const map = {"福州市":"#","南京市":"#","无锡市":"#","苏州市":"#","杭州市":"#","宁波市":"#","广州市":"#","深圳市":"#","南宁市":"#","北海市":"#","武汉市":"#","成都市":"#",};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
		"backgroundColor": "#f8f8f8",
        "title": [
            {
	            "textStyle": {
					"color": "#000",
					"fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""				
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "series": [
            {
	            "mapType": cityname,
	            "data": [
					{name: '福州市', value: 1},
					{name: '南京市', value: 1},
					{name: '无锡市', value: 1},
					{name: '苏州市', value: 1},
					{name: '杭州市', value: 1},
					{name: '宁波市', value: 1},
					{name: '广州市', value: 1},
					{name: '深圳市', value: 1},
					{name: '南宁市', value: 1},
					{name: '北海市', value: 1},
					{name: '武汉市', value: 1},
					{name: '成都市', value: 1}
	            ],
	            "name": "",
				"symbol": "circle",
				"showLegendSymbol": false,
	            "type": "map",
				"roam": false,
				"label": {
					"normal": {
					  "show": false
					}
				  }
            }
		],
		"visualMap": {
			type: "piecewise",
			pieces: [
				{value: 0, label: '未营运', color: "#eee"},
				{value: 1, label: '营运中', color: "#a1d69e"},
				{value: 2, label: '即时告警错误', color: "#f29394"}
			],
			"textStyle": {
				'color': '#333'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
        make_smap2(params.name, 'c_city');
       // window.location.href = filename + ".html";
    });
}
function make_smap2(cityname, dom_id){
	const map = {"海港区":"#"};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
		"backgroundColor": "#f8f8f8",
        "title": [
            {
	            "textStyle": {
					"color": "#000",
					"fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""				
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "series": [
            {
	            "mapType": cityname,
	            "data": [
					{name: '海港区', value: 1}
	            ],
	            "name": "",
				"symbol": "circle",
				"showLegendSymbol": false,
	            "type": "map",
				"roam": false,
				"label": {
					"normal": {
					  "show": false
					}
				  }
            }
		],
		"visualMap": {
			type: "piecewise",
			pieces: [
				{value: 0, label: '未营运', color: "#eee"},
				{value: 1, label: '营运中', color: "#a1d69e"},
				{value: 2, label: '即时告警错误', color: "#f29394"}
			],
			"textStyle": {
				'color': '#333'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
		console.log(params.name);
        //window.location.href = filename + ".html";
    });
}

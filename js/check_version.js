// 验证版本日期 only联合装机
function check_version (terminal_no,version,check_date) {
  var sub_terminal_no = terminal_no.substr(0,4);
  var ter_status = '';
  var ary_ter_pax = ['637','638','6323','6324','6325'];
  var ary_ter_sgs = ['635','636','6321','6322','6368']; 
  var ary_ter_pos = ['688','687']; 
  var ary_version = ["I8","S3","IH","BF","AH","A2","MP","A3","M2"];

  for (var i = 0; i < ary_ter_pax.length; i++) {
    if( sub_terminal_no.indexOf(ary_ter_pax[i])!=-1 ){
      ter_status='pax';
    }
  }  

  for (var i = 0; i < ary_ter_sgs.length; i++) {
    if( sub_terminal_no.indexOf(ary_ter_sgs[i])!=-1 ){
      ter_status='sgs';
    }
  }

  if ( sub_terminal_no.indexOf(ary_ter_pos[i])!=-1 ) {
    ter_status='pos';
  }

  if ( ter_status==''||ary_version.indexOf(version)==-1) {
    return "-1";
  }else if ( version=="I8"&&ter_status=='pax'&&check_date=='20150922' ) {
    return "1";
  }else if ( version=="S3"&&ter_status=='pax'&&check_date=='20150713' ) {
    return "1";
  }else if ( version=="IH"&&ter_status=='pax'&&check_date=='20131119' ) {
    return "1";
  }else if ( (version=="BF"||version=="I3"||version=="SF003")&&ter_status=='pax'&&check_date=='20150123' ) {
    return "1";
  }else if ( version=="AH"&&ter_status=='pax'&&check_date=='20150807' ) {
    return "1";
  }else if ( version=="A2"&&ter_status=='pax'&&check_date=='20160825' ) {
    return "1";
  }else if ( version=="A3"&&ter_status=='pax'&&check_date=='20161226' ) {
    return "1";
  }else if ( (version=="I8"||version=="IH")&&ter_status=='sgs'&&check_date=='20150730' ) {
    return "1";
  }else if ( (version=="BF"||version=="I3"||version=="SF003")&&ter_status=='sgs'&&check_date=='20141219' ) {
    return "1";
  }else if ( version=="AH"&&ter_status=='sgs'&&check_date=='20160505' ) {
    return "1";
  }else if ( version=="MP"&&ter_status=='pos'&&check_date=='MPOS001' ) {
    return "1";
  }else if ( version=="M2"&&ter_status=='pos'&&check_date=='MPOS002' ) {
    return "1";
  }else{
    return "0";
  }
}

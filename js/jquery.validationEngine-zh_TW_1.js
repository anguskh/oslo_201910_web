(function($){
    $.fn.validationEngineLanguage = function(){
    };
    $.validationEngineLanguage = {
        newLang_zh_tw: function(){
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* 此栏位不可空白",
                    "alertTextCheckboxMultiple": "* 请选择一个项目",
                    "alertTextCheckboxe": "* 您必需勾选此栏位",
                    "alertTextDateRange": "* 日期范围栏位都不可空白"
                },
                "requiredInFunction": { 
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Field must equal test"
                },
                "dateRange": {
                    "regex": "none",
                    "alertText": "* 无效的 ",
                    "alertText2": " 日期范围"
                },
                "dateTimeRange": {
                    "regex": "none",
                    "alertText": "* 无效的 ",
                    "alertText2": " 时间范围"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* 最少 ",
                    "alertText2": " 个字元"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* 最多 ",
                    "alertText2": " 个字元"
                },
				"groupRequired": {
                    "regex": "none",
                    "alertText": "* 你必需选填其中一个栏位"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* 最小值为 "
                },
                "max": {
                    "regex": "none",
                    "alertText": "* 最大值为 "
                },
                "past": {
                    "regex": "none",
                    "alertText": "* 日期必需早于 "
                },
                "future": {
                    "regex": "none",
                    "alertText": "* 日期必需晚于 "
                },	
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* 最多选取 ",
                    "alertText2": " 个项目"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* 请选取 ",
                    "alertText2": " 个项目"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* 栏位内容不相符"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* 无效的信用卡号码"
                },
                "batchno": {
                    "regex": /^\d{8}$/,
                    "alertText": "* 批号格式错误(Ex.20160101) "
                },
				"checkpassword": {	//自行定义, 如需使用请输入custom[checkpassword]
                    "regex": /^(?=^.{7,}$)((?=.*[0-9])(?=.*[A-Za-z]))^.*$/,
                    "alertText": "*密码请包含数字和字母"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9 \.\-\/]{3,20})((#|x|ext|extension)[ ]?[0-9]{1,4})?$/,
                    "alertText": "* 无效的电话号码"
                },
                "email": {
                    // Shamelessly lifted from Scott Gonzalez via the Bassistance Validation plugin http://projects.scottsplayground.com/email_address_validation/
                    "regex": /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    "alertText": "* 信箱格式错误"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* 不是有效的整数"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* 无效的数字"
                },
                "date": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    "alertText": "* 无效的日期，格式必需为 YYYY-MM-DD"
                },
                "time": {
                    "regex": /^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/,
                    "alertText": "* 无效的时间，格式必需为 hh:mm:ss"
                },
                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* 无效的 IP 位址"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9]+$/,
                    "alertText": "* 只能填数字"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* 只接受英文字母大小写"
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* 只接受英文和数字"
                },
                "onlyAmount": {
                    "regex": /^(([0-9]{1,7})|([0-9]{1,7}[.]{1,1}[0-9]{1,2}))$/,
                    "alertText": "* 金额格式错误！<br>整数时可以不输入小数点；<br>如果有输入小数点，同时填上小数点后１～２位金额！"
                },
                 // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
                "ajaxOldPassword": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/check_old_password",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 密码不可与前三次相同",
                    "alertTextLoad": "* 正在确认密码是否与前三次相同，请稍等。"
                },
				"ajax_pk_car_machine_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_car_machine_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 车机编号重复",
                    "alertTextLoad": "* 正在确认车机编号，请稍等。"
                },
				"ajax_pk_battery_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_battery_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 电池序号重复",
                    "alertTextLoad": "* 正在确认电池序号，请稍等。"
                },
				"ajax_pk_bss_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_bss_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 电池租借站序号重复",
                    "alertTextLoad": "* 正在确认电池租借站序号，请稍等。"
                },
				"ajax_pk_dept_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_dept_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 部门代号重复",
                    "alertTextLoad": "* 正在确认部门代号，请稍等。"
                },
                "ajax_pk_rel_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_rel_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 申请单号重复",
                    "alertTextLoad": "* 正在确认申请单号，请稍等。"
                },
                "ajax_pk_ret_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ret_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 申请单号重复",
                    "alertTextLoad": "* 正在确认申请单号，请稍等。"
                },
                "ajax_pk_ist_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ist_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 入库单号重复",
                    "alertTextLoad": "* 正在确认入库单号，请稍等。"
                },
				"ajax_pk_position_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_position_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此职称代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认职称代号，请稍等。"
                },
				"ajax_pk_location_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_location_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此办公室代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认办公室代号，请稍等。"
                },
				"ajax_pk_district_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_district_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此地区代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认地区代号，请稍等。"
                },
				"ajax_pk_bank_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_bank_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此客户代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认客户代号，请稍等。"
                },
				"ajax_pk_mcht_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_mcht_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#sn'],
                    "alertText": "* 此商店代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认客户代号，请稍等。"
                },
				"ajax_pk_kind_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_kind_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此类别代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别代号，请稍等。"
                },
				"ajax_pk_vnd_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_vnd_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此厂商代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认厂商代号，请稍等。"
                },
				"ajax_pk_item_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_item_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#kind_no'],
                    "alertText": "* 此料号重复, 或被停用",
                    "alertTextLoad": "* 正在确认料号，请稍等。"
                },
				"ajax_pk_version": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_version",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#version_no1'],
                    "alertText": "* 此类别的版本代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ap_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ap_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此版本编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machineap": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machineap",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此端末机AP版本重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_kernel_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_kernel_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此版本编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machine_type_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machine_type_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此机型编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_t_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_t_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此请购单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_t_no2": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_t_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此代签单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ord_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ord_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此订购单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_isp_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_isp_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此验收单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_shp_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_shp_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此交货单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ret_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ret_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此请款单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
                "ajax_pk_ins_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ins_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此安装单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_mai_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_mai_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此维修单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_sha_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_sha_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此共用单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_rec_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_rec_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此回收单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ost_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ost_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此出库单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_tra_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_tra_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此移仓单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_che_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_che_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此盘点单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machine_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machine_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此机器序号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_emp_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_emp_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此员工编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_user_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_user_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此帐号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_check_detect_per": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_check_detect_per",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#sn'],
                    "alertText": "* 此机器序号重复新增",
                    "alertTextLoad": "* 正在确认机器序号，请稍等。"
                },
				"ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* 此名称已经被其他人使用",
                    "alertTextLoad": "* 正在确认名称是否有其他人使用，请稍等。"
                },
				"ajaxUserCallPhp": {
                    "url": "phpajax/ajaxValidateFieldUser.php",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* 此帐号名称可以使用",
                    "alertText": "* 此帐号名称已经被其他人使用",
                    "alertTextLoad": "* 正在确认帐号名称是否有其他人使用，请稍等。"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* 此名称可以使用",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* 此名称已经被其他人使用",
                    // speaks by itself
                    "alertTextLoad": "* 正在确认名称是否有其他人使用，请稍等。"
                },
				 "ajaxNameCallPhp": {
	                    // remote json service location
	                    "url": "phpajax/ajaxValidateFieldName.php",
	                    // error
	                    "alertText": "* 此名称已经被其他人使用",
	                    // speaks by itself
	                    "alertTextLoad": "* 正在确认名称是否有其他人使用，请稍等。"
	                },
                "validate2fields": {
                    "alertText": "* 请输入 HELLO"
                },
	            //tls warning:homegrown not fielded 
                "dateFormat":{
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    "alertText": "* 无效的日期格式"
                },
				"ajax_pk_paper_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_paper_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此文件编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_industry_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_industry_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此行业编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
                //tls warning:homegrown not fielded 
				"dateTimeFormat": {
	                "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    "alertText": "* 无效的日期或时间格式",
                    "alertText2": "可接受的格式： ",
                    "alertText3": "mm/dd/yyyy hh:mm:ss AM|PM 或 ", 
                    "alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
	            }
            };
            
        },
        newLang_english: function(){
            $.validationEngineLanguage.allRules = {
                "required": { // Add your regex rules here, you can take telephone as an example
                    "regex": "none",
                    "alertText": "* This field can not be space",
                    "alertTextCheckboxMultiple": "* Please select an item",
                    "alertTextCheckboxe": "* You must check this field",
                    "alertTextDateRange": "* Date Range fields can not be space"
                },
                "requiredInFunction": { 
                    "func": function(field, rules, i, options){
                        return (field.val() == "test") ? true : false;
                    },
                    "alertText": "* Field must equal test"
                },
                "dateRange": {
                    "regex": "none",
                    "alertText": "* Invalid",
                    "alertText2": " Date"
                },
                "dateTimeRange": {
                    "regex": "none",
                    "alertText": "* Invalid ",
                    "alertText2": " Time"
                },
                "minSize": {
                    "regex": "none",
                    "alertText": "* At least",
                    "alertText2": " word(s)"
                },
                "maxSize": {
                    "regex": "none",
                    "alertText": "* At most",
                    "alertText2": " word(s)"
                },
				"groupRequired": {
                    "regex": "none",
                    "alertText": "* You must select one of the fields"
                },
                "min": {
                    "regex": "none",
                    "alertText": "* the Minimum is"
                },
                "max": {
                    "regex": "none",
                    "alertText": "* the Maximun is"
                },
                "past": {
                    "regex": "none",
                    "alertText": "* Date must be earlier than"
                },
                "future": {
                    "regex": "none",
                    "alertText": "* Date must be later than"
                },	
                "maxCheckbox": {
                    "regex": "none",
                    "alertText": "* Select up to",
                    "alertText2": " item(s)"
                },
                "minCheckbox": {
                    "regex": "none",
                    "alertText": "* Please select",
                    "alertText2": " item(s)"
                },
                "equals": {
                    "regex": "none",
                    "alertText": "* Fields do not match"
                },
                "creditCard": {
                    "regex": "none",
                    "alertText": "* Invalid card no"
                },
				"checkpassword": {	//自行定义, 如需使用请输入custom[checkpassword]
                    "regex": /^(?=^.{7,}$)((?=.*[0-9])(?=.*[A-Za-z]))^.*$/,
                    "alertText": "*密码请包含数字和字母"
                },
                "phone": {
                    // credit: jquery.h5validate.js / orefalo
                    "regex": /^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9 \.\-\/]{3,20})((#|x|ext|extension)[ ]?[0-9]{1,4})?$/,
                    "alertText": "* Invalid phone number"
                },
                "email": {
                    // Shamelessly lifted from Scott Gonzalez via the Bassistance Validation plugin http://projects.scottsplayground.com/email_address_validation/
                    "regex": /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
                    "alertText": "* Invalid Email"
                },
                "integer": {
                    "regex": /^[\-\+]?\d+$/,
                    "alertText": "* Invalid Integer"
                },
                "number": {
                    // Number, including positive, negative, and floating decimal. credit: orefalo
                    "regex": /^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/,
                    "alertText": "* Invalid number"
                },
                "date": {
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/,
                    "alertText": "* Invalid Date，Format is  YYYY-MM-DD"
                },
                "time": {
                    "regex": /^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/,
                    "alertText": "* Invalid Time，Format is hh:mm:ss"
                },
                "ipv4": {
                    "regex": /^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/,
                    "alertText": "* Invalid IP address"
                },
                "url": {
                    "regex": /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
                    "alertText": "* Invalid URL"
                },
                "onlyNumberSp": {
                    "regex": /^[0-9]+$/,
                    "alertText": "* Only fill in numeric"
                },
                "onlyLetterSp": {
                    "regex": /^[a-zA-Z\ \']+$/,
                    "alertText": "* Only accept uppercase and lowercase letters "
                },
                "onlyLetterNumber": {
                    "regex": /^[0-9a-zA-Z]+$/,
                    "alertText": "* Special characters are not accepted"
                },
                "onlyAmount": {
                    "regex": /^(([0-9]{1,7})|([0-9]{1,7}[.]{1,1}[0-9]{1,2}))$/,
                    "alertText": "* Amount format error！<br>You can enter a integer without decimal；<br>If there is a decimal point, fill Decimal places from 1 to 2！"
                },
                 // --- CUSTOM RULES -- Those are specific to the demos, they can be removed or changed to your likings
				"ajax_pk_dept_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_dept_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此部门代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认部门代号，请稍等。"
                },
				"ajax_pk_position_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_position_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此职称代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认职称代号，请稍等。"
                },
				"ajax_pk_location_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_location_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此办公室代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认办公室代号，请稍等。"
                },
				"ajax_pk_district_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_district_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此地区代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认地区代号，请稍等。"
                },
				"ajax_pk_bank_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_bank_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此客户代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认客户代号，请稍等。"
                },
				"ajax_pk_mcht_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_mcht_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此商店代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认客户代号，请稍等。"
                },
				"ajax_pk_kind_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_kind_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此类别代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别代号，请稍等。"
                },
				"ajax_pk_vnd_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_vnd_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此厂商代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认厂商代号，请稍等。"
                },
				"ajax_pk_item_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_item_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#kind_no'],
                    "alertText": "* 此料号重复, 或被停用",
                    "alertTextLoad": "* 正在确认料号，请稍等。"
                },
				"ajax_pk_version": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_version",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#version_no1'],
                    "alertText": "* 此类别的版本代号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ap_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ap_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此版本编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machineap": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machineap",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此端末机AP版本重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_kernel_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_kernel_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此版本编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machine_type_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machine_type_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此机型编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_t_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_t_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此请购单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ord_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ord_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此订购单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_isp_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_isp_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此验收单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_shp_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_shp_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此交货单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ret_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ret_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此请款单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
                "ajax_pk_ins_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ins_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此安装单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_mai_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_mai_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此维修单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_sha_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_sha_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此共用单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                }, 
                "ajax_pk_rec_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_rec_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此回收单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_ost_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_ost_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此出库单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_tra_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_tra_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此移仓单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_che_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_che_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此盘点单号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_machine_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_machine_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此机器序号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_emp_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_emp_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此员工编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_user_id": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_user_id",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此帐号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_paper_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_paper_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此文件编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_pk_industry_no": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_pk_industry_no",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
                    "alertText": "* 此行业编号重复, 或被停用",
                    "alertTextLoad": "* 正在确认类别，请稍等。"
                },
				"ajax_check_detect_per": {
                    "url": location.pathname.replace(/\/index/, "").replace(/\/[0-9]+/, "").replace(/__[0-9]+/, "").replace(/\/addition/, "").replace(/\/modification/, "")+"/ajax_check_detect_per",
                    // you may want to pass extra data on the ajax call
                    "extraData": "",
					"extraDataDynamic": ['#sn'],
                    "alertText": "* 此机器序号重复新增",
                    "alertTextLoad": "* 正在确认机器序号，请稍等。"
                },
                "ajaxUserCall": {
                    "url": "ajaxValidateFieldUser",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    "alertText": "* This name has been used by others",
                    "alertTextLoad": "* Confirm whether the name is being used by others, please wait。"
                },
				"ajaxUserCallPhp": {
                    "url": "phpajax/ajaxValidateFieldUser.php",
                    // you may want to pass extra data on the ajax call
                    "extraData": "name=eric",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* You can use this account",
                    "alertText": "* This account is already being used by others",
                    "alertTextLoad": "* Confirm whether the account being used by others, please wait。"
                },
                "ajaxNameCall": {
                    // remote json service location
                    "url": "ajaxValidateFieldName",
                    // error
                    "alertText": "* This name can be used",
                    // if you provide an "alertTextOk", it will show as a green prompt when the field validates
                    "alertTextOk": "* This name has been used by others",
                    // speaks by itself
                    "alertTextLoad": "* Confirm whether the name is being used by others, please wait."
                },
				 "ajaxNameCallPhp": {
	                    // remote json service location
	                    "url": "phpajax/ajaxValidateFieldName.php",
	                    // error
	                    "alertText": "* This name has been used by others",
	                    // speaks by itself
	                    "alertTextLoad": "* Confirm whether the name is being used by others, please wait."
	                },
                "validate2fields": {
                    "alertText": "* Please input HELLO"
                },
	            //tls warning:homegrown not fielded 
                "dateFormat":{
                    "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/,
                    "alertText": "* Invalid date format"
                },
                //tls warning:homegrown not fielded 
				"dateTimeFormat": {
	                "regex": /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1}$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^((1[012]|0?[1-9]){1}\/(0?[1-9]|[12][0-9]|3[01]){1}\/\d{2,4}\s+(1[012]|0?[1-9]){1}:(0?[1-5]|[0-6][0-9]){1}:(0?[0-6]|[0-6][0-9]){1}\s+(am|pm|AM|PM){1})$/,
                    "alertText": "* Invalid date or time format",
                    "alertText2": "Acceptable formats： ",
                    "alertText3": "mm/dd/yyyy hh:mm:ss AM|PM or ", 
                    "alertText4": "yyyy-mm-dd hh:mm:ss AM|PM"
	            }
            };
            
        }
    };
    $.validationEngineLanguage.newLang_zh_tw();
})(jQuery);

// Search Form

$j(function() {
    var button = $j('#searchButton');
    var box = $j('#searchBox');
    var form = $j('#searchForm');
    button.removeAttr('href');
    button.mouseup(function(search) {
        box.toggle();
        button.toggleClass('active');
    });
    form.mouseup(function() { 
        return false;
    });
    $j(this).mouseup(function(search) {
		// console.log(search.target);
		//由日期触发, 不隐藏画面
		if($j(search.target).parents('#calroot').length > 0){
			
		}else{//不是日期选择的触发, 才隐藏画面, by阿卡
			if(!($j(search.target).parent('#searchButton').length > 0) && !($j(search.target).parents("[follow=kind_no_input]").length> 0) && !($j(search.target).parents("[follow=item_no_input]").length> 0) ) {
				button.removeClass('active');
				box.hide();
			}
		}
    });
});

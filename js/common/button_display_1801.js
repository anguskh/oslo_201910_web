//list 頁面, 按鈕顯示控制
function button_display(browse_flag = false){
	//全選按鈕
	var ckbAll = $j("#listTable #ckbAll");
	//各別按鈕
	var ckbSel = $j("#listTable .ckbSel");
	//另外隱藏按紐
	var ckbSel2 = $j("#listTable .ckbSel2");
	//列表
	var listTable = $j("#listTable");
	//列表明細
	var show_detail = $j(".show_detail");
	
	//預設執行
	function default_display(){
		button_display_control();
	}
	
	//一般正常顯示, option[1]顯示, [0]隱藏
	function normal_show(obj_name, option){
		var obj_enable = $j("#"+obj_name+"_enable");
		var obj_disable = $j("#"+obj_name+"_disable");
		// console.log(obj_name+':'+obj_enable.length);
		if(obj_enable.length == 1 && obj_disable.length == 1){
			if(option == 1){ //啟用
				obj_enable.show();
				obj_disable.hide();
			}else{  //停用
				obj_enable.hide();
				obj_disable.show();
			}
		}
	}
	
	//計算打勾的數量
	function count_checked(){
		var cnt = $j("#listTable .ckbSel:checked").length;
		return cnt;
	}
	
	//搜尋欄位是否有值
	function check_searchForm(){
		var result = false;
		// $j("#search_table .td_search:not(.sol-container) input").each(function(){
			
		$j("#search_table .td_search input:not(.sol-input):not(.sol-checkbox)").each(function(){
			console.log($j(this).attr('class'));
			console.log($j(this).attr('name'));
			if($j(this).val() != '' && $j(this).val() != null){
				console.log($j(this).val());
				console.log($j(this).attr('class'));
				result = true;
				return false;
			}
		});
		
		$j("#search_table .td_search select").each(function(){
			if($j(this).val() != '' && $j(this).val() != null){
				console.log($j(this).val());
				result = true;
				return false;
			}
		});
		return result;
	}
	
	//按鈕顯示控制
	function button_display_control(){
		var cnt = count_checked();
		if ( cnt == 1 ) {
			normal_show('up', 1);
			normal_show('down', 1);
			normal_show('modify', 1);
			normal_show('view', 1);
			normal_show('delete', 1);
			normal_show('print', 1);
			normal_show('print_ok_vender', 1);
			normal_show('print_item_all', 1);
			normal_show('print_item_new_old', 1);
			normal_show('data_search', 1);
			if(browse_flag == 'linksearch'){  //跟搜尋關聯
				console.log(check_searchForm());
				if(check_searchForm() === true){  //有搜尋資料
					normal_show('export_excel', 1);  //匯出excel
				}else{
					normal_show('export_excel', 0);  //匯出excel
				}
			}else{
				normal_show('export_excel', 1);  //匯出excel
			}
			if(browse_flag)
			{
				var sn = $j("#listTable .ckbSel:checked").val();
				if($j("#checkdetail_flag_"+sn).val() == 'Y')
				{
					// normal_show('browse', 1);//瀏覽資料
					normal_show('delete', 0);
					// normal_show('modify', 0);
				}
				else
				{
					// normal_show('browse', 0);//瀏覽資料
				}
			}
		} else if ( cnt > 1 ) {
			normal_show('up', 0);
			normal_show('down', 0);
			normal_show('modify', 0);
			normal_show('view', 0);
			normal_show('delete', 1);
			normal_show('print', 1);
			normal_show('print_ok_vender', 1);
			normal_show('print_item_all', 1);
			normal_show('print_item_new_old', 1);
			normal_show('data_search', 1);
			if(browse_flag == 'linksearch'){  //跟搜尋關聯
				if(check_searchForm() === true){  //有搜尋資料
					normal_show('export_excel', 1);  //匯出excel
				}else{
					normal_show('export_excel', 0);  //匯出excel
				}
			}else{
				normal_show('export_excel', 1);  //匯出excel
			}
			if(browse_flag)
			{
				normal_show('browse', 0);//瀏覽資料
			}
		} else {
			normal_show('up', 0);
			normal_show('down', 0);
			normal_show('modify', 0);
			normal_show('view', 0);
			normal_show('delete', 0);
			normal_show('print', 0);
			normal_show('print_ok_vender', 1);
			normal_show('print_item_all', 1);
			normal_show('print_item_new_old', 1);
			normal_show('data_search', 1);
			if(browse_flag == 'linksearch'){  //跟搜尋關聯
				console.log(check_searchForm());
				if(check_searchForm() === true){  //有搜尋資料
					normal_show('export_excel', 1);  //匯出excel
				}else{
					normal_show('export_excel', 0);  //匯出excel
				}
			}else{
				normal_show('export_excel', 1);  //匯出excel
			}
			if(browse_flag)
			{
				normal_show('browse', 0);//瀏覽資料
			}
		}
	}
	

	//按鈕事件
		//全選
	ckbAll.click( function () {
		var flag = $j(this).prop("checked");
		var cnt = 0 ;
		ckbSel.prop("checked", flag);
		ckbSel2.prop("checked", flag);
		button_display_control();
	});
		//各別
	ckbSel.click( function () {
		button_display_control();
		$j(this).siblings(".ckbSel2").prop("checked", $j(this).prop("checked"));
	});
	
		//表單
	// listTable.click( function () {
		// button_display_control();
	// });
	
		//表單tr觸發選擇按鈕 
		//:not(.noclick) 用來避開按到tr就勾選checkbox
	$j("#listTable tr:not(.noclick)").slice(1).children("td").slice(1).click(function(){
	// $j("#listTable tr").slice(1).children("td").slice(1).click(function(){
		//var ckbSel = $j(this).children("td").eq(0).children(".ckbSel");
		var ckbSel = $j(this).siblings().eq(0).children(".ckbSel");
		var ckbSel2 = $j(this).siblings().eq(0).children(".ckbSel2");
		var flag = ckbSel.prop("checked");
		
		var cknoclick = ckbSel.context.id;
		if(cknoclick == 'noclick'){
			//若td的id為noclick時 就不勾取了
			flag = true;
		}
		ckbSel.prop("checked", !flag);
		ckbSel2.prop("checked", !flag);
		button_display_control();
	});
	
		//明細表單觸發選擇按鈕
	$j('body').on( "click", "#show_detail td", function() {
		var ckbSel = $j(this).siblings().eq(0).children(".ckdetail");
		var flag = ckbSel.prop("checked");
		ckbSel.prop("checked", !flag);
	});
	
	//預設執行
	default_display();
}

//自动完成, 自动连接至输入的input和select
//input_id 要连接的input id, 
//select_id 要连接的select id, 
//auto_url 要查询资料的网址
//off_event 是否移除原本事件
function link_obj(input_id, select_id, auto_url, off_event ){
	if(off_event == undefined || off_event == true){  //预设移除
		//移除既有事件
		$j("#"+input_id).off();
		$j("#"+select_id).off();
	}
	
	//stert 自动完成===============================================
	//input自动连结select, 让select和 input同步
	function connectSelect(get_value){
		var obj = $j("#"+select_id+" option[value='"+get_value+"']:not(:disabled)");
		var count = obj.length;
		if(count == 1){
			obj.prop("selected", true);
		}else{
			$j("#"+select_id+" option").eq(0).prop("selected", true);
		}	
	}

	//select自动连结input, 让input和 select同步
	$j("#"+select_id).change(function(){
		var get_value = $j(this).val();
		$j("#"+input_id).val(get_value);

		var val_name =  $j("#"+select_id+" option:selected").attr('val_name');
		$j("#"+select_id+" option:selected").text(val_name);
		
	}); 
	
	$j("#"+select_id).click(function(){
		// console.time('First Method');
		AutoChangeOption(select_id);
		// console.timeEnd('First Method');
	}); 

	//取的栏位资料
	function get_data() {
		var data = {
			'input_data': $j("#"+input_id).val()
		};
		return data;
	};

	//显示自动完成资讯
	$auto("#"+input_id).autocomplete({ 
		//单一参数方法, 变数名称为term
		//source: "<?//=$mer_autoStr?>", 
		//多参数方法, 变数名称可自取
		source: function(request, response) {
			$j.getJSON(auto_url, get_data(), response);
		}, 
		minLength: 1,
		close: function() {	//选择清单后动作(关闭时)
			var get_value = $j(this).val();
			connectSelect(get_value);
			$j("#"+select_id).trigger("change");  //触发select的change事件, 如果有的话
		}, 
		select: function(event, ui) { //选择后的动作(选择时)
			//alert(ui.item.value);  //所选的value
			//alert($j(this).attr("id"));
		}
	}); 

	//边动作边搜寻
	$j("#"+input_id).keyup(function(){
		var get_value = $j(this).val();
		connectSelect(get_value);
	});

	//end 自动完成===============================================	
}

function link_obj_mcht(input_id, bank_id, auto_url, off_event){
	if(off_event == undefined || off_event == true){  //预设移除
		//移除既有事件
		$j("#"+input_id).off();
		// $j("#"+select_id).off();
		$j("#"+bank_id).off();
	}
	
	//stert 自动完成===============================================
	//input自动连结select, 让select和 input同步
	function connectSelect(get_value){
		var obj = $j("#"+select_id+" option[value='"+get_value+"']:not(:disabled)");
		var count = obj.length;
		if(count == 1){
			obj.prop("selected", true);
		}else{
			$j("#"+select_id+" option").eq(0).prop("selected", true);
		}	
	}

	//select自动连结input, 让input和 select同步
	// $j("#"+select_id).change(function(){
	// 	var get_value = $j(this).val();
	// 	$j("#"+input_id).val(get_value);
	// }); 

	//取的栏位资料
	function get_data() {
		var data = {
			'input_data': $j("#"+input_id).val(),
			'bank_no' : $j("#"+bank_id).val()
		};
		return data;
	};

	//显示自动完成资讯
	$auto("#"+input_id).autocomplete({ 
		//单一参数方法, 变数名称为term
		//source: "<?//=$mer_autoStr?>", 
		//多参数方法, 变数名称可自取
		source: function(request, response) {
			if ($j("#"+bank_id).val()=='') {
				// 必须先选择银行才能填商店代号
				alert("请先选择银行");
				$j("#"+input_id).val("");
				return;
			}
			if ($j("#"+input_id).val().length>=5) {
				// 超过5个字在搜寻
				$j.getJSON(auto_url, get_data(), response);
			}
			
		}, 
		minLength: 1,
		close: function() {	//选择清单后动作(关闭时)
			if ($j("#"+input_id).val().length>=5) {
				// 超过5个字在搜寻
				var get_value = $j(this).val();
				// connectSelect(get_value);
				// $j("#"+select_id).trigger("change");  //触发select的change事件, 如果有的话
			}
		}, 
		select: function(event, ui) { //选择后的动作(选择时)
			//alert(ui.item.value);  //所选的value
			//alert($j(this).attr("id"));
		}
	}); 

	//边动作边搜寻
	$j("#"+input_id).keyup(function(){
		if ($j("#"+input_id).val().length>=5) {
			// 超过5个字在搜寻
			var get_value = $j(this).val();
			// connectSelect(get_value);
		}
	});

	//end 自动完成===============================================	
}

//input自动搜寻
function input_auto(input_id, auto_url, input_id2 = '', min){
	//移除原事件
	$j("#"+input_id).off();
	
	if(min == undefined || min == ""){
		min = 1;
	}
	
	//stert 自动完成===============================================
	//取的栏位资料
	function get_data() {
		var data = {
			'input_data': $j("#"+input_id).val()
		};
		return data;
	};

	//显示自动完成资讯
	$auto("#"+input_id).autocomplete({ 
		//单一参数方法, 变数名称为term
		//source: "<?//=$mer_autoStr?>", 
		//多参数方法, 变数名称可自取
		source: function(request, response) {
			$j.getJSON(auto_url, get_data(), response);
		}, 
		minLength: min,
		close: function() {	//选择清单后动作(关闭时)
		
		}, 
		select: function(event, ui) { //选择后的动作(选择时)
			//alert(ui.item.value);  //所选的value
			//alert($j(this).attr("id"));

			if(input_id2 != ''){
				$j("#"+input_id2).val(ui.item.id);
			}
		}
	}); 

	//end 自动完成===============================================	
}

//连结过滤, 依来源资料过滤
//input_id 来源输入资料
//select_id 来源选择资料, 过滤资料栏位(目前以这个为主过滤)
//tar_input_id 过滤目标 input
//tar_select_id 过滤目标 select
//filter_field 过滤栏位名称
//url 动态更新auto url
function filter_obj(input_id, select_id, tar_input_id, tar_select_id, filter_field, url){
	var select_val = $j("#"+select_id).val();  //取得原有值
	var tar_input_id_val = $j("#"+tar_input_id).val();  //取得原有值
	var tar_select_val = $j("#"+tar_select_id).val();  //取得原有值

	//if(select_val != ""){ //一开始来源输入有值, 让刚开始就以触发过滤, 并且选回的过滤目标
		start_filter();  //先预设执行
	//}
	
	$j("#"+select_id).change(function(){
		clearData();
		start_filter();
	});

	// $j("#"+input_id).blur(function(){
		// clearData();
		// start_filter();
	// });
	
	//自动完成绑定click事件
	$j("ul[follow='"+input_id+"']").on('click', function(){
		clearData();
		start_filter();
	});
	// $j(".ui-corner-all").each(function(){
		// $j(this).on('click', function(){
			// autoOPERID();
		// });
	// });

	//开始过滤
	function start_filter(){
		var val = $j("#"+select_id).val();
		//过滤目标select option
		var obj_tar_select_id_option = $j("#"+tar_select_id+" option");
		if(val != ""){
			//动态更新auto
			link_obj(tar_input_id, tar_select_id, url+'/'+val, false);
			
			//更新 option
				//清空
			$j("#"+tar_input_id).val("");
				//显示和隐藏资料
			var obj_show = $j("#"+tar_select_id+" option["+filter_field+"='"+val+"']");
			var obj_hide = $j("#"+tar_select_id+" option["+filter_field+"!='"+val+"']");
			var obj_always_show = $j("#"+tar_select_id+" option[value='all']");
			obj_show.prop("disabled", false);
			obj_show.show();
			obj_hide.prop("disabled", true);
			obj_hide.hide();
			obj_always_show.prop("disabled", false);
			obj_always_show.show();
				//刚开始有原资料, 就显示回原资料状态, 没有就回清空状态
			if(tar_input_id_val != '' && tar_select_val != ''){
				// console.log(tar_input_id_val);
				// console.log(tar_select_val);
				var obj_aftershow = $j("#"+tar_select_id+" option["+filter_field+"='"+tar_select_val+"']");
				obj_aftershow.prop("disabled", false);
				obj_aftershow.show();
				obj_aftershow.prop("selected", true);
				$j("#"+tar_input_id).val(tar_input_id_val);
			}else{
				//console.log(2);
				obj_tar_select_id_option.eq(0).show();
				obj_tar_select_id_option.eq(0).prop("disabled", false);
				obj_tar_select_id_option.eq(0).prop("selected", true);
				//将值带给input, 如果有预设值的话
				var objDef = $j("#"+tar_select_id+" option:selected");
				var defVal = objDef.val();
				$j("#"+tar_input_id).val(defVal);
				// if(defVal != ""){
					// console.log(defVal);
					// objDef.prop("disabled", false);
				// }
			}
		}else{
			//全部恢复
			$j("#"+tar_input_id).val("");
			//console.log(tar_input_id);
			obj_tar_select_id_option.prop("disabled", false);
			// obj_tar_select_id_option.show();
			obj_tar_select_id_option.eq(0).prop("selected", true);
		}
	}
	
	function clearData(){
		tar_input_id_val = "";
		tar_select_val = "";
	}
	
}

//删除明细空白资讯
function clearDetailDiv(div_class){
	$j("."+div_class).each(function(){
		var all_input = $j(this).find(":input");
		// console.log(all_input.length);
		var delFlag = true;  //true 删除, flase 不删除
		
		all_input.each(function(){
			var val = $j(this).val();
			//var name = $j(this).attr("name");
			//console.log(id+' '+val+':'+name);
			if(val != ""){ //不为空, 不删除, 跳出回圈
				delFlag = false;
				return false; // 等于break
			}
		});
		
		if(delFlag){  //删除
			$j(this).remove();
		}
	});
}

//下拉选单增加下拉宽度, 改到CSS
// function addCSS(select_id){
	// $j("#"+select_id).css("max-width","90%");
// }

//下拉增加[代号]
function AutoChangeOption(field){
	var select_id = "";
	var val = '';
	var val_name = '';
	var text = '';
	
	if(typeof(field) == "object"){  //物件
		select_id = field.id;
	}else if(typeof(field) == 'string'){  //字串
		select_id = field;
	}  //其他就不动作
	
	
	if(select_id != ""){  
		//下拉选单增加下拉宽度
		// addCSS(select_id);
		//回圈加入[代号]
		$j("#"+select_id+" option:enabled").each(function(){
			val = $j(this).val();
			val_name = $j(this).attr("val_name");
			text = $j(this).text();
			
			if($j(this).prop("selected")){  //被选择就不加入[代号]
				$j(this).text(val_name);
			}else{
				if(val != "" && val != 'all'){
					// $j(this).text('['+val+']'+val_name);
					$j(this).text(val+' ｜ '+val_name);
				}
			}
		});
	}
}


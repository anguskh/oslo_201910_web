Array.prototype.unique = function() {  
	 return this.join(",").match(/([^,]+)(?!.*\1)/ig);  
}  

//sleep
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

//日期起讫检查, 起日不可大于迄日
function checkDate(sdate, edate, checksdate){
	if(checksdate == undefined){
		checksdate = true;
	}
	//时间
	var startDate = $j("#"+sdate).val(); //起
	var endDate = $j("#"+edate).val(); //迄
	if(startDate != "" && endDate != ""){
		startDate = startDate.replace(/-/g, "/");
		startDate = Date.parse(startDate).valueOf();
		endDate = endDate.replace(/-/g, "/");
		endDate = Date.parse(endDate).valueOf();
		if(startDate > endDate){	//起日不可大于迄日
			alert('起日不可大于迄日!');
			return false;
		}else{
			return true;
		}
	}else if(startDate ==  "" && checksdate == true){
		alert('请选择起日!');
		return false;
	}else{
		return true;
	}
}

//若str的长度小于length，则在其左边补sign
function padLeft(str,length,sign){
	if(str.length>=length)	return str;
	else	return padLeft(sign+str,length,sign);
}
//若str的长度小于length，则在其右边补sign
function padRight(str,length,sign){
	if(str.length>=length)	return str;
	else	return padRight(str+sign,length,sign);
}

//只可输入数字2 ,只能有数字不能有小数点
function ValidateNumber2(){
	return /[0-9]/.test(String.fromCharCode(event.keyCode));
}

//加入千分位
// function commafy(num) {
	// num = num + "";
	// var re = /(-?\d+)(\d{3})/
	// while (re.test(num)) {
		// num = num.replace(re, "$1,$2")
	// }
	// return num;
// }

//支持小数点的千分位
function commafy(n) {
    n += "";
    var arr = n.split(".");
    var re = /(\d{1,3})(?=(\d{3})+$)/g;
	//改为都会显示小数4码
    // return arr[0].replace(re,"$1,") + (arr.length == 2 ? "."+arr[1] : "");
    return arr[0].replace(re,"$1,") + (arr.length == 2 ? "."+padRight(arr[1], 4, '0') : "."+padRight('0', 4, '0'));
}

//移除千分位
function remve_commafy(num){
	return num.replace(/[,]+/g,"");
}

//浮点数相加
function FloatAdd(arg1, arg2)
{
  var r1, r2, m;
  try { r1 = arg1.toString().split(".")[1].length; } catch (e) { r1 = 0; }
  try { r2 = arg2.toString().split(".")[1].length; } catch (e) { r2 = 0; }
  m = Math.pow(10, Math.max(r1, r2));
  return (FloatMul(arg1, m) + FloatMul(arg2, m)) / m;
}
//浮点数相减
function FloatSubtraction(arg1, arg2)
{
  var r1, r2, m, n;
  try { r1 = arg1.toString().split(".")[1].length } catch (e) { r1 = 0 }
  try { r2 = arg2.toString().split(".")[1].length } catch (e) { r2 = 0 }
  m = Math.pow(10, Math.max(r1, r2));
  n = (r1 >= r2) ? r1 : r2;
  return ((arg1 * m - arg2 * m) / m).toFixed(n);
}
//浮点数相乘
function FloatMul(arg1, arg2)
{
  var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
  try { m += s1.split(".")[1].length; } catch (e) { }
  try { m += s2.split(".")[1].length; } catch (e) { }
  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}
//浮点数相除
function FloatDiv(arg1, arg2)
{
  var t1 = 0, t2 = 0, r1, r2;
  try { t1 = arg1.toString().split(".")[1].length } catch (e) { }
  try { t2 = arg2.toString().split(".")[1].length } catch (e) { }
  with (Math)
  {
    r1 = Number(arg1.toString().replace(".", ""))
    r2 = Number(arg2.toString().replace(".", ""))
    return (r1 / r2) * pow(10, t2 - t1);
  }
}

//start input 支持千分位============================================================================================
$j(".thousandComma").focus(function(){
	thousandComma_disabled($j(this), true);
});

$j(".thousandComma").blur(function(){
	thousandComma_enabled($j(this), true);
});

$j(".thousandComma_nodp").focus(function(){
	thousandComma_disabled($j(this) ,false);
});

$j(".thousandComma_nodp").blur(function(){
	thousandComma_enabled($j(this), false);
});

//没输入不补零
$j(".thousandComma_nokey").focus(function(){
	if($j(this).val() != ''){
		thousandComma_disabled($j(this), true);
	}
});

$j(".thousandComma_nokey").blur(function(){
	if($j(this).val() != ''){
		thousandComma_enabled($j(this), true);
	}
});

//移除千分位
function thousandComma_disabled(obj, dp){
	var pattern = /\,/g;  //移除逗点
	var val = obj.val();
	val = val.replace(pattern, '');
	
	pattern = /\.0{4}/g;  //移除小数点之后有4个0
	val = val.replace(pattern, '');
	
	obj.val(val);
}

//显示千分位
function thousandComma_enabled(obj, dp){
	var val = obj.val();
	if(val == ""){
		val = '0';
	}
	var arr_val = val.split('.');
	var val_1 = arr_val[0];
	val_1 = parseInt(val_1);
	val_1 = val_1.toString();
	var val_2 = "";
	if(arr_val[1] == undefined){  //没小数点 
		if(dp){
			val_2 = padRight('0', 4, '0');
		}else{
			val_2 = '';
		}
	}else{  //有小数点, 右边补0
		if(dp){
			val_2 = padRight(arr_val[1], 4, '0');
		}else{
			val_2 = '';
		}
	}
	
	var pattern = /(-?\d+)(\d{3})/;
	  
	while(pattern.test(val_1)){
		val_1 = val_1.replace(pattern, "$1,$2");
	}
	if(val_2 != ""){  //有小数点
		obj.val(val_1+'.'+val_2);
	}else{  //没有
		obj.val(val_1);
	}
}
//end input 支持千分位============================================================================================
//控制scroll bar 至最下面
function end_scroll(selecter){
	var sH = $j(selecter).prop("scrollHeight")+40;
	$j(selecter).scrollTop(sH);
}

//easyui function
//filed: selecter
//width: 显示宽度
//def_val: 给予预设值
//seq_no: search_machine_count(seq_no) (移仓点选要呼叫function)
function show_combobox(selecter, width, def_val, seq_no){
	$auto(function() {
		if(def_val == undefined || def_val == ''){
			 $j_1102(selecter).combobox({
				panelWidth:width,
				onSelect: function(rec){
					$j(selecter+" option").attr("selected", false);
					$j(selecter+" option[value='"+rec.value+"']").attr("selected", true);
				},
				onChange:function(newValue,oldValue){
					if(seq_no != undefined && seq_no != ''){
						search_machine_count(seq_no);
					}
				}
			 });
		}else{
			 $j_1102(selecter).combobox({
				panelWidth:width, 
				value: def_val,
				onSelect: function(rec){
					$j(selecter+" option").attr("selected", false);
					$j(selecter+" option[value='"+rec.value+"']").attr("selected", true);
				},
				onChange:function(newValue,oldValue){
					if(seq_no != undefined && seq_no != ''){
						search_machine_count(seq_no);
					}
				}
			 });
		}

		//点的时候预设选项时清空内容
		$j_1102(selecter).combobox('textbox').bind('focus',function(e){ 
			//alert($j('#return_per').val());/*
			var target = this;
			var len = $j_1102(target).val().length;
			var temp = $j_1102(target).val();
			var value = $j_1102(selecter).combobox('getValue');

			if(value == ''){
				$j_1102(target).val('');
			}
		});  

		//离开时
		$j_1102(selecter).combobox('textbox').bind('blur',function(e){ 
			var value = $j_1102(selecter).combobox('getValue');
			if(value == ''){
				$j_1102(selecter).combobox('clear');
			}
		});  
	});
}

//单一combobox
function combobox(selecter, width){
	$auto(function() {
		$j_1102(selecter).combobox({
			panelWidth:width
		});

		//点的时候预设选项时清空内容
		$j_1102(selecter).combobox('textbox').bind('focus',function(e){ 
			//alert($j('#return_per').val());/*
			var target = this;
			var len = $j_1102(target).val().length;
			var temp = $j_1102(target).val();
			var value = $j_1102(selecter).combobox('getValue');

			if(value == ''){
				$j_1102(target).val('');
			}
		});  

		//离开时
		$j_1102(selecter).combobox('textbox').bind('blur',function(e){ 
			var value = $j_1102(selecter).combobox('getValue');
			if(value == ''){
				$j_1102(selecter).combobox('clear');
			}
		});  
	});
}

//自动找出对应的部门，对应的主管
function change_manager(selecter, manager_id){
	var dept_no = selecter.find("option:selected").attr("dept_no");
	var position = selecter.find("option:selected").attr("position");
	var head_position = 'DM';
	if(dept_no == '010'){
		head_position = 'DR';
	}else if(dept_no == '013' || dept_no == '014'){
		dept_no = '006';
	}

	if(position == 'DM'){
		head_position = 'DR';
		if(dept_no == '012'  || dept_no == '023'  || dept_no == '024'){
			dept_no = '008';
			head_position = 'SDM-B';
		}else if(dept_no == '004' || dept_no == '006' || dept_no == '007' || dept_no == '013' || dept_no == '014'){
			dept_no = '006';
		}else if(dept_no == '009' || dept_no == '011' || dept_no == '018' || dept_no == '019' || dept_no == '020'){
			dept_no = '011';
		}else if(dept_no == '005' || dept_no == '008' || dept_no == '012' || dept_no == '017' || dept_no == '023' || dept_no == '024'){
			dept_no = '005';
			head_position = 'DGM';
		}
	}else if(position == 'DR'){
		dept_no = '002';
		head_position = 'GM';
	}else if(position == 'GM'){
		head_position = 'GM';
	}

	if($j("#"+manager_id+" option[dept_no='"+dept_no+"'][position='"+head_position+"']").length == 1){
		$j("#"+manager_id+" option[dept_no='"+dept_no+"'][position='"+head_position+"']").prop("selected", true);
	}else{
		$j("#"+manager_id+" option[dept_no='"+dept_no+"'][position='"+head_position+"']").eq(0).prop("selected", true);
	}
}

function clear_searchData(mutiselectobjArr = ""){
	$j('.searchData').val('');
	$j('.searchData_sel').val('');
	$j('.twoInout_input').val('');
	$j('.twoInout_sel').val('');
	$j('.searchdateData').val('');
	$j('.searchcheckbox').prop('checked', false);
	if(mutiselectobjArr != "")
	{
		var i =0;
		//清除多选物件内的值
		if(is_array(mutiselectobjArr))
		{
			mutiselectobjArr.forEach(function(){
				if(mutiselectobjArr[i] !== 'undefined')
					mutiselectobjArr[i].deselectAll();
				i++;
			});
		}
	}
}

//2017/01/04 点选清空按钮 by ming
$j("#btClear, #btClear2").click(function(){
	clear_searchData();
});



//红色X关闭搜寻
$j("#closeX").click(function(){
	$j("#searchBox").hide();
});

var is_array = function(v){
 return Object.prototype.toString.call(v) === '[object Array]';
 //call也可改成用apply，效果一样
 //return Object.prototype.toString.apply(v) === '[object Array]';
}

//停用所有元件
function fn_disable_all(){
	console.log('disabled all');
	//关闭主页面的元件
		//所有input停用
	$j("#formID :input").prop("disabled", true);
	$j("#formID :checkbox").prop("disabled", true);
		//移除明细新增和删除
	//$j("#formID img").remove();
	$j("#formID .btn_label").remove();
	$j("#btn_detail_add").remove();
	$j("#btn_detail_del").remove();
	// $j("#formID img").css("opacity", 0.5);
		//移除一些外挂的触发
	$j(".sol-caret-container").remove();
	//关闭radio
	$j("#formID :radio").prop("disabled", true);
		//启用radio
	//$j("#formID :radio").prop("disabled", false);
		//启用需要开启的按钮
	$j("#btn_mNo").prop("disabled", false);
	
	//关闭序号明细的元件
	$j(".modal :input").prop("disabled", true);
		//移除序号明细新增和删除, 存档
	//$j(".modal img").remove();
	$j(".modal :button").remove();
}

//隐藏离职员工
function disabled_emp(selecter){
	$j(selecter+" option").each(function(){
		var onduty = $j(this).attr("onduty");
		var selected = $j(this).prop('selected');
		if(onduty == '0'){
			if(selected == false){
				$j(this).remove();
			}
		}
	});
}

var theight = 60;
if($j("#listForm .infobox").length>0)
{
	theight = theight-8;
}
if($j("div .pagination").length>0)
{
	theight = theight-6;
}
//$j("#listTable").wrapAll("<div id='fixtable' style='margin-top:50px;height:"+theight+"vh;overflow-y: auto;'>");

// //固定table第一行
//$j("#listTable tbody").before('<thead>');
//$j("#listTable tbody").after('<tfoot>');
//$j("#listTable tbody tr").eq(0).appendTo('thead');
//$j('#listTable').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: false });
//$j('.fht-thead #ckbAll').click(function(){
//    if($j(this).attr('checked'))
//    {
//        $j('#listTable #ckbAll').prop("checked",true);
//    }
//    $j('#listTable #ckbAll').trigger("click");
//});

$j('#s_search_txt').keypress(function (event) {
	if (event.which === 13)
	{
		// 如果鍵盤按下的是 Enter 的動作 
		$j('#btSearch, #btSearch2').click();
	}
});

function stuck_apnel(){	
/*	var flag = false; 
	//要使用固定選單
	//加入class網址
	var allow_class = [];
	//allow_class[0] = '/phoebus/nimda';
	//allow_class[1] = '/phoebus/dealer';
	//allow_class[2] = '/phoebus/operator';
	//allow_class[3] = '/phoebus/device';
	//allow_class[4] = '/phoebus/member';
	//allow_class[5] = '/phoebus/feedback';
	//allow_class[6] = '/phoebus/inquiry';
	//allow_class[7] = '/phoebus/customer';

	//是否通過
	/*for (i = 0; i < allow_class.length; i++) { 
		var page_class = allow_class[i];
		var len = page_class.length;
		var url_class = location.pathname.substr(0, len);
		if(page_class == url_class){
			// console.log(page_class+':'+url_class);
			flag = true;
			break;
		}
	}

	// console.log(flag);
	//不使用固定選單
	//加入url網址
	var not_allow_url = [];
	not_allow_url[0] = '/phoebus/stock/ctbc_stock';
	not_allow_url[1] = '/phoebus/stock_report';
	
	for (i = 0; i < not_allow_url.length; i++) { 
		var page_url = not_allow_url[i];
		var len = page_url.length;
		var get_url = location.pathname.substr(0, len);
		if(page_url == get_url){
			flag = false;
			break;
		}
	}
	// console.log(flag);
	if(flag){
		$j(".topper").css('position', 'fixed');
		$j(".topper").css('width', '100%');
		$j(".topper").css('z-index', '10000');
		$j(".topper").css('background', '#FFF');
		$j("#menu_area").css('position', 'fixed');
		$j("#menu_area").css('top', '70px');
		$j("#menu_area").css('width', '100%');
		$j("#menu_area").css('z-index', '10000');
	}else{
		//$j("#html_title_content").css('position', 'relative');
		return false;
	}

	//製造出放控制項目, 複製清單table的固定點
	var scroll_top = $j('html').scrollTop();
	// $j('html').scrollTop(0);
	var container_fixed_html = '<div id="container_fixed"><div id="body" class="box"></div></div>';
	$j(container_fixed_html).insertBefore(".wrapper");

	//定位container_fixed
	var obj_menu_topper = $j(".topper");
	var obj_menu_table = $j("#menu_area");
	var menu_table_top = obj_menu_table.offset().top;
	var menu_table_h = obj_menu_table.outerHeight();
	//$j("#container_fixed").css("top", menu_table_top + menu_table_h - scroll_top);
	//$j("#container_fixed #body").css("top", menu_table_top + menu_table_h - scroll_top);
	$j("#container_fixed").css('position', 'fixed');
	$j("#container_fixed").css('width', '100%');
	$j("#container_fixed").css('left', '0');
	$j("#container_fixed").css('z-index', '9999');
	$j("#container_fixed").css('padding', '165px 0 0 0');
	$j("#container_fixed").css('background', '#fdf4ec');

	$j("#container_fixed #body").css('border-bottom', '0');

	if($j("#listTable").val() == undefined){
		//內頁
		$j("#container_fixed #body").css('padding', '50px 50px 35px 50px');
	}else{
		$j("#container_fixed #body").css('padding', '50px 50px 0 50px');
	}
	//$j("#container_fixed #body").css('margin', ' 0 auto');
	$j("#container_fixed #body").css('margin', ' 0px 35px');

	
	//把控制項目複製到固定點
	$j(".wrapper").find("code").appendTo("#container_fixed #body");
	//找出控制項目和清單之間有無其他div, 一起複製到固定點
	$j("#listArea").children("div").each(function(){
		if($j(this).attr("id") != undefined){
			$j(this).appendTo("#container_fixed div#body");
		}
	});
	
	//搜尋導引
	var title_H = 0;
	$j("#listArea").find("table").each(function(){
		// console.log($j(this).attr("id"));
		if($j(this).attr("id") == undefined){  //沒有名稱的table, 應該是搜尋導引, 有名稱是清單, 略過
			if($j(this).find("#td_editer").length == 0){  //沒有庫存序號明細, td裡面顯示資訊
				$j(this).appendTo("#container_fixed div#body");
				title_H = $j(this).outerHeight();
			}
		}
	});
	
	//清單標題高度
	var tr_H = 0;
	//有清單固定修正高度
	var fix_h = 1;  
	var tr_show_h = 0;
	if($j("#listTable").length == 1){  //有清單才複製
		//複製清單table最上方th到固定點
		var thead_cnt = $j("#listTable").find('thead').length;
		// var td_editer_cnt = $j("#listTable").find('td_editer').length;
		if(thead_cnt == 0){ //一般寫法
			$j("#listTable").clone(true).appendTo("#container_fixed div#body").attr("id", "listTable_fixed").find("tr:first").nextAll().remove();
			// $j("#container_fixed table[id!='listTable_fixed']").remove();
		}else{
			$j("#listTable").clone(true).appendTo("#container_fixed div#body").attr("id", "listTable_fixed").find("#listTbody").remove();
		}

		//th寬度修正
		var obj_listTable_fixed = $j("#listTable_fixed th");
		$j("#listTable th").each(function(idx){
			obj_listTable_fixed.eq(idx).width($j(this).width());
		});
		
		//清單標題高度
		tr_H = $j("#listTable tr:first").outerHeight();
	
		fix_h = 1.5;
		$j("#container_fixed").css('class', $j("#listTable").attr('class'));
	}

	//算出固定的高度
	var obj_container_fixed = $j("#container_fixed");  
	var container_fixed_m_h =  parseInt($j(".wrapper").css("margin-top").replace('px', ''));  
	var container_fixed_offset = obj_container_fixed.offset();
	var container_H = container_fixed_offset.top + obj_container_fixed.outerHeight();


	//有搜尋導引
	// var cnt = $j("#listArea").find("table").length;
	// var title_H = 0;
	// if(cnt == 2){
		// title_H = $j("#listArea").find("table:first").outerHeight();
	// }

	// console.log('container_H:'+container_H);
	// console.log('tr_H:'+tr_H);
	// console.log('title_H:'+title_H);
	// console.log('container_fixed_m_h:'+container_fixed_m_h);
	// console.log('fix_h:'+ fix_h);
	title_H = 0;
	var menu_topper_h = obj_menu_topper.outerHeight()
	// var h = container_H - tr_H - title_H - 2 - 17;
	var h = container_H  - tr_H - title_H - container_fixed_m_h - fix_h - tr_show_h;
	var obj_container_fixed_body = $j("#body");  
	var container_body_H = obj_container_fixed_body.outerHeight();

	//h = container_H - container_body_H ;
	//alert( $j(".box").outerHeight());
	//h = $j(".box").outerHeight() - container_body_H ;
	// console.log(h);
	//庫存序號明細, 有問題針對的修改
	// if(h < 0){
		// h = 260;
		// $j("#formID").css("margin-top", '-250px');
	// }else{
	// }
	
	//將container清單資料往下移動
	var scroll_top = $j('html').scrollTop();
	//139.5px

	//alert(container_H - $j("#listTable").offset().top - 31);
	//$j(".wrapper").children(".box").css("margin-top", (h - scroll_top) +'px');
	//$j(".wrapper").children(".box").css("margin-top", '160px');
//alert($j("#listTable").offset().top);
	if($j("#listTable").val() == undefined){
		//內頁
		$j(".wrapper").children(".box").css("margin-top", (container_H -  61) +'px');
	}else{
		//console.log(container_H);//404
		//console.log($j("#listTable tr:first").offset().top);//27
		//console.log($j("#listTable_fixed tr:first").outerHeight() );//132
		//console.log(tr_H);//76
		if($j("#listTable_fixed tr:first").outerHeight() > tr_H){
			//$j(".wrapper").children(".box").css("margin-top", ($j("#listTable tr:first").offset().top + $j("#listTable_fixed tr:first").offset().top) +'px');
			$j(".wrapper").children(".box").css("margin-top", (container_H - tr_H - $j("#listTable tr:first").offset().top  ) +'px');
		}else{
			//$j(".wrapper").children(".box").css("margin-top", (container_H - $j("#listTable").offset().top - 31) +'px');
			$j(".wrapper").children(".box").css("margin-top", (container_H - $j("#listTable_fixed tr:first").outerHeight() - $j("#listTable tr:first").offset().top  ) +'px');
		}
	}*/
}

//把選單固定再隱藏的最外層位置, 避免圖曾被蓋掉
function menu_hold(){
	/*var menu_hold_html = '<div id="menu_hold"></div>';
	$j('body').prepend(menu_hold_html);

	//把選單移到網頁最外層header.php, 避免選單跟著卷軸跑掉
	$j("#subMenusContainer").appendTo("#menu_hold");*/
}
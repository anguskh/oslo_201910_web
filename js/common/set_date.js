function set_date(change_flag = true){
	//日期picker, 初始化
	$j(":date").dateinput({ 
		lang: 'zh-TW', 
		trigger: false, 
		format: 'yyyy-mm-dd', 
		offset: [0, 0], 
		selectors: true, 
		yearRange: [-7, 3],
		change: function(event) {
			if(change_flag)
			{
				changedate(this.getInput().attr('id'),this.getValue("yyyy-mm-dd"));
			}
			changedate1(this.getInput().attr('id'),this.getValue("yyyy-mm-dd"));

			if(this.getInput().attr('name') == 'limit_date'){
				//update value 再呼叫
				$j('#'+this.getInput().attr('id')+'_new').val(this.getValue("yyyy-mm-dd"));
				change_limit_date(this.getInput().attr('id') , this.getInput().attr('name'));
			}
		}
	});

	$j(":date").unbind('keydown');
	$j(":date").attr('maxlength','10');
	$j(":date").keydown(function(event){
		var key = event.which;
		// console.log(key);
		
		//只可输入数字和 - / backspace(8) del(46) tab(9)
		if ((key >= 48 && key <= 57) || key == 189 || key == 191 || 
			(key >= 96 && key <= 105) || key == 109 || key == 111 || 
			key == 46 || key == 9 || key == 8) {
		}else{
			event.preventDefault();
		}
	});

	$j(":date").blur(function(){
		var val = $j(this).val();
		if(val != ''){
			val = val.replace(/\//g, "-");
			$j(this).val(val);
			//检查日期
			if(isNaN(checkDate(val))){
				alert('日期格式不正确');
				$j(this).val('');
				$j(this).focus();
				return;
			}else{
				$j(this).val(new Date(val).format('yyyy-MM-dd'));
				if(change_flag)
				{
					changedate($j(this).attr('id'),$j(this).val());
				}	
				changedate1($j(this).attr('id'),$j(this).val());

				if($j(this).attr('name') == 'limit_date'){
					//update value 再呼叫
					$j('#'+$j(this).attr('id')+'_new').val(val);
					change_limit_date($j(this).attr('id') , $j(this).attr('name'));
				}
			}
		}
		else
		{
			if($j(this).attr('id') == "notice_date")
			{
			    $j("#limit_date").val("");
			}
			else if($j(this).attr('id') == "finish_date")
			{
				// $j("#close_op").prop("checked", false);
				// $j("#flag_sel option[value='']").prop("selected", true).change();
			}
			if($j(this).attr('name') == 'limit_date'){
				//update value 再呼叫
				$j('#'+$j(this).attr('id')+'_new').val('');
				change_limit_date($j(this).attr('id') , $j(this).attr('name'));
			}

		}
	});

	//检查日期
	function checkDate(str) {
		var t = Date.parse(str);
		console.log(t);
		return t;
	}
	
	//日期格式转换
	Number.prototype.pad2 =function(){   
		return this>9?this:'0'+this;   
	}   
	Date.prototype.format=function (format) {   
		var it=new Date();   
		var it=this;   
		var M=it.getMonth()+1,H=it.getHours(),m=it.getMinutes(),d=it.getDate(),s=it.getSeconds();   
		var n={ 'yyyy': it.getFullYear()   
				,'MM': M.pad2(),'M': M   
				,'dd': d.pad2(),'d': d   
				,'HH': H.pad2(),'H': H   
				,'mm': m.pad2(),'m': m   
				,'ss': s.pad2(),'s': s   
		};   
		return format.replace(/([a-zA-Z]+)/g,function (s,$1) { return n[$1]; });   
	}

	function changedate(nowid,nowdate){
		/*输入 通知日期 后自动对应 安装期限,有notice_date此ID才动作*/
		if(nowid == "notice_date")
		{
			Date.prototype.toInputFormat = function() {
		       var yyyy = this.getFullYear().toString();
		       var mm = (this.getMonth()+1).toString(); 
		       var dd  = this.getDate().toString();
		       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
		    };

			var notice_date = new Date(nowdate);
		    days = 2;
		    
		    if(!isNaN(notice_date.getTime())){
		        notice_date.setDate(notice_date.getDate() + days);
		        if( notice_date.getDay()==6 || notice_date.getDay()==0 ){
		        	notice_date.setDate(notice_date.getDate() + 2);
		        }
		        $j("#limit_date").val(notice_date.toInputFormat());
		    }
		    else
		    {
		    	$j("#limit_date").val("");
		    }
		}
	}

	function changedate1(nowid,nowdate){
		// if(nowid == "finish_date")/*有key完成日期时,将结案打勾以及逾期完成填入99,有finish_date此ID才改变*/
		// {
			// if(nowdate!="")
			// {
				// $j("#close_op").prop("checked", true);
				// $j("#flag_sel option[value='99']").prop("selected", true).change();
			// }
			// else
			// {
				// $j("#close_op").prop("checked", false);
				// $j("#flag_sel option[value='']").prop("selected", true).change();
			// }
		// }
	}
}
/* ========================================================================
 * Bootstrap: popover.js v3.3.1
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($j) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$j.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.1'

  Popover.DEFAULTS = $j.extend({}, $j.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $j.extend({}, $j.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $jtip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $jtip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $jtip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $jtip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$jtip.find('.popover-title').html()) $jtip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $je = this.$jelement
    var o  = this.options

    return $je.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($je[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$jarrow = this.$jarrow || this.tip().find('.arrow'))
  }

  Popover.prototype.tip = function () {
    if (!this.$jtip) this.$jtip = $j(this.options.template)
    return this.$jtip
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $jthis    = $j(this)
      var data     = $jthis.data('bs.popover')
      var options  = typeof option == 'object' && option
      var selector = options && options.selector

      if (!data && option == 'destroy') return
      if (selector) {
        if (!data) $jthis.data('bs.popover', (data = {}))
        if (!data[selector]) data[selector] = new Popover(this, options)
      } else {
        if (!data) $jthis.data('bs.popover', (data = new Popover(this, options)))
      }
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $j.fn.popover

  $j.fn.popover             = Plugin
  $j.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $j.fn.popover.noConflict = function () {
    $j.fn.popover = old
    return this
  }

}(jQuery);

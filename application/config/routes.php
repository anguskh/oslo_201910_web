<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
// rework by Jaff
$route['default_controller'] = "login";
$route['404_override'] = '';

//乱码url路由
// $route['xxx/xxx__(:num)'] = "xxx/xxx";
// $route['xxx/xxx__(:num)/index'] = "xxx/xxx";
// $route['xxx/xxx__(:num)/index/(:num)'] = "xxx/xxx/index/$2";
// $route['xxx/xxx__(:num)/(:any)'] = "xxx/xxx/$2";

$route['nimda/password__(:num)'] = "nimda/password";
$route['nimda/password__(:num)/index'] = "nimda/password";
$route['nimda/password__(:num)/index/(:num)'] = "nimda/password/index/$2";
$route['nimda/password__(:num)/(:any)'] = "nimda/password/$2";

$route['nimda/config__(:num)'] = "nimda/config";
$route['nimda/config__(:num)/index'] = "nimda/config";
$route['nimda/config__(:num)/index/(:num)'] = "nimda/config/index/$2";
$route['nimda/config__(:num)/(:any)'] = "nimda/config/$2";

$route['nimda/group__(:num)'] = "nimda/group";
$route['nimda/group__(:num)/index'] = "nimda/group";
$route['nimda/group__(:num)/index/(:num)'] = "nimda/group/index/$2";
$route['nimda/group__(:num)/(:any)'] = "nimda/group/$2";

$route['nimda/menu__(:num)'] = "nimda/menu";
$route['nimda/menu__(:num)/index'] = "nimda/menu";
$route['nimda/menu__(:num)/index/(:num)'] = "nimda/menu/index/$2";
$route['nimda/menu__(:num)/(:any)'] = "nimda/menu/$2";


$route['nimda/user__(:num)'] = "nimda/user";
$route['nimda/user__(:num)/index'] = "nimda/user";
$route['nimda/user__(:num)/index/(:num)'] = "nimda/user/index/$2";
$route['nimda/user__(:num)/(:any)'] = "nimda/user/$2";

$route['device/battery__(:num)'] = "device/battery";
$route['device/battery__(:num)/index'] = "device/battery";
$route['device/battery__(:num)/index/(:num)'] = "device/battery/index/$2";
$route['device/battery__(:num)/(:any)'] = "device/battery/$2";
$anytxt = '';
for($i=2;$i<5;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['device/battery__(:num)'.$anytxt] = "device/battery{$path}";
}
$route['device/battery__(:num)/detail/(:num)/(:any)'] = "device/battery/detail/$2";
$route['device/battery__(:num)/detail/(:num)/(:any)/(:any)'] = "device/battery/detail/$2/$3";


$route['device/gps_manage__(:num)'] = "device/gps_manage";
$route['device/gps_manage__(:num)/index'] = "device/gps_manage";
$route['device/gps_manage__(:num)/index/(:num)'] = "device/gps_manage/index/$2";
$route['device/gps_manage__(:num)/(:any)'] = "device/gps_manage/$2";

$route['device/bss_manage__(:num)'] = "device/bss_manage";
$route['device/bss_manage__(:num)/index'] = "device/bss_manage";
$route['device/bss_manage__(:num)/index/(:num)'] = "device/bss_manage/index/$2";
$route['device/bss_manage__(:num)/(:any)'] = "device/bss_manage/$2";
for($i=2;$i<6;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['device/bss_manage__(:num)'.$anytxt] = "device/bss_manage{$path}";
}

$route['device/bss_advertising__(:num)'] = "device/bss_advertising";
$route['device/bss_advertising__(:num)/index'] = "device/bss_advertising";
$route['device/bss_advertising__(:num)/index/(:num)'] = "device/bss_advertising/index/$2";
$route['device/bss_advertising__(:num)/(:any)'] = "device/bss_advertising/$2";

$route['device/bss_manage__(:num)/bssdetaillist/(:num)'] = "device/bss_manage/bssdetaillist/$2";
$route['device/bss_manage__(:num)/bssdetaillist/(:num)/(:any)'] = "device/bss_manage/bssdetaillist/$2/$3";

$route['inquiry/log_operating__(:num)'] = "inquiry/log_operating";
$route['inquiry/log_operating__(:num)/index'] = "inquiry/log_operating";
$route['inquiry/log_operating__(:num)/index/(:num)'] = "inquiry/log_operating/index/$2";
$route['inquiry/log_operating__(:num)/(:any)'] = "inquiry/log_operating/$2";

$route['dealer/dealer__(:num)'] = "dealer/dealer";
$route['dealer/dealer__(:num)/index'] = "dealer/dealer";
$route['dealer/dealer__(:num)/index/(:num)'] = "dealer/dealer/index/$2";
$route['dealer/dealer__(:num)/(:any)'] = "dealer/dealer/$2";

$route['dealer/sub_dealer__(:num)'] = "dealer/sub_dealer";
$route['dealer/sub_dealer__(:num)/index'] = "dealer/sub_dealer";
$route['dealer/sub_dealer__(:num)/index/(:num)'] = "dealer/sub_dealer/index/$2";
$route['dealer/sub_dealer__(:num)/(:any)'] = "dealer/sub_dealer/$2";

$route['operator/operator__(:num)'] = "operator/operator";
$route['operator/operator__(:num)/index'] = "operator/operator";
$route['operator/operator__(:num)/index/(:num)'] = "operator/operator/index/$2";
$route['operator/operator__(:num)/(:any)'] = "operator/operator/$2";

$route['operator/sub_operator__(:num)'] = "operator/sub_operator";
$route['operator/sub_operator__(:num)/index'] = "operator/sub_operator";
$route['operator/sub_operator__(:num)/index/(:num)'] = "operator/sub_operator/index/$2";
$route['operator/sub_operator__(:num)/(:any)'] = "operator/sub_operator/$2";

$route['device/vehicle__(:num)'] = "device/vehicle";
$route['device/vehicle__(:num)/index'] = "device/vehicle";
$route['device/vehicle__(:num)/index/(:num)'] = "device/vehicle/index/$2";
$route['device/vehicle__(:num)/(:any)'] = "device/vehicle/$2";
$anytxt = '';
for($i=2;$i<5;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['device/vehicle__(:num)'.$anytxt] = "device/vehicle{$path}";
}

$route['member/member__(:num)'] = "member/member";
$route['member/member__(:num)/index'] = "member/member";
$route['member/member__(:num)/index/(:num)'] = "member/member/index/$2";
$anytxt = '';
for($i=2;$i<6;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['member/member__(:num)'.$anytxt] = "member/member{$path}";
}

$route['operator/battery_swap_station__(:num)'] = "operator/battery_swap_station";
$route['operator/battery_swap_station__(:num)/index'] = "operator/battery_swap_station";
$route['operator/battery_swap_station__(:num)/index/(:num)'] = "operator/battery_swap_station/index/$2";
$route['operator/battery_swap_station__(:num)/(:any)'] = "operator/battery_swap_station/$2";
$anytxt = '';
for($i=2;$i<5;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['operator/battery_swap_station__(:num)'.$anytxt] = "operator/battery_swap_station{$path}";
}

$route['operator/battery_swap_station_group__(:num)'] = "operator/battery_swap_station_group";
$route['operator/battery_swap_station_group__(:num)/index'] = "operator/battery_swap_station_group";
$route['operator/battery_swap_station_group__(:num)/index/(:num)'] = "operator/battery_swap_station_group/index/$2";
$route['operator/battery_swap_station_group__(:num)/(:any)'] = "operator/battery_swap_station_group/$2";

$route['device/battery_swap_ccb__(:num)'] = "device/battery_swap_ccb";
$route['device/battery_swap_ccb__(:num)/index'] = "device/battery_swap_ccb";
$route['device/battery_swap_ccb__(:num)/index/(:num)'] = "device/battery_swap_ccb/index/$2";
$route['device/battery_swap_ccb__(:num)/(:any)'] = "device/battery_swap_ccb/$2";

$route['operator/battery_swap_track__(:num)'] = "operator/battery_swap_track";
$route['operator/battery_swap_track__(:num)/index'] = "operator/battery_swap_track";
$route['operator/battery_swap_track__(:num)/index/(:num)'] = "operator/battery_swap_track/index/$2";
$route['operator/battery_swap_track__(:num)/(:any)'] = "operator/battery_swap_track/$2";
$anytxt = '';
for($i=2;$i<5;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['operator/battery_swap_track__(:num)'.$anytxt] = "operator/battery_swap_track{$path}";
}

$route['operator/battery_swap_track__(:num)/detail/(:num)/(:any)'] = "operator/battery_swap_track/detail/$2";
$route['operator/battery_swap_track__(:num)/detail/(:num)/(:any)/(:any)'] = "operator/battery_swap_track/detail/$2/$3";


$route['operator/lease_price__(:num)'] = "operator/lease_price";
$route['operator/lease_price__(:num)/index'] = "operator/lease_price";
$route['operator/lease_price__(:num)/index/(:num)'] = "operator/lease_price/index/$2";
$route['operator/lease_price__(:num)/(:any)'] = "operator/lease_price/$2";

$route['operator/lease_price_new__(:num)'] = "operator/lease_price_new";
$route['operator/lease_price_new__(:num)/index'] = "operator/lease_price_new";
$route['operator/lease_price_new__(:num)/index/(:num)'] = "operator/lease_price_new/index/$2";
$route['operator/lease_price_new__(:num)/(:any)'] = "operator/lease_price_new/$2";

$route['inquiry/battery_inventory__(:num)'] = "inquiry/battery_inventory";
$route['inquiry/battery_inventory__(:num)/index'] = "inquiry/battery_inventory";
$route['inquiry/battery_inventory__(:num)/index/(:num)'] = "inquiry/battery_inventory/index/$2";
$route['inquiry/battery_inventory__(:num)/(:any)'] = "inquiry/battery_inventory/$2";

$route['inquiry/log_driving__(:num)'] = "inquiry/log_driving";
$route['inquiry/log_driving__(:num)/index'] = "inquiry/log_driving";
$route['inquiry/log_driving__(:num)/index/(:num)'] = "inquiry/log_driving/index/$2";
$route['inquiry/log_driving__(:num)/(:any)'] = "inquiry/log_driving/$2";

$route['inquiry/log_battery_exchange__(:num)'] = "inquiry/log_battery_exchange";
$route['inquiry/log_battery_exchange__(:num)/index'] = "inquiry/log_battery_exchange";
$route['inquiry/log_battery_exchange__(:num)/index/(:num)'] = "inquiry/log_battery_exchange/index/$2";
$route['inquiry/log_battery_exchange__(:num)/(:any)'] = "inquiry/log_battery_exchange/$2";

$route['inquiry/log_battery_info__(:num)'] = "inquiry/log_battery_info";
$route['inquiry/log_battery_info__(:num)/index'] = "inquiry/log_battery_info";
$route['inquiry/log_battery_info__(:num)/index/(:num)'] = "inquiry/log_battery_info/index/$2";
$route['inquiry/log_battery_info__(:num)/(:any)'] = "inquiry/log_battery_info/$2";

$route['inquiry/log_battery_leave_return__(:num)'] = "inquiry/log_battery_leave_return";
$route['inquiry/log_battery_leave_return__(:num)/index'] = "inquiry/log_battery_leave_return";
$route['inquiry/log_battery_leave_return__(:num)/index/(:num)'] = "inquiry/log_battery_leave_return/index/$2";
$route['inquiry/log_battery_leave_return__(:num)/(:any)'] = "inquiry/log_battery_leave_return/$2";

$route['customer/feedback__(:num)'] = "customer/feedback";
$route['customer/feedback__(:num)/index'] = "customer/feedback";
$route['customer/feedback__(:num)/index/(:num)'] = "customer/feedback/index/$2";
$route['customer/feedback__(:num)/(:any)'] = "customer/feedback/$2";

$route['customer/supplementary__(:num)'] = "customer/supplementary";
$route['customer/supplementary__(:num)/index'] = "customer/supplementary";
$route['customer/supplementary__(:num)/index/(:num)'] = "customer/supplementary/index/$2";
$route['customer/supplementary__(:num)/(:any)'] = "customer/supplementary/$2";

$route['inquiry/log_alarm_online__(:num)'] = "inquiry/log_alarm_online";
$route['inquiry/log_alarm_online__(:num)/index'] = "inquiry/log_alarm_online";
$route['inquiry/log_alarm_online__(:num)/index/(:num)'] = "inquiry/log_alarm_online/index/$2";
$route['inquiry/log_alarm_online__(:num)/(:any)'] = "inquiry/log_alarm_online/$2";

$route['inquiry/log_battery_warring__(:num)'] = "inquiry/log_battery_warring";
$route['inquiry/log_battery_warring__(:num)/index'] = "inquiry/log_battery_warring";
$route['inquiry/log_battery_warring__(:num)/index/(:num)'] = "inquiry/log_battery_warring/index/$2";
$route['inquiry/log_battery_warring__(:num)/(:any)'] = "inquiry/log_battery_warring/$2";

$route['inquiry/log_swap_track_warring__(:num)'] = "inquiry/log_swap_track_warring";
$route['inquiry/log_swap_track_warring__(:num)/index'] = "inquiry/log_swap_track_warring";
$route['inquiry/log_swap_track_warring__(:num)/index/(:num)'] = "inquiry/log_swap_track_warring/index/$2";
$route['inquiry/log_swap_track_warring__(:num)/(:any)'] = "inquiry/log_swap_track_warring/$2";

$route['report/battery_gps__(:num)'] = "report/battery_gps";
$route['report/battery_gps__(:num)/index'] = "report/battery_gps";
$route['report/battery_gps__(:num)/index/(:num)'] = "report/battery_gps/index/$2";
$route['report/battery_gps__(:num)/(:any)'] = "report/battery_gps/$2";

$route['report/swap_station__(:num)'] = "report/swap_station";
$route['report/swap_station__(:num)/index'] = "report/swap_station";
$route['report/swap_station__(:num)/index/(:num)'] = "report/swap_station/index/$2";
$route['report/swap_station__(:num)/(:any)'] = "report/swap_station/$2";

$route['report/exchange__(:num)'] = "report/exchange";
$route['report/exchange__(:num)/index'] = "report/exchange";
$route['report/exchange__(:num)/index/(:num)'] = "report/exchange/index/$2";
$route['report/exchange__(:num)/(:any)'] = "report/exchange/$2";

$route['statistics/exchange_average__(:num)'] = "statistics/exchange_average";
$route['statistics/exchange_average__(:num)/index'] = "statistics/exchange_average";
$route['statistics/exchange_average__(:num)/index/(:num)'] = "statistics/exchange_average/index/$2";
$route['statistics/exchange_average__(:num)/(:any)'] = "statistics/exchange_average/$2";

$route['statistics/exchange_nobattery__(:num)'] = "statistics/exchange_nobattery";
$route['statistics/exchange_nobattery__(:num)/index'] = "statistics/exchange_nobattery";
$route['statistics/exchange_nobattery__(:num)/index/(:num)'] = "statistics/exchange_nobattery/index/$2";
$route['statistics/exchange_nobattery__(:num)/(:any)'] = "statistics/exchange_nobattery/$2";

$route['statistics/exchange_oneday__(:num)'] = "statistics/exchange_oneday";
$route['statistics/exchange_oneday__(:num)/index'] = "statistics/exchange_oneday";
$route['statistics/exchange_oneday__(:num)/index/(:num)'] = "statistics/exchange_oneday/index/$2";
$route['statistics/exchange_oneday__(:num)/(:any)'] = "statistics/exchange_oneday/$2";

$route['statistics/exchanges_number__(:num)'] = "statistics/exchanges_number";
$route['statistics/exchanges_number__(:num)/index'] = "statistics/exchanges_number";
$route['statistics/exchanges_number__(:num)/index/(:num)'] = "statistics/exchanges_number/index/$2";
$route['statistics/exchanges_number__(:num)/(:any)'] = "statistics/exchanges_number/$2";

$route['statistics/exchange_onedayavg__(:num)'] = "statistics/exchange_onedayavg";
$route['statistics/exchange_onedayavg__(:num)/index'] = "statistics/exchange_onedayavg";
$route['statistics/exchange_onedayavg__(:num)/index/(:num)'] = "statistics/exchange_onedayavg/index/$2";
$route['statistics/exchange_onedayavg__(:num)/(:any)'] = "statistics/exchange_onedayavg/$2";

$route['statistics/exchange_statistics__(:num)'] = "statistics/exchange_statistics";
$route['statistics/exchange_statistics__(:num)/index'] = "statistics/exchange_statistics";
$route['statistics/exchange_statistics__(:num)/index/(:num)'] = "statistics/exchange_statistics/index/$2";
$route['statistics/exchange_statistics__(:num)/(:any)'] = "statistics/exchange_statistics/$2";

$route['monitor/monitor__(:num)'] = "monitor/monitor";
$route['monitor/monitor__(:num)/index'] = "monitor/monitor";
$route['monitor/monitor__(:num)/index/(:num)'] = "monitor/monitor/index/$2";
$route['monitor/monitor__(:num)/(:any)'] = "monitor/monitor/$2";

$route['monitor/monitor_alarm_online__(:num)'] = "monitor/monitor_alarm_online";
$route['monitor/monitor_alarm_online__(:num)/index'] = "monitor/monitor_alarm_online";
$route['monitor/monitor_alarm_online__(:num)/index/(:num)'] = "monitor/monitor_alarm_online/index/$2";
$route['monitor/monitor_alarm_online__(:num)/(:any)'] = "monitor/monitor_alarm_online/$2";

$route['pre_order/operator_order__(:num)'] = "pre_order/operator_order";
$route['pre_order/operator_order__(:num)/index'] = "pre_order/operator_order";
$route['pre_order/operator_order__(:num)/index/(:num)'] = "pre_order/operator_order/index/$2";
$route['pre_order/operator_order__(:num)/(:any)'] = "pre_order/operator_order/$2";
$anytxt = '';
for($i=2;$i<6;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['pre_order/operator_order__(:num)'.$anytxt] = "pre_order/operator_order{$path}";
}


$route['pre_order/card_management__(:num)'] = "pre_order/card_management";
$route['pre_order/card_management__(:num)/index'] = "pre_order/card_management";
$route['pre_order/card_management__(:num)/index/(:num)'] = "pre_order/card_management/index/$2";
$route['pre_order/card_management__(:num)/(:any)'] = "pre_order/card_management/$2";
$anytxt = '';
for($i=2;$i<6;$i++){
	 $anytxt .= "/(:any)";
	 $path = "";
	 for($j=$i;$j>=2;$j--)
	 {
	  if($path == "")
	   $path = "/$$j";
	  else
	   $path = "/$$j".$path;
	 }
	 $route['pre_order/card_management__(:num)'.$anytxt] = "pre_order/card_management{$path}";
}

$route['advertising/showad__(:num)'] = "advertising/showad";
$route['advertising/showad__(:num)/index'] = "advertising/showad";
$route['advertising/showad__(:num)/index/(:num)'] = "advertising/showad/index/$2";
$route['advertising/showad__(:num)/(:any)'] = "advertising/showad/$2";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
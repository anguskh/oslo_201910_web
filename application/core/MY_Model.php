<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11
class MY_Model extends CI_Model {
/**
 * DB的相关资料
 * http://codeigniter.com/user_guide//database/examples.html
 * http://www.codeigniter.org.tw/user_guide/database/helpers.html
 * http://www.codeigniter.org.tw/user_guide/database/active_record.html
 */
//-------------------------------------------------------------------------------------------------
	/**
	 * @var Model_user 使用者帐号
	 */
	var $model_user;
//-------------------------------------------------------------------------------------------------

    var $_db ;
    
	/**
	 * 建构子
	 */
    function __construct()
    {
        parent::__construct();
		$this->load->library("common_function") ;
        // load database，(这种写法是应付一个系统会LOAD很多个DB Connecting)预先会load那些db连线，先建立起来
        $this->_db['default'] = $this->load->database('default', TRUE);
		//$this->_db['airlink'] = $this->load->database('airlink', TRUE);  //连线多个资料库
		//$this->_db['mssql'] = $this->load->database('mssql', TRUE);  //连线多个资料库, 改至model_transaction再进行连线
        // $this->_db['hm_db'] = $this->load->database('hm_db',TRUE);
        
        // $this->_db['default'] = $this->load->database(); // 只会load default setting的DB，参考./application/config/database.php
    }
	
	//取得连线driver
	//来源:$db_name 连线名称
	public function getDBdriver($db_name = '') {
        if (empty($db_name)) {
            $db_name = "default";
        }
		$db = $this->getDb($db_name);
		$retrun = "";
		$retrun = $db->dbdriver;
		return $retrun;
	}
	
	//取得连线DB name
	//来源:$db_name 连线名称
	public function getDBname() {
        $db_name = "default";
		$db = $this->getDb($db_name);
		$retrun = "";
		$retrun = $db->database;
		return $retrun;
	}
    
	/**
	 * 直接传入SQLCmd
	 * @param SQLCmd SQLCmd - 字串
	 * @param db_name 资料库名称 - 字串
	 */
	public function db_query($SQLCmd = '', $db_name = '')
    {
		if ( empty( $SQLCmd ) ) {
			return array();
		}
		if ( empty( $db_name ) ) {
			$db_name = "default";
		}

		$db = $this->getDb($db_name);
		
		if (empty($db)) {
			return "没有连结到DB";
		} else {
			//$this->session->set_userdata('sql_cmd', "111");
			//纪录SQL
			$this->sql_start_record($SQLCmd);
			if ( strpos( strtoupper( "  ".$SQLCmd ), "INSERT INTO" ) > 0 || strpos( strtoupper( "  ".$SQLCmd ), "UPDATE" ) > 0 || strpos( strtoupper( "  ".$SQLCmd ), "DELETE FROM" ) > 0) {
				// do INSERT or UPDATE or DELETE cmd
				return $db->query($SQLCmd) ;
			} else {
				// do other cmd
				return $db->query($SQLCmd)->result_array() ;
			}
		}
    }

	/**
	 * 还在测 不能用
	 */
	public function db_quert_where( $tableName = '', $whereArr = "", $db_name = "" )
	{
		if ( empty( $tableName ) or empty( $whereArr )) {
			return array();
		}
		if ( empty( $db_name ) ) {
			$db_name = "default";
		}

		$db = $this->getDb($db_name);

		if (empty($db)) {
			return "没有连结到DB";
		} else {
			return $db->get_where( $tableName, $whereArr )->result_array();
			// return $db->query($SQLCmd)->result_array();
		}
		
	}
	
	/**
	 * 传入table name, 栏位资料阵列，即可进行新增
	 * @param tableName 资料表名称 - 字串
	 * @param colDataArr 栏位阵列 - 阵列
	 * @param db_name 资料库名称 - 字串
	 */
	public function db_insert( $tableName = '', $colDataArr = '', $db_name = '' )
	{
        if ( empty( $tableName ) or empty( $colDataArr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }

        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "没有连结到DB";
        } else {
        	if (in_array("now()", $colDataArr)) {
        		$SQLCmd = $db->insert_string($tableName, $colDataArr) ; // 产生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				//纪录SQL
				$this->sql_start_record($SQLCmd);
				$this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				return $db->insert($tableName, $colDataArr) ; // 直接新增进db
			}
			
        }		
	}
	
	/**
	 * 传入table name, 栏位资料阵列, 条件阵列，即可进行更新
	 * @param tableName 资料表名称 - 字串
	 * @param colDataArr 栏位阵列 - 阵列
	 * @param whereDataStr 条件字串 - 字串
	 * @param db_name 资料库名称 - 字串
	 */
	public function db_update( $tableName = '', $colDataArr = '', $whereDataStr = '', $db_name = '' )
	{
        if ( empty( $tableName ) or empty( $colDataArr ) or empty( $whereDataStr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }
        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "没有连结到DB";
			
        } else {
        	if ( in_array( "now()", $colDataArr ) ) {
        		$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 产生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				//纪录SQL
				$this->sql_start_record($SQLCmd);
				$this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 产生新增的SQLCmd
				//纪录SQL
				$this->sql_start_record($SQLCmd);
				$this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			}
			
        }		
	}


	
	/**
	 * 这没有真的删除，只是更换flag
	 * 传入table name, 栏位资料阵列, 条件阵列，即可进行更新
	 * @param tableName 资料表名称 - 字串
	 * @param colDataArr 栏位阵列 - 阵列
	 * @param whereDataStr 条件字串 - 字串
	 * @param db_name 资料库名称 - 字串
	 */
	public function db_delete($tableName = '', $colDataArr = '', $whereDataStr = '', $db_name = '')
	{
        if ( empty( $tableName ) or empty( $colDataArr ) or empty( $whereDataStr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }

        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "没有连结到DB";
        } else {
        	if ( in_array( "now()", $colDataArr ) ) {
        		$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 产生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				//纪录SQL
				$this->sql_start_record($SQLCmd);
				$this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				return $db->update( $tableName, $colDataArr, $whereDataStr ) ; // 直接新增进db
			}
			
        }		
	}
	
    public function getDb($db_name){
        return $this->_db[$db_name];
    }
	
	//判断是否要记录SQL
	public function sql_start_record($SQLCmd){
		if($this->session->userdata("sql_start") == true){
			$this->session->set_userdata("sql_cmd", $SQLCmd);
		}
	}
	
	//记录新增, 修改或删除栏位值
	public function data_record($colDataArr){
		$this->session->set_userdata("desc", $colDataArr);
	}
	
// 转址================================================================
	function redirect_alert( $url, $msg ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url = $url . "/".$this->session->userdata('PageStartRow');
			//$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<Script Language="JavaScript">
	alert("$msg");
	location.href="$url";
</SCRIPT>

SCRIPT
;
	}
	
	function redirect( $url ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<Script Language="JavaScript">
	location.href="$url";
</SCRIPT>

SCRIPT
;
	}
	function redirect_question_alert( $url1, $url2, $msg ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url1 = $url1 . "/".$this->session->userdata('PageStartRow');
			//$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<Script Language="JavaScript">
	if(confirm("$msg"))
	{
		window.open("$url2",'_blank');
		location.href="$url1";
	}
	else
	{
		location.href="$url1";
	}
</SCRIPT>

SCRIPT
;
	}
	
// 转址================================================================
}

/* End of file my_model.php */
/* Location: ./application/core/my_model.php */
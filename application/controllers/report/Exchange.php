<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Exchange extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("report/Model_exchange", "Model_exchange") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("exchange", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("exchange", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		//$totalRow = $this->Model_exchange->getAllCnt() ;
		$totalRow = 0;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		//$data["InfoRow"] = $this->Model_exchange->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		$data["InfoRow"] = array();
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站报表');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		//$whereStr = "1=1";
		//$rs = $this->Model_exchange->search_all($whereStr);
		//$data["chart_list"] = $rs;


		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "report/exchange_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "exchange");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}

	public function check_download(){
		date_default_timezone_set("Asia/Taipei");
		$search_start_date = $this->input->post('date_start');
		$search_end_date = $this->input->post('date_end');
		$whereStr = " 1=1 ";
		$date_content = "";	
		if($search_start_date != '' && $search_end_date != ''){
			$whereStr .= " and (date(log_date) >= '{$search_start_date}' and date(log_date) <= '{$search_end_date}') ";
			$date_content = $search_start_date."～".$search_end_date;
		}else if($search_start_date != ''){
			$whereStr .= " and date(log_date) >= '{$search_start_date}' ";
			$date_content = $search_start_date."～";
		}else if($search_end_date != ''){
			$whereStr .= " and date(log_date) <= '{$search_end_date}' ";
			$date_content = "～".$search_end_date;
		}else{
			$date_content = "全部";
		}

		$rs = $this->Model_exchange->search_all($whereStr);
		//$cell_name = array("A1","A2","A3","A4","A5", "A6", "A7");
		$cell_name = array("A1","A2","A3","A4","A5", "A6", "A7", "A8", "A9", "A10");
		$total_cell = count($cell_name);
		//$tw_name = array("換電站：","換電站電池平均每日交換次數：","單一換電站最多電池交換次數：","單一換電站最少電池交換次數：","單一換電站單日最多電池交換次數：", "單一換電站單日最少電池交換次數：", "換電站電池平均時段交換次數：");
		$tw_name = array("換電站：","換電站電池總計交換次數：","換電站電池平均每日交換次數：", "換電站電池平均時段交換次數：","單一換電站平均電池交換次數：","單一換電站最多電池交換次數：","單一換電站最少電池交換次數：","單一換電站單日最多電池交換次數：", "單一換電站單日最少電池交換次數：","單一換電站電池平均時段交換次數：");

		$set_title = 'exchange';
		//讀取excel元件
		$this->loadExcel();
		
		//初始化
		$objPHPExcel = new PHPExcel(); 
		for($x=0; $x<1; $x++){
			//寫入
			$objPHPExcel->createSheet($x);
			$objPHPExcel->setActiveSheetIndex($x);
			$objPHPExcel->getActiveSheet()->setTitle($set_title); //頁籤

			//預設
			//$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');	//預設字型
			$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(8);	//設定欄位寬度
			$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	//垂直置中
			$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); //水平置中

			$objPHPExcel->getActiveSheet()->getColumnDimension( 'A' )->setWidth(5); //欄位寬度  
			$objPHPExcel->getActiveSheet()->getColumnDimension( 'B' )->setWidth(8); //欄位寬度  
			$objPHPExcel->getActiveSheet()->getColumnDimension( 'C' )->setWidth(40); //欄位寬度  
			$objPHPExcel->getActiveSheet()->getColumnDimension( 'D' )->setWidth(13); //欄位寬度  

			/*
				getHorizontal()：获得水平居中方式
				getVertical()：获得垂直居中方式
				setHorizontal()：设置水平居中方式，返回对齐方式对象
				setVertical()：设置垂直居中方式，返回对齐方式对象			
			*/
			//$objPHPExcel->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //邊框

			$row=2;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "資料區間：{$date_content}");
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,$row,3,$row); 
			$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	//靠左
			$row++;

			/*
				getAllborders() 全框線
				getTop() 上框線
				getBottom() 下框線
				getLeft() 左框線
				getRight()	右框線
				BORDER_MEDIUM 粗線
				BORDER_THIN 細線
				getStyle('A3:D3')
			*/

			for($st=1; $st<4; $st++){
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getFill()->getStartColor()->setARGB('CCDDFF'); 
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getBorders()->getBottom
()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//下框線加粗

				if($st==1){
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
				}

				if($st==3){
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
				}
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($st,$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);	//上框線加粗
			}
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, "編號");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, "欄位名稱");
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, "次數");

			$row++;

			if(isset($rs)){
				for($q=0; $q<10; $q++){
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->setValueExplicit($cell_name[$q], PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
					$objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->setValueExplicit($tw_name[$q], PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
					if($q == 3 || $q == 9){
						if(count($rs[$q])>0){
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,($row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

							$start = 3;
							$o_start = 0;
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							foreach($rs[$q] as $key=>$arr){
								if($o_start == 12){
									//換行
									$start = 3;
									$row+=2;
								}

								$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($start, $row)->setValueExplicit($key, PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
								$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($start, ($row+1))->setValueExplicit($arr, PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
								$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+1))->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								if($start > 3){
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
								}

								if(count($rs[$q]) == ($o_start+1)){
									//右框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row-1))->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row-2))->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+1))->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
								}

								if($o_start == 12){
									//左邊加粗
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,$row)->getBorders()->	getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+1))->getBorders()->	getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,$row)->getBorders()->	getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//上框
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($start,($row+1))->getBorders()->	getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//下框
								}
								$start++;
								$o_start++;
							}
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row-2))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗

							$row++;
						}else{
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
							$row+=4;
						}

						$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,($row-3),1,$row); 
						$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,($row-3),2,$row); 
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,($row-3))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	//靠右

						for($rq=($row-3); $rq<=$row; $rq++){
							//下全框線
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$rq)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$rq)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$rq)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
						}

						/*$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row-1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row-2))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,($row-3))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗*/
					}else{

						//echo $q.' '.$row; echo '<br />';
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

						$objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $row)->setValueExplicit($rs[$q], PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//右框
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(1,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);//左框線加粗
					}

					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2,$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	//靠右
					$row++;
				}
			}
			$row++;
		}

		//輸出
		$filename = date("YmdHis");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('excel/'.$filename.'.xlsx');
		
		//下載excel 
		$this->saveExcel($filename);
		unlink('excel/'.$filename.'.xlsx');
		exit();
	}


	public function loadExcel(){
		date_default_timezone_set('Asia/Taipei');
		set_time_limit(0); //設定 PHP 執行時間無限制
		ini_set('memory_limit','1024m');//設定記憶體
		include 'phpExcel.1.8.0/Classes/PHPExcel.php';
		include 'phpExcel.1.8.0/Classes/PHPExcel/Writer/Excel2007.php';
		require_once 'phpExcel.1.8.0/Classes/PHPExcel/IOFactory.php';
	}
	
	public function saveExcel($filename){
		ob_start();
		Header ( 'Cache-Control: no-cache, must-revalidate' );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=".$filename.".xlsx");
		header("Content-Length: ".filesize('excel/'.$filename.'.xlsx'));
		@readfile('excel/'.$filename.'.xlsx');
		ob_flush();
	}

}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
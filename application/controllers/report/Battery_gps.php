<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Battery_gps extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("report/Model_battery_gps", "Model_battery_gps") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("battery_gps", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("battery_gps", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_battery_gps->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_battery_gps->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池gps报表');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "report/battery_gps_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		//檢查是否預設進來
		$this->session->set_userdata('searchType', "battery_gps");
		$fn = get_fetch_class_random();
		$searchStart = $this->session->userdata("{$fn}_".'searchStart');
		$date_start = $this->input->post('date_start');
		$date_end = $this->input->post('date_end');

		if($date_start!='' && $date_end != ''){
			$this->session->set_userdata("{$fn}_".'searchStart', 'Y');
			$searchArr = array();
			$searchArr[0] = "system_log_date[:;]between[:;]{$date_start}[:;]{$date_end}";
			$this->session->set_userdata("{$fn}_".'searchData', $searchArr);
		}else{
			$searchArr = setSearch2Arr($this->input->post());
			$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

			//最外层的search_txt
			$search_txt = $this->input->post("search_txt");
			$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);
		}

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}

	public function check_download(){
		date_default_timezone_set("Asia/Taipei");
		$search_start_date = $this->input->post('date_start');
		$search_end_date = $this->input->post('date_end');
		
		$whereStr = "";
		
		if($search_start_date != '' && $search_end_date != ''){
			$whereStr .= " and (date(ldi.system_log_date) >= '{$search_start_date}' and date(ldi.system_log_date) <= '{$search_end_date}') ";
		}else if($search_start_date != ''){
			$whereStr .= " and date(ldi.system_log_date) >= '{$search_start_date}' ";
		}else if($search_end_date != ''){
			$whereStr .= " and date(ldi.system_log_date) <= '{$search_end_date}' ";
		}

		$rs = $this->Model_battery_gps->search_all_battery($whereStr);
		$cell_name = array("电池序号","出厂日期","电池位置(经纬度)","电池电量");
		$cell_en_name = array("battery_id","manufacture_date","gps_position","battery_capacity");
		$total_cell = count($cell_name);

		
		$set_title = 'battery_gps';
		//讀取excel元件
		$this->loadExcel();
		
		//初始化
		$objPHPExcel = new PHPExcel(); 
		for($x=0; $x<1; $x++){
			//寫入
			$objPHPExcel->createSheet($x);
			$objPHPExcel->setActiveSheetIndex($x);
			$objPHPExcel->getActiveSheet()->setTitle($set_title); //頁籤

			//預設
			$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');	//預設字型
			$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(18);	//設定欄位寬度
			$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	//垂直置中
			$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); //水平置中

			$objPHPExcel->getActiveSheet()->getColumnDimension( 'A' )->setWidth(40); //欄位寬度  
			$objPHPExcel->getActiveSheet()->getColumnDimension( 'C' )->setWidth(22); //欄位寬度  

			/*
				getHorizontal()：获得水平居中方式
				getVertical()：获得垂直居中方式
				setHorizontal()：设置水平居中方式，返回对齐方式对象
				setVertical()：设置垂直居中方式，返回对齐方式对象			
			*/
			//$objPHPExcel->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //邊框

			$row=1;
			/*
				getAllborders() 全框線
				getTop() 上框線
				getBottom() 下框線
				getLeft() 左框線
				getRight()	右框線
				BORDER_MEDIUM 粗線
				BORDER_THIN 細線
				getStyle('A3:D3')
			*/

			for($s=0; $s<$total_cell; $s++){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($s, $row, $cell_name[$s]);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //上框線加粗
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //下框線加粗
				
				if($s==0){
					//左框線加粗
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
				}

				if($s==($total_cell-1)){
					//右框線加粗
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
				}
			}

			$row++;

			if(isset($rs)){
				foreach($rs as $arr){
					for($j=0; $j<$total_cell; $j++){

						$objPHPExcel->getActiveSheet()->getCellByColumnAndRow($j, $row)->setValueExplicit($arr[$cell_en_name[$j]], PHPExcel_Cell_DataType::TYPE_STRING);//設定欄位為字串格式
						//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, $row, $arr[$cell_en_name[$j]]);

						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						
						if($j==0){
							//左框線加粗
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
						}

						if($j==($total_cell-1)){
							//右框線加粗
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
						}
					}
					$row++;
				}
			}
			$row++;
		}

		//輸出
		$filename = date("YmdHis");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('excel/'.$filename.'.xlsx');
		
		//下載excel 
		$this->saveExcel($filename);
		unlink('excel/'.$filename.'.xlsx');
		exit();
	}


	public function loadExcel(){
		date_default_timezone_set('Asia/Taipei');
		set_time_limit(0); //設定 PHP 執行時間無限制
		ini_set('memory_limit','1024m');//設定記憶體
		include 'phpExcel.1.8.0/Classes/PHPExcel.php';
		include 'phpExcel.1.8.0/Classes/PHPExcel/Writer/Excel2007.php';
		require_once 'phpExcel.1.8.0/Classes/PHPExcel/IOFactory.php';
	}
	
	public function saveExcel($filename){
		ob_start();
		Header ( 'Cache-Control: no-cache, must-revalidate' );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=".$filename.".xlsx");
		header("Content-Length: ".filesize('excel/'.$filename.'.xlsx'));
		@readfile('excel/'.$filename.'.xlsx');
		ob_flush();
	}

}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
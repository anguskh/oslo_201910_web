<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Supplementary extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spSupplementaryArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("customer/Model_supplementary", "Model_supplementary") ;
		$this->load->helper("select_helper");
		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->library("my_splitpage", $spSupplementaryArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("supplementary", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("supplementary", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		/*$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_supplementary->getSupplementaryAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:tbl_supplementary的资料
		$data["supplementaryInfoRow"] = $this->Model_supplementary->getSupplementaryList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["supplementaryInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		$menu_arr = $this->model_access->getNowMenuSn('客户反馈管理');
		$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		$data["member_list"] = $this->Model_show_list->getMemberList();

		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "customer/supplementary_list", $data ) ;
		$this->load->view( "common/footer") ;*/

		$this->addition();
	}
	
	/**
	 * 取得样版下拉选单
	 */
	public function getpattern(){
		$this->Model_supplementary->getpattern();
	}

	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//如果是有商店和前台登入使用者, 新增帐号为系统自动给予
		if($this->session->userdata('user_merchant') != "" && $this->session->userdata('login_side') == '0'){
			$data['autoUserID'] = $this->model_user->autoUserID();
		}

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('线上补登电池序号');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();

		//取得會員資料表
		$data['memberList'] = $this->Model_show_list->getMemberList();

		//取得電池序號資料表
		$data['batteryList'] = $this->Model_show_list->getbatteryList();

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "customer/supplementary_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_supplementary->insertData() ;
		} else {  //有流水号为修改
			$this->Model_supplementary->updateData() ;
		}		
	}
	
 }
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Memberpic extends CI_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spMemberArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("member/Model_member", "Model_member") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("member", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("member", $this->session->userdata('display_language'));
		}
	}

	public function getImageSrc($s_num, $img_name){
	  $tmpRow = $this->Model_member->getMemberInfo( $s_num ) ;
	  $image = $tmpRow[0][$img_name];
	  header('Content-Type: image/jpeg'); //or whatever
	  echo $image;
	}
}
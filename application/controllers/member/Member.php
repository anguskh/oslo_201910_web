<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Member extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spMemberArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("member/Model_member", "Model_member") ;
		$this->load->helper("select_helper");
		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("inquiry/Model_log_battery_leave_return", "Model_log_battery_leave_return") ;
		$this->load->library("my_splitpage", $spMemberArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("member", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("member", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_member->getMemberAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:tbl_member的资料
		$data["memberInfoRow"] = $this->Model_member->getMemberList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["memberInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('会员管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		//查儲值方式
		$data["stored_type"] = $this->Model_show_list->getstored_type();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "member/member_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 取得样版下拉选单
	 */
	public function getpattern(){
		$this->Model_member->getpattern();
	}

	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//如果是有商店和前台登入使用者, 新增帐号为系统自动给予
		if($this->session->userdata('user_merchant') != "" && $this->session->userdata('login_side') == '0'){
			$data['autoUserID'] = $this->model_user->autoUserID();
		}

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('会员管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
	
		//查租借项目
		$data["tlp_list"] = $this->Model_show_list->getleasepriceList();

		//查经销商
		$data["dealer_list"] = $this->Model_show_list->getdealerList();

		//查子经销商
		$data["sub_dealer_list"] = $this->Model_show_list->getsubdealerList();

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//查儲值方式
		$data["stored_type"] = $this->Model_show_list->getstored_type();

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "member/member_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_member->insertData() ;
		} else {  //有流水号为修改
			$this->Model_member->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($s_num = "")
	{
		$this->form_create($s_num);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($s_num = "")
	{
		$this->form_create($s_num, true);
	}	
	
	//修改检视页面
	function form_create($s_num = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$s_num = $this->input->post("ckbSelArr");
		$s_num = $s_num[0];
		
		$tmpRow = $this->Model_member->getMemberInfo( $s_num ) ;
		$data["memberInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["memberInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('会员管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		//查租借项目
		$data["tlp_list"] = $this->Model_show_list->getleasepriceList();

		//查经销商
		$data["dealer_list"] = $this->Model_show_list->getdealerList();

		//查子经销商
		$data["sub_dealer_list"] = $this->Model_show_list->getsubdealerList();

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//查儲值方式
		$data["stored_type"] = $this->Model_show_list->getstored_type();

		//查會員微信資訊資料表
		$wechatInfo = $this->Model_show_list->getwechatinfoList($s_num);
		if(count($wechatInfo)>0){
			$data["wechatInfo"] = $wechatInfo[0];
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "member/member_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_member->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "member");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	public function getImageSrc($s_num, $img_name){
	  $tmpRow = $this->Model_member->getMemberInfo( $s_num ) ;
	  $image = $tmpRow[0][$img_name];
	  header('Content-Type: image/jpeg'); //or whatever
	  echo $image;
	 }

	//租借记录
	public function detail_record( $member_sn = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		$member_main = $this->Model_member->getMemberInfo($member_sn) ;
		$data["member_name"] = $member_main[0]['name'] ;
		$data["member_sn"] = $member_sn;

		$d_type = 'record';
		$data["d_type"] = $d_type;

		$totalRow = $this->Model_member->getDetailAllCnt($data["member_sn"], $d_type) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_member->getDetailList( $this->session->userdata('paging_rows'), $startRow, $data["member_sn"], $d_type ) ;

		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $member_sn );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('会员管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "member/member_record_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	//储值扣款记录
	public function detail_pay( $member_sn = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		$member_main = $this->Model_member->getMemberInfo($member_sn) ;
		$data["member_name"] = $member_main[0]['name'] ;
		$data["member_sn"] = $member_sn;


		$d_type = 'pay';
		$data["d_type"] = $d_type;

		$totalRow = $this->Model_member->getDetailAllCnt($data["member_sn"], $d_type) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_member->getDetailList( $this->session->userdata('paging_rows'), $startRow, $data["member_sn"], $d_type ) ;

		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $member_sn );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('会员管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "member/member_pay_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 搜寻作业　
	 */
	public function detail_search () 
	{
		$this->session->set_userdata('searchType', "log_battery_leave_return");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData_detail', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt_detail', $search_txt);

		$member_sn = $this->input->post("member_sn");
		$main_startRow = $this->input->post("main_startRow");
		$d_type = $this->input->post("d_type");

		if($d_type == 'record'){
			$get_full_url_random = get_full_url_random().'/detail_record/'.$member_sn.'/'.$main_startRow;
		}else{
			$get_full_url_random = get_full_url_random().'/detail_pay/'.$member_sn.'/'.$main_startRow;
		}

		redirect($get_full_url_random, 'refresh');
	}
	
	//匯入txt
	public function import(){
		$this->Model_member->import();
	}

	//路線軌跡圖
	public function detail_map_trail($s_num = '')
	{
		$data["s_num"] = $s_num;
		$tmpRow = $this->Model_log_battery_leave_return->getlogInfo($s_num) ;
		
		$data["logInfo"] = $tmpRow;
		$this->load->view( "member/battery_map_trail", $data ) ;
	}
	 
 }
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
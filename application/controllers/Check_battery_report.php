<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check_battery_report extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Clear_driving_info的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->model("model_check_battery_report", "model_check_battery_report") ;
    }

    public function index()
    {
    	$this->model_check_battery_report->check_battery_report();
    }
    
}

/* End of file Clear_api_history_data.php */
/* Location: ./application/controllers/Clear_api_history_data.php */
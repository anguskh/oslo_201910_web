<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends CI_Controller {
	
	/**
	 * 建构式
	 * 预先载入Login的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_get_data", "Model_get_data") ;
		$this->load->helper("select_helper");
		$this->load->helper("common"); //补助函数, 通用
		
		//语系
		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("request", $this->session->userdata('default_language'));
			$this->lang->load("order", $this->session->userdata('default_language'));
			$this->lang->load("inspect", $this->session->userdata('default_language'));
			$this->lang->load("ship", $this->session->userdata('default_language'));
			$this->lang->load("receivable", $this->session->userdata('default_language'));
			$this->lang->load("outstock", $this->session->userdata('default_language'));
			$this->lang->load("trans", $this->session->userdata('default_language'));
			$this->lang->load("check", $this->session->userdata('default_language'));
			$this->lang->load("inv_main", $this->session->userdata('default_language'));
			$this->lang->load("ncccins", $this->session->userdata('default_language'));
			$this->lang->load("ncccother_fee", $this->session->userdata('default_language'));
			$this->lang->load("oil_fee", $this->session->userdata('default_language'));
			$this->lang->load("contract_main", $this->session->userdata('default_language'));
			$this->lang->load("machine_stock", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("request", $this->session->userdata('display_language'));
			$this->lang->load("order", $this->session->userdata('display_language'));
			$this->lang->load("inspect", $this->session->userdata('display_language'));
			$this->lang->load("ship", $this->session->userdata('display_language'));
			$this->lang->load("receivable", $this->session->userdata('display_language'));
			$this->lang->load("outstock", $this->session->userdata('display_language'));
			$this->lang->load("trans", $this->session->userdata('display_language'));
			$this->lang->load("check", $this->session->userdata('display_language'));
			$this->lang->load("inv_main", $this->session->userdata('display_language'));
			$this->lang->load("ncccins", $this->session->userdata('default_language'));
			$this->lang->load("ncccother_fee", $this->session->userdata('display_language'));
			$this->lang->load("oil_fee", $this->session->userdata('display_language'));
			$this->lang->load("contract_main", $this->session->userdata('default_language'));
			$this->lang->load("machine_stock", $this->session->userdata('default_language'));

		}
    }
	//自动搜寻start==============================================
	
	//商店自动搜寻
	public function autocomplete_mcht(){
		$this->Model_show_list->autocomplete_mcht();
	}	

	//商店自动搜寻
	public function autocomplete_mcht_bank(){
		$this->Model_show_list->autocomplete_mcht_bank();
	}	

	//商店名称自动搜寻
	public function autocomplete_mcht_name(){
		$this->Model_show_list->autocomplete_mcht_name();
	}	

	//城市自动搜寻
	public function autocomplete_city(){
		$this->Model_show_list->autocomplete_city();
	}
	
	//部门自动搜寻
	public function autocomplete_dept(){
		$this->Model_show_list->autocomplete_dept();
	}
	
	//办公室地点自动搜寻
	public function autocomplete_location(){
		$this->Model_show_list->autocomplete_location();
	}
	
	//地区自动搜寻
	public function autocomplete_district(){
		$this->Model_show_list->autocomplete_district();
	}
	
	//职位自动搜寻
	public function autocomplete_position(){
		$this->Model_show_list->autocomplete_position();
	}
	
	//客户类别自动搜寻
	public function autocomplete_basic_code($kind_no = ""){
		$this->Model_show_list->autocomplete_basic_code($kind_no);
	}

	//使用者自动搜寻
	public function autocomplete_emp_id(){
		$this->Model_show_list->autocomplete_emp_id();
	}	
	//使用者自动搜寻, 安装人员
	public function autocomplete_emp_id_install(){
		$this->Model_show_list->autocomplete_emp_id("AND install_per = 1");
	}

	//使用者自动搜寻, 请购人员
	public function autocomplete_emp_id_Odr(){
		$this->Model_show_list->autocomplete_emp_id(" and emp.isp_per = 1 ");
		// $this->Model_show_list->autocomplete_emp_id();
	}
	
	//使用者自动搜寻, 验收人员
	public function autocomplete_emp_id_Isp_Odr(){
		$this->Model_show_list->autocomplete_emp_id(" and emp.dept_no = '004' ");
		// $this->Model_show_list->autocomplete_emp_id();
	}
	
	//使用者(厂商员工)自动搜寻
	public function autocomplete_emp_id_D(){
		$this->Model_show_list->autocomplete_emp_id_D();
	}
	
	//使用者(部门主管)自动搜寻
	public function autocomplete_emp_id_M(){
		$this->Model_show_list->autocomplete_emp_id_M(" and emp.fulltime = 1 
														and (emp.position like '%M' or emp.position = 'DR'  or emp.position = 'SDM-B' )");
	}
	
	//使用者(订购部门主管)自动搜寻
	public function autocomplete_emp_id_M_odr(){
		// $this->Model_show_list->autocomplete_emp_id_M(" and su.isp_per = 1 
														// and substr(su.position, -1) = 'M' ");
		$this->Model_show_list->autocomplete_emp_id_M(" and substr(su.position, -1) = 'M' or emp.position = 'DR'  or emp.position = 'SDM-B' ");
	}
	
	//出货人员
	public function autocomplete_ship_per(){
		$this->Model_show_list->autocomplete_emp_id(" and emp.isp_per = 1 ");
	}
	
	//发货人员
	public function autocomplete_deliver_per(){
		$this->Model_show_list->autocomplete_emp_id(" and emp.deliver_per = 1 ");
	}

	//银行自动搜寻
	public function autocomplete_bank_no(){
		$this->Model_show_list->autocomplete_bank_no();
	}
	
	//银行自动搜寻2
	public function autocomplete_bank_no2(){
		$this->Model_show_list->autocomplete_bank_no2();
	}
	
	//料品类别自动搜寻
	public function autocomplete_kind_no(){
		$this->Model_show_list->autocomplete_kind_no();
	}
	
	//机型自动搜寻
	public function autocomplete_machine_type_no($kind_no = ""){
		$this->Model_show_list->autocomplete_machine_type_no($kind_no);
	}

	//料品自动搜寻
	public function autocomplete_item_no($kind_no = ""){
		$this->Model_show_list->autocomplete_item_no($kind_no);
	}
	
	//厂商自动搜寻
	public function autocomplete_vnd_no(){
		$this->Model_show_list->autocomplete_vnd_no();
	}
	
	//仓管人员自动搜寻
	public function autocomplete_instock_per(){
		$this->Model_show_list->autocomplete_emp_id(' and fulltime = 1 and stock_per = 1 ');
	}
	
	//仓管人员2自动搜寻
	public function autocomplete_instock_per2(){
		$this->Model_show_list->autocomplete_emp_id(' and onduty = 1 and fulltime = 1 and stock_per = 1 ');
	}

	//邮递区号自动搜寻
	public function autocomplete_post_tel(){
		$this->Model_show_list->autocomplete_post_tel();
	}

	//汇款银行
	public function autocomplete_pay_bank(){
		$this->Model_show_list->autocomplete_pay_bank();
	}

	//行业
	public function autocomplete_industry_code(){
		$this->Model_show_list->autocomplete_industry_code();
	}

	//分行代号
	public function autocomplete_pay_branch(){
		$this->Model_show_list->autocomplete_pay_branch();
	}
	
	//版本
	public function autocomplete_version(){
		$this->Model_show_list->autocomplete_version();
	}
	
	//自动搜寻end==============================================
	
	//promptWin弹出选择start===================================
		//银行
	public function promptWin_bank($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["bank_no_list"] = $this->Model_show_list->bank_no_list();
		$this->load->view( "common/promptWin_bank", $data) ;
	}
	
		//料品类别
	public function promptWin_item_kind($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["kind_no_list"] = $this->Model_show_list->kind_no_list();
		$this->load->view( "common/promptWin_item_kind", $data) ;
	}
	
		//请购单
	public function promptWin_request($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('i3');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list(' and emp.isp_per = 1 ');
		$this->load->view( "common/promptWin_request", $data) ;
	}
	
		//订购单
	public function promptWin_order($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('i3');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list(' and emp.isp_per = 1 ');
		$this->load->view( "common/promptWin_order", $data) ;
	}
	
		//验收单
	public function promptWin_inspect($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('i3');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list(' and emp.isp_per = 1 ');
		$this->load->view( "common/promptWin_inspect", $data) ;
	}
	
	public function promptWin_ship($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('SC');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list();
		$this->load->view( "common/promptWin_ship", $data) ;
	}
	
	public function promptWin_receivable($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('SC');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list(' and fulltime = 1 and stock_per = 1 ');
		$this->load->view( "common/promptWin_receivable", $data) ;
	}
	
	public function promptWin_outstock($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('OS');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list(' and fulltime = 1 and stock_per = 1 ');
		$this->load->view( "common/promptWin_outstock", $data) ;
	}
	
	public function promptWin_trans($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["basic_code_list"] = $this->Model_show_list->basic_code_list('TC');
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list('and onduty = 1 and fulltime = 1 and stock_per = 1 ');
		$this->load->view( "common/promptWin_trans", $data) ;
	}

	public function promptWin_check($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["emp_id_list"] = $this->Model_show_list->emp_id_list();
		$this->load->view( "common/promptWin_check", $data) ;
	}
	
		//库存开档作业
	public function promptWin_YYMM($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$this->load->view( "common/promptWin_YYMM", $data) ;
	}

		//机型
	public function promptWin_machinetype($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$data["machine_type_list"] = $this->Model_show_list->machine_type_list2();
		$data["bank_no_list"] = $this->Model_show_list->bank_no_list();
		$this->load->view( "common/promptWin_machinetype", $data) ;
	}

		//库存明细
	public function promptWin_inv_main_all($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		
		//料品类别
		$data["kind_no_list"]= $this->Model_show_list->kind_no_list();
		//料品
		$data["item_no_list"]= $this->Model_show_list->item_no_list();
		//仓库
		$data["warehouse_no_list"] = $this->Model_show_list->basic_code_list('WH');
		$this->load->view( "common/promptWin_inv_main_all", $data) ;
	}

		//工作型态
	public function promptWin_basic_code($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		//清单
		$kind_no = ($src_calss=='oil_fee')?'WT':'NW';
		if($src_calss == 'oil_fee'){
			$data["bank_no_list"] = $this->Model_show_list->bank_no_list(" and (bk.bank_no = '01' or  bk.bank_no = '24')");
		}else if($src_calss == 'ncccother_fee' || $src_calss == 'emp_work_fee' ){
			$data["bank_no_list"] = $this->Model_show_list->bank_no_list();
		}

		if($src_calss == 'ncccother_fee'){
			$data["work_type_list"] = $this->Model_show_list->basic_code_list2($kind_no);
		}else{
			$data["work_type_list"] = $this->Model_show_list->basic_code_list($kind_no);
		}

		$this->load->view( "common/promptWin_basic_code", $data) ;
	}
		//签约
	public function promptWin_district($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		$data["district_list"] = $this->Model_show_list->district_list();
		$this->load->view( "common/promptWin_district", $data) ;
	}
	
		//领料出库明细表
	public function promptWin_machine_stock($src_calss = ""){
		$this->session->unset_userdata('searchType');
		$this->session->unset_userdata('searchData');
		//乱数fnuction name
		$data["fn"] = $this->input->post("fn");
		
		//清除子搜寻的内容, 避免弹出和子搜寻内容造成混淆
		$this->session->unset_userdata("{$data["fn"]}_".'searchData');
		
		//来源Class
		$data["src_calss"] = $src_calss;
		$data["machine_type_no_list"] = $this->Model_show_list->machine_list();
		$this->load->view( "common/promptWin_machine_stock", $data) ;
	}

	//promptWin弹出选择end===================================

	public function get_version_list()
	{
		$bank_no = $this->input->post("bank_no");
		$where = '';
		if ($bank_no != ''){
			$where = "AND bank_no = '{$bank_no}'";
		}

		$ary_version = $this->Model_show_list->version_no_list($where);
		echo("<option value=''>请选择银行版本</option>");
		foreach ($ary_version as $key => $value) {
			$option_value =  $value['version_no2'];
			$option_text =  $value['version_name'];
			echo ("<option value='{$option_value}' val_name='{$option_value} | {$option_text}'>{$option_text}</option>");
		}
	}	

	public function get_posap_list()
	{
		$bank_no = $this->input->post("bank_no");
		$where = '';
		if ($bank_no != ''){
			$where = "AND bank_no = '{$bank_no}'";
		}

		$ary_version = $this->Model_show_list->machineap_list($where);
		echo("<option value=''>请选择POSAP行版本</option>");
		foreach ($ary_version as $key => $value) {
			$option_value =  $value['ap_no'];
			$option_text =  $value['ap_text'];
			echo ("<option value='{$option_value}' val_name='{$option_text}'>{$option_text}</option>");
		}
	}

	public function get_item_list()
	{
		$kind_no = $this->input->post("kind_no");
		$where = '' ;
		if ( $kind_no != '') {
			$where = "AND it.kind_no='".$kind_no."'";
		}

		$item_list = $this->Model_show_list->item_no_list($where);
		echo json_encode($item_list);
	}

	public function get_item_select()
	{
		$kind_no_ary = $this->input->post("kind_no");
		$item_no = $this->input->post("item_no");
		$multiple = $this->input->post("multiple");
		$whereStrs ="";
		
		if($kind_no_ary != 'null'){
			$kindArr = join( "','", $kind_no_ary );
			$kindArr = "'".$kindArr."'";
			$whereStrs .= " and it.kind_no IN ({$kindArr})";
		}

		$item_list = $this->Model_show_list->item_no_list($whereStrs);
		$item_no_tmp ="<select id='item_no' multiple='{$multiple}'>";
		foreach($item_list as $item_arr){
			$selected = '';
			if($item_no != 'null'){
				if(in_array($item_arr['item_no'],$item_no)){
					$selected = 'selected';
				}
			}
			$item_no_tmp .= '<option value="'.$item_arr['item_no'].'" '.$selected.'>'.$item_arr['item_no'].'｜'.$item_arr['item_name'].'</option>';
		}
		$item_no_tmp .= '</select>';

		echo json_encode($item_no_tmp);
	}

	public function get_custom_data()
	{
		$mcht_id = $this->input->post("mcht_id");
		$custom_data ='';
		if ($mcht_id != '') {
			$custom_data = $this->Model_show_list->custom_data($mcht_id);
		}
		echo json_encode($custom_data);
	}
	
	public function get_address_zip_code()
	{
		$address = $this->input->post("address");
		$zip_data ='';
		if ($address != '') {
			$zip_data = $this->Model_show_list->get_address_zip_code($address);
		}
		echo json_encode($zip_data);
	}
	
	public function get_zip_code_installer()
	{
		$zip_code = $this->input->post("zip_code");
		$emp_id ='';
		if ($zip_code != '') {
			$emp_id = $this->Model_show_list->get_zip_code_installer($zip_code);
		}
		echo json_encode($emp_id);
	}

	public function get_custom_list(){
		$bank_no = $this->input->post("bank_no");
		$where = '';
		if ($bank_no != ''){
			$where = "AND bank_no = '{$bank_no}'";
		}

		$ary_custom = $this->Model_show_list->mcht_id_list($where);
		echo("<option value=''>请选择商店</option>");
		foreach ($ary_custom as $key => $value) {
			$option_value =  $value['mcht_id'];
			$option_text =  $value['mcht_name'];
			echo ("<option value='{$option_value}'>{$option_text}</option>");
		}
	}

	// 取得财编+端代
	public function get_wealth_term_no()
	{
		echo json_encode( $this->Model_get_data->get_wealth_term_no() );
	}	
	// 取得机器序号+端代
	public function get_machine_no()
	{
		echo json_encode( $this->Model_get_data->get_machine_no() );
	}	
	// 取得机器序号+财编
	public function get_WtnoMno()
	{
		echo json_encode( $this->Model_get_data->get_WtnoMno() );
	}
	// 取得stock 机器序号+财编
	public function get_StockNo()
	{
		echo json_encode( $this->Model_get_data->get_StockNo() );
	}
	
	// 取得安装的端末机最新的商店资料
	public function get_last_mid()
	{
		echo json_encode( $this->Model_get_data->get_last_mid() );
	}

	//检查该笔单号机器序号是否重覆
	public function check_machine_no(){
		$this->Model_get_data->check_machine_no();
	}
}

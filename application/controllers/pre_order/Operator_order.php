<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Operator_order extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spOperator_orderArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("pre_order/Model_operator_order", "Model_operator_order") ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->library("my_splitpage", $spOperator_orderArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("operator_order", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("operator_order", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_operator_order->getoperatorAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:tbl_operator_order的资料
		$data["suboperatorInfoRow"] = $this->Model_operator_order->getDealerList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["operatorInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('营运商预购次数管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2'");

		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/operator_order_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		$data["s_num"] = "";

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('营运商预购次数管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		$data["c_num"] = $this->Model_show_list->getMemberCount(" and SUBSTR(user_id,1,3) = 'MOD'");

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2' and top08 = 0");

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/operator_order_form", $data ) ;
		$this->load->view( "common/footer") ;
	}

	/**
	 * 修改作业 页面
	 */
	public function modification($s_num = "")
	{
		$this->form_create($s_num);
	}
	
	//修改检视页面
	function form_create($s_num = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$s_num = $this->input->post("ckbSelArr");
		$s_num = $s_num[0];
		$data["s_num"] = $s_num;
		
		$tmpRow = $this->Model_operator_order->getCard( $s_num ) ;
		$data["CardInfo"] = $tmpRow ;

		$data["c_num"] = $this->Model_show_list->getMemberCount(" and SUBSTR(user_id,1,3) = 'MOD'");

		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["CardInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('营运商预购次数管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2'");

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/operator_order_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	//新增卡片
	public function additionCard () 
	{
		$this->Model_operator_order->insertCardData() ;
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($s_num = "")
	{
		$this->form_create($s_num, true);
	}	
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_operator_order->insertData() ;
		} else {  //有流水号为修改
			$this->Model_operator_order->updateData() ;
		}		
	}

	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "operator_order");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}


	//明細
	public function op_detail( $to_num = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		//查营运商
		$operator_list = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$data["top_name"] = $operator_list[0]['top01'] ;
		$data["to_num"] = $to_num;

		$totalRow = $this->Model_operator_order->getDetailAllCnt($to_num) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_operator_order->getDetailList( $this->session->userdata('paging_rows'), $startRow, $to_num) ;

		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $to_num );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('营运商预购次数管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/operator_order_detail_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	

	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Card_management extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spCard_managementArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("pre_order/Model_card_management", "Model_card_management") ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("member/Model_member", "Model_member") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->library("my_splitpage", $spCard_managementArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("card_management", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("card_management", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_card_management->getSuboperatorAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:tbl_card_management的资料
		$data["suboperatorInfoRow"] = $this->Model_card_management->getSubDealerList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["operatorInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('卡片管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2'");

		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/card_management_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('卡片管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2' and top08 > 0");

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/card_management_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_card_management->insertData() ;
		} else {  //有流水号为修改
			$this->Model_card_management->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($s_num = "")
	{
		$this->form_create($s_num);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($s_num = "")
	{
		$this->form_create($s_num, true);
	}	
	
	
	//修改检视页面
	function form_create($s_num = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$s_num = $this->input->post("ckbSelArr");
		$s_num = $s_num[0];
		
		$tmpRow = $this->Model_card_management->getCardMember( $s_num ) ;
		$data["card_managementInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["card_managementInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('卡片管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList(" and top02 = '2' and top08 > 0");

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/card_management_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_card_management->deleteData();
	}
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "card_management");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	

	//明細
	public function card_detail( $tm_num = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		//查名称
		$member_list = $this->Model_card_management->getCardMember($tm_num);
		$data["tm_name"] = $member_list[0]['name'] ;
		$data["tm_num"] = $tm_num;

		$totalRow = $this->Model_card_management->getDetailAllCnt($tm_num) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_card_management->getDetailList( $this->session->userdata('paging_rows'), $startRow, $tm_num) ;

		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $tm_num );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('卡片管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/card_management_detail_list", $data ) ;
		$this->load->view( "common/footer") ;
	}

	//租借记录
	public function detail_record( $member_sn = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		//查名称
		$member_main = $this->Model_member->getMemberInfo($member_sn) ;
		$data["member_name"] = $member_main[0]['name'] ;
		$data["member_sn"] = $member_sn;

		$d_type = 'record';
		$data["d_type"] = $d_type;

		$totalRow = $this->Model_member->getDetailAllCnt($member_sn, $d_type) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_member->getDetailList( $this->session->userdata('paging_rows'), $startRow, $data["member_sn"], $d_type ) ;

		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $member_sn );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('卡片管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "pre_order/card_management_record_list", $data ) ;
		$this->load->view( "common/footer") ;
	}

	/**
	 * 搜寻作业　
	 */
	public function detail_search () 
	{
		$this->session->set_userdata('searchType', "log_battery_leave_return");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData_detail', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt_detail', $search_txt);

		$member_sn = $this->input->post("member_sn");
		$main_startRow = $this->input->post("main_startRow");
		$d_type = $this->input->post("d_type");
		$get_full_url_random = get_full_url_random().'/detail_record/'.$member_sn.'/'.$main_startRow;

		redirect($get_full_url_random, 'refresh');
	}

	/**
	 * 匯出資訊　
	 */
	public function export()
	{
		date_default_timezone_set("Asia/Taipei");

		$export_type = $this->input->post("export_type");
		$search_start_date = $this->input->post('date_start');
		$search_end_date = $this->input->post('date_end');
		if($export_type==1)
		{
			$searchdataArr = $this->Model_card_management->getexportdata();
			$cell_en_name = array("top01","name","nick_name","user_id","lease_type","lease_price");
			$cell_name = array("营运商名称","卡片名称","保管人姓名","qrcode内容","预购类型","单次金额");
			$set_title = "card";
			$filename = "card_data";
			$this->exportExcel($cell_name,$cell_en_name,$set_title,$filename,$searchdataArr,$export_type);


		}
		else if($export_type==2)
		{
			$date_type = $this->input->post("date_type");
			$date_month = $this->input->post("date_month");
			$date_season = $this->input->post("date_season");
			if($date_type == 3){
				//月份
				$search_start_date = date("Y-m-d",mktime(0,0,0,substr($date_month,5,2),1,substr($date_month,0,4)));
				$search_end_date = date("Y-m-d",mktime(0,0,0,substr($date_month,5,2)+1,0,substr($date_month,0,4)));
			}else if($date_type == 4){
				$now_year = date("Y");
				//季
				if($date_season == 1){
					$search_start_date = date("Y-m-d",mktime(0,0,0,1,1,$now_year));
					$search_end_date = date("Y-m-d",mktime(0,0,0,4,0,$now_year));
				}else if($date_season==2){
					$search_start_date = date("Y-m-d",mktime(0,0,0,4,1,$now_year));
					$search_end_date = date("Y-m-d",mktime(0,0,0,7,0,$now_year));
				}else if($date_season==3){
					$search_start_date = date("Y-m-d",mktime(0,0,0,7,1,$now_year));
					$search_end_date = date("Y-m-d",mktime(0,0,0,10,0,$now_year));
				}else if($date_season==4){
					$search_start_date = date("Y-m-d",mktime(0,0,0,10,10,$now_year));
					$search_end_date = date("Y-m-d",mktime(0,0,0,13,0,$now_year));
				}
			}

			$trans_type = $this->input->post("trans_type");

			$whereStr = "";
			$date_content = "";	
			if($search_start_date != '' && $search_end_date != ''){
				$whereStr .= " and (date(lblr.leave_date) >= '{$search_start_date}' and date(lblr.leave_date) <= '{$search_end_date}') ";
				$date_content = $search_start_date."～".$search_end_date;
			}else if($search_start_date != ''){
				$whereStr .= " and date(lblr.leave_date) >= '{$search_start_date}' ";
				$date_content = $search_start_date."～";
			}else if($search_end_date != ''){
				$whereStr .= " and date(lblr.leave_date) <= '{$search_end_date}' ";
				$date_content = "～".$search_end_date;
			}else{
				$date_content = "全部";
			}
			
			if($trans_type == '2'){
				//預購
				$whereStr .= " and tm.lease_type = 1 ";
				
			}else if($trans_type == '3'){
				//結算
				$whereStr .= " and tm.lease_type = 2 ";
			}

			$searchdataArr = $this->Model_card_management->getleave_return($whereStr);
			$cell_en_name = array("user_id","member_name","battery_id","system_log_date","leave_bss_id");
			$cell_name = array("qrcode内容","卡片名称","交易的电池序号","时间","站点");
			$set_title = "card";
			$filename = "card_data";

			$this->exportExcel($cell_name,$cell_en_name,$set_title,$filename,$searchdataArr,$export_type,$date_content);
		}
	}

	//産生excel
	public function exportExcel($cell_name,$cell_en_name,$set_title,$filename,$carddataArr,$export_type,$date_content=""){
		$this->loadExcel();
		$filepath = "excel/";
		$reader = PHPExcel_IOFactory::createReader('Excel2007'); // 讀取2007 excel 檔案
		// $PHPExcel = $reader->load($filepath."export.xlsx"); // 檔案名稱
		// $sheetCount = $PHPExcel->getSheetCount();//獲取工作表的數目
		// $sheetNames = $PHPExcel->getSheetNames();
		
		// $cell_name = array("商店代號","商店名稱","端末機代號");
		// $cell_en_name = array("merchant_id","merchant_name","terminal_id");
		$total_cell = count($cell_name);

		$objPHPExcel = new PHPExcel(); 
		
		if($export_type == 1){
			$x=0;
			// $sheet = $PHPExcel->getSheet($x); // 讀取第一個工作表(編號從 0 開始)
			// $sheettitle = $sheet->getTitle(); // 取得工作表名稱
			// $highestRow = $sheet->getHighestRow(); // 取得總列數
			
			// $set_title = '匯出';
			//寫入
			$objPHPExcel->createSheet($x);
			$objPHPExcel->setActiveSheetIndex($x);
			$objPHPExcel->getActiveSheet()->setTitle($set_title); //頁籤

			//預設
			$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');	//預設字型
			$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(16);	//設定欄位寬度
			$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	//垂直置中
			$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); //水平置中
			$objPHPExcel->getActiveSheet()->getColumnDimension( 'D' )->setWidth(50); //欄位寬度 


			/*
				getHorizontal()：获得水平居中方式
				getVertical()：获得垂直居中方式
				setHorizontal()：设置水平居中方式，返回对齐方式对象
				setVertical()：设置垂直居中方式，返回对齐方式对象			
			*/
			//$objPHPExcel->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //邊框

			$row=1;
			/*
				getAllborders() 全框線
				getTop() 上框線
				getBottom() 下框線
				getLeft() 左框線
				getRight()	右框線
				BORDER_MEDIUM 粗線
				BORDER_THIN 細線
				getStyle('A3:D3')
			*/

			for($s=0; $s<$total_cell; $s++){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($s, $row, $cell_name[$s]);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //上框線加粗
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //下框線加粗
				
				if($s==0){
					//左框線加粗
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
				}

				if($s==($total_cell-1)){
					//右框線加粗
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
				}

				/*設定背景色*/		
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFill()->getStartColor()->setARGB('AAFFEE'); 
			}

			$row++;


			if(isset($carddataArr)){
				foreach($carddataArr as $val){
					for($j=0; $j<$total_cell; $j++){
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, $row, $val[$cell_en_name[$j]]);
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

						if($j==0){
							//左框線加粗
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
						}

						if($j==($total_cell-1)){
							//右框線加粗
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

							//最右底是灰的
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
							$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getFill()->getStartColor()->setARGB('DCDCDC'); 
						}
					}
					$row++;
				}
			}
		}else{
			$trans_type = $this->input->post("trans_type");
			$start = 0;
			$end = 2;
			if($trans_type == 2){
				$start = 0;
				$end = 1;
			}else if($trans_type == 3){
				$start = 1;
				$end = 2;
			}

			for($k=$start; $k<$end; $k++){
				$x=$k;
				if($k==0){
					$set_title = '预购';
				}else{
					$set_title = '月结';
					$cell_en_name = array("user_id","member_name","battery_id","system_log_date","leave_bss_id","lease_price");
					$cell_name = array("qrcode内容","卡片名称","交易的电池序号","时间","站点","单次金额");
					$total_cell = count($cell_name);
				}
				// $sheet = $PHPExcel->getSheet($x); // 讀取第一個工作表(編號從 0 開始)
				// $sheettitle = $sheet->getTitle(); // 取得工作表名稱
				// $highestRow = $sheet->getHighestRow(); // 取得總列數
				
				// $set_title = '匯出';
				//寫入
				$objPHPExcel->createSheet($x);
				$objPHPExcel->setActiveSheetIndex($x);
				$objPHPExcel->getActiveSheet()->setTitle($set_title); //頁籤

				//預設
				$objPHPExcel->getDefaultStyle()->getFont()->setName('標楷體');	//預設字型
				$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(16);	//設定欄位寬度
				$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);	//垂直置中
				$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); //水平置中
				$objPHPExcel->getActiveSheet()->getColumnDimension( 'A' )->setWidth(40); //欄位寬度 
				$objPHPExcel->getActiveSheet()->getColumnDimension( 'D' )->setWidth(25); //欄位寬度 
				$objPHPExcel->getActiveSheet()->getColumnDimension( 'E' )->setWidth(50); //欄位寬度 


				/*
					getHorizontal()：获得水平居中方式
					getVertical()：获得垂直居中方式
					setHorizontal()：设置水平居中方式，返回对齐方式对象
					setVertical()：设置垂直居中方式，返回对齐方式对象			
				*/
				//$objPHPExcel->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN); //邊框

				$row=1;
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$row,($total_cell-1),$row); //(A1:F1)
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, '日期：'.$date_content);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0,$row)->getFont()->setSize(16);
				$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0,$row)->getFont()->setBold(true);

				$row++;

				/*
					getAllborders() 全框線
					getTop() 上框線
					getBottom() 下框線
					getLeft() 左框線
					getRight()	右框線
					BORDER_MEDIUM 粗線
					BORDER_THIN 細線
					getStyle('A3:D3')
				*/

				for($s=0; $s<$total_cell; $s++){
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($s, $row, $cell_name[$s]);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //上框線加粗
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM); //下框線加粗
					
					if($s==0){
						//左框線加粗
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
					}

					if($s==($total_cell-1)){
						//右框線加粗
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
					}

					/*設定背景色*/		
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($s,$row)->getFill()->getStartColor()->setARGB('AAFFEE'); 
				}

				$row++;

				$list_arr = $carddataArr[0];
				$total_count = $carddataArr[1];
				$total_money = $carddataArr[2];
				$lblr_sn = $carddataArr[3];
				if(isset($list_arr[($k+1)])){
					foreach($list_arr[($k+1)] as $tm_num=>$arr2){
						foreach($arr2 as $key2=>$val){
							for($j=0; $j<$total_cell; $j++){
								$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j, $row, $val[$cell_en_name[$j]]);
								$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getAllborders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								if($cell_en_name[$j] == 'leave_bss_id'){
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);	//靠左
								}

								if($j==0){
									//左框線加粗
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
								}

								if($j==($total_cell-1)){
									//右框線加粗
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getBorders()->	getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

									//最右底是灰的
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
									$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($j,$row)->getFill()->getStartColor()->setARGB('DCDCDC'); 
								}
							}
						}
						$row++;
					}
					//最後一行 次數
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($total_cell-1), ($row+1), '总次数：'.$total_count[($k+1)]);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(($total_cell-1),($row+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(($total_cell-1),($row+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					$row++;
					if(($k+1) == 2){
						$row++;
						//月結總金額
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($total_cell-1), ($row), '总金额：'.$total_money[($k+1)]);
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(($total_cell-1),$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						$objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(($total_cell-1),$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						
						$cancel_flag = $this->input->post("cancel_flag");
						if($cancel_flag == 1){
							$this->Model_card_management->add_cancel_flag($lblr_sn);
						}
					}
				}
			}
		}
		// $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 

		//輸出
		$filename = "cardData_".date("YmdHis");
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('excel/'.$filename.'.xlsx');
		
		//下載excel 
		$this->saveExcel($filename);
		unlink('excel/'.$filename.'.xlsx');
		exit();
		// $objWriter->save('tmpexcel/export.xlsx');

		// $export_file = 'tmpexcel/export.xlsx';
		// return $export_file;
	}


	public function loadExcel(){
		date_default_timezone_set('Asia/Taipei');
		set_time_limit(0); //設定 PHP 執行時間無限制
		ini_set('memory_limit','1024m');//設定記憶體
		include 'phpExcel.1.8.0/Classes/PHPExcel.php';
		include 'phpExcel.1.8.0/Classes/PHPExcel/Writer/Excel2007.php';
		require_once 'phpExcel.1.8.0/Classes/PHPExcel/IOFactory.php';
	}

	public function saveExcel($filename){
		ob_start();
		Header ( 'Cache-Control: no-cache, must-revalidate' );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=".$filename.".xlsx");
		header("Content-Length: ".filesize('excel/'.$filename.'.xlsx'));
		@readfile('excel/'.$filename.'.xlsx');
		ob_flush();
	}
	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
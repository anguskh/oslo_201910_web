<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 功能名称 : 历程资料查询
 * log_battery_leave_return 历程资料查询
 */
class Log_battery_leave_return extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入log_battery_leave_return的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("inquiry/Model_log_battery_leave_return", "Model_log_battery_leave_return") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		
		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("log_battery_leave_return", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("log_battery_leave_return", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 首页
	 */
	public function index ( $startRow = 0 ) 
	{

		$this->show_list_page( $startRow ) ;
	}
	
	/**
	 * 选择页面
	 */
	public function select_log_battery_leave_return()
	{
		$data["fn"] = $this->input->post("fn");
		$this->session->unset_userdata('searchType', "log_battery_leave_return");
		$this->session->unset_userdata('searchData');
		$this->load->view( "common/select_log_battery_exchange", $data ) ;
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "log_battery_leave_return");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);
		
		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	/**
	 * 
	 */
	private function show_list_page( $startRow = 0 )
	{
		$data["tgs_selBankSN"]  	= $this->session->userdata('selBankSN') ;
		$data["tgs_selMerchantSN"]  = $this->session->userdata('selMerchantSN') ;
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = 0 ;

		//if ( !empty( $data["tgs_selBankSN"] ))
			$totalRow = $this->Model_log_battery_leave_return->getCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		
		//取得选单对应资料, 用于记录档查询
		$data["menuData"] = $this->model_menu->get_MenuData();

		// 取得table:tbl_pattern_file_field的资料
		$data["InfoRow"] = $this->Model_log_battery_leave_return->getInfo( $this->session->userdata('paging_rows'), $startRow ) ;

		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電归还记录');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "inquiry/log_battery_leave_return_list.php", $data ) ;
		$this->load->view( "common/footer") ;
	}

	//路線軌跡圖
	public function detail_map_trail($s_num = '')
	{
		$data["s_num"] = $s_num;
		$tmpRow = $this->Model_log_battery_leave_return->getlogInfo($s_num) ;
		
		$data["logInfo"] = $tmpRow;
		$this->load->view( "member/battery_map_trail", $data ) ;
	}

	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電归还记录');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();

		//取得會員資料表
		$data['memberList'] = $this->Model_show_list->getMemberList();

		//取得電池序號資料表
		$data['batteryList'] = $this->Model_show_list->getbatteryList();

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "inquiry/log_battery_leave_return_form", $data ) ;
		$this->load->view( "common/footer") ;
	}

	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_log_battery_leave_return->insertData() ;
		} else {  //有流水号为修改
			$this->Model_log_battery_leave_return->updateData() ;
		}		
	}
	
	public function select_edit_label(){
		$s_num = $this->input->post("sn");
		$tmpRow = $this->Model_log_battery_leave_return->getInfo2( $s_num ) ;
		$data["dataInfo"] = $tmpRow[0] ;

		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();

		$this->load->view( "inquiry/log_battery_leave_return_form_2", $data ) ;
	}
}
/* End of file Pattern_file_field.php */
/* Location: ./application/controllers/pattern/Pattern_file_field.php */
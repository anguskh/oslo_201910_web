<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 功能名称 : 历程资料查询
 * log_swap_track_warring 历程资料查询
 */
class Log_swap_track_warring extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入log_swap_track_warring的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("nimda/model_user", "model_user") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("inquiry/model_log_swap_track_warring", "model_log_swap_track_warring") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		
		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("log_swap_track_warring", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("log_swap_track_warring", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$this->show_list_page( $startRow ) ;
	}

	/**
	 * 列表页
	 * 选择银行、商店与端末机后，列表
	 */
	public function getlist ( $startRow = 0 )
	{
		$fn = get_fetch_class_random();
		$this->show_list_page( $startRow ) ;
	}	

	
	/**
	 * 选择页面
	 */
	public function select_log_swap_track_warring()
	{
		$data["fn"] = $this->input->post("fn");
		$this->session->unset_userdata('searchType', "log_swap_track_warring");
		$this->session->unset_userdata('searchData');
		$this->load->view( "common/select_log_swap_track_warring", $data ) ;
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{

		$this->session->set_userdata('searchType', "log_swap_track_warring");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);
		
		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	/**
	 * 
	 */
	private function show_list_page( $startRow = 0 )
	{
		$data["tgs_selBankSN"]  	= $this->session->userdata('selBankSN') ;
		$data["tgs_selMerchantSN"]  = $this->session->userdata('selMerchantSN') ;
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = 0 ;

		//if ( !empty( $data["tgs_selBankSN"] ))
			$totalRow = $this->model_log_swap_track_warring->getCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		
		//取得选单对应资料, 用于记录档查询
		$data["menuData"] = $this->model_menu->get_MenuData();

		// 取得table:tbl_pattern_file_field的资料
		$data["InfoRow"] = $this->model_log_swap_track_warring->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站轨道即时告警');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();

		//查所有帳號管理人員名稱
		$data["sysusername"] = $this->Model_show_list->getSysuser();


		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "inquiry/log_swap_track_warring_list.php", $data ) ;
		$this->load->view( "common/footer") ;
	}

	public function select_edit_label(){
		$s_num = $this->input->post("sn");
		$tmpRow = $this->model_log_swap_track_warring->getInfo( $s_num ) ;
		//寫入檢視人員與檢視日期
		$edit_type = $this->model_log_swap_track_warring->edit_log_swap_track_warring( $tmpRow[0] ) ;
		if($edit_type == 'Y'){
			//重查一次
			$tmpRow = $this->model_log_swap_track_warring->getInfo( $s_num ) ;
		}
		$data["dataInfo"] = $tmpRow[0] ;
		//查经销商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();
		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();
		$this->load->view( "inquiry/log_swap_track_warring_form", $data ) ;
	}

	/**
	 * insert/update db 
	 */
	public function modification_db() 
	{
		$s_num = $this->input->post("s_num");
		$this->model_log_swap_track_warring->updateData() ;
	}

}
/* End of file Pattern_file_field.php */
/* Location: ./application/controllers/pattern/Pattern_file_field.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 使用者帐号管理
 * User 使用者帐号管理
 */
class User extends MY_Controller {
	private $control_colnum = 4;  //权限操作栏位数目
    
	/**
	 * 建构式
	 * 预先载入User的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

        $this->load->model("nimda/model_user", "model_user") ;
		$this->load->model("nimda/model_group", "model_group") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/model_checkfunction", "model_checkfunction") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("user", $this->session->userdata('default_language'));
			$this->lang->load("menu", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("user", $this->session->userdata('display_language'));
			$this->lang->load("menu", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 使用者管理 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->model_user->getUserAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:sys_user的资料
		$data["userInfoRow"] = $this->model_user->getUserList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["userInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('帐号管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/user_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 选择商店作业 页面 动态生成
	 */
	public function select_merchant()
	{
		$this->load->view( "common/select_merchant" ) ;
	}
	
	/**
	 * 检查是否有重覆帐号
	 */
	public function checkuseraccount()
	{
		$result = $this->model_user->countUser();
		echo $result;
	}

	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//如果是有商店和前台登入使用者, 新增帐号为系统自动给予
		if($this->session->userdata('user_merchant') != "" && $this->session->userdata('login_side') == '0'){
			$data['autoUserID'] = $this->model_user->autoUserID();
		}

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('帐号管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/user_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("user_sn");
		if ( empty( $sn ) ) {
			$this->model_user->insertUser() ;
		} else {
			$this->model_user->updateUser() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$user_sn = $this->input->post("ckbSelArr");
		$user_sn = $user_sn[0];
		
		$tmpRow = $this->model_user->getUserInfo( $user_sn ) ;
		$data["userInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["userInfo"]);

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('帐号管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		//查营运商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/user_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->model_user->deleteUser();
	}
	
	/**
	 * 取得群组的选单资料
	 */
	public function getGroupMenu(){
		$group_sn = $this->input->post("group_sn") ;
		$user_sn = $this->input->post("user_sn") ;
		$org_group_sn = $this->input->post("org_group_sn") ;  //原本的群组
		if($group_sn != ''){
			$group_menu = $this->model_user->getGroupMenu($group_sn);
			if($user_sn != "" && $org_group_sn == $group_sn){  //必须要原本群组和选后群组相同, 才会抓取使用者自订选单功能
				$user_menu = $this->model_user->getUserMenu($user_sn);
			}else{
				$user_menu = array(); 
			}
			//栏位数
			$colnum = 1;
			$main_colnum = $colnum + 1 + $this->control_colnum-2;
			//建立群组拥有的选单和权限
			echo "<table width='100%' id='meunTable'>";
			foreach($group_menu as $key => $rsArr){
				if($rsArr["parent_menu_sn"] == "0"){  //父层选单
					$menu_sn = $rsArr["menu_sn"];
					
					//显示父层选单
					echo <<<html
					<tr>
						<td class="main" colspan="2">
							{$rsArr["menu_name"]}
						</td>
						<td id="control_label" colspan="{$main_colnum}">
							{$this->lang->line('menu_access_control')}
						</td>
					</tr>
html;
					$this->submenu($colnum, $menu_sn, $group_menu, $user_menu);
				}
			}
			echo '</table>';
		}
	}
	
	//子层选单与权限
	public function submenu($colnum, $menu_sn, $group_menu, $user_menu){
		$i = 0;
		//换行前栏位数
		$last = 0;
		foreach($group_menu as $key => $value){
			if($value['parent_menu_sn'] == $menu_sn){
				if($i % $colnum == 0){
					if($i != 0){
						//补td
						for($y = 1; $y <= $colnum-$last; $y++){
							echo '<td></td>';
						}
						echo '</tr>';
					}
					$last = 0;
					echo '<tr><td class="subfirst"></td>';
					
				}
				
				//显示子层选单, 隐藏起来, 用来带入选单编号
				echo <<<html
					<td class="td_sub">
						<input name="sub_menu[{$value["menu_sn"]}]" 
							type="checkbox" sub_sn="{$value["menu_sn"]}" class="sub" parent_sn={$menu_sn} value="1" checked />
						{$value["menu_name"]}
					</td>
html;
				//显示权限
				if(isset($value['access_control'])){  //修改
					//$this->sub_access_control($menu_sn, $value["menu_sn"], $value['access_control'], $value['mg_control']);
					$this->sub_access_control($user_menu, $menu_sn, $value["menu_sn"], $value['access_control']);
				}else{  //新增
					$this->sub_access_control($user_menu, $menu_sn, $value["menu_sn"], $value['access_control']);
				}
				$last++;
				$i++;
			}
		}
		
		if($i != 0){
			//补td
			for($y = 1; $y <= $colnum-$last; $y++){
				echo '<td></td>';
			}
			echo '</tr>';
		}
	}
	
	//子层, 操作权限
	//$menu_sn 父编号, $sub_menu_sn 子编号, $g_control 群组有的操作功能, $um_control 使用者拥有开启的功能
	public function sub_access_control($user_menu = array(), $menu_sn, $sub_menu_sn, $g_control ,$um_control = ""){
		$arr_access_control_list = $this->model_access->access_control_list();
		if($g_control != ""){
			$i = 1;
			//列出该群组所拥有的功能
			foreach($arr_access_control_list as $access_key =>$access_name){
				if($i % ($this->control_colnum+1) == 0){
					if($i / ($this->control_colnum+1) >= 2){ //第二次以后的tr
						echo '</tr>';
					}
					if(($g_control & $access_key) === $access_key){
						echo '<tr>
								<td colspan="2"></td>';
					}
				}
				//选单预设开启的功能
				if(($g_control & $access_key) === $access_key){
					if(count($user_menu) > 0){  //使用者有选单自订选单权限
						//取得使用者对应选单的功能
						$access_control = -1;
						foreach($user_menu as $user_key => $user_access){
							if($user_access['menu_sn'] == $sub_menu_sn){
								$access_control = $user_access['access_control'];
								break;
							}
						}
						
						//群组预设功能
						//代表使用者选单没有自订
						$checked = '';
						if($access_control === -1){  
							$checked = 'checked';
						}else{  //有自订
							if(($access_control & $access_key) === $access_key){
								$checked = 'checked';
							}
						}
					}else{  //无自订, 取群组权限, 群组有开都是勾选状态
						$checked = 'checked';
					}
					
					echo <<<html
						<td class='td_control'>
							<input name="sub_control[{$sub_menu_sn}][]" 
								type="checkbox" class="sub_control" parent_sn={$menu_sn} sub_parent_sn={$sub_menu_sn} value="{$access_key}" {$checked} />
							{$access_name}
						</td>
html;
					$i++;
				}
				
				
			}
		}
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "user");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	public function autocomplete_user(){
		$this->model_user->autocomplete_user();
	}	
		
		
	//确认帐号唯一
	public function ajax_pk_user_id(){
		$this->model_user->ajax_pk_user_id();
	}	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
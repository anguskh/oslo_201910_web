<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 基本代号资料
 * 
 */
class Basic_kind extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("nimda/Model_basic_kind", "Model_basic_kind") ;
		$this->load->model("nimda/Model_basic_code", "Model_basic_code") ;
		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("basic_kind", $this->session->userdata('default_language'));
			$this->lang->load("basic_code", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("basic_kind", $this->session->userdata('display_language'));
			$this->lang->load("basic_code", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_basic_kind->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_basic_kind->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/basic_kind_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	


	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/basic_kind_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("sn");
		if ($sn == '') {  //没流水为新增
			$this->Model_basic_kind->insertData() ;
		} else {  //有流水号为修改
			$this->Model_basic_kind->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$kind_no = $this->input->post("ckbSelArr");
		$kind_no = $kind_no[0];
		
		$tmpRow = $this->Model_basic_kind->getInfo($kind_no) ;
		$data["dataInfo"] = $tmpRow[0] ;
		//取得类别明细
		$basic_code_list = $this->Model_basic_code->getBasic_code_list($kind_no);
		if(count($basic_code_list) > 0){
			$data["basic_code_list"] = $basic_code_list;
		}
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["dataInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/basic_kind_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_basic_kind->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "basic_kind");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	//确认类别代号唯一
	public function ajax_pk_kind_no(){
		$this->Model_basic_kind->ajax_pk_kind_no();
	}
	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
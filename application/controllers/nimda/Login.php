<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * 建构式
	 * 预先载入Login的物件
	 */
    function __construct() 
    {
        parent::__construct();
		
        $this->load->model("common/model_background", "model_background") ;
        $this->load->model("common/model_access", "model_access") ;
		$this->load->model("nimda/model_config", "model_config") ;

		$this->load->library('session');
		$this->load->library('user_agent');
		$this->load->helper('url');
		//取得预设语系
		$language = "";
		//$language = $this->config->item('language'); 此为读取php设定
		$language = $this->displayLanguage();  //此为读取资料库设定
		
		if($language != ""){
			$this->lang->load("login", $language);
		}else{
			$this->lang->load("login", "zh_tw");
		}

		// 跨领域存取
        // header("Access-Control-Allow-Origin:*");
    }
	
	public function index()
	{
		$this->session->sess_destroy();
		
		//汇入基本参数
		$configRows = $this->model_config->getConfigInfoAssign();
		foreach ($configRows as $key => $configArr) {
			$this->session->set_userdata($configArr["config_code"], $configArr["config_set"]);
		}
		
		if($this->session->userdata('foreground_pc_name') == $_SERVER['HTTP_HOST']){	//前台
			//redirect('/login', 'refresh');
			$this->load->view('login_nimda');
		}else if($this->session->userdata('background_pc_name') == $_SERVER['HTTP_HOST']){  //后台
			$this->load->view('login_nimda');
		}else{	//无符合, 跳原来网页
			$this->load->view('login_nimda');
			//$this->load->view('index.html');
		}
	}
	
	public function displayLanguage(){
		$this->load->model("common/model_background", "model_background") ;
		return $this->model_background->displayLanguage();
	}

	public function login_check()
	{
		$this->session->set_userdata('function_name', $this->router->fetch_class());
		//汇入基本参数
		$configRows = $this->model_config->getConfigInfoAssign();
		foreach ($configRows as $key => $configArr) {
			$this->session->set_userdata($configArr["config_code"], $configArr["config_set"]);
		}
		
		//检查ip是否为设定网段内
		if($this->check_allow_ip()){

		}else{
			$arrayToJs[0] = '8';		//状态码
			$arrayToJs[1] = 0;	//尝试次数
			echo json_encode($arrayToJs);
			exit();
		}
		
		//检查帐密
	    $loginUser = addslashes($this->input->post("loginUser"));
	    $loginPass = MD5(addslashes($this->input->post("loginPass")));
	    $userRow = $this->model_background->check_user_account( $loginUser, $loginPass );
		$arrayToJs = array();
		//检查来源网址是否正确
		$strReferrer = $_SERVER["HTTP_REFERER"];
		$strReferrer = str_replace ("#","",$strReferrer);
		$web = $this->config->item('base_url');
		if(!empty($userRow) && 
			( $strReferrer == $web.'mobile/mobile/' || 
		      $strReferrer == $web.'mobile/mobile' || 
			  $strReferrer == $web.'nimda/login/' ||
			  $strReferrer == $web.'nimda/login' ||
			  $strReferrer == $web.'nimda/' ||
			  $strReferrer == $web.'nimda')
			) {
			if($userRow[0]["status"] == '1'){
				//先檢查是否為營運商，營運商s_num不能為空值
				if($userRow[0]["to_flag"] == '1'){
					if($userRow[0]["to_num"]  == '0'){
						//不可登入成功
						//新增纪录档
						$this->model_background->log_operating_insert('0', 
							'<font color=red>['.$this->lang->line('error_account_cant_login_again').']</font>'.$loginUser);
									
						$retry = $this->check_retry($loginUser);
						$arrayToJs[0] = '10';		//状态码
						$arrayToJs[1] = $retry;	//尝试次数
						echo json_encode($arrayToJs);
						return false;
					}
				}

				//先取得登入前后台资讯
				$this->session->set_userdata("login_side", $userRow[0]["login_side"]);
				foreach ($userRow[0] as $key => $value) {
					$this->session->set_userdata($key, $value);
				}
				
				//改为全部都记录,并确认资料
				$session_sn = $this->model_background->check_logindata($userRow[0]['user_sn']);
				// echo '$session_sn:'.$this->session->userdata('session_sn');
				if(	$session_sn != 0 && 
					$this->session->userdata("login_retry_cnt") < $this->session->userdata("password_retry_maxcount") ){	//有正常运作, 有帐号, 必须小于最大尝试次数

					//记录登入方式
					$this->session->set_userdata("login_way","nimda");
					
					//取目前使用的资料库名称
					$this->session->set_userdata("DBname", $this->model_background->getDefDBname());
					
					//新增纪录档
					$this->session->set_userdata("desc", "");
					$this->model_background->log_operating_insert('0');
					
					//重置尝试登入次数
					$this->model_background->reset_retry($loginUser);
					
					//输出群组可登入的前后台
					//echo $this->session->userdata('login_side');
					
					//取得已定义好的权限
					$access_control_list = $this->model_access->access_control_list();
					$this->session->set_userdata('access_control_list', $access_control_list);
					
					//取得使用者操作权限
					$get_access = $this->model_background->get_user_access($userRow[0]['user_sn']);
					if($get_access !== false){
						$arr_access = array();
						foreach ($get_access as $access_key => $access_value) {
							$arr_access[$access_value['menu_directory']] = $access_value['access_control'];
						}
						$this->session->set_userdata('user_access', $arr_access);
					}
					//更新 上次登入日期login_before/上次登入-IPlogin_b_ip	/本次登入日期	login_now/本次登入-IP	login_b_ip
					$this->model_background->insert_user_log($userRow[0]["user_sn"], $userRow[0]["login_now"],$userRow[0]["login_b_ip"]);

					//输出群组
					$arrayToJs[0] = $this->session->userdata('login_side');		//状态码, 登入前后台权限, 0前, 1后
					$arrayToJs[1] = 0;	//尝试次数
					echo json_encode($arrayToJs);
				}else{	//异常
					//新增纪录档
					$this->model_background->log_operating_insert('0', 
						'<font color=red>['.$this->lang->line('error_account_cant_login_again').']</font>'.$loginUser);
								
					$retry = $this->check_retry($loginUser);
					$arrayToJs[0] = '2';		//状态码
					$arrayToJs[1] = $retry;	//尝试次数
					echo json_encode($arrayToJs);
				}	
			}else{	//被停用
				//新增纪录档
				$this->model_background->log_operating_insert('0', 
					'<font color=red>['.$this->lang->line('error_account_disabled').']</font>'.$loginUser);
						
				$retry = $this->check_retry($loginUser);
				$arrayToJs[0] = '3';		//状态码
				$arrayToJs[1] = $retry;	//尝试次数
				echo json_encode($arrayToJs);
			}
		}else {  //无资料
			//新增纪录档
			$this->model_background->log_operating_insert('0', 
				'<font color=red>['.$this->lang->line('error_account').']</font>'.$loginUser);
					
			$retry = $this->check_retry($loginUser);
			$arrayToJs[0] = '2';		//状态码
			$arrayToJs[1] = $retry;	//尝试次数
			echo json_encode($arrayToJs);
		}
	}
	
	//检查帐号尝试次数, 回传剩余次数
	public function check_retry($loginUser){
		$retry = $this->model_background->sum_retry_error($loginUser);
		if($retry != "" && $retry != null && $retry != -999){	//有回传次数
			$retry =  (int)$this->session->userdata('password_retry_maxcount') - $retry;
			if($retry <= 0){
				$retry = 0;
			}
		}
		return $retry;
	}
	
	//检查ip是否为设定网段内
	public function check_allow_ip(){
		//允许网段
		$get_allow_ip = $this->session->userdata('allow_ip_segment');
		if($get_allow_ip == ""){  //无设定,预设为任何ip 都可以通过
			return true;
		}else{
			$get_allow_ip = explode(",", $get_allow_ip);
			$get_allow_ip[] = "::1";
			//客户端网段
			$get_client_ip = $this->model_background->get_client_ip();
			if($get_client_ip == ""){
				$get_client_ip = $this->input->ip_address();
			}
			if($get_client_ip == "::1"){
				$get_client_ip = "127.0.0.1";
			}
			$get_client_ip = explode(".", $get_client_ip);
			//$get_client_ip = $get_client_ip[0].'.'.$get_client_ip[1];
			
			foreach($get_allow_ip as $value){
				$arr_ip = explode(".", $value);
				$i = 0;
				$allow = false;
				foreach($arr_ip as $value2){
					if($value2 == '*'){
						// echo '*';
						// echo '<br />';
						$allow = true;
					}else if($value2 == $get_client_ip[$i]){
						// echo $value2.':'.$get_client_ip[$i];
						// echo '<br />';
						$allow = true;
					}else{
						// echo $value2.':'.$get_client_ip[$i].':'.'false';
						// echo '<br />';
						$allow = false;
					}
					$i++;
					if($allow == false){  //不符合就跳出
						break ;
					}
				}
				
				if($allow == true){	//全符合就跳出
					break;
				}
			}
			//echo '回传:'.$allow;
			return $allow;
		}	
	}
	
	public function forgotemail()
	{
		$this->load->model("common/model_background", "model_background") ;
		$email = $this->input->post('recoverEmail');
		if(trim($email)=="")
		{
			echo "0";
		}
		else
		{
			$checkmail = $this->model_background->checkmail($email);
			if($checkmail=='0')
			{
				echo "0";
			}
			else if($checkmail!="")
			{
				$sendmail = $this->model_background->sendemail($email,"忘记密码通知函","",$checkmail,"phoebus");
				echo $sendmail;
			}
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
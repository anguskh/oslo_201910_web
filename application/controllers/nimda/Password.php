<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 使用者帐号管理
 * Password 使用者帐号管理
 */
class Password extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入Password的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("nimda/model_user", "model_user") ;
	}
	
	/**
	 * 使用者管理 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('更改密码');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/password_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 变更密码作业 页面
	 */
	public function change_password () 
	{
		
		$data["mode"] = $this->input->post("mode");
		$data["change_type"] = $this->input->post("change_type");
		$this->load->view( "nimda/password_form", $data ) ;
	}
	
	//查询旧密码是否输入正确
	public function check_current_password(){
		$this->model_user->check_current_password() ;
	}

	/**
	 * insert/update db 
	 */
	public function change_password_db () 
	{
		$this->model_user->updateUserPassword() ;
	}
	
	//检核是否有使用到近四笔旧密码
	public function check_old_password(){
		$this->model_user->check_old_password();
	}
		
}
/* End of file password.php */
/* Location: ./application/controllers/nimda/password.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 选单	管理
 * Menu 选单管理
 */
class Menu extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入Menu的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

        $this->load->model("nimda/model_menu", "model_menu") ;
		$this->load->model("common/model_checkfunction", "model_checkfunction") ;
		$this->load->model("common/model_access", "model_access") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("menu", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("menu", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 选单管理 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->model_menu->getMenuAllCnt(true) ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow2"] = $this->model_menu->getMenuList( $this->session->userdata('paging_rows'), $startRow, true, true, '2', true ) ;
		// print_r( $data["menuInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('选单管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();

		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/menu_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 取得table:tbl_menu的资料
		$data["menuSelectListRow"] = $this->model_menu->getParentMenuList() ;
		$data["menuSelectSecondListRow"] = $this->model_menu->getSecondMenuList() ;
		//权限清单
		$data["arr_access_control_list"] = $this->model_access->access_control_list();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('选单管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/menu_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$postMenuSn = $this->input->post("menu_sn");
		if ( empty( $postMenuSn ) ) {
			// echo "insertMenu" ;
			$this->model_menu->insertMenu() ;
		} else {
			// echo "updateMenu" ;
			$this->model_menu->updateMenu() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	

	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$menu_sn = $this->input->post("ckbSelArr");
		$menu_sn = $menu_sn[0];
		
		$tmpRow = $this->model_menu->getMenuInfo( $menu_sn ) ;
		$data["menuInfo"] = $tmpRow[0] ;

		// 取得table:tbl_menu的资料
		$data["menuSelectListRow"] = $this->model_menu->getParentMenuList( $tmpRow[0]["menu_sn"] ) ;
		$data["menuSelectSecondListRow"] = $this->model_menu->getSecondMenuList($tmpRow[0]["menu_sn"]) ;
		//权限清单
		$data["arr_access_control_list"] = $this->model_access->access_control_list();
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["menuInfo"]);

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('选单管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/menu_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	//新增我的最爱
	public function addfavor(){
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$this->model_shortcut->addfavor();
		//我的最爱, 中间图片显示
		$this->model_shortcut->favor_pic();
	}
	
	//我的最爱, 中间图片显示
	public function favor_pic(){
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$this->model_shortcut->favor_pic();
	}
	
	//显示捷径列
	public function showshortcut(){
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$this->model_shortcut->shortcut_pic();
	}
	
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->model_menu->deleteMenu();
	}
	
	//删除选单图档作业
	public function deletemenuimg()
	{
		$this->model_menu->deletemenuimg();
	}
	
	/**
	 * 上移作业
	 */
	public function sequence_up_db () 
	{
		$this->model_menu->sequenceMenuUp();
	}
	
	/**
	 * 下移作业
	 */
	public function sequence_down_db () 
	{
		$this->model_menu->sequenceMenuDown();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "menu");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}

	public function ChangeCardList(){
		$this->model_menu->ChangeCardList();
	}
	
}
/* End of file menu.php */
/* Location: ./application/controllers/nimda/menu.php */
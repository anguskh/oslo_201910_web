<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Export extends MY_Controller {

    
	/**
	 * 建构式
	 * 预先载入User的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->model("nimda/model_export", "model_export");
	}
	
	public function export()
	{
		$this->model_export->export();
	}

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 使用者群组管理
 * Group 群组管理
 */
class Group extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入Group的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

        $this->load->model("nimda/model_group", "model_group") ;
		$this->load->model("nimda/model_menu_group", "model_menu_group") ;
		$this->load->model("common/model_checkfunction", "model_checkfunction") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("group", $this->session->userdata('default_language'));
			$this->lang->load("menu", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("group", $this->session->userdata('display_language'));
			$this->lang->load("menu", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 群组管理 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->model_group->getGroupAllCnt() ;


		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:sys_group的资料
		$data["groupInfoRow"] = $this->model_group->getGroupList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["groupInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/group_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 取得table:sys_group的资料
		$data["groupInfoRow"] = $this->model_group->getGroupSelectList() ;

		$data["MenuGroupInfo"] = $this->model_menu_group->getMenuGroupInfo() ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/group_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$postGroupSn = $this->input->post("group_sn");
		if ( empty( $postGroupSn ) ) {
			// echo "insertGroup" ;
			$this->model_group->insertGroup() ;
		} else {
			// echo "updateGroup" ;
			$this->model_group->updateGroup() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$group_sn = $this->input->post("ckbSelArr");
		$group_sn = $group_sn[0];
		
		$tmpRow = $this->model_group->getGroupInfo( $group_sn ) ;
		$data["groupInfo"] = $tmpRow[0] ;
		// 取得table:sys_group的资料
		$data["groupInfoRow"] = $this->model_group->getGroupSelectList2() ;
		
		$group_sn = $this->input->post("group_sn") ;
		$data["MenuGroupInfo"] = $this->model_menu_group->getMenuGroupInfo( $group_sn ) ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["groupInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/group_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->model_group->deleteGroup();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "group");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	//查询group, 是否有在使用者之中使用
	public function checkUserGroupCount(){
		$this->model_group->checkUserGroupCount();
	}
	
	public function getLogin_menu(){
		$this->model_menu_group->getLogin_menu();
	}
	
}
/* End of file group.php */
/* Location: ./application/controllers/nimda/group.php */
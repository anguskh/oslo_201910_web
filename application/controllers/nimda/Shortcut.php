<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 捷径管理
 * Shortcut 捷径管理
 */
class Shortcut extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入Shortcut的物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

        $this->load->model("nimda/model_shortcut", "model_shortcut") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("shortcut", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("shortcut", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 * 捷径管理 首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->model_shortcut->getShortcutAllCnt() ;


		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:sys_shortcut的资料
		$data["shortcutInfoRow"] = $this->model_shortcut->getShortcutList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["shortcutInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "nimda/shortcut_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		// 取得table:tbl_menu的资料
		$data["menuSelectListRow"] = $this->model_menu->getShoutcutMenuList() ;
		$data["StartRow"] = $this->input->post("StartRow");

		$this->load->view( "nimda/shortcut_form", $data ) ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$postShortcutSn = $this->input->post("shortcut_sn");
		if ( empty( $postShortcutSn ) ) {
			// echo "insertShortcut" ;
			$this->model_shortcut->insertShortcut() ;
		} else {
			// echo "updateShortcut" ;
			$this->model_shortcut->updateShortcut() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification()
	{
		$shortcut_sn = $this->input->post("shortcut_sn") ;
		$tmpRow = $this->model_shortcut->getShortcutInfo( $shortcut_sn ) ;
		$data["shortcutInfo"] = $tmpRow[0] ;

		// 取得table:tbl_menu的资料
		$data["menuSelectListRow"] = $this->model_menu->getShoutcutMenuList( $tmpRow[0]["menu_sn"] ) ;
		$data["StartRow"] = $this->input->post("StartRow");

		$this->load->view( "nimda/shortcut_form", $data ) ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->model_shortcut->deleteShortcut();
	}
	
	/**
	 * 上移作业
	 */
	public function sequence_up_db () 
	{
		$this->model_shortcut->sequenceShortcutUp();
	}
	
	/**
	 * 下移作业
	 */
	public function sequence_down_db () 
	{
		$this->model_shortcut->sequenceShortcutDown();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "shortcut");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
}
/* End of file shortcut.php */
/* Location: ./application/controllers/nimda/shortcut.php */
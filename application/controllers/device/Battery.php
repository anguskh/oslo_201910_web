<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Battery extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("device/Model_battery", "Model_battery") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->model("inquiry/Model_log_battery_leave_return", "Model_log_battery_leave_return") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;

		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("battery", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("battery", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_battery->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_battery->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/battery_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	


	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//取得经销商下拉选单资料
		$data['dealerList'] = $this->Model_show_list->getdealerList();
		//取得营运商下拉选单资料
		$data['operatorList'] = $this->Model_show_list->getoperatorList();
		//取得车辆下拉选单资料
		$data['vehicleList'] = $this->Model_show_list->getvehicleList();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/battery_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("s_num");
		if ($sn == '') {  //没流水为新增
			$this->Model_battery->insertData() ;
		} else {  //有流水号为修改
			$this->Model_battery->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$kind_no = $this->input->post("ckbSelArr");
		$kind_no = $kind_no[0];
		
		$tmpRow = $this->Model_battery->getInfo($kind_no) ;
		$data["dataInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["dataInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//取得经销商下拉选单资料
		$data['dealerList'] = $this->Model_show_list->getdealerList();
		//取得营运商下拉选单资料
		$data['operatorList'] = $this->Model_show_list->getoperatorList();
		//取得车辆下拉选单资料
		$data['vehicleList'] = $this->Model_show_list->getvehicleList();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/battery_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_battery->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "battery");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
	//确认类别代号唯一
	public function ajax_pk_kind_no(){
		$this->Model_battery->ajax_pk_kind_no();
	}

	//租借记录
	public function detail( $battery_sn = '', $startRow = 0, $main_startRow = 0){
		$startRow = $startRow < 0 ? 0 : $startRow;
		$battery_main = $this->Model_battery->getInfo($battery_sn) ;
		$data["battery_id"] = $battery_main[0]['battery_id'] ;

		$totalRow = $this->Model_battery->getDetailAllCnt($data["battery_id"]) ;
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_battery->getDetailList( $this->session->userdata('paging_rows'), $startRow, $data["battery_id"] ) ;

		$data["battery_sn"] = $battery_sn;
		$data["main_startRow"] = $main_startRow;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow, $battery_sn );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/battery_detail_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 搜寻作业　
	 */
	public function detail_search () 
	{
		$this->session->set_userdata('searchType', "log_battery_leave_return");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData_detail', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt_detail', $search_txt);

		$battery_sn = $this->input->post("battery_sn");
		$main_startRow = $this->input->post("main_startRow");

		$get_full_url_random = get_full_url_random().'/detail/'.$battery_sn.'/'.$main_startRow;
		redirect($get_full_url_random, 'refresh');
	}
	
	//上傳
	public function check_upload(){
		date_default_timezone_set("Asia/Taipei");
		//將檔案移到temp
		move_uploaded_file($_FILES['upload_file']['tmp_name'],'./excel/jxlrwtest.xls');
		
		$file_name = './excel/jxlrwtest.xls';
		include 'phpExcel.1.7.6/Classes/PHPExcel.php';
		include 'phpExcel.1.7.6/Classes/PHPExcel/IOFactory.php';
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Writer/Excel2007.php");
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Reader/Excel5.php");
		// 取得資料檔的型式
		$fileType = PHPExcel_IOFactory::identify($file_name);
		// 產生 Reader 物件
		$reader = PHPExcel_IOFactory::createReader($fileType);
		$PHPExcel = $reader->load($file_name); // 檔案名稱
		$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
		$highestRow = $sheet->getHighestRow(); // 取得總列數
		//取得最大欄數
		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
		
		//厂商类型(D/O) DorO_flag 经销商D sd_num 营运商O so_num 车辆sv_num 电池序号battery_id 出厂日期manufacture_date 电池位置(B/V) position 电池状态(0~4) status

		$dataArr = array();
		$tmp_battery = array();
		$dataname = array('DorO_flag','sd_num','so_num','sv_num','battery_id','manufacture_date','position','status','sys_no');
		//getFiledsum($table, $field, $field_name, $field_val)
		for($row = 2; $row <= $highestRow; $row++) {
			$dataArr[$row]['cart_flag'] = 'N';
			for($column = 0; $column < 9; $column++)
			{
				$value = $sheet->getCellByColumnAndRow($column, $row)->getValue();
				$dataArr[$row][$dataname[$column]]= $value;

				switch($dataname[$column]){
					case 'sd_num': //经销商
						if($value != ''){
							$sd_count = $this->Model_show_list->getFiledsum('tbl_dealer', 's_num', 'tde01', $value);
							if(count($sd_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'so_num'://营运商tbl_operator
						if($value != ''){
							$so_count = $this->Model_show_list->getFiledsum('tbl_operator', 's_num', 'top01', $value);
							if(count($so_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'sv_num'://车辆
						if($value != ''){
							$sv_count = $this->Model_show_list->getFiledsum('tbl_vehicle', 's_num', 'unit_id', $value);
							if(count($sv_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'battery_id'://电池序号
						if($value != ''){
							$flag = $this->Model_battery->countbattery_id($value, '');
							if($flag){
								$dataArr[$row]['cart_flag'] = 'Y';
							}else{
								$dataArr[$row][$dataname[$column]] = '<font style="color:red">'.$value.'【电池序号重覆】</font>';
							}
							
							//判斷同一excel序號是否重覆
							if(!isset($tmp_battery[$value])){
								$tmp_battery[$value] = $value;
							}else{
								$dataArr[$row]['cart_flag'] = 'N';
								$dataArr[$row][$dataname[$column]] = '<font style="color:red">'.$value.'【excel电池序号重覆】</font>';
							}
						}
					break;
				}
			}

		}
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('电池管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		$data["InfoRow"] = $dataArr;

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/battery_upload_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	public function upload(){
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$num_temp = array();
		for($i=0; $i<count($ckbSelArr); $i++){
			$num_temp[$ckbSelArr[$i]] = $ckbSelArr[$i];
		}

		date_default_timezone_set("Asia/Taipei");
		$file_name = './excel/jxlrwtest.xls';
		include 'phpExcel.1.7.6/Classes/PHPExcel.php';
		include 'phpExcel.1.7.6/Classes/PHPExcel/IOFactory.php';
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Writer/Excel2007.php");
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Reader/Excel5.php");

		// 取得資料檔的型式
		$fileType = PHPExcel_IOFactory::identify($file_name);
		// 產生 Reader 物件
		$reader = PHPExcel_IOFactory::createReader($fileType);
		$PHPExcel = $reader->load($file_name); // 檔案名稱
		$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
		$highestRow = $sheet->getHighestRow(); // 取得總列數
		//取得最大欄數
		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
		
		//厂商类型(D/O) DorO_flag 经销商D sd_num 营运商O so_num 车辆sv_num 电池序号battery_id 出厂日期manufacture_date 电池位置(B/V) position 电池状态(0~4) status

		$dataArr = array();
		$dataname = array('DorO_flag','sd_num','so_num','sv_num','battery_id','manufacture_date','position','status','sys_no');
		//getFiledsum($table, $field, $field_name, $field_val)
		$num=1;

		$battery_temp = array(
			'DorO_flag' => '',
			'sd_num' => '',
			'so_num' => '',
			'sv_num' => '',
			'battery_id' => '',
			'manufacture_date' => '',
			'position' => '',
			'status' => '0'
		);
		$error = '';
		for($row = 2; $row <= $highestRow; $row++) {
			//每寫入一行就重新初始化
			$battery_array = $battery_temp;
			if(in_array($num, $num_temp)){
				for($column = 0; $column < 9; $column++)
				{
					$value = $sheet->getCellByColumnAndRow($column, $row)->getValue();
					$battery_array[$dataname[$column]] = $value;

					switch($dataname[$column]){
						case 'sd_num': //经销商
							if($value != ''){
								$sd_count = $this->Model_show_list->getFiledsum('tbl_dealer', 's_num', 'tde01', $value);
								$battery_array[$dataname[$column]] = '';
								if(count($sd_count) > 0){
									$battery_array[$dataname[$column]] = $sd_count[0]['s_num'];
								}
							}
						break;
						case 'so_num'://营运商tbl_operator
							if($value != ''){
								$so_count = $this->Model_show_list->getFiledsum('tbl_operator', 's_num', 'top01', $value);
								$battery_array[$dataname[$column]] = '';
								if(count($so_count) > 0){
									$battery_array[$dataname[$column]] = $so_count[0]['s_num'];
								}
							}
						break;
						case 'sv_num'://车辆
							if($value != ''){
								$sv_count = $this->Model_show_list->getFiledsum('tbl_vehicle', 's_num', 'unit_id', $value);
								$battery_array[$dataname[$column]] = '';
								if(count($sv_count) > 0){
									$battery_array[$dataname[$column]] = $sv_count[0]['s_num'];
								}
							}
						break;
					}
				}

				$flag = $this->Model_battery->upload_insertData($battery_array);
				if($flag == 'N'){
					$error .= '序號【'.$row.'】匯入失敗<br />';
				}
			}
			$num++;

		}

		if($error == ''){
			header("Content-Type:text/html; charset=utf-8");
			echo"<script> setTimeout(function () {alert(\"匯入成功\"); location.href = './';}, 3000);</script>";
		}else{
			header("Content-Type:text/html; charset=utf-8");
			echo $error;
			echo"<script> setTimeout(function () {alert(\"匯入失敗\");location.href = './';}, 3000);</script>";
		}
	}

	public function ajax_pk_battery_id(){
		$this->Model_battery->ajax_pk_battery_id();
	}

	public function detail_map( $s_num = ''){
		$data["s_num"] = $s_num;
		if($s_num == 'all'){
			$tmpRow = $this->Model_battery->get_batteryALL() ;
			$data["error"] = '';
			$data["latitude"] = $tmpRow[0]['latitude'];
			$data["longitude"] = $tmpRow[0]['longitude'];
		}else{
			$tmpRow = $this->Model_battery->getInfo($s_num) ;
			$data["error"] = '';
			$data["latitude"] = $tmpRow[0]['latitude'];
			$data["longitude"] = $tmpRow[0]['longitude'];
			
			if($data["latitude"] == '' || $data["longitude"] == ''){
				$data["error"] = '查無此坐標';
			}
			$data["battery_id"] = $tmpRow[0]['battery_id'];
			$data["last_five_report_data"] = $tmpRow[0]['last_five_report_data'];
			$data["last_report_datetime"] = $tmpRow[0]['last_report_datetime'];
		}
		$data["stationInfo"] = $tmpRow;
		$this->load->view( "device/battery_map", $data ) ;
	}

	//路線軌跡圖
	public function detail_map_trail($s_num = '')
	{
		$data["s_num"] = $s_num;
		$tmpRow = $this->Model_log_battery_leave_return->getlogInfo($s_num) ;
		
		$data["logInfo"] = $tmpRow;
		$this->load->view( "member/battery_map_trail", $data ) ;
	}

	public function batterytowechat()
	{
		$this->Model_battery->batterytowechat();
	}
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
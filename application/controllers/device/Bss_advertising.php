<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Bss_advertising extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("device/Model_bss_advertising", "Model_bss_advertising") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("bss_advertising", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("bss_advertising", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;

		if($startRow!="")
			$this->session->set_userdata("bss_advertising_row",$startRow);

		$totalRow = $this->Model_bss_advertising->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_bss_advertising->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('BSS广告管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/bss_advertising_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	


	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		$data['select_batteryswapstation'] = "";

		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('BSS广告管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/bss_advertising_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("s_num");
		if ($sn == '') {  //没流水为新增
			$this->Model_bss_advertising->insertData() ;
		} else {  //有流水号为修改
			$this->Model_bss_advertising->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$kind_no = $this->input->post("ckbSelArr");
		$kind_no = $kind_no[0];
		
		$tmpRow = $this->Model_bss_advertising->getInfo($kind_no) ;
		$data["dataInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["dataInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		$data['select_batteryswapstation'] = $this->Model_bss_advertising->getbss($kind_no);
		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();


		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('BSS广告管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/bss_advertising_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	public function deletefile()
	{
		$this->Model_bss_advertising->deletefile();
	}

	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_bss_advertising->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$fn = get_fetch_class_random();

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	

	//檢查檔名
	public function check_file_name(){
		$s_num = $this->input->post("s_num");
		$filename = $this->input->post("filename");
		$file_type = $this->input->post("file_type");
		$rs = $this->Model_bss_advertising->check_file_name($s_num, $filename, $file_type);
		echo $rs;
	}

}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
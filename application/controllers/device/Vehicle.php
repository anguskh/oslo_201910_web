<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 车辆资料
 * 
 */
class Vehicle extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("device/Model_vehicle", "Model_vehicle") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("vehicle", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("vehicle", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_vehicle->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_vehicle->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('车辆管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/vehicle_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 选择子经销商
	 */
	public function select_sub_dealer()
	{
		$td_num = $this->input->post('td_num');

		$data['SubDealerListInfo'] = $this->Model_show_list->getsubdealerList($td_num);
		$data['tsd_num']= $this->input->post('tsd_num');
		$this->load->view( "common/select_sub_dealer",$data) ;
	}

	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//取得经销商下拉选单资料
		$data['dealerList'] = $this->Model_show_list->getdealerList();
		//取得营运商下拉选单资料
		$data['operatorList'] = $this->Model_show_list->getoperatorList();
		//查子经销商
		$data["sub_dealer_list"] = $this->Model_show_list->getsubdealerList();
		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('车辆管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/vehicle_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("s_num");
		if ($sn == '') {  //没流水为新增
			$this->Model_vehicle->insertData() ;
		} else {  //有流水号为修改
			$this->Model_vehicle->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$kind_no = $this->input->post("ckbSelArr");
		$kind_no = $kind_no[0];
		
		$tmpRow = $this->Model_vehicle->getInfo($kind_no) ;
		$data["dataInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["dataInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//取得经销商下拉选单资料
		$data['dealerList'] = $this->Model_show_list->getdealerList();
		//取得营运商下拉选单资料
		$data['operatorList'] = $this->Model_show_list->getoperatorList();
		//查子经销商
		$data["sub_dealer_list"] = $this->Model_show_list->getsubdealerList();
		//查子营运商
		$data["sub_operator_list"] = $this->Model_show_list->getsuboperatorList();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('车辆管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/vehicle_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_vehicle->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "vehicle");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}

	public function detail_map( $vehicle_sn = ''){
		$tmpRow = $this->Model_vehicle->getInfo($vehicle_sn) ;
		$latitude = $tmpRow[0]['latitude'];
		$longitude = $tmpRow[0]['longitude'];
		$unit_id = $tmpRow[0]['unit_id'];

		$data["error"] = '';
		$data["latitude"] = '';
		$data["longitude"] = '';
		if($latitude != '' && $longitude != ''){
			$data["latitude"] = $latitude;
			$data["longitude"] = $longitude;
		}else{
			$data["error"] = '查无经纬度';
		}

		$data["unit_id"] = $unit_id;
		$this->load->view( "device/vehicle_map", $data ) ;
	}


	public function ajax_pk_car_machine_id(){
		$this->Model_vehicle->ajax_pk_car_machine_id();
	}
	//上傳
	public function check_upload(){
		date_default_timezone_set("Asia/Taipei");
		//將檔案移到temp
		move_uploaded_file($_FILES['upload_file']['tmp_name'],'./excel/jxlrwtest.xls');
		
		$file_name = './excel/jxlrwtest.xls';
		include 'phpExcel.1.7.6/Classes/PHPExcel.php';
		include 'phpExcel.1.7.6/Classes/PHPExcel/IOFactory.php';
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Writer/Excel2007.php");
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Reader/Excel5.php");
		// 取得資料檔的型式
		$fileType = PHPExcel_IOFactory::identify($file_name);
		// 產生 Reader 物件
		$reader = PHPExcel_IOFactory::createReader($fileType);
		$PHPExcel = $reader->load($file_name); // 檔案名稱
		$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
		$highestRow = $sheet->getHighestRow(); // 取得總列數
		//取得最大欄數
		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
		
		$dataArr = array();
		$dataname = array('status','to_num','tso_num','td_num','tsd_num','unit_id','vehicle_code','manufacture_date','license_plate_number','engine_number','car_machine_id','vehicle_model','vehicle_color','4g_ip','bluetooth_name','bluetooth_mac','lora_sn','vehicle_user_id','vehicle_type','use_type','vehicle_charge','charge_count','latitude','longitude');
		//getFiledsum($table, $field, $field_name, $field_val)
		for($row = 2; $row <= $highestRow; $row++) {
			$dataArr[$row]['cart_flag'] = 'N';
			for($column = 0; $column < $highestColumn; $column++)
			{
				$value = $sheet->getCellByColumnAndRow($column, $row)->getValue();
				$dataArr[$row][$dataname[$column]]= $value;
				switch($dataname[$column]){
					case 'td_num': //经销商
						if($value != ''){
							$sd_count = $this->Model_show_list->getFiledsum('tbl_dealer', 's_num', 'tde01', $value);
							if(count($sd_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'tsd_num': //子经销商
						if($value != ''){
							$tsd_count = $this->Model_show_list->getFiledsum('tbl_sub_dealer', 's_num', 'tsde01', $value);
							if(count($tsd_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'to_num'://营运商tbl_operator
						if($value != ''){
							$so_count = $this->Model_show_list->getFiledsum('tbl_operator', 's_num', 'top01', $value);
							if(count($so_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'tso_num'://子营运商tbl_operator
						if($value != ''){
							$tso_count = $this->Model_show_list->getFiledsum('tbl_sub_operator', 's_num', 'tsop01', $value);
							if(count($tso_count)==0){
								$dataArr[$row][$dataname[$column]]= '<font style="color:red">'.$value.'</font>';
							}
						}
					break;
					case 'car_machine_id':	//車機編號
						if($value != ''){
							$flag = $this->Model_vehicle->countcar_machine($value, '');
							if($flag){
								$dataArr[$row]['cart_flag'] = 'Y';
							}else{
								$dataArr[$row][$dataname[$column]] = '<font style="color:red">'.$value.'【车机编号重覆】</font>';
							}
						}
					break;

				}
			}

		}
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('车辆管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		$data["InfoRow"] = $dataArr;

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "device/vehicle_upload_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	public function upload(){
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$num_temp = array();
		for($i=0; $i<count($ckbSelArr); $i++){
			$num_temp[$ckbSelArr[$i]] = $ckbSelArr[$i];
		}

		date_default_timezone_set("Asia/Taipei");
		$file_name = './excel/jxlrwtest.xls';
		include 'phpExcel.1.7.6/Classes/PHPExcel.php';
		include 'phpExcel.1.7.6/Classes/PHPExcel/IOFactory.php';
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Writer/Excel2007.php");
		require_once("phpExcel.1.7.6/Classes/PHPExcel/Reader/Excel5.php");

		// 取得資料檔的型式
		$fileType = PHPExcel_IOFactory::identify($file_name);
		// 產生 Reader 物件
		$reader = PHPExcel_IOFactory::createReader($fileType);
		$PHPExcel = $reader->load($file_name); // 檔案名稱
		$sheet = $PHPExcel->getSheet(0); // 讀取第一個工作表(編號從 0 開始)
		$highestRow = $sheet->getHighestRow(); // 取得總列數
		//取得最大欄數
		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
		
		$dataArr = array();
		$dataname = array('status','to_num','tso_num','td_num','tsd_num','unit_id','vehicle_code','manufacture_date','license_plate_number','engine_number','car_machine_id','vehicle_model','vehicle_color','4g_ip','bluetooth_name','bluetooth_mac','lora_sn','vehicle_user_id','vehicle_type','use_type','vehicle_charge','charge_count','latitude','longitude');
		//getFiledsum($table, $field, $field_name, $field_val)
		$num=1;
		$vehicle_temp = array(
			'status'			=>	"",
			'to_num'			=>	"",
			'tso_num'			=>	"",
			'td_num'			=>	"",
			'tsd_num'			=>	"",
			'unit_id'			=>	"",
			'vehicle_code'			=>	"",
			'manufacture_date'		=>	"",
			'license_plate_number'	=>	"",
			'engine_number'			=>	"",
			'car_machine_id'		=>	"",
			'vehicle_model'			=>	"",
			'vehicle_color'			=>	"",
			'4g_ip'					=>	"",
			'bluetooth_name'		=>	"",
			'bluetooth_mac'			=>	"",
			'lora_sn'				=>	"",
			'vehicle_user_id'		=>	"",
			'vehicle_type'			=>	"",
			'use_type'				=>	"",
			'vehicle_charge'		=>	"",
			'charge_count'			=>	"",
			'latitude'				=>	"",
			'longitude'				=>	""
		);

		$error = '';
		for($row = 2; $row <= $highestRow; $row++) {
			//每寫入一行就重新初始化
			$vehicle_array = $vehicle_temp;
			if(in_array($num, $num_temp)){
				for($column = 0; $column < $highestColumn; $column++)
				{
					$value = $sheet->getCellByColumnAndRow($column, $row)->getValue();
					$vehicle_array[$dataname[$column]] = $value;

					switch($dataname[$column]){
						case 'td_num': //经销商
							if($value != ''){
								$sd_count = $this->Model_show_list->getFiledsum('tbl_dealer', 's_num', 'tde01', $value);
								if(count($sd_count) > 0){
									$vehicle_array[$dataname[$column]] = $sd_count[0]['s_num'];
								}
							}
						break;
						case 'tsd_num': //子经销商
							if($value != ''){
								$tsd_count = $this->Model_show_list->getFiledsum('tbl_sub_dealer', 's_num', 'tsde01', $value);
								if(count($tsd_count) > 0){
									$vehicle_array[$dataname[$column]] = $tsd_count[0]['s_num'];
								}
							}
						break;
						case 'to_num'://营运商tbl_operator
							if($value != ''){
								$so_count = $this->Model_show_list->getFiledsum('tbl_operator', 's_num', 'top01', $value);
								if(count($so_count) > 0){
									$vehicle_array[$dataname[$column]] = $so_count[0]['s_num'];
								}
							}
						break;
					}
				}

				$flag = $this->Model_vehicle->upload_insertData($vehicle_array);
				if($flag == 'N'){
					$error .= '序號【'.$row.'】匯入失敗<br />';
				}
			}
			$num++;

		}

		if($error == ''){
			header("Content-Type:text/html; charset=utf-8");
			echo"<script> setTimeout(function () {alert(\"匯入成功\"); location.href = './';}, 3000);</script>";
		}else{
			header("Content-Type:text/html; charset=utf-8");
			echo $error;
			echo"<script> setTimeout(function () {alert(\"匯入失敗\");location.href = './';}, 3000);</script>";
		}
	}	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
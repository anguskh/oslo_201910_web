<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 登出管理
 * Logout 登出管理
 */
class Logout extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入Logout的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->helper('url');
		//语系载入与取得
		if($this->session->userdata('default_language')){
			$this->lang->load("logout", $this->session->userdata('default_language'));
		}else{
			$this->lang->load("logout", $this->session->userdata('display_language'));
		}
    }

	/**
	 * 登出作业 页面
	 */
	public function logout_confirm () 
	{
		$this->load->view( "logout" ) ;
	}
	
	/**
	 * 确认登出作业 页面
	 */
	public function logout_ok () 
	{
		$this->session->set_userdata('function_name', $this->router->fetch_class());
		$this->session->set_userdata('desc', array());
		$this->model_background->log_operating_insert('0');
		
		//清除资料库内使用者登入资讯
		$this->load->model("common/model_background", "model_background") ;
		$this->model_background->destroy_logindata($this->session->userdata('user_sn'));
		$group_sn = $this->session->userdata('group_sn');
		$this->session->sess_destroy();
		if($this->session->userdata('login_side') == 1)
			$web = $this->config->item('base_url')."nimda/";
		else
			$web = $this->config->item('base_url');
		//redirect($web.'login', 'refresh');

		echo '<script type="text/javascript">
						parent.location.href = "'.$web.'login"
				</script>';
	}
	
}
/* End of file logout.php */
/* Location: ./application/controllers/logout.php */
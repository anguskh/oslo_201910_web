<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bss_international extends MY_Controller {
	
	/**
	 * 建构式
	 * 预先载入Login的物件
	 */
    function __construct() 
    {
        parent::__construct();

        // $this->load->model("simulator/Model_bss", "model_bss") ;
        $this->load->library('session');
		$this->load->library('user_agent');
        $this->load->helper('url');
        $this->load->library('ciqrcode');
        $this->load->model("operator/Model_battery_swap_station", "model_battery_swap_station") ;
        $this->load->model("common/Model_show_list", "model_show_list") ;
        $this->load->model("common/Model_access", "Model_access") ;
        $this->load->model("common/Model_background", "Model_background") ;
        $this->load->model("operator/Model_battery_swap_station", "Model_battery_swap_station") ;
        $this->load->model("inquiry/Model_log_battery_leave_return", "Model_log_battery_leave_return") ;
        if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("bss_simulator", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("bss_simulator", $this->session->userdata('display_language'));
		}
		$language = "";
    }
	
	public function index()
	{
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data['bssInfo'] = $this->model_show_list->getbatteryswapstationList();
		$s_num = $data['bssInfo'][0]['s_num'];
		$info = $this->Model_battery_swap_station->getInfo($s_num) ;
		$data["location"] = $info[0]['location'];
		$data["bss_id"] = $info[0]['bss_id'];
		$data["last_online"] = (($info[0]['last_online'] == '0000-00-00 00:00:00')?'':$info[0]['last_online']);
		$tmpRow = $this->Model_battery_swap_station->getbattery_swap_track($s_num) ;
		$data["dataInfo"] = $tmpRow;
		$data["memberInfo"] = $this->model_show_list->getMemberList();

		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('機櫃模擬器(國際版)');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "simulator/bss_international_simulator_view", $data ) ;
		$this->load->view( "common/footer") ;

	}

	public function detail_view()
	{
		$s_num = $this->input->post("s_num");
		$info = $this->Model_battery_swap_station->getInfo($s_num) ;
		$returnArr["location"] = $info[0]['location'];
		$returnArr["bss_id"] = $info[0]['bss_id'];
		$returnArr["last_online"] = (($info[0]['last_online'] == '0000-00-00 00:00:00')?'':$info[0]['last_online']);
		$tmpRow = $this->Model_battery_swap_station->getbattery_swap_track($s_num) ;
		$dataInfo = $tmpRow;

		$battery_cell_name = array(""=>"","0"=>"N/A","1"=>"Over Temp");
		$bcu_name = array(""=>"","1"=>"正常","2"=>"异常","3"=>"停用");
		$status_name = array(""=>"", "0"=>"充电中","1"=>"饱电","2"=>"充电中无电流","3"=>"电池过温");
		$track_name = array(""=>"","1"=>"空轨且都正常","2"=>"有电池且都正常","3"=>"电池应取但未取出","4"=>"电池充电时间过长","5"=>"轨道停用","6"=>"CCB过温","7"=>"机柜温度过温");
		$returnArr['bss'] = "";
		for($i=1; $i<=8; $i++){
			
			$onclick = "";
			$spandata = $i;
			$content = '';
			$class = '';
			$canuse = "";
			$battery_id= "";
			$trackdata="";
			$s = 1;
			if(isset($dataInfo[$i])){
				if($dataInfo[$i]['column_charge'] == 'Y'){
					$class = 'charge';
				}else if($dataInfo[$i]['column_charge'] == 'F'){
					//故障
					$class = 'error';
				}else if($dataInfo[$i]['column_charge'] == 'N' || $dataInfo[$i]['column_charge'] == 'S'){
					$class = "";
				}

				if($dataInfo[$i]['column_park'] == 'Y'){
					if(!isset($dataInfo[$i])){
						$content = "";
					}else{
						if($dataInfo[$i]['battery_status']=="1")
						{
							$canuse = "canuse";
						}
						else if($dataInfo[$i]['battery_status']=="0")	
						{
							$canuse = "canuse";
						}
						//echo $dataInfo[$i]['battery_status'];
						$battery_id = $dataInfo[$i]['battery_id'];
						$battery_voltage = $dataInfo[$i]['battery_voltage'];
						$battery_status = $dataInfo[$i]['battery_status'];
						$battery_amps = $dataInfo[$i]['battery_amps'];
						$battery_capacity = $dataInfo[$i]['battery_capacity'];
						$battery_temperature = $dataInfo[$i]['battery_temperature'];
						$track_status = $dataInfo[$i]['track_status'];
						$trackdata="{$battery_id},{$battery_voltage},{$battery_status},{$battery_amps},{$battery_capacity},{$battery_temperature},{$track_status}";
						$content = '电池序号：'.$dataInfo[$i]['battery_id'].'<br/>';
						$content .= '电池电压：'.$dataInfo[$i]['battery_voltage'].'<br/>';
						$content .= '电池状态：'.$status_name[$dataInfo[$i]['battery_status']].'<br/>';
						$content .= '电池电流：'.$dataInfo[$i]['battery_amps'].'<br/>';
						$content .= 'SOC：'.$dataInfo[$i]['battery_capacity'].'<br/>';
						$content .= '电池温度：'.$dataInfo[$i]['battery_temperature'].'<br/>';
						$content .= '轨道状态：'.$track_name[$dataInfo[$i]['track_status']].'<br/>';
					}
				}else if($dataInfo[$i]['column_park'] == 'N'){
					//沒電池(空軌)
					$class = 'no';
					$onclick = "returnBattery(this)";
					$spandata = '<button id="returnBt" onclick="returnbt(this)">'.$i.'</button>';
				}

				if($dataInfo[$i]['tbst_status'] == 'E'){
					//故障
					$class = 'error';
				}else if($dataInfo[$i]['tbst_status'] == 'N'){
					//電池交換軌道狀態(N 停用)
					$class = 'stop';
				}

				$returnArr['bss'] .='<li>
						<span id="track'.$i.'" data-toggle="tooltip" data-placement="right" data-html="true" title data-original-title="'.$content.'" class="'.$class.' '.$canuse.'" batteryid="'.$battery_id.'" trackdata="'.$trackdata.'">'.$spandata.'</span>
					  </li>';

			}
		
		}
		echo json_encode($returnArr);
	}

	//動態產生QR Code
	public function createQR_Code($bss_id){
		// echo $bss_id;
		if($bss_id != ""){
			header("Content-Type: image/png");
			
			//$params['data'] = $apk_url;
			$params['data'] = $bss_id;

			$qrcode = $this->ciqrcode->generate($params);
		}
	}

	public function getuserbrrow()
	{
		$user_id = $this->input->post("user_id");
		$batteryIDArr = $this->Model_log_battery_leave_return->getuserallbattery($user_id);
		$battery_id = "";
		if($batteryIDArr)
		{
			foreach($batteryIDArr as $key => $value)
			{
				if($value['leave_battery_id']!="")
				{
					if($battery_id == "")
					{
						$battery_id = $value['leave_battery_id'];
					}
					else
					{
						$battery_id .= ",".$value['leave_battery_id'];
					}
				}
			}
		}
		echo $battery_id;
	}

	public function getreturnUserID()
	{
		$returnBatteryID = $this->input->post("returnBatteryID");
		$returnUserID = $this->Model_log_battery_leave_return->getBatteryuserID($returnBatteryID);
		echo $returnUserID;
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
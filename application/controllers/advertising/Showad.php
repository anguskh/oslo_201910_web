<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Showad extends CI_Controller {
	
	/**
	 * 建构式
	 * 预先载入Login的物件
	 */
    function __construct() 
    {
        parent::__construct();

        $this->load->model("advertising/Model_showad", "model_showad") ;
		$language = "";
    }
	
	public function index($ad_num="")
	{
		$adInfo = $this->model_showad->getfileurl($ad_num);
		$ad_path = $this->model_showad->getadpath($ad_num);
		$data['error_msg'] = "";
		$data['ad_type'] = "";
		$data['fileurlArr'] =array();
		if(count($adInfo)!=0 && $ad_path!="")
		{
			$data['ad_type'] = $adInfo[0]['advertising_type'];
			$imgurlArr = array();
			$imgnameArr = explode(",",$adInfo[0]['fileStr']);
			foreach($imgnameArr as $key => $value)
			{
				$dataArr[$key] = $ad_path."/".$value;
			}
			$data['fileurlArr'] = $dataArr;
		}
		else
		{
			$data['error_msg'] = "參數錯誤";
		}

		if($data['ad_type'] == 2)
			$this->load->view( "advertising/show_videoad_view", $data ) ;
		else
			$this->load->view( "advertising/show_imgad_view", $data ) ;
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Exchange_average extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("statistics/Model_exchange_average", "Model_exchange_average") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("exchange_average", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("exchange_average", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		//$totalRow = $this->Model_exchange_average->getAllCnt() ;
		$totalRow = 0;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		//$data["InfoRow"] = $this->Model_exchange_average->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		$data["InfoRow"] = array();
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站平均时数交换次数');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$whereStr = "1=1";
		$rs = $this->Model_exchange_average->search_all($whereStr);
		$data["chart_list"] = $rs;


		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "statistics/exchange_average_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "exchange_average");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}

}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
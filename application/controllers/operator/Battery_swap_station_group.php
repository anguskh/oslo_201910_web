<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 电池资料
 * 
 */
class Battery_swap_station_group extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spConfigArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->model("operator/Model_battery_swap_station_group", "Model_battery_swap_station_group") ;
		$this->load->model("common/Model_background", "Model_background") ;
		$this->load->library("my_splitpage", $spConfigArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("battery_swap_station_group", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("battery_swap_station_group", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{

		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_battery_swap_station_group->getAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得清单的资料
		$data["InfoRow"] = $this->Model_battery_swap_station_group->getList( $this->session->userdata('paging_rows'), $startRow ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//取得換電站下拉选单资料
		$rs = $this->Model_show_list->getbatteryswapstationList();
		$battery_station_list = array();
		foreach($rs as $key => $value){
			$battery_station_list[$value['s_num']] = $value['bss_id_name'];
		}
		$data["battery_station_list"] = $battery_station_list;


		//权限功能
		$data["user_access_control"] = $this->Model_access->user_access_control();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/battery_swap_station_group_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	


	/**
	 * 新增作业 页面
	 */
	public function addition () 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/battery_swap_station_group_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$sn = $this->input->post("s_num");
		if ($sn == '') {  //没流水为新增
			$this->Model_battery_swap_station_group->insertData() ;
		} else {  //有流水号为修改
			$this->Model_battery_swap_station_group->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($priNo = "")
	{
		$this->form_create($priNo);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($priNo = "")
	{
		$this->form_create($priNo, true);
	}	
	
	//修改检视页面
	function form_create($priNo = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$kind_no = $this->input->post("ckbSelArr");
		$kind_no = $kind_no[0];
		
		$tmpRow = $this->Model_battery_swap_station_group->getInfo($kind_no) ;
		$data["dataInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["dataInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//取得換電站下拉选单资料
		$data['batteryswapstationList'] = $this->Model_show_list->getbatteryswapstationList();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('換電站群组管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/battery_swap_station_group_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_battery_swap_station_group->deleteData();
	}

	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		/*$this->session->set_userdata('searchType', "battery_swap_station_group");
		$searchArr = setSearch2Arr($this->input->post());
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);*/
		$fn = get_fetch_class_random();
		//最外层的search_txt
		$search_txt = $this->input->post("search_txt");
		$this->session->set_userdata("{$fn}_".'search_txt', $search_txt);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
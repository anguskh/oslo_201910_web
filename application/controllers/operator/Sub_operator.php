<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

/**
 * 功能名称 : 安装基本资料
 * 
 */
class Sub_operator extends MY_Controller {
    
	/**
	 * 建构式
	 * 预先载入物件
	 */
    function __construct() 
    {
        parent::__construct();

		$spSub_operatorArr = array( "base_pageRow"=>$this->session->userdata('paging_rows') ) ;

		$this->load->model("operator/Model_operator", "Model_operator") ;
		$this->load->model("operator/Model_sub_operator", "Model_sub_operator") ;

		$this->load->model("common/Model_checkfunction", "Model_checkfunction") ;
		$this->load->model("common/Model_show_list", "Model_show_list") ;
		$this->load->model("common/Model_access", "Model_access") ;
		$this->load->library("my_splitpage", $spSub_operatorArr, "SplitPage") ;
		$this->load->database();

		if($this->session->userdata('default_language'))
		{
			$this->lang->load("common", $this->session->userdata('default_language'));
			$this->lang->load("sub_operator", $this->session->userdata('default_language'));
		}
		else {
			$this->lang->load("common", $this->session->userdata('display_language'));
			$this->lang->load("sub_operator", $this->session->userdata('display_language'));
		}
	}
	
	/**
	 *	首页
	 */
	public function index ( $startRow = 0 ) 
	{
		$startRow = $startRow < 0 ? 0 : $startRow;
		$totalRow = $this->Model_sub_operator->getSuboperatorAllCnt() ;

		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;

		// 取得table:tbl_sub_operator的资料
		$data["suboperatorInfoRow"] = $this->Model_sub_operator->getSubDealerList( $this->session->userdata('paging_rows'), $startRow ) ;
		// print_r( $data["operatorInfoRow"] ) ;
		
		//取的我的最爱资料
		$this->load->model("nimda/model_shortcut", "model_shortcut"); 
		$data["favor_data"] = $this->model_shortcut->get_user_favor();
		
		// 分页设定处理
		$data["pageInfo"] = $this->SplitPage->getPageAreaArr( $totalRow, $startRow );
		$this->session->set_userdata('PageStartRow', $startRow);
		// echo "data[pageInfo] = {$data["pageInfo"]}" ;

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('子营运商管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];
		
		//权限功能
		$data["user_access_control"] = $this->model_access->user_access_control();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/sub_operator_list", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 取得样版下拉选单
	 */
	public function getpattern(){
		$this->Model_sub_operator->getpattern();
	}

	/**
	 * 新增作业 页面
	 */
	public function addition ( $startRow = 0 ) 
	{
		$data["StartRow"] = $this->input->post("start_row");
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();
		
		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('子营运商管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = false;
		$data["user_access_control"] = $this->model_access->user_access_control('edit');
		
		//查经销商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/sub_operator_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * insert/update db 
	 */
	public function modification_db () 
	{
		$s_num = $this->input->post("s_num");
		if ($s_num == '') {  //没流水为新增
			$this->Model_sub_operator->insertData() ;
		} else {  //有流水号为修改
			$this->Model_sub_operator->updateData() ;
		}		
	}
	
	/**
	 * 修改作业 页面
	 */
	public function modification($s_num = "")
	{
		$this->form_create($s_num);
	}
	
	/**
	 * 检视作业 页面
	 */
	public function view($s_num = "")
	{
		$this->form_create($s_num, true);
	}	
	
	//修改检视页面
	function form_create($s_num = "", $bView = false){
		$data["StartRow"] = $this->input->post("start_row");
		$s_num = $this->input->post("ckbSelArr");
		$s_num = $s_num[0];
		
		$tmpRow = $this->Model_sub_operator->getSuboperatorInfo( $s_num ) ;
		$data["sub_operatorInfo"] = $tmpRow[0] ;
		
		//纪录修改前的资料
		$this->session->set_userdata("before_desc", $data["sub_operatorInfo"]);
		
		// 取得table:sys_menu的资料
		$data["menuInfoRow"] = $this->model_menu->getMenuList() ;
		//取的我的最爱资料
		$this->load->model("nimda/Model_shortcut", "Model_shortcut"); 
		$data["favor_data"] = $this->Model_shortcut->get_user_favor();

		//查当下选单
		//$menu_arr = $this->model_access->getNowMenuSn('子营运商管理');
		//$data["one_menu_sn"] = $menu_arr[0]['parent_menu_sn'];
		//$data["now_menu_sn"] = $menu_arr[0]['menu_sn'];

		//权限功能
		$data['bView'] = $bView;
		if($bView){
			$data["user_access_control"] = $this->model_access->user_access_control('view');
		}else{
			$data["user_access_control"] = $this->model_access->user_access_control('edit');
		}

		//查经销商
		$data["operator_list"] = $this->Model_show_list->getoperatorList();
		
		$this->load->view( "common/header", $data) ;
		$this->load->view( "common/menu", $data ) ;
		$this->load->view( "operator/sub_operator_form", $data ) ;
		$this->load->view( "common/footer") ;
	}
	
	/**
	 * 删除作业 db
	 */
	public function delete_db () 
	{
		$this->Model_sub_operator->deleteData();
	}
	
	/**
	 * 搜寻作业　
	 */
	public function search () 
	{
		$this->session->set_userdata('searchType', "sub_operator");
		$searchArr = setSearch2Arr($this->input->post());
		$fn = get_fetch_class_random();
		$this->session->set_userdata("{$fn}_".'searchData', $searchArr);

		$get_full_url_random = get_full_url_random();
		redirect($get_full_url_random, 'refresh');
	}
	
}
/* End of file user.php */
/* Location: ./application/controllers/nimda/user.php */
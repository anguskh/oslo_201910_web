<?php
//其他
$lang['card_management_management'] = '卡片管理';
//其他
$lang['card_management_to_num'] = '营运商名称';
$lang['card_management_top08'] = '预购次数';
$lang['operator_card_count'] = '卡片张数';
$lang['card_management_status'] = '状态';
$lang['card_management_detail'] = '明细';
$lang['card_management_detail_record'] = '交换次数历程';
$lang['card_management_name'] = '卡片名称';
$lang['card_management_qrocde'] = 'qrocde内容';
$lang['card_management_pre_order_num'] = '总预购次数';
$lang['card_management_lave_num'] = '剩馀次数';
$lang['card_management_add_num'] = '加购次数';
$lang['card_management_before_total_lave_num'] = '加购前总剩馀次数';
$lang['card_management_create_date'] = '加购日期';
$lang['card_management_lease_type'] = '加购类型';
$lang['card_management_lease_price'] = '单次金额';

$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['to_num'] = '营运商';
$lang['name'] = '卡片名称';
$lang['user_id'] = 'qrocde内容';
$lang['pre_order_num'] = '总预购次数';
$lang['lave_num'] = '剩馀次数';
$lang['nick_name'] = '保管人姓名';
$lang['mobile'] = '行动电话'; 
$lang['city'] = '居住县市'; 
$lang['address'] = '连络地址'; 

$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';
$lang['lease_type'] = '加购类型';
$lang['lease_type_1'] = '预购';
$lang['lease_type_2'] = '月结';
$lang['lease_price'] = '单次交易金额';

//租借记录
$lang['log_battery_leave_return_tv_num'] = 'Unit ID';
$lang['log_battery_leave_return_tm_num'] = '会员名称';
$lang['log_battery_leave_return_system_log_date'] = '系统记录时间';
$lang['log_battery_leave_return_leave_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_leave_do_num'] = '(借)营运商/经销商';
$lang['log_battery_leave_return_leave_dso_num'] = '(借)子营运商/子经销商';
$lang['log_battery_leave_return_leave_sb_num'] = '租借換電站';
$lang['log_battery_leave_return_leave_sb_num_name'] = '租借換電站地点名称（換電站序号）';
$lang['log_battery_leave_return_battery_user_id'] = 'User ID';
$lang['log_battery_leave_return_leave_request_date'] = '換電请求日期时间';
$lang['log_battery_leave_return_leave_coordinate'] = '借电请求时的纬度经度';
$lang['log_battery_leave_return_leave_date'] = '換電实际日期时间';
$lang['log_battery_leave_return_leave_track_no'] = '租借轨道编号';
$lang['log_battery_leave_return_leave_battery_id'] = '租借电池序号';
$lang['log_battery_leave_return_return_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_return_do_num'] = '(还)营运商/经销商';
$lang['log_battery_leave_return_return_dso_num'] = '(还)子营运商/子经销商';
$lang['log_battery_leave_return_return_sb_num'] = '归还換電站';
$lang['log_battery_leave_return_return_sb_num_name'] = '归还換電站地点名称（換電站序号）';

$lang['log_battery_leave_return_return_request_date'] = '电池归还请求日期时间';
$lang['log_battery_leave_return_return_coordinate'] = '归还请求时的纬度经度';
$lang['log_battery_leave_return_return_date'] = '电池归还实际日期时间';
$lang['log_battery_leave_return_return_track_no'] = '归还轨道编号';
$lang['log_battery_leave_return_return_battery_id'] = '归还电池序号';
$lang['log_battery_leave_return_usage_time'] = '电池使用时间';
$lang['log_battery_leave_return_charge_amount'] = '计算此次扣款金额';

$lang['log_battery_trail'] = '路线轨迹';

$lang['log_store_balance_charge_type'] = '储值/扣款类别';
$lang['log_store_balance_charge_exchange_date'] = '储值/扣款日期时间';
$lang['log_store_balance_charge_charge_amount'] = '此次储值/扣款金额';
$lang['log_store_balance_charge_store_balance_before'] = '储值/扣款前储值余额';
$lang['log_store_balance_charge_store_balance_after'] = '储值/扣款后储值余额';



/* End of file card_management_lang.php */
/* Location: ./system/language/zh_tw/card_management_lang.php */

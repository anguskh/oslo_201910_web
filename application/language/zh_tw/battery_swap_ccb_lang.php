<?php
//标题
$lang['battery_swap_ccb_management'] = '借电站CCB管理';

//其他
$lang['battery_swap_ccb_select_operator'] = '--请选择营运商--';
$lang['battery_swap_ccb_select_station'] = '--请选換電站--';
$lang['battery_swap_ccb_operator'] = '营运商';
$lang['battery_swap_ccb_ccb_id'] = '換電CCB序号';
$lang['battery_swap_ccb_bss_id'] = '換電站序號';
$lang['battery_swap_ccb_station'] = '換電站';
$lang['battery_swap_ccb_status'] = '状态';
$lang['battery_swap_ccb_statusY'] = '启用';
$lang['battery_swap_ccb_statusN'] = '停用';
$lang['battery_swap_ccb_statusE'] = '故障';

//栏位
$lang['so_num'] = '营运商';
$lang['sb_num'] = '換電站';
$lang['ccb_id'] = '換電CCB序号';
$lang['status'] = '換電轨道状态';

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

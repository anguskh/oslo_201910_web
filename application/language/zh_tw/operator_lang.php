<?php
//其他
$lang['operator_management'] = '营运商管理';
//其他
$lang['operator_s_num'] = '营运商编号';
$lang['operator_top01'] = '营运商名称';
$lang['operator_top02'] = '型别';
$lang['operator_top03'] = '连络人';
$lang['operator_top04'] = '连络电话';
$lang['operator_top05'] = '行动电话';
$lang['operator_top06'] = '地址';
$lang['operator_top07'] = 'Email';
$lang['operator_top08'] = '平台交易手续费率';
$lang['operator_top09'] = '银行转帐代码';
$lang['operator_top10'] = '银行转帐帐号';
$lang['operator_top11'] = '代处理子营运帐务';
$lang['operator_status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
$lang['operator_tbss_count'] = '机柜数';
$lang['operator_tb_count'] = '电池数量';

//栏位
$lang['s_num'] = '营运商编号';
$lang['top01'] = '营运商名称';
$lang['top02'] = '营运商型别';
$lang['top02_1'] = '直营';
$lang['top02_2'] = '加盟';
$lang['top02_3'] = '其它';
$lang['top03'] = '营运商连络人姓名';
$lang['top04'] = '营运商连络电话';
$lang['top05'] = '营运商行动电话';
$lang['top06'] = '营运商地址';
$lang['top07'] = 'Email';
$lang['top09'] = '营运商代码';
$lang['status'] = '营运商状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file operator_lang.php */
/* Location: ./system/language/zh_tw/operator_lang.php */

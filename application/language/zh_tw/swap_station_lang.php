<?php
//标题
$lang['swap_station_management'] = '电池监控报表';

//栏位
$lang['bss_id'] = 'BSS序號';
$lang['log_date'] = '借电站發送日期';
$lang['battery_id'] = '电池序號';
$lang['status'] = '电池狀態';
$lang['battery_capacity'] = 'SOC';
$lang['battery_temperature'] = '电池溫度';
$lang['battery_amps'] = '电池电流';
$lang['battery_voltage'] = '电池电壓';
$lang['charge_cycles'] = '充电次數';
$lang['exchange_num'] = '电池交換次數';
/* End of file swap_station_lang.php */
/* Location: ./system/language/zh_tw/swap_station_lang.php */

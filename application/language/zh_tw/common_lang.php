<?php
//一般通用
$lang['airlink'] = '科技股份有限公司';
$lang['airlink_eng'] = 'test Technology Co.,Ltd.';
$lang['airlink_addr'] = '无地址';
$lang['airlink_tel'] = 'TEL:(02)1111-1111 FAX:(02)1111-1111';

$lang['logout'] = '登出';
$lang['select'] = '选择';
$lang['browser'] = '浏览/查询';
$lang['view'] = '检视';
$lang['view_1'] = '<i class="fas fa-eye"></i>';
$lang['add'] = '新增';
$lang['add_1'] = '<i class="fas fa-plus"></i>';
$lang['edit'] = '修改';
$lang['edit_1'] = '<i class="fas fa-edit"></i>';
$lang['delete'] = '删除';
$lang['delete_1'] = '<i class="fas fa-trash-alt"></i>';
$lang['save'] = '存档';
$lang['cancel'] = '取消';
$lang['print'] = '预览列印';
$lang['auto_change'] = '显示即时监控';
$lang['buy'] = '加购';
$lang['make_up'] = '补登';

$lang['print_ok_vender'] = '合格厂商一览表';
$lang['print_item_all'] = '料品明细表';
$lang['print_item_new_old'] = '料品新旧对照表';
$lang['trans_inv'] = '前月转入';
$lang['recount_inv'] = '重新计算库存';
$lang['data_search'] = '搜寻';
$lang['export_xml'] = '汇出XML';
$lang['export_excel'] = '汇出EXCEL';
$lang['batch_report'] = '批次回报';

$lang['resetkey'] = '回收';
$lang['resetphone'] = '清除匹配';
$lang['change_readersn'] = '维修换机';
$lang['up'] = '上移';
$lang['down'] = '下移';
$lang['copy'] = '复制';
$lang['reload'] = '重选';
$lang['search'] = '搜寻';
$lang['search_button'] = '搜 寻';
$lang['import_img'] = '汇入图档';
$lang['select_img'] = '选择图档';
$lang['img_format'] = '仅支援jpg,png格式,大小:175*320';
$lang['prev_page'] = '上一页';
$lang['paring_merchant'] = '配对商店';
$lang['favor_tip'] = '此区域可自订捷径, 如需增加请点选子选单前的星号!';

$lang['enable'] = '启用';
$lang['disable'] = '停用';
$lang['bypass'] = 'ByPass HASH check';
$lang['bypass2'] = 'ByPass';
$lang['nolimits'] = '新版本更新通知 (无期限)';
$lang['nolimits2'] = '无期限';
$lang['limits'] = '版本将于截止日后停用 (有限期)';
$lang['limits2'] = '有限期';
$lang['disable_2'] = '未启用';
$lang['disableuse'] = '暂停使用';
$lang['lockup'] = '锁机';
$lang['approve'] = '通过';
$lang['not_approve'] = '未通过';
$lang['yes'] = '是';
$lang['no'] = '否';
$lang['sucess'] = '成功';
$lang['fail'] = '失败';
$lang['login_side_0'] = '前台';
$lang['login_side_1'] = '后台';
$lang['login_side_2'] = '全部';
$lang['search_all'] = '全部';
$lang['merchant_event'] = '商店事件';
$lang['system_event'] = '系统事件';

$lang['male'] = '男';
$lang['female'] = '女';

$lang['currently_online_users'] = '目前正在线上的人员';
$lang['last_10_rows_of_unsettlement'] = '最近未结帐明细';
$lang['last_10_rows_of_modify_merchant'] = '最近异动商店';
$lang['last_10_rows_of_modify_merchant_data'] = '最近异动资料';
$lang['last_10_rows_of_event_log'] = '最近事件日志';


$lang['add_successfully'] = '新增成功';
$lang['edit_successfully'] = '修改成功';
$lang['edit_failed'] = '修改失败';
$lang['add_failed'] = '新增失败';
$lang['copy_successfully'] = '复制成功';
$lang['reset_successfully'] = '清空成功';
$lang['disable_merchant'] = '\n, 目前已无拥有端末机, 将自动停用此商店和相关使用者帐号!';
$lang['change_successfully'] = '换机成功';
$lang['delete_successfully'] = '删除成功';
$lang['delete_failed'] = '删除失败';
$lang['recover_successfully'] = '复原资料成功';
$lang['recover_fail'] = '复原资料失败';

$lang['confirm_delete'] = '是否确认要删除？';
$lang['cnat_delete'] = '无法删除!';
$lang['confirm_resetkey'] = '是否确认要清空XAC E50序号和key值？\n此动作将会清除重要栏位,\n如(BKLK_KEK...)';
$lang['confirm_resetphone'] = '是否确认要清空手机匹配？';
$lang['ajax_request_an_error_occurred'] = 'Ajax request 发生错误！\n请重试！';
$lang['search_not_found'] = '查无资料';
$lang['process_failed'] = '无资料可删除';
$lang['delete_img'] = '删除图档';


$lang['confirm'] = '确认执行';
$lang['next'] = '下一步';
$lang['resend'] = '重新发送';
$lang['space'] = '　';
$lang['space1_9'] = '　';
$lang['space2'] = '　';
$lang['space6'] = '　　　　　';

$lang['home'] = '首页';
$lang['all'] = '全部列表';
$lang['all_not_approve'] = '全部未覆核列表';
$lang['bank'] = '银行：';
$lang['bank2'] = '银行';
$lang['kind'] = '类别：';
$lang['merchant'] = '商店：';
$lang['terminal'] = '端末机：';
$lang['project'] = '专案：';
$lang['user'] = '使用者 : ';

$lang['select_bank'] = '请选择银行';
$lang['select_bank_help'] = '-- 请选择欲维护专案的银行 --&nbsp;';
$lang['select_supplier'] = '请选择刷卡机厂商：';
$lang['select_supplier_help'] = '-- 请选择欲维护专案的刷卡机厂商 --&nbsp;';
$lang['select_merchant'] = '请选择商店';
$lang['select_pattern'] = '请选择样版';
$lang['select_merchant_help'] = '-- 请选择欲维护的商店 --&nbsp;';
$lang['select_terminal'] = '请选择端末机：';
$lang['select_terminal_help'] = '-- 请选择欲维护的端末机 --&nbsp;';
$lang['select_project'] = '请选择专案：';
$lang['select_project_help'] = '-- 请选择欲维护的专案 --&nbsp;';
$lang['select_user'] = '请选择使用者：';
$lang['select_user_help'] = '-- 请选择欲维护的使用者 --&nbsp;';
$lang['select_choose_a_date'] = '请选择日期区间';
$lang['select_choose_close_date'] = '请选择结帐日期';
$lang['select_choose_statistical'] = '请选择统计日期<br />(无选择结束日期, 预设取起日前三个月资料)';
$lang['select_event_type'] = '请选择事件';
$lang['select_event_type_help'] = '-- 请选择事件 --　';
$lang['select_system_event'] = '系统事件';
$lang['select_merchant_event'] = '商店事件';
$lang['select_all'] = '全部';
$lang['select_all_bank_no'] = '全部银行';
$lang['select_all_kind_no'] = '全部类别';
$lang['select_all_isp_class'] = '全部分类';
$lang['select_all_ship_class'] = '全部分类';
$lang['select_all_stock_class'] = '全部分类';
$lang['select_city'] = '请选择城市';
$lang['select_dept'] = '请选择部门';
$lang['select_location'] = '请选择办公室地点';
$lang['select_district'] = '请选择地区';
$lang['select_machine'] = '请选择机型';
$lang['select_machineap'] = '请选择POSAP版本';
$lang['select_kernel'] = '请选择kernel版本';
$lang['select_status'] = '请选择状态';
$lang['select_flag'] = '请选择完成结果';
$lang['select_unfinish'] = '请选择未完成原因';
$lang['select_onsite'] = '请选择回收方式';
$lang['select_maintain_onsite'] = '请选择处理方式';
$lang['select_cause_no'] = '请选择不良原因';
$lang['select_air_cause_no'] = '请选择公司不良原因';
$lang['select_fixway_no'] = '请选择完成维修';
$lang['select_use_situation'] = '请选择卡机处理';
$lang['select_position'] = '请选择职位';
$lang['select_charge_area'] = '请选择负责区域';
$lang['select_bank_kind'] = '请选择客户类别';
$lang['select_per_no'] = '请选择业务负责人';
$lang['select_per_no2'] = '请输入或选择人员';
$lang['select_request_per_no'] = '请选择请购人员';
$lang['select_request_ord_per'] = '请选择订购人员';
$lang['select_request_isp_per'] = '请选择验收人员';
$lang['select_check_per'] = '请选择盘点人员';
$lang['select_isp_per'] = '请选择申请人员';
$lang['select_stock_per'] = '请选择仓管人员';
$lang['select_stock_per2'] = '请选择验收主管';
$lang['select_ship_per'] = '请选择发货人员';
$lang['select_ship_per2'] = '请选择请款人员';
$lang['select_ship_per3'] = '请选择申请人员';
$lang['select_t_manager'] = '请选择部门主管';
$lang['select_check_per'] = '请选择盘点人员';
$lang['select_stock_manager'] = '请选择仓管主管';
$lang['select_ship_way'] = '请选择交货方式';
$lang['select_sales_kind'] = '请选择交货类别';
$lang['select_machine_type_per_no'] = '请选择厂商员工';
$lang['select_account_close_date'] = '请选择结帐日';
$lang['select_payway'] = '请选择付款方式';
$lang['select_paydate'] = '请选择付款日';
$lang['select_invoice_way'] = '请选择发票种类';
$lang['select_paygetway'] = '请选择取款方式';
$lang['select_bank_no'] = '请选择银行';
$lang['select_bank_no2'] = '请选择客户编号';
$lang['select_bank_no3'] = '请选择客户名称';
$lang['select_four_dbc'] = '请选择4DBC';
$lang['select_kind_no'] = '请选择料号类别';
$lang['select_item_no'] = '请选择料号';
$lang['select_isp_class'] = '请选择物品分类';
$lang['select_ship_class'] = '请选择物品分类';
$lang['select_use_mark'] = '请选择用途';
$lang['select_request_status'] = '请选择报比价';
$lang['select_vnd_no'] = '请选择厂商';
$lang['select_warranty_way'] = '请选择保固条件';
$lang['select_paytxt'] = '请选择付款条件';
$lang['select_remark'] = '请选择备注';
$lang['select_stock_class_2'] = '请选择出库类别';
$lang['select_stock_class_3'] = '请选择移仓类别';
$lang['select_tra_reason'] = '请选择移仓原因';
$lang['select_reason'] = '请选择差异原因';
$lang['select_version'] = '请选择银行版本';
$lang['select_user2'] = '请选择使用者';
$lang['select_notice_no'] = '请选择通知方式';

$lang['select_release_per'] = '请选择申请人员';
$lang['select_stock_per'] = '请选择仓管人员';
$lang['select_rel_sub_per'] = '请选择代申请人员';
$lang['select_stock_class'] = '请选择领用类别';
$lang['select_instock_class'] = '请选择入库类别';
$lang['select_stock_kind'] = '请选择领用大类';
$lang['select_warehouse_no'] = '请选择出库仓库';
$lang['select_warehouse_no2'] = '请选择仓库';
$lang['select_warehouse_no3'] = '请输入或选择仓库';
$lang['select_new_warehouse_no'] = '请选择入库仓库';
$lang['select_release_reason'] = '请选择出库原因';
$lang['select_ost_sub_per_no'] = '请选择代出人员';
$lang['select_ret_sub_per'] = '请选择代入人员';
$lang['select_ins_per_no'] = '请选择安装人员';
$lang['select_mai_per_no'] = '请选择维护人员';
$lang['select_sha_per_no'] = '请选择共用人员';
$lang['select_rec_per_no'] = '请选择回收人员';
$lang['select_rep_per'] = '请选择记录人员';
$lang['select_area_no'] = '请选择储位';
$lang['select_emsType'] = '请选择类别代号';
$lang['select_contlsUFlag'] = '请选择含U卡功能';
$lang['select_contlsVFlag'] = '请选择含VISA功能';
$lang['select_contlsMFlag'] = '请选择含MASTER功能';
$lang['select_contlsJFlag'] = '请选择含JBC功能';
$lang['select_contlsAEFlag'] = '请选择含AE功能';

$lang['select_return_reason'] = '请选择入库原因';
$lang['select_ret_warehouse_no'] = '请选择入库仓库';
$lang['select_check_result'] = '请选择检查结果';
$lang['select_problem_reason'] = '请选择问题件原因';

$lang['select_zip_Code'] = '请选择邮递区号';
$lang['select_pay_bank'] = '请选择汇款银行';
$lang['select_mcht_district'] = '请选择装机地区';
$lang['select_industry_code'] = '请选择行业别';
$lang['select_pay_branch'] = '请选择分行代号';

$lang['select_import_excel'] = '请选择汇入的EXCEL档案';
$lang['select_import_caption_excel2_error'] = '汇入过程失败!, 请联络管理员!';


$lang['select_all_help'] = '-- 选择全部 --&nbsp;';
$lang['select_all_mer_help'] = '-- 全部商店 --&nbsp;';
$lang['select_stock_help'] = '-- 选择库存端末机 --&nbsp;';
$lang['all_mer'] = '全部商店';

$lang['select_machine_type_no'] = '请选择机型';
$lang['select_machine_type_no2'] = '请选择机型编号';
$lang['select_all_machine_type_no'] = '全部机型';

$lang['please_input_merchant_id'] = '请输入商店代号';
$lang['please_input_terminal_id'] = '请输入端末机代号';
$lang['please_input'] = '请输入';

$lang['label_include'] = '含序号';
$lang['report_no_data'] = '无资料';

//讯息
$lang['check_url_error'] = '没有权限存取此页面，\n将自动跳转到登入页面';
$lang['success_action'] = '成功';
$lang['failed_action'] = '失败';
$lang['out_max_no'] = '超过自动编号, 请联络管理员!';
$lang['timeout_msg'] = '系统闲置已登出!';

//分页
$lang['common_first_page'] = '第一页';
$lang['common_last_page'] = '最后一页';
$lang['common_prev_page'] = '上一页';
$lang['common_next_page'] = '下一页';
$lang['common_page'] = '页';

$lang['copy_bank_project_help'] = '此复制功能仅限于将所有资料，完整复制一份至下方选择的银行专案里！！！';
$lang['copy_bank_project_error'] = '所选择的银行专案已经有资料了，所以不予许复制！！！';
$lang['select_import_caption_xml'] = '请选取欲汇入的派工档案（XML格式）';
$lang['select_import_caption_excel'] = '请选取欲汇入的读卡机配对档案（EXCEL格式）';
$lang['select_upload_file'] = '选择上传档案';

$lang['login_user_title'] = '綠農創意管理系统';
$lang['login_nimda_title'] = '綠農創意管理系统';
$lang['html_title'] = '綠農創意';
$lang['btn_mno'] = '序号明细';
$lang['btn_delete'] = '删除资料';

//log
$lang['searchData'] = '搜寻';
$lang['acquire_bank_id'] = '银行编号';
$lang['merchant_id'] = '商店编号';
$lang['terminal_id'] = '商店编号';
$lang['create_date'] = '建档日期';
$lang['create_user'] = '建档人员';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['UPDATE_DATETIME'] = '修改日期';
$lang['start_date'] = '起日';
$lang['end_date'] = '迄日';
$lang['FILE_DATE_Start'] = '起日';
$lang['FILE_DATE_End'] = '迄日';
$lang['INITIALIZE_DATE_Start'] = '起日';
$lang['INITIALIZE_DATE_End'] = '迄日';
$lang['ACT'] = '动作';
$lang['RETURN'] = '回传';
$lang['SN'] = '编号';
$lang['BANK_ID'] = '银行编号';
$lang['error_date'] = '起日不可大于迄日!';
$lang['error_date2'] = '请选择日期!!';


$lang['report_sign_General_Manager'] = '总经理';
$lang['report_sign_Vice_President'] = '副总经理';
$lang['report_sign_Unit_Manager'] = '单位主管';
$lang['report_sign_Lister'] = '单位主管';

$lang['total_count'] = '总笔数:';

$lang['select_dealer_class'] = '请选择经销商'; 
$lang['select_subdealer_class'] = '请选择子经销商'; 
$lang['select_operator_class'] = '请选择营运商'; 
$lang['select_suboperator_class'] = '请选择子营运商'; 
$lang['select_tlp_class'] = '请选择租借项目'; 
$lang['select_batteryswaps_class'] = '请选择換電站'; 
$lang['select_vehicle_class'] = '请选择电动机车'; 
$lang['select_member_class'] = '请选择会员'; 

$lang['all_station_count'] = '总換電站数量'; 
$lang['all_battery_count'] = '总电池数量'; 
$lang['all_vehicle_count'] = '总机车数量'; 
$lang['all_station_num'] = '总換電次數'; 
$lang['all_vehicle_km'] = '所有机车的总行驶里程数'; 
$lang['all_emissions'] = '总减少二氧化碳排放量'; 
$lang['all_weChatpay'] = '微信支付的总费用'; 

$lang['battery_status_statistics'] = '电池状态统计表'; 
$lang['battery_power_statistics'] = '电池电量统计表'; 
$lang['power_average'] = '当日各时段和系统平均各时段的換電数量'; 
$lang['monitor_management_online'] = '即时告警';
$lang['so_num'] = '营运商';
$lang['log_battery_info_sb_num'] = '电池換電站';
$lang['sb_num'] = '电池換電站';
$lang['log_date'] = '记录时间';
$lang['bss_token_id'] = '換電站Token ID';
$lang['type'] = '即时告警类别';
$lang['status'] = '处理状态';
$lang['status_desc'] = '处理状态描述';
$lang['view_date'] = '已检视日期';
$lang['view_users'] = '已检视人员资料';
$lang['process_date'] = '处理中日期';
$lang['process_user'] = '处理中人员资料';
$lang['finish_date'] = '已完成日期';
$lang['finish_user'] = '已完成人员资料';
$lang['type_1'] = '火灾';
$lang['type_2'] = '淹水';
$lang['type_3'] = '无轨道可归还';
$lang['type_4_1'] = "前台時間與系統時間相差";
$lang['type_4_2'] = "分鐘以上";
$lang['battery_exchange_ranking'] = '換電站電池單日交換排行';

$lang['bss_id'] = '換電站地点名称<br />（換電站序号）';
$lang['log_date'] = '日期';
$lang['sum_exchangenum'] = '交换次数';

$lang['battery_change_nums'] = '電池交换次数';
$lang['exchanges_average_num'] = '換電站電池平均時段交換次數';
$lang['one_exchanges_average'] = '單一換電站電池平均時段交換次數';

$lang['battery_statusY'] = '充电中';
$lang['battery_statusN'] = '饱电';
$lang['battery_statusS'] = '待機';
$lang['battery_statusE'] = '故障';
$lang['battery_statusV'] = '电动机车';


/* End of file common_lang.php */
/* Location: ./system/language/zh_tw/common_lang.php */

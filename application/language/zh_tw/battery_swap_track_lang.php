<?php
//标题
$lang['battery_swap_track_management'] = '換電轨道管理';

//其他
$lang['battery_swap_track_select_operator'] = '--请选择营运商--';
$lang['battery_swap_track_operator'] = '营运商';
$lang['battery_swap_track_bss_id'] = '換電站序号';
$lang['battery_swap_track_no'] = '換電站轨道编号';
$lang['battery_swap_track_column_park'] = '換電站轨道电池状态';
$lang['battery_swap_track_column_parkY'] = '有电池';
$lang['battery_swap_track_column_parkN'] = '没电池';
$lang['battery_swap_track_column_charge'] = '換電站轨道充电状态';
$lang['battery_swap_track_column_chargeY'] = '充电中';
$lang['battery_swap_track_column_chargeN'] = '充饱电';
$lang['battery_swap_track_column_chargeS'] = '待机';
$lang['battery_swap_track_column_chargeF'] = '故障';
$lang['battery_swap_track_battery_id'] = '电池序号';
$lang['battery_swap_track_status'] = '状态';
$lang['battery_swap_track_statusY'] = '启用';
$lang['battery_swap_track_statusN'] = '停用';
$lang['battery_swap_track_statusE'] = '故障';
$lang['battery_swap_track_bss_id_name'] = '換電站地点名称<br />（換電站序号）';

//栏位
$lang['so_num'] = '营运商s_num';
$lang['sb_num'] = '換電站s_num';
$lang['track_no'] = '換電站轨道编号';
$lang['column_park'] = '換電站轨道电池状态';
$lang['column_charge'] = '換電站轨道充电状态';
$lang['battery_id'] = '电池序号';
$lang['status'] = '換電站轨道状态';

//detail
$lang['log_battery_leave_return_tv_num'] = 'Unit ID';
$lang['log_battery_leave_return_tm_num'] = '会员名称';
$lang['log_battery_leave_return_system_log_date'] = '系统记录时间';
$lang['log_battery_leave_return_leave_DorO_flag'] = '营运商/经销商flag';
//$lang['log_battery_leave_return_leave_do_num'] = '(借)营运商/经销商';
$lang['log_battery_leave_return_leave_do_num'] = '营运商/经销商';
$lang['log_battery_leave_return_leave_dso_num'] = '(借)子营运商/子经销商';
//$lang['log_battery_leave_return_leave_sb_num'] = '租借換電站';
$lang['log_battery_leave_return_leave_sb_num'] = '換電站';
$lang['log_battery_leave_return_battery_user_id'] = 'User ID';
//$lang['log_battery_leave_return_leave_request_date'] = '換電请求日期时间';
$lang['log_battery_leave_return_leave_request_date'] = '租借／归还日期时间';
$lang['log_battery_leave_return_leave_status'] = '租借／归还狀態';
$lang['log_battery_leave_return_leave_coordinate'] = '借电请求时的纬度经度';
$lang['log_battery_leave_return_leave_date'] = '換電实际日期时间';
$lang['log_battery_leave_return_leave_track_no'] = '租借轨道编号';
$lang['log_battery_leave_return_leave_battery_id'] = '租借电池序号';
$lang['log_battery_leave_return_return_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_return_do_num'] = '(还)营运商/经销商';
$lang['log_battery_leave_return_return_dso_num'] = '(还)子营运商/子经销商';
$lang['log_battery_leave_return_return_sb_num'] = '归还換電站';
$lang['log_battery_leave_return_return_request_date'] = '电池归还请求日期时间';
$lang['log_battery_leave_return_return_coordinate'] = '归还请求时的纬度经度';
$lang['log_battery_leave_return_return_date'] = '电池归还实际日期时间';
$lang['log_battery_leave_return_return_track_no'] = '归还轨道编号';
$lang['log_battery_leave_return_return_battery_id'] = '归还电池序号';
$lang['log_battery_leave_return_usage_time'] = '电池使用时间';
$lang['log_battery_leave_return_charge_amount'] = '计算此次扣款金额';
$lang['log_battery_l_r_type'] = '借／还';
$lang['log_battery_leave_return_battery_id'] = '电池序号';

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

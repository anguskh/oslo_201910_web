<?php
//其他
$lang['dealer_management'] = '经销商管理';
//其他
$lang['dealer_s_num'] = '经销商编号';
$lang['dealer_tde01'] = '经销商名称';
$lang['dealer_tde02'] = '型别';
$lang['dealer_tde03'] = '连络人';
$lang['dealer_tde04'] = '连络电话';
$lang['dealer_tde05'] = '行动电话';
$lang['dealer_tde06'] = '地址';
$lang['dealer_tde07'] = 'Email';
$lang['dealer_tde08'] = '平台交易手续费率';
$lang['dealer_tde09'] = '银行转帐代码';
$lang['dealer_tde10'] = '银行转帐帐号';
$lang['dealer_tde11'] = '代处理子营运帐务';
$lang['dealer_status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['s_num'] = '经销商编号';
$lang['tde01'] = '经销商名称';
$lang['tde02'] = '经销商型别';
$lang['tde02_1'] = '直营';
$lang['tde02_2'] = '加盟';
$lang['tde02_3'] = '其它';
$lang['tde03'] = '经销商连络人姓名';
$lang['tde04'] = '经销商连络电话';
$lang['tde05'] = '经销商行动电话';
$lang['tde06'] = '经销商地址';
$lang['tde07'] = 'Email';
$lang['tde08'] = '平台交易手续费率';
$lang['tde09'] = '银行转帐代码';
$lang['tde10'] = '银行转帐帐号';
$lang['tde11'] = '代处理子营运帐务';
$lang['tde11_Y'] = '是';
$lang['tde11_N'] = '否';
$lang['status'] = '经销商状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file dealer_lang.php */
/* Location: ./system/language/zh_tw/dealer_lang.php */

<?php
//其他
$lang['log_alarm_online_inquiry'] = '即时告警通知记录';
$lang['monitor_management_online'] = '即时告警';

$lang['log_alarm_online_battery_id'] = '电池序号';
//栏位
$lang['alarm_online_sn'] = '即时告警通知流水号';
$lang['so_num'] = '营运商';
$lang['sb_num'] = '电池換電站';
$lang['bss_id_name'] = '換電站地点名称<br />（換電站序号）';
$lang['log_date'] = '记录时间';
$lang['bss_token_id'] = '換電站Token ID';
$lang['type'] = '即时告警类别';
$lang['type_1'] = '火灾';
$lang['type_2'] = '淹水';
$lang['type_3'] = '无轨道可归还';
$lang['type_4'] = '前台時間與系統時間相差';
$lang['type_5'] = '取电未关门';
$lang['status'] = '处理状态';
$lang['status_1'] = '未检视';
$lang['status_2'] = '处理中';
$lang['status_3'] = '已完成';
$lang['status_desc'] = '处理状态描述';
$lang['view_date'] = '已检视日期';
$lang['view_users'] = '已检视人员资料';
$lang['process_date'] = '处理中日期';
$lang['process_user'] = '处理中人员资料';
$lang['finish_date'] = '已完成日期';
$lang['finish_user'] = '已完成人员资料';





/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
//其他
$lang['log_battery_leave_return_inquiry'] = '換電归还记录';

//栏位
$lang['log_battery_leave_return_tv_num'] = 'Unit ID';
$lang['log_battery_leave_return_tm_num'] = '会员名称';
$lang['log_battery_leave_return_system_log_date'] = '系统记录时间';
$lang['log_battery_leave_return_leave_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_leave_do_num'] = '(借)营运商/经销商';
$lang['log_battery_leave_return_leave_dso_num'] = '(借)子营运商/子经销商';
$lang['log_battery_leave_return_leave_sb_num'] = '租借換電站';
$lang['log_battery_leave_return_leave_sb_num_name'] = '租借換電站地点名称（換電站序号）';
$lang['log_battery_leave_return_battery_user_id'] = 'User ID';
$lang['log_battery_leave_return_leave_request_date'] = '換電请求日期时间';
$lang['log_battery_leave_return_leave_coordinate'] = '借电请求时的纬度经度';
$lang['log_battery_leave_return_leave_date'] = '換電实际日期时间';
$lang['log_battery_leave_return_leave_track_no'] = '租借轨道编号';
$lang['log_battery_leave_return_leave_battery_id'] = '租借电池序号';
$lang['log_battery_leave_return_leave_status'] = '租借状态';
$lang['log_battery_leave_return_return_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_return_do_num'] = '(还)营运商/经销商';
$lang['log_battery_leave_return_return_dso_num'] = '(还)子营运商/子经销商';
$lang['log_battery_leave_return_return_sb_num'] = '归还換電站';
$lang['log_battery_leave_return_return_sb_num_name'] = '归还換電站地点名称（換電站序号）';
$lang['log_battery_leave_return_return_request_date'] = '电池归还请求日期时间';
$lang['log_battery_leave_return_return_coordinate'] = '归还请求时的纬度经度';
$lang['log_battery_leave_return_return_date'] = '电池归还实际日期时间';
$lang['log_battery_leave_return_return_track_no'] = '归还轨道编号';
$lang['log_battery_leave_return_return_battery_id'] = '归还电池序号';
$lang['log_battery_leave_return_return_status'] = '归还状态';
$lang['log_battery_leave_return_usage_time'] = '电池使用时间';
$lang['log_battery_leave_return_charge_amount'] = '计算此次扣款金额';
$lang['log_battery_trail'] = '路线轨迹';

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
//其他
$lang['lease_price_management'] = '租借项目管理新';
//其他
$lang['lease_price_s_num'] = '租借项目编号';
$lang['lease_price_so_num'] = '营运商';
$lang['lease_price_sso_num'] = '子营运商';
$lang['lease_price_type'] = '租借价目类别';
$lang['lease_price_remark'] = '备注';
$lang['lease_price_status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['s_num'] = '租借项目编号';
$lang['so_num'] = '营运商';
$lang['sso_num'] = '子营运商';
$lang['type'] = '租借价目类别';
$lang['type_1'] = '依照分钟扣除';
$lang['type_2'] = '依照填写分钟范围扣除金额';
$lang['price_rule'] = '计价规则';
$lang['erery_time'] = '每多少分钟';
$lang['every_amount'] = '多少钱';
$lang['remark'] = '备注';
$lang['status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file lease_price_lang.php */
/* Location: ./system/language/zh_tw/lease_price_lang.php */

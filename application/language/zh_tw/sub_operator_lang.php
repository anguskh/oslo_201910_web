<?php
//其他
$lang['sub_operator_management'] = '子营运商管理';
//其他
$lang['sub_operator_s_num'] = '子营运商编号';
$lang['sub_operator_to_num'] = '营运商名称';
$lang['sub_operator_tsop01'] = '子营运商名称';
$lang['sub_operator_tsop02'] = '型别';
$lang['sub_operator_tsop03'] = '连络人';
$lang['sub_operator_tsop04'] = '连络电话';
$lang['sub_operator_tsop05'] = '行动电话';
$lang['sub_operator_tsop06'] = '地址';
$lang['sub_operator_tsop07'] = 'Email';
$lang['sub_operator_tsop08'] = '平台交易手续费率';
$lang['sub_operator_tsop09'] = '银行转帐代码';
$lang['sub_operator_tsop10'] = '银行转帐帐号';
$lang['sub_operator_tsop11'] = '代处理子营运帐务';
$lang['sub_operator_status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['s_num'] = '子营运商编号';
$lang['to_num'] = '营运商';
$lang['tsop01'] = '子营运商名称';
$lang['tsop02'] = '子营运商型别';
$lang['tsop02_1'] = '直营';
$lang['tsop02_2'] = '加盟';
$lang['tsop02_3'] = '其它';
$lang['tsop03'] = '子营运商连络人姓名';
$lang['tsop04'] = '子营运商连络电话';
$lang['tsop05'] = '子营运商行动电话';
$lang['tsop06'] = '子营运商地址';
$lang['tsop07'] = '子营运商E-mail';
$lang['tsop08'] = '银行转帐代码';
$lang['tsop09'] = '银行转帐帐号';
$lang['status'] = '营运商状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file sub_operator_lang.php */
/* Location: ./system/language/zh_tw/sub_operator_lang.php */

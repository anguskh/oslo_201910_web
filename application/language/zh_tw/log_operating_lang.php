<?php
//其他
$lang['log_operating_inquiry'] = '历程记录查询';

//栏位
$lang['log_operating_operating_date'] = '操作时间';
$lang['log_operating_user_sn'] = '使用者名称';
$lang['log_operating_mode'] = '操作类型';
$lang['log_operating_desc'] = '操作后';
$lang['log_operating_sql'] = '指令';
$lang['log_operating_MERCHANT_ID'] = '商店代号';
$lang['log_operating_TERMINAL_ID'] = '端末机代号';
$lang['log_operating_function_name'] = '功能名称';
$lang['log_operating_before_desc'] = '操作前';
$lang['log_operating_ip_address'] = 'IP位址';
$lang['log_operating_status'] = '状态';



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
//标题
$lang['battery_gps_management'] = '电池gps报表';

//栏位
$lang['system_log_date'] = '系統紀錄時間';
$lang['battery_id'] = '电池序号';
$lang['manufacture_date'] = '出厂日期';
$lang['gps_position'] = '电池位置(经纬度)';
$lang['battery_capacity'] = '电池电量';

/* End of file battery_gps_lang.php */
/* Location: ./system/language/zh_tw/battery_gps_lang.php */

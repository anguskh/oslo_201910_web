<?php
//标题
$lang['monitor_management'] = '监控';

//其他
$lang['monitor_province'] = '省份';
$lang['monitor_city'] = '城市';
$lang['monitor_district'] = '区域';
$lang['monitor_bss_id'] = '換電站地点名称<br>(換電站序号)';
$lang['monitor_bss_no'] = '机柜编号';
$lang['monitor_track_no'] = '轨道编号';
$lang['monitor_battery_id'] = '电池ID';
$lang['monitor_battery_temperature'] = '温度';
$lang['monitor_battery_voltage'] = '电压';
$lang['monitor_battery_amps'] = '电流';
$lang['monitor_battery_capacity'] = '电量';
$lang['monitor_status'] = '维修状态(是否已维修)';
$lang['monitor_remark'] = '备注';
$lang['monitor_user_name'] = '维修人员姓名';
$lang['monitor_user_id'] = '维修人员编号';
$lang['monitor_icon'] = '小图示';

$lang['monitor_to_name'] = '营运商';
$lang['monitor_location'] = '站点名称';
$lang['monitor_charge_cycles'] = '循环次数';
$lang['monitor_electrify_time'] = '已充电时间';
$lang['monitor_battery_status'] = '电池状态';

$lang['monitor_log_date'] = '最后一次回报时间';
$lang['monitor_battery_type'] = '电池告警类别';
/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

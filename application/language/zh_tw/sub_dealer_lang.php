<?php
//其他
$lang['sub_dealer_management'] = '子经销商管理';
//其他
$lang['sub_dealer_s_num'] = '子经销商编号';
$lang['sub_dealer_td_num'] = '经销商名称';
$lang['sub_dealer_tsde01'] = '子经销商名称';
$lang['sub_dealer_tsde02'] = '型别';
$lang['sub_dealer_tsde03'] = '连络人';
$lang['sub_dealer_tsde04'] = '连络电话';
$lang['sub_dealer_tsde05'] = '行动电话';
$lang['sub_dealer_tsde06'] = '地址';
$lang['sub_dealer_tsde07'] = 'Email';
$lang['sub_dealer_tsde08'] = '平台交易手续费率';
$lang['sub_dealer_tsde09'] = '银行转帐代码';
$lang['sub_dealer_tsde10'] = '银行转帐帐号';
$lang['sub_dealer_tsde11'] = '代处理子营运帐务';
$lang['sub_dealer_status'] = '状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['s_num'] = '子经销商编号';
$lang['td_num'] = '经销商';
$lang['tsde01'] = '子经销商名称';
$lang['tsde02'] = '子经销商型别';
$lang['tsde02_1'] = '直营';
$lang['tsde02_2'] = '加盟';
$lang['tsde02_3'] = '其它';
$lang['tsde03'] = '子经销商连络人姓名';
$lang['tsde04'] = '子经销商连络电话';
$lang['tsde05'] = '子经销商行动电话';
$lang['tsde06'] = '子经销商地址';
$lang['tsde07'] = '子经销商E-mail';
$lang['tsde08'] = '平台交易手续费率';
$lang['tsde09'] = '银行转帐代码';
$lang['tsde10'] = '银行转帐帐号';
$lang['tsde11'] = '代处理子营运帐务';
$lang['tsde11_Y'] = '是';
$lang['tsde11_N'] = '否';
$lang['status'] = '经销商状态';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file sub_dealer_lang.php */
/* Location: ./system/language/zh_tw/sub_dealer_lang.php */

<?php
//标题
$lang['exchange_nobattery_management'] = '換電站无可用电池总时数';

//栏位
$lang['bss_id'] = '換電站地点名称<br />
（換電站序号）';
$lang['log_date_start'] = '开始时间';
$lang['log_date_end'] = '结束时间';
$lang['minutes'] = '天　时:分:秒';

/* End of file exchange_nobattery_lang.php */
/* Location: ./system/language/zh_tw/exchange_nobattery_lang.php */

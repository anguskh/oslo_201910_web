<?php
//标题
$lang['bss_manage_management'] = 'BSS软体管理';

//栏位
$lang['bss_content'] = '档案说明';
$lang['bss_file'] = '档案';
$lang['bss_version_date'] = '版本日期';
$lang['bss_version_no'] = '版本号码';
$lang['bss_checksum'] = '檔案checksum';
$lang['up_flag'] = '立即更新';
$lang['total_num'] = '租借站总数';
$lang['notify'] = '已通知<br>未通知';
$lang['update'] = '已更新<br>未更新';

//BSS軟體管理下載明細列表
$lang['sys_date'] = 'BSS软体版本<br>建立时间';
$lang['bss_id'] = '租借站序号';
$lang['notify_date'] = '首次通知时间';
$lang['updating_date'] = '更新时间';

/* End of file bss_manage_lang.php */
/* Location: ./system/language/zh_tw/bss_manage_lang.php */

<?php
//其他
$lang['group_management'] = '群组管理';
$lang['group_input_name'] = '请输入群组名称';
$lang['group_name_duplicate'] = '群组名称不可重复';
$lang['group_in_use'] = '有使用者正在使用';
$lang['group_login_side'] = '登入权限';
$lang['group_select_user_group'] = '请勾选使用者群组';
$lang['group_msg'] = '使用者管理需勾选使用者群组!';
$lang['group_main_flag'] = '首页捷径';

//log
$lang['group_user_group'] = '使用者群组';
$lang['group_menu_sn'] = '选单编号';
$lang['group_group_sn'] = '群组编号';
$lang['to_flag'] = '营运商';
$lang['card_flag'] = '卡片';


//栏位
$lang['group_group_name'] = '群组名称';
$lang['group_status'] = '群组状态';
$lang['group_approve_terminal'] = '覆核端末机';


/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */

<?php
//其他
$lang['log_swap_track_warring_inquiry'] = '換電站即时告警';
$lang['log_swap_track_warring_battery_id'] = '电池序号';
//栏位
$lang['alarm_online_sn'] = '即时告警通知流水号';
$lang['sb_num'] = '換電站';
$lang['bss_id'] = '換電站';
$lang['bss_id_name'] = '換電站地点名称<br />（換電站序号）';

$lang['track_no'] = '轨道';
$lang['log_date'] = '记录时间';
$lang['battery_id'] = '电池序号';
$lang['type'] = '告警状态';
$lang['type_E'] = '故障';
$lang['type_N'] = '停用';

$lang['status'] = '处理状态';
$lang['status_1'] = '未检视';
$lang['status_2'] = '处理中';
$lang['status_3'] = '已完成';
$lang['status_desc'] = '处理状态描述';
$lang['view_date'] = '已检视日期';
$lang['view_users'] = '已检视人员资料';
$lang['process_date'] = '处理中日期';
$lang['process_user'] = '处理中人员资料';
$lang['finish_date'] = '已完成日期';
$lang['finish_user'] = '已完成人员资料';





/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

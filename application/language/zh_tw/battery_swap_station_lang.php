<?php
//标题
$lang['battery_swap_station_management'] = '換電站管理';

//其他
$lang['battery_swap_station_select_operator'] = '--请选择营运商--';
$lang['battery_swap_station_operator'] = '营运商';
$lang['battery_swap_station_note'] = '备注';
$lang['battery_swap_station_bss_id'] = '換電站序号';
$lang['battery_swap_station_bss_id_name'] = '換電站地点名称<br />（換電站序号）';
$lang['battery_swap_station_bss_token'] = '換電站Token ID';
$lang['battery_swap_station_ip_type'] = '換電站IP类别';
$lang['battery_swap_station_ip'] = '換電站IP';
$lang['battery_swap_station_last_online'] = '最后一次连线回报时间';
$lang['battery_swap_station_version_no'] = '版本号码';
$lang['battery_swap_station_version_date'] = '版本日期';
$lang['battery_swap_station_track_quantity'] = '轨道数';
$lang['battery_swap_station_status'] = '状态';
$lang['battery_swap_station_statusY'] = '启用';
$lang['battery_swap_station_statusN'] = '停用';
$lang['battery_swap_station_statusD'] = '删除';
$lang['battery_swap_station_latitude'] = '纬度';
$lang['battery_swap_station_longitude'] = '经度';
$lang['battery_swap_station_virtual_map'] = '实景图';
$lang['battery_swap_station_location'] = '換電站地点名称';
$lang['battery_swap_station_exchange_num'] = '交换次数';
$lang['battery_swap_station_canuse_battery_num'] = '可租借<br>电池数量';
$lang['battery_swap_station_map'] = '地图';
$lang['battery_swap_station_monitor'] = '即时监控';
$lang['battery_swap_station_station_no'] = '換電站版本号码';

//栏位
$lang['so_num'] = '营运商';
$lang['note'] = '备注';
$lang['bss_id'] = '換電站序号';
$lang['bss_token'] = '換電站Token ID';
$lang['ip_type'] = '換電站IP类别';
$lang['ip_type_F'] = '固定IP';
$lang['ip_type_C'] = '变动IP';
$lang['ip'] = '換電站IP';

$lang['last_online'] = '最后一次连线回报时间';
$lang['version_no'] = '換電站版本号码';
$lang['version_date'] = '換電站更版日期';
$lang['track_quantity'] = '換電站轨道数';
$lang['status'] = '換電站状态';
$lang['city'] = '县市'; 
$lang['battery_swap_station_bss_no'] = '机柜编号'; 
$lang['sim_no'] = 'SIM卡卡号'; 
						

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

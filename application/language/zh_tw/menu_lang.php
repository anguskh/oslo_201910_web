<?php
//其他
$lang['menu_management'] = '选单管理';
$lang['menu_parent_name'] = '上层选单名称';
$lang['menu_select_parent_name'] = '请选择上层选单名称';
$lang['menu_select_second_name'] = '请选择第二层选单名称';
$lang['menu_input_name'] = '请输入选单中文名称';
$lang['menu_input_en_name'] = '请输入选单英文名称';
$lang['menu_input_directory'] = '请输入选单路径';
$lang['menu_input_image'] = '请输入选单图档名称';
$lang['menu_input_sequence'] = '请输入顺序';
$lang['menu_status2'] = '选单状态';
$lang['icon_class'] = 'icon_class';

//栏位
$lang['menu_menu_sn'] = '选单编号';
$lang['menu_parent_menu_sn'] = '上层选单编号';
$lang['menu_second_menu_sn'] = '第二层选单名称';
$lang['menu_name'] = '选单中文名称';
$lang['menu_menu_name_english'] = '选单英文名称';
$lang['menu_menu_name'] = '选单名称';
$lang['menu_menu_directory'] = '选单路径';
$lang['menu_menu_image'] = '选单图档名称';
$lang['menu_menu_sequence'] = '选单排列顺序';
$lang['menu_login_side_display'] = '登入显示';
$lang['menu_access_control'] = '预设功能';
$lang['menu_status'] = '状态';
$lang['menu_menu_content'] = '选单说明';

//控制权限
$lang['menu_access_control'] = '操作权限';


/* End of file menu_lang.php */
/* Location: ./system/language/zh_tw/menu_lang.php */

<?php
//标题
$lang['exchange_onedayavg_management'] = '換電站每日換電次数';

//栏位
$lang['bss_id'] = '換電站地点名称<br />（換電站序号）';
$lang['log_date'] = '日期';
$lang['sum_exchangenum'] = '交换次数';

/* End of file exchange_average_lang.php */
/* Location: ./system/language/zh_tw/exchange_average_lang.php */

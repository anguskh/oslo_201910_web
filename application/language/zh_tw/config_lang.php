<?php
//其他
$lang['config_management'] = '参数管理';
$lang['config_please_input'] = '请输入';
$lang['config_allow_ip_segment_msg'] = '当不输入任何网段, 预设为任何IP都可进入!';
$lang['config_allow_pc_name'] = '当不输入, 预设可进入任何登入页面!';
$lang['config_P'] = '正式参数';
$lang['config_T'] = '测试参数';

//栏位
$lang['config_config_sn'] = '参数流水号';
$lang['config_config_desc'] = '参数名称';
$lang['config_config_set'] = '参数值';
$lang['config_after_desc'] = '参数单位';


/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */

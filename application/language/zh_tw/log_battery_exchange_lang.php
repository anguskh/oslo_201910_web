<?php
//其他
$lang['log_battery_exchange_inquiry'] = '換電记录查询';

//栏位
$lang['log_battery_exchange_so_num'] = '营运商';
$lang['log_battery_exchange_sb_num'] = '电池換電站';
$lang['log_battery_exchange_sb_num_name'] = '換電站地点名称<br />（換電站序号）';
$lang['log_battery_exchange_sv_num'] = '电动机车流水号';
$lang['log_battery_exchange_vehicle_code'] = '机车编号';
$lang['log_battery_exchange_vehicle_user_id'] = '车辆使用者编号';
$lang['log_battery_exchange_no_track_no'] = '换回电池的轨道编号';
$lang['log_battery_exchange_no_battery_pack_sn'] = '换回电池组流水号';
$lang['log_battery_exchange_yes_track_no'] = '换出电池的轨道编号';
$lang['log_battery_exchange_yes_battery_pack_sn'] = '换出电池组流水号';
$lang['log_battery_exchange_date'] = '交换日期时间';
$lang['log_battery_exchange_exchange_type'] = '交换类别';


/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

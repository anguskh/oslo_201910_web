<?php
//标题
$lang['battery_management'] = '电池管理';
$lang['battery_management_upload'] = '电池管理-确认汇入';
$lang['battery_detail_management'] = '电池管理-租借历程记录';

//其他
$lang['battery_status0'] = '正常(充电中)';
$lang['battery_status0_1'] = '正常';
$lang['battery_status1'] = '已满电';
$lang['battery_status2'] = '故障';
$lang['battery_status3'] = '维修';
$lang['battery_status4'] = '报废';
$lang['battery_status5'] = '电池失窃';
$lang['battery_status6'] = '仓库';

//栏位

$lang['battery_DorO_flag'] = '厂商类型';
$lang['dealer'] = '经销商';
$lang['operator'] = '营运商';
$lang['battery_dealer'] = '经销商';
$lang['battery_operator'] = '营运商';
$lang['battery_M30'] = '车辆';
$lang['battery_id'] = '电池序号';
$lang['battery_manufacture_date'] = '出厂日期';
$lang['battery_position'] = '电池位置';
$lang['battery_exchange_count'] = '換電<br />累积次数';
$lang['battery_status'] = '电池状态';
$lang['battery_map'] = '地图';
$lang['last_report_datetime'] = '最后一次回报时间';
$lang['bss_id'] = '換電站';
$lang['bss_id_name'] = '換電站地点名称<br />（換電站序号）';
$lang['track_no'] = '轨道';
$lang['sys_no'] = '版本号码';
$lang['battery_recording'] = '租借历程';
$lang['battery_station_position'] = '基站位置<br />(MCC，MNC，LAC，CELLID)';
$lang['battery_gps_manufacturer'] = 'GPS 制造商';
$lang['battery_gps_version'] = 'GPS 固件版本';

//detail
$lang['log_battery_leave_return_tv_num'] = 'Unit ID';
$lang['log_battery_leave_return_tm_num'] = '会员名称';
$lang['log_battery_leave_return_system_log_date'] = '系统记录时间';
$lang['log_battery_leave_return_leave_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_leave_do_num'] = '(借)营运商/经销商';
$lang['log_battery_leave_return_leave_dso_num'] = '(借)子营运商/子经销商';
$lang['log_battery_leave_return_leave_sb_num'] = '租借換電站';
//$lang['log_battery_leave_return_leave_sb_num_name'] = '租借換電站';
$lang['log_battery_leave_return_leave_sb_num_name'] = '租借換電站地点名称（換電站序号）';
$lang['log_battery_leave_return_battery_user_id'] = 'User ID';
$lang['log_battery_leave_return_leave_request_date'] = '換電请求日期时间';
$lang['log_battery_leave_return_leave_status'] = '租借状态';
$lang['log_battery_leave_return_leave_coordinate'] = '借电请求时的纬度经度';
$lang['log_battery_leave_return_leave_date'] = '換電实际日期时间';
$lang['log_battery_leave_return_leave_track_no'] = '租借轨道编号';
$lang['log_battery_leave_return_leave_battery_id'] = '租借电池序号';
$lang['log_battery_leave_return_return_DorO_flag'] = '营运商/经销商flag';
$lang['log_battery_leave_return_return_do_num'] = '(还)营运商/经销商';
$lang['log_battery_leave_return_return_dso_num'] = '(还)子营运商/子经销商';
$lang['log_battery_leave_return_return_sb_num'] = '归还換電站';
//$lang['log_battery_leave_return_return_sb_num_name'] = '归还換電站';
$lang['log_battery_leave_return_return_sb_num_name'] = '归还換電站地点名称（換電站序号）';
$lang['log_battery_leave_return_return_request_date'] = '电池归还请求日期时间';
$lang['log_battery_leave_return_return_coordinate'] = '归还请求时的纬度经度';
$lang['log_battery_leave_return_return_date'] = '电池归还实际日期时间';
$lang['log_battery_leave_return_return_track_no'] = '归还轨道编号';
$lang['log_battery_leave_return_return_status'] = '归还状态';
$lang['log_battery_leave_return_return_battery_id'] = '归还电池序号';
$lang['log_battery_leave_return_usage_time'] = '电池使用时间';
$lang['log_battery_leave_return_charge_amount'] = '计算此次扣款金额';
$lang['log_battery_trail'] = '路线轨迹';

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

<?php
//其他
$lang['log_driving_inquiry'] = '电池充放电历程查询';

$lang['log_driving_battery_id'] = '电池序号';
//栏位
$lang['td_num'] = '经销商';
$lang['tsd_num'] = '子经销商';
$lang['to_num'] = '营运商';
$lang['tso_num'] = '子营运商';
$lang['do_num'] = '经销商/营运商';
$lang['dso_num'] = '子经销商/子营运商';
$lang['tv_num'] = '车辆流水号';
$lang['tm_num'] = '会员流水号';
$lang['system_log_date'] = '系统记录时间';
$lang['driving_date'] = '行车记录时间';
$lang['unit_id'] = 'Unit ID';
$lang['unit_name'] = '名称';
$lang['unit_status'] = '状态';
$lang['utc_date'] = 'UTC日期';
$lang['utc_time'] = 'UTC时间';
$lang['latitude'] = 'GPS纬度';
$lang['longitude'] = 'GPS经度';
$lang['speed'] = '时速';
$lang['angle'] = '角度';
$lang['gps_satellite'] = 'GPS卫星数';
$lang['event_id'] = '事件编号';
$lang['return_type'] = '回报类型';
$lang['lease_status'] = '租借状态';
$lang['engine_status'] = '启动状态';
$lang['battery_voltage'] = '电池电压';
$lang['battery_amps'] = '电池电流';
$lang['battery_temperature'] = '电池温度';
$lang['environment_temperature'] = '环境温度';
$lang['battery_capacity'] = '电池电量';
$lang['power_bank_voltage'] = 'PowerBank电压';
$lang['in_out_status'] = 'I/O状态';
$lang['battery_station_position'] = '基站位置';
$lang['battery_gps_manufacturer'] = 'GPS制造商';
$lang['battery_gps_version'] = 'GPS固件版本';



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
//其他
$lang['login_user_title'] = '綠農創意管理系统';
$lang['login_nimda_title'] = '綠農創意管理系统';
$lang['login_advise'] = '建议您使用 <a href="http://www.google.com/chrome?hl=zh-TW&amp;brand=CHMI">Google Chrome</a> 以取得较佳的荧幕视野、较亲和的使用者介面、较快速的内容呈视。';
$lang['login_web_foreground'] = '綠農創意管理系统-前台';
$lang['login_web_background'] = '綠農創意管理系统';
$lang['login_id'] = '帐号';
$lang['login_password'] = '密码';
$lang['login_button'] = '登入';
$lang['submit_button'] = '送出';
$lang['security_Input'] = '验证码';
$lang['try_another'] = '换一个';
$lang['forget_password'] = '忘记密码';
$lang['forget_email'] = '输入Email';
$lang['return_to_login'] = '回登入页';
$lang['login_send'] = '送出';
$lang['login_email_wrong_email_address'] = '信箱不正确, 请重新输入!';
$lang['login_email_right_email_address'] = '新密码已送至信箱！';

$lang['login_email_subject'] = "密码查询";
$lang['login_email_error1'] = "信件无法寄送, 请联络";
$lang['login_email_error2'] = "系统管理员, 确认信箱设定";
$lang['login_email_body1'] = "已传送新密码，请登入后修改密码!";
$lang['login_email_body2'] = "帐号";
$lang['login_email_body3'] = "新密码";
$lang['login_email_body4'] = "网址";
$lang['login_account_disabled'] = "此帐号已被停用!!";
$lang['login_verify_code_error'] = "验证码错误!!";
$lang['login_ip_error'] = "登入IP错误!!";
$lang['login_account_password_error'] = "帐号或密码错误!!";
$lang['login_account_lock_out'] = "此帐号已无法再次登入, 详情请洽管理员!";
$lang['login_remain'] = "剩余尝试次数";
$lang['login_count'] = "次";

$lang['error_account_cant_login_again'] = '帐号或密码错误!!\n此帐号已无法再次登入, 详情请洽管理员!';
$lang['error_account_disabled'] = '此帐号被停用!!';
$lang['error_account'] = '帐号或密码错误!!';
$lang['error_ip'] = 'IP不符合网段设定!!';
$lang['login_retry_cnt'] = '尝试次数 ';


/* End of file bank_lang.php */
/* Location: ./system/language/zh_tw/bank_lang.php */

<?php
//标题
$lang['bss_advertising_management'] = 'BSS广告管理';

//栏位
$lang['advertising_explain'] = '说明';
$lang['advertising_type'] = '广告类型';
$lang['advertising_upload_file'] = '上传档案';
$lang['advertising_updating_date'] = '更新日期';
$lang['total_num'] = '租借站总数';
$lang['enable'] = "啟用";
$lang['disable'] = "停用";


//BSS廣告管理下載明細列表
$lang['advertising_address'] = '网址';
$lang['notify'] = '已通知<br>未通知';
$lang['advertising_status'] = "狀態";

/* End of file bss_manage_lang.php */
/* Location: ./system/language/zh_tw/bss_manage_lang.php */

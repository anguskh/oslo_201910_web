<?php
//其他
$lang['operator_order_management'] = '营运商预购次数管理';
//其他
$lang['operator_order_s_num'] = '子营运商编号';
$lang['operator_order_to_num'] = '营运商名称';
$lang['operator_order_top08'] = '预购次数';
$lang['operator_card_count'] = '卡片张数';
$lang['operator_order_status'] = '状态';
$lang['operator_order_detail'] = '明细';
$lang['operator_order_add_num'] = '加购次数';
$lang['operator_order_before_total_lave_num'] = '加购前总剩馀次数';
$lang['operator_order_create_date'] = '加购日期';


$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['delete_ip'] = '删除IP';
//栏位
$lang['s_num'] = '子营运商编号';
$lang['to_num'] = '营运商';
$lang['top08'] = '预购次数';
$lang['lease_type'] = '预购类型';
$lang['lease_type_1'] = '预购';
$lang['lease_type_2'] = '月结';
$lang['card_num'] = '卡片张数';
$lang['lease_price'] = '单次交易金额';
$lang['create_user'] = '建档人员';
$lang['create_date'] = '建档日期';
$lang['create_ip'] = '建档IP';
$lang['update_user'] = '修改人员';
$lang['update_date'] = '修改日期';
$lang['update_ip'] = '修改IP';
$lang['delete_user'] = '删除人员';
$lang['delete_date'] = '删除日期';
$lang['enable'] = '启用';
$lang['disable'] = '停用';

/* End of file operator_order_lang.php */
/* Location: ./system/language/zh_tw/operator_order_lang.php */

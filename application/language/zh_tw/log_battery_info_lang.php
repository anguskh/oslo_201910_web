<?php
//其他
$lang['log_battery_info_inquiry'] = '电池充电次数查询';

//栏位

$lang['log_battery_info_do_num'] = '经销商/营运商';
$lang['log_battery_info_so_num'] = '营运商';
$lang['log_battery_info_sb_num'] = '电池換電站';
$lang['log_battery_info_bss_id_name'] = '換電站地点名称<br />（換電站序号）';
$lang['log_battery_info_log_date'] = '记录时间';
$lang['log_battery_info_track_no'] = '轨道编号';
$lang['log_battery_info_battery_id'] = '电池序号';
$lang['log_battery_info_vehicle_user_id'] = '车辆使用者编号';
$lang['log_battery_info_battery_voltage'] = '电池电压';
$lang['log_battery_info_battery_cell_status'] = '电池芯状态';
$lang['log_battery_info_battery_amps'] = '电池电流';
$lang['log_battery_info_battery_capacity'] = 'SOC';
$lang['log_battery_info_battery_temperature'] = '电池温度';
$lang['log_battery_info_charge_cycles'] = '充电次数';
$lang['log_battery_info_electrify_time'] = '已充电时间';
$lang['log_battery_info_status'] = '电池状态';

/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

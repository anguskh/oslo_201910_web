<?php
//标题
$lang['exchange_statistics_management'] = '電池盤點';
//栏位
$lang['battery_id'] = '电池序号';
$lang['manufacture_date'] = '出厂日期';
$lang['gps_position'] = '电池位置(经纬度)';
$lang['battery_capacity'] = '电池电量';

/* End of file exchange_average_lang.php */
/* Location: ./system/language/zh_tw/exchange_average_lang.php */

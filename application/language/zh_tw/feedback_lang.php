<?php
//其他
$lang['feedback_management'] = '客户反馈管理';
//其他
$lang['feedback_s_num'] = '系统自动编号'; 
$lang['feedback_tm_num'] = '会员'; 
$lang['feedback_log_date'] = '客户反馈日期时间'; 
$lang['feedback_question_item'] = '问题'; 
$lang['feedback_question_additional'] = '问题补充说明'; 
$lang['feedback_coordinate'] = '客户反馈时的经纬度'; 
$lang['feedback_status'] = '客户反馈处理结果'; 

//栏位
$lang['s_num'] = '系统自动编号'; 
$lang['tm_num'] = '会员'; 
$lang['log_date'] = '客户反馈日期时间'; 
$lang['question_item'] = '问题编号'; 
$lang['question_item_1'] = '取电失败'; 
$lang['question_item_2'] = '发现故障'; 
$lang['question_item_3'] = '违规停车'; 
$lang['question_item_4'] = '其它问题'; 
$lang['question_additional'] = '问题补充说明'; 
$lang['status'] = '客户反馈处理结果'; 
$lang['status_N'] = '尚未处理'; 
$lang['status_P'] = '处理中'; 
$lang['status_S'] = '处理完毕';
$lang['coordinate'] = '客户反馈时的经纬度'; 

/* End of file feedback_lang.php */
/* Location: ./system/language/zh_tw/feedback_lang.php */

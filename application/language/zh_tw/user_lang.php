<?php
//其他
$lang['user_management'] = '帐号管理';
$lang['employee_management'] = '员工基本资料';
$lang['user_input_account'] = '请输入登入帐号';
$lang['user_input_name'] = '请输入姓名';
$lang['user_pwd'] = '密码';
$lang['user_input_pwd'] = '请输入登入密码';
$lang['user_confirm_pwd'] = '确认密码';
$lang['user_input_confirm_pwd'] = '请输入相同登入密码';
$lang['user_input_mobile'] = '请输入行动电话';
$lang['user_input_phone'] = '请输入电话';
$lang['user_input_email'] = '请输入电子邮件信箱';
$lang['user_bank'] = '选择银行';
$lang['user_bank_help'] = '-- 请选择银行 --&nbsp;';
$lang['user_merchant'] = '选择商店';
$lang['user_merchant_help'] = '-- 请选择商店 --&nbsp;';
$lang['user_select_group'] = '请选择使用者群组';
$lang['user_status2'] = '使用者状态';
$lang['user_login_force_pwd'] = '强制更改密码';
$lang['user_repassword'] = '回复密码';
$lang['user_log_change_password'] = '<font color=blue>[更改密码]</font>';
$lang['user_log_target'] = '<font color=blue>[目标]</font>';
$lang['user_log_content'] = '<font color=blue>[内容]</font>';
$lang['user_log_user_id'] = '帐号:';
$lang['user_log_pwd'] = '密码:';
$lang['user_label_basic'] = '基本资料';
$lang['user_label_company'] = '公司相关';
$lang['user_label_group_access'] = '群组与权限';

$lang['user_label_emp_ename'] = '英文姓名';
$lang['user_label_dept_no'] = '部门';
$lang['user_input_dept_no'] = '请输入部门';
$lang['user_label_emp_id'] = '员工编号';
$lang['user_label_birth_date'] = '出生日期';
$lang['user_label_hire_date'] = '雇用日期';
$lang['user_label_location'] = '办公室地点';
$lang['user_label_fax'] = '传真';
$lang['user_label_home_tel'] = '住家电话';
$lang['user_label_district'] = '国家地区';
$lang['user_label_city'] = '乡镇县市';
$lang['user_label_zip_code'] = '邮递区号';
$lang['user_label_address'] = '地址';
$lang['user_label_urgent_name'] = '紧急连络人';
$lang['user_label_urgent_tel'] = '紧急连络电话';
$lang['user_label_ID_No'] = '身分证字号';
$lang['user_label_mobile2'] = '行动电话2';
$lang['user_label_position'] = '职位';
$lang['user_label_charge_area'] = '负责区域';
$lang['user_label_extension'] = '分机';
$lang['user_label_status'] = '状态';
$lang['user_label_fulltime'] = '正职';
$lang['user_label_onduty'] = '在职';
$lang['user_label_install_per'] = '装机人员';
$lang['user_label_isp_per'] = '采购人员';
$lang['user_label_stock_per'] = '仓管人员';
$lang['user_label_sale_per'] = '销售人员';
$lang['user_label_contract_per'] = '委外签约人员';
$lang['user_label_contract_per_no'] = '委外代号';
$lang['user_label_remark'] = '备注';


//栏位
$lang['user_user_id'] = '帐号';
$lang['user_user_name'] = '使用者';
$lang['user_sex'] = '性别';
$lang['user_mobile'] = '行动电话';
$lang['user_phone'] = '电话';
$lang['user_email'] = '电子邮件';
$lang['user_group_sn'] = '使用者群组';
$lang['user_comment_notes'] = '银行';
$lang['user_merchant_name_chinese'] = '商店';
$lang['user_status'] = '状态';
$lang['user_access'] = '权限控管';
$lang['to_num'] = '营运商';
$lang['tso_num'] = '子营运商';
$lang['user_view_area'] = '可查看租借站省份';


/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

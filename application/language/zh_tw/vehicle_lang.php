<?php
//标题
$lang['vehicle_management'] = '车辆管理';
$lang['vehicle_management_upload'] = '车辆管理-确认汇入';

//其他
$lang['vehicle_use_typeN'] = '一般车';
$lang['vehicle_use_typeS'] = '样品车(计费不扣款)';
$lang['vehicle_use_typeP'] = '样品车(计费扣款)';

$lang['vehicle_status1'] = '托售';
$lang['vehicle_status2'] = '已售';
$lang['vehicle_status3'] = '待租';
$lang['vehicle_status4'] = '已租';
$lang['vehicle_status5'] = '遗失';
$lang['vehicle_status6'] = '报废';
$lang['vehicle_status7'] = '呆帐';

$lang['vehicle_chargeY'] = '充电中';
$lang['vehicle_chargeN'] = '充饱电';
$lang['vehicle_chargeS'] = '待机';

$lang['vehicle_typeB'] = '电动自行车';
$lang['vehicle_typeM'] = '电动机车';

//栏位

$lang['DorO_flag'] = '厂商类型';
$lang['vehicle_dealer'] = '经销商';
$lang['vehicle_sub_dealer'] = '子经销商';
$lang['vehicle_operator'] = '营运商';
$lang['vehicle_sub_operator'] = '子营运商';
$lang['vehicle_unit_id'] = 'Unit ID';
$lang['vehicle_code'] = '车架号码';
$lang['vehicle_manufacture_date'] = '出厂日期';
$lang['vehicle_license_plate_number'] = '车牌号码';
$lang['vehicle_engine_number'] = '引擎号码';
$lang['vehicle_car_machine_id'] = '车机编号';
$lang['vehicle_model'] = '车款型号';
$lang['vehicle_color'] = '车款颜色';
$lang['vehicle_4g_ip'] = '机车4G IP';
$lang['vehicle_bluetooth_name'] = '机车蓝芽名称';
$lang['vehicle_bluetooth_mac'] = '机车蓝芽MAC';
$lang['vehicle_lora_sn'] = 'LoRa编号';
$lang['vehicle_user_id'] = '车辆使用者编号';
$lang['vehicle_type'] = '车辆类别';
$lang['vehicle_use_type'] = '使用类别';
$lang['vehicle_charge'] = '车辆充电状态';
$lang['vehicle_charge_count'] = '车辆电池充电次数';
$lang['vehicle_status'] = '车辆状态';
$lang['vehicle_latitude'] = '车辆当前纬度';
$lang['vehicle_longitude'] = '车辆当前经度';

/* End of file vehicle_lang.php */
/* Location: ./system/language/zh_tw/vehicle_lang.php */

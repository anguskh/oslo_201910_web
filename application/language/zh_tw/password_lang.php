<?php

$lang['password_current'] = '旧密码';
$lang['password_input_current'] = '请输入旧密码';
$lang['password_current_error'] = '旧密码输入错误';
$lang['password_change'] = '更改密码';
$lang['password_new'] = '新密码';
$lang['password_input_new'] = '请输入新密码';
$lang['password_new_confirm'] = '确认新密码';
$lang['password_input_new_confirm'] = '请输入相同的新密码';
$lang['password_first_login'] = '首次登入请更换密码';


/* End of file password_lang.php */
/* Location: ./system/language/zh_tw/password_lang.php */

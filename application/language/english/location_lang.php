<?php
//其他
$lang['location_management'] = '办公室地点';
$lang['location_label_location_no'] = '办公室代号';
$lang['location_input_location_no'] = '请输入办公室代号';
$lang['location_label_location_name'] = '办公室地点';
$lang['location_input_location_name'] = '请输入办公室地点';


//栏位
$lang['location_location_no'] = '办公室代号';
$lang['location_location_name'] = '办公室地点';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

<?php
//其他
$lang['menu_management'] = 'Menu Management';
$lang['menu_select_parent_name'] = 'Select parent menu name';
$lang['menu_select_second_name'] = 'Select second menu name';
$lang['menu_input_name'] = 'Please input menu chinese name';
$lang['menu_input_en_name'] = 'Please input menu english name';
$lang['menu_input_directory'] = 'Please input menu path';
$lang['menu_input_image'] = 'Please input menu image';
$lang['menu_input_sequence'] = 'Input sequence';
$lang['menu_status2'] = 'Menu Status';

//栏位
$lang['menu_menu_sn'] = 'Menu SN';
$lang['menu_parent_menu_sn'] = 'Parent Menu SN';
$lang['menu_parent_name'] = 'Parent Menu Name';
$lang['menu_second_menu_sn'] = 'Parent Second Name';
$lang['menu_name'] = 'Menu Chinese Name';
$lang['menu_menu_name_english'] = 'Menu English Name';
$lang['menu_menu_directory'] = 'Menu Path';
$lang['menu_menu_image'] = 'Menu Image';
$lang['menu_menu_sequence'] = 'Menu Sequence';
$lang['menu_login_side_display'] = 'login side display';
$lang['menu_access_control'] = 'default Control';
$lang['menu_status'] = 'Status';

//控制权限
$lang['menu_access_control'] = 'Access Control';

/* End of file menu_lang.php */
/* Location: ./system/language/english/menu_lang.php */

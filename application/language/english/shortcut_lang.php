<?php
//其他
$lang['shortcut_management'] = 'Shortcut Management';
$lang['shortcut_select_menu'] = 'Select shortcut menu';
$lang['shortcut_input_image'] = 'Input shortcut image name';
$lang['shortcut_input_sequence'] = 'Input Sequence';

//栏位
$lang['shortcut_menu_name'] = 'Menu Name';
$lang['shortcut_shortcut_image_name'] = 'Shortcut Image Name';
$lang['shortcut_shortcut_sequence'] = 'Shortcut Sequence';
$lang['shortcut_status'] = 'Shortcut Status';


/* End of file shortcut_lang.php */
/* Location: ./system/language/english/shortcut_lang.php */

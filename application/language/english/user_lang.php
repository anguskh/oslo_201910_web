<?php
//其他
$lang['user_management'] = 'User Management';
$lang['user_input_account'] = 'Input login account';
$lang['user_input_name'] = 'Input name';
$lang['user_password'] = 'Password';
$lang['user_input_password'] = 'Input passsword';
$lang['user_confirm_password'] = 'Confirm Password';
$lang['user_input_confirm_password'] = 'Input confirm password';
$lang['user_input_mobile'] = 'Input mobile phone';
$lang['user_input_phone'] = 'Input company phone';
$lang['user_input_email'] = 'Please input email';
$lang['user_bank'] = 'Select Bank';
$lang['user_bank_help'] = '-- Please Select Bank --&nbsp;';
$lang['user_merchant'] = 'Select Merchant';
$lang['user_merchant_help'] = '-- Please Select Merchant --&nbsp;';
$lang['user_select_group'] = 'Select user group';
$lang['user_status2'] = 'User Status';
$lang['user_login_force_pwd'] = 'Force change password';
$lang['user_repassword'] = 'Restore Password';

$lang['user_log_change_password'] = '<font color=blue>[Change Password]</font>';
$lang['user_log_target'] = '<font color=blue>[Target]</font>';
$lang['user_log_content'] = '<font color=blue>[Content]</font>';
$lang['user_log_user_id'] = 'User ID:';
$lang['user_log_password'] = 'Password:';
$lang['user_label_basic'] = '基本资料';
$lang['user_label_group_access'] = '群组与权限';

//栏位
$lang['user_user_id'] = 'Account';
$lang['user_user_name'] = 'Name';
$lang['user_sex'] = 'Gender';
$lang['user_mobile'] = 'Mobile Phone';
$lang['user_phone'] = 'Company Phone';
$lang['user_email'] = 'E-Mail';
$lang['user_group_sn'] = 'User Group';
$lang['user_comment_notes'] = 'bank';
$lang['user_merchant_name_chinese'] = 'merchant';
$lang['user_status'] = 'Status';
$lang['user_access'] = 'Access';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

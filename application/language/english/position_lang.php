<?php
//其他
$lang['position_management'] = '部门基本资料';
$lang['position_label_position_no'] = '职称代号';
$lang['position_input_position_no'] = '请输入职称代号';
$lang['position_label_position_name'] = '职称';
$lang['position_input_position_name'] = '请输入职称';


//栏位
$lang['position_position_no'] = '职称代号';
$lang['position_position_name'] = '职称';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

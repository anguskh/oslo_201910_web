<?php
//其他
$lang['department_management'] = '部门基本资料';
$lang['department_label_dept_no'] = '部门代号';
$lang['department_input_dept_no'] = '请输入部门代号';
$lang['department_label_dept_name'] = '部门名称';
$lang['department_input_dept_name'] = '请输入部门名称';
$lang['department_label_isp_yes'] = '采购';
$lang['department_label_ship_yes'] = '销售';
$lang['department_label_stock_yes'] = '库存';
$lang['department_label_install_yes'] = '服务';
$lang['department_label_mark'] = '状态';

//栏位
$lang['department_dept_no'] = '部门代号';
$lang['department_dept_name'] = '部门名称';
$lang['department_isp_yes'] = '采购';
$lang['department_ship_yes'] = '销售';
$lang['department_stock_yes'] = '库存';
$lang['department_install_yes'] = '服务';
$lang['department_mark'] = '状态';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

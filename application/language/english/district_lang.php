<?php
//其他
$lang['district_management'] = '地区基本资料';
$lang['district_label_district_no'] = '地区代号';
$lang['district_input_district_no'] = '请输入地区代号';
$lang['district_label_district_name'] = '地区名称';
$lang['district_input_district_name'] = '请输入地区名称';
$lang['district_label_NCCC_no'] = 'NCCC代号';
$lang['district_input_NCCC_no'] = '请输入NCCC代号';
$lang['district_label_class_no'] = '类别代号';
$lang['district_input_class_no'] = '请输入类别代号';
$lang['district_label_city_no'] = '城市代号';
$lang['district_input_city_no'] = '请输入城市代号';



//栏位
$lang['district_district_no'] = '地区代号';
$lang['district_district_name'] = '地区名称';
$lang['district_NCCC_no'] = 'NCCC代号';
$lang['district_class_no'] = '类别代号';
$lang['district_city_no'] = '城市代号';
$lang['district_city_name'] = '城市名称';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

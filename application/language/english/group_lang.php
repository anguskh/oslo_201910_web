<?php
//其他
$lang['group_management'] = 'Group Management';
$lang['group_input_name'] = 'Please input group name';
$lang['group_name_duplicate'] = 'Duplicate Group Name';
$lang['group_in_use'] = 'in use';
$lang['group_approve_terminal_button'] = 'Approve';
$lang['group_login_side'] = 'Login Side';
$lang['group_select_user_group'] = 'Select User Group';
$lang['group_msg'] = 'Please Select User Group!';

//栏位
$lang['group_group_name'] = 'Group Name';
$lang['group_status'] = 'Group Status';
$lang['group_approve_terminal'] = 'Approve Terminal';

/* End of file group_lang.php */
/* Location: ./system/language/english/group_lang.php */

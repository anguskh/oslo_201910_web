<?php

$lang['password_current'] = 'Current Password';
$lang['password_input_current'] = 'Input current password';
$lang['password_current_error'] = 'current password error';
$lang['password_change'] = 'Change Password';
$lang['password_new'] = 'New Password';
$lang['password_input_new'] = 'Input new password';
$lang['password_new_confirm'] = 'Confirm New Password';
$lang['password_input_new_confirm'] = 'Input new password again';
$lang['password_first_login'] = 'Please Change Password for First Login';


/* End of file password_lang.php */
/* Location: ./system/language/english/password_lang.php */

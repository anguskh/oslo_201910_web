<?php
//其他
$lang['menu_management'] = '메뉴 관리';
$lang['menu_parent_name'] = '상위 메뉴 이름';
$lang['menu_select_parent_name'] = '상위 메뉴 이름을 선택하십시오';
$lang['menu_select_second_name'] = '두 번째 레벨 메뉴 이름을 선택하십시오';
$lang['menu_input_name'] = '메뉴 중국 이름을 입력하십시오';
$lang['menu_input_en_name'] = '메뉴의 영어 이름을 입력하십시오';
$lang['menu_input_directory'] = '메뉴 경로를 입력하십시오';
$lang['menu_input_image'] = '메뉴 이미지 파일 이름을 입력하십시오';
$lang['menu_input_sequence'] = '주문을 입력하십시오';
$lang['menu_status2'] = '메뉴 상태';
$lang['icon_class'] = 'icon_class';

// 필드
$lang['menu_menu_sn'] = '메뉴 번호';
$lang['menu_parent_menu_sn'] = '상위 메뉴 번호';
$lang['menu_second_menu_sn'] = '두 번째 레벨 메뉴 이름';
$lang['menu_name'] = '메뉴 중국 이름';
$lang['menu_menu_name_english'] = '메뉴 영어 이름';
$lang['menu_menu_name'] = '메뉴 이름';
$lang['menu_menu_directory'] = '메뉴 경로';
$lang['menu_menu_image'] = '메뉴 이미지 파일 이름';
$lang['menu_menu_sequence'] = '메뉴 순서';
$lang['menu_login_side_display'] = '로그인 표시';
$lang['menu_access_control'] = '사전 설정 기능';
$lang['menu_status'] = '상태';
$lang['menu_menu_content'] = '메뉴 설명';

// 제어 권한
$lang['menu_access_control'] = '운영 허가';

/* End of file menu_lang.php */
/* Location: ./system/language/zh_tw/menu_lang.php */

<?php
// 기타
$lang['log_battery_leave_return_inquiry'] = '배터리 대여 반품 기록';

// 필드
$lang['log_battery_leave_return_tv_num'] = '단위 ID';
$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_DorO_flag'] = '운영자 / 딜러 플래그';
$lang['log_battery_leave_return_leave_do_num'] = '(차용) 운영자 / 딜러';
$lang['log_battery_leave_return_leave_dso_num'] = '(차용) 하위 운영자 / 하위 딜러';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_leave_return_battery_user_id'] = '사용자 ID';
$lang['log_battery_leave_return_leave_request_date'] = '배터리 대여 요청 날짜 시간';
$lang['log_battery_leave_return_leave_coordinate'] = '대출 요청시 위도와 경도';
$lang['log_battery_leave_return_leave_date'] = '실제 배터리 임대 날짜 및 시간';
$lang['log_battery_leave_return_leave_track_no'] = '렌트 트랙 번호';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';
$lang['log_battery_leave_return_leave_status'] = '임대 상태';
$lang['log_battery_leave_return_return_DorO_flag'] = '운영자 / 리셀러 플래그';
$lang['log_battery_leave_return_return_do_num'] = '(또한 운영자 / 딜러)';
$lang['log_battery_leave_return_return_dso_num'] = '(Re) 하위 연산자 / 하위 딜러';
$lang['log_battery_leave_return_return_sb_num'] = '배터리 대여소를 반환하십시오';
$lang['log_battery_leave_return_return_sb_num_name'] = '배터리 대여 스테이션 (배터리 대여 스테이션 번호)의 위치를 ​​반환합니다';
$lang['log_battery_leave_return_return_request_date'] = '배터리 반환 요청 날짜 및 시간';
$lang['log_battery_leave_return_return_coordinate'] = '반환 요청시 위도와 경도';
$lang['log_battery_leave_return_return_date'] = '배터리 반품에 대한 실제 날짜 및 시간';
$lang['log_battery_leave_return_return_track_no'] = '반환 트랙 번호';
$lang['log_battery_leave_return_return_battery_id'] = '배터리 번호 반환';
$lang['log_battery_leave_return_return_status'] = '반품 상태';
$lang['log_battery_leave_return_usage_time'] = '배터리 사용 시간';
$lang['log_battery_leave_return_charge_amount'] = '이 청구 금액 계산';
$lang['log_battery_trail'] = '루트 트레일';	
	

/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

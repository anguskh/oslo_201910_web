<?php
//其他
$lang['log_operating_inquiry'] = '이력 쿼리';

// 필드
$lang['log_operating_operating_date'] = '작동 시간';
$lang['log_operating_user_sn'] = '사용자 이름';
$lang['log_operating_mode'] = '작업 유형';
$lang['log_operating_desc'] = '작업 후';
$lang['log_operating_sql'] = '방향';
$lang['log_operating_MERCHANT_ID'] = '상점 코드';
$lang['log_operating_TERMINAL_ID'] = '엔드 엔드 머신 코드';
$lang['log_operating_function_name'] = '함수 이름';
$lang['log_operating_before_desc'] = '작업 전';
$lang['log_operating_ip_address'] = 'IP 주소';
$lang['log_operating_status'] = '상태';


/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
// 제목
$lang['bss_manage_management'] = 'BSS 소프트웨어 관리';

// 필드
$lang['bss_content'] = '파일 설명';
$lang['bss_file'] = '파일';
$lang['bss_version_date'] = '버전 날짜';
$lang['bss_version_no'] = '버전 번호';
$lang['bss_checksum'] = '파일 체크섬';
$lang['up_flag'] = '지금 업데이트';
$lang['total_num'] = '총 임대 스테이션 수';
$lang['notify'] = '알림 <br> 알립니다';
$lang['update'] = '업데이트 된 <br> 업데이트 안 됨';

// BSS 소프트웨어 관리 목록 다운로드
$lang['sys_date'] = 'BSS 소프트웨어 버전 <br> 생성 시간';
$lang['bss_id'] = '임대 국 번호';
$lang['notify_date'] = '첫 번째 알림 시간';
$lang['updating_date'] = '업데이트 시간';
/* End of file bss_manage_lang.php */
/* Location: ./system/language/zh_tw/bss_manage_lang.php */

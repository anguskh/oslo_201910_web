<?php
// 기타
$lang['group_management'] = '그룹 관리';
$lang['group_input_name'] = '그룹 이름을 입력하십시오';
$lang['group_name_duplicate'] = '그룹 이름을 복제 할 수 없습니다';
$lang['group_in_use'] = '사용자가 사용하고 있습니다';
$lang['group_login_side'] = '로그인 권한';
$lang['group_select_user_group'] = '사용자 그룹을 확인하십시오';
$lang['group_msg'] = '사용자 관리는 사용자 그룹을 확인해야합니다!';
$lang['group_main_flag'] = '홈 바로 가기';

// 로그
$lang['group_user_group'] = '사용자 그룹';
$lang['group_menu_sn'] = '메뉴 번호';
$lang['group_group_sn'] = '그룹 번호';
$lang['to_flag'] = '연산자';
$lang['card_flag'] = '카드';


// 필드
$lang['group_group_name'] = '그룹 이름';
$lang['group_status'] = '그룹 상태';
$lang['group_approve_terminal'] = '터미널 머신 검토';
/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */

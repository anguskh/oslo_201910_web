<?php
//其他
$lang['login_user_title'] = '그린 파머 크리에이티브 관리 시스템';
$lang['login_nimda_title'] = '그린 파머 크리에이티브 관리 시스템';
$lang['login_advise'] = '사용하는 것이 좋습니다 <a href="http://www.google.com/chrome?hl=zh-TW&amp;brand=CHMI">Google Chrome</a> 더 나은 화면보기, 더 친숙한 사용자 인터페이스, 더 빠른 컨텐츠 렌더링을 위해.';
$lang['login_web_foreground'] = 'Lvnong 창의적 관리 시스템-전경';
$lang['login_web_background'] = '그린 파머 크리에이티브 관리 시스템';
$lang['login_id'] = '계정';
$lang['login_password'] = '비밀번호';
$lang['login_button'] = '로그인';
$lang['submit_button'] = '제출';
$lang['security_Input'] = '확인 코드';
$lang['try_another'] = '다른 것을 바꾸십시오';
$lang['forget_password'] = '비밀번호를 잊어 버렸습니다';
$lang['forget_email'] = '이메일 입력';
$lang['return_to_login'] = '로그인 페이지로 돌아 가기';
$lang['login_send'] = '보내기';
$lang['login_email_wrong_email_address'] = '이메일이 틀 렸습니다. 다시 입력 해주세요!';
$lang['login_email_right_email_address'] = '사서함에 새 비밀번호가 전송되었습니다! ';

$lang['login_email_subject'] = "비밀번호 쿼리";
$lang['login_email_error1'] = "문자를 보낼 수 없습니다. 연락하십시오";
$lang['login_email_error2'] = "시스템 관리자, 이메일 설정 확인";
$lang['login_email_body1'] = "새로운 비밀번호가 전송되었습니다. 로그인 후 비밀번호를 변경하십시오!";
$lang['login_email_body2'] = "계정";
$lang['login_email_body3'] = "새 비밀번호";
$lang['login_email_body4'] = "URL";
$lang['login_account_disabled'] = "이 계정은 비활성화되었습니다 !!";
$lang['login_verify_code_error'] = "확인 코드 오류 !!";
$lang['login_ip_error'] = "로그인 IP 오류 !!";
$lang['login_account_password_error'] = "잘못된 계정 또는 비밀번호 !!";
$lang['login_account_lock_out'] = "이 계정은 더 이상 다시 로그인 할 수 없습니다. 자세한 내용은 관리자에게 문의하십시오!";
$lang['login_remain'] = "남은 시도";
$lang['login_count'] = "次";

$lang['error_account_cant_login_again'] = '잘못된 계정 또는 비밀번호 !! \ n이 계정은 더 이상 다시 로그인 할 수 없습니다. 자세한 내용은 관리자에게 문의하십시오!';
$lang['error_account_disabled'] = '이 계정은 비활성화되었습니다 !!';
$lang['error_account'] = '잘못된 계정 또는 비밀번호 !!';
$lang['error_ip'] = 'IP가 네트워크 세그먼트 설정과 일치하지 않습니다 !!';
$lang['login_retry_cnt'] = '시도 횟수';

/* End of file bank_lang.php */
/* Location: ./system/language/zh_tw/bank_lang.php */

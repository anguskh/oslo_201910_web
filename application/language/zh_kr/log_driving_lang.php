<?php
// 기타
$lang['log_driving_inquiry'] = '배터리 충전 및 방전 이력 쿼리';

$lang['log_driving_battery_id'] = '배터리 일련 번호';
// 필드
$lang['td_num'] = '딜러';
$lang['tsd_num'] = '하위 딜러';
$lang['to_num'] = '연산자';
$lang['tso_num'] = '하위 연산자';
$lang['do_num'] = '딜러 / 운영자';
$lang['dso_num'] = '하위 딜러 / 하위 연산자';
$lang['tv_num'] = '차량 일련 번호';
$lang['tm_num'] = '회원 ID';
$lang['system_log_date'] = '시스템 로그 시간';
$lang['driving_date'] = '교통 시간';
$lang['unit_id'] = '단위 ID';
$lang['unit_name'] = '이름';
$lang['unit_status'] = '상태';
$lang['utc_date'] = 'UTC 날짜';
$lang['utc_time'] = 'UTC 시간';
$lang['latitude'] = 'GPS 위도';
$lang['longitude'] = 'GPS 경도';
$lang['speed'] = '속도';
$lang['angle'] = '각도';
$lang['gps_satellite'] = 'GPS 위성 수';
$lang['event_id'] = '이벤트 ID';
$lang['return_type'] = '반환 유형';
$lang['lease_status'] = '임대 상태';
$lang['engine_status'] = '시작 상태';
$lang['battery_voltage'] = '배터리 전압';
$lang['battery_amps'] = '배터리 전류';
$lang['battery_temperature'] = '배터리 온도';
$lang['environment_temperature'] = '주변 온도';
$lang['battery_capacity'] = '배터리 전력';
$lang['power_bank_voltage'] = '파워 뱅크 전압';
$lang['in_out_status'] = 'I / O 상태';
$lang['battery_station_position'] = '기지국 위치';
$lang['battery_gps_manufacturer'] = 'GPS 제조업체';
$lang['battery_gps_version'] = 'GPS 펌웨어 버전';
/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

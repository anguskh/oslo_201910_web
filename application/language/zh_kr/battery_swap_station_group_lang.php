<?php
// 제목
$lang['battery_swap_station_group_management'] = '배터리 렌탈 스테이션 그룹 관리';

// 기타
$lang['battery_swap_station_group_group_name'] = '그룹 이름';
$lang['battery_swap_station_group_all_sb_num'] = '배터리 대여소 선택';
$lang['battery_swap_station_group_sb_num'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
//栏位
 
/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

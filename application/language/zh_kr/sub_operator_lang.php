<?php
// 기타
$lang['sub_operator_management'] = '하위 운영자 관리';
// 기타
$lang['sub_operator_s_num'] = '하위 운영자 ID';
$lang['sub_operator_to_num'] = '운영자 이름';
$lang['sub_operator_tsop01'] = '하위 운영자 이름';
$lang['sub_operator_tsop02'] = '유형';
$lang['sub_operator_tsop03'] = '연락처';
$lang['sub_operator_tsop04'] = '전화 문의';
$lang['sub_operator_tsop05'] = '휴대 전화';
$lang['sub_operator_tsop06'] = '주소';
$lang['sub_operator_tsop07'] = '이메일';
$lang['sub_operator_tsop08'] = '플랫폼 거래 수수료';
$lang['sub_operator_tsop09'] = '은행 송금 코드';
$lang['sub_operator_tsop10'] = '은행 송금 계좌 번호';
$lang['sub_operator_tsop11'] = '대리 운영 계정 처리';
$lang['sub_operator_status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['s_num'] = '하위 운영자 ID';
$lang['to_num'] = '연산자';
$lang['tsop01'] = '하위 연산자 이름';
$lang['tsop02'] = '하위 연산자 유형';
$lang['tsop02_1'] = '직접 관리';
$lang['tsop02_2'] = '가입';
$lang['tsop02_3'] = '기타';
$lang['tsop03'] = '하위 운영자 담당자 이름';
$lang['tsop04'] = '하위 반송파 연락처';
$lang['tsop05'] = '하위 반송파 휴대 전화';
$lang['tsop06'] = '하위 운영자 주소';
$lang['tsop07'] = '하위 운영자 이메일';
$lang['tsop08'] = '은행 송금 코드';
$lang['tsop09'] = '은행 송금 계좌 번호';
$lang['status'] = '운영자 상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
/* End of file sub_operator_lang.php */
/* Location: ./system/language/zh_tw/sub_operator_lang.php */

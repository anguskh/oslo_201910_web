<?php
//其他
$lang['user_management'] = '계정 관리';
$lang['employee_management'] = '직원 기본 정보';
$lang['user_input_account'] = '로그인 계정을 입력하십시오';
$lang['user_input_name'] = '이름을 입력하십시오';
$lang['user_pwd'] = '비밀번호';
$lang['user_input_pwd'] = '로그인 비밀번호를 입력하십시오';
$lang['user_confirm_pwd'] = '비밀번호 확인';
$lang['user_input_confirm_pwd'] = '같은 로그인 암호를 입력하십시오';
$lang['user_input_mobile'] = '휴대 전화를 입력하십시오';
$lang['user_input_phone'] = '전화 번호를 입력하십시오';
$lang['user_input_email'] = '이메일 주소를 입력하십시오';
$lang['user_bank'] = '은행을 선택하십시오';
$lang['user_bank_help'] = '-- 은행을 선택하십시오 --&nbsp;';
$lang['user_merchant'] = '매장 선택';
$lang['user_merchant_help'] = '-- 상점을 선택하십시오 --&nbsp;';
$lang['user_select_group'] = '사용자 그룹을 선택하십시오';
$lang['user_status2'] = '사용자 상태';
$lang['user_login_force_pwd'] = '강제 암호 변경';
$lang['user_repassword'] = '답장 비밀번호';
$lang['user_log_change_password'] = '<font color=blue>[비밀번호 변경]</font>';
$lang['user_log_target'] = '<font color=blue>[목표]</font>';
$lang['user_log_content'] = '<font color=blue>[내용]</font>';
$lang['user_log_user_id'] = '계정 :';
$lang['user_log_pwd'] = '비밀번호 :';
$lang['user_label_basic'] = '기본 정보';
$lang['user_label_company'] = '회사 관련';
$lang['user_label_group_access'] = '그룹 및 권한';

$lang['user_label_emp_ename'] = '영어 이름';
$lang['user_label_dept_no'] = '부서';
$lang['user_input_dept_no'] = '부서를 입력하십시오';
$lang['user_label_emp_id'] = '직원 ID';
$lang['user_label_birth_date'] = '생년월일';
$lang['user_label_hire_date'] = '채용일';
$lang['user_label_location'] = '사무실 위치';
$lang['user_label_fax'] = '팩스';
$lang['user_label_home_tel'] = '집 전화 번호';
$lang['user_label_district'] = '국가';
$lang['user_label_city'] = '시, 카운티, 도시';
$lang['user_label_zip_code'] = '우편 번호';
$lang['user_label_address'] = '주소';
$lang['user_label_urgent_name'] = '비상 연락처';
$lang['user_label_urgent_tel'] = '비상 연락 전화 번호';
$lang['user_label_ID_No'] = '아이디 ID';
$lang['user_label_mobile2'] = '모바일 2';
$lang['user_label_position'] = '포지션';
$lang['user_label_charge_area'] = '책임 지역';
$lang['user_label_extension'] = '확장자';
$lang['user_label_status'] = '상태';
$lang['user_label_fulltime'] = '긍정적 인 게시물';
$lang['user_label_onduty'] = '작업 중';
$lang['user_label_install_per'] = '설치자';
$lang['user_label_isp_per'] = '구매 ​​직원';
$lang['user_label_stock_per'] = '창고 관리자';
$lang['user_label_sale_per'] = '판매원';
$lang['user_label_contract_per'] = '외주 계약자';
$lang['user_label_contract_per_no'] = '외부 코드';
$lang['user_label_remark'] = '비고';


// 필드
$lang['user_user_id'] = '계정';
$lang['user_user_name'] = '사용자';
$lang['user_sex'] = '성별';
$lang['user_mobile'] = '휴대 전화';
$lang['user_phone'] = '전화 번호';
$lang['user_email'] = '이메일';
$lang['user_group_sn'] = '사용자 그룹';
$lang['user_comment_notes'] = '은행';
$lang['user_merchant_name_chinese'] = '상점';
$lang['user_status'] = '상태';
$lang['user_access'] = '권한 제어';
$lang['to_num'] = '연산자';
$lang['tso_num'] = '하위 연산자';
$lang['user_view_area'] = '렌탈 스테이션 지역을 볼 수 있습니다';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */

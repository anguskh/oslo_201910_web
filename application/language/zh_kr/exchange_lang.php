<?php
//标题
$lang['exchange_management'] = '교환국';

// 필드
$lang['battery_id'] = '배터리 일련 번호';
$lang['manufacture_date'] = '공장 날짜';
$lang['gps_position'] = '배터리 위치 (위도 및 경도)';
$lang['battery_capacity'] = '배터리 전력';
/* End of file exchange_lang.php */
/* Location: ./system/language/zh_tw/exchange_lang.php */

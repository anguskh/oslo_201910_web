<?php
// 기타
$lang['log_alarm_online_inquiry'] = '즉시 알람 알림 로그';
$lang['monitor_management_online'] = '즉시 경보';

$lang['log_alarm_online_battery_id'] = '배터리 일련 번호';
// 필드
$lang['alarm_online_sn'] = '즉시 알람 알림 일련 번호';
$lang['so_num'] = '연산자';
$lang['sb_num'] = '배터리 교환 스테이션';
$lang['bss_id_name'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
$lang['log_date'] = '로그 시간';
$lang['bss_token_id'] = '교환소 토큰 ID';
$lang['type'] = '즉시 경고 카테고리';
$lang['type_1'] = '화재';
$lang['type_2'] = '홍수';
$lang['status'] = '처리 상태';
$lang['status_1'] = '미확인';
$lang['status_2'] = '처리 중';
$lang['status_3'] = '완료';
$lang['status_desc'] = '처리 상태 설명';
$lang['view_date'] = '볼 수있는 날짜';
$lang['view_users'] = '보기 된 사람들의 프로필';
$lang['process_date'] = '처리 중 날짜';
$lang['process_user'] = '진행중인 인원';
$lang['finish_date'] = '완료 날짜';
$lang['finish_user'] = '완료된 사람 프로필';



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

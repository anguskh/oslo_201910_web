<?php
// 제목
$lang['battery_gps_management'] = '배터리 GPS 보고서';

// 필드
$lang['system_log_date'] = '시스템 기록 시간';
$lang['battery_id'] = '배터리 발전기 번호';
$lang['manufacture_date'] = '제조 일자';
$lang['gps_position'] = '배터리 위치 (위도 및 광도)';
$lang['battery_capacity'] = '배터리 잔량';
/* End of file battery_gps_lang.php */
/* Location: ./system/language/zh_tw/battery_gps_lang.php */

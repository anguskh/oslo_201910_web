<?php
// 제목
$lang['vehicle_management'] = '차량 관리';
$lang['vehicle_management_upload'] = '차량 관리 확인 가져 오기';

// 기타
$lang['vehicle_use_typeN'] = '일반 자동차';
$lang['vehicle_use_typeS'] = '샘플 자동차 (청구 료 없음)';
$lang['vehicle_use_typeP'] = '샘플 자동차 (청구 료)';

$lang['vehicle_status1'] = '위탁';
$lang['vehicle_status2'] = '판매';
$lang['vehicle_status3'] = '임대 예정';
$lang['vehicle_status4'] = '임대';
$lang['vehicle_status5'] = '분실';
$lang['vehicle_status6'] = '종료';
$lang['vehicle_status7'] = '잘못된 계정';

$lang['vehicle_chargeY'] = '충전';
$lang['vehicle_chargeN'] = '충전 및 청구';
$lang['vehicle_chargeS'] = '대기';

$lang['vehicle_typeB'] = '전자 자전거';
$lang['vehicle_typeM'] = '전기차';

// 필드

$lang['DorO_flag'] = '공급 업체 유형';
$lang['vehicle_dealer'] = '딜러';
$lang['vehicle_sub_dealer'] = '하위 딜러';
$lang['vehicle_operator'] = '운영자';
$lang['vehicle_sub_operator'] = '하위 연산자';
$lang['vehicle_unit_id'] = '단위 ID';
$lang['vehicle_code'] = '프레임 번호';
$lang['vehicle_manufacture_date'] = '공장 날짜';
$lang['vehicle_license_plate_number'] = '라이센스 플레이트 번호';
$lang['vehicle_engine_number'] = '엔진 번호';
$lang['vehicle_car_machine_id'] = '차량 ID';
$lang['vehicle_model'] = '자동차 모델';
$lang['vehicle_color'] = '자동차 색상';
$lang['vehicle_4g_ip'] = '오토바이 4G IP';
$lang['vehicle_bluetooth_name'] = '기관차 이름';
$lang['vehicle_bluetooth_mac'] = 'MAC 블루투스 MAC';
$lang['vehicle_lora_sn'] = 'LoRa ID';
$lang['vehicle_user_id'] = '차량 사용자 ID';
$lang['vehicle_type'] = '차량 종류';
$lang['vehicle_use_type'] = '사용 카테고리';
$lang['vehicle_charge'] = '차량 충전 상태';
$lang['vehicle_charge_count'] = '차량 배터리 충전 횟수';
$lang['vehicle_status'] = '차량 상태';
$lang['vehicle_latitude'] = '현재 차량 위도';
$lang['vehicle_longitude'] = '현재 차량 경도';
/* End of file vehicle_lang.php */
/* Location: ./system/language/zh_tw/vehicle_lang.php */

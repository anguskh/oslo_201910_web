<?PHP
// 기타
$lang['operator_order_management'] = '운영자 사전 주문 관리';
// 기타
$lang['operator_order_s_num'] = '하위 운영자 ID';
$lang['operator_order_to_num'] = '운영자 이름';
$lang['operator_order_top08'] = '선주문';
$lang['operator_card_count'] = '카드 수';
$lang['operator_order_status'] = '상태';
$lang['operator_order_detail'] = '세부 사항';
$lang['operator_order_add_num'] = '구매 ​​추가';
$lang['operator_order_before_total_lave_num'] = '구매 ​​전 남아있는 총 구매 수';
$lang['operator_order_create_date'] = '추가 구매 날짜';


$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['s_num'] = '하위 운영자 ID';
$lang['to_num'] = '연산자';
$lang['top08'] = '선주문';
$lang['lease_type'] = '선주문 유형';
$lang['lease_type_1'] = '선주문';
$lang['lease_type_2'] = '월간';
$lang['card_num'] = '카드 수';
$lang['lease_price'] = '거래 당 금액';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';

/* End of file operator_order_lang.php */
/* Location: ./system/language/zh_tw/operator_order_lang.php */

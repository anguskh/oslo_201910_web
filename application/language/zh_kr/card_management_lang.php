<?php
// 기타
$lang['card_management_management'] = '카드 관리';
// 기타
$lang['card_management_to_num'] = '캐리어 이름';
$lang['card_management_top08'] = '선주문';
$lang['operator_card_count'] = '카드 수';
$lang['card_management_status'] = '상태';
$lang['card_management_detail'] = '세부 사항';
$lang['card_management_detail_record'] = '거래소 기록';
$lang['card_management_name'] = '카드 이름';
$lang['card_management_qrocde'] = 'qrocde 컨텐츠';
$lang['card_management_pre_order_num'] = '총 선주문';
$lang['card_management_lave_num'] = '남은 시간';
$lang['card_management_add_num'] = '구매 ​​추가';
$lang['card_management_before_total_lave_num'] = '구매 ​​전 총 구매 횟수';
$lang['card_management_create_date'] = '추가 구매 일';
$lang['card_management_lease_type'] = '구매 ​​유형 추가';
$lang['card_management_lease_price'] = '단일 금액';

$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['to_num'] = '연산자';
$lang['name'] = '카드 이름';
$lang['user_id'] = 'qrocde 컨텐츠';
$lang['pre_order_num'] = '총 예약 주문';
$lang['lave_num'] = '남은 시간';
$lang['nick_name'] = '양육권 이름';
$lang['mobile'] = '휴대 전화';
$lang['city'] = 'Living County';
$lang['address'] = '연락처 주소';

$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
$lang['lease_type'] = '구매 ​​유형 추가';
$lang['lease_type_1'] = '선주문';
$lang['lease_type_2'] = '월간';
$lang['lease_price'] = '거래 당 금액';

// 대여 기록
$lang['log_battery_leave_return_tv_num'] = '단위 ID';
$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_DorO_flag'] = '운영자 / 딜러 플래그';
$lang['log_battery_leave_return_leave_do_num'] = '(차용) 운영자 / 딜러';
$lang['log_battery_leave_return_leave_dso_num'] = '(차용) 하위 운영자 / 하위 딜러';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_leave_return_battery_user_id'] = '사용자 ID';
$lang['log_battery_leave_return_leave_request_date'] = '배터리 대여 요청 날짜 시간';
$lang['log_battery_leave_return_leave_coordinate'] = '대출 요청시 위도와 경도';
$lang['log_battery_leave_return_leave_date'] = '실제 배터리 임대 날짜 및 시간';
$lang['log_battery_leave_return_leave_track_no'] = '렌트 트랙 번호';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';
$lang['log_battery_leave_return_return_DorO_flag'] = '운영자 / 리셀러 플래그';
$lang['log_battery_leave_return_return_do_num'] = '(또한 운영자 / 딜러)';
$lang['log_battery_leave_return_return_dso_num'] = '(Re) 하위 연산자 / 하위 딜러';
$lang['log_battery_leave_return_return_sb_num'] = '배터리 대여소를 반환하십시오';
$lang['log_battery_leave_return_return_sb_num_name'] = '배터리 대여 스테이션 (배터리 대여 스테이션 번호)의 위치를 ​​반환합니다';

$lang['log_battery_leave_return_return_request_date'] = '배터리 반환 요청 날짜 및 시간';
$lang['log_battery_leave_return_return_coordinate'] = '반환 요청시 위도와 경도';
$lang['log_battery_leave_return_return_date'] = '배터리 반품에 대한 실제 날짜 및 시간';
$lang['log_battery_leave_return_return_track_no'] = '반환 트랙 번호';
$lang['log_battery_leave_return_return_battery_id'] = '배터리 번호 반환';
$lang['log_battery_leave_return_usage_time'] = '배터리 사용 시간';
$lang['log_battery_leave_return_charge_amount'] = '이 청구 금액 계산';

$lang['log_battery_trail'] = '루트 트레일';

$lang['log_store_balance_charge_type'] = '매장 가치 / 공제 유형';
$lang['log_store_balance_charge_exchange_date'] = '매장 가치 / 공제 날짜 및 시간';
$lang['log_store_balance_charge_charge_amount'] = '이 저장된 가치 / 공제 금액';
$lang['log_store_balance_charge_store_balance_before'] = '공제 전의 저장 가치 / 저장 가치 균형';
$lang['log_store_balance_charge_store_balance_after'] = '공제 후의 저장 가치 / 저장 가치 균형';

/* End of file card_management_lang.php */
/* Location: ./system/language/zh_tw/card_management_lang.php */

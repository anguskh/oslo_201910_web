<?php
// 기타
$lang['boundbattery_management'] = '바운드 배터리 관리';
// 기타
$lang['user_id'] = '사용자 ID';
$lang['member_name'] = '회원 이름';
$lang['member_type'] = '회원 유형';
$lang['battery_count'] = '바운드 배터리 수';

$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';

/* End of file boundbattery_lang.php */
/* Location: ./system/language/zh_tw/boundbattery_lang.php */

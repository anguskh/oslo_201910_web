<?php
//其他
$lang['supplementary_management'] = '온라인 교체 배터리 일련 번호';
// 기타
$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';

/* End of file supplementary_lang.php */
/* Location: ./system/language/zh_tw/supplementary_lang.php */

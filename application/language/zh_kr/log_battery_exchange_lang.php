<?php
// 기타
$lang['log_battery_exchange_inquiry'] = '배터리 교환 레코드 쿼리';

// 필드
$lang['log_battery_exchange_so_num'] = '운영자';
$lang['log_battery_exchange_sb_num'] = '배터리 교환 스테이션';
$lang['log_battery_exchange_sb_num_name'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
$lang['log_battery_exchange_sv_num'] = '전기 자동차 일련 번호';
$lang['log_battery_exchange_vehicle_code'] = '기관차 번호';
$lang['log_battery_exchange_vehicle_user_id'] = '차량 사용자 ID';
$lang['log_battery_exchange_no_track_no'] = '배터리의 트랙 번호 변경';
$lang['log_battery_exchange_no_battery_pack_sn'] = '배터리 팩 일련 번호로 다시 변경';
$lang['log_battery_exchange_yes_track_no'] = '배터리 트랙 번호 교체';
$lang['log_battery_exchange_yes_battery_pack_sn'] = '배터리 팩 일련 번호 변경';
$lang['log_battery_exchange_date'] = '교환 날짜 시간';
$lang['log_battery_exchange_exchange_type'] = '교환 카테고리';

/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

<?php
// 기타
$lang['dealer_management'] = '딜러 관리';
// 기타
$lang['dealer_s_num'] = '딜러 ID';
$lang['dealer_tde01'] = '딜러 이름';
$lang['dealer_tde02'] = '유형';
$lang['dealer_tde03'] = '연락처';
$lang['dealer_tde04'] = '전화 문의';
$lang['dealer_tde05'] = '휴대 전화';
$lang['dealer_tde06'] = '주소';
$lang['dealer_tde07'] = '이메일';
$lang['dealer_tde08'] = '플랫폼 거래 수수료';
$lang['dealer_tde09'] = '은행 송금 코드';
$lang['dealer_tde10'] = '은행 송금 계좌 번호';
$lang['dealer_tde11'] = '에이전트를 대신하여 하위 계정 처리';
$lang['dealer_status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['s_num'] = '딜러 ID';
$lang['tde01'] = '리셀러 이름';
$lang['tde02'] = '딜러 유형';
$lang['tde02_1'] = '직접 관리';
$lang['tde02_2'] = '가입';
$lang['tde02_3'] = '기타';
$lang['tde03'] = '리셀러 연락처 이름';
$lang['tde04'] = '리셀러 연락처 전화 번호';
$lang['tde05'] = '대리점 휴대 전화';
$lang['tde06'] = '딜러 주소';
$lang['tde07'] = '이메일';
$lang['tde08'] = '플랫폼 거래 수수료';
$lang['tde09'] = '은행 송금 코드';
$lang['tde10'] = '은행 송금 계좌 번호';
$lang['tde11'] = '하나를 대신하여 하위 계정 처리';
$lang['tde11_Y'] = '예';
$lang['tde11_N'] = '아니오';
$lang['status'] = '딜러 상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
/* End of file dealer_lang.php */
/* Location: ./system/language/zh_tw/dealer_lang.php */

<?php
//一般通用
$lang['airlink'] = '기술 회사';
$lang['airlink_eng'] = '테스트 기술 유한 회사';
$lang['airlink_addr'] = '주소 없음';
$lang['airlink_tel'] = '전화 : (02) 1111-1111 팩스 : (02) 1111-1111';

$lang['logout'] = '로그 아웃';
$lang['select'] = '선택';
$lang['browser'] = '찾아보기 / 조회';
$lang['view'] = '보기';
$lang['view_1'] = '<i class="fas fa-eye"></i>';
$lang['add'] = '추가';
$lang['add_1'] = '<i class="fas fa-plus"></i>';
$lang['edit'] = '수정';
$lang['edit_1'] = '<i class="fas fa-edit"></i>';
$lang['delete'] = '삭제';
$lang['delete_1'] = '<i class="fas fa-trash-alt"></i>';
$lang['save'] = '아카이브';
$lang['cancel'] = '취소';
$lang['print'] = '미리보기 인쇄';
$lang['auto_change'] = '실시간 모니터링 표시';
$lang['buy'] = '플러스 구매';
$lang['make_up'] = '보충';

$lang['print_ok_vender'] = '자격 판매 업체 목록';
$lang['print_item_all'] = '재료 상세 목록';
$lang['print_item_new_old'] = '신규 및 기존 재료 비교표';
$lang['trans_inv'] = '이전 달의 이체';
$lang['recount_inv'] = '인벤토리 재 계산';
$lang['data_search'] = '검색';
$lang['export_xml'] = 'XML 내보내기';
$lang['export_excel'] = '엑셀 내보내기';
$lang['batch_report'] = '배치 보고서';

$lang['resetkey'] = '재활용';
$lang['resetphone'] = '명확한 일치';
$lang['change_readersn'] = '수리';
$lang['up'] = '이동';
$lang['down'] = '이동';
$lang['copy'] = '복사';
$lang['reload'] = '재 선택';
$lang['search'] = '검색';
$lang['search_button'] = '검색';
$lang['import_img'] = '이미지 파일 가져 오기';
$lang['select_img'] = '이미지 선택';
$lang['img_format'] = 'jpg, png 형식, 크기 만 지원합니다 : 175 * 320';
$lang['prev_page'] = '이전 페이지';
$lang['paring_merchant'] = '매칭 스토어';
$lang['favor_tip'] = '이 영역에서 단축키를 사용자화할 수 있습니다. 추가해야하는 경우 하위 메뉴 앞의 별표를 클릭하십시오!';

$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
$lang['bypass'] = '바이 패스 해시 체크';
$lang['bypass2'] = 'ByPass';
$lang['nolimits'] = '새로운 버전 업데이트 알림 (무한)';
$lang['nolimits2'] = '마감일 없음';
$lang['limits'] = '마감일 (기간 제한) 이후 버전이 중단됩니다';
$lang['limits2'] = '한정 기간';
$lang['disable_2'] = '사용할 수 없습니다';
$lang['disableuse'] = '비활성화';
$lang['lockup'] = '기계 잠금';
$lang['approve'] = '통과';
$lang['not_approve'] = '실패';
$lang['yes'] = '예';
$lang['no'] = '아니요';
$lang['sucess'] = '성공';
$lang['fail'] = '실패';
$lang['login_side_0'] = '앞면';
$lang['login_side_1'] = '배경';
$lang['login_side_2'] = '모두';
$lang['search_all'] = '모두';
$lang['merchant_event'] = '이벤트 저장';
$lang['system_event'] = '시스템 이벤트';

$lang['male'] = '남성';
$lang['female'] = '여성';

$lang['currently_online_users'] = '현재 온라인 인';
$lang['last_10_rows_of_unsettlement'] = '최근 미결제 청구 내역';
$lang['last_10_rows_of_modify_merchant'] = '최근 변경점';
$lang['last_10_rows_of_modify_merchant_data'] = '최근 변경 사항';
$lang['last_10_rows_of_event_log'] = '최근 이벤트 로그';


$lang['add_successfully'] = '성공적으로 추가하십시오';
$lang['edit_successfully'] = '성공적으로 수정되었습니다';
$lang['edit_failed'] = '수정 실패';
$lang['add_failed'] = '추가 실패';
$lang['copy_successfully'] = '성공적으로 복사되었습니다';
$lang['reset_successfully'] = '성공적으로 정리되었습니다';
$lang['disable_merchant'] = '\n, 현재 터미널 머신이 없습니다.이 스토어 및 관련 사용자 계정은 자동으로 비활성화됩니다!';
$lang['change_successfully'] = '성공적으로 변경되었습니다';
$lang['delete_successfully'] = '성공적으로 삭제되었습니다';
$lang['delete_failed'] = '삭제 실패';
$lang['recover_successfully'] = '성공적으로 복구 된 데이터';
$lang['recover_fail'] = '데이터 복구 실패';

$lang['confirm_delete'] = '정말로 삭제하겠습니까? ';
$lang['cnat_delete'] = '삭제할 수 없습니다!';
$lang['confirm_resetkey'] = 'XAC E50 일련 번호와 키 값을 지우시겠습니까? \n이 작업을 수행하면 중요한 필드가 지워집니다 (예 : (BKLK_KEK ...) ';
$lang['confirm_resetphone'] = '전화 일치를 지우시겠습니까? ';
$lang['ajax_request_an_error_occurred'] = '아약스 요청에 오류가 발생했습니다! \n 다시 시도하십시오! ';
$lang['search_not_found'] = '데이터 확인 안함';
$lang['process_failed'] = '삭제할 데이터가 없습니다';
$lang['delete_img'] = '이미지 파일 삭제';


$lang['confirm'] = '실행 확인';
$lang['next'] = '다음';
$lang['resend'] = '재전송';
$lang['space'] = '';
$lang['space1_9'] = '';
$lang['space2'] = '';
$lang['space6'] = '';

$lang['home'] = '홈';
$lang['all'] = '모든리스트';
$lang['all_not_approve'] = '검토되지 않은 모든 목록';
$lang['bank'] = '은행 :';
$lang['bank2'] = '은행';
$lang['kind'] = '카테고리 :';
$lang['merchant'] = 'Store :';
$lang['terminal'] = '터미널 터미널 :';
$lang['project'] = '프로젝트 :';
$lang['user'] = '사용자 :';

$lang['select_bank'] = '은행을 선택하십시오';
$lang['select_bank_help'] = '-- 프로젝트를 관리 할 은행을 선택하십시오 --&nbsp;';
$lang['select_supplier'] = '신용 카드 기계 제조업체를 선택하십시오 :';
$lang['select_supplier_help'] = '-- 프로젝트를 유지하려면 신용 카드 기계 제조업체를 선택하십시오 --&nbsp;';
$lang['select_merchant'] = '상점을 선택하십시오';
$lang['select_pattern'] = '샘플을 선택하십시오';
$lang['select_merchant_help'] = '-- 유지할 매장을 선택하십시오 --&nbsp;';
$lang['select_terminal'] = '터미널을 선택하십시오 :';
$lang['select_terminal_help'] = '-- 유지 보수 할 터미널 머신을 선택하십시오 --&nbsp;';
$lang['select_project'] = '프로젝트를 선택하십시오 :';
$lang['select_project_help'] = '-- 유지하려는 프로젝트를 선택하십시오 --&nbsp;';
$lang['select_user'] = '사용자를 선택하십시오 :';
$lang['select_user_help'] = '-- 유지할 사용자를 선택하십시오 --&nbsp;';
$lang['select_choose_a_date'] = '날짜 범위를 선택하십시오';
$lang['select_choose_close_date'] = '체크 아웃 날짜를 선택하십시오';
$lang['select_choose_statistical'] = '통계 날짜를 선택하십시오 <br /> (선택 종료 날짜 없음, 시작 날짜 3 개월 전의 사전 설정 데이터)';
$lang['select_event_type'] = '이벤트를 선택하십시오';
$lang['select_event_type_help'] = '-이벤트를 선택하십시오-';
$lang['select_system_event'] = '시스템 이벤트';
$lang['select_merchant_event'] = '이벤트 저장';
$lang['select_all'] = '모두';
$lang['select_all_bank_no'] = '모든 은행';
$lang['select_all_kind_no'] = '모든 카테고리';
$lang['select_all_isp_class'] = '모든 카테고리';
$lang['select_all_ship_class'] = '모든 카테고리';
$lang['select_all_stock_class'] = '모든 카테고리';
$lang['select_city'] = '도시를 선택하십시오';
$lang['select_dept'] = '부서를 선택하십시오';
$lang['select_location'] = '사무실 위치를 선택하십시오';
$lang['select_district'] = '지역을 선택하십시오';
$lang['select_machine'] = '모델을 선택하십시오';
$lang['select_machineap'] = 'POSAP 버전을 선택하십시오';
$lang['select_kernel'] = '커널 버전을 선택하십시오';
$lang['select_status'] = '상태를 선택하십시오';
$lang['select_flag'] = '완료 결과를 선택하십시오';
$lang['select_unfinish'] = '완료되지 않은 이유를 선택하십시오';
$lang['select_onsite'] = '재활용 방법을 선택하십시오';
$lang['select_maintain_onsite'] = '처리 방법을 선택하십시오';
$lang['select_cause_no'] = '나쁜 원인을 선택하십시오';
$lang['select_air_cause_no'] = '나쁜 회사의 원인을 선택하십시오';
$lang['select_fixway_no'] = '수리를 완료하도록 선택하십시오';
$lang['select_use_situation'] = '처리 할 카드 기계를 선택하십시오';
$lang['select_position'] = '위치를 선택하십시오';
$lang['select_charge_area'] = '담당 지역을 선택하십시오';
$lang['select_bank_kind'] = '고객 카테고리를 선택하십시오';
$lang['select_per_no'] = '사업 담당자를 선택하십시오';
$lang['select_per_no2'] = '사람을 입력하거나 선택하십시오';
$lang['select_request_per_no'] = '구매자를 선택하십시오';
$lang['select_request_ord_per'] = '발주자를 선택하십시오';
$lang['select_request_isp_per'] = '수락 인을 선택하십시오';
$lang['select_check_per'] = '재고 담당자를 선택하십시오';
$lang['select_isp_per'] = '신청자를 선택하십시오';
$lang['select_stock_per'] = '창고 관리자를 선택하십시오';
$lang['select_stock_per2'] = '수용 감독자를 선택하십시오';
$lang['select_ship_per'] = '배송 업체를 선택하십시오';
$lang['select_ship_per2'] = '결제를 요청한 사람을 선택하십시오';
$lang['select_ship_per3'] = '신청자를 선택하십시오';
$lang['select_t_manager'] = '부서를 선택하십시오';
$lang['select_check_per'] = '재고 담당자를 선택하십시오';
$lang['select_stock_manager'] = '창고 관리자를 선택하십시오';
$lang['select_ship_way'] = '배송 방법을 선택하십시오';
$lang['select_sales_kind'] = '배송 카테고리를 선택하십시오';
$lang['select_machine_type_per_no'] = '공급 업체 직원을 선택하십시오';
$lang['select_account_close_date'] = '체크 아웃 날짜를 선택하십시오';
$lang['select_payway'] = '지급 방법을 선택하십시오';
$lang['select_paydate'] = '지급일을 선택하십시오';
$lang['select_invoice_way'] = '인보이스 유형을 선택하십시오';
$lang['select_paygetway'] = '결제 수단을 선택하십시오';
$lang['select_bank_no'] = '은행을 선택하십시오';
$lang['select_bank_no2'] = '고객 번호를 선택하십시오';
$lang['select_bank_no3'] = '고객 이름을 선택하십시오';
$lang['select_four_dbc'] = '4DBC를 선택하십시오';
$lang['select_kind_no'] = '자재 번호 카테고리를 선택하십시오';
$lang['select_item_no'] = '부품 번호를 선택하십시오';
$lang['select_isp_class'] = '항목 카테고리를 선택하십시오';
$lang['select_ship_class'] = '항목 카테고리를 선택하십시오';
$lang['select_use_mark'] = '목적을 선택하십시오';
$lang['select_request_status'] = '가격 비교를 선택하십시오';
$lang['select_vnd_no'] = '공급 업체를 선택하십시오';
$lang['select_warranty_way'] = '보증 조건을 선택하십시오';
$lang['select_paytxt'] = '지불 조건을 선택하십시오';
$lang['select_remark'] = '노트를 선택하십시오';
$lang['select_stock_class_2'] = '스타킹 클래스를 선택하십시오';
$lang['select_stock_class_3'] = '시프 팅 카테고리를 선택하십시오';
$lang['select_tra_reason'] = '직책 이동 이유를 선택하십시오';
$lang['select_reason'] = '차이에 대한 이유를 선택하십시오';
$lang['select_version'] = '은행 버전을 선택하십시오';
$lang['select_user2'] = '사용자를 선택하십시오';
$lang['select_notice_no'] = '알림 방법을 선택하십시오';

$lang['select_release_per'] = '신청자를 선택하십시오';
$lang['select_stock_per'] = '창고 관리자를 선택하십시오';
$lang['select_rel_sub_per'] = '신청자를 대신하여 신청자를 선택하십시오';
$lang['select_stock_class'] = '픽업 클래스를 선택하십시오';
$lang['select_instock_class'] = '스타킹 클래스를 선택하십시오';
$lang['select_stock_kind'] = '사용할 주요 카테고리를 선택하십시오';
$lang['select_warehouse_no'] = '배송 할 창고를 선택하십시오';
$lang['select_warehouse_no2'] = '창고를 선택하십시오';
$lang['select_warehouse_no3'] = '창고를 입력하거나 선택하십시오';
$lang['select_new_warehouse_no'] = '창고를 선택하십시오';
$lang['select_release_reason'] = '배송 이유를 선택하십시오';
$lang['select_ost_sub_per_no'] = '대체자를 선택하십시오';
$lang['select_ret_sub_per'] = '대체자를 선택하십시오';
$lang['select_ins_per_no'] = '설치 프로그램을 선택하십시오';
$lang['select_mai_per_no'] = '유지 보수 담당자를 선택하십시오';
$lang['select_sha_per_no'] = '공유 된 사람을 선택하십시오';
$lang['select_rec_per_no'] = '리사이클 러를 선택하십시오';
$lang['select_rep_per'] = '레코더를 선택하십시오';
$lang['select_area_no'] = '저장 위치를 ​​선택하십시오';
$lang['select_emsType'] = '카테고리 코드를 선택하십시오';
$lang['select_contlsUFlag'] = 'U 카드로 기능을 선택하십시오';
$lang['select_contlsVFlag'] = 'VISA 기능으로 선택하십시오';
$lang['select_contlsMFlag'] = '마스터 기능으로 선택하십시오';
$lang['select_contlsJFlag'] = 'JBC 기능으로 선택하십시오';
$lang['select_contlsAEFlag'] = 'AE 기능으로 선택하십시오';

$lang['select_return_reason'] = '저장 이유를 선택하십시오';
$lang['select_ret_warehouse_no'] = '창고를 선택하십시오';
$lang['select_check_result'] = '확인 결과를 선택하십시오';
$lang['select_problem_reason'] = '문제의 원인을 선택하십시오';

$lang['select_zip_Code'] = '우편 번호를 선택하십시오';
$lang['select_pay_bank'] = '송금 은행을 선택하십시오';
$lang['select_mcht_district'] = '설치 지역을 선택하십시오';
$lang['select_industry_code'] = '업종을 선택하십시오';
$lang['select_pay_branch'] = '지사 코드를 선택하십시오';

$lang['select_import_excel'] = '가져온 Excel 파일을 선택하십시오';
$lang['select_import_caption_excel2_error'] = '가져 오기 과정이 실패했습니다! 관리자에게 연락하십시오!';


$lang['select_all_help'] = '-- 모두 선택 --&nbsp;';
$lang['select_all_mer_help'] = '-- 모든 상점 --&nbsp;';
$lang['select_stock_help'] = '-- 스톡 터미널 머신 선택 --&nbsp;';
$lang['all_mer'] = '모든 상점';

$lang['select_machine_type_no'] = '모델을 선택하십시오';
$lang['select_machine_type_no2'] = '모델 번호를 선택하십시오';
$lang['select_all_machine_type_no'] = '모든 모델';

$lang['please_input_merchant_id'] = '상점 코드를 입력하십시오';
$lang['please_input_terminal_id'] = '터미널 터미널 코드를 입력하십시오';
$lang['please_input'] = '입력 해주세요';

$lang['label_include'] = '일련 번호 포함';
$lang['report_no_data'] = '데이터 없음';

//讯息
$lang['check_url_error'] = '이 페이지에 접근 할 수있는 권한이 없습니다. \n 자동으로 로그인 페이지로 이동합니다';
$lang['success_action'] = '성공';
$lang['failed_action'] = '실패';
$lang['out_max_no'] = '자동 번호 매기기를 초과했습니다. 관리자에게 문의하십시오!';
$lang['timeout_msg'] = '시스템 유휴 상태가 로그 아웃되었습니다!';

// 페이지 매김
$lang['common_first_page'] = '첫 페이지';
$lang['common_last_page'] = '마지막 페이지';
$lang['common_prev_page'] = '이전 페이지';
$lang['common_next_page'] = '다음 페이지';
$lang['common_page'] = '페이지';


$lang['copy_bank_project_help'] = '이 복사 기능은 아래 선택된 은행 프로젝트로 모든 자료를 복사하는 것으로 제한됩니다! !! !! ';
$lang['copy_bank_project_error'] = '선택한 은행 프로젝트에 이미 데이터가 있으므로 복사가 허용되지 않습니다! !! !! ';
$lang['select_import_caption_xml'] = '가져올 작업 파일을 선택하십시오 (XML 형식)';
$lang['select_import_caption_excel'] = '가져올 카드 리더 페어링 파일을 선택하십시오 (EXCEL 형식)';
$lang['select_upload_file'] = '업로드 파일 선택';

$lang['login_user_title'] = '그린 파머 크리에이티브 관리 시스템';
$lang['login_nimda_title'] = '그린 파머 크리에이티브 관리 시스템';
$lang['html_title'] = 'Lvnong 광고 소재';
$lang['btn_mno'] = '일련 세부 사항';
$lang['btn_delete'] = '데이터 삭제';

// 로그
$lang['searchData'] = '검색';
$lang['acquire_bank_id'] = '은행 ID';
$lang['merchant_id'] = '상점 ID';
$lang['terminal_id'] = '상점 ID';
$lang['create_date'] = '생성 날짜';
$lang['create_user'] = '파일러';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['UPDATE_DATETIME'] = '수정일';
$lang['start_date'] = '起 日';
$lang['end_date'] = '지금까지';
$lang['FILE_DATE_Start'] = '起 日';
$lang['FILE_DATE_End'] = '지금까지';
$lang['INITIALIZE_DATE_Start'] = '오늘부터';
$lang['INITIALIZE_DATE_End'] = '지금까지';
$lang['ACT'] = '조치';
$lang['RETURN'] = '포스트 백';
$lang['SN'] = '숫자';
$lang['BANK_ID'] = '은행 ID';
$lang['error_date'] = '시작 날짜는 현재 날짜보다 클 수 없습니다!';
$lang['error_date2'] = '날짜를 선택하십시오 !!';


$lang['report_sign_General_Manager'] = '일반 관리자';
$lang['report_sign_Vice_President'] = '부총재';
$lang['report_sign_Unit_Manager'] = '단위 감독자';
$lang['report_sign_Lister'] = '단위 감독자';

$lang['total_count'] = '총계 :';

$lang['select_dealer_class'] = '대리점을 선택하십시오';
$lang['select_subdealer_class'] = '하위 딜러를 선택하십시오';
$lang['select_operator_class'] = '연산자를 선택하십시오';
$lang['select_suboperator_class'] = '하위 연산자를 선택하십시오';
$lang['select_tlp_class'] = '임대 품목을 선택하십시오';
$lang['select_batteryswaps_class'] = '배터리 대여소를 선택하십시오';
$lang['select_vehicle_class'] = '전기차를 선택하십시오';
$lang['select_member_class'] = '회원을 선택하십시오';

$lang['all_station_count'] = '총 스위칭 스테이션 수'; 
$lang['all_battery_count'] = '총 배터리 수'; 
$lang['all_vehicle_count'] = '총 기관차 수'; 
$lang['all_station_num'] = '총 전력 변화 횟수';
$lang['all_vehicle_km'] = '모든 기관차의 총 마일리지';
$lang['all_emissions'] = '총 이산화탄소 배출량 감소';
$lang['all_weChatpay'] = '위챗이 지불 한 총 수수료';

$lang['battery_status_statistics'] = '배터리 상태 통계';
$lang['battery_power_statistics'] = '배터리 전력 통계';
$lang['power_average'] = '일별 기간 및 시스템 평균 기간 동안의 전력 교환 횟수';
$lang['monitor_management_online'] = '즉시 경보';
$lang['so_num'] = '연산자';
$lang['log_battery_info_sb_num'] = '배터리 교체 스테이션';
$lang['sb_num'] = '배터리 교체 스테이션';
$lang['log_date'] = '로그 시간';
$lang['bss_token_id'] = '스테이션 토큰 ID 변경';
$lang['type'] = '즉시 경고 카테고리';
$lang['status'] = '처리 상태';
$lang['status_desc'] = '처리 상태 설명';
$lang['view_date'] = '볼 수있는 날짜';
$lang['view_users'] = '보기 된 사람들의 프로필';
$lang['process_date'] = '처리 중 날짜';
$lang['process_user'] = '진행중인 인원';
$lang['finish_date'] = '완료 날짜';
$lang['finish_user'] = '완료된 사람 프로필';

$lang['type_1'] = '불';
$lang['type_2'] = '홍수';
$lang['type_3'] = '무적 반품';
$lang['type_4_1'] = "프론트 데스크 시간은 시스템 시간과 다릅니다";
$lang['type_4_2'] = "몇 분 이상";
$lang['battery_exchange_ranking'] = '배터리 교체 스테이션의 일일 교환 순위';

$lang['bss_id'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
$lang['log_date'] = '날짜';
$lang['sum_exchangenum'] = '교환 횟수';

$lang['battery_change_nums'] = '배터리 교환 시간';
$lang['exchanges_average_num'] = '변전소의 평균 배터리 교환 횟수';
$lang['one_exchanges_average'] = '단일 변전소의 평균 배터리 교환 횟수';

$lang['battery_statusY'] = '충전';
$lang['battery_statusN'] = '완전히 청구 됨';
$lang['battery_statusS'] = '대기';
$lang['battery_statusE'] = '오류';
$lang['battery_statusV'] = '전기차';
/* End of file common_lang.php */
/* Location: ./system/language/zh_tw/common_lang.php */

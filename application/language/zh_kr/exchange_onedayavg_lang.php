<?php
//标题
$lang['exchange_onedayavg_management'] = '교환소에서 매일 배터리 교환 횟수';

// 필드
$lang['bss_id'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
$lang['log_date'] = '날짜';
$lang['sum_exchangenum'] = '교환 횟수';
/* End of file exchange_average_lang.php */
/* Location: ./system/language/zh_tw/exchange_average_lang.php */

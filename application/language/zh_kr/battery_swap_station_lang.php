<?php
// 제목
$lang['battery_swap_station_management'] = '배터리 렌탈 스테이션 관리';

// 기타
$lang['battery_swap_station_select_operator'] = '-연산자를 선택하십시오-';
$lang['battery_swap_station_operator'] = '운영자';
$lang['battery_swap_station_note'] = '노트';
$lang['battery_swap_station_bss_id'] = '배터리 렌탈 스테이션 번호';
$lang['battery_swap_station_bss_id_name'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 번호)';
$lang['battery_swap_station_bss_token'] = '배터리 렌탈 스테이션 토큰 ID';
$lang['battery_swap_station_ip_type'] = '배터리 렌탈 스테이션 IP 유형';
$lang['battery_swap_station_ip'] = '배터리 렌탈 스테이션 IP';
$lang['battery_swap_station_last_online'] = '최종 연결보고 시간';
$lang['battery_swap_station_version_no'] = '버전 번호';
$lang['battery_swap_station_version_date'] = '버전 날짜';
$lang['battery_swap_station_track_quantity'] = '트랙 수';
$lang['battery_swap_station_status'] = '상태';
$lang['battery_swap_station_statusY'] = '사용';
$lang['battery_swap_station_statusN'] = '사용 안함';
$lang['battery_swap_station_statusD'] = '삭제';
$lang['battery_swap_station_latitude'] = '위도';
$lang['battery_swap_station_longitude'] = '경도';
$lang['battery_swap_station_virtual_map'] = '실제 장면 맵';
$lang['battery_swap_station_location'] = '배터리 렌탈 스테이션 위치 이름';
$lang['battery_swap_station_exchange_num'] = '스왑 횟수';
$lang['battery_swap_station_canuse_battery_num'] = '임대 가능한 <br> 배터리 수';
$lang['battery_swap_station_map'] = '지도';
$lang['battery_swap_station_monitor'] = '실시간 모니터링';
$lang['battery_swap_station_station_no'] = '스왑 스테이션 버전 번호';

// 필드
$lang['so_num'] = '연산자';
$lang['note'] = '참고';
$lang['bss_id'] = '배터리 렌탈 스테이션 번호';
$lang['bss_token'] = '배터리 렌탈 스테이션 토큰 ID';
$lang['ip_type'] = '배터리 렌탈 스테이션 IP 유형';
$lang['ip_type_F'] = '고정 IP';
$lang['ip_type_C'] = 'IP 변경';
$lang['ip'] = '배터리 렌탈 스테이션 IP';

$lang['last_online'] = '최종 연결보고 시간';
$lang['version_no'] = '배터리 렌탈 스테이션 버전 번호';
$lang['version_date'] = '배터리 렌탈 스테이션 업데이트 날짜';
$lang['track_quantity'] = '배터리 대여소 트랙';
$lang['status'] = '배터리 렌탈 스테이션 상태';
$lang['city'] = '카운티 시티';
$lang['battery_swap_station_bss_no'] = '인클로저 번호';
$lang['sim_no'] = 'SIM 카드 번호';
/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

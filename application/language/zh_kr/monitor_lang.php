<?php
// 제목
$lang['monitor_management'] = '모니터링';

// 기타
$lang['monitor_province'] = '주도';
$lang['monitor_city'] = '도시';
$lang['monitor_district'] = '지역';
$lang['monitor_bss_id'] = '배터리 대여소 위치 이름 <br> (배터리 대여소 번호)';
$lang['monitor_bss_no'] = '캐비닛 번호';
$lang['monitor_track_no'] = '트랙 번호';
$lang['monitor_battery_id'] = '배터리 ID';
$lang['monitor_battery_temperature'] = '온도';
$lang['monitor_battery_voltage'] = '전압';
$lang['monitor_battery_amps'] = '현재';
$lang['monitor_battery_capacity'] = '배터리';
$lang['monitor_status'] = '유지 보수 상태 (수리 여부)';
$lang['monitor_remark'] = '비고';
$lang['monitor_user_name'] = '유지 보수 직원 이름';
$lang['monitor_user_id'] = '유지 보수 직원 번호';
$lang['monitor_icon'] = '작은 아이콘';

$lang['monitor_to_name'] = '운영자';
$lang['monitor_location'] = '사이트 이름';
$lang['monitor_charge_cycles'] = '사이클 수';
$lang['monitor_electrify_time'] = '충전 된 시간';
$lang['monitor_battery_status'] = '배터리 상태';

$lang['monitor_log_date'] = '최종보고 시간';
$lang['monitor_battery_type'] = '배터리 경고 유형';
/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

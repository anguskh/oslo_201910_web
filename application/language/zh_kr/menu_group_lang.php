<?php
//其他
$lang['menu_group_management'] = '메뉴 그룹 관리';
$lang['menu_group_input_name'] = '메뉴 그룹 이름을 입력하십시오';

// 필드
$lang['menu_group_group_name'] = '메뉴 그룹 이름';
$lang['menu_group_status'] = '메뉴 그룹 상태';

/* End of file menu_group_lang.php */
/* Location: ./system/language/zh_tw/menu_group_lang.php */

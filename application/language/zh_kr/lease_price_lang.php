<?php
// 기타
$lang['lease_price_management'] = '임대 가격 관리';
// 기타
$lang['lease_price_s_num'] = '임대 품목 번호';
$lang['lease_price_so_num'] = '운영자';
$lang['lease_price_sso_num'] = '하위 연산자';
$lang['lease_price_type'] = '임대 가격 카테고리';
$lang['lease_price_name'] = '가격 항목 이름';
$lang['lease_price_price'] = '임대료';
$lang['lease_price_remark'] = '비고';
$lang['lease_price_status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['s_num'] = '대여 아이템 번호';
$lang['so_num'] = '연산자';
$lang['sso_num'] = '하위 연산자';
$lang['type'] = '임대 요율 카테고리';
$lang['type_1'] = '시스템 공유';
$lang['type_2'] = '연산자 소유';
$lang['name'] = '가격 이름';
$lang['price'] = '대여 비용';
$lang['method'] = '가격 법';
$lang['비고'] = '비고';
$lang['status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
/* End of file lease_price_lang.php */
/* Location: ./system/language/zh_tw/lease_price_lang.php */

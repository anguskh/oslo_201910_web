<?php
// 제목
$lang['bss_advertising_management'] = 'BSS 광고 관리';

// 필드
$lang['advertising_explain'] = '설명';
$lang['advertising_type'] = '광고 유형';
$lang['advertising_upload_file'] = '파일 업로드';
$lang['advertising_updating_date'] = '업데이트 날짜';
$lang['total_num'] = '총 임대 스테이션 수';
$lang['enable'] = "활성화";
$lang['disable'] = "비활성화 됨";


// BSS 광고 관리 목록 다운로드
$lang['advertising_address'] = 'URL';
$lang['notify'] = '알림 <br> 알립니다';
$lang['advertising_status'] = "상태";
/* End of file bss_manage_lang.php */
/* Location: ./system/language/zh_tw/bss_manage_lang.php */

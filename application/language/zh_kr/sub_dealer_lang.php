<?php
// 기타
$lang['sub_dealer_management'] = '서브 딜러 관리';
// 기타
$lang['sub_dealer_s_num'] = '하위 딜러 ID';
$lang['sub_dealer_td_num'] = '판매자 이름';
$lang['sub_dealer_tsde01'] = '하위 딜러 이름';
$lang['sub_dealer_tsde02'] = '유형';
$lang['sub_dealer_tsde03'] = '연락처';
$lang['sub_dealer_tsde04'] = '전화 문의';
$lang['sub_dealer_tsde05'] = '휴대 전화';
$lang['sub_dealer_tsde06'] = '주소';
$lang['sub_dealer_tsde07'] = '이메일';
$lang['sub_dealer_tsde08'] = '플랫폼 거래 수수료';
$lang['sub_dealer_tsde09'] = '은행 송금 코드';
$lang['sub_dealer_tsde10'] = '은행 송금 계좌 번호';
$lang['sub_dealer_tsde11'] = '하나를 대신하여 하위 계정 처리';
$lang['sub_dealer_status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
// 필드
$lang['s_num'] = '하위 판매자 ID';
$lang['td_num'] = '딜러';
$lang['tsde01'] = '하위 판매자 이름';
$lang['tsde02'] = '하위 딜러 유형';
$lang['tsde02_1'] = '직접 관리';
$lang['tsde02_2'] = '가입';
$lang['tsde02_3'] = '기타';
$lang['tsde03'] = '하위 판매자 담당자 이름';
$lang['tsde04'] = '하위 딜러 연락처 전화';
$lang['tsde05'] = '보조 업체 휴대 전화';
$lang['tsde06'] = '보조 업체 주소';
$lang['tsde07'] = '보조 업체 이메일';
$lang['tsde08'] = '플랫폼 거래 수수료';
$lang['tsde09'] = '은행 송금 코드';
$lang['tsde10'] = '은행 송금 계좌 번호';
$lang['tsde11'] = '하위 계정을 대신하여 하위 계정 처리';
$lang['tsde11_Y'] = '예';
$lang['tsde11_N'] = '아니오';
$lang['status'] = '딜러 상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
/* End of file sub_dealer_lang.php */
/* Location: ./system/language/zh_tw/sub_dealer_lang.php */

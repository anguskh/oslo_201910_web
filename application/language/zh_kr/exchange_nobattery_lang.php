<?php
//标题
$lang['exchange_nobattery_management'] = '교환에서 총 배터리 시간을 사용할 수 없습니다';

// 필드
$lang['bss_id'] = '배터리 대여소 위치 이름 <br />
(배터리 렌탈 스테이션 번호) ';
$lang['log_date_start'] = '시작 시간';
$lang['log_date_end'] = '종료 시간';
$lang['minutes'] = '시간 : 분 : 초';
/* End of file exchange_nobattery_lang.php */
/* Location: ./system/language/zh_tw/exchange_nobattery_lang.php */

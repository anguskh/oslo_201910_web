<?php
//其他
$lang['operator_management'] = '운영자 관리';
// 기타
$lang['operator_s_num'] = '운영자 ID';
$lang['operator_top01'] = '운영자 이름';
$lang['operator_top02'] = '유형';
$lang['operator_top03'] = '주소록';
$lang['operator_top04'] = '전화 문의';
$lang['operator_top05'] = '휴대 전화';
$lang['operator_top06'] = '주소';
$lang['operator_top07'] = '이메일';
$lang['operator_top08'] = '플랫폼 거래 수수료';
$lang['operator_top09'] = '은행 송금 코드';
$lang['operator_top10'] = '은행 송금 계좌 번호';
$lang['operator_top11'] = '대리 운영 계정 처리';
$lang['operator_status'] = '상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['delete_ip'] = 'IP 삭제';
$lang['operator_tbss_count'] = '캐비닛 개수';
$lang['operator_tb_count'] = '배터리 수';

// 필드
$lang['s_num'] = '운영자 ID';
$lang['top01'] = '운영자 이름';
$lang['top02'] = '운영자 유형';
$lang['top02_1'] = '직접 관리';
$lang['top02_2'] = '가입';
$lang['top02_3'] = '기타';
$lang['top03'] = '운영자 연락처 이름';
$lang['top04'] = '이동 통신사 연락처';
$lang['top05'] = '운영자 휴대폰';
$lang['top06'] = '캐리어 주소';
$lang['top07'] = '이메일';
$lang['top09'] = '캐리어 코드';
$lang['status'] = '운영자 상태';
$lang['create_user'] = '파일러';
$lang['create_date'] = '생성 날짜';
$lang['create_ip'] = '빌딩 IP';
$lang['update_user'] = '사람 수정';
$lang['update_date'] = '수정일';
$lang['update_ip'] = 'IP 수정';
$lang['delete_user'] = '사람 삭제';
$lang['delete_date'] = '날짜 삭제';
$lang['enable'] = '사용';
$lang['disable'] = '비활성화';
/* End of file operator_lang.php */
/* Location: ./system/language/zh_tw/operator_lang.php */

<?php

$lang['password_current'] = '오래된 암호';
$lang['password_input_current'] = '이전 암호를 입력하십시오';
$lang['password_current_error'] = '오래된 암호가 잘못 입력되었습니다';
$lang['password_change'] = '비밀번호 변경';
$lang['password_new'] = '새 비밀번호';
$lang['password_input_new'] = '새로운 암호를 입력하십시오';
$lang['password_new_confirm'] = '새 비밀번호 확인';
$lang['password_input_new_confirm'] = '같은 새로운 암호를 입력하십시오';
$lang['password_first_login'] = '처음 로그인 할 때 비밀번호를 변경하십시오';
/* End of file password_lang.php */
/* Location: ./system/language/zh_tw/password_lang.php */

<?PHP
// 其他
$lang['feedback_management'] = '고객 피드백 관리';
// 其他
$lang['feedback_s_num'] = '시스템에 의한 자동 넘버링';
$lang['feedback_tm_num'] = '회원';
$lang['feedback_log_date'] = '고객 피드백 날짜 및 시간';
$lang['feedback_question_item'] = '질문';
$lang['feedback_question_additional'] = '질문 보충 설명';
$lang['feedback_coordinate'] = '고객 피드백의 위도 및 경도';
$lang['feedback_status'] = '고객 피드백 처리 결과';

//栏位
$lang['s_num'] = '시스템에 의한 자동 넘버링';
$lang['tm_num'] = '회원';
$lang['log_date'] = '고객 피드백 날짜 및 시간';
$lang['question_item'] = '질문 번호';
$lang['question_item_1'] = '권력을 얻지 못했습니다';
$lang['question_item_2'] = '오류 발견';
$lang['question_item_3'] = '주차 위반';
$lang['question_item_4'] = '기타 질문';
$lang['question_additional'] = '질문 추가 설명';
$lang['status'] = '고객 피드백 처리 결과';
$lang['status_N'] = '처리되지 않음';
$lang['status_P'] = '처리 중';
$lang['status_S'] = '처리됨';
$lang['coordinate'] = '고객 피드백의 위도 및 경도';


/* End of file feedback_lang.php */
/* Location: ./system/language/zh_tw/feedback_lang.php */
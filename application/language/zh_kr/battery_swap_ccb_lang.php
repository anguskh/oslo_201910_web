<?php
// 제목
$lang['battery_swap_ccb_management'] = '차용 된 CCB 관리';

// 기타
$lang['battery_swap_ccb_select_operator'] = '-연산자를 선택하십시오-';
$lang['battery_swap_ccb_select_station'] = '-배터리 대여소를 선택하십시오-';
$lang['battery_swap_ccb_operator'] = '운영자';
$lang['battery_swap_ccb_ccb_id'] = '배터리 임대를위한 CCB 일련 번호';
$lang['battery_swap_ccb_bss_id'] = '배터리 렌탈 스테이션 번호';
$lang['battery_swap_ccb_station'] = '배터리 렌탈 스테이션';
$lang['battery_swap_ccb_status'] = '상태';
$lang['battery_swap_ccb_statusY'] = '사용';
$lang['battery_swap_ccb_statusN'] = '사용 안함';
$lang['battery_swap_ccb_statusE'] = '오류';

// 필드
$lang['so_num'] = '연산자';
$lang['sb_num'] = '배터리 렌탈 스테이션';
$lang['ccb_id'] = '배터리 임대에 대한 CCB 일련 번호';
$lang['status'] = '배터리 렌탈 트랙 상태';
/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

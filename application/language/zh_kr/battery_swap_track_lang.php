<?php
// 제목
$lang['battery_swap_track_management'] = '배터리 렌탈 트랙 관리';

// 기타
$lang['battery_swap_track_select_operator'] = '-연산자를 선택하십시오-';
$lang['battery_swap_track_operator'] = '운영자';
$lang['battery_swap_track_bss_id'] = '배터리 렌탈 스테이션 번호';
$lang['battery_swap_track_no'] = '배터리 렌탈 스테이션 트랙 번호';
$lang['battery_swap_track_column_park'] = '배터리 렌탈 스테이션 트랙 배터리 상태';
$lang['battery_swap_track_column_parkY'] = '배터리 포함';
$lang['battery_swap_track_column_parkN'] = '배터리 없음';
$lang['battery_swap_track_column_charge'] = '배터리 렌탈 스테이션 트랙 충전 상태';
$lang['battery_swap_track_column_chargeY'] = '충전';
$lang['battery_swap_track_column_chargeN'] = '충전 완료';
$lang['battery_swap_track_column_chargeS'] = '대기';
$lang['battery_swap_track_column_chargeF'] = '오류';
$lang['battery_swap_track_battery_id'] = '배터리 일련 번호';
$lang['battery_swap_track_status'] = '상태';
$lang['battery_swap_track_statusY'] = '사용';
$lang['battery_swap_track_statusN'] = '사용 안함';
$lang['battery_swap_track_statusE'] = '오류';
$lang['battery_swap_track_bss_id_name'] = '배터리 대여 스테이션의 위치 이름 <br /> (배터리 대여 스테이션 번호)';

// 필드
$lang['so_num'] = '연산자 _num';
$lang['sb_num'] = '배터리 렌탈 스테이션 s_num';
$lang['track_no'] = '배터리 렌탈 스테이션 트랙 번호';
$lang['column_park'] = '배터리 렌탈 스테이션 트랙 배터리 상태';
$lang['column_charge'] = '배터리 렌탈 스테이션 트랙 충전 상태';
$lang['battery_id'] = '배터리 일련 번호';
$lang['status'] = '배터리 렌탈 스테이션 트랙 상태';

// 세부 사항
$lang['log_battery_leave_return_tv_num'] = '단위 ID';
$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_DorO_flag'] = '운영자 / 딜러 플래그';
// $lang['log_battery_leave_return_leave_do_num'] = '(Browrow) 연산자 / 딜러';
$lang['log_battery_leave_return_leave_do_num'] = '운영자 / 판매자';
$lang['log_battery_leave_return_leave_dso_num'] = '(차용) 하위 운영자 / 하위 딜러';
// $lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소';
$lang['log_battery_leave_return_battery_user_id'] = '사용자 ID';
// $lang['log_battery_leave_return_leave_request_date'] = '배터리 대여 요청 날짜 및 시간';
$lang['log_battery_leave_return_leave_request_date'] = '임대 / 반환 날짜 시간';
$lang['log_battery_leave_return_leave_status'] = '임대 / 반환 상태';
$lang['log_battery_leave_return_leave_coordinate'] = '대출 요청시 위도와 경도';
$lang['log_battery_leave_return_leave_date'] = '실제 배터리 임대 날짜 및 시간';
$lang['log_battery_leave_return_leave_track_no'] = '렌트 트랙 번호';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';
$lang['log_battery_leave_return_return_DorO_flag'] = '운영자 / 리셀러 플래그';
$lang['log_battery_leave_return_return_do_num'] = '(또한 운영자 / 딜러)';
$lang['log_battery_leave_return_return_dso_num'] = '(Re) 하위 연산자 / 하위 딜러';
$lang['log_battery_leave_return_return_sb_num'] = '배터리 대여소를 반환하십시오';
$lang['log_battery_leave_return_return_request_date'] = '배터리 반환 요청 날짜 및 시간';
$lang['log_battery_leave_return_return_coordinate'] = '반환 요청시 위도와 경도';
$lang['log_battery_leave_return_return_date'] = '배터리 반품에 대한 실제 날짜 및 시간';
$lang['log_battery_leave_return_return_track_no'] = '반환 트랙 번호';
$lang['log_battery_leave_return_return_battery_id'] = '배터리 번호 반환';
$lang['log_battery_leave_return_usage_time'] = '배터리 사용 시간';
$lang['log_battery_leave_return_charge_amount'] = '이 청구 금액 계산';
$lang['log_battery_l_r_type'] = '차입금 / 반품';
$lang['log_battery_leave_return_battery_id'] = '배터리 일련 번호';

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

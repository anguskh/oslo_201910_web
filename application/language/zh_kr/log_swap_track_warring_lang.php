<?php
$lang['log_swap_track_warring_inquiry'] = '배터리 렌탈 스테이션에 즉각적인 알람';
$lang['log_swap_track_warring_battery_id'] = '배터리 일련 번호';
// 필드
$lang['alarm_online_sn'] = '즉시 알람 알림 일련 번호';
$lang['sb_num'] = '배터리 렌탈 스테이션';
$lang['bss_id'] = '배터리 대여소';
$lang['bss_id_name'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 번호)';

$lang['track_no'] = '트랙';
$lang['log_date'] = '로그 시간';
$lang['battery_id'] = '배터리 일련 번호';
$lang['type'] = '알람 상태';
$lang['type_E'] = '오류';
$lang['type_N'] = '사용 안함';

$lang['status'] = '처리 상태';
$lang['status_1'] = '미확인';
$lang['status_2'] = '처리 중';
$lang['status_3'] = '완료';
$lang['status_desc'] = '처리 상태 설명';
$lang['view_date'] = '볼 수있는 날짜';
$lang['view_users'] = '보기 된 사람들의 프로필';
$lang['process_date'] = '처리 중 날짜';
$lang['process_user'] = '진행중인 인원';
$lang['finish_date'] = '완료 날짜';
$lang['finish_user'] = '완료된 사람 프로필';



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

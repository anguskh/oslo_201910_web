<?php
// 기타
$lang['config_management'] = '매개 변수 관리';
$lang['config_please_input'] = '입력 해주세요';
$lang['config_allow_ip_segment_msg'] = '네트워크 세그먼트를 입력하지 않으면 기본적으로 모든 IP를 입력 할 수 있습니다!';
$lang['config_allow_pc_name'] = '입력하지 않으면 기본적으로 모든 로그인 페이지를 입력 할 수 있습니다!';
$lang['config_P'] = '공식 파라미터';
$lang['config_T'] = '테스트 파라미터';

// 필드
$lang['config_config_sn'] = '매개 변수 일련 번호';
$lang['config_config_desc'] = '매개 변수 이름';
$lang['config_config_set'] = '매개 변수 값';
$lang['config_after_desc'] = '파라미터 단위';

/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */

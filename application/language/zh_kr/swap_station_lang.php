<?php
//标题
$lang['swap_station_management'] = '배터리 모니터링 보고서';

// 필드
$lang['bss_id'] = 'BSS 시리얼 번호';
$lang['log_date'] = '차용 스테이션에서 보낸 날짜';
$lang['battery_id'] = '배터리 일련 번호';
$lang['status'] = '배터리 상태';
$lang['battery_capacity'] = 'SOC';
$lang['battery_temperature'] = '배터리 온도';
$lang['battery_amps'] = '배터리 전류';
$lang['battery_voltage'] = '배터리 전압';
$lang['charge_cycles'] = '충전 횟수';
$lang['exchange_num'] = '배터리 교환 횟수';

/* End of file swap_station_lang.php */
/* Location: ./system/language/zh_tw/swap_station_lang.php */

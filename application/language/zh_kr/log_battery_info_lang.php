<?php
// 기타
$lang['log_battery_info_inquiry'] = '배터리 충전 시간 쿼리';

// 필드

$lang['log_battery_info_do_num'] = '대리점 / 운영자';
$lang['log_battery_info_so_num'] = '운영자';
$lang['log_battery_info_sb_num'] = '배터리 교환 스테이션';
$lang['log_battery_info_bss_id_name'] = '배터리 대여소 <br />의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_info_log_date'] = '로그 시간';
$lang['log_battery_info_track_no'] = '트랙 번호';
$lang['log_battery_info_battery_id'] = '배터리 일련 번호';
$lang['log_battery_info_vehicle_user_id'] = '차량 사용자 ID';
$lang['log_battery_info_battery_voltage'] = '배터리 전압';
$lang['log_battery_info_battery_cell_status'] = '배터리 상태';
$lang['log_battery_info_battery_amps'] = '배터리 전류';
$lang['log_battery_info_battery_capacity'] = 'SOC';
$lang['log_battery_info_battery_temperature'] = '배터리 온도';
$lang['log_battery_info_charge_cycles'] = '충전 횟수';
$lang['log_battery_info_electrify_time'] = '충전 된 시간';
$lang['log_battery_info_status'] = '배터리 상태';
/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */

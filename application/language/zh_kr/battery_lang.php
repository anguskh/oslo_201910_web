<?php
// 제목
$lang['battery_management'] = '배터리 관리';
$lang['battery_management_upload'] = '배터리 관리-수입 확인';
$lang['battery_detail_management'] = '배터리 관리 임대 내역';

// 기타
$lang['battery_status0'] = '정상 (충전)';
$lang['battery_status0_1'] = '정상';
$lang['battery_status1'] = '완전히 청구 됨';
$lang['battery_status2'] = '오류';
$lang['battery_status3'] = '유지 보수';
$lang['battery_status4'] = '사용 중지';
$lang['battery_status5'] = '배터리 도난';
$lang['battery_status6'] = '창고';

// 필드

$lang['battery_DorO_flag'] = '공급 업체 유형';
$lang['dealer'] = '대리점';
$lang['operator'] = '운영자';
$lang['battery_dealer'] = '딜러';
$lang['battery_operator'] = '운영자';
$lang['battery_M30'] = '차량';
$lang['battery_id'] = '배터리 일련 번호';
$lang['battery_manufacture_date'] = '공장 날짜';
$lang['battery_position'] = '배터리 위치';
$lang['battery_exchange_count'] = '배터리 교환 <br /> 누적 횟수';
$lang['battery_status'] = '배터리 상태';
$lang['battery_map'] = '지도';
$lang['last_report_datetime'] = '최종보고 시간';
$lang['bss_id'] = '배터리 대여소';
$lang['bss_id_name'] = '배터리 대여 스테이션 위치 이름 <br /> (배터리 대여 스테이션 일련 번호)';
$lang['track_no'] = '트랙';
$lang['sys_no'] = '버전 번호';
$lang['battery_recording'] = '임대 이력';
$lang['battery_station_position'] = '기지국 위치 <br /> (MCC, MNC, LAC, CELLID)';
$lang['battery_gps_manufacturer'] = 'GPS 제조업체';
$lang['battery_gps_version'] = 'GPS 펌웨어 버전';

// 세부 사항
$lang['log_battery_leave_return_tv_num'] = '단위 ID';
$lang['log_battery_leave_return_tm_num'] = '회원 이름';
$lang['log_battery_leave_return_system_log_date'] = '시스템 로그 시간';
$lang['log_battery_leave_return_leave_DorO_flag'] = '운영자 / 딜러 플래그';
$lang['log_battery_leave_return_leave_do_num'] = '(차용) 운영자 / 딜러';
$lang['log_battery_leave_return_leave_dso_num'] = '(차용) 하위 운영자 / 하위 딜러';
$lang['log_battery_leave_return_leave_sb_num'] = '배터리 대여소 대여';
// $lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소 대여';
$lang['log_battery_leave_return_leave_sb_num_name'] = '배터리 대여소의 위치 이름 (배터리 대여소 번호)';
$lang['log_battery_leave_return_battery_user_id'] = '사용자 ID';
$lang['log_battery_leave_return_leave_request_date'] = '배터리 대여 요청 날짜 시간';
$lang['log_battery_leave_return_leave_status'] = '임대 상태';
$lang['log_battery_leave_return_leave_coordinate'] = '대출 요청시 위도와 경도';
$lang['log_battery_leave_return_leave_date'] = '실제 배터리 임대 날짜 및 시간';
$lang['log_battery_leave_return_leave_track_no'] = '렌트 트랙 번호';
$lang['log_battery_leave_return_leave_battery_id'] = '렌탈 배터리 번호';
$lang['log_battery_leave_return_return_DorO_flag'] = '운영자 / 리셀러 플래그';
$lang['log_battery_leave_return_return_do_num'] = '(또한 운영자 / 딜러)';
$lang['log_battery_leave_return_return_dso_num'] = '(Re) 하위 연산자 / 하위 딜러';
$lang['log_battery_leave_return_return_sb_num'] = '배터리 대여소를 반환하십시오';
// $lang['log_battery_leave_return_return_sb_num_name'] = '배터리 대여소 반환';
$lang['log_battery_leave_return_return_sb_num_name'] = '배터리 대여 스테이션 (배터리 대여 스테이션 번호)의 위치를 ​​반환합니다';
$lang['log_battery_leave_return_return_request_date'] = '배터리 반환 요청 날짜 및 시간';
$lang['log_battery_leave_return_return_coordinate'] = '반환 요청시 위도와 경도';
$lang['log_battery_leave_return_return_date'] = '배터리 반품에 대한 실제 날짜 및 시간';
$lang['log_battery_leave_return_return_track_no'] = '반환 트랙 번호';
$lang['log_battery_leave_return_return_status'] = '반품 상태';
$lang['log_battery_leave_return_return_battery_id'] = '배터리 번호 반환';
$lang['log_battery_leave_return_usage_time'] = '배터리 사용 시간';
$lang['log_battery_leave_return_charge_amount'] = '이 청구 금액 계산';
$lang['log_battery_trail'] = '루트 트레일';

/* End of file battery_lang.php */
/* Location: ./system/language/zh_tw/battery_lang.php */

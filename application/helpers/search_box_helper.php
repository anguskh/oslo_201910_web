<?
//产生栏位搜寻
//$colArr 为清单显示栏位
//$searchData 为 先前搜寻的 session资料
//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
//目前类型只有判断date
//需要忽略的栏位array()
function create_search_box($colArr, $searchData = "", $fieldType = "", $ignoreArr = array(), $searchable = "", $twoInout = "", $threeInout = array(), $fourInout = array()){
	$searchArr = array();
	if($searchData){
		foreach($searchData as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];
			$option = $tmparr[1];
			$data = $tmparr[2];
			$searchArr[$field][0] = $field;
			$searchArr[$field][1] = $option;
			$searchArr[$field][2] = $data;
			if(isset($tmparr[3])){  //迄日资料
				$searchArr[$field][3] = $tmparr[3];
			}
		}
	}
	
	//产生search
	$search_box = "";
	$i=0;
	//搜寻方式
	$optionArr = array("=", "like", "between", "KW"); 
	$optionArr2 = array("oks", "nok"); 
	$search_box .= "<table id=\"search_table\">";
	foreach($colArr as $key => $value){
		if(in_array($key, $ignoreArr)){  
			continue;
		}
		$sessOption = "";
		$sessData = "";
		$sessData2 = "";  //迄日资料
		if(isset($searchArr[$key])){
			$sessOption = $searchArr[$key][1];
			$sessData = $searchArr[$key][2];
			if(isset($searchArr[$key][3])){  //迄日资料
				$sessData2 = $searchArr[$key][3];
			}
		}
		
		//栏位类型
		$searchFieldType = "";
		//特定关键字
		$fieldDefault = ((isset($fieldType[$key]))?$fieldType[$key]:'');
		//预设option
		$search_option = default_option($i, $optionArr, $sessOption, $fieldDefault);
		//预设input
		$search_input = default_input($i, $sessData, $fieldDefault);

		$td_colspan = 1;

		if(isset($fieldType[$key])){  //栏位类型是否有定义
			if(is_string($fieldType[$key])){  //字串, 包括日期,..等等
				if(strtolower($fieldType[$key]) == 'limit'){  //limit
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = "";
					$search_input = default_input($i, $sessData, $fieldDefault,"limit");

				}else if(strtolower($fieldType[$key]) == 'onok'){  //limit
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = default_option($i, $optionArr2, $sessOption, $fieldType,"Y");

				}else if(strtolower($fieldType[$key]) == 'date' || strtolower($fieldType[$key]) == 'month'){  //日期
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = date_option($i, $optionArr);
					$search_input = date_input($i, $sessData, $sessData2, strtolower($fieldType[$key]));
				}else if($fieldType[$key] == 'range'){
					//起始批号 ...等等
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = range_option($i, $optionArr);
					$search_input = range_input($i, $sessData, $sessData2, strtolower($fieldType[$key]));
				}
				else if($fieldType[$key] == 'selectmonth')
				{
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = date_month_option($i, $optionArr);
					$search_input = date_month_input($i, $sessData, 'month');
					$td_colspan = 2;
				}
				else if($fieldType[$key] == 'checkinput')
				{
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = checkbox_option($i, $optionArr);
					$search_input = checkbox_input($i, '1', $sessData);
				}
			}else if(is_array($fieldType[$key])){  //阵列资料
				if(isset($searchable[$key])){
					$multiple = true;
					if(is_array($searchable[$key])){  //是阵列参数
						if($searchable[$key][0] == 'Y'){
							if(isset($searchable[$key][1])){
								$multiple = $searchable[$key][1];
							}
						}else{//目前没有N的设定
						}
					}else{ //字串参数
						if($searchable[$key] == 'Y'){
							// $multiple = true;
						}else{  //目前没有N的设定
							// $multiple = true;
						}
					}
					
					$td_colspan = 2;
					$searchFieldType = "searchable";
					$search_input = arr_input2($i, $fieldType[$key], $sessData, $key, $multiple);	
				}else if (isset($twoInout[$key])) {
					$searchFieldType = "";
					$search_input = arr_input3($i, $fieldType[$key], $sessData, $key);
				}else if (isset($threeInout[$key])) {
					$searchFieldType = "";
					$search_input = arr_input4($i, $fieldType[$key], $sessData, $key);
				}else{
					$searchFieldType = "";
					$fourInout[$key] = ((isset($fourInout[$key]))?$fourInout[$key]:'');
					$search_input = arr_input($i, $fieldType[$key], $sessData, $fourInout[$key]);
				}
			}
		}else{  //无定义, 就是一般input或者是非资料库对应select
			//查询该栏位有无非资料库的对应内容, 例如 0 = 停用, 1 = 启用
			//有则为select提供选择, 无则是一般input
			$aliasArr = alias_colName($key);
			if($aliasArr){   //有对应, 非资料库内容
				$search_input = alias_input($i, $aliasArr, $sessData);
			}
		}
		
		$search_box .= "<tr class=\"tr_{$key}\">";
		$search_box .= "<td class=\"td_label_{$key}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchField_{$i}\" value=\"{$key}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchFieldType_{$i}\" value=\"{$searchFieldType}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchNum\" value=\"{$i}\">";
		$search_box .= "	<label>{$value}</label>";
		$search_box .= "</td>";

		//非searchable需要 = or like
		if(!isset($searchable[$key]) && $searchFieldType != 'selectmonth' ){
			$search_box .= "<td class=\"td_option_{$key} date_zone\"  >";
			$search_box .= $search_option;
			$search_box .= "</td>";
		}
		$search_box .= "<td colspan=\"{$td_colspan}\" class=\"td_data_{$key} td_search\" >";
		$search_box .= $search_input;
		$search_box .= "</td>";
		$search_box .= "</tr>";
		$i++;
	}
	$search_box .= "</table>";
	//加清空與搜尋
	$search_box .= '<div class="btnzone">';
	$search_box .= '<input type="button" id="btClear" class="buttondark--thin" value="清 空" /> ';
	$search_box .= '<input type="button" id="btSearch" class="buttongreen--thin" value="搜 寻" />';
	$search_box .= '</div>';
	// exit();

	return $search_box;
}

//日期选项
function date_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"{$optionArr[2]}\" selected>{$optionArr[2]}</option>";
	$tmp .= "	</select>";
	
	$tmp .= "<div class=\"start_end_label\">起/讫</div>";
	return $tmp;
}

//月份日期选项
function date_month_option($i, $optionArr){
	$tmp = "";
	// $tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	// $tmp .= "	<option value=\"{$optionArr[0]}\" selected>{$optionArr[0]}</option>";
	// $tmp .= "	</select>";
	return $tmp;
}

//起始input选项
function range_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"{$optionArr[2]}\" selected>{$optionArr[2]}</option>";
	$tmp .= "	</select>";
	
	$tmp .= "<div class=\"start_end_label\">起/讫</div>";
	return $tmp;
}
function searchnok_option(){


}
//预设选项
function default_option($i, $optionArr, $sessOption, $fieldType, $nok = ""){
	$optionArrName = array("="=>"等于","like"=> "相似(右)", "between"=>"区间", "KW"=>"相似(全)", "oks"=> "包含" , "nok"=>"過濾"); 

	if($fieldType == 'KW'){
		$sessOption = (($sessOption=='')?'KW':$sessOption);
	}
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline\" >";
	//对应类别选择, 目前只有 = 和 like
	foreach($optionArr as $opKey => $opValue){
		if($nok != "Y"){
			if($opKey == 2){  //略过brtween, 是日期在用的
				continue;
			}
		}
		$selected = "";
		if($opValue == $sessOption){
			$selected = " selected";
		}
		$opName = $optionArrName[$opValue];
		$tmp .= "	<option value=\"{$opValue}\" {$selected}>{$opName}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//预设
function default_input($i, $sessData, $fieldType, $limit_type = ""){
	if($limit_type == 'limit'){
			$tmp = "	<input type=\"number\" class=\"searchData\" name=\"searchData_{$i}\" value=\"{$sessData}\"  max=\"50\" min=\"1\">";
	}else{
		if($fieldType == 'KW'){
			$tmp = "	<input type=\"text\" class=\"searchData\" name=\"searchData_{$i}\" value=\"{$sessData}\" placeholder=\"关键字搜寻\">";
		}else{
			$tmp = "	<input type=\"text\" class=\"searchData\" name=\"searchData_{$i}\" value=\"{$sessData}\">";
		}
	}
	return $tmp;
}


//checkbox input
function checkbox_input($i, $sessValue, $sessData){
	$checked = "";
	if($sessData == '1'){
		$checked = "checked";
	}
	$tmp = "	<input type=\"checkbox\" class=\"searchcheckbox\" name=\"searchData_{$i}\" value=\"{$sessValue}\" {$checked}>";
	return $tmp;
}
//checkbox选项
function checkbox_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"checkbox\" selected>checkbox</option>";
	$tmp .= "	</select>";
	
	return $tmp;
}

//有对应非资料库内容
function alias_input($i, $aliasArr, $sessData){
	$tmp = "";
	$tmp .= "	<select name=\"searchData_{$i}\" class=\"searchData_sel\" >";
	foreach($aliasArr as $key => $value){
		$selected = "";
		//转成字串
		$key = $key.'';
		if($sessData === $key){
			$selected = " selected";
		}
		$tmp .= "	<option value=\"{$key}\" {$selected}>{$value}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//日期输入
function date_input($i, $sessData, $sessData2, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$tmp .= "	<input type=\"date\" class=\"searchdateData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	$tmp .= "	<input type=\"date\" class=\"searchdateData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	return $tmp;
}

//月份日期输入
function date_month_input($i, $sessData, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$sessData = "20".substr($sessData,0,2)."-".substr($sessData,2,2);
	$tmp .= "	<input type=\"month\" id='datepicker' name=\"searchData_{$i}\" value=\"{$sessData}\">";
	return $tmp;
}

//起始输入
function range_input($i, $sessData, $sessData2, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$tmp .= "	<input type=\"text\" class=\"searchdateData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	$tmp .= "	<input type=\"text\" class=\"searchdateData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	return $tmp;
}

//阵列资料
function arr_input($i, $arrData, $sessData, $f_key){
	$tmp = "";
	$tmp .= "	<select name=\"searchData_{$i}\" class=\"searchData_sel\">";
	$tmp .= "	<option value=\"\"></option>";

	foreach($arrData as $arrkey => $arrvalue){
		if($f_key == 'Y'){
			//value以s_num為主
			$arrkeys = array_keys($arrvalue);
			$getKey = $arrkeys[0]; 
			$value = $arrvalue[$getKey];
			$getKey = $arrkeys[1]; 
			$label = $arrvalue[$getKey];
		}else{
			if(!is_array($arrvalue)){
				$getKey = $arrkey; 
				$value = $arrkey;
				$value = sprintf($value); //把數字變字串
				$label = $arrvalue;
			}else{
				$arrkeys = array_keys($arrvalue);  //取第二个key, 因为通常list 阵列, 第二笔资料都是所需的资料
				//先都取第二个key, 到时再来看要怎样改成找第一个key, 或者全部功能都一起改
				$getKey = $arrkeys[1]; 
				$value = $arrvalue[$getKey];
				$getKey = $arrkeys[1]; 
				$label = $arrvalue[$getKey];
			}
		}
		$selected = "";
		if($sessData === $value){
			$selected = " selected";
		}

		$tmp .= "	<option value=\"{$value}\" {$selected}>{$label}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//阵列资料 搜寻下拉选单
function arr_input2($i, $arrData, $sessData, $key, $multiple = true){
	if($multiple){
		$multiple_str = "multiple=\"multiple\"";
	}else{
		$multiple_str = "";
	}
	//$sessData 用逗号区隔
	$sessArr = explode(',',$sessData);
	$tmp = "";
	$tmp .= "	<select id=\"{$key}\" name=\"searchData_{$i}[]\" {$multiple_str}>";

	foreach($arrData as $arrkey => $arrvalue){
		
		$arrkeys = array_keys($arrvalue);  //取第二个key, 因为通常list 阵列, 第二笔资料都是所需的资料
		$CodeKey = $arrkeys[0]; 
		if(isset($arrkeys[1])){  
			$getKey = $arrkeys[1]; 
			$value = $arrvalue[$CodeKey].'｜'.$arrvalue[$getKey];
		}else{  //如果找不到栏位, 就已第一个为主
			$getKey = $arrkeys[0];
			$value = $arrvalue[$CodeKey];
		}
		
		$selected = "";

		if(in_array("'".$arrvalue[$CodeKey]."'", $sessArr)) { 
			
			$selected = " selected";
		}

		$tmp .= "	<option value=\"{$arrvalue[$CodeKey]}\" {$selected}>{$value}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}
//阵列资料
function arr_input3($i, $arrData, $sessData, $key){
	$tmp = "";
	$tmp .= "	<input type='text' id='{$key}_input' class=\"label_inline twoInout_input\" value=\"{$sessData}\">";
	$tmp .= "	<select id=\"{$key}\" name=\"searchData_{$i}\" class=\"twoInout_sel\">";
	$tmp .= "	<option value=\"\"></option>";

	foreach($arrData as $arrkey => $arrvalue){
		$arrkeys = array_keys($arrvalue);  //取第二个key, 因为通常list 阵列, 第二笔资料都是所需的资料
		//先都取第二个key, 到时再来看要怎样改成找第一个key, 或者全部功能都一起改
		$getKey = $arrkeys[0]; 
		$value = $arrvalue[$getKey];
		$getKey = $arrkeys[1]; 
		$label = $arrvalue[$getKey];
		$selected = "";
		if($sessData === $value){
			$selected = " selected";
		}

		$attr ="";
		if ($key == "item_no") {
			$kind_no = $arrvalue["kind_no"];
			$attr =" kind_no='{$kind_no}' ";
		}

		$tmp .= " <option value=\"{$value}\" val_name=\"{$label}\" {$attr} {$selected}>{$label}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//阵列资料
//阵列资料
function arr_input4($i, $arrData, $sessData){
	$tmp = "";
	$tmp .= "	<select name=\"searchData_{$i}\" class=\"searchData_sel\">";
	$tmp .= "	<option value=\"\"></option>";

	foreach($arrData as $arrkey => $arrvalue){
		if(!is_array($arrvalue)){
			$getKey = $arrkey; 
			$value = $arrkey;
			$value = sprintf($value); //把數字變字串
			$label = $arrvalue;
		}else{
			$arrkeys = array_keys($arrvalue);  //取第二个key, 因为通常list 阵列, 第二笔资料都是所需的资料
			//先都取第二个key, 到时再来看要怎样改成找第一个key, 或者全部功能都一起改
			$getKey = $arrkeys[1]; 
			$value = $arrvalue[$getKey];
			$getKey = $arrkeys[2]; 
			$label = $arrvalue[$getKey];
		}
		$selected = "";
		if($sessData === $value){
			$selected = " selected";
		}

		$tmp .= "	<option value=\"{$value}\" {$selected}>{$label}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}


?>
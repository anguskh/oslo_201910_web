<?
//接收搜寻资料, 转为SQL语法
//$searchData 清单搜寻的条件
//$defAlias 预设栏位主别名 如 {$defAlias}.Fields
//$setAlias 自订栏位主别名 如 $setAlias = array( "Fields"=> "Alias" );, 可多组	
function setSearchLimit($searchData = "", $defAlias = "",$setAlias = array()){
	if($searchData != ""){
		$limit_num = "";
		foreach($searchData as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];
			$option = $tmparr[1];
			$data = $tmparr[2];
			if($field == 'show_limit'){
				//顯示幾名
				$limit_num = $data;
			}
		}
		// exit();
		return $limit_num;
	}else{
		return;
	}
}
function setSearchData($searchData = "", $defAlias = "",$setAlias = array()){
	if($searchData != ""){
		$searchStr = "";
		$searchArr = "";
		foreach($searchData as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];
			$option = $tmparr[1];
			$data = $tmparr[2];
			//SQL别名
			if(isset($setAlias[$field])){
				if(strpos($setAlias[$field], '.') !== false){  //有.代表有别名+栏位名称, 用来取代原先设定的别名
					$alias = "";
					$field = $setAlias[$field];
				}else if($setAlias[$field] == 'Y'){
					//直接用他自己取的别名
					$alias = '';
				}else{  //没有., 就直接别名+栏位名称
					$alias = $setAlias[$field].'.';
				}
			}else if($defAlias != ""){
				$alias = $defAlias.'.';
			}else{
				$alias = "";
			}
			//搜寻类别
			$like = "";
			if($field == 'show_limit'){
				//顯示幾名 不處理
				//$searchArr = "N";
			}else{
				if($option == "oks"){
					$option = "IN";
					$d_arr = explode(",", $data);
					$data = join( "','", $d_arr);
					$data = "'".$data	."'";
					$searchArr[] = "{$alias}{$field} {$option} ({$data})";
				}else if($option == "nok"){
					$option = "NOT IN";
					$d_arr = explode(",", $data);
					$data = join( "','", $d_arr);
					$data = "'".$data	."'";
					$searchArr[] = "{$alias}{$field} {$option} ({$data})";
				}else if($option == "KW"){
					$option = 'like';
					$like = "%";
					$searchArr[] = "{$alias}{$field} {$option} '{$like}{$data}{$like}'";
				}else if($option == "between"){  //日期
					$data2 = $tmparr[3];  //$data 就是起日, 第四笔资料是迄日
					if($data != "" && $data2 != ""){  //有起讫日
						//$searchArr[] = "{$alias}{$field} {$option} '{$data}' and '{$data2}'";
						$searchArr[] = "date({$alias}{$field}) {$option} '{$data}' and '{$data2}'";
					}else if($data != ""){
						$searchArr[] = "date({$alias}{$field}) = '{$data}'";
					}else if($data2 != ""){
						$searchArr[] = "date({$alias}{$field}) = '{$data2}'";
					}
				}else if($option == "IN"){ //多笔
					$searchArr[] = "{$alias}{$field} {$option} ({$data})";
				}else if($option == ""){ //checkbox
					$option = "=";
					$searchArr[] = "{$alias}{$field} {$option} ({$data})";
				}else{
					$searchArr[] = "{$alias}{$field} {$option} '{$data}{$like}'";
				}
			}
		}

		if($searchArr != ""){
			$searchStr = join(' and ', $searchArr);
			$searchStr = " and ({$searchStr})";
		}else{
			$searchArr = "";
		}
		
		// echo($searchStr);
		// exit();
		return $searchStr;
	}else{
		return;
	}
}

//将清单送出的搜寻资料转成阵列
function setSearch2Arr($postData){
	$searchArr = array();
	$searchNum = $postData["searchNum"];
	
	for($i = 0; $i <= $searchNum; $i++){
		//栏位类别
		$searchFieldType = $postData["searchFieldType_{$i}"];
		$searchField = $postData["searchField_{$i}"];
		$searchOption = isset($postData["searchOption_{$i}"])?$postData["searchOption_{$i}"]:'';
		if(strtolower($searchFieldType) == 'date' || strtolower($searchFieldType) == 'month'){  //为日期
			for($k = 0; $k <= 1; $k++){  //1起日, 2迄日
				${"searchData_".$k} = addslashes($postData["searchData_{$i}_{$k}"]);
				if(${"searchData_".$k} != ""){
					${"searchData_".$k} = str_replace("/", "-", ${"searchData_".$k});
				}
			}
			if($searchData_0 != "" || $searchData_1 != ""){
				$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData_0}[:;]{$searchData_1}";
			}
		}else if(strtolower($searchFieldType) == 'range'){  //起始input
			for($k = 0; $k <= 1; $k++){  //1开始, 2结束
				${"searchData_".$k} = addslashes($postData["searchData_{$i}_{$k}"]);
				if(${"searchData_".$k} != ""){
					${"searchData_".$k} = str_replace("/", "-", ${"searchData_".$k});
				}
			}
			if($searchData_0 != "" || $searchData_1 != ""){
				$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData_0}[:;]{$searchData_1}";
			}
		}else if(strtolower($searchFieldType) == 'searchable'){  //searchable套件
		
			if(isset($postData["searchData_{$i}"]) ){
				$ckbSelArr = '';
				if(count($postData["searchData_{$i}"])>0){

					$ckbSelArr = $postData["searchData_{$i}"];
					$IdStr = join( "','", $ckbSelArr );
					$IdStr = "'".$IdStr."'";

					$searchArr[] = "{$searchField}[:;]IN[:;]{$IdStr}";

					//remark[:;]like[:;]123
				}
			}
		}else if(strtolower($searchFieldType) == 'selectmonth'){  //选择月
			$searchData_0 = $postData["searchData_0"];
			if($searchData_0 != ""){
				$searchData_0 = substr($searchData_0,2,2).substr($searchData_0,5,2);
				$searchArr[] = "{$searchField}[:;]=[:;]{$searchData_0}";
			}
		}else if(strtolower($searchFieldType) == 'checkinput'){  //checkinput
			if(isset($postData["searchData_{$i}"])){
				$searchData = addslashes($postData["searchData_{$i}"]);
				$searchArr[] = "{$searchField}[:;]=[:;]{$searchData}";
			}
		}else{  //其他字串搜寻
			if(isset($postData["searchData_{$i}"])){
				$searchData = addslashes($postData["searchData_{$i}"]);
				if($searchData != ""){
					$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData}";
				}
			}
		}

	}
	return $searchArr;
}

//join对应, 依照搜寻条件来加入join查询, 仅SQL count 语法使用
function leftJoinSet($search, $joinMap){
	$joinStr = "";
	if(!empty( $search)){
		foreach($search as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];  //栏位
			if(isset($joinMap[$field])){  //有找到
				$joinStr .= $joinMap[$field];
			}else{  //没找到
				foreach($joinMap as $key2 => $value2){  //找joiMap的key值是否有可以分割的key
					$arrExp = explode(",", $key2);
					if(count($arrExp) >= 2){  //分割
						if(in_array($field, $arrExp)){  //有找到
							$joinStr .= $value2;
							unset($joinMap[$key2]); //移除此笔对应, 避免又重复join
						}
						break;
					}
				}
			}
		}
	}
	return $joinStr;
}

?>
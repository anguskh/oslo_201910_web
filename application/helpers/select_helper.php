<?
	//产生select
	//$list 来源阵列清单
	//$keyData 传入要对应的资料
	//$defMsg 预设显示文字
	//$filterArr 来源其他栏位给予input 其他属性值, 如果是二微阵列则value是要预先过滤的值
	//$defMsg 当为阵列, $defMsg[0] 为选择是字串, $defMsg[1] 为value值
	function create_select_option($list, $keyData = "", $defMsg = "", $filterArr = array()){
		if( !empty( $list ) )
		{
			if(is_array($defMsg)){  //是阵列
				echo "<option value=\"{$defMsg[1]}\">{$defMsg[0]}　</option>\n";
			}else if($defMsg === false){ //false, 没有option
				
			}else if($defMsg != ""){  //有字串, 没字串就不产生文字select
				echo "<option value=\"\">{$defMsg}　</option>\n";
			}else{
				echo "<option value=\"\"></option>\n";
			}
			// print_r($list);
			foreach ($list as $key => $value) {
				$filterFlag = true;
				$keyArr = array_keys($value);
				$showValue = $value[$keyArr[0]];
				if(isset($keyArr[1])){
					$showName = $value[$keyArr[1]];
				}else{
					$showName = $value[$keyArr[0]];
				}
				
				
				$filterStr = "";
				if(count($filterArr) > 0){
					foreach($filterArr as $filterKey => $filterVal){
						$filterFieldVal = "";
						if(is_string($filterKey)){  //判断是否为字串, 为字串代表$filterKey 为过滤栏位, $filterVal为要过滤的栏位值
							$filterFlag = false;
							$filterField = $filterKey;
							$filterFieldVal = $filterVal;
						}else{  //非字串, 则$filterVal为过滤栏位, $filterKey只是但纯阵列索引
							$filterField = $filterVal;
						}
						$filterStr .= $filterField."=\"{$value[$filterField]}\" "; 
						
						if($filterFieldVal != "" && $value[$filterField] == $filterFieldVal){
							$filterFlag = true;
						}
					}
				}

				if( $keyData == $showValue && $filterFlag){
					$selected = " selected";
				}else{
					$selected = "";
				}
					//echo "<option value=\"{$showValue}\" {$filterStr} {$selected}>[{$showValue}] {$showName}</option>\n";
					echo "<option value=\"{$showValue}\" {$filterStr} {$selected} val_name=\"{$showName}\">{$showName}</option>\n";
			}
		}
		else
		{
			echo "<option value=\"\">{$defMsg}　</option>\n";
		}
	}

	
	//用于先过滤类别, 再产生资料
	function create_select_option_2($list, $keyData = "", $defMsg = "", $filterArr = array()){
		$str = "";
		if( !empty( $list ) )
		{
			if(count($filterArr) > 0){
				$first_key = array_shift($filterArr);
				if(is_string($first_key)){
					//$kind = $filterArr[$first_key];
					$list = $list[$first_key];
				}
			}
			$str .= "<option value=\"\">{$defMsg}　</option>\n";
			foreach ($list as $key => $value) {
				$filterFlag = true;
				$keyArr = array_keys($value);
				$showValue = $value[$keyArr[0]];
				$showName = $value[$keyArr[1]];
				
				$filterStr = "";
				if(count($filterArr) > 0){
					foreach($filterArr as $filterKey => $filterVal){
						$filterFieldVal = "";
						if(is_string($filterKey)){  //判断是否为字串, 为字串代表$filterKey 为过滤栏位, $filterVal为要过滤的栏位值
							$filterFlag = false;
							$filterField = $filterKey;
							$filterFieldVal = $filterVal;
						}else{  //非字串, 则$filterVal为过滤栏位, $filterKey只是但纯阵列索引
							$filterField = $filterVal;
						}
						$filterStr .= $filterField."=\"{$value[$filterField]}\" "; 
						
						if($filterFieldVal != "" && $value[$filterField] == $filterFieldVal){
							$filterFlag = true;
						}
					}
				}

				if( $keyData == $showValue && $filterFlag)
					$selected = " selected";
				else
					$selected = "";
					//echo "<option value=\"{$showValue}\" {$filterStr} {$selected}>[{$showValue}] {$showName}</option>\n";
					$str .= "<option value=\"{$showValue}\" {$filterStr} {$selected} val_name=\"{$showName}\">{$showName}</option>\n";
			}
		}
		else
		{
			$str .= "<option value=\"\">{$defMsg}　</option>\n";
		}
		return $str;
	}

?>
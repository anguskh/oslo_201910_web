<?
//取得目前url的乱数编号
function get_url_random(){
	$CI =& get_instance();
	$random_function = $CI->uri->segment(2);
	$random_function = explode('__',$random_function);
	$fn_random = "";
	if(isset($random_function[1])){
		$fn_random = $random_function[1];
	}
	return $fn_random;
}

//取得目前url的乱数编号 [function name.'__'.random]
function get_fetch_class_random(){
	$CI =& get_instance();
	$random_function = $CI->uri->segment(2);
	return $random_function;
}

//取得目前url至function name 
function get_full_url_random(){
	$CI =& get_instance();
	$web = $CI->config->item('base_url');
	$fetch_directory = $CI->router->fetch_directory();
	$random_function = $CI->uri->segment(2);
	return $web.$fetch_directory.$random_function;
}

// 去除空格UTF-8版
function mb_trim($string, $trim_chars = '\s'){
    return preg_replace('/^['.$trim_chars.']*(?U)(.*)['.$trim_chars.']*$/u', '\\1',$string);
}

//日期是空的给null, 避免存档出现0000-00-00
function fn_empty2null($dataArr, $dateArr = array()){
	if(!empty($dateArr)){
		foreach($dateArr as $key => $value){
			if(isset($dataArr[$value])){
				if($dataArr[$value] == '' || $dataArr[$value] == '0000-00-00' || $dataArr[$value] == '0000-00-00 00:00:00'){
					$dataArr[$value] = null;
				}
			}else{
				$dataArr[$value] = null;
			}
		}
	}
	return $dataArr;
}

//数字转国字大写
function num2Zh( $numInp ) {
	$numInp = trim( $numInp );
	$numInp = str_replace(' ', '', $numInp);
	
	$numInp = number_format( (float)$numInp, 0, '', '' );	//-- 转成数字
	if( $numInp == 0 ) {
		return '零';
	}
	
	$zh = array('零', '壹', '贰', '参', '肆', '伍', '陆', '柒', '捌', '玖');
	$units = array('', '仟', '佰', '拾');
	$great_units = array('', '万', '亿', '兆');

	$num_len = strlen($numInp);
	$num_ary = str_split($numInp);	//-- 转成字元阵列

	$gu = (int)($num_len / 4);	//-- 最大单位
	$mod = $num_len % 4;		//-- 超过大单位的 数字长度
	$gu = ( $mod == 0 ) ? $gu - 1 : $gu;

	$numOut = '';	//-- 最终输出字串
	$count = 1;
	$lastNum = '';
	foreach( $num_ary as $num ) {
		$lastNum = $num;
		if( $lastNum != '0' ) {
			$numOut .= $zh[ $num ];
		}

		$tmp = ($count-$mod) % 4;
		$tmp = ( $tmp < 0 ) ? $tmp + 4 : $tmp;
		
		if( $count == $mod ) {
			$numOut = trimLastZero( $numOut );
			
			//-- 加上大单位
			$numOut .= $great_units[ $gu ];
			$gu--;
		}
		else if( $tmp == 0 ) {
			//-- 最后字元为 '零' 时，去掉。
			if( $gu == 1 ) {
				$numOut = trimLastZero( $numOut );
			}
			
			//-- 加上大单位
			$numOut .= $great_units[ $gu ];
			$gu--;
		}
		else if( $tmp > 0 && $num !== '0' ) {
			//-- 加上 仟佰拾 单位
			$numOut .= $units[ $tmp ];
		}
		else if( $tmp > 0 && $num === '0'  ) {
			$numOut .= '零';
		}
		
		$count++;
	}

	//-- 去掉多余的 '零'，只显示一个
	$numOut = preg_replace('/(零)+/', '零', $numOut);	
	$numOut = trimLastZero( $numOut );
	return $numOut;
}

//-- 去除尾数的 "零"
function trimLastZero( $numOut ) {
	
	$final_str = mb_strlen( $numOut, 'UTF-8' ) - 1;
	if( mb_substr( $numOut, $final_str, 1, 'UTF-8' ) == '零' ) {
		$numOut = mb_substr( $numOut, 0, $final_str, 'UTF-8' );
	}
	return $numOut;
}
?>
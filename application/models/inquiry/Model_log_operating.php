<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_operating extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		//$whereStr  = "and su.ACQUIRE_BANK_ID = '{$this->session->userdata('selBankSN')}' " ;
		$whereStr = "";
		
		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and su.user_name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"user_name" => "su"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lo', $setAlias);
		else $whereStr .= "" ;

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;

			$SQLCmd  = "select count(*) cnt 
						from log_operating lo 
						left join sys_user su ON su.user_sn = lo.user_sn and su.status <> 'D'
						where {$whereStr}";
			$rs = $this->db_query($SQLCmd) ;
			return $rs[0]["cnt"];
		}
		/*$SQLCmd  = "select sum(cnt) as cnt from (
						SELECT count(*) cnt 
						FROM log_operating lo 
						left join 
							(
								select su.user_sn, su.user_id, CONCAT(su.user_id, su.user_name) as user_name  
								from sys_user su
								where su.status <> 'D'
							) su on lo.user_sn = su.user_sn 
						WHERE lo.user_sn = su.user_sn 
						{$whereStr} {$whereStr2} 
						) tmp
					";*/
		//$rs = $this->db_query($SQLCmd) ;
		//return $rs[0]["cnt"];
		/*if($this->session->userdata('show_list_mode') == 'P'){
			$rs = $this->db_query($SQLCmd) ;
			return $rs[0]["cnt"];
		}else{
			if(empty( $search)){
				return $this->session->userdata('paging_rows');
			}else{
				$rs = $this->db_query($SQLCmd) ;
				return $rs[0]["cnt"];
			}
		}*/
	}

	/**
	 * 取得清单
	 */
	public function getInfo( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		//$whereStr  = "and su.ACQUIRE_BANK_ID = '{$this->session->userdata('selBankSN')}' " ;
		$whereStr = "";
		$whereStr2 = "";

		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and su.user_name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"user_name" => "su"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lo', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = "totaldata.";
			}
			$sql_orderby = " order by ".$ali.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lo.SN desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;

			$SQLCmd  = "select lo.*, su.user_name
						from log_operating lo 
						left join sys_user su ON su.user_sn = lo.user_sn and su.status <> 'D'
						where {$whereStr}
						{$sql_orderby} 
						LIMIT {$startRow} , {$offset} ";
			$rs = $this->db_query($SQLCmd) ;
			//新增纪录档
			$arrSearch = array();
			if($search_txt != ''){
				$arrSearch['user_name'] = $search_txt;
			}
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			
			return $rs;
		}
	}
	
}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_battery_inventory extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;


		$SQLCmd = "SELECT count(*) cnt 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' {$whereStr} and tb.status <> 'D'" ;

		$rs = $this->db_query($SQLCmd) ;



		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tb.s_num ";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;

		$SQLCmd = "SELECT ldi.unit_id as battery_id,
							tb.manufacture_date,
							CONCAT(ldi.latitude,',',ldi.longitude) as gps_position,
							CONCAT(ldi. battery_capacity,'%') as battery_capacity 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' and tb.status <> 'D' 
						{$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset}" ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	

	public function search_all( $whereStr = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " ";

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, '');
		else $whereStr .= "" ;


 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tb.DorO_flag = 'O' and tb.do_num = {$to_flag_num}" ;
		}
		
		$SQLCmd = "SELECT tb.*,tv.unit_id,(CASE tb.DorO_flag WHEN 'D' THEN td.tde01 WHEN 'O' THEN top.top01 END ) as do_name, tbss.bss_id, tbst.track_no, tbss.location
  						FROM tbl_battery tb 
  						LEFT JOIN tbl_dealer td ON tb.do_num = td.s_num 
  						LEFT JOIN tbl_operator top ON tb.do_num = top.s_num 
  						LEFT JOIN tbl_vehicle tv ON tb.sv_num = tv.s_num 
						left join tbl_battery_swap_track tbst ON tbst.battery_id = tb.battery_id and tbst.status <> 'D'
						left join tbl_battery_swap_station tbss ON tbss.s_num = tbst.sb_num and tbss.status <> 'D'
  						where tb.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		$battery_1 = 0;
		$battery_2 = 0;
		$battery_3 = 0;
		$n_battery = array();
		$battery_num = count($rs);
		$n=0;
		foreach($rs as $rs_arr){
			if($rs_arr['position'] == 'B'){
				$battery_2++;
			}else if($rs_arr['position'] == 'V'){
				$battery_1++;
			}else{
				$battery_3++;
				//寫入detail明細
				$n_battery[$n]['battery_id'] = $rs_arr['battery_id'];
				$n_battery[$n]['s_num'] = $rs_arr['s_num'];
				$n_battery[$n]['last_report_datetime'] = $rs_arr['last_report_datetime'];
			}
			$n++;
		}

		$return = array();
		$return[0] = $battery_num;
		$return[1] = $battery_1;
		$return[2] = $battery_2;
		$return[3] = $battery_3;
		$return[4] = $n_battery;

		
		return $return ;
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
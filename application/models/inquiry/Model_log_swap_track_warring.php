<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_swap_track_warring extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lstw', $setAlias);
		else $whereStr .= "" ;

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$SQLCmd  = "SELECT count(*) cnt
						FROM log_swap_track_warring lstw 
						left join tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num
						where {$whereStr}" ;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lstw', $setAlias);
		else $whereStr .= "" ;
					
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "lstw";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = 'lstw';
			}else if($field == 'BSS_ID'){
				$field = 'bss_id';
				$ali = 'tbss';
			}
			$sql_orderby = " order by ".$ali.'.'.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lstw.s_num desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$SQLCmd  = "SELECT lstw.*, concat(tbss.location, '（',tbss.bss_id,'）') as bss_id
						FROM log_swap_track_warring lstw
						left join tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num
						WHERE {$whereStr}
						{$sql_orderby} 
						LIMIT {$startRow} , {$offset} " ;


			//echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			
			return $rs;
		}
	}

	/**
	 * 取得单一 info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT lstw.*, tbss.bss_id 
					FROM log_swap_track_warring lstw
					left join tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num
					where lstw.s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_dealer", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 修改 tbl_operator
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		$old_status = $this->input->post("old_status");
		
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}
		
		if($colDataArr['status'] != $old_status){
			//狀態有被改過
			if($colDataArr['status'] == '2'){
				//更新處理人員狀態
				$colDataArr['process_date'] = "now()";
				$colDataArr['process_user'] = $this->session->userdata('user_sn');
			}
			if($colDataArr['status'] == '3'){
				//更新完成人員狀態
				$colDataArr['finish_date'] = "now()";
				$colDataArr['finish_user'] = $this->session->userdata('user_sn');
			}
		}
		$whereStr = "s_num = '{$s_num}'";
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "log_swap_track_warring", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}

	public function edit_log_swap_track_warring( $dataArr = array() ){
		$update_type = 'N';
		$s_num = $dataArr['s_num'];
		if($dataArr['status'] != 3){
			$user_sn = $this->session->userdata('user_sn');
			
			$up_arr = array();
			//未完成先更新檢視人員
			if($dataArr['view_date'] == ''){
				$up_arr['view_date'] = 'now()';
				$update_type = 'Y';
			}
			
			$view_users = array();
			if($dataArr['view_users'] != ''){
				$view_users = explode(',',$dataArr['view_users']);
			}
			
			$count = count($view_users)+1;
			if(!in_array($user_sn, $view_users)){
				$view_users[$count] = $user_sn;
				$update_type = 'Y';
			}

			if($update_type == 'Y'){
				$viewStr = join( ",", $view_users );
				$up_arr['view_users'] = $viewStr;
				$whereStr = "s_num = '{$s_num}'";

				$this->db_update( "log_swap_track_warring", $up_arr, $whereStr );
			}
		}

		return $update_type;
	}

}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_alarm_online extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt($source_type = "" )
	{
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";
		if($source_type == 'monitor'){
			$whereStr .= " and lao.status != '3'";
		}

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lao.bss_token_id like '%{$search_txt}%'";
		}
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
			"tbss.bss_id"		=>	"Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lao', $setAlias);
		else $whereStr .= "" ;

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;

			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lao.so_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT count(*) cnt
						FROM log_alarm_online lao 
  						LEFT JOIN tbl_operator top ON lao.so_num = top.s_num 
  						LEFT JOIN tbl_battery_swap_station tbss ON lao.sb_num = tbss.s_num 
						where {$whereStr}" ;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getList( $offset, $startRow = "", $source_type = "" )
	{
		
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";
		if($source_type == 'monitor'){
			$whereStr .= " and lao.status != '3'";
		}


 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lao.bss_token_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
			"tbss.bss_id"		=>	"Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lao', $setAlias);
		else $whereStr .= "" ;
					
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = 'lao';
			}
			if($field != 'battery_id')
			{
				$ali = 'lao';
			}
			else
			{
				$ali = 'tb';
			}
			$sql_orderby = " order by ".$ali.'.'.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lao.alarm_online_sn desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;

			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lao.so_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT lao.*, top.top01,/* tbss.bss_id,*/concat(tbss.location, '（',tbss.bss_id,'）') as bss_id
						FROM log_alarm_online lao 
  						LEFT JOIN tbl_operator top ON lao.so_num = top.s_num 
  						LEFT JOIN tbl_battery_swap_station tbss ON lao.sb_num = tbss.s_num 
						WHERE {$whereStr}
						{$sql_orderby} 
						LIMIT {$startRow} , {$offset} " ;
			//echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			
			return $rs;
		}
	}

	/**
	 * 取得单一 info
	 */
	public function getInfo( $alarm_online_sn = "" )
	{
		
		$whereArr = array ( "alarm_online_sn" => $alarm_online_sn ) ;
		$SQLCmd = "SELECT * FROM log_alarm_online where alarm_online_sn={$alarm_online_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_dealer", $whereArr ) ;
		return $rs ;
	}

	public function getErrorALL(){
		$SQLCmd  = "SELECT lao.*, top.top01,/* tbss.bss_id,*/concat(tbss.location, '（',tbss.bss_id,'）') as bss_id,
					tp.map_province_name as province, city, district, tp.map_type
					FROM log_alarm_online lao 
					LEFT JOIN tbl_operator top ON lao.so_num = top.s_num 
					LEFT JOIN tbl_battery_swap_station tbss ON lao.sb_num = tbss.s_num 
					left join tbl_province tp ON tp.province_name = tbss.province
					and lao.status != '3'
					group by tbss.province, tbss.city, tbss.district" ;


		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		//新增纪录档
		$arrSearch = array();
		$this->model_background->log_insert_search($arrSearch, $SQLCmd);
		
		$err_province = array();
		$err_city = array();
		if(count($rs)>0){
			foreach($rs as $rs_arr){
				if(!isset($err_province[$rs_arr['province']])){
					$err_province[$rs_arr['province']] = $rs_arr['province'];
				}

				if($rs_arr['map_type'] == 0){
					if(!isset($err_city[$rs_arr['city']])){
						$err_city[$rs_arr['city']] = $rs_arr['city'];
					}
				}
				if(!isset($err_city[$rs_arr['district']])){
					$err_city[$rs_arr['district']] = $rs_arr['district'];
				}
			}
		}
		
		$return = array();
		$return[0] = $err_province;
		$return[1] = $err_city;

		return $return;
	
	}
	public function getqueryList(){

		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1 = 1 ";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lao.bss_token_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
			"tbss.bss_id"		=>	"Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lao', $setAlias);
		else $whereStr .= "" ;
					
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = 'lao';
			}
			if($field != 'battery_id')
			{
				$ali = 'lao';
			}
			else
			{
				$ali = 'tb';
			}
			$sql_orderby = " order by ".$ali.'.'.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lao.alarm_online_sn desc";
		}

 		$province = $this->input->post("province");
 		$city = $this->input->post("city");
 		$district = $this->input->post("district");
		$map_type = 0;
 		if($province!="")
 		{
			//检查当下的省份转成资料库存档的省份
			$p_arr = $this->Model_show_list->check_province($province);
			$new_province = $p_arr[0];
			$map_type = $p_arr[1];
 			$whereStr .= " and tbss.province = '{$new_province}'";
 		}

 		if($city!="")
 		{
			if($map_type == 0){
 				$whereStr .= " and tbss.city = '{$city}'";
			}
 		}
		
		if($map_type == 1){
			if($city!=""){
				$whereStr .= " and tbss.district = '{$city}'";
			}
		}else{
			if($district!="")
			{
				$whereStr .= " and tbss.district = '{$district}'";
			}
		}
		

		$SQLCmd  = "SELECT lao.*, top.top01,/* tbss.bss_id,*/concat(tbss.location, '（',tbss.bss_id,'）') as bss_id
					FROM log_alarm_online lao 
					LEFT JOIN tbl_operator top ON lao.so_num = top.s_num 
					LEFT JOIN tbl_battery_swap_station tbss ON lao.sb_num = tbss.s_num 
					WHERE {$whereStr} and lao.status != 3
					{$sql_orderby} " ;
		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		//新增纪录档
		$arrSearch = array();
		$arrSearch['searchData'] = $search;
		$this->model_background->log_insert_search($arrSearch, $SQLCmd);
		
		return $rs;

	}
	/**
	 * 修改 tbl_operator
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$source_type = $this->input->post("source_type");
		$s_num = $this->input->post("s_num");
		$old_status = $this->input->post("old_status");
		
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}
		
		if($colDataArr['status'] != $old_status){
			//狀態有被改過
			if($colDataArr['status'] == '2'){
				//更新處理人員狀態
				$colDataArr['process_date'] = "now()";
				$colDataArr['process_user'] = $this->session->userdata('user_sn');
			}
			if($colDataArr['status'] == '3'){
				//更新完成人員狀態
				$colDataArr['finish_date'] = "now()";
				$colDataArr['finish_user'] = $this->session->userdata('user_sn');
			}
		}
		$whereStr = "alarm_online_sn = '{$s_num}'";
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		$web = $this->config->item('base_url');
		$new_url = $web."monitor/monitor_alarm_online";
		if ( $this->db_update( "log_alarm_online", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[alarm_online_sn: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			if($source_type == 'monitor'){
				$this->redirect_alert($new_url, "{$this->lang->line('edit_successfully')}") ;
			}else{
				$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
			}
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[alarm_online_sn: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			if($source_type == 'monitor'){
				$this->redirect_alert($new_url, "{$this->lang->line('edit_failed')}") ;
			}else{
				$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
			}
		}
	}

	public function edit_log_alarm_online( $dataArr = array() ){
		$update_type = 'N';
		$alarm_online_sn = $dataArr['alarm_online_sn'];
		if($dataArr['status'] != 3){
			$user_sn = $this->session->userdata('user_sn');
			
			$up_arr = array();
			//未完成先更新檢視人員
			if($dataArr['view_date'] == ''){
				$up_arr['view_date'] = 'now()';
				$update_type = 'Y';
			}
			
			$view_users = array();
			if($dataArr['view_users'] != ''){
				$view_users = explode(',',$dataArr['view_users']);
			}
			
			$count = count($view_users)+1;
			if(!in_array($user_sn, $view_users)){
				$view_users[$count] = $user_sn;
				$update_type = 'Y';
			}

			if($update_type == 'Y'){
				$viewStr = join( ",", $view_users );
				$up_arr['view_users'] = $viewStr;
				$whereStr = "alarm_online_sn = '{$alarm_online_sn}'";

				$this->db_update( "log_alarm_online", $up_arr, $whereStr );
			}
		}

		return $update_type;
	}

}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
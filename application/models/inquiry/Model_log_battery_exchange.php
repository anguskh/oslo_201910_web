<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_battery_exchange extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lbe.vehicle_code like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lbe', $setAlias);
		else $whereStr .= "" ;
		
		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lbe.so_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT count(*) cnt
						FROM log_battery_exchange lbe 
						LEFT JOIN tbl_operator top ON top.s_num = lbe.so_num 
						LEFT JOIN tbl_battery_swap_station tbs ON tbs.s_num = lbe.sb_num 
						LEFT JOIN tbl_vehicle tv ON tv.s_num = lbe.sv_num 
						WHERE {$whereStr} " ;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getInfo( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lbe.vehicle_code like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lbe', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = "lbe.";
			}
			$sql_orderby = " order by ".$ali.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lbe.s_num desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lbe.so_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT lbe.*, top.top01 as top_name, tbs.bss_id, tv.unit_id,tbs.location
						FROM log_battery_exchange lbe 
						LEFT JOIN tbl_operator top ON top.s_num = lbe.so_num 
						LEFT JOIN tbl_battery_swap_station tbs ON tbs.s_num = lbe.sb_num 
						LEFT JOIN tbl_vehicle tv ON tv.s_num = lbe.sv_num 
						WHERE {$whereStr}
						{$sql_orderby} 
						LIMIT {$startRow} , {$offset} " ;
			//echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			
			return $rs;
		}
	}
}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
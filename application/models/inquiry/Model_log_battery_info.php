<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_battery_info extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lbi.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"	=> "Y",
			"td.tde01"	=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lbi', $setAlias);
		else $whereStr .= "" ;

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lbi.DorO_flag = 'O' and lbi.do_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT count(*) cnt
									FROM log_battery_info lbi 
									LEFT JOIN tbl_dealer td ON lbi.do_num = td.s_num 
									LEFT JOIN tbl_operator top ON  lbi.do_num = top.s_num
									LEFT JOIN tbl_battery_swap_station tbs ON tbs.s_num = lbi.sb_num 
									where ".$whereStr;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getInfo( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr  = '';

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and lbi.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"	=> "Y",
			"td.tde01"	=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lbi', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = "lbi.";
			}
			$sql_orderby = " order by ".$ali.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by lbi.s_num desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and lbi.DorO_flag = 'O' and lbi.do_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT lbi.*, tbs.bss_id, tbs.location
							,(CASE lbi.DorO_flag WHEN 'D' THEN td.tde01 WHEN 'O' THEN top.top01 END ) as do_name
						FROM log_battery_info lbi 
						LEFT JOIN tbl_dealer td ON lbi.do_num = td.s_num 
						LEFT JOIN tbl_operator top ON  lbi.do_num = top.s_num
						LEFT JOIN tbl_battery_swap_station tbs ON tbs.s_num = lbi.sb_num 
						WHERE {$whereStr}
						{$sql_orderby} 
						LIMIT {$startRow} , {$offset} " ;
			//echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			return $rs;
		}
	}
}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_battery_leave_return extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y",
			"tv.unit_id"	=> "Y",
			"l_tbss.bss_id"	=> "Y",
			"r_tbss.bss_id"	=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;
		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		//2018/05/21先把借電日期不為空取消
		//$whereStr .= " and (lblr.leave_date is not null or lblr.leave_date != '')";
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$join = "";
			if($whereStr!="")
			{
				$join = "LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
						LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
						LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num 
						LEFT JOIN tbl_battery_swap_station r_tbss ON lblr.return_sb_num = r_tbss.s_num 
						LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
						LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
						LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
						LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
						LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
						LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
						LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
						LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num ";
			}
			$whereStr = ' 1 = 1 '.$whereStr;

			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and tm.to_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT count(*) cnt
									FROM log_battery_leave_return lblr 
									{$join} 
									where ".$whereStr;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getInfo( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr  = '';

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y",
			"tv.unit_id"		=> "Y",
			"l_tbss.bss_id"		=> "Y",
			"r_tbss.bss_id"		=> "Y"
			
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = "lblr.";
			}
			$sql_orderby = " order by ".$ali.$field.' '.$orderby;
			$sql_orderby1 = "";
			$limit = " LIMIT {$startRow} , {$offset}";
			$limit1 = "";
		}else{
			$sql_orderby = " order by lblr.s_num desc";
			$sql_orderby1 = " ORDER BY s_num DESC";
			$limit = "";
			$limit1 = " LIMIT {$startRow} , {$offset}";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		//2018/05/21先把借電日期不為空取消
		//$whereStr .= " and (lblr.leave_date is not null or lblr.leave_date != '')";
		if($rs_0 == 'Y'){
			return null;
		}else{
			if($whereStr!="")
			{
				$sql_orderby1 = "";
				$limit = " LIMIT {$startRow} , {$offset}";
				$limit1 = "";
			}

			$whereStr = ' 1 = 1 '.$whereStr;

			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and tm.to_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT lblr.*
						,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
						,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name
						,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as rdo_name
						,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as rdso_name, tm.name as member_name, tv.unit_id, 
							/*r_tbss.bss_id as return_bss_id, */
							/*l_tbss.bss_id as leave_bss_id*/
							concat(r_tbss.location, '（',r_tbss.bss_id,'）') as return_bss_id, 
							concat(l_tbss.location, '（',l_tbss.bss_id,'）') as leave_bss_id
									FROM (SELECT * FROM log_battery_leave_return {$sql_orderby1} {$limit1}) lblr 
									LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
									LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
									LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num 
									LEFT JOIN tbl_battery_swap_station r_tbss ON lblr.return_sb_num = r_tbss.s_num 
									LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
									LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
									LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
									LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
									LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
									LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
									LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
									LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
									where {$whereStr} {$sql_orderby} {$limit}";
			// echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			return $rs;
		}
	}

	public function getlogInfo($s_num)
	{
		$SQLCmd = "SELECT gps_path_track FROM log_battery_leave_return WHERE s_num = {$s_num}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}
	
	public function getInfo2($s_num)
	{
		$SQLCmd = "SELECT lblr.*,  tm.name as member_name
									FROM log_battery_leave_return lblr
									LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
									WHERE lblr.s_num = {$s_num}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}
	
	/**
	 * 新增 tbl_battery_swap_station_group
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['system_log_date'] = "now()";

		//查user_sn的user_id
		$tm_arr = $this->Model_show_list->getMemberList($dataArr['tm_num']);
		if(count($tm_arr)>0){
			$dataArr['battery_user_id'] = $tm_arr[0]['user_id'];
		}

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "log_battery_leave_return", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	public function updateData(){
	
		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		$leave_date = $this->input->post("leave_date");
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}

		$colDataArr['return_request_date']  = $colDataArr['return_date'];
		$whereStr = "s_num = '{$s_num}'";

		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['return_status'] = 'W';//補登
		$colDataArr['usage_time'] = $this->shi_jian_cha($colDataArr['return_date'],$leave_date);
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "log_battery_leave_return", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	
	}
	//計算時間差
	public function shi_jian_cha($d,$d1)
	{ 
		$time = strtotime($d) - strtotime($d1); 
		$n_time = str_pad(floor($time%(24*3600)/3600),2,0,STR_PAD_LEFT ).":".str_pad(floor($time%3600/60),2,0,STR_PAD_LEFT).":".str_pad($time%3600%60, 2, 0, STR_PAD_LEFT)."秒";
		return $n_time;
	}

	public function getuserlastbattery($user_id)
	{
		$SQLCmd = "SELECT leave_battery_id FROM log_battery_leave_return WHERE battery_user_id LIKE '{$user_id}' AND leave_date IS NOT NULL AND leave_status=0 AND return_date IS NULL AND (return_status!=0 OR return_status IS NULL) ORDER BY s_num DESC limit 1";
		$rs = $this->db_query($SQLCmd);
		if($rs)
			return $rs[0]['leave_battery_id'];
		else
			return "";
	}

	public function getuserallbattery($user_id)
	{
		$SQLCmd = "SELECT leave_battery_id FROM log_battery_leave_return WHERE battery_user_id LIKE '{$user_id}' AND leave_date IS NOT NULL AND leave_status=0 AND return_date IS NULL AND (return_status!=0 OR return_status IS NULL) ORDER BY s_num";
		$rs = $this->db_query($SQLCmd);
		if($rs)
			return $rs;
		else
			return "";
	}

	public function getBatteryuserID($returnbatteryID)
	{
		$SQLCmd = "SELECT battery_user_id FROM log_battery_leave_return WHERE leave_battery_id = '{$returnbatteryID}' AND return_date IS NULL ORDER BY leave_date DESC limit 1";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			return $rs[0]['battery_user_id'];
		}
		else
		{
			return "";
		}
	}

}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
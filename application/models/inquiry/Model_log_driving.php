<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_driving extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getCnt()
	{
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi', $setAlias);
		else $whereStr .= "" ;

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return 0;
		}else{
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and ldi.DorO_flag = 'O' and ldi.do_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT count(*) cnt
						FROM log_driving_info ldi 
						where ldi.log_type = 'B' and ldi.unit_id != '' {$whereStr}" ;
			$rs = $this->db_query($SQLCmd);	
			return $rs[0]['cnt'];
		}
	}

	/**
	 * 取得清单
	 */
	public function getInfo( $offset, $startRow = "" )
	{
		
		$fn = get_fetch_class_random();

		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tso.tsop01"		=>	"Y",
			"td.tde01"			=>  "Y",
			"tsd.tsde01"		=>	"Y",
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi', $setAlias);
		else $whereStr .= "" ;
					
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$ali = "";
			if(strtolower($field) == "desc"){
				$field = strtolower($field);
				$ali = 'ldi';
			}
			if($field != 'battery_id')
			{
				$ali = 'ldi';
			}
			else
			{
				$ali = 'tb';
			}
			$sql_orderby = " order by ".$ali.'.'.$field.' '.$orderby;
		}else{
			$sql_orderby = " order by ldi.system_log_date desc";
		}

		$inquiry_type = $this->session->userdata("inquiry_type");
		$rs_0 = 'N';
		if($inquiry_type == 'Y'){
			if($whereStr == ''){
				$rs_0 = 'Y';
			}
		}
		if($rs_0 == 'Y'){
			return null;
		}else{
			$whereStr = ' 1 = 1 '.$whereStr;
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and ldi.DorO_flag = 'O' and ldi.do_num = {$to_flag_num}" ;
			}

			if($startRow > 50000){
			
				$SQLCmd  = "SELECT ldi.system_log_date, ldi.s_num, ldi.DorO_flag, ldi.do_num, ldi.dso_num, ldi.unit_id, ldi.battery_voltage, ldi.battery_amps, ldi.battery_temperature, ldi.environment_temperature, ldi.battery_capacity, ldi.battery_station_position, ldi.battery_gps_manufacturer, ldi.battery_gps_version
							FROM log_driving_info ldi 
					INNER JOIN ( SELECT ldi.s_num FROM log_driving_info ldi WHERE {$whereStr} and ldi.log_type = 'B' and ldi.unit_id != '' 
					{$sql_orderby} LIMIT {$startRow} , {$offset}  ) AS s
					ON ldi.s_num = s.s_num";
			}else{
				$SQLCmd  = "SELECT ldi.system_log_date, ldi.s_num, ldi.DorO_flag, ldi.do_num, ldi.dso_num, ldi.unit_id, ldi.battery_voltage, ldi.battery_amps, ldi.battery_temperature, ldi.environment_temperature, ldi.battery_capacity, ldi.battery_station_position, ldi.battery_gps_manufacturer, ldi.battery_gps_version
							FROM log_driving_info ldi 
							WHERE {$whereStr} and ldi.log_type = 'B' and ldi.unit_id != ''
							{$sql_orderby} 
							LIMIT {$startRow} , {$offset} " ;
			}
			//echo $SQLCmd;
			$rs = $this->db_query($SQLCmd) ;
			//新增纪录档
			$arrSearch = array();
			$arrSearch['searchData'] = $search;
			$this->model_background->log_insert_search($arrSearch, $SQLCmd);
			
			return $rs;
		}
	}
}
/* End of file model_initial_password.php */
/* Location: ./application/models/inquiry/model_initial_password.php */
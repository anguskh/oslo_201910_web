<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_bss_manage extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tgm.bss_content like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tgm', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_bss_manage tgm
  						where tgm.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;

		//排序动作
		$sql_orderby = " order by tgm.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tgm.bss_content like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tgm', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT tgm.*,(SELECT count(*) FROM log_updating_bss_version WHERE tgm.s_num = bss_version_num ) as total_num,(SELECT count(*) FROM log_updating_bss_version WHERE tgm.s_num = bss_version_num AND notify_date IS NOT NULL AND notify_date!='') as notify, (SELECT count(*) FROM log_updating_bss_version WHERE tgm.s_num = bss_version_num AND updating_date IS NOT NULL AND updating_date!='') as download 
  						FROM tbl_bss_manage tgm 
  						where tgm.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 bss_manage info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_bss_manage where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_bss_manage", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_bss_manage
	 */
	public function insertData()
	{
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$save_path = 'BSSFW';
		if ( ! is_dir($save_path))
		{
			mkdir($save_path, 0700, TRUE);
		}

		//上传檔案
		if(isset($_FILES['bss_file']['tmp_name']) && $_FILES['bss_file']['tmp_name']!='')
		{
				$dataArr['bss_file'] = $_FILES['bss_file']['name'];	//檔名
				$web = $this->config->item('base_url');

				$filepath = "BSSFW/".$dataArr['bss_file'] ;
				if($_FILES["bss_file"]["tmp_name"]!='')
				{
					move_uploaded_file( $_FILES["bss_file"]["tmp_name"] , $filepath ) ;
				}
		}

		$group_sb_num = $this->input->post("group_sb_num");
		$dataArr['group_sb_num'] = "";
		if(count($group_sb_num)>0){
			$dataArr['group_sb_num'] = join( ",", $group_sb_num );
		}
		
		$dataArr['all_sb_num'] = "";
		$all_sb_num = $this->input->post("all_sb_num");
		if(count($all_sb_num)>0){
			$dataArr['all_sb_num'] = join( ",", $all_sb_num );
		}

		$dataArr['up_flag'] = ((isset($dataArr['up_flag']))?1:0);
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_bss_manage", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);

			$SQLCmdLa = "select s_num as bss_version_num FROM tbl_bss_manage WHERE create_date = '{$nowDate}' AND create_user = {$this->session->userdata("user_sn")}";
			$rsLa = $this->db_query($SQLCmdLa);


			//寫入BSS版本更新log
			if($rsLa)
			{
				$bss_version_num = $rsLa[0]['bss_version_num'];
				if(count($group_sb_num)>0)
				{
					foreach($group_sb_num as $keyg => $valueg)
					{
						$SQLCmdG = "SELECT all_sb_num FROM tbl_battery_swap_station_group WHERE s_num = {$valueg} AND status <> 'D'";
						$rsG = $this->db_query($SQLCmdG);
						if($rsG)
						{
							$sbnumArr = explode(",",$rsG[0]['all_sb_num']);
							foreach($sbnumArr as $keysb => $valuesb)
							{
								$dataArrLU = array();
								$dataArrLU['sys_date'] = $nowDate;
								$dataArrLU['bss_version_num'] = $bss_version_num;
								$dataArrLU['sb_num'] = $valuesb;
								$dataArrLU['bss_version_no'] = $dataArr['bss_version_no'];
								$dataArrLU['bss_version_date'] = $dataArr['bss_version_date'];
								$this->db_insert("log_updating_bss_version",$dataArrLU);
							}
						}
					}
				}

				if(count($all_sb_num)>0)
				{
					foreach($all_sb_num as $keysb => $valuesb)
					{
						$dataArrLU = array();
						$dataArrLU['sys_date'] = $nowDate;
						$dataArrLU['bss_version_num'] = $bss_version_num;
						$dataArrLU['sb_num'] = $valuesb;
						$dataArrLU['bss_version_no'] = $dataArr['bss_version_no'];
						$dataArrLU['bss_version_date'] = $dataArr['bss_version_date'];
						$this->db_insert("log_updating_bss_version",$dataArrLU);
					}
				}
			}

			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_bss_manage
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");
		$bss_version_num = $this->input->post('s_num');
		$whereStr = " s_num={$bss_version_num}" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		$group_sb_num = $this->input->post("group_sb_num");
		$dataArr['group_sb_num'] = "";
		if(count($group_sb_num)>0){
			$dataArr['group_sb_num'] = join( ",", $group_sb_num );
		}
		
		$dataArr['all_sb_num'] = "";
		$all_sb_num = $this->input->post("all_sb_num");
		if(count($all_sb_num)>0){
			$dataArr['all_sb_num'] = join( ",", $all_sb_num );
		}

		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = $nowDate;
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 

		$dataArr['up_flag'] = ((isset($dataArr['up_flag']))?1:0);
		if ( $this->db_update( "tbl_bss_manage", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//先刪除原本BSS版本更新log
			$SQLCmdD = "DELETE FROM log_updating_bss_version WHERE bss_version_num = {$bss_version_num}";
			$this->db_query($SQLCmdD);
			$new_all_sb_num = $all_sb_num;
			//寫入BSS版本更新log
			if(count($group_sb_num)>0)
			{
				foreach($group_sb_num as $keyg => $valueg)
				{
					$SQLCmdG = "SELECT all_sb_num FROM tbl_battery_swap_station_group WHERE s_num = {$valueg} AND status <> 'D'";
					$rsG = $this->db_query($SQLCmdG);
					if($rsG)
					{
						$sbnumArr = explode(",",$rsG[0]['all_sb_num']);
						foreach($sbnumArr as $keysb => $valuesb)
						{
							$dataArrLU = array();
							$dataArrLU['sys_date'] = $nowDate;
							$dataArrLU['bss_version_num'] = $bss_version_num;
							$dataArrLU['sb_num'] = $valuesb;
							$dataArrLU['bss_version_no'] = $dataArr['bss_version_no'];
							$dataArrLU['bss_version_date'] = $dataArr['bss_version_date'];
							$this->db_insert("log_updating_bss_version",$dataArrLU);
							if(isset($valuesb) && isset($new_all_sb_num)){
								$key = array_search($valuesb, $new_all_sb_num);
								if($key!=="")
								{
									unset($new_all_sb_num[$key]);
								}
							}
						}
					}
				}
			}

			if(count($new_all_sb_num)>0)
			{
				foreach($new_all_sb_num as $keysb => $valuesb)
				{
					$dataArrLU = array();
					$dataArrLU['sys_date'] = $nowDate;
					$dataArrLU['bss_version_num'] = $bss_version_num;
					$dataArrLU['sb_num'] = $valuesb;
					$dataArrLU['bss_version_no'] = $dataArr['bss_version_no'];
					$dataArrLU['bss_version_date'] = $dataArr['bss_version_date'];
					$this->db_insert("log_updating_bss_version",$dataArrLU);
				}
			}

			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		$whereStr = " s_num in ({$delIdStr}) ";
		//将档案名称捞出来删除
		$SQLCmd = "SELECT bss_file
  						FROM tbl_bss_manage tgm
  						where {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		foreach($rs as $rs_arr){
			$filepath = "BSSFW/".$rs_arr['bss_file'];
			if(file_exists($filepath)){			
				unlink($filepath);//將檔案刪除
			}
		}

		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_bss_manage", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function check_file_name($s_num, $filename ){
		$whereStr = " and tgm.bss_file =  '{$filename}'";
		if($s_num != ''){
			$whereStr .= " and tgm.s_num != {$s_num}";
		}
		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_bss_manage tgm
  						where tgm.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		if($rs[0]['cnt'] > 0){
			//檔名重覆
			echo 'N';
		}else{
			echo 'Y';
		}
	}

	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function get_bss_update_log_cnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
 		$whereStr = "";
		//判斷要顯示的明細,1:全部,2:已通知,3:未通知,4:已下載,5;未下載
		if($this->session->userdata("detail_type")!="")
		{
			switch($this->session->userdata("detail_type"))
			{
				case 2:
					$whereStr .= " AND lubv.notify_date IS NOT NULL AND lubv.notify_date !=''";
					break;
				case 3:
					$whereStr .= " AND (lubv.notify_date IS NULL OR lubv.notify_date ='')";
					break;
				case 4:
					$whereStr .= " AND lubv.updating_date IS NOT NULL AND lubv.updating_date !=''";
					break;
				case 5:
					$whereStr .= " AND (lubv.updating_date IS NULL OR lubv.updating_date ='')";
					break;
				default:
					break;
			}
		}

		if($this->session->userdata("bss_version_num")!="")
			$whereStr .= " AND lubv.bss_version_num = {$this->session->userdata("bss_version_num")}";
		// if($search_txt != ''){
		// 	$whereStr .= " and tgm.bss_content like '%{$search_txt}%'";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		// $setAlias = array(
		// 	"top.top01"		=>  "Y",
		// 	"td.tde01"		=>	"Y"
		// );
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lubv', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT count(*) cnt 
  					FROM log_updating_bss_version lubv 
  					LEFT JOIN tbl_battery_swap_station tbss ON lubv.sb_num = tbss.s_num 
  					where 1=1 {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function get_bss_update_log_list( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;

		//排序动作
		$sql_orderby = " order by lubv.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";
		//判斷要顯示的明細,1:全部,2:已通知,3:未通知,4:已下載,5;未下載
		if($this->session->userdata("detail_type")!="")
		{
			switch($this->session->userdata("detail_type"))
			{
				case 2:
					$whereStr .= " AND lubv.notify_date IS NOT NULL AND lubv.notify_date !=''";
					break;
				case 3:
					$whereStr .= " AND (lubv.notify_date IS NULL OR lubv.notify_date ='')";
					break;
				case 4:
					$whereStr .= " AND lubv.updating_date IS NOT NULL AND lubv.updating_date !=''";
					break;
				case 5:
					$whereStr .= " AND (lubv.updating_date IS NULL OR lubv.updating_date ='')";
					break;
				default:
					break;
			}
		}

		if($this->session->userdata("bss_version_num")!="")
			$whereStr .= " AND lubv.bss_version_num = {$this->session->userdata("bss_version_num")}";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		// if($search_txt != ''){
		// 	$whereStr .= " and tgm.bss_content like '%{$search_txt}%'";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		// $setAlias = array(
		// 	"top.top01"		=>  "Y",
		// 	"td.tde01"		=>	"Y"
		// );
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lubv', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT lubv.*,tbss.bss_id 
  					FROM log_updating_bss_version lubv 
  					LEFT JOIN tbl_battery_swap_station tbss ON lubv.sb_num = tbss.s_num 
  					where 1=1 {$whereStr} {$sql_orderby} 
  					LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
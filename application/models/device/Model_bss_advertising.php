<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_bss_advertising extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tga.bss_content like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(

		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tga', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_bss_advertising tga
  						where tga.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;

		//排序动作
		$sql_orderby = " order by tga.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tga.bss_content like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(

		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tga', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT tga.*,(SELECT count(*) FROM tbl_bss_advertising_match WHERE tga.s_num = advertising_num ) as total_num,(SELECT count(*) FROM tbl_bss_advertising_match WHERE tga.s_num = advertising_num AND notify_date IS NOT NULL AND notify_date!='') as notify 
  						FROM tbl_bss_advertising tga 
  						where tga.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 bss_advertising info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_bss_advertising where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_bss_advertising", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_bss_advertising
	 */
	public function insertData()
	{
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		
		// //上传檔案
		// if(isset($_FILES['bss_file']['tmp_name']) && $_FILES['bss_file']['tmp_name']!='')
		// {
		// 		$dataArr['bss_file'] = $_FILES['bss_file']['name'];	//檔名
		// 		$web = $this->config->item('base_url');

		// 		$filepath = "BSSFW/".$dataArr['bss_file'] ;
		// 		if($_FILES["bss_file"]["tmp_name"]!='')
		// 		{
		// 			move_uploaded_file( $_FILES["bss_file"]["tmp_name"] , $filepath ) ;
		// 		}
		// }

		$all_sb_num = $this->input->post("all_sb_num");

		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_bss_advertising", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);

			$SQLCmdLa = "select s_num as bss_advertising_num FROM tbl_bss_advertising WHERE create_date = '{$nowDate}'";
			$rsLa = $this->db_query($SQLCmdLa);


			
			if($rsLa)
			{
				$advertising_num = $rsLa[0]['bss_advertising_num'];
				$save_path = $this->session->userdata('advertising_file_url')."ad_{$advertising_num}/";
				//上傳檔案
				if ( ! is_dir($save_path))
				{
					mkdir($save_path, 0777, TRUE);
				}

				# 取得上傳檔案數量
				$fileCount = count($_FILES['advertising_file']['name']);
				$fileNameArr = array();
				for ($i = 0; $i < $fileCount; $i++) {
				  # 檢查檔案是否上傳成功
				  if ($_FILES['advertising_file']['error'][$i] === UPLOAD_ERR_OK){

				    # 檢查檔案是否已經存在
				    if (file_exists($save_path . $_FILES['advertising_file']['name'][$i])){
				      echo '檔案已存在。<br/>';
				    } else {
				      $file = $_FILES['advertising_file']['tmp_name'][$i];
				      $dest = $save_path.$_FILES['advertising_file']['name'][$i];
				      $fileNameArr[$i] = $_FILES['advertising_file']['name'][$i];
				      # 將檔案移至指定位置
				      move_uploaded_file($file, $dest);
				    }
				  } else {
				    echo '錯誤代碼：' . $_FILES['advertising_file']['error'] . '<br/>';
				  }
				}

				//更新檔案名稱以及網址
				$whereStr = " s_num = {$advertising_num}";
				$dataArrA = array();
				if($dataArr['advertising_type']==1)
					$dataArrA['advertising_upload_img'] = join( ",", $fileNameArr );
				else
					$dataArrA['advertising_upload_video'] = join( ",", $fileNameArr );

				$web = $this->config->item('base_url');
				$web = str_replace("https","http",$web);
				$dataArrA['advertising_address'] = $web."advertising/showad/index/{$advertising_num}";
				$this->db_update("tbl_bss_advertising",$dataArrA,$whereStr);

				//寫入對應表
				if(count($all_sb_num)>0){
					$dataArrm = array();
					foreach ($all_sb_num as $keysb => $valuesb) {
						$dataArrm['advertising_num'] = $rsLa[0]['bss_advertising_num'];
						$dataArrm['sb_num'] = $valuesb;
						$this->db_insert('tbl_bss_advertising_match',$dataArrm);
					}
				}
			}

			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_bss_advertising
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");
		$advertising_num = $this->input->post('s_num');
		$whereStr = " s_num={$advertising_num}" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		$save_path = $this->session->userdata('advertising_file_url')."ad_{$advertising_num}/";
		//上傳檔案
		if ( ! is_dir($save_path))
		{
			mkdir($save_path, 0777, TRUE);
		}

		# 取得上傳檔案數量
		$fileCount = count($_FILES['advertising_file']['name']);
		$fileNameArr = array();
		for ($i = 0; $i < $fileCount; $i++) {
		  # 檢查檔案是否上傳成功
		  if ($_FILES['advertising_file']['error'][$i] === UPLOAD_ERR_OK){

		    # 檢查檔案是否已經存在
		    if (!file_exists($save_path . $_FILES['advertising_file']['name'][$i])){
		      $file = $_FILES['advertising_file']['tmp_name'][$i];
		      $dest = $save_path.$_FILES['advertising_file']['name'][$i];
		      $fileNameArr[$i] = $_FILES['advertising_file']['name'][$i];
		      # 將檔案移至指定位置
		      move_uploaded_file($file, $dest);
		    }
		  }
		}

		if(count($fileNameArr)>0)
		{
			$SQLCmd = "SELECT advertising_upload_img,advertising_upload_video FROM tbl_bss_advertising WHERE {$whereStr}";
			$rs = $this->db_query($SQLCmd);
			if($dataArr['advertising_type']==1)
			{
				$orgfile = $rs[0]['advertising_upload_img'];
				$dataArr['advertising_upload_img'] = $orgfile.",".join( ",", $fileNameArr );
			}	
			else
			{
				$orgfile = $rs[0]['advertising_upload_video'];
				$dataArr['advertising_upload_video'] = $orgfile.",".join( ",", $fileNameArr );
			}
			$web = $this->config->item('base_url');
			$web = str_replace("https","http",$web);
			$dataArr['advertising_address'] = $web."advertising/showad/index/{$advertising_num}";
		}

		$all_sb_num = $this->input->post("all_sb_num");
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = $nowDate;
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 

		if ( $this->db_update( "tbl_bss_advertising", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//先刪除原本對應表
			$SQLCmdD = "DELETE FROM tbl_bss_advertising_match WHERE advertising_num = {$advertising_num}";
			$this->db_query($SQLCmdD);

			//重新寫入對應表
			if(count($all_sb_num)>0){
				$dataArrm = array();
				foreach ($all_sb_num as $keysb => $valuesb) {
					$dataArrm['advertising_num'] = $advertising_num;
					$dataArrm['sb_num'] = $valuesb;
					$this->db_insert('tbl_bss_advertising_match',$dataArrm);
				}
			}

			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	public function deletefile()
	{
		$s_num = $this->input->post("s_num");
		$whereStr = " s_num = {$s_num}";
		$filepath = $this->input->post("filepath");
		$file_type = $this->input->post("file_type");
		if(unlink($filepath))
		{
			if($file_type==1)
				$dataArr['advertising_upload_img'] = $this->input->post("filename");
			else if($file_type==2)
				$dataArr['advertising_upload_video'] = $this->input->post("filename");
			$this->db_update("tbl_bss_advertising",$dataArr,$whereStr);
			echo "Y";
		}
		else
		{
			echo "N";
		}
	}

	public function getbss($s_num = "")
	{
		$whereStr = "advertising_num = {$s_num}";
		$SQLCmd = "SELECT sb_num FROM tbl_bss_advertising_match WHERE {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		$bssArr =array();
		if($rs)
		{
			foreach($rs as $key => $value)
			{
				$bssArr[$key] = $value['sb_num'];
			}
		}
		return $bssArr;
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		$whereStr = " s_num in ({$delIdStr}) ";
		//将档案名称捞出来删除
		$SQLCmd = "SELECT bss_file
  						FROM tbl_bss_advertising tga
  						where {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		foreach($rs as $rs_arr){
			$filepath = "BSSFW/".$rs_arr['bss_file'];
			if(file_exists($filepath)){			
				unlink($filepath);//將檔案刪除
			}
		}

		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_bss_advertising", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function check_file_name($s_num, $filename, $file_type){
		$fileArr = explode(",",$filename);

		$whereStr = "";
		if($s_num != ''){
			$whereStr .= " and s_num != {$s_num}";
			if($file_type=='1')
			{
				$SQLCmd = "SELECT advertising_upload_img 
							FROM tbl_bss_advertising  
							WHERE status <> 'D' {$whereStr}";
			}
			else if($file_type=='2')
			{
				$SQLCmd = "SELECT advertising_upload_video 
							FROM tbl_bss_advertising  
							WHERE status <> 'D' {$whereStr}";
			}
		
			$rs = $this->db_query($SQLCmd) ;
			if($rs[0]['cnt'] > 0){
				//檔名重覆
				echo 'N';
			}else{
				echo 'Y';
			}
		}
		else
		{
			echo "Y";
		}
		
	}

	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function get_bss_update_log_cnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
 		$whereStr = "";
		//判斷要顯示的明細,1:全部,2:已通知,3:未通知,4:已下載,5;未下載
		if($this->session->userdata("detail_type")!="")
		{
			switch($this->session->userdata("detail_type"))
			{
				case 2:
					$whereStr .= " AND lubv.notify_date IS NOT NULL AND lubv.notify_date !=''";
					break;
				case 3:
					$whereStr .= " AND (lubv.notify_date IS NULL OR lubv.notify_date ='')";
					break;
				case 4:
					$whereStr .= " AND lubv.updating_date IS NOT NULL AND lubv.updating_date !=''";
					break;
				case 5:
					$whereStr .= " AND (lubv.updating_date IS NULL OR lubv.updating_date ='')";
					break;
				default:
					break;
			}
		}

		if($this->session->userdata("bss_version_num")!="")
			$whereStr .= " AND lubv.bss_version_num = {$this->session->userdata("bss_version_num")}";
		// if($search_txt != ''){
		// 	$whereStr .= " and tga.bss_content like '%{$search_txt}%'";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		// $setAlias = array(
		// 	"top.top01"		=>  "Y",
		// 	"td.tde01"		=>	"Y"
		// );
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lubv', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT count(*) cnt 
  					FROM log_updating_bss_version lubv 
  					LEFT JOIN tbl_battery_swap_station tbss ON lubv.sb_num = tbss.s_num 
  					where 1=1 {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function get_bss_update_log_list( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;

		//排序动作
		$sql_orderby = " order by lubv.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";
		//判斷要顯示的明細,1:全部,2:已通知,3:未通知,4:已下載,5;未下載
		if($this->session->userdata("detail_type")!="")
		{
			switch($this->session->userdata("detail_type"))
			{
				case 2:
					$whereStr .= " AND lubv.notify_date IS NOT NULL AND lubv.notify_date !=''";
					break;
				case 3:
					$whereStr .= " AND (lubv.notify_date IS NULL OR lubv.notify_date ='')";
					break;
				case 4:
					$whereStr .= " AND lubv.updating_date IS NOT NULL AND lubv.updating_date !=''";
					break;
				case 5:
					$whereStr .= " AND (lubv.updating_date IS NULL OR lubv.updating_date ='')";
					break;
				default:
					break;
			}
		}

		if($this->session->userdata("bss_version_num")!="")
			$whereStr .= " AND lubv.bss_version_num = {$this->session->userdata("bss_version_num")}";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		// if($search_txt != ''){
		// 	$whereStr .= " and tga.bss_content like '%{$search_txt}%'";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		// $setAlias = array(
		// 	"top.top01"		=>  "Y",
		// 	"td.tde01"		=>	"Y"
		// );
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lubv', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT lubv.*,tbss.bss_id 
  					FROM log_updating_bss_version lubv 
  					LEFT JOIN tbl_battery_swap_station tbss ON lubv.sb_num = tbss.s_num 
  					where 1=1 {$whereStr} {$sql_orderby} 
  					LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_battery extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tb.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tb', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tb.DorO_flag = 'O' and tb.do_num = {$to_flag_num}" ;
		}
		
		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_battery tb 
  						LEFT JOIN tbl_dealer td ON tb.do_num = td.s_num 
  						LEFT JOIN tbl_operator top ON tb.do_num = top.s_num 
  						LEFT JOIN tbl_vehicle tv ON tb.sv_num = tv.s_num 
						left join tbl_battery_swap_track tbst ON tbst.battery_id = tb.battery_id and tbst.status <> 'D'
						left join tbl_battery_swap_station tbss ON tbss.s_num = tbst.sb_num and tbss.status <> 'D'
  						where tb.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;



		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tb.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tb.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tb', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tb.DorO_flag = 'O' and tb.do_num = {$to_flag_num}" ;
		}
		
		$SQLCmd = "SELECT tb.*,tv.unit_id,(CASE tb.DorO_flag WHEN 'D' THEN td.tde01 WHEN 'O' THEN top.top01 END ) as do_name, tbss.bss_id, tbst.track_no, tbss.location
  						FROM tbl_battery tb 
  						LEFT JOIN tbl_dealer td ON tb.do_num = td.s_num 
  						LEFT JOIN tbl_operator top ON tb.do_num = top.s_num 
  						LEFT JOIN tbl_vehicle tv ON tb.sv_num = tv.s_num 
						left join tbl_battery_swap_track tbst ON tbst.battery_id = tb.battery_id and tbst.status <> 'D'
						left join tbl_battery_swap_station tbss ON tbss.s_num = tbst.sb_num and tbss.status <> 'D'
  						where tb.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 battery info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_battery where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery", $whereArr ) ;
		return $rs ;
	}

	public function get_batteryALL()
	{
		$SQLCmd = "SELECT * FROM tbl_battery" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_station", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_battery
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("sd_num");
		}else{
			$dataArr['do_num'] = $this->input->post("so_num");
		}

		$dataArr['battery_id'] = $this->Model_show_list->nf_to_wf($dataArr['battery_id']);
		$dataArr['battery_id'] = trim($dataArr['battery_id']);
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_battery", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			if($dataArr['sys_no']==2)
			{
				// $batteryArr[0] = $dataArr['battery_id'];
				$postArr['action'] = "actionPostImportBattery";
				$postArr['data'] = "[\"{$dataArr['battery_id']}\"]";
				$this->Model_background->wechat_callback($postArr,"bulkimport","批量导入电池");
			}
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_battery
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("sd_num");
		}else{
			$dataArr['do_num'] = $this->input->post("so_num");
		}
		$dataArr['battery_id'] = $this->Model_show_list->nf_to_wf($dataArr['battery_id']);
		$dataArr['battery_id'] = trim($dataArr['battery_id']);
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_battery", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_battery", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function getDetailAllCnt( $battery_id = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');
		$whereStr = "";
 		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tb.DorO_flag = 'O' and tb.do_num = {$to_flag_num}" ;
		}
		
		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd  = "SELECT count(*) cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.leave_battery_id = '{$battery_id}'  or lblr.return_battery_id = '{$battery_id}') ".$whereStr;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $battery_id = '' )
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr  = '';

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		//if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		//else $whereStr .= "" ;
		//排序动作
		$sql_orderby = " order by lblr.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and ( l_tbss.so_num = {$to_flag_num} or r_tbss.so_num = {$to_flag_num})" ;
		}
		
		$SQLCmd  = "SELECT lblr.*
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as rdo_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as rdso_name, tm.name as member_name, tv.unit_id,
					concat(r_tbss.location, '（',r_tbss.bss_id,'）') as return_bss_id, 
					concat(l_tbss.location, '（',l_tbss.bss_id,'）') as leave_bss_id
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num 
								LEFT JOIN tbl_battery_swap_station r_tbss ON lblr.return_sb_num = r_tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.leave_battery_id = '{$battery_id}' or lblr.return_battery_id = '{$battery_id}') ".$whereStr."
								{$sql_orderby} 
  								LIMIT {$startRow} , {$offset} ";
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	//確認电池序号唯一
	public function ajax_pk_battery_id(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得資料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		$fieldnum = $_GET['fieldnum'];
		
		$result = $this->countbattery_id($validateValue, $fieldnum);
		$arrayToJs = array();
		//一定要帶回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//無重複
		}else{
			$arrayToJs[1] = false;	//有重複
		}
		echo json_encode($arrayToJs);
	}

	//查出是否有重複
	public function countbattery_id($battery_id, $fieldnum){
		$where = "";
		if($battery_id != ''){
			if($fieldnum != ''){
				$where = " and tb.s_num != '{$fieldnum}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_battery tb 
						where tb.battery_id = '{$battery_id}' and tb.status <> 'D' {$where}";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * upload_新增 tbl_battery
	 */
	public function upload_insertData($dataArr)
	{

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = is_null($dataArr['sd_num'])?'':$dataArr['sd_num'];
		}else{
			$dataArr['do_num'] = is_null($dataArr['so_num'])?'':$dataArr['so_num'];
		}
		unset($dataArr['sd_num']);
		unset($dataArr['so_num']);
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		$add_flag = 'N';
		if(!isset($dataArr['sys_no'])){
			//空值時預設2
			$dataArr['sys_no'] = 2;
		}
		if ( $this->db_insert( "tbl_battery", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			$add_flag = 'Y';

			if($dataArr['sys_no']==2)
			{
				// $batteryArr[0] = $dataArr['battery_id'];
				$postArr['action'] = "actionPostImportBattery";
				$postArr['data'] = "[\"{$dataArr['battery_id']}\"]";
				$this->Model_background->wechat_callback($postArr,"bulkimport","批量导入电池");
			}

		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
		}
		return $add_flag;
	}

	//同步DB所有電池到微信
	public function batterytowechat()
	{
		$SQLCmd = "SELECT battery_id FROM tbl_battery WHERE status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			$batteryStr = "[";
			foreach($rs as $key => $value)
			{
				if($key==0)
				{
					$batteryStr .= "\"{$value['battery_id']}\"";
				}
				else
				{
					$batteryStr .= ",\"{$value['battery_id']}\"";
				}
				// $batteryArr[$key] = $value['battery_id'];
			}
			$batteryStr .= "]";
			// $batteryStr = "['D010500000001']";
			$postArr['action'] = "actionPostImportBattery";
			$postArr['data'] = $batteryStr;
			$this->Model_background->wechat_callback($postArr,"bulkimport","批量导入电池");
			print_r($postArr);
		}
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
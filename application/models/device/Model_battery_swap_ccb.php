<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_battery_swap_ccb extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbsc.ccb_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"tbss.bss_id"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbsc', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbsc.so_num = {$to_flag_num}" ;
		}
		
		$SQLCmd = "SELECT count(*) cnt
  						FROM tbl_battery_swap_ccb tbsc 
  						LEFT JOIN tbl_operator top ON tbsc.so_num = top.s_num 
  						LEFT JOIN tbl_battery_swap_station tbss ON tbsc.sb_num = tbss.s_num 
  						where tbsc.status <> 'D' {$whereStr}" ;

		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tbsc.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbsc.ccb_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"tbss.bss_id"	=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbsc', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbsc.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT tbsc.*,top.top01, tbss.bss_id
  						FROM tbl_battery_swap_ccb tbsc 
  						LEFT JOIN tbl_operator top ON tbsc.so_num = top.s_num 
  						LEFT JOIN tbl_battery_swap_station tbss ON tbsc.sb_num = tbss.s_num 
  						where tbsc.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 battery_swap_ccb info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_battery_swap_ccb where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_ccb", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_battery_swap_ccb
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_battery_swap_ccb", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_battery_swap_ccb
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_battery_swap_ccb", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_battery_swap_ccb", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
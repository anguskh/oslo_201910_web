<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_vehicle extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tv.vehicle_code like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tv', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tv.do_num = {$to_flag_num}" ;
		}
		
		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_vehicle tv 
  						LEFT JOIN tbl_dealer td ON tv.do_num = td.s_num 
  						LEFT JOIN tbl_operator top ON tv.do_num = top.s_num 
  						LEFT JOIN tbl_sub_dealer tsd ON tv.dso_num = tsd.s_num 
  						LEFT JOIN tbl_sub_operator tso ON tv.dso_num = tso.s_num 
  						where tv.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tv.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tv.vehicle_code like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"		=>  "Y",
			"td.tde01"		=>	"Y"
		);
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tv', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tv.do_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT tv.*
						,(CASE tv.DorO_flag WHEN 'D' THEN td.tde01 WHEN 'O' THEN top.top01 END ) as do_name
						,(CASE tv.DorO_flag WHEN 'D' THEN tsd.tsde01 WHEN 'O' THEN tso.tsop01 END ) as dso_name
  						FROM tbl_vehicle tv 
  						LEFT JOIN tbl_dealer td ON tv.do_num = td.s_num 
  						LEFT JOIN tbl_operator top ON tv.do_num = top.s_num 
  						LEFT JOIN tbl_sub_dealer tsd ON tv.dso_num = tsd.s_num 
  						LEFT JOIN tbl_sub_operator tso ON tv.dso_num = tso.s_num 
  						where tv.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 vehicle info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_vehicle where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_vehicle", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_vehicle
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		//if($dataArr['status'] == '2'){
		//	$dataArr['do_num'] = $this->input->post("td_num");
		//	$dataArr['dso_num'] = $this->input->post("tsd_num");
		//}else if($dataArr['status'] == '4'){
		//	$dataArr['do_num'] = $this->input->post("to_num");
		//	$dataArr['dso_num'] = $this->input->post("tso_num");
		//}

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("td_num");
			$dataArr['dso_num'] = $this->input->post("tsd_num");
		}else if($dataArr['DorO_flag'] == 'O'){
			$dataArr['do_num'] = $this->input->post("to_num");
			$dataArr['dso_num'] = $this->input->post("tso_num");
		}

		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_vehicle", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_vehicle
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		//if($dataArr['status'] == '2'){
		//	$dataArr['do_num'] = $this->input->post("td_num");
		//	$dataArr['dso_num'] = $this->input->post("tsd_num");
		//}else if($dataArr['status'] == '4'){
		//	$dataArr['do_num'] = $this->input->post("to_num");
		//	$dataArr['dso_num'] = $this->input->post("tso_num");
		//}
		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("td_num");
			$dataArr['dso_num'] = $this->input->post("tsd_num");
		}else if($dataArr['DorO_flag'] == 'O'){
			$dataArr['do_num'] = $this->input->post("to_num");
			$dataArr['dso_num'] = $this->input->post("tso_num");
		}

		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_vehicle", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_vehicle", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function getlog_driving_info($unit_id){
		$SQLCmd  = "SELECT latitude, longitude
					FROM log_driving_info ldi 
					WHERE unit_id = '{$unit_id}'
					order by ldi.s_num desc limit 1" ;
		$rs = $this->db_query($SQLCmd);	

		return $rs;
	}

	//確認车机编号唯一
	public function ajax_pk_car_machine_id(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得資料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		$fieldnum = $_GET['fieldnum'];
		
		$result = $this->countcar_machine($validateValue, $fieldnum);
		$arrayToJs = array();
		//一定要帶回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//無重複
		}else{
			$arrayToJs[1] = false;	//有重複
		}
		echo json_encode($arrayToJs);
	}

	//查出是否有重複
	public function countcar_machine($car_machine_id, $fieldnum){
		$where = "";
		if($car_machine_id != ''){
			if($fieldnum != ''){
				$where = " and tv.s_num != '{$fieldnum}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_vehicle tv 
						where tv.car_machine_id = '{$car_machine_id}' and tv.status <> 'D' {$where}";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	/**
	 * 新增 tbl_vehicle
	 */
	public function upload_insertData($dataArr)
	{
		if($dataArr['status'] == '2'){
			$dataArr['do_num'] = is_null($dataArr['td_num'])?'':$dataArr['td_num'];
			$dataArr['dso_num'] = is_null($dataArr['tsd_num'])?'':$dataArr['tsd_num'];
		}else if($dataArr['status'] == '4'){
			$dataArr['do_num'] = is_null($dataArr['to_num'])?'':$dataArr['to_num'];
			$dataArr['dso_num'] = is_null($dataArr['tso_num'])?'':$dataArr['tso_num'];
		}
		unset($dataArr['td_num']);
		unset($dataArr['tsd_num']);
		unset($dataArr['to_num']);
		unset($dataArr['tso_num']);

		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$add_flag = 'N';

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_vehicle", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			$add_flag = 'Y';
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
		}
		
		return $add_flag;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
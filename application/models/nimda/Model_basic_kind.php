<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_basic_kind extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'bsk', $setAlias);
		else $whereStr = "" ;
		
		$SQLCmd  = "SELECT count(*) cnt 
					FROM basic_kind bsk 
					where bsk.mark <> 'CL' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得使用者帐号清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'bsk', $setAlias);
		else $whereStr = "" ;
		
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}else{
			$sql_orderby = " order by bsk.sn desc";
		}
		
		$SQLCmd  = "SELECT * 
					FROM basic_kind bsk 
  					WHERE bsk.mark <> 'CL' {$whereStr} {$sql_orderby} 
  					LIMIT {$startRow} , {$offset} " ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search($search, $SQLCmd);
		return $rs ;
	}

	/**
	 * 取得单一 info
	 */
	public function getInfo( $kind_no = "" )
	{
		$SQLCmd  = "SELECT bsk.* 
					FROM basic_kind bsk 
   					WHERE bsk.kind_no = '{$kind_no}' " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//查出是否有重复
	public function countbasic_kind($kind_no){
		if($kind_no != ''){
			$SQLCmd = "	select count(*) as cnt
						from basic_kind bsk 
						where bsk.kind_no = '{$kind_no}' ";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //无资料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	/**
	 * 新增 
	 */
	public function insertData()
	{
		$postdata = $this->input->post("postdata");
		$hidden_sn = $this->input->post("hidden_sn");
		$kind_no = $postdata['kind_no'];
		
		//检查唯一
		$result = $this->countbasic_kind($kind_no);
		if (!$result){
			$this->redirect_alert("./index", '此地区代号重复, 或被停用') ;
			exit();
		}
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		
		//检查栏位资料
		//$this->checkFieldData('add');
		
		$dataArr['first_per'] = $this->session->userdata('user_sn');

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "basic_kind", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			//新增类别明细
			$this->Model_basic_code->insertBasic_code($kind_no, $hidden_sn);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
	
	/**
	 * 修改 sys_user
	 */
	public function updateData()
	{		
		$postdata = $this->input->post("postdata");
		$hidden_sn = $this->input->post("hidden_sn");
		
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				case 'kind_no':
					$kind_no = $value;
					break;
				default:
					$dataArr[$key] = $value;
					break;
			}
		}	
		
		$whereStr = "kind_no = '{$kind_no}'";
		
		//检查栏位资料
		//$this->checkFieldData('edit');
		
		$dataArr['change_per'] = $this->session->userdata('user_sn');
		$dataArr['change_date'] = "now()";
		
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "basic_kind", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[kind_no: '.$kind_no.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//新增类别明细
			$this->Model_basic_code->insertBasic_code($kind_no, $hidden_sn);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[kind_no: '.$kind_no.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " kind_no in ({$delIdStr}) ";
		$colDataArr = array (
			"mark"					=> "CL",
			"change_per"			=> $this->session->userdata('user_sn'), 
			"change_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "basic_kind", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[kind_no: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			//清除类别名系
			foreach($ckbSelArr as $key => $value){
				$this->Model_basic_code->deleteBasic_code($value);
			}
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[kind_no: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
			"user_id, txtAccount"		=> array("required" => "", "minSize" => 4, "maxSize" => 20), 
			"user_name, txtChName"		=> array("required" => "", "maxSize" => 10), 
			"mobile, txtMobile"			=> array("minSize" => 10, "maxSize" => 10, "phone" => ""), 
			"phone, txtTEL"				=> array("minSize" => 10, "maxSize" => 20, "phone" => ""), 
			"email, txtEmail"			=> array("maxSize" => 50, "email" => ""), 
			"group_sn, selGroupSN"		=> array("required" => ""), 
			"login_force_pwd, radioChange_password"		=> array("required" => "", "justsomevalue" => "0, 1"), 
			"status, selStatus"			=> array("required" => "", "justsomevalue" => "0, 1")
		);
		
		if($act == 'add'){
			$checkArr["password, pwPswd"] = array("required" => "", "minSize" => 7, "maxSize" => 12, "equals" => "pwPswd, pwPswd2");
		}else{
			$checkArr["user_sn, user_sn"] = array("required" => "", "integer" => "");
			$pwPswd = $this->input->post("pwPswd");
			$pwPswd2 = $this->input->post("pwPswd2");
			if(strlen($pwPswd) != 32 || strlen($pwPswd2) != 32){  //不是原本加密32码, 加入检查
				$checkArr["password, pwPswd"] = array("required" => "", "minSize" => 7, "maxSize" => 12, "equals" => "pwPswd, pwPswd2");
			}
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
	
	//确认类别代号唯一
	public function ajax_pk_kind_no(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得资料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		
		$result = $this->countbasic_kind($validateValue);
		$arrayToJs = array();
		//一定要带回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//无重复
		}else{
			$arrayToJs[1] = false;	//有重复
		}
		echo json_encode($arrayToJs);
	}

}
/* End of file model_user.php */
/* Location: ./application/models/model_user.php */
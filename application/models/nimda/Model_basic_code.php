<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_basic_code extends MY_Model {
	/**
	 * 取得类别明细
	 */
	public function getBasic_code_list( $kind_no = "" )
	{
		$SQLCmd  = "select * 
					from basic_code 
					where kind_no = '{$kind_no}' " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//新增类别明细
	public function insertBasic_code($kind_no = "", $hidden_sn = 0){
		if($kind_no != ""){
			$this->deleteBasic_code($kind_no);
			//新增资料
			for($i=0; $i<=$hidden_sn; $i++){
				$dataArr = array();
				$code_no = $this->input->post("code_no_{$i}");
				$code_name = $this->input->post("code_name_{$i}");
				$remark = $this->input->post("remark_{$i}");
				$sys_mark = $this->input->post("sys_mark_{$i}");
				if( $code_no != "" || 
					$code_name != "" ||
					$remark != "" ||
					$sys_mark != "" 
					){
					$dataArr['kind_no'] = $kind_no;
					$dataArr['code_no'] = $code_no;
					$dataArr['code_name'] = $code_name;
					$dataArr['remark'] = $remark;
					$dataArr['sys_mark'] = $sys_mark;
					$dataArr['first_per'] = $this->session->userdata('user_sn');
					$dataArr['first_date'] = "now()";
					
					$this->db_insert( "basic_code", $dataArr );
				}
			}
		}
		
	}
	
	public function deleteBasic_code($kind_no = ""){
		//清除客户相关联络人
		if($kind_no != ""){
			$SQLCmd  = "delete 
						from basic_code 
						where kind_no = '{$kind_no}' " ;
			$this->db->query($SQLCmd);
		}
	}
}
/* End of file model_user.php */
/* Location: ./application/models/model_user.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_user extends MY_Model {
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getUserAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"group_name"=> "sg"
		);		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'su', $setAlias);
		else $whereStr = "" ;
		
		//群组
		$SQLCmdg = "SELECT user_group FROM sys_group WHERE group_sn = {$this->session->userdata('group_sn')}";
		$rsg = $this->db_query($SQLCmdg);
		if($rsg)
		{
			if($rsg[0]['user_group']!="")
				$whereStr .= " AND sg.group_sn in ({$rsg[0]['user_group']})";
			else //没设定使用者群组，则看不到所有使用者
				$whereStr .=" AND 1=2";
		}
		
		$SQLCmd  = "SELECT count(*) cnt 
					FROM sys_user su 
					LEFT JOIN sys_user_group sug ON su.user_sn = sug.user_sn 
					LEFT JOIN sys_group sg ON sg.group_sn = sug.group_sn 
  					WHERE su.status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得使用者帐号清单
	 */
	public function getUserList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"group_name"=> "sg"
		);		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'su', $setAlias);
		else $whereStr = "" ;
		
		//群组
	    $SQLCmdg = "SELECT user_group FROM sys_group WHERE group_sn = {$this->session->userdata('group_sn')}";
		$rsg = $this->db_query($SQLCmdg);

		if($rsg)
		{
			if($rsg[0]['user_group']!="")
				$whereStr .= " AND sg.group_sn in ({$rsg[0]['user_group']})";
			else //没设定使用者群组，则看不到所有使用者
				$whereStr .=" AND 1=2";
		}
		
		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}else{
			$sql_orderby = " order by su.user_sn desc";
		}
		
		$SQLCmd  = "SELECT su.*, sg.group_name, sg.user_group 
  					FROM sys_user su
					LEFT JOIN sys_user_group sug 
						ON su.user_sn = sug.user_sn 
					LEFT JOIN sys_group sg 
						ON sg.group_sn = sug.group_sn 
  					WHERE su.status <> 'D' {$whereStr} {$sql_orderby} 
  					LIMIT {$startRow} , {$offset} " ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search($search, $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得群组为刷卡机验证员的所有使用者下拉选单
	 */
	public function getUserListNoPage()
	{
		$SQLCmd  = "SELECT su.user_sn, su.user_id ,su.user_name  
					FROM sys_user su 
					WHERE su.status <> 'D' " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//自动完成, 搜寻使用者
	public function autocomplete_user(){
		$userID = $this->input->get("userID") ;
		$SQLCmd = " SELECT su.user_id, su.user_name 
					FROM sys_user su 
					WHERE su.STATUS <> 'D' and su.user_id like '{$userID}%' " ;
	
		$rs = $this->db_query($SQLCmd) ;
		
		foreach($rs as $key => $value){
			$result[] = array( 
				'id' => $value['user_id'], 
				'label' => $value['user_id'] 
			); 
		}
		echo json_encode($result);  //输出json数据
	}

	/**
	 * 取得单一 user info
	 */
	public function getUserInfo( $user_sn = "" )
	{
		$SQLCmd  = "SELECT su.*, sg.group_sn,sg.group_name 
					FROM sys_user su
					LEFT JOIN sys_user_group sug 
						ON su.user_sn = sug.user_sn 
					LEFT JOIN sys_group sg 
						ON sg.group_sn = sug.group_sn 
   					WHERE su.user_sn={$user_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		return $rs ;
	}
	
	/**
	 * 取得单一 user menu权限资料
	 */
	public function getUserAccess( $user_sn = "" )
	{
		$SQLCmd  = "SELECT su.* 
					FROM sys_user su 
					LEFT JOIN sys_user_group sug 
						ON su.user_sn = sug.user_sn 
					LEFT JOIN sys_group sg 
						ON sg.group_sn = sug.group_sn 
 					LEFT JOIN sys_menu_group smg ON sug.group_sn = smg.group_sn 
					LEFT JOIN sys_user_menu_access suma ON su.user_sn = suma.user_sn and smg.menu_sn = suma.menu_sn  
   					WHERE su.user_sn = {$user_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得群组的选单资料
	 */
	public function getGroupMenu($group_sn = "")
	{
		$SQLCmd  = "select 
					smg.menu_sn, 
					smg.access_control, 
					sm.menu_name, 
					sm.parent_menu_sn 
					from sys_menu_group smg 
					left join sys_menu sm on smg.menu_sn = sm.menu_sn 
					left join sys_menu sm2 on sm.parent_menu_sn = sm2.menu_sn 
					where smg.group_sn = {$group_sn} 
					order by sm.menu_sequence ";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得使用者选单权限资料
	 */
	public function getUserMenu($user_sn = "")
	{
		$SQLCmd  = "select 
					suma.menu_sn, 
					suma.access_control
					from sys_user_menu_access suma 
					where suma.user_sn = {$user_sn} ";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得单一 user info
	 */
	public function getUserInfo2( $user_id = "" )
	{
		$SQLCmd  = " select 
					 su.user_sn, su.user_id, su.user_name  
					 from sys_user su
					 where su.user_id = '{$user_id}' " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//新增或修改使用者时,查询有无重复user_id
	public function countUser($user_sn = "", $user_id = ""){
		$return = false;
		$where = '';
		if($user_sn != ""){
			$where = " and user_sn <> {$user_sn} ";
		}
		
		$SQLCmd  = "select count(*) as cnt
					from sys_user 
					where user_id = '{$user_id}' {$where} AND status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		//echo $SQLCmd;
		if($rs[0]['cnt'] == 0){
			$return = true;
		}
		return $return;
	}
	
	/**
	 * 新增 sys_user
	 */
	public function insertUser()
	{
		$postdata = $this->input->post("postdata");
		
		//给予field栏位值
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				case "sn":
					break;
				case "user_sn":
					$user_sn = $value;
					$dataArr[$key] = $value;
					break;
				case "user_id":
					$user_id = $value;
					$dataArr[$key] = $value;
					break;
				case "pwd":
					$pwd = MD5($value);  //md5加密
					$dataArr[$key] = $pwd;
					$dataArr['old_pwd'] = $pwd;
					break;
				case "group_sn":
					$group_sn = $value;
					break;
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		
		$nowDate = date("Y-m-d H:i:s");
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_user'] = $this->session->userdata('user_sn');
		
		$dataArr['view_area'] = "";
		$all_area = $this->input->post("all_area");
		if(count($all_area)>0){
			$dataArr['view_area'] = join( ",", $all_area );
		}

		// $result = $this->countUser($user_sn, $user_id);
		// if (!$result){
		// 	$this->redirect_alert("./index", '帐号重复') ;
		// 	exit();
		// }

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 

		if ( $this->db_insert( "sys_user", $dataArr ) ) {
			//取得使用者的SN
			// $user_sn = mysql_insert_id();
			$SQLCmd = "SELECT user_sn FROM sys_user WHERE create_date = '{$nowDate}' AND create_user = {$dataArr['create_user']}";
			$rs = $this->db_query($SQLCmd);
			$user_sn = $rs[0]['user_sn'];

			// echo $user_sn;
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			//新增使用者群组
			$this->addGroup($user_sn,$group_sn);
			//新增使用者权限
			$this->addUserAccess($user_sn);

			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
	
	/**
	 * 修改 sys_user
	 */
	public function updateUser()
	{
		$postdata = $this->input->post("postdata");
		
		//给予field栏位值
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				case "sn":
					break;
				case "user_sn":
					$user_sn = $value;
					break;
				case "user_id":
					$user_id = $value;
					$dataArr[$key] = $value;
					break;
				case "pwd":
					$pwd = $value;  //md5加密
					break;
				case "group_sn":
					$group_sn = $value;
					break;
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		$user_sn = $this->input->post('user_sn');
		$dataArr['create_date'] = "now()";
		$dataArr['create_user'] = $this->session->userdata('user_sn');
		
		//查询原本密码
		$whereStr = " user_sn = {$user_sn}";
		$SQLCmd = "SELECT user_id, pwd FROM sys_user WHERE {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		
		if($pwd != $rs[0]['pwd']){	//有密码异动更新, 重置尝试次数
			$dataArr["pwd"] = md5($pwd);
			$dataArr["old_pwd"] = md5($pwd);
			$dataArr["login_retry_cnt"] = 0;
		}
		
		$dataArr['view_area'] = "";
		$all_area = $this->input->post("all_area");
		if(count($all_area)>0){
			$dataArr['view_area'] = join( ",", $all_area );
		}
		
		// $result = $this->countUser($user_sn, $user_id);
		// if (!$result){
			// $this->redirect_alert("./index", '帐号重复') ;
			// exit();
		// }
		// print_r('<pre>');
		// print_r($dataArr);
		// print_r('</pre>');
		// exit();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "sys_user", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[user_sn: '.$user_sn.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//更新使用者群组
			$this->updateGroup($user_sn, $group_sn);
			
			//新增使用者权限
			$this->addUserAccess($user_sn);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[user_sn: '.$user_sn.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//新增使用者权限
	public function addUserAccess($user_sn = ""){
		if($user_sn != ""){
			//删除使用者选单权限
			$SQLCmd = "delete from sys_user_menu_access where user_sn = {$user_sn}";
			$this->db_query($SQLCmd);
			
			//新增使用者权限
			//选单
			$sub_menu = $this->input->post("sub_menu");
			//权限
			$sub_control = $this->input->post("sub_control");
			if(count($sub_menu) > 0){
				foreach($sub_menu as $key =>$value){
					$access = 0;
					if($value == '1'){
						$menu_sn = $key;
						if(isset($sub_control[$menu_sn])){
							$arr_access = $sub_control[$menu_sn];
							//加总权限
							foreach($arr_access as $key2 => $value2){
								$access += $value2;
							}
						}
						$colDataArr = array(
							"user_sn"				=> $user_sn,
							"menu_sn"				=> $menu_sn,
							"access_control"		=> $access,
							"create_date"			=> "now()",
							"create_user"			=> $this->session->userdata('user_sn')
						) ;
						$this->db_insert( "sys_user_menu_access", $colDataArr );
					}
				}
			}
		}
	}
	
	function addGroup($user_sn = "", $group_sn = ""){
		$colDataArr = array(
			"user_sn"					=> $user_sn,
			"group_sn"					=> $group_sn,
			"status"					=> "1",
			"create_date"				=> "now()",
			"create_user"				=> $this->session->userdata("user_sn")
		) ;
		$this->db_insert( "sys_user_group", $colDataArr);
	}

	function updateGroup($user_sn = "", $group_sn = ""){
		$colDataArr = array(
			"group_sn"					=> $group_sn
		) ;
		$whereStr = "user_sn = {$user_sn}";
		$this->db_update( "sys_user_group", $colDataArr, $whereStr );
	}
	
	/**
	 * 修改密码 sys_user
	 */
	public function updateUserPassword($mode = "")
	{
	
	
		//取得密码有效期限天数, 找不到值预设30天
		$change_password_day = '30';
		if($this->session->userdata('change_password_day')){
			$change_password_day = $this->session->userdata('change_password_day');
		}
		$SQLCmd  = "select date_format(DATE_ADD(now(), INTERVAL {$change_password_day} DAY), '%Y-%m-%d') as date";
		$rs = $this->db_query($SQLCmd) ;
		$date_30 = $rs[0]['date'];
		
		$whereStr = " user_sn={$this->session->userdata('user_sn') }" ;
		$pwd = MD5($this->input->post("pwPswd"));  //加密密码
		$old_pwd = $this->insert_old_password($pwd);	//加入旧密码
		$colDataArr = array(
			"pwd"					=> $pwd,
			"old_pwd"				=> $old_pwd,
			"login_force_pwd"		=> "0",	//有更改强制变更就改回0
			"pwd_deadline"	=> "{$date_30}", //密码有效期限 更新+30天
			"update_date"				=> "now()",
			"update_user"				=> $this->session->userdata('user_sn')
		) ;

		//$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 

		if ( $this->db_update( "sys_user", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[user_sn: '.$this->session->userdata('user_sn').']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			if($mode == "front"){
				$this->session->set_userdata("login_force_pwd", "0");
				$this->redirect_alert($this->config->item('base_url')."login", "{$this->lang->line('edit_successfully')}") ;
			}else{
				$this->session->set_userdata("login_force_pwd", "0");
				if($this->session->userdata("login_way") == "nimda"){	//登入方式
					$this->redirect_alert($this->config->item('base_url')."nimda/login", "{$this->lang->line('edit_successfully')}") ;
				}else{
					$this->redirect_alert($this->config->item('base_url')."login", "{$this->lang->line('edit_successfully')}") ;
				}
			}
		} else {
				$this->session->set_userdata("login_force_pwd", "0");
				if($this->session->userdata("login_way") == "nimda"){	//登入方式
					$this->redirect_alert($this->config->item('base_url')."nimda/login", "{$this->lang->line('edit_successfully')}") ;
				}else{
					$this->redirect_alert($this->config->item('base_url')."login", "{$this->lang->line('edit_successfully')}") ;
				}
		}
	}
	
	//新增旧密码
	//$password  传入已加密的密码
	public function insert_old_password($password = ""){
		
		$SQLCmd  = "select old_pwd 
					from sys_user 
					where status <> 'D' and 
					user_sn={$this->session->userdata('user_sn')} ";
		$rs = $this->db_query($SQLCmd) ;
		$old_pwd = $rs[0]["old_pwd"];
		$old_pwd = explode(",", $old_pwd);
		
		$count = count($old_pwd);
		
		if($count == 1 && $old_pwd[0] == ""){
			$old_pwd[0] = $password;
		}else{
			$old_pwd[$count+1] = $password;
		}
		
		if(count($old_pwd) <= 3){	//当旧密码数量小于等于3组, 直接写入
		
		}else{	//超过3组, 取最新改的3组
			unset($old_pwd[0]);	//删除第1组(最旧)
			$old_pwd = array_values($old_pwd);  //重整排序
		}
		return join(",", $old_pwd);
	}
	
	//查询旧密码是否输入正确
	public function check_current_password(){
		$current_pwPswd = MD5($this->input->post("current_pwPswd"));
		
		$SQLCmd  = "select count(*) as cnt 
					from sys_user 
					where status <> 'D' and 
					user_sn={$this->session->userdata('user_sn') } and 
					pwd='{$current_pwPswd}' ";
		$rs = $this->db_query($SQLCmd) ;
		//echo $this->input->post("current_pwPswd");
		if($rs[0]['cnt'] > 0){
			echo '1';
		}else{
			echo '0';
		}
	}
	
	//自动给予新增使用者ID
	//先抓取目前使用者商店最后一笔帐号, 增加后三码数值 如001->002->003..., 如果没有则ID直接增加001
	public function autoUserID(){
		$user_merchant = $this->session->userdata('user_merchant');
		$SQLCmd  = " select user_id from sys_user 
					 where merchant_id = '{$user_merchant}' 
					 and user_id like '{$user_merchant}%' 
					 order by user_sn desc LIMIT 1";
					 
		$rs = $this->db_query($SQLCmd) ;
		
		$first = $user_merchant;
		if(count($rs) == 0){	//无
			$last3 = '001';
		}else{	//有
			$user_id = $rs[0]['user_id'];
			$last3 = substr($user_id, -3);
			//判断后3码是否为数字
			if(preg_match("/^[0-9]+$/", $last3)){
				$last3 = intval($last3);
				$last3 = $last3 +1;
			}else{	//没有就补001
				$last3 = '001';
			}
			$last3 = str_pad($last3, 3, "0", STR_PAD_LEFT);
		}
		$user_id = $first.$last3;

		return $user_id;
	}
	
	/**
	 * 删除 sys_user
	 */
	public function deleteUser()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		
		$return = false;
		foreach($ckbSelArr as $key => $value){
			$return = $this->check_nimdaGroup($value);
			if($return){
				$this->redirect_alert("./index", "不可删除超级管理员") ;
				exit();
			}
		}
		
		$delIdStr = join( ',', $ckbSelArr ) ;

		$whereStr = " user_sn in ({$delIdStr}) ";
		$colDataArr = array (
			"status"		=> "D",
			"update_date"	=> "now()",
			"update_user"	=> $this->session->userdata('user_sn')
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 

		if ( $this->db_delete( "sys_user", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[user_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[user_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
	
	//确认是否是为超级管理员
	public function check_nimdaGroup($user_sn){
		$rs = $this->getUserInfo($user_sn);
		$group_sn = $rs[0]['group_sn'];
		if($group_sn == '1'){	//超级管理员
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 后台首页 Panel ：目前正在线上验证的人员
	 */
	public function getOnlineVerifyUserList()
	{
		// 先删除已过期的 Session
		//$ExpiredTime = time() - $this->config->item('sess_expiration');
		$ExpiredTime = time() - $this->session->userdata("session_expiration");
		
		$SQLCmd  = "DELETE FROM log_sessions 
					WHERE last_activity < {$ExpiredTime} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		$where = "";
		
		//echo 'A:'.$this->session->userdata("user_db_name");
		// 取得目前正在线上验证的人员
		$SQLCmd  = "SELECT ls.user_sn, su.user_name, su.mobile, su.tel 
					FROM log_sessions ls
					left join sys_user su 
						on ls.user_sn = su.user_sn 
						{$where}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//检核是否有使用到近四笔旧密码
	public function check_old_password(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得资料
		$validateValue = MD5($_GET['fieldValue']);
		$validateId = $_GET['fieldId'];
		
		$SQLCmd  = "select old_pwd 
					from sys_user 
					where status <> 'D' and 
					user_sn={$this->session->userdata('user_sn')} ";
		$rs = $this->db_query($SQLCmd) ;			
		$old_pwd = $rs[0]["old_pwd"];
		$old_pwd = explode(",", $old_pwd);
		
		$arrayToJs = array();
		//一定要带回$validateId
		$arrayToJs[0] = $validateId;
		if(in_array($validateValue, $old_pwd)){
			$arrayToJs[1] = false;	//有重复
		}else{
			$arrayToJs[1] = true;	//无重复
		}
		echo json_encode($arrayToJs);
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
			"user_id, txtAccount"		=> array("required" => "", "minSize" => 4, "maxSize" => 20), 
			"group_sn, selGroupSN"		=> array("required" => ""), 
			"login_force_pwd, radioChange_password"		=> array("required" => "", "justsomevalue" => "0, 1"), 
			"status, selStatus"			=> array("required" => "", "justsomevalue" => "0, 1")
		);
		
		if($act == 'add'){
			$checkArr["password, pwPswd"] = array("required" => "", "minSize" => 7, "maxSize" => 12, "equals" => "pwPswd, pwPswd2");
		}else{
			$checkArr["user_sn, user_sn"] = array("required" => "", "integer" => "");
			$pwPswd = $this->input->post("pwPswd");
			$pwPswd2 = $this->input->post("pwPswd2");
			if(strlen($pwPswd) != 32 || strlen($pwPswd2) != 32){  //不是原本加密32码, 加入检查
				$checkArr["password, pwPswd"] = array("required" => "", "minSize" => 7, "maxSize" => 12, "equals" => "pwPswd, pwPswd2");
			}
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
	
	//查出是否有重复
	public function countuser_id($user_id){
		if($user_id != ''){
			$SQLCmd = "	select count(*) as cnt
						from sys_user su 
						where su.user_id = '{$user_id}' AND status <> 'D'";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //无资料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	//确认部门代号唯一
	public function ajax_pk_user_id(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得资料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		
		$result = $this->countuser_id($validateValue);
		$arrayToJs = array();
		//一定要带回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//无重复
		}else{
			$arrayToJs[1] = false;	//有重复
		}
		echo json_encode($arrayToJs);
	}

}
/* End of file model_user.php */
/* Location: ./application/models/model_user.php */
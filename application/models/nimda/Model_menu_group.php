<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_menu_group extends MY_model {
	private $control_colnum = 4;  //权限操作栏位数目
	
	/**
	 * 取得单一 menu_group info
	 */
	public function getMenuGroupInfo( $group_sn = "", $login_side = "")
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
		
		//不是超级管理员, 无法看到群组管理
		$where = "";
		if($group_sn != '1'){
			$where = ' and sm.menu_sn <> 6';
		}
		
		//对应登入条件
		if($login_side != ""){
			$where .= " and (sm.login_side_display = '2' or sm.login_side_display = '{$login_side}') ";
		}

		if($group_sn != "")
		{
			$SQLCmd  = "SELECT DISTINCT sm.menu_sn, {$field}, sm.parent_menu_sn, sm.second_menu_sn, ssgm.group_sn, 
					sm.access_control as m_control, 
					ssgm.access_control as mg_control , ssgm.main_flag
					FROM sys_menu sm
					LEFT JOIN sys_menu_group ssgm ON (ssgm.menu_sn = sm.menu_sn AND ssgm.group_sn = $group_sn)
					WHERE sm.status = '1' {$where}
					ORDER BY sm.menu_sequence" ;
		}
		else
		{
			$SQLCmd  = "SELECT DISTINCT sm.menu_sn, {$field}, sm.parent_menu_sn, '' as group_sn, 
						sm.access_control as m_control  , '' as main_flag
						FROM sys_menu sm 
						WHERE sm.status = '1' {$where}
						ORDER BY sm.menu_sequence" ;
		}

		$rs = $this->db_query($SQLCmd) ;
		//echo $SQLCmd;
		return $rs ;
	}
	
	//取得对应群组, 对应前后台的选单
	public function getLogin_menu(){
		$group_sn = $this->input->post("group_sn");
		$login_side = $this->input->post("login_side");
		$MenuGroupInfo = $this->getMenuGroupInfo($group_sn, $login_side);
		//栏位数
		$colnum = 1;
		$main_colnum = $colnum + 1 + $this->control_colnum-2;
		echo "<table width='100%' id='meunTable'>";
		foreach($MenuGroupInfo as $key => $MenuGroupArr){
			if($MenuGroupArr["parent_menu_sn"] == "0"){
				$menu_sn = $MenuGroupArr["menu_sn"];
				$SelY = "";
				if ($MenuGroupArr["group_sn"] != ""){
					$SelY = " checked";
				}
				echo <<<html
				<tr>
					<td style="background-color: #fafad2;">首页捷径</td>
					<td class="main" colspan="2">
						<input id="menu_sn_{$MenuGroupArr["menu_sn"]}" name="selStatus[{$MenuGroupArr["menu_sn"]}]" 
							type="checkbox" value="1" class="parent" main_sn="{$MenuGroupArr["menu_sn"]}" {$SelY} />
						{$MenuGroupArr["menu_name"]}
					</td>
					<td id="control_label" colspan="{$main_colnum}">
						{$this->lang->line('menu_access_control')}
					</td>
				</tr>
html;
				$this->submenu($colnum, $menu_sn, $MenuGroupInfo);
			}
		}
		echo '</table>';
		
	}
	
	//子栏位
	public function submenu($colnum, $menu_sn ,$arrData){
		$i = 0;
		//换行前栏位数
		$last = 0;
		foreach($arrData as $key => $value){
			if($value['parent_menu_sn'] == $menu_sn){
				$SelY = "";
				if ($value["group_sn"] != ""){
					$SelY = " checked";
				}
			
				$SelM = "";
				if ($value["main_flag"] == 1){
					$SelM = " checked";
				}
			
				if($i % $colnum == 0){
					if($i != 0){
						//补td
						for($y = 1; $y <= $colnum-$last; $y++){
							echo '<td></td>';
						}
						echo '</tr>';
					}
					$last = 0;
					echo '<tr><td class="subfirst"></td>';
					
				}
				
				$mg_control = 0;
				if(isset($value['mg_control'])){
					$mg_control = $value['mg_control'];
				}
				echo <<<html
					<td>
						<input name="main_flag_{$value["menu_sn"]}"  type="checkbox" sub_sn="{$value["menu_sn"]}" parent_sn={$menu_sn} value="1" {$SelM} />
					</td>
					<td>
						<input type="hidden" name="org{$value["menu_sn"]}" value="{$mg_control}" />
						<input name="selStatus[{$value["menu_sn"]}]" 
							type="checkbox" sub_sn="{$value["menu_sn"]}" class="sub" parent_sn={$menu_sn} value="1" {$SelY} />
						{$value["menu_name"]}
					</td>
html;
				if(isset($value['mg_control'])){  //修改
					$this->sub_access_control($menu_sn, $value["menu_sn"], $value['m_control'], $value['mg_control']);
				}else{  //新增
					$this->sub_access_control($menu_sn, $value["menu_sn"], $value['m_control']);
				}
				$last++;
				$i++;
			}
		}
		
		if($i != 0){
			//补td
			for($y = 1; $y <= $colnum-$last; $y++){
				echo '<td></td>';
			}
			echo '</tr>';
		}
	}
	
	//子功能, 操作权限
	//$menu_sn 父编号, $sub_menu_sn 子编号, $m_control 选单有的操作功能, $mg_control 群组拥有的操作功能
	public function sub_access_control($menu_sn, $sub_menu_sn, $m_control ,$mg_control = ""){
		$arr_access_control_list = $this->model_access->access_control_list();
		if($m_control != ""){
			$i = 1;
			//列出该选单所拥有的功能
			foreach($arr_access_control_list as $access_key =>$access_name){
				if($i % ($this->control_colnum+1) == 0){
					if($i / ($this->control_colnum+1) >= 2){ //第二次以后的tr
						echo '</tr>';
					}
					if(($m_control & $access_key) === $access_key){
						echo '<tr>
								<td colspan="2"></td>';
					}
				}
				//选单预设开启的功能
				if(($m_control & $access_key) === $access_key){
					//群组选择开启的功能
					$checked = '';
					if(($mg_control & $access_key) === $access_key){
						$checked = 'checked';
					}
					
					echo <<<html
						<td class='td_control'>
							<input name="sub_control[{$sub_menu_sn}][]" 
								type="checkbox" class="sub_control" parent_sn={$menu_sn} sub_parent_sn={$sub_menu_sn} value="{$access_key}" {$checked} />
							{$access_name}
						</td>
html;
					$i++;
				}
				
				
			}
		}
	}
	
	/**
	 * 新增 sys_menu_group
	 */
	public function insertMenuGroup( $group_sn )
	{
		/**
		 * 先删除原本所有该使用者的 "使用者系统资料档"
		 */
		$SQLCmd  = "DELETE FROM sys_menu_group 
					WHERE group_sn = {$group_sn} " ;
		$rs = $this->db_query($SQLCmd) ;

		/**
		 * 新增资料库
		 */
		$arrData = $this->input->post("selStatus");
		if(count($arrData) > 0 && is_array($arrData)){
			foreach ($this->input->post("selStatus") as $key => $value)
			{
				if($value == '1')
				{
					$colDataArr = array(
						"group_sn"		=> $group_sn,
						"menu_sn"		=> $key,
						"status"		=> "1",
						"create_date"	=> "now()",
						"create_user"	=> $this->session->userdata('user_sn'),
						"create_ip"			=> $this->input->ip_address()
					) ;

					$this->db_insert( "sys_menu_group", $colDataArr );
				}
			}
		}
		$web = $this->config->item('base_url');
		$this->redirect_alert("{$web}nimda/group/", "{$this->lang->line('edit_successfully')}") ;
	}

}
/* End of file model_menu_group.php */
/* Location: ./application/models/model_menu_group.php */
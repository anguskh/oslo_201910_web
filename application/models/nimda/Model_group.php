<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_group extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getGroupAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, '', $setAlias);
		else $whereStr = "" ;

		$SQLCmd = "select count(*) cnt from sys_group where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得群组清单
	 */
	public function getGroupList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, '', $setAlias);
		else $whereStr = "" ;
		
		//排序动作
		$sql_orderby = " order by group_sn desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$SQLCmd = "SELECT group_sn, group_name, status
  						FROM sys_group where status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search($search, $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得群组下拉选单
	 */
	 //修改目标的群组
	public function getGroupSelectList( $group_sn = "" ,$nowgroupsn = "", $insert = false, $user_group = "")
	{
		$where = "";
		//不论新增修改, 只要有超级管理员都无法将其他人新增或修改为超级管理员
			//检查有无, 超级管理员, 如果有, 使用者群组就无超级管理员选择项目
		if($user_group != '1'){	//修改目标的群组
			$return = $this->count_nimdaGroup();
			if($return){
				$where = " and group_sn <>'1' ";
			}
		}
		
		$SQLCmd = "SELECT group_sn, group_name, login_side, to_flag, card_flag FROM sys_group where status = '1' " ;
		if($nowgroupsn !="")
		{
			$SQLCmdg = "SELECT user_group FROM sys_group WHERE group_sn = {$nowgroupsn} ";
			$rsg = $this->db_query($SQLCmdg);
			if($rsg)
				$SQLCmd .= "AND group_sn in ({$rsg[0]['user_group']}) ";
		}
		if( !empty( $group_sn ) )
			$SQLCmd .= "AND group_sn={$group_sn} ";
			
		$SQLCmd .= $where;
		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得群组下拉选单
	 */
	 //修改目标的群组
	public function getGroupSelectList2( $group_sn = "" ,$nowgroupsn = "", $insert = false, $user_group = "")
	{
		$where = "";
		
		$SQLCmd = "SELECT group_sn, group_name, login_side FROM sys_group where status = '1' " ;
		if($nowgroupsn !="")
		{
			$SQLCmdg = "SELECT user_group FROM sys_group WHERE group_sn = {$nowgroupsn} ";
			$rsg = $this->db_query($SQLCmdg);
			if($rsg)
				$SQLCmd .= "AND group_sn in ({$rsg[0]['user_group']}) ";
		}
		if( !empty( $group_sn ) )
			$SQLCmd .= "AND group_sn={$group_sn} ";
			
		$SQLCmd .= $where;
		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//查看是否已经有超级管理员
	public function count_nimdaGroup(){
		$SQLCmdg = "SELECT count(*) cnt 
					FROM sys_user_group 
					WHERE group_sn = '1' ";
		$rs = $this->db_query($SQLCmdg);
		if($rs[0]['cnt'] > 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 取得单一 group info
	 */
	public function getGroupInfo( $group_sn = "" )
	{
		
		$whereArr = array ( "group_sn" => $group_sn ) ;
		$SQLCmd = "SELECT * FROM sys_group where group_sn={$group_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "sys_group", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 新增 sys_group
	 */
	public function insertGroup()
	{
		$group_name	= $this->input->post("txtChName") ;
		$status		= $this->input->post("selStatus") ;
		$usergroupArr = $this->input->post("usergroup");
		
		//检查group是否有重复名称
		$cnt = $this->count_Group_name($group_name);
		if($cnt != 0){	
			$this->redirect_alert("./index", "{$this->lang->line('group_name_duplicate')}") ;
			exit();
		}
		
		//检查栏位资料
		$this->checkFieldData('add');
		
		$usergroup = "";
		date_default_timezone_set("Asia/Taipei");
		$Nowdate = date("Y-m-d H:i:s");
		if($usergroupArr!='' && is_array($usergroupArr))
		{
			foreach($usergroupArr as $key => $value)
			{
				if($usergroup=="")
					$usergroup = $value;
				else
					$usergroup .= ",".$value;
			}
		}
		/**
		 * 新增资料库 方法一
		 */
		/*
		$SQLCmd = "insert into sys_group (group_name, status, create_date) values 
				( '{$group_name}', '{$status}', now() )" ;
		echo "SQLCmd : $SQLCmd";
	
		$this->db_query($SQLCmd) ;*/
		
		/**
		 * 新增资料库 方法二
		 */
		$card_flag = $this->input->post("card_flag");
		$card_flag = ($card_flag == '')?0:$card_flag; 
		$to_flag = $this->input->post("to_flag");
		$to_flag = ($to_flag == '')?0:$to_flag; 
		$colDataArr = array(
			"group_name"		=> $this->input->post("txtChName"),
			"user_group"		=> $usergroup,
			"login_side"		=> $this->input->post("selplatform"),
			"to_flag"			=> $to_flag,
			"card_flag"			=> $card_flag,
			"status"			=> $this->input->post("selStatus1"),
			"create_date"		=> $Nowdate,
			"create_user"		=> $this->session->userdata('user_sn'),
			"create_ip"			=> $this->input->ip_address()
		) ;

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "sys_group", $colDataArr ) ) {
			$sql_cmd = $this->session->userdata("sql_cmd");
			
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $colDataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$SQLCmdg = "SELECT group_sn FROM sys_group WHERE create_date = '{$Nowdate}'";
			$rsg = $this->db_query($SQLCmdg);
			$group_sn = $rsg[0]['group_sn'];
			//将新增的群组加到超级管理员权限里面
			$SQLCmds = "SELECT user_group FROM sys_group WHERE group_sn = '1'";
			$rss = $this->db_query($SQLCmds);
			$whereStr = " group_sn = 1";
			$dataS['user_group'] = $rss[0]['user_group'].','.$group_sn;
			$this->db_update('sys_group',$dataS,$whereStr);
			/**
			 * 新增群组选单
			 * 先删除原本所有该使用者的 "使用者系统资料档"
			 */
			$SQLCmd  = "DELETE FROM sys_menu_group 
						WHERE group_sn = {$group_sn} " ;
			$rs = $this->db_query($SQLCmd) ;

			/**
			 * 新增资料库至群组与选单对应表
			 */
			$arrData = $this->input->post("selStatus");
			$arr_sub_control = $this->input->post("sub_control");
			if(count($arrData) > 0 && is_array($arrData)){
				foreach ($this->input->post("selStatus") as $key => $value)
				{
					$sub_control = 0;
					if(isset($arr_sub_control[$key])){
						foreach($arr_sub_control[$key] as $value){
							$sub_control += $value;
						}
					}
					$arrTmp[] = $key;
					$colDataArr = array(
						"group_sn"			=> $group_sn,
						"menu_sn"			=> $key,
						"access_control"	=> $sub_control,
						"status"			=> "1",
						"create_date"		=> "now()",
						"create_user"		=> $this->session->userdata('user_sn'),
						"create_ip"			=> $this->input->ip_address()
					) ;

					//上首页
					$main_flag = $this->input->post("main_flag_".$key);
					$colDataArr1['main_flag'] = isset($main_flag)?$main_flag:'0';

					$this->db_insert( "sys_menu_group", $colDataArr );
				}
			}
			
			//新增纪录档
			$this->session->set_userdata("sql_cmd", $sql_cmd);
			$this->session->set_userdata("before_desc", '');
			$arrTmp = join(',', $arrTmp);
			$target_key = "skip[menu_sn: {$arrTmp}]";
			$this->session->set_userdata("desc", $colDataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//失败纪录
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
	
	/**
	 * 修改 sys_group
	 */
	public function updateGroup()
	{
		//检查栏位资料
		$this->checkFieldData('edit');	
		
		$group_sn = $this->input->post("group_sn");
		
		$whereStr = " group_sn={$this->input->post("group_sn") }" ;
		$usergroupArr = $this->input->post("usergroup");
		$usergroup = "";
		if($usergroupArr!='' && is_array($usergroupArr))
		{
			foreach($usergroupArr as $key => $value)
			{
				if($usergroup=="")
					$usergroup = $value;
				else
					$usergroup .= ",".$value;
			}
		}
		$card_flag = $this->input->post("card_flag");
		$card_flag = ($card_flag == '')?0:$card_flag; 
		$to_flag = $this->input->post("to_flag");
		$to_flag = ($to_flag == '')?0:$to_flag; 
		$colDataArr = array(
			"group_name"		=> $this->input->post("txtChName"),
			"user_group"		=> $usergroup,
			"login_side"		=> $this->input->post("selplatform"),
			"to_flag"			=> $to_flag,
			"card_flag"			=> $card_flag,
			"status"			=> $this->input->post("selStatus1"),
			"update_date"		=> "now()",
			"update_user"		=> $this->session->userdata('user_sn'),
			"update_ip"			=> $this->input->ip_address()
		) ;
		
		//检查群组名称有无重复
		$cnt = $this->count_Group_name($this->input->post("txtChName"), $this->input->post("group_sn"));
		if($cnt != 0){	
			$this->redirect_alert("./index", "{$this->lang->line('group_name_duplicate')}") ;
			exit();
		}
		

		// $this->db_update( "sys_group", $colDataArr, $whereArr ) 
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		/**
		 * 修改群组选单
		 * 先删除原本所有该使用者的 "使用者系统资料档"
		 */
		$SQLCmd  = "DELETE FROM sys_menu_group 
					WHERE group_sn = {$group_sn} " ;
		$rs = $this->db_query($SQLCmd) ;

		/**
		 * 新增资料库
		 */
		$arrData = $this->input->post("selStatus");
		$arr_sub_control = $this->input->post("sub_control");
		if(count($arrData) > 0 && is_array($arrData)){
			foreach ($this->input->post("selStatus") as $key => $value)
			{
				$sub_control = 0;
				if(isset($arr_sub_control[$key])){
					foreach($arr_sub_control[$key] as $value){
						$sub_control += $value;
					}
				}
				$arrTmp[] = $key;
				$colDataArr1 = array(
					"group_sn"			=> $group_sn,
					"menu_sn"			=> $key,
					"access_control"	=> $sub_control,
					"status"			=> "1",
					"create_date"		=> "now()",
					"create_user"		=> $this->session->userdata('user_sn'),
					"create_ip"			=> $this->input->ip_address()
				) ;

				//上首页
				$main_flag = $this->input->post("main_flag_".$key);
				$colDataArr1['main_flag'] = isset($main_flag)?$main_flag:'0';

				$this->db_insert( "sys_menu_group", $colDataArr1 );
				
				//只修改使用者权限与原群组权限共通的地方, 使用者有自订的权限, 将不作任何异动
				//取得原本群组权限
				$org_access = $this->input->post("org".$key);
				//取得该群组底下的使用者, 和权限
				$SQLCmd = " select sug.user_sn, suma.access_control 
							from sys_user_group sug
							left join sys_user_menu_access suma 
								on sug.user_sn = suma.user_sn 
								and suma.menu_sn = {$key} 
							where sug.group_sn = {$group_sn} ";
				$arr_user = $this->db_query($SQLCmd);
				foreach($arr_user as $user_key => $user_value){
					$user_access_control = $user_value['access_control'];
					$user_sn = $user_value['user_sn'];
					//取出原群组和使用者群组不一样的地方
					$user_xor_access = abs($org_access - $user_access_control);
					//扣掉现在的群组权限, 这样就不会异动到使用者原有异动的权限
					$arr_access_control_list = $this->model_access->access_control_list();
					$xor = 0;
					foreach($arr_access_control_list as $access_key => $access_value){
						if(($user_xor_access & $access_key) === $access_key){  //不一样, 不异动
							$xor += $access_key;
						}else{  //一样
							if(($sub_control & $access_key) === $access_key){  //更新异动
								$xor += $access_key;
							}
						}
					}
					
					
					$userArr = array(
						"access_control"	=> $xor,
						"create_date"		=> "now()",
						"create_user"		=> $this->session->userdata('user_sn')
					) ;
					$userwhere = " user_sn = {$user_sn} and menu_sn = {$key} ";
					$this->db_update( "sys_user_menu_access", $userArr, $userwhere );
				}
			}
		}
		//exit();
		
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "sys_group", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = "[group_sn: {$group_sn}]";
			$this->model_background->log_operating_insert('2', $target_key);
			
			//纪录修改资料2
			$this->session->set_userdata("before_desc", '');
			$arrTmp = join(',', $arrTmp);
			$target_key = "skip[menu_sn: {$arrTmp}]";
			$this->session->set_userdata("desc", $colDataArr1);
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//纪录修改失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = "[group_sn: {$group_sn}]";
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	/**
	 * 删除 sys_group
	 */
	public function deleteGroup()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$delIdStr = join( ',', $ckbSelArr ) ;
		$whereStr = " group_sn in ({$delIdStr}) ";
		
		$colDataArr = array (
			"status"		=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "sys_group", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[group_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[group_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
	
	//查询group, 是否有在使用者之中使用
	public function checkUserGroupCount(){
		$dataArr = $this->input->post("dataArr");
		//$dataArr = join( ',', $dataArr );
		foreach($dataArr as $key => $value){
			$SQLCmd = " select group_name 
						from sys_group 
						where group_sn = {$value} and 
						(select count(*) from sys_user_group 
							where status <> 'D' 
							and group_sn = {$value}) > 0 ";
			$rs = $this->db_query($SQLCmd);
			if(count($rs) > 0){
				echo "[".$rs[0]["group_name"]."]\n";
			}
		}
	}
	
	//检查group name是否有重复名称
	//$insert == false, 则检查修改本身除外的group name
	public function count_Group_name($group_name = "", $group_sn = ""){
		if($group_name != ""){
			$where = "";
			if($group_sn != ""){
				$where = " and group_sn <> {$group_sn} ";
			}
		
			$SQLCmd = " select count(*) cnt from sys_group 
						where status <> 'D' 
						and group_name = '{$group_name}' {$where} ";
			$rs = $this->db_query($SQLCmd) ;
			//echo $SQLCmd;
			return $rs[0]["cnt"];
		}else{
			return 1;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
			"group_name, txtChName"		=> array("required" => "", "maxSize" => 10), 
			"login_side, selplatform"	=> array("required" => "", "justsomevalue" => "0, 1"), 
			"status, selStatus1"		=> array("required" => "", "justsomevalue" => "0, 1")
		);
		
		if($act == 'add'){
		}else{
			$checkArr["group_sn, group_sn"] = array("required" => "", "integer" => "");
		}

		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
	
	/**
	 * 取得使用者的群组
	 */
	public function getUserGroup( $user_sn = "" )
	{
		if($user_sn != ""){
			$SQLCmd = "	SELECT group_sn 
						FROM sys_user_group 
						where user_sn = {$user_sn}" ;
			$rs = $this->db_query($SQLCmd) ;
			if(isset($rs[0]['group_sn'])){
				return $rs[0]['group_sn'];
			}else{
				return 0;
			}
			return $rs ;
		}else{
			return 0 ;
		}
	}
}
/* End of file model_group.php */
/* Location: ./application/models/model_group.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_shortcut extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getShortcutAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		if ( !empty( $search) )	$whereStr = "and shortcut_image_name like '%{$search}%'" ;
		else $whereStr = "" ;

		$SQLCmd = "select count(*) cnt from sys_shortcut where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得捷径清单
	 */
	public function getShortcutList( $offset, $startRow = "" )
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
	
		if ( empty( $startRow ) ) $startRow = 0 ;
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		if ( !empty( $search) )	$whereStr = "and shortcut_name like '%{$search}%'" ;
		else $whereStr = "" ;
	    
		$SQLCmd  = "SELECT shortcut_sn, shortcut_image_name, shortcut_sequence, {$field}, sm.menu_directory, ss.status
					FROM sys_shortcut ss
					LEFT JOIN sys_menu sm ON sm.menu_sn = ss.menu_sn
					WHERE ss.status <> 'D' {$whereStr}
					ORDER BY shortcut_sequence
					LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得可以显示的捷径清单
	 */
	public function getShortcutListShow()
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
		
		$SQLCmd  = "SELECT DISTINCT ss.shortcut_sn, ss.shortcut_image_name, ss.shortcut_sequence, 
					{$field}, sm.menu_directory, ss.status
					FROM sys_menu_group smg, sys_shortcut ss, sys_menu sm
					WHERE smg.status = '1'
					AND ss.status = '1'
					AND smg.group_sn = {$this->session->userdata('group_sn')}
					AND smg.menu_sn = ss.menu_sn
					AND smg.menu_sn = sm.menu_sn
					ORDER BY ss.shortcut_sequence" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//取得使用者, 我的最爱资料
	public function get_user_favor(){
		$user_sn = $this->session->userdata('user_sn');
		
		$SQLCmd = " select favor 
					from sys_user_favor 
					where user_sn = {$user_sn} ";
		$rs = $this->db_query($SQLCmd) ;
		$favor = "";
		if(count($rs) > 0){
			$favor = $rs[0]['favor'] ;
		}
		//echo $SQLCmd;
		return $favor;
	}
	
	//找出使用者我的最爱
	public function favor(){
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
		
		$favor = $this->get_user_favor();
		$favorArr = array();
		if($favor !="")
			$favorArr = explode(",",$favor);
		if(count($favorArr)>0)
		{
			$i = 0;
			foreach($favorArr as $key => $value)
			{
				$SQLCmd = "SELECT  sm.menu_image as shortcut_image_name,
									sm.menu_sequence as shortcut_sequence, 
									{$field}, 
									sm.menu_directory, 
									sm.status, 
									sm.menu_sn 
							FROM sys_menu_group smg, sys_menu sm
							WHERE smg.status = '1'
							AND sm.status = '1'
							AND smg.group_sn = {$this->session->userdata('group_sn')}
							AND smg.menu_sn = sm.menu_sn 
							AND sm.menu_sn = '{$value}' 
							AND sm.parent_menu_sn <> 0 
							ORDER BY sm.menu_sequence";
				$ts = $this->db_query($SQLCmd);
				if(count($ts)>0)
				{
					foreach($ts as $key1 => $value1)
					{
						foreach($value1 as $field1 => $data1)
						{
							$rs[$i][$field1] = $data1;
						}
					}
					$i++;
				}
			}
		}
		else
		{
			$rs = array();
		}
		
		// if($favor !=""){	//有资料
			// $favor_str = " and sm.menu_sn in({$favor}) ";
		// }else{	//无资料
			// $favor_str = " and 1 = 2 ";
		// }
					
		// $SQLCmd  = "SELECT  sm.menu_image as shortcut_image_name,
							// sm.menu_sequence as shortcut_sequence, 
							// {$field}, 
							// sm.menu_directory, 
							// sm.status, 
							// sm.menu_sn 
					// FROM sys_menu_group smg, sys_menu sm
					// WHERE smg.status = '1'
					// AND sm.status = '1'
					// AND smg.group_sn = {$this->session->userdata('group_sn')}
					// AND smg.menu_sn = sm.menu_sn 
					// {$favor_str}
					// ORDER BY sm.menu_sequence" ;
		
		// $rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	//我的最爱, 中间图片显示
	public function favor_pic(){
		$web = $this->config->item('base_url');
		$rs = $this->favor();
		
		//列出我的最爱图片
		echo "<table id='template' border='0' cellspacing='0' cellpadding='0'>";
		$i=0;
		foreach ($rs as $key => $shortcutInfoArr){
			//如果登入者所属商店为自动结帐或Auth only, 则略过, 不显示结帐作业
			if(($this->session->userdata('SETTLE_TYPE') === "0" || $this->session->userdata('SETTLE_TYPE') === "2") && 
				$shortcutInfoArr["menu_sn"] == "26"){
				continue;
			}
			//如果登入者所属商店为Auth only, 则略过, 不显示退货作业
			if($this->session->userdata('SETTLE_TYPE') === "2" && $shortcutInfoArr["menu_sn"] == "25"){
				continue;
			}
			
			if($key%6==0)
				echo "<tr>";
			echo "<td draggable='true' ondragstart='dragstartHandler(event)' menu_sn='{$shortcutInfoArr['menu_sn']}'>";
			echo "<div tabindex=-1 style='FLOAT: left'>\n";
			echo "	<div tabindex=-1 class='icon'>\n";
			echo "		<a tabindex=-1 href='{$web}{$shortcutInfoArr['menu_directory']}/' target='_top'>
							<div class='remove_favor' style='display:none' menu_sn='{$shortcutInfoArr['menu_sn']}'>x</div> 
							<img alt='{$shortcutInfoArr['menu_name']}' src='{$web}img/shortcut/{$shortcutInfoArr['shortcut_image_name']}' align='middle' border='0'>
							<span style='font-size:14px; padding-top: 8px;'>{$shortcutInfoArr['menu_name']}</span>
						</a>\n";
			echo "	</div>\n";
			echo "</div>\n";
			echo "</td>";
			if($key%6==5)
				echo "</tr>";
			$i++;
		}
		if($i%6!=0 && $i%6!=5 )
			echo "</tr>";
		echo "</table>";
	}
	
	//显示捷径图示
	public function shortcut_pic(){
		$web = $this->config->item('base_url');
		$rs = $this->getShortcutListShow();
		//列出我的最爱图片
		foreach ($rs as $key => $shortcutInfoArr){
			echo "<div tabindex=-1 style='FLOAT: left'>\n";
			echo "	<div tabindex=-1 class='icon'>\n";
			echo "		<a tabindex=-1 href='{$web}{$shortcutInfoArr['menu_directory']}/' target='_top'>
							<img alt='{$shortcutInfoArr['menu_name']}' src='{$web}img/shortcut/{$shortcutInfoArr['shortcut_image_name']}' align='middle' border='0'>
							<span style='font-size:14px; padding-top: 8px;'>{$shortcutInfoArr['menu_name']}</span>
						</a>\n";
			echo "	</div>\n";
			echo "</div>\n";
		}
	}
	
	//新增我的最爱
	public function addfavor(){
		$arr_menu_sn = $this->input->post("arr_menu_sn") ;
		if($arr_menu_sn != ""){
			$arr_menu_sn = join(',', $arr_menu_sn);
		}
		
		$SQLCmd = "	SELECT count(*) as cnt 
					FROM sys_user_favor 
					where user_sn = {$this->session->userdata('user_sn')}" ;
		$rs = $this->db_query($SQLCmd) ;
		
		$dataArr['favor'] = $arr_menu_sn;
		if($rs[0]['cnt'] == 0){  //无资料, 新增
			$dataArr['user_sn'] = $this->session->userdata('user_sn');
			$this->db_insert( "sys_user_favor", $dataArr);
		}else{ //有资料, 修改
			$where = "user_sn = {$this->session->userdata('user_sn')}";
			$this->db_update( "sys_user_favor", $dataArr ,$where);
		}
	}
	
	/**
	 * 取得捷径下拉选单
	 */
	public function getShortcutSelectList( $shortcut_sn = "" )
	{
		$SQLCmd = "SELECT shortcut_sn, shortcut_name FROM sys_shortcut where status <> 'D' " ;
		if( !empty( $shortcut_sn ) )
			$SQLCmd .= "AND shortcut_sn={$shortcut_sn}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}

	/**
	 * 取得单一 shortcut info
	 */
	public function getShortcutInfo( $shortcut_sn = "" )
	{
		
		$whereArr = array ( "shortcut_sn" => $shortcut_sn ) ;
		$SQLCmd = "SELECT * FROM sys_shortcut where shortcut_sn={$shortcut_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "sys_shortcut", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 新增 sys_shortcut
	 */
	public function insertShortcut()
	{
		$colDataArr = array(
			"menu_sn"				=> $this->input->post("selMenuSN"),
			"shortcut_image_name"	=> $this->input->post("txtChImageName"),
			"shortcut_sequence"		=> $this->input->post("txtChSequence"),
			"status"				=> $this->input->post("selStatus"),
			"create_date"			=> "now()",
			"create_user"			=> $this->session->userdata('user_sn')
		) ;

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		if ( $this->db_insert( "sys_shortcut", $colDataArr ) ) {
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			
		}
	}
	
	/**
	 * 修改 sys_shortcut
	 */
	public function updateShortcut()
	{
		$whereStr = " shortcut_sn={$this->input->post("shortcut_sn") }" ;
		$colDataArr = array(
			"shortcut_image_name"	=> $this->input->post("txtChImageName"),
			"shortcut_sequence"		=> $this->input->post("txtChSequence"),
			"status"				=> $this->input->post("selStatus"),
			"update_date"			=> "now()",
			"update_user"			=> $this->session->userdata('user_sn')
		) ;

		// $this->db_update( "sys_shortcut", $colDataArr, $whereArr ) 
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		if ( $this->db_update( "sys_shortcut", $colDataArr, $whereStr ) ) {
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			
		}
	}
	
	/**
	 * 删除 sys_shortcut
	 */
	public function deleteShortcut()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$delIdStr = join( ',', $ckbSelArr ) ;
		// echo "delIdStr : $delIdStr" ;
		$whereStr = " shortcut_sn in ({$delIdStr}) ";
		
		$colDataArr = array (
			"status"		=> "D",
			"update_date"	=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		if ( $this->db_delete( "sys_shortcut", $colDataArr, $whereStr ) ) {
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}
	}
	
	/**
	 * 上移 sys_shortcut
	 */
	public function sequenceShortcutUp()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$upIdStr = join( ',', $ckbSelArr ) ;

		// 先取得选取捷径的排列顺序编号
		$SQLCmd  = "SELECT shortcut_sequence 
					FROM sys_shortcut 
					WHERE shortcut_sn IN ({$upIdStr}) 
					AND status <> 'D' ";
		$rs = $this->db_query($SQLCmd) ;
		$shortcut_sequence = $rs[0]["shortcut_sequence"];

		// 找寻看有没有比目前排列顺序编号还小的排列顺序编号
		$SQLCmd  = "SELECT shortcut_sn, shortcut_sequence 
					FROM sys_shortcut 
					WHERE shortcut_sequence < {$shortcut_sequence} 
					AND status <> 'D' 
					ORDER BY shortcut_sequence DESC 
					LIMIT 0, 1 ";
		$rs = $this->db_query($SQLCmd) ;
		if($rs)
		{
			$up_shortcut_sn = $rs[0]["shortcut_sn"];
			$up_shortcut_sequence = $rs[0]["shortcut_sequence"];

			// 将上一笔捷径的排列顺序编号改为目前选取捷径的排列顺序编号
			$whereStr = " shortcut_sn = {$up_shortcut_sn}" ; 		
			$colDataArr = array(
				"shortcut_sequence"	=> $shortcut_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn')
			) ;
			$this->db_update( "sys_shortcut", $colDataArr, $whereStr );

			// 将目前选取捷径的排列顺序编号改为上一笔捷径的排列顺序编号
			$whereStr = " shortcut_sn IN ({$upIdStr}) ";
			$colDataArr = array(
				"shortcut_sequence"	=> $up_shortcut_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn')
			) ;
			$this->db_update( "sys_shortcut", $colDataArr, $whereStr );
		}
		$this->redirect("./index") ;
	}
	
	/**
	 * 下移 sys_shortcut
	 */
	public function sequenceShortcutDown()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$downIdStr = join( ',', $ckbSelArr ) ;

		// 先取得选取捷径的排列顺序编号
		$SQLCmd  = "SELECT shortcut_sequence 
					FROM sys_shortcut 
					WHERE shortcut_sn IN ({$downIdStr}) 
					AND status <> 'D' ";
		$rs = $this->db_query($SQLCmd) ;
		$shortcut_sequence = $rs[0]["shortcut_sequence"];

		// 找寻看有没有比目前排列顺序编号还大的排列顺序编号
		$SQLCmd  = "SELECT shortcut_sn, shortcut_sequence 
					FROM sys_shortcut 
					WHERE shortcut_sequence > {$shortcut_sequence} 
					AND status <> 'D' 
					ORDER BY shortcut_sequence  
					LIMIT 0, 1 ";
		$rs = $this->db_query($SQLCmd) ;
		if($rs)
		{
			$down_shortcut_sn = $rs[0]["shortcut_sn"];
			$down_shortcut_sequence = $rs[0]["shortcut_sequence"];

			// 将上一笔捷径的排列顺序编号改为目前选取捷径的排列顺序编号
			$whereStr = " shortcut_sn = {$down_shortcut_sn}" ;
			$colDataArr = array(
				"shortcut_sequence"	=> $shortcut_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn')
			) ;
			$this->db_update( "sys_shortcut", $colDataArr, $whereStr );

			// 将目前选取捷径的排列顺序编号改为上一笔捷径的排列顺序编号
			$whereStr = " shortcut_sn IN ({$downIdStr}) ";
			$colDataArr = array(
				"shortcut_sequence"	=> $down_shortcut_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn')
			) ;
			$this->db_update( "sys_shortcut", $colDataArr, $whereStr );
		}
		$this->redirect("./index") ;
	}
}
/* End of file model_shortcut.php */
/* Location: ./application/models/model_shortcut.php */
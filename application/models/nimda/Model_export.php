<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_export extends MY_Model {
	//简讯
	function connectToAirlink(){
		$config['hostname'] = 'localhost';
		$config['username'] = 'root';
		$config['password'] = '';
		//$config['database'] = 'airlink';
		$config['database'] = 'ub_data';
		$config['dbdriver'] = 'mysql';
		$config['dbprefix'] = '';
		$config['pconnect'] = TRUE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = '';
		$config['char_set'] = 'utf8';
		$config['dbcollat'] = 'utf8_general_ci';
		$config['swap_pre'] = '';
		$config['autoinit'] = TRUE;
		$config['stricton'] = FALSE;
		
		$this->_db['airlink'] = $this->load->database($config, true);  //连线多个资料库
	}
	
	public function AtoZ(){
		$data = array('A', 'B', 'C', 'D', 'E',
					  'F', 'G', 'H', 'I', 'J',
					  'K', 'L', 'M', 'N', 'O',
					  'P', 'Q', 'R', 'S', 'T',
					  'U', 'V', 'W', 'X', 'Y',
					  'Z');
		return $data;
	}
	
	//取得excel栏位
	public function getColumn($num){
		return $this->processColumn($num);
	}
	
	//取得excel栏位, 计算
	public function processColumn($num){
		if(!isset($str)){
			$str = "";
		}
		$AZ_Arr = $this->AtoZ();
		//除数
		$div = 26;  //26个英文字
		//商数
		$quo = floor($num/$div);
		//余数
		$rem = $num%$div;

		if($rem == 0){
			$rem2 = 26;
			$str = $AZ_Arr[$rem2-1].$str;
			$quo = $quo -1;
		}else{
			$str = $AZ_Arr[$rem-1].$str;
		}
		
		if($quo > 0){
			$str = $this->processColumn($quo).$str;
		}
		return $str;
	}
	
	public function loadExcel(){
		date_default_timezone_set('Asia/Taipei');
		set_time_limit(0); //设定 PHP 执行时间无限制
		ini_set('memory_limit','128m');//设定记忆体
		include 'phpExcel.1.7.6/Classes/PHPExcel.php';
		include 'phpExcel.1.7.6/Classes/PHPExcel/Writer/Excel2007.php';
		require_once 'phpExcel.1.7.6/Classes/PHPExcel/IOFactory.php';
	}
	
	public function saveExcel(){
		ob_start();
		Header ( 'Cache-Control: no-cache, must-revalidate' );
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=result.xlsx");
		header("Content-Length: ".filesize('result.xlsx'));
		@readfile('result.xlsx');
		ob_flush();
	}
	
	//四个方向都画线, array(上, 右, 下, 左),值=0不画线, 1细线, 2粗线, 3双线
	public function excel_border_4($objPHPExcel, $column, $line = array(0, 0, 0, 0)){
		foreach($line as $key => $value){
			switch($key){
				case 0: //上
					$pos = $objPHPExcel->getActiveSheet()->getStyle($column)->getBorders()->getTop();
					break;
				case 1: //右
					$pos = $objPHPExcel->getActiveSheet()->getStyle($column)->getBorders()->getRight();
					break;
				case 2: //下
					$pos = $objPHPExcel->getActiveSheet()->getStyle($column)->getBorders()->getBottom();
					break;
				case 3: //左
					$pos = $objPHPExcel->getActiveSheet()->getStyle($column)->getBorders()->getLeft();
					break;
				default:
					break;
			}
			$this->draw_border($pos, $value);
		}
	}
	
	//画线
	public function draw_border($pos, $value){
		switch($value){
			case 1: //细线
				$pos->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
				break;
			case 2: //粗线
				$pos->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
				break;
			case 3: //双线
				$pos->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
				break;
			default:
				break;
		}
	}
	
	//设定背景颜色
	public function excel_fill_color($objPHPExcel, $column, $color){
		$objPHPExcel->getActiveSheet()->getStyle($column)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle($column)->getFill()->getStartColor()->setARGB($color);
	}
	
	//设定文字置中
	public function excel_font_align_center($objPHPExcel, $column){
		$objPHPExcel->getActiveSheet()->getStyle($column)->getAlignment()->
			setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);  
		$objPHPExcel->getActiveSheet()->getStyle($column)->getAlignment()->
			setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
	}
	
	public function proccess_table_Excel($objPHPExcel){
		$this->connectToAirlink();
		$rs = $this->db_query('show table status', 'airlink');
		
		$iSheet = 0;
		foreach($rs as $key => $data){
			$tableName = $data['Name'];  //资料表名称
		
			//EXCEL初始化
			$objPHPExcel->createSheet($iSheet);
			$objPHPExcel->setActiveSheetIndex($iSheet);
			$objPHPExcel->getActiveSheet()->setTitle($tableName);
			$iSheet++;
			
			//产生资料
			$iColumn = 2;  //A~Z
			$iLine = 2;  //第几行
			
			//title: 档案格式说明
			$column = $this->getColumn($iColumn);
			//合并储存格
			$iColumn += 3;
			$margin_column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.$iLine);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '档案格式说明');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(2, 2, 1, 2));
			
			//title: 类型
			$iLine++;  //换行
			$iColumn = 2;  //A~Z
			$column = $this->getColumn($iColumn);
			$iColumn += 1;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.$iLine);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '类型');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(1, 1, 1, 2));
			//title_value: 类型
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, 'TABLE');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, 1, 1));
			//title: 档案名称
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '档案名称');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, 1, 1));
			//title_value: 档案名称
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 3;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.$iLine);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $tableName);
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(2, 1, 1, 1));
			//title: 档案说明
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 1;
			$margin_column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.$iLine);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '档案说明');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(2, 1, 1, 1));
			$iColumn += 1;  //空白
			$column = $this->getColumn($iColumn);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(2, 2, 1, 1));
			
			//title: index
			$iLine++;  //换行
			$iColumn = 2;  //A~Z
			$column = $this->getColumn($iColumn);
			$iColumn += 1;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine+1));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, 'Index');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(1, 1, 1, 2));
			$this->excel_border_4($objPHPExcel, $column.($iLine+1), array(0, 0, 0, 2));
			$this->excel_font_align_center($objPHPExcel, $column.$iLine);
			
			//title: PRIMARY : 01
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 7;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, 'PRIMARY : 01');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(1, 1, 1, 1));
			//title: PRIMARY
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, 'PRIMARY');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 2, 1, 1));
			
			//title: 没资料, 合并一些储存格
			$iLine++;  //换行
			$iColumn = 4;  //A~Z
			$column = $this->getColumn($iColumn);
			$iColumn += 7;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(0, 1, 0, 1));
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 2, 3, 1));
			
			//title: 序号, 栏位名称, 栏位格式, 栏位说明, 属性, 空值, 预设值, 备注
			$color = 'CCFFCC'; //颜色
			$iLine++;  //换行
			$iColumn = 2;  //A~Z
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '序号');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 1, 1, 2));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 2;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '栏位名称');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '栏位格式');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 1;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '栏位说明');
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '属性');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '空值');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '预设值');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 1, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '备注');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(3, 2, 1, 1));
			$this->excel_fill_color($objPHPExcel, $column.$iLine, $color);
			
			//产生table下的field资料
			$objPHPExcel = $this->proccess_field_Excel($objPHPExcel, $tableName, $iLine);
			//break;  //只跑一笔
		}
		return $objPHPExcel;
	}
	
	public function proccess_field_Excel($objPHPExcel, $tableName, $iLine){
		$this->connectToAirlink();
		$rs = $this->db_query("show full FIELDS from `{$tableName}`", 'airlink');  //` 为避免使用到保留字而造成无法查询
		$sno = 0;  //序号
		$count = count($rs);
		foreach($rs as $key => $data){
			//栏位资料
			$field = $data['Field'];
			$type = $data['Type'];
			$null = $data['Null'];
			$default = $data['Default'];
			$extra = $data['Extra'];
			$comment = $data['Comment'];
	
			if($null == 'YES'){  //允许NULL
				$ynspace = '是';
				$default = 'NULL '.$default;
			}else{
				$ynspace = '否';
			}
			
			$iColumn = 2;  //A~Z
			$iLine++;
			$sno++;
			$sno = str_pad($sno, 2, "0", STR_PAD_LEFT);  //补0
			
			if($key != ($count-1)){  //最后一笔, 下面画粗线
				$bottom = 1;
			}else{
				$bottom = 2;
			}
			
			//title: 序号
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $sno);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, $bottom, 2));
			//title: 栏位名称
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 2;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $field);
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(1, 1, $bottom, 1));
			//title: 栏位格式
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $type);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, $bottom, 1));
			//title: 栏位说明
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$iColumn += 1;
			$margin_column = $this->getColumn($iColumn);
			//合并储存格
			$objPHPExcel->getActiveSheet()->mergeCells($column.$iLine.':'.$margin_column.($iLine));
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $comment);
			$this->excel_border_4($objPHPExcel, $column.$iLine.':'.$margin_column.$iLine, array(1, 1, $bottom, 1));
			//title: 属性
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, '');
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, $bottom, 1));
			//title: 空值
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $ynspace);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, $bottom, 1));
			//title: 预设值
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $default);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 1, $bottom, 1));
			//title: 备注
			$iColumn += 1;
			$column = $this->getColumn($iColumn);
			$objPHPExcel->getActiveSheet()->setCellValue($column.$iLine, $extra);
			$this->excel_border_4($objPHPExcel, $column.$iLine, array(1, 2, $bottom, 1));
		}
		return $objPHPExcel;
	}
	
	public function export()
	{

		// print_r($rs);
		
		// exit();
		
		//读取excel元件
		$this->loadExcel();
		
		//初始化
		$objPHPExcel = new PHPExcel(); 
		
		//设定宽度
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(2.36);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(4.82);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(0.89);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11.45);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15.45);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(16);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(11);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8.27);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(4.82);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6.82);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(44.82);
		
		//产生table资料
		$objPHPExcel = $this->proccess_table_Excel($objPHPExcel);
		
		//垂直置中
		//$this->excel_font_align_center($objPHPExcel, 'A'.($i-1));
		
		//结束
		//产出EXCEL
		//输出
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
		$objWriter->save('result.xlsx');
		
		//下载excel 
		$this->saveExcel();
		exit();
		
	}
	


}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_config extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getConfigAllCnt()
	{
		$SQLCmd = "select count(*) cnt from sys_config where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getConfigList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by config_sn desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$SQLCmd = "SELECT config_code, config_sn, config_desc, config_set, input_size, after_desc
  						FROM sys_config where status <> 'D' {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得指定　config_code　的 config info
	 */
	public function getConfigInfoAssign( $config_code = "" )
	{
		if ( !empty( $config_code) )	$whereStr = "where config_code='{$config_code}'" ;
		else $whereStr = "" ;

		$SQLCmd = "SELECT config_code, config_set FROM sys_config {$whereStr}" ;
		// echo "SQLCmd=".$SQLCmd."<BR>";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得单一 config info
	 */
	public function getConfigInfo( $config_sn = "" )
	{
		
		$whereArr = array ( "config_sn" => $config_sn ) ;
		$SQLCmd = "SELECT * FROM sys_config where config_sn={$config_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "sys_config", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 修改 sys_config
	 */
	public function updateConfig()
	{
		//检查栏位资料
		$this->checkFieldData('edit');
		
		$whereStr = " config_sn={$this->input->post("config_sn") }" ;
		$colDataArr = array(
			"config_set"	=> $this->input->post("config_set"),
			"update_date"	=> "now()",
			"update_user"	=> $this->session->userdata('user_sn')
		) ;
		
		// $this->db_update( "sys_config", $colDataArr, $whereArr ) 
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "sys_config", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[config_sn: '.$this->input->post("config_sn").']';
			$this->model_background->log_operating_insert('2', $target_key);
		
			$configRows = $this->getConfigInfoAssign();
			foreach ($configRows as $key => $configArr) {
				$this->session->set_userdata($configArr["config_code"], $configArr["config_set"]);
			}
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[config_sn: '.$this->input->post("config_sn").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["config_sn, config_sn"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_menu extends MY_Model {

	//查询栏位
	function getSearchcol(){
		$colArr = array (
			"sm.menu_sn",
			"sm.parent_menu_sn",
			//"sm.menu_name",
			"sm.menu_directory",
			"sm.menu_image",
			"sm.menu_sequence", 
			"sm.access_control"
		);
		return $colArr;
	}
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getMenuAllCnt($optionStatus = false, $login_side_display = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'sm', $setAlias);
		else $whereStr = "" ;
		
		$status = " sm.status = '1' ";	//一般方式
		if($optionStatus){	//全开
			$where = " sm.status <> 'D' {$whereStr}";
		}
		else
		{
			$where = "{$status} {$whereStr} AND smg.group_sn = {$this->session->userdata('group_sn')} ";
		}
		
		//如果为空就会找使用者登入的login_side为基准
		if($login_side_display  == ""){
			$login_side_display = $this->session->userdata('login_side');
		}
		
		//当等于2, 略过前后台显示分类
		if($login_side_display == '2'){
			
		}else{	//分类显示
			$where .= "and (sm.login_side_display = '2' or sm.login_side_display = {$login_side_display})" ;
		}
		//2017-08-23 增加 and sug.user_sn = {$this->session->userdata('user_sn')}
		$SQLCmd  = "SELECT DISTINCT sm.menu_sn, sm.menu_directory, 
					sm.menu_image, sm.parent_menu_sn, sm.menu_sequence, sm.status, 
					sm.login_side_display 
					FROM sys_menu sm
					LEFT JOIN sys_menu_group smg 
						ON smg.menu_sn = sm.menu_sn
					LEFT JOIN sys_user_group sug 
						ON sug.group_sn = smg.group_sn 
						AND sug.user_sn = {$this->session->userdata('user_sn')}
					WHERE {$where} and sug.user_sn = {$this->session->userdata('user_sn')}";
		$rs = $this->db_query($SQLCmd) ;

		// echo $SQLCmd;
		// echo '<br />';
		return count($rs);
	}

	/**
	 * 取得选单清单
	 */
	public function getMenuListView( $offset = "", $startRow = "", $optionSearch = false)
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array();		
		if ( !empty( $search) ) $whereStr = setSearchData($search, 'sm', $setAlias);
		else $whereStr = "" ;
	
		$limit = "";
		if ( $offset != "" )
		{
			if ( empty( $startRow ) )
				$startRow = 0 ;
			$limit = "LIMIT {$startRow} , {$offset} " ;
		}

		if (	$this->session->userdata('default_language') == "zh_tw" || 
				( 	$this->session->userdata('default_language') == "" && 
					$this->session->userdata('display_language') == "zh_tw"	)
			)
		{
			$menu_field_name = "menu_name";

		}else{
			$menu_field_name = "menu_name_english";
		}
		
		$SQLCmd  = "SELECT DISTINCT sm.menu_sn, sm.{$menu_field_name} as menu_name, sm.menu_directory, sm.menu_image, sm.parent_menu_sn, 
					sm.second_menu_sn, sm.menu_sequence, sm.status, 
					sm.login_side_display, sm.icon_class
					FROM sys_menu sm
					LEFT JOIN sys_menu_group smg ON smg.menu_sn = sm.menu_sn
					left join sys_user_group sug
						on sug.group_sn = smg.group_sn 
						and sug.user_sn = {$this->session->userdata('user_sn')} 
					WHERE sm.status <> 'D' {$whereStr} 
					and (sm.login_side_display = '2' or sm.login_side_display = {$this->session->userdata('login_side')}) 
					ORDER BY sm.menu_sequence
					{$limit}" ;

		$rs = $this->db_query($SQLCmd) ;
		// echo $whereStr;
		// echo '<br />';

		return $rs ;
	}

	/**
	 * 取得群组可使用之选单清单
	 */
	 //$optionStatus 用于选单管理使用, 选单管理必须全开, fasle为关闭全开, 依照一般方式, true 为全开
	 //$login_side_display 对应前后台选单显示, 预设为空, 如果为空就会找使用者登入的login_side为基准
	public function getMenuList( $offset = "", $startRow = "", $optionSearch = false, $optionStatus = false, $login_side_display = "", $use_orderby = false)
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');

		//搜寻对应
		//ex "field_name"=> "Alias"
		if($optionSearch){
			$setAlias = array();		
			if ( !empty( $search) ) $whereStr = setSearchData($search, 'sm', $setAlias);
			else $whereStr = "" ;
		}else{
			$whereStr = "";
		}
		
		$limit = "";
		if ( $offset != "" )
		{
			if ( empty( $startRow ) )
				$startRow = 0 ;
			$limit = "LIMIT {$startRow} , {$offset} " ;
		}

		if (	$this->session->userdata('default_language') == "zh_tw" || 
				( 	$this->session->userdata('default_language') == "" && 
					$this->session->userdata('display_language') == "zh_tw"	)
			)
		{
			$menu = 'sm.menu_name as menu_name';
		}
		else
		{
			$menu = 'sm.menu_name_english as menu_name';
		}

		$status = " sm.status = '1' ";	//一般方式
		if($optionStatus){	//全开
			$where = " sm.status <> 'D' {$whereStr} ";
		}
		else
		{
			$where = "{$status} {$whereStr} AND smg.group_sn = {$this->session->userdata('group_sn')} ";
		}
		
		//如果为空就会找使用者登入的login_side为基准
		if($login_side_display  == ""){
			$login_side_display = $this->session->userdata('login_side');
		}
		
		//当等于2, 略过前后台显示分类
		if($login_side_display == '2'){
			
		}else{	//分类显示
			$where .= "and (sm.login_side_display = '2' or sm.login_side_display = {$login_side_display})" ;
		}
		
		//排序动作
		if($use_orderby){
			$sql_orderby = "";
			$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
			$orderby = $this->session->userdata("{$fn}_".'orderby');
			if($field != "" && $orderby != ""){
				$sql_orderby = " order by ".$field.' '.$orderby;
			}else{
				$sql_orderby = " ORDER BY sm.menu_sequence ";
			}
		}else{
			$sql_orderby = " ORDER BY sm.menu_sequence ";
		}
		
		//2017-08-23 增加 and sug.user_sn = {$this->session->userdata('user_sn')}
		$SQLCmd  = "SELECT DISTINCT sm.menu_sn,{$menu}, sm.menu_directory, sm.menu_image, sm.parent_menu_sn, 
					sm.second_menu_sn, sm.menu_sequence, sm.status,  sm.menu_content,  smg.main_flag, 
					sm.login_side_display, sm.icon_class 
					FROM sys_menu sm
					LEFT JOIN sys_menu_group smg 
						ON smg.menu_sn = sm.menu_sn
					left join sys_user_group sug 
						on sug.group_sn = smg.group_sn 
						and sug.user_sn = {$this->session->userdata('user_sn')}
					WHERE {$where} and sug.user_sn = {$this->session->userdata('user_sn')} {$sql_orderby} {$limit}" ;
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd.'<br />';
		// echo $whereStr;
		// echo '<br />';
		// echo $SQLCmd;
		// echo '<br />';
		//记录查询log
		if ( $login_side_display == '2'){
			$this->model_background->log_insert_search($search, $SQLCmd);
		}
		
		return $rs ;
	}
	
	/**
	 * 取得捷径使用的选单清单
	 */
	public function getShoutcutMenuList( $menu_sn = "" )
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
	
		if($menu_sn != "")
			$where = "sm.menu_sn = {$menu_sn}";
		else
			$where = "sm.menu_sn NOT IN (SELECT menu_sn FROM sys_shortcut)";

		$SQLCmd  = "SELECT DISTINCT sm.menu_sn, sm.login_side_display, {$field} 
					FROM sys_menu sm, sys_menu_group smg, sys_user_group sug
					WHERE {$where}
					AND sm.parent_menu_sn != 0
					AND sug.group_sn = smg.group_sn
					AND smg.menu_sn = sm.menu_sn
					AND sug.user_sn = {$this->session->userdata('user_sn')}
					AND sm.status = '1'
					ORDER BY sm.menu_sn, sm.menu_sequence" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}

	/**
	 * 取得可供选择的第一层选单清单
	 */
	public function getParentMenuList( $menu_sn = "" )
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
		
		$where = "";
		if($menu_sn != "")
			$where = "AND sm.menu_sn != {$menu_sn}";

		$SQLCmd  = "SELECT DISTINCT sm.menu_sn, sm.login_side_display, {$field}
					FROM sys_menu sm, sys_menu_group smg, sys_user_group sug
					WHERE sug.group_sn = smg.group_sn
						AND smg.menu_sn = sm.menu_sn
						AND sug.user_sn = {$this->session->userdata('user_sn')}
						AND sm.status = '1' 
						AND sm.parent_menu_sn = 0 
						{$where} 
					ORDER BY sm.menu_sn, sm.menu_sequence" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}

	/**
	 * 取得可供选择的第二层选单清单
	 */
	public function getSecondMenuList( $menu_sn = "" )
	{
		//取得ci预设语言
		$lang = $this->config->item('language');
		//取得使用者语言
		if($this->session->userdata('default_language') != ""){
			$lang = $this->session->userdata('default_language');
		}else if ($this->session->userdata('display_language') != ""){
			$lang = $this->session->userdata('display_language');
		}
		
		$lang = strtolower($lang);
		$field = '';
		//依照语言给予栏位
		switch($lang){
			case 'zh_tw':
				$field = 'sm.menu_name as menu_name';
				break;
			case 'english':
				$field = 'sm.menu_name_english as menu_name';
				break;
			default:
				$field = 'sm.menu_name as menu_name';
				break;
		}
		
		$where = "";
		if($menu_sn != "")
			$where = "AND sm.menu_sn != {$menu_sn}";

		$SQLCmd  = "SELECT DISTINCT sm.menu_sn, sm.login_side_display, {$field}
					FROM sys_menu sm, sys_menu_group smg, sys_user_group sug 
					WHERE sug.group_sn = smg.group_sn
					AND smg.menu_sn = sm.menu_sn
					AND sug.user_sn = {$this->session->userdata('user_sn')}
					AND sm.status = '1' 
					AND sm.parent_menu_sn <> 0 
					AND (sm.second_menu_sn = 0 or sm.second_menu_sn is NULL)
					{$where}
					ORDER BY sm.menu_sn, sm.menu_sequence" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}

	/**
	 * 取得可以显示的选单清单
	 */
	public function getMenuListShow()
	{
		$SQLCmd  = "SELECT menu_sn, menu_image_name, menu_sequence, sm.menu_name, sm.menu_directory, ss.status, ss.login_side_display 
					FROM sys_menu ss
					LEFT JOIN sys_menu sm ON sm.menu_sn = ss.menu_sn
					WHERE ss.status = '1'
					ORDER BY menu_sequence " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得选单下拉选单
	 */
	public function getMenuSelectList( $menu_sn = "" )
	{
		$SQLCmd = "SELECT menu_sn, menu_name, login_side_display FROM sys_menu where status <> 'D' " ;
		if( !empty( $menu_sn ) )
			$SQLCmd .= "AND menu_sn={$menu_sn}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}

	/**
	 * 取得单一 menu info
	 */
	public function getMenuInfo( $menu_sn = "" )
	{
		
		$whereArr = array ( "menu_sn" => $menu_sn ) ;
		$SQLCmd = "SELECT * FROM sys_menu where menu_sn={$menu_sn}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "sys_menu", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 新增 sys_menu
	 */
	public function insertMenu()
	{
		//检查栏位资料
		$this->checkFieldData('add');
		
		date_default_timezone_set("Asia/Taipei");
		$Nowdate = date("Y-m-d H:i:s");
		
		//权限加总
		$access_control = $this->input->post("access_control");
		$sum_ac = 0;
		if($access_control != ""){
			foreach($access_control as $value){
				$sum_ac +=  $value;
			}
		}
		$colDataArr = array(
			"menu_name"				=> $this->input->post("txtChMenuName"),
			"menu_name_english"		=> $this->input->post("txtChMenuEnName"),
			"parent_menu_sn"		=> $this->input->post("selParentMenuSN"),
			"second_menu_sn"		=> $this->input->post("selSecondMenuSN"),
			"login_side_display"	=> $this->input->post("sellogin_side_display"),
			"menu_directory"		=> $this->input->post("txtChImageDirectory"),
			"menu_sequence"			=> $this->input->post("txtChSequence"),
			"menu_content"			=> $this->input->post("txtmenu_content"),
			"icon_class"			=> $this->input->post("icon_class"),
			"access_control"		=> $sum_ac,
			"status"				=> $this->input->post("selStatus"),
			"create_date"			=> "now()",
			"create_user"			=> $this->session->userdata('user_sn'),
			"create_ip"			=> $this->input->ip_address()
		) ;

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "sys_menu", $colDataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $colDataArr);
			$this->model_background->log_operating_insert('1', $target_key);
		
			//上传选单图档
			if(isset($_FILES['menu_image']['tmp_name']) && $_FILES['menu_image']['tmp_name']!='')
			{
				$SQLCmd = "SELECT menu_sn FROM sys_menu WHERE create_date = '{$Nowdate}'";
				$rs = $this->db_query($SQLCmd);
				//判断图片大小是否在限制大小内
				$size = getimagesize($_FILES['menu_image']['tmp_name']);
				$width = $size[0];
				$height = $size[1];
				if($width>100 || $height>100){
					echo "<script>
							alert('图片长度或宽度超出限制，请上传100x100以内之图片');
							window.history.back();
							</script>";
					exit;
				}
				$whereStr = "menu_sn = {$rs[0]['menu_sn']}";
				// $get_name = $_FILES['menu_image']['name'];
				// $DataArr['menu_image'] = $get_name;
				$DataArr['menu_image'] = $rs[0]['menu_sn'].".png";
				$filepath = "img/shortcut/".$DataArr['menu_image'];
				if($_FILES["menu_image"]["tmp_name"]!='')
				{
						move_uploaded_file( $_FILES["menu_image"]["tmp_name"] , $filepath ) ;
				}
				$this->db_update('sys_menu',$DataArr,$whereStr);
			}
			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $colDataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
	
	/**
	 * 修改 sys_menu
	 */
	public function updateMenu()
	{
		//检查栏位资料
		$this->checkFieldData('edit');
		
		//修改前的权限资料
		//$org_access_control = $this->input->post("org_access_control");
		
		//权限加总
		$access_control = $this->input->post("access_control");
		$sum_ac = 0;
		if(!empty($access_control)){
			foreach($access_control as $value){
				$sum_ac +=  $value;
			}
		}
		
		// $xor = $sum_ac ^ $org_access_control;
		// echo '修改前:'.$org_access_control;
		// echo '<br />';
		// echo '修改后:'.$sum_ac;
		// echo '<br />';
		// echo 'XOR:'.$xor;
		// exit();
		
		$menu_sn = $this->input->post("menu_sn");
		$whereStr = " menu_sn= {$menu_sn} " ;
		$colDataArr = array(
			"menu_name"			=> $this->input->post("txtChMenuName"),
			"menu_name_english"	=> $this->input->post("txtChMenuEnName"),
			"parent_menu_sn"	=> $this->input->post("selParentMenuSN"),
			"second_menu_sn"	=> $this->input->post("selSecondMenuSN"),
			"login_side_display"	=> $this->input->post("sellogin_side_display"),
			"menu_directory"	=> $this->input->post("txtChImageDirectory"),
			"menu_sequence"		=> $this->input->post("txtChSequence"),
			"menu_content"			=> $this->input->post("txtmenu_content"),
			"icon_class"			=> $this->input->post("icon_class"),
			"access_control"	=> $sum_ac,
			"status"			=> $this->input->post("selStatus"),
			"update_date"		=> "now()",
			"update_user"		=> $this->session->userdata('user_sn'),
			"update_ip"			=> $this->input->ip_address()
		) ;

		//上传选单图档
		if(isset($_FILES['menu_image']['tmp_name']) && $_FILES['menu_image']['tmp_name']!='')
		{
			//判断图片大小是否在限制大小内
			$size = getimagesize($_FILES['menu_image']['tmp_name']);
			$width = $size[0];
			$height = $size[1];
			if($width>100 || $height>100){
				echo "<script>
						alert('图片长度或宽度超出限制，请上传100x100以内之图片');
						window.history.back();
						</script>";
				exit;
			}
			// $get_name = $_FILES['menu_image']['name'];
			// $colDataArr['menu_image'] = $get_name;
			$colDataArr['menu_image'] = $menu_sn.".png";
			$filepath = "img/shortcut/".$colDataArr['menu_image'];
			if($_FILES["menu_image"]["tmp_name"]!='')
			{
				move_uploaded_file( $_FILES["menu_image"]["tmp_name"] , $filepath ) ;
			}
		}
		
		// $this->db_update( "sys_menu", $colDataArr, $whereArr ) 
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "sys_menu", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[menu_sn: '.$menu_sn.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//更新群组权限
			//$this->updateGroupMenu($menu_sn, $sum_ac);
			$this->redirect_alert("./index", $this->lang->line('edit_successfully')) ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[menu_sn: '.$menu_sn.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//更新群组选单
	public function updateGroupMenu($menu_sn, $access_control){
		$SQLCmd = "select 
				   smg.group_sn, 
		           smg.access_control
				   from sys_menu_group smg 
				   where smg.menu_sn = {$menu_sn}" ;
		$rs = $this->db_query($SQLCmd);
		
		foreach($rs as $key => $value){
			$group_sn = $value['group_sn'];
			//$access = $value['access_control'] & $access_control;
			$access = $access_control;
			$colDataArr = array(
				"access_control"	=> $access, 				
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn'),
				"update_ip"			=> $this->input->ip_address()
			) ;
			$whereStr = " menu_sn= {$menu_sn} and group_sn = {$group_sn}" ;
			$this->db_update( "sys_menu_group", $colDataArr, $whereStr );
		}
	}
	
	/**
	 * 删除 sys_menu
	 */
	public function deleteMenu()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$delIdStr = join( ',', $ckbSelArr ) ;
		// echo "delIdStr : $delIdStr" ;
		$whereStr = " menu_sn in ({$delIdStr}) ";
		
		$colDataArr = array (
			"status"		=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "sys_menu", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[menu_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", $this->lang->line('delete_successfully')) ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[menu_sn: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
	
	//删除选单图档
	public function deletemenuimg()
	{
		$menu_sn = $this->input->post('menu_sn');
		$menu_image = $this->input->post('menu_image');
		$whereStr = "menu_sn = {$menu_sn}";
		$SQLCmd = "SELECT {$menu_image} FROM sys_menu WHERE {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		$menuimgpath = $web."img/shortcut/".$rs[0]['menu_image'];
		if(file_exists($menuimgpath))
			unlink($menuimgpath);//删除掉server的logo档案
		$dataArr[$menu_image] = '';
		$dataArr['update_date'] = 'now()';
		$dataArr['update_user'] = $this->session->userdata('user_sn');
		$this->db_update('sys_menu',$dataArr,$whereStr);
		
		//纪录删除资料
			//纪录修改前的资料
		$before_desc = $this->session->userdata("before_desc");
		$this->session->set_userdata("desc", $colDataArr);
		$target_key = 'skip[menu_sn: '.$menu_sn.']';
		$target_key .= "[{$menu_image}: null]";
		$this->model_background->log_operating_insert('3', $target_key);
		$this->session->set_userdata("before_desc", $before_desc);
		
		echo 'D';
	}
	
	/**
	 * 上移 sys_menu
	 */
	public function sequenceMenuUp()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$upIdStr = join( ',', $ckbSelArr ) ;

		// 先取得选取选单的排列顺序编号
		$SQLCmd  = "SELECT menu_sequence 
					FROM sys_menu 
					WHERE menu_sn IN ({$upIdStr}) 
					AND status <> 'D' ";
		$rs = $this->db_query($SQLCmd) ;
		$menu_sequence = $rs[0]["menu_sequence"];

		// 找寻看有没有比目前排列顺序编号还小的排列顺序编号
		$SQLCmd  = "SELECT menu_sn, menu_sequence 
					FROM sys_menu 
					WHERE menu_sequence < {$menu_sequence} 
					AND status <> 'D' 
					ORDER BY menu_sequence DESC 
					LIMIT 0, 1 ";
		$rs = $this->db_query($SQLCmd) ;
		if($rs)
		{
			$up_menu_sn = $rs[0]["menu_sn"];
			$up_menu_sequence = $rs[0]["menu_sequence"];

			// 将上一笔选单的排列顺序编号改为目前选取选单的排列顺序编号
			$whereStr = " menu_sn = {$up_menu_sn}" ;
			$colDataArr = array(
				"menu_sequence"	=> $menu_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn'),
				"update_ip"			=> $this->input->ip_address()
			) ;
			$this->db_update( "sys_menu", $colDataArr, $whereStr );

			// 将目前选取选单的排列顺序编号改为上一笔选单的排列顺序编号
			$whereStr = " menu_sn IN ({$upIdStr}) ";
			$colDataArr = array(
				"menu_sequence"	=> $up_menu_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn'),
				"update_ip"			=> $this->input->ip_address()

			) ;
			$this->db_update( "sys_menu", $colDataArr, $whereStr );
		}
		$this->redirect("./") ;
	}
	
	/**
	 * 下移 sys_menu
	 */
	public function sequenceMenuDown()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$downIdStr = join( ',', $ckbSelArr ) ;

		// 先取得选取选单的排列顺序编号
		$SQLCmd  = "SELECT menu_sequence 
					FROM sys_menu 
					WHERE menu_sn IN ({$downIdStr}) 
					AND status <> 'D' ";
		$rs = $this->db_query($SQLCmd) ;
		$menu_sequence = $rs[0]["menu_sequence"];

		// 找寻看有没有比目前排列顺序编号还大的排列顺序编号
		$SQLCmd  = "SELECT menu_sn, menu_sequence 
					FROM sys_menu 
					WHERE menu_sequence > {$menu_sequence} 
					AND status <> 'D' 
					ORDER BY menu_sequence  
					LIMIT 0, 1 ";
		$rs = $this->db_query($SQLCmd) ;
		if($rs)
		{
			$down_menu_sn = $rs[0]["menu_sn"];
			$down_menu_sequence = $rs[0]["menu_sequence"];

			// 将上一笔选单的排列顺序编号改为目前选取选单的排列顺序编号
			$whereStr = " menu_sn = {$down_menu_sn}" ;
			$colDataArr = array(
				"menu_sequence"	=> $menu_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn'),
				"update_ip"			=> $this->input->ip_address()
			) ;
			$this->db_update( "sys_menu", $colDataArr, $whereStr );

			// 将目前选取选单的排列顺序编号改为上一笔选单的排列顺序编号
			$whereStr = " menu_sn IN ({$downIdStr}) ";
			$colDataArr = array(
				"menu_sequence"	=> $down_menu_sequence,
				"update_date"		=> "now()",
				"update_user"		=> $this->session->userdata('user_sn'),
				"update_ip"			=> $this->input->ip_address()
			) ;
			$this->db_update( "sys_menu", $colDataArr, $whereStr );
		}
		$this->redirect("./") ;
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
			"menu_name, txtChMenuName"					=> array("required" => "", "maxSize" => 20), 
			"menu_name_english, txtChMenuEnName"		=> array("required" => "", "maxSize" => 40), 
			"menu_directory, txtChImageDirectory"		=> array("required" => "", "maxSize" => 50), 
			"menu_sequence, txtChSequence"				=> array("required" => "", "minSize" => 1, "maxSize" => 3, "onlyNumberSp" => ""), 
			"login_side_display, sellogin_side_display"	=> array("required" => "", "onlyNumberSp" => "0, 1, 2"), 
			"status, selStatus"							=> array("required" => "", "justsomevalue" => "0, 1") 
		);
		
		if($act == 'add'){

		}else{
			$checkArr["menu_sn, menu_sn"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
	
	//取得选单资料, 用于操作记录对应
	function get_MenuData(){
		if (	$this->session->userdata('default_language') == "zh_tw" || 
				( 	$this->session->userdata('default_language') == "" && 
					$this->session->userdata('display_language') == "zh_tw"	)
			) //中文
		{
			$couumn = "menu_name";
		}else{	//英文
			$couumn = "menu_name_english";
		}
		
		$SQLCmd  = "SELECT {$couumn} as menu_name, menu_directory
					FROM sys_menu 
					WHERE parent_menu_sn <> 0 
					and status <> 'D' " ;
		$rs = $this->db_query($SQLCmd) ;
		$arrData = array();
		foreach($rs as $key => $value){
			//分解
			$arrTmp = explode('/', $value['menu_directory']);
			if(isset($arrTmp[1])){
				$setKey = $arrTmp[1];
				
			}else{
				$setKey = $arrTmp[0];
			}
			$arrData[$setKey] = $value['menu_name'];
		}
		return $arrData;
	}

	//檢查營運商統計頁面
	public function show_op_statistics(){
		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');
		
		$top08 = 0;
		$use_top08 = 0;
		$remnant_top08 = 0;
		$battery = 0;
		$battery_list = NULL;
		$count_ranking = NULL;
		$hour_arr = array();

		if($login_side == 0){
			//看到自己的營運商資料
			if($login_to_num != ''){
				$SQLCmd_1  = "SELECT top.top08,(top.top08-SUM(tm.lave_num)) as use_top08 ,SUM(tm.lave_num) as remnant_top08
				FROM tbl_operator top 
				LEFT JOIN tbl_member tm ON tm.to_num = top.s_num 
				WHERE substr(tm.user_id,1,3) = 'MOD' AND top.s_num = {$login_to_num}" ;
				$rs_1 = $this->db_query($SQLCmd_1) ;

				$top08 = $rs_1[0]['top08'];
				$use_top08 = $rs_1[0]['use_top08'];
				$remnant_top08 = $rs_1[0]['remnant_top08'];

				$SQLCmd_2  = "SELECT lblr.leave_battery_id, tm.name as member_name, tm.user_id, tm.nick_name, tm.lease_type, tm.lease_price, ltop.top01,concat(l_tbss.location, '（',l_tbss.bss_id,'）') as leave_bss_id, tb.latitude, tb.longitude, tb.s_num as battery_s_num
										FROM log_battery_leave_return lblr 
										LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
										LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num
										LEFT JOIN tbl_operator ltop ON tm.to_num = ltop.s_num 
										LEFT JOIN tbl_battery tb ON lblr.leave_battery_id = tb.battery_id
										where tm.to_num = {$login_to_num} and SUBSTR(tm.user_id,1,3) = 'MOD' and (lblr.return_date is null or lblr.return_date = '')
										order by lblr.leave_date, tm.s_num";
				$rs_2 = $this->db_query($SQLCmd_2) ;
				$battery = count($rs_2);
				$battery_list = $rs_2;

				$SQLCmd_3  = "SELECT tm.name as member_name, count(lblr.s_num) as cnt
										FROM log_battery_leave_return lblr 
										LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num
										where tm.to_num = {$login_to_num} and SUBSTR(tm.user_id,1,3) = 'MOD'
										group by lblr.tm_num
										order by count(lblr.s_num) desc";
				$rs_3 = $this->db_query($SQLCmd_3) ;
				$count_ranking = $rs_3;


				$SQLCmd4 = "SELECT DATE_FORMAT(leave_date,'%H') as hour, count(lblr.s_num) as hour_count
					 FROM log_battery_leave_return lblr 
						LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num
						where tm.to_num = {$login_to_num} and SUBSTR(tm.user_id,1,3) = 'MOD'
						 AND leave_date IS NOT NULL
					 GROUP BY DATE_FORMAT(leave_date,'%H')
					 ORDER BY DATE_FORMAT(leave_date,'%H')";
				$rs4 = $this->db_query($SQLCmd4) ;
				if($rs4){
					foreach($rs4 as $key => $value){
						$hour_arr[$value['hour']] = $value['hour_count'];
					}
				}
			}
		}

		$return = array();
		$return[0] = $top08;
		$return[1] = $use_top08;
		$return[2] = $remnant_top08;
		$return[3] = $battery;
		$return[4] = $battery_list;
		$return[5] = $count_ranking;
		$return[6] = $hour_arr;

		return $return;
	}

	public function ChangeCardList(){
		date_default_timezone_set("Asia/Taipei");

		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');
		$date_type = $this->input->post("date_type");
		$date_month = $this->input->post("date_month");
		$date_year = $this->input->post("date_year");
		$search_start_date = $this->input->post("date_start");
		$search_end_date = $this->input->post("date_end");
		if($date_type == 3){
			//月份
			$search_start_date = date("Y-m-d",mktime(0,0,0,substr($date_month,5,2),1,substr($date_month,0,4)));
			$search_end_date = date("Y-m-d",mktime(0,0,0,substr($date_month,5,2)+1,0,substr($date_month,0,4)));
		}else if($date_type == 4){
			//年
			$search_start_date = date("Y-m-d",mktime(0,0,0,1,1,$date_year));
			$search_end_date = date("Y-m-d",mktime(0,0,0,13,0,$date_year));
		}

		$whereStr = "";
		if($search_start_date != '' && $search_end_date != ''){
			$whereStr .= " and (date(lblr.leave_date) >= '{$search_start_date}' and date(lblr.leave_date) <= '{$search_end_date}') ";
			$date_content = $search_start_date."～".$search_end_date;
		}else if($search_start_date != ''){
			$whereStr .= " and date(lblr.leave_date) >= '{$search_start_date}' ";
			$date_content = $search_start_date."～";
		}else if($search_end_date != ''){
			$whereStr .= " and date(lblr.leave_date) <= '{$search_end_date}' ";
			$date_content = "～".$search_end_date;
		}

		$SQLCmd_3  = "SELECT tm.name as member_name, count(lblr.s_num) as cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num
								where tm.to_num = {$login_to_num} and SUBSTR(tm.user_id,1,3) = 'MOD' {$whereStr}
								group by lblr.tm_num
								order by count(lblr.s_num) desc";
		$rs_3 = $this->db_query($SQLCmd_3) ;
		$k_labels_arr = array();
		$k_labels_all_arr = array();
		$k_color = array();
		$val_arr = array();
		$k_labels = "";
		$vals = "";

		if(count($rs_3)>0){
			foreach($rs_3 as $key=>$val){
				$k_labels_arr[] = (($val['member_name'] == '')?$key:$val['member_name']);
				$val_arr[] = $val['cnt'];
			}
			
			//$k_labels = join( "','", $k_labels_arr );
			//$k_labels = "'".$k_labels."'";
			//$vals = join( "','", $val_arr );
			//$vals = "'".$vals."'";
		}
		$return = array();
		$return[0] = $k_labels_arr;
		$return[1] = $val_arr;

		echo json_encode($return);
	}
}
/* End of file model_menu.php */
/* Location: ./application/models/model_menu.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_battery_swap_track extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbst.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tbss.bss_id"		=>	"Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbst', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd = "SELECT count(*) cnt
  						FROM tbl_battery_swap_track tbst 
  						LEFT JOIN tbl_operator top ON tbst.so_num = top.s_num and top.status <> 'D'
  						LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
  						where tbst.status <> 'D' and tbss.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbst.battery_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y",
			"tbss.bss_id"		=>	"Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbst', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = " order by tbst.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT tbst.*,top.top01,tbss.bss_id, tbss.location 
  						FROM tbl_battery_swap_track tbst 
  						LEFT JOIN tbl_operator top ON tbst.so_num = top.s_num and top.status <> 'D'
  						LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
  						where tbst.status <> 'D' and tbss.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		if(count($rs)==0)
		{
			if($search_txt != ''){
				$SQLCmdB = "SELECT count(*) cnt FROM tbl_battery WHERE battery_id like '%{$search_txt}%' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB[0]['cnt']==0)
				{
					$rs[0]['msg'] = "查無此電池序號"; 
				}
				else
				{
					$rs[0]['msg'] = "此電池序號已借出"; 
				}
			}
		}
		return $rs ;
	}
	
	/**
	 * 取得单一 battery_swap_station info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT tbst.*, tbss.bss_id, tbss.location
  						FROM tbl_battery_swap_track tbst 
  						LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
  						where tbst.s_num={$s_num}" ;


		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_track", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_battery_swap_track
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_battery_swap_track", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_battery_swap_track
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_battery_swap_track", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_battery_swap_track", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function getDetailAllCnt( $sb_num = '', $track_no = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');
		$whereStr = "";
 		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd  = "SELECT count(*) cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.leave_sb_num = '{$sb_num}' or lblr.return_sb_num = '{$sb_num}') and (lblr.leave_track_no = '{$track_no}' or lblr.return_track_no = '{$track_no}') ".$whereStr;

		$SQLCmd  = "
				SELECT count(*) cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.leave_sb_num = '{$sb_num}') and (lblr.leave_track_no = '{$track_no}')  ".$whereStr." 
					UNION
				SELECT count(*) cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.return_sb_num = '{$sb_num}') and (lblr.return_track_no = '{$track_no}') ".$whereStr;  
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $sb_num = '', $track_no = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr  = '';

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}
		
		//排序动作
		//$sql_orderby = " order by lblr.s_num desc";
		$sql_orderby = " order by request_date desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		/*$SQLCmd  = "SELECT lblr.*
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as rdo_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as rdso_name, tm.name as member_name, tv.unit_id, tbss.bss_id as return_bss_id
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.leave_sb_num = '{$sb_num}' or lblr.return_sb_num = '{$sb_num}') and (lblr.leave_track_no = '{$track_no}' or lblr.return_track_no = '{$track_no}') ".$whereStr."
								{$sql_orderby} 
  								LIMIT {$startRow} , {$offset} ";*/
	    
	$SQLCmd  = "
				SELECT lblr.s_num, lblr.system_log_date, lblr.leave_date as request_date, '' as usage_time
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name,
					tm.name as member_name, tv.unit_id, l_tbss.bss_id, '借' as l_r_type, lblr.leave_battery_id as battery_id, lblr.leave_status as status 
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								where (lblr.leave_sb_num = '{$sb_num}') and (lblr.leave_track_no = '{$track_no}')  ".$whereStr." 
					UNION
				SELECT lblr.s_num, lblr.system_log_date, lblr.return_request_date as request_date, lblr.usage_time
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as ldo_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as ldso_name
					, tm.name as member_name, tv.unit_id, r_tbss.bss_id, '还' as l_r_type, lblr.return_battery_id as battery_id, lblr.return_status as status 
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station r_tbss ON lblr.return_sb_num = r_tbss.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where (lblr.return_sb_num = '{$sb_num}') and (lblr.return_track_no = '{$track_no}') ".$whereStr."  
								{$sql_orderby} 
  								LIMIT {$startRow} , {$offset}";
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}


}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
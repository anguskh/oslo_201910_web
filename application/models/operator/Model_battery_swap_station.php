<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_battery_swap_station extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.version_no like '%{$search_txt}%'";
		}

		$view_area = $this->session->userdata("view_area");
		if($view_area!="")
		{
			$view_areaArr = explode(",",$view_area);
			$view_areaStr = join( "','", $view_areaArr );
			$view_areaStr = "'".$view_areaStr."'";
			$whereStr .= " AND tbss.province in ({$view_areaStr})";
		}
			
		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbss', $setAlias);
		else $whereStr .= "" ;
		
 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd = "SELECT count(*) cnt 
  						FROM tbl_battery_swap_station tbss 
  						LEFT JOIN tbl_operator top ON tbss.so_num = top.s_num 
  						where tbss.status <> 'D' {$whereStr}";

		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tbss.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.version_no like '%{$search_txt}%'";
		}

		$view_area = $this->session->userdata("view_area");
		if($view_area!="")
		{
			$view_areaArr = explode(",",$view_area);
			$view_areaStr = join( "','", $view_areaArr );
			$view_areaStr = "'".$view_areaStr."'";
			$whereStr .= " AND tbss.province in ({$view_areaStr})";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbss', $setAlias);
		else $whereStr .= "" ;

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT tbss.*,top.top01 
  						FROM tbl_battery_swap_station tbss 
  						LEFT JOIN tbl_operator top ON tbss.so_num = top.s_num 
  						where tbss.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 battery_swap_station info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT *, concat(location,'（',bss_id,'）') as bss_id_name FROM tbl_battery_swap_station where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_station", $whereArr ) ;
		return $rs ;
	}

	public function getbattery_swap_stationALL()
	{
		$SQLCmd = "SELECT s_num, virtual_map, latitude, longitude, concat(location,'（',bss_id,'）') as bss_id_name FROM tbl_battery_swap_station where status <> 'D' and ( latitude != '' and longitude 
		!= '')" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_station", $whereArr ) ;
		return $rs ;
	}

	/**
	 * 新增 tbl_battery_swap_station
	 */
	public function insertData()
	{
		date_default_timezone_set("Asia/Taipei");
		$nowDate = date("Y-m-d H:i:s");
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['bss_id'] = $this->Model_show_list->nf_to_wf($dataArr['bss_id']);
		$dataArr['bss_id'] = trim($dataArr['bss_id']); 
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_ip'] = $this->input->ip_address();

		//处理图片
		$img_array = array('virtual_map');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$file = fopen($_FILES[$img_array[$i]]["tmp_name"], "rb");
				// 读入图片档资料
				$fileContents = fread($file, filesize($_FILES[$img_array[$i]]["tmp_name"])); 
				//关闭图片档
				fclose($file);
				// 图片档案资料编码
				$dataArr[$img_array[$i]]  =$fileContents;
			}
		}
		$new_bss_token = 'HolyCan'.$dataArr['bss_id'];
		$new_bss_token = md5($new_bss_token);
		$dataArr['bss_token'] = $new_bss_token;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_battery_swap_station", $dataArr) ) {

			//取得刚新增之s_num
			$SQLCmd = "SELECT s_num FROM tbl_battery_swap_station WHERE create_date = '{$nowDate}' AND create_user = {$this->session->userdata("user_sn")}";
			$rs = $this->db_query($SQLCmd);
			$sb_num = $rs[0]['s_num'];

			//同时新增換電站轨道资料
			for($i=1;$i<=$dataArr['track_quantity'];$i++)
			{
				$BSTdataArr['so_num'] = $dataArr['so_num'];
				$BSTdataArr['sb_num'] = $sb_num;
				$BSTdataArr['track_no'] = $i;
				$BSTdataArr['column_park'] = 'N';
				$BSTdataArr['column_charge'] = 'S';
				$BSTdataArr['status'] = 'Y';
				$BSTdataArr['create_user'] = $this->session->userdata("user_sn");
				$BSTdataArr['create_date'] = "now()";
				$BSTdataArr['create_ip'] = $this->input->ip_address();
				$this->db_insert( "tbl_battery_swap_track", $BSTdataArr);
			}

			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$dataArrS = array();
			$dataArrS[0]['cabinetNumber'] = $dataArr['bss_id'];
			$dataArrS[0]['cabinetName'] = $dataArr['location'];
			$dataArrS[0]['longitude'] = $dataArr['longitude'];
			$dataArrS[0]['latitude'] = $dataArr['latitude'];
			$dataArrS[0]['port'] = $dataArr['track_quantity'];
			$dataArrS[0]['location'] = $dataArr['note'];
			$StationArr = json_encode($dataArrS);
			// $StationArr[0]['cabinet_number'] = $dataArr['bss_id'];
			// $StationArr[0]['port'] = $dataArr['track_quantity'];
			$postArr['action'] = "actionPostImportCabinet";
			$postArr['data'] = $StationArr;
			$this->Model_background->wechat_callback($postArr,"bulkimport","批量导入机柜");

			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_battery_swap_station
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;

		$SQLCmd = "SELECT MAX(track_no) as max_track_no, so_num  FROM tbl_battery_swap_track WHERE sb_num = {$this->input->post('s_num') } AND status <> 'D'";
		$rs = $this->db_query($SQLCmd);

		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		if($dataArr['track_quantity']<$rs[0]['max_track_no'])
		{
			$this->redirect_alert("./index", "不可减少轨道数") ;
			exit;
		}

		$dataArr['bss_id'] = $this->Model_show_list->nf_to_wf($dataArr['bss_id']);
		$dataArr['bss_id'] = trim($dataArr['bss_id']); 
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();

		//处理图片
		$img_array = array('virtual_map');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$file = fopen($_FILES[$img_array[$i]]["tmp_name"], "rb");
				// 读入图片档资料
				$fileContents = fread($file, filesize($_FILES[$img_array[$i]]["tmp_name"])); 
				//关闭图片档
				fclose($file);
				// 图片档案资料编码
				$dataArr[$img_array[$i]]  =$fileContents;
			}
		}

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_battery_swap_station", $dataArr, $whereStr ) ) {
			//同時更新

			if($rs)
			{
				if($rs[0]['so_num'] != $dataArr['so_num']){
					$whereStrUp = " sb_num={$this->input->post('s_num') }" ;
					//更新軌道的so_num
					$up_arr = array();
					$up_arr['so_num'] = $dataArr['so_num'];
					$up_arr['update_user'] = $this->session->userdata("user_sn");
					$up_arr['update_date'] = "now()";
					$up_arr['update_ip'] = $this->input->ip_address();
					$this->db_update( "tbl_battery_swap_track", $up_arr, $whereStrUp );
				}
				//同时新增換電站轨道资料
				for($i=$rs[0]['max_track_no']+1;$i<=$dataArr['track_quantity'];$i++)
				{
					$BSTdataArr['so_num'] = $dataArr['so_num'];
					$BSTdataArr['sb_num'] = $this->input->post('s_num');
					$BSTdataArr['track_no'] = $i;
					$BSTdataArr['column_park'] = 'N';
					$BSTdataArr['column_charge'] = 'S';
					$BSTdataArr['status'] = 'Y';
					$BSTdataArr['create_user'] = $this->session->userdata("user_sn");
					$BSTdataArr['create_date'] = "now()";
					$BSTdataArr['create_ip'] = $this->input->ip_address();
					$this->db_insert( "tbl_battery_swap_track", $BSTdataArr);
				}
			}

			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_battery_swap_station", $colDataArr, $whereStr ) ) {
			//同時刪除軌道
			$whereStrUp = " sb_num in ({$delIdStr}) ";
			//更新軌道的so_num
			$up_arr = array (
				"status"				=> "D",
				"delete_user"			=> $this->session->userdata('user_sn'), 
				"delete_date"			=> "now()",
				"delete_ip"				=> $this->input->ip_address()
			) ;
			$this->db_delete( "tbl_battery_swap_track", $up_arr, $whereStrUp );

			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	//取得軌道狀態
	public function getbattery_swap_track($s_num){
		$SQLCmd = "SELECT tbst.track_no, tbst.column_park, tbst.column_charge, tbst.battery_id, tbst.bcu_status, tbst.status, tb.status ,tb.battery_status, tb.battery_voltage, tb.battery_cell_status, tb.battery_amps, tb.battery_capacity, tb.battery_temperature, tbst.track_status, tbst.status as tbst_status
						FROM tbl_battery_swap_track tbst
						left join tbl_battery tb ON tbst.battery_id = tb.battery_id and tb.status <> 'D'
						WHERE tbst.sb_num = {$s_num}";
		$rs = $this->db_query($SQLCmd);
		$battery_track = array();

		if(count($rs)>0){
			foreach($rs as $rs_arr){
				$battery_track[$rs_arr['track_no']]['track_no'] = $rs_arr['track_no'];
				$battery_track[$rs_arr['track_no']]['column_park'] = $rs_arr['column_park'];
				$battery_track[$rs_arr['track_no']]['column_charge'] = $rs_arr['column_charge'];
				$battery_track[$rs_arr['track_no']]['battery_id'] = $rs_arr['battery_id'];
				$battery_track[$rs_arr['track_no']]['bcu_status'] = $rs_arr['bcu_status'];
				$battery_track[$rs_arr['track_no']]['status'] = $rs_arr['status'];
				$battery_track[$rs_arr['track_no']]['battery_status'] = $rs_arr['battery_status'];
				$battery_track[$rs_arr['track_no']]['track_status'] = $rs_arr['track_status'];
				$battery_track[$rs_arr['track_no']]['tbst_status'] = $rs_arr['tbst_status'];	//軌道狀態

				$battery_track[$rs_arr['track_no']]['battery_voltage'] = $rs_arr['battery_voltage'];
				$battery_track[$rs_arr['track_no']]['battery_cell_status'] = $rs_arr['battery_cell_status'];
				$battery_track[$rs_arr['track_no']]['battery_amps'] = $rs_arr['battery_amps'];
				$battery_track[$rs_arr['track_no']]['battery_capacity'] = $rs_arr['battery_capacity'];
				$battery_track[$rs_arr['track_no']]['battery_temperature'] = $rs_arr['battery_temperature'];


			}
		}
		return $battery_track;
	}

	//取得電池的資訊
	public function getbatteryinfo($s_num){
		$SQLCmd = "SELECT max(s_num) as s_num
					FROM log_battery_info 
					where sb_num = '{$s_num}'
					group by track_no
					order by s_num desc";
		$rs = $this->db_query($SQLCmd);
		$battery_info = array();
		if(count($rs)>0){
			
			$search_arr = array();
			foreach($rs as $rs_arr){
				$search_arr[] = $rs_arr['s_num'];
			}
			$Str = join( "','", $search_arr );
			$ALLStr = "'".$Str."'";
			
			$whereStr = " s_num in ({$ALLStr}) ";
			$SQLCmd2 = "SELECT * FROM log_battery_info 
						where {$whereStr}";
			$rs2 = $this->db_query($SQLCmd2);
			
			foreach($rs2 as $rs_arr){
				$battery_info[$rs_arr['track_no']]['battery_id'] = $rs_arr['battery_id'];
				$battery_info[$rs_arr['track_no']]['log_date'] = $rs_arr['log_date'];
				$battery_info[$rs_arr['track_no']]['battery_voltage'] = $rs_arr['battery_voltage'];
				$battery_info[$rs_arr['track_no']]['battery_cell_status'] = $rs_arr['battery_cell_status'];
				$battery_info[$rs_arr['track_no']]['battery_amps'] = $rs_arr['battery_amps'];
				$battery_info[$rs_arr['track_no']]['battery_capacity'] = $rs_arr['battery_capacity'];
				$battery_info[$rs_arr['track_no']]['battery_temperature'] = $rs_arr['battery_temperature'];

			}
		}

		return $battery_info;
	}

	//確認电池序号唯一
	public function ajax_pk_bss_id(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得資料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		$fieldnum = $_GET['fieldnum'];
		
		$result = $this->countbss_id($validateValue, $fieldnum);
		$arrayToJs = array();
		//一定要帶回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//無重複
		}else{
			$arrayToJs[1] = false;	//有重複
		}
		echo json_encode($arrayToJs);
	}

	//查出是否有重複
	public function countbss_id($bss_id, $fieldnum){
		$where = "";
		if($bss_id != ''){
			if($fieldnum != ''){
				$where = " and tbss.s_num != '{$fieldnum}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_battery_swap_station tbss 
						where tbss.bss_id = '{$bss_id}' and tbss.status <> 'D' {$where}";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function stationtowechat()
	{
		$SQLCmd = "SELECT bss_id,location,longitude,latitude,track_quantity,note FROM tbl_battery_swap_station WHERE status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			$dataArr = array();
			foreach($rs as $key => $value)
			{
				$dataArr[$key]['cabinetNumber'] = $value['bss_id'];
				$dataArr[$key]['cabinetName'] = $value['location'];
				$dataArr[$key]['longitude'] = $value['longitude'];
				$dataArr[$key]['latitude'] = $value['latitude'];
				$dataArr[$key]['port'] = $value['track_quantity'];
				$dataArr[$key]['location'] = $value['note'];
			}
		}
		$StationArr = json_encode($dataArr);
		// print_r($callArr);
		$postArr['action'] = "actionPostImportCabinet";
		$postArr['data'] = $StationArr;
		print_r($postArr);
		$this->Model_background->wechat_callback($postArr,"bulkimport","批量导入机柜");
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_lease_price extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getLease_priceAllCnt()
	{
		$whereStr = "";
		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "select count(*) cnt from tbl_lease_price where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getLease_priceList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tlp.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		$whereStr = "";
		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and so_num = {$to_flag_num}" ;
		}
	    
		$SQLCmd = "SELECT tlp.*, top.top01 as so_name, tso.tsop01 as sso_name
  						FROM tbl_lease_price tlp 
						LEFT JOIN tbl_operator top ON top.s_num = tlp.so_num and top.status = 'Y'
						LEFT JOIN tbl_sub_operator tso ON tso.s_num = tlp.sso_num and tso.status = 'Y'
						where tlp.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 operator info
	 */
	public function getLease_priceInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_lease_price where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_lease_price", $whereArr ) ;
		return $rs ;
	}
	
		/**
	 * 新增 tbl_lease_price
	 */
	public function insertData()
	{
		//检查栏位资料
		//$this->checkFieldData('add');
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		//C=系统共用 营运商s_num	/子营运商s_num	 清空
		if($dataArr['type'] == 'C'){
			 $dataArr['so_num'] = '';
			 $dataArr['sso_num'] = '';
		}

		$dataArr['create_user'] = $this->session->userdata('user_sn');
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_lease_price", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);

			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_lease_price
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}

		//C=系统共用 营运商s_num	/子营运商s_num	 清空
		if($colDataArr['type'] == 'C'){
			 $colDataArr['so_num'] = '';
			 $colDataArr['sso_num'] = '';
		}

		$whereStr = "s_num = '{$s_num}'";

		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['update_date'] = "now()";
		$colDataArr['update_ip'] = $this->input->ip_address();

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_lease_price", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	/**
	 * 删除 tbl_lease_price
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"						=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_lease_price", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
}
/* End of file model_operator.php */
/* Location: ./application/models/model_operator.php */
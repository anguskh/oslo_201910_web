<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_operator extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getOperatorAllCnt()
	{
		$whereStr = "";
		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and top.s_num = {$to_flag_num}" ;
		}

		$SQLCmd = "select top.s_num, (SELECT count(*) FROM tbl_battery_swap_station tbss WHERE tbss.so_num = top.s_num and tbss.status <> 'D') as tbss_count, (SELECT count(*) FROM tbl_battery tb WHERE tb.do_num = top.s_num and tb.status <> 'D' and tb.DorO_flag = 'O') as tb_count
		from tbl_operator top where top.status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		$tbss_count = 0;
		$tb_count = 0;
		if(count($rs)>0){
			foreach($rs as $arr){
				$tbss_count += $arr['tbss_count'];
				$tb_count += $arr['tb_count'];
			}
		}
		

		$return = array();
		$return[0] = count($rs);
		$return[1] = $tbss_count;
		$return[2] = $tb_count;
		return $return;
	}

	/**
	 * 取得参数清单
	 */
	public function getOperatorList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr = "";

		//排序动作
		$sql_orderby = " order by top.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and top.s_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT *,(SELECT count(*) FROM tbl_battery_swap_station tbss WHERE tbss.so_num = top.s_num and tbss.status <> 'D') as tbss_count, (SELECT count(*) FROM tbl_battery tb WHERE tb.do_num = top.s_num and tb.status <> 'D' and tb.DorO_flag = 'O') as tb_count
  						FROM tbl_operator top
						where top.status <> 'D' 
						{$whereStr}
						{$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得指定　operator_code　的 operator info
	 */
	public function getOperatorInfoAssign( $operator_code = "" )
	{
		if ( !empty( $operator_code) )	$whereStr = "where operator_code='{$operator_code}'" ;
		else $whereStr = "" ;

		$SQLCmd = "SELECT operator_code, operator_set FROM tbl_operator {$whereStr}" ;
		// echo "SQLCmd=".$SQLCmd."<BR>";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得单一 operator info
	 */
	public function getOperatorInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_operator where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_operator", $whereArr ) ;
		return $rs ;
	}
	
	//sub_operator要取得
	public function getOperatorALL(){
		$SQLCmd = "SELECT s_num, tde01
  						FROM tbl_operator where status <> 'D'
						order by s_num";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}
	
		/**
	 * 新增 tbl_operator
	 */
	public function insertData()
	{
		//检查栏位资料
		//$this->checkFieldData('add');
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}

		$dataArr['create_user'] = $this->session->userdata('user_sn');
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_operator", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);

			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_operator
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}

		$whereStr = "s_num = '{$s_num}'";

		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['update_date'] = "now()";
		$colDataArr['update_ip'] = $this->input->ip_address();

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_operator", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	/**
	 * 删除 tbl_operator
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"						=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_operator", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
}
/* End of file model_operator.php */
/* Location: ./application/models/model_operator.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_monitor extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 	// 	$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		// if($search_txt != ''){
		// 	$whereStr .= " and tbst.battery_id like '%{$search_txt}%'";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"tbss.bss_no"		=>  "Y",
			"tbss.bss_id"		=>	"Y",
			"tbss.province"		=>	"Y",
			"tbss.city"			=>	"Y",
			"tbss.district"		=>	"Y",
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lstw', $setAlias);
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd = "SELECT count(*) cnt 
					FROM log_swap_track_warring lstw 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
					LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
					LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
					where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' {$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 	// 	$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		// if($search_txt != ''){
		// 	$whereStr .= " and (tbss.bss_no like '%{$search_txt}%' or ";
		// }

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"tbss.bss_no"		=>  "Y",
			"tbss.bss_id"		=>	"Y",
			"tbss.province"		=>	"Y",
			"tbss.city"			=>	"Y",
			"tbss.district"		=>	"Y",
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lstw', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = " order by lstw.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		/*$SQLCmd = "SELECT lstw.*,tbss.location,tbss.bss_id,tbss.bss_no,tbss.bss_no as bss_no2,tbss.province,tbss.city,tbss.district,tbst.battery_id,su.user_name,su.user_id,tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity 
					FROM log_swap_track_warring lstw 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
					LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
					LEFT JOIN (SELECT * FROM log_battery_info ORDER BY s_num DESC) tbi ON tbss.s_num = tbi.sb_num AND tbst.track_no = tbi.track_no AND tbst.battery_id = tbi.battery_id 
					LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
					where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' 
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby} " ;*/


		$SQLCmd = "SELECT lstw.*,tbss.location,tbss.bss_id,tbss.bss_no,tbss.bss_no as bss_no2,tbss.province,tbss.city,tbss.district,tbst.battery_id,su.user_name,su.user_id,tp.map_province_name, tp.map_type
					FROM log_swap_track_warring lstw 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
					left join tbl_province tp ON tp.province_name = tbss.province
					LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
					LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
					where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' 
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby} " ;

		$rs = $this->db_query($SQLCmd) ;
		//记录查询log
		$err_province = array();
		$err_city = array();

		$where_sb_num_in = "";
		$where_battery_in = "";
		if($rs)
		{
			$sbNumArr = array();
			$batteryArr = array();
			$sb_n = 0;
			$b_n = 0;
			foreach($rs as $key => $value)
			{
				if(!in_array($value['sb_num'], $sbNumArr))
				{
					$sbNumArr[$sb_n] = $value['sb_num'];
					$sb_n++;
				}
				if(!in_array($value['battery_id'], $batteryArr))
				{
					$batteryArr[$b_n] = $value['battery_id'];
					$b_n++;
				}

				if(!isset($err_province[$value['map_province_name']])){
					$err_province[$value['map_province_name']] = $value['map_province_name'];
				}

				if($value['map_type'] == 0){
					if(!isset($err_city[$value['city']])){
						$err_city[$value['city']] = $value['city'];
					}
				}
				if(!isset($err_city[$value['district']])){
					$err_city[$value['district']] = $value['district'];
				}
			}
			$where_sb_num_in = join( "','", $sbNumArr ) ;
			$where_battery_in = join( "','", $batteryArr ) ;
		}

		$this->model_background->log_insert_search('', $SQLCmd);

		$sql_battery = "SELECT tbi.battery_id, tbst.track_no, tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity 
						FROM tbl_battery tbi 
						LEFT JOIN tbl_battery_swap_track tbst ON tbst.battery_id = tbi.battery_id 
						WHERE tbi.battery_id in ('{$where_battery_in}') AND tbst.sb_num in ('{$where_sb_num_in}')";
		// echo $sql_battery;
		$rs_2 = $this->db_query($sql_battery) ;
		//记录查询log
		$this->model_background->log_insert_search('', $sql_battery);

		$battery_info = array();
		if(count($rs_2)>0){
			foreach($rs_2 as $rs_arr){
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_temperature'] = $rs_arr['battery_temperature'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_voltage'] = $rs_arr['battery_voltage'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_amps'] = $rs_arr['battery_amps'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_capacity'] = $rs_arr['battery_capacity'];
			}
		}
		
		$return = array();
		$return[0] = $rs;
		$return[1] = $battery_info;
		$return[2] = $err_province;
		$return[3] = $err_city;

		return $return;
	}

	public function getqueryList()
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$province = $this->input->post("province");
 		$city = $this->input->post("city");
 		$district = $this->input->post("district");
 		$bss_id = $this->input->post("bss_id");
		$map_type = 0;
 		if($province!="")
 		{
			//检查当下的省份转成资料库存档的省份
			$p_arr = $this->Model_show_list->check_province($province);
			$new_province = $p_arr[0];
			$map_type = $p_arr[1];
 			$whereStr .= " and tbss.province = '{$new_province}'";
 		}

 		if($city!="")
 		{
			if($map_type == 0){
 				$whereStr .= " and tbss.city = '{$city}'";
			}
 		}
		
		if($map_type == 1){
			if($city!=""){
				$whereStr .= " and tbss.district = '{$city}'";
			}
		}else{
			if($district!="")
			{
				$whereStr .= " and tbss.district = '{$district}'";
			}
		}

 		if($bss_id)
 		{
 			$whereStr .= " and tbss.bss_id = '{$bss_id}'";
 		}

		//排序动作
		$sql_orderby = " order by lstw.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		// $SQLCmd = "SELECT lstw.*,tbss.location,tbss.bss_id,tbss.bss_no,tbss.bss_no as bss_no2,tbss.province,tbss.city,tbss.district,tbst.battery_id,su.user_name,su.user_id,tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity 
		// 			FROM log_swap_track_warring lstw 
		// 			LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
		// 			LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
		// 			LEFT JOIN (SELECT * FROM log_battery_info ORDER BY s_num DESC) tbi ON tbss.s_num = tbi.sb_num AND tbst.track_no = tbi.track_no AND tbst.battery_id = tbi.battery_id 
		// 			LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
		// 			where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' 
		// 			{$whereStr} 
		// 			GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
		// 			{$sql_orderby} " ;
		// $rs = $this->db_query($SQLCmd) ;
		
		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT lstw.*,tbss.location,tbss.bss_id,tbss.bss_no,tbss.bss_no as bss_no2,tbss.province,tbss.city,tbss.district,tbst.battery_id,su.user_name,su.user_id
					FROM log_swap_track_warring lstw 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
					LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
					LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
					where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' 
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby} " ;


		$rs = $this->db_query($SQLCmd) ;
		//记录查询log

		$where_sb_num_in = "";
		$where_battery_in = "";
		if($rs)
		{
			$sbNumArr = array();
			$batteryArr = array();
			foreach($rs as $key => $value)
			{
				$sbNumArr[$key] = $value['battery_id'];
				$batteryArr[$key] = $value['sb_num'];
			}
			$where_sb_num_in = join( "','", $sbNumArr ) ;
			$where_battery_in = join( "','", $batteryArr ) ;
		}

		$this->model_background->log_insert_search('', $SQLCmd);

		$sql_battery = "SELECT tbi.battery_id, tbi.track_no, tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity 
						FROM (SELECT * FROM log_battery_info WHERE sb_num in ('{$where_sb_num_in}') AND battery_id in ('{$where_battery_in}') order by s_num desc) tbi 
						group by tbi.battery_id";
		// echo $sql_battery;
		$rs_2 = $this->db_query($sql_battery) ;
		//记录查询log
		$this->model_background->log_insert_search('', $sql_battery);

		$battery_info = array();
		if(count($rs_2)>0){
			foreach($rs_2 as $rs_arr){
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_temperature'] = $rs_arr['battery_temperature'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_voltage'] = $rs_arr['battery_voltage'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_amps'] = $rs_arr['battery_amps'];
				$battery_info[$rs_arr['battery_id']][$rs_arr['track_no']]['battery_capacity'] = $rs_arr['battery_capacity'];
			}
		}
		
		$return = array();
		$return[0] = $rs;
		$return[1] = $battery_info;

		return $return;
	}

	public function get_battery_staion_track_List()
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$sb_num = $this->input->post("sb_num");

 		$whereStr .= " and tbss.s_num = '{$sb_num}'";

		//排序动作
		$sql_orderby = "";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT lstw.*,tbss.location,tbss.bss_id,tbss.bss_no,tbss.bss_no as bss_no2,tbss.province,tbss.city,tbss.district,tbst.battery_id,su.user_name,su.user_id
					FROM log_swap_track_warring lstw 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lstw.sb_num 
					LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = lstw.sb_num AND tbst.track_no = lstw.track_no 
					LEFT JOIN sys_user su ON su.user_sn = lstw.process_user 
					where lstw.status <> '3' AND tbss.status <> 'D' AND tbst.status <> 'D' 
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby} " ;


		$rs = $this->db_query($SQLCmd) ;
		//记录查询log

		$where_sb_num_in = "";
		$where_battery_in = "";
		if($rs)
		{
			$sbNumArr = array();
			$batteryArr = array();
			foreach($rs as $key => $value)
			{
				$sbNumArr[$key] = $value['battery_id'];
				$batteryArr[$key] = $value['sb_num'];
			}
			$where_sb_num_in = join( "','", $sbNumArr ) ;
			$where_battery_in = join( "','", $batteryArr ) ;
		}

		$this->model_background->log_insert_search('', $SQLCmd);

		$SQLCmd = "SELECT to1.top01,tbss.location,tbss.bss_id,tbss.bss_no,tbst.track_no,tbst.battery_id,tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity,tbi.charge_cycles,tbi.electrify_time,tbi.status 
						from tbl_battery_swap_track tbst 
						LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
						LEFT JOIN tbl_operator to1 ON tbss.so_num = to1.s_num 
						left join (SELECT * FROM log_battery_info 
						WHERE sb_num in ('{$where_sb_num_in}') AND battery_id in ('{$where_battery_in}') order by s_num desc) tbi ON tbi.sb_num = tbss.s_num
						where tbst.status <> 'D' and tbss.status <> 'D' {$whereStr}
						GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id {$sql_orderby}";


		/*$SQLCmd = "SELECT to1.top01,tbss.location,tbss.bss_id,tbss.bss_no,tbst.track_no,tbst.battery_id,tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity,tbi.charge_cycles,tbi.electrify_time,tbi.status 
					FROM tbl_battery_swap_track tbst 
					LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
					LEFT JOIN tbl_operator to1 ON tbss.so_num = to1.s_num 
					LEFT JOIN (SELECT * FROM log_battery_info ORDER BY s_num DESC) tbi ON tbss.s_num = tbi.sb_num AND tbst.track_no = tbi.track_no AND tbst.battery_id = tbi.battery_id 
					where tbss.status <> 'D' AND tbst.status <> 'D'  
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby}" ;



		/*$SQLCmd = "SELECT to1.top01,tbss.location,tbss.bss_id,tbss.bss_no,tbst.track_no,tbst.battery_id,tbi.battery_temperature,tbi.battery_voltage,tbi.battery_amps,tbi.battery_capacity,tbi.charge_cycles,tbi.electrify_time,tbi.status 
					FROM tbl_battery_swap_track tbst 
					LEFT JOIN tbl_battery_swap_station tbss ON tbst.sb_num = tbss.s_num 
					LEFT JOIN tbl_operator to1 ON tbss.so_num = to1.s_num 
					LEFT JOIN (SELECT * FROM log_battery_info ORDER BY s_num DESC) tbi ON tbss.s_num = tbi.sb_num AND tbst.track_no = tbi.track_no AND tbst.battery_id = tbi.battery_id 
					where tbss.status <> 'D' AND tbst.status <> 'D'  
					{$whereStr} 
					GROUP BY tbst.sb_num,tbst.track_no,tbst.battery_id 
					{$sql_orderby}" ;*/
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	/**
	 * 取得单一 battery_swap_station info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_battery_swap_track where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery_swap_track", $whereArr ) ;
		return $rs ;
	}

	public function getBatteryStation(){
		$whereStr = "";
		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tbss.so_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT 
						tp.map_province_name as province, city, district, tp.map_type
					FROM tbl_battery_swap_station tbss
					left join tbl_province tp ON tp.province_name = tbss.province
					where tbss.status = 'Y' {$whereStr} group by tbss.province, tbss.city, tbss.district"  ;
		$rs = $this->db_query($SQLCmd) ;
		$province = array();
		$city = array();
		if(count($rs)>0){
			foreach($rs as $rs_arr){
				if(!isset($province[$rs_arr['province']])){
					$province[$rs_arr['province']] = $rs_arr['province'];
				}

				if($rs_arr['map_type'] == 0){
					if(!isset($city[$rs_arr['city']])){
						$city[$rs_arr['city']] = $rs_arr['city'];
					}
				}
				if(!isset($city[$rs_arr['district']])){
					$city[$rs_arr['district']] = $rs_arr['district'];
				}
			}
		}
		
		$return = array();
		$return[0] = $province;
		$return[1] = $city;

		return $return;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_column extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	/**
	 * 端末机管理 列表栏位
	 */
	public function getTerminalListCol()
	{
		
		$colArr = array (
			"TERMINAL_ID"		=>	"端末机代号",
			"MERCHANT_ID"		=>	"商店代号",
			"ACQUIRE_BANK_ID"	=>	"银行别代码",
			"TMS_DOWNLOAD"		=>	"是否需下载TMS参数",
			"STATUS"			=>	"状态"
		);

		$colSetArr = array (
			"TERMINAL_ID"		=>	"",
			"MERCHANT_ID"		=>	"",
			"ACQUIRE_BANK_ID"	=>	"",
			"TMS_DOWNLOAD"		=>	"",
			"STATUS"			=>	"width=1%"
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	/**
	 * 未结帐交易明细查询 列表栏位
	 */
	public function getUnsettlement_transactionListCol()
	{
		
		$colArr = array (
			"MERCHANT_ID"		=>	"商店代号",
			"FILE_DATETIME"		=>	"系统时间",
			"TRANS_DATETIME"	=>	"交易时间",
			"NEW_TRANS_TYPE"	=>	"交易别名称",
			"CARD_NAME"			=>	"卡别",
			"CARD_NO"			=>	"卡号",
			"NEW_AMOUNT"		=>	"交易金额",
			"INVOICE_NO"		=>	"调阅编号",
			"BATCH_NO"			=>	"批次号码",
		);

		$colSetArr = array (
			"MERCHANT_ID"		=>	"",
			"FILE_DATETIME"		=>	"",
			"TRANS_DATETIME"	=>	"",
			"NEW_TRANS_TYPE"	=>	"",
			"CARD_NAME"			=>	"",
			"CARD_NO"			=>	"",
			"NEW_AMOUNT"		=>	"",
			"INVOICE_NO"		=>	"",
			"BATCH_NO"			=>	"",
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	/**
	 * 商店初始化密码查询 列表栏位
	 */
	public function getInitalPWDListCol()
	{
		
		$colArr = array (
			"INITIAL_PASSWORD"		=>	"初始化密码",
			"STATUS"				=>	"状态"
		);

		$colSetArr = array (
			"INITIAL_PASSWORD"		=>	"",
			"STATUS"				=>	"width=1%"
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	/**
	 * 事件日志查询 列表栏位
	 */
	public function getEventLogListCol()
	{
		
		$colArr = array (
			"TERMINAL_ID"		=>	"端末机代号",
			"EVENT_DATA"		=>	"事件日志",
			"EVENT_TYPE"		=>	"事件日志类型",
		);

		$colSetArr = array (
			"TERMINAL_ID"		=>	"",
			"EVENT_DATA"		=>	"",
			"EVENT_TYPE"		=>	"",
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	/**
	 * 商店退货作业 列表栏位
	 */
	public function getRefundListCol()
	{
		
		$colArr = array (
			"MERCHANT_ID"		=>	"商店代号",
			"FILE_DATETIME"		=>	"系统时间",
			"TRANS_DATETIME"	=>	"交易时间",
			"TRANS_TYPE"		=>	"交易别名称",
			"CARD_NAME"			=>	"卡别",
			"CARD_NO"			=>	"卡号",
			"AMOUNT"			=>	"交易金额",
			"INVOICE_NO"		=>	"调阅编号",
			"BATCH_NO"			=>	"批次号码",
		);

		$colSetArr = array (
			"MERCHANT_ID"		=>	"",
			"FILE_DATETIME"		=>	"",
			"TRANS_DATETIME"	=>	"",
			"TRANS_TYPE"		=>	"",
			"CARD_NAME"			=>	"",
			"CARD_NO"			=>	"",
			"AMOUNT"			=>	"",
			"INVOICE_NO"		=>	"",
			"BATCH_NO"			=>	"",
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	
	/**
	 * 商店结帐作业结果 列表栏位
	 */
	public function getSettlementResultListCol()
	{
		
		$colArr = array (
			"MERCHANT_ID"			=>	"商店代号",
			"TERMINAL_ID"			=>	"端末机ID",
			"BATCH_NO"				=>	"批次号码",
			"HOST_SALE_CNT"			=>	"主机销售笔数",
			"HOST_SALE_AMOUNT"		=>	"主机销售金额",
			"HOST_REFUND_CNT"		=>	"主机退货笔数",
			"HOST_REFUND_AMOUNT"	=>	"主机退货金额",
			"rssult"				=>	"结帐结果"
		);

		$colSetArr = array (
			"MERCHANT_ID"			=>	"",
			"TERMINAL_ID"			=>	"",
			"BATCH_NO"				=>	"",
			"HOST_SALE_CNT"			=>	"",
			"HOST_SALE_AMOUNT"		=>	"",
			"HOST_REFUND_CNT"		=>	"",
			"HOST_REFUND_AMOUNT"	=>	"",
			"rssult"				=>	""
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}
	
	/**
	 * 商店读卡机配对管理 列表栏位
	 */
	public function getReader_pairingListCol()
	{
		
		$colArr = array (
			"TERMINAL_ID"		=>	"端末机代号",
			"MERCHANT_ID"		=>	"商店代号",
			"ACQUIRE_BANK_ID"	=>	"收单代号",
			"STATUS"			=>	"状态",
		);

		$colSetArr = array (
			"TERMINAL_ID"		=>	"",
			"MERCHANT_ID"		=>	"",
			"ACQUIRE_BANK_ID"	=>	"",
			"STATUS"			=>	"",
		);
		return array( "colName"=>$colArr, "colSet"=>$colSetArr ) ;
	}

}
/* End of file model_column.php */

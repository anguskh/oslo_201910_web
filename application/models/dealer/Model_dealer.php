<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_dealer extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getDealerAllCnt()
	{
		$SQLCmd = "select count(*) cnt from tbl_dealer where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDealerList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$SQLCmd = "SELECT *
  						FROM tbl_dealer where status <> 'D' {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得指定　dealer_code　的 dealer info
	 */
	public function getDealerInfoAssign( $dealer_code = "" )
	{
		if ( !empty( $dealer_code) )	$whereStr = "where dealer_code='{$dealer_code}'" ;
		else $whereStr = "" ;

		$SQLCmd = "SELECT dealer_code, dealer_set FROM tbl_dealer {$whereStr}" ;
		// echo "SQLCmd=".$SQLCmd."<BR>";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得单一 dealer info
	 */
	public function getDealerInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_dealer where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_dealer", $whereArr ) ;
		return $rs ;
	}
	
	//sub_dealer要取得
	public function getDealerALL(){
		$SQLCmd = "SELECT s_num, tde01
  						FROM tbl_dealer where status <> 'D'
						order by s_num";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}
	
		/**
	 * 新增 tbl_dealer
	 */
	public function insertData()
	{
		//检查栏位资料
		//$this->checkFieldData('add');
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}

		$dataArr['create_user'] = $this->session->userdata('user_sn');
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_dealer", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);

			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_dealer
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}

		$whereStr = "s_num = '{$s_num}'";

		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['update_date'] = "now()";
		$colDataArr['update_ip'] = $this->input->ip_address();

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_dealer", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	/**
	 * 删除 tbl_dealer
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"						=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_dealer", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}
}
/* End of file model_dealer.php */
/* Location: ./application/models/model_dealer.php */
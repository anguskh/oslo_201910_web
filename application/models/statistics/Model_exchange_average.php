<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchange_average extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;


		$SQLCmd = "SELECT count(*) cnt 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' {$whereStr} and tb.status <> 'D'" ;

		$rs = $this->db_query($SQLCmd) ;



		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tb.s_num ";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;

		$SQLCmd = "SELECT ldi.unit_id as battery_id,
							tb.manufacture_date,
							CONCAT(ldi.latitude,',',ldi.longitude) as gps_position,
							CONCAT(ldi. battery_capacity,'%') as battery_capacity 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' and tb.status <> 'D'
						{$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset}" ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	public function search_all( $whereStr = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, '');
		else $whereStr .= "" ;


		$return = array();
		//算起始天數
		$SQLCmd8 = "SELECT DATEDIFF(MAX(leave_date),MIN(leave_date)) as log_days 
			 FROM log_battery_leave_return where {$whereStr} AND leave_date IS NOT NULL ";
		$rs8 = $this->db_query($SQLCmd8) ;
		$log_days = $rs8[0]['log_days'];	//相差天數
		$log_days = (($log_days > 0)?$log_days:'1');	//相差天數

		$SQLCmd1 = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE status <> 'D'" ;
		$rs1 = $this->db_query($SQLCmd1) ;
		$statino_num = $rs1[0]['cnt'];	
		$return[0] = $statino_num; //換電站


		$SQLCmd7 = "SELECT DATE_FORMAT(leave_date,'%H') as hour,count(s_num)/{$statino_num} as housr_avgNum 
			 FROM log_battery_leave_return where {$whereStr} AND leave_date IS NOT NULL 
			 GROUP BY DATE_FORMAT(leave_date,'%H')
			 ORDER BY DATE_FORMAT(leave_date,'%H')";
		$rs7 = $this->db_query($SQLCmd7) ;
		$hour_arr = array();
		if($rs7){
			foreach($rs7 as $key => $value){
				$hour_arr[$value['hour']] = $value['housr_avgNum'];
			}
		}
		$return[3] = $hour_arr;

		$SQLCmd9 = "SELECT DATE_FORMAT(leave_date,'%H') as hour,count(s_num)/{$statino_num}/{$log_days} as housr_avgNum 
			 FROM log_battery_leave_return where {$whereStr} AND leave_date IS NOT NULL 
			 GROUP BY DATE_FORMAT(leave_date,'%H')
			 ORDER BY DATE_FORMAT(leave_date,'%H')";
		$rs9 = $this->db_query($SQLCmd9) ;
		$hour9_arr = array();
		if($rs9){
			foreach($rs9 as $key => $value){
				$hour9_arr[$value['hour']] = number_format(round($value['housr_avgNum'],4),4);
			}
		}
		$return[9] = $hour9_arr;
		return $return ;
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
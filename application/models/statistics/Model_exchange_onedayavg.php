<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchange_onedayavg extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "1=1";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr');
		else $whereStr .= "" ;

		if($whereStr == "1=1")
		{
			$whereStr = "DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') between date_sub(curdate(),interval 1 month) AND curdate()";
		}

		$SQLCmd = "SELECT count(a.s_num) cnt  
					FROM (SELECT lblr.s_num FROM log_battery_leave_return lblr 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
					WHERE {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL 
					GROUP BY DATE_FORMAT(lblr.leave_date,'%Y-%m-%d'),lblr.leave_sb_num) a " ;

		$rs = $this->db_query($SQLCmd) ;

		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by log_date ";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			if($field == 'sum_exchangenum'){
				$sql_orderby = " order by count(lblr.s_num)".$orderby;
			}else{
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "1=1";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr');
		else $whereStr .= "" ;

		if($whereStr == "1=1")
		{
			$whereStr = "DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') between date_sub(curdate(),interval 1 month) AND curdate()";
		}

		$SQLCmd = "SELECT DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') as log_date,count(lblr.s_num) as sum_exchangenum,tbss.bss_id,tbss.location  
					FROM log_battery_leave_return lblr 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
					WHERE {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL 
					GROUP BY DATE_FORMAT(lblr.leave_date,'%Y-%m-%d'),lblr.leave_sb_num 
					{$sql_orderby} 
  					LIMIT {$startRow} , {$offset}" ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	public function search_all( $whereStr = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}
		//搜寻对应
		//ex "field_name"=> "Alias"

		// echo $whereStr;

		$return = array();

		if($whereStr == " 1=1 ")
		{
			$whereStr = "DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') between date_sub(curdate(),interval 1 month) AND curdate()";
		}

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr');


		//算起始天數
		$SQLCmd8 = "SELECT DATEDIFF(MAX(lblr.leave_date),MIN(lblr.leave_date)) as log_days 
			 		FROM log_battery_leave_return lblr 
			 		LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
			 		where {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL ";
		$rs8 = $this->db_query($SQLCmd8) ;
		$return[8] = $rs8[0]['log_days'];	//相差天數

		$SQLCmd = "SELECT MAX(lblr.leave_date) as max_log_date,MIN(lblr.leave_date) as min_log_date
					FROM log_battery_leave_return lblr 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
					where {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL ";
		$rs = $this->db_query($SQLCmd) ;
		$min_log_date = ((count($rs)>0)?substr($rs[0]['min_log_date'],0,10):"");
		$max_log_date = ((count($rs)>0)?substr($rs[0]['max_log_date'],0,10):"");
		$return[1] = $min_log_date;
		$return[2] = $max_log_date;

		$SQLCmd7 = "SELECT DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') as log_date,count(lblr.s_num) as sum_exchangenum 
					FROM log_battery_leave_return lblr 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
					WHERE {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL 
					GROUP BY DATE_FORMAT(lblr.leave_date,'%Y-%m-%d')";
		$rs7 = $this->db_query($SQLCmd7) ;
		$day_arr = array();
		if($rs7){
			foreach($rs7 as $key => $value){
				$day_arr[$value['log_date']] = $value['sum_exchangenum'];
			}
		}
		$return[3] = $day_arr;

		$SQLCmd4 = "SELECT DATE_FORMAT(lblr.leave_date,'%Y-%m-%d') as log_date,count(lblr.s_num) as sum_exchangenum,tbss.bss_id,tbss.location  
					FROM tbl_battery_swap_station tbss 
					LEFT JOIN log_battery_leave_return lblr ON tbss.s_num = lblr.leave_sb_num 
					WHERE {$whereStr} AND tbss.status <> 'D' AND lblr.leave_date IS NOT NULL 
					GROUP BY DATE_FORMAT(lblr.leave_date,'%Y-%m-%d'),lblr.leave_sb_num";
		$rs4 = $this->db_query($SQLCmd4);
		$onday_bss_arr = array();
		if($rs4)
		{
			foreach($rs4 as $key4 => $value4)
			{
				$onday_bss_arr[$value4['bss_id']][$value4['log_date']] = $value4['sum_exchangenum'];
				$onday_bss_arr[$value4['bss_id']]['location'] = $value4['location'];
			}
		}

		$return[4] = $onday_bss_arr;
		return $return ;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
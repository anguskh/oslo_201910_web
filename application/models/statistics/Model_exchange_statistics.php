<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchange_statistics extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;


		$SQLCmd = "SELECT count(*) cnt 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' {$whereStr} and tb.status <> 'D'" ;

		$rs = $this->db_query($SQLCmd) ;



		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tb.s_num ";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;

		$SQLCmd = "SELECT ldi.unit_id as battery_id,
							tb.manufacture_date,
							CONCAT(ldi.latitude,',',ldi.longitude) as gps_position,
							CONCAT(ldi. battery_capacity,'%') as battery_capacity 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id 
						WHERE ldi.log_type = 'B' and tb.battery_id != '' and tb.status <> 'D' 
						{$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset}" ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	

	public function search_all( $whereStr = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, '');
		else $whereStr .= "" ;


		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		$whereStr2 = "";
		$whereStrbattery = "";
		$whereStrPY = "";
		$whereStrTB = "";
		$whereStrSo = "";
		$wherelt = "";
		$wherelt2 = "";
		if($to_flag == '1'){
			$whereStr2 .= " and so_num = {$to_flag_num}" ;
			$whereStrbattery .= " and DorO_flag = 'O' and  do_num= {$to_flag_num}" ;
			$whereStrPY .= " and  tm.to_num= {$to_flag_num}" ;
			$whereStrTB .= " and tb.DorO_flag = 'O' and  tb.do_num= {$to_flag_num}" ;
			$whereStrSo .= " and lao.so_num= {$to_flag_num}" ;
			$wherelt .= " and ( ( leave_DorO_flag = 'O' and  leave_do_num= {$to_flag_num} )  or ( return_DorO_flag = 'O' and  return_do_num= {$to_flag_num} ) )" ;
			$wherelt2 .= " and ( ( lblr.leave_DorO_flag = 'O' and  lblr.leave_do_num= {$to_flag_num} )  or ( lblr.return_DorO_flag = 'O' and  lblr.return_do_num= {$to_flag_num} ) )" ;
		}

		$return = array();
		//算起始天數
		$SQLCmd8 = "SELECT DATEDIFF(MAX(leave_date),MIN(leave_date)) as log_days 
			 FROM log_battery_leave_return where {$whereStr} AND leave_date IS NOT NULL ";
		$rs8 = $this->db_query($SQLCmd8) ;
		$log_days = $rs8[0]['log_days'];		//相差天數
		$log_days = (($log_days == '')?0:$log_days);
		$SQLCmd1 = "SELECT count(*) cnt, sum(exchange_num) as exchange_num FROM tbl_battery_swap_station WHERE status <> 'D' {$whereStr2}" ;
		$rs1 = $this->db_query($SQLCmd1) ;
		$statino_num = $rs1[0]['cnt'];	
		$battery_exchange_count = $rs1[0]['exchange_num'];	
		$return['statino_num'] = $statino_num;	//換電站總數
		$return['battery_exchange_count'] = $battery_exchange_count; //電池總交換次數


		$SQLCmd2 = "SELECT count(*) cnt FROM tbl_battery WHERE status <> 'D' {$whereStrbattery }" ;
		$rs2 = $this->db_query($SQLCmd2) ;
		$battery_num = $rs2[0]['cnt'];	
		$return['battery_num'] = $battery_num;						//電池總數

		//$SQLCmd3 = "SELECT count(*) cnt FROM tbl_vehicle WHERE status <> 'D'" ;
		//$rs3 = $this->db_query($SQLCmd3) ;
		//$vehicle_num = $rs3[0]['cnt'];	
		//$return['vehicle_num'] = $vehicle_num; //車輛總數

		$SQLCmd3 = "SELECT count(*) cnt FROM tbl_battery WHERE status <> 'D' and position = 'V' {$whereStrbattery}" ;
		$rs3 = $this->db_query($SQLCmd3) ;
		$vehicle_num = $rs3[0]['cnt'];	
		$return['vehicle_num'] = $vehicle_num; //車輛總數(改撈電池 位置在機車上的)

		//$SQLCmd4 = "SELECT sum(price) sum_price FROM tbl_payorder WHERE pay_status = 1 and refund_status = 'N'" ;
		$SQLCmd4 = "SELECT sum(tp.price) sum_price
									FROM tbl_payorder tp
									LEFT JOIN tbl_member tm ON tm.s_num = tp.tm_num
									WHERE tp.pay_status = 1 and tp.refund_status = 'N' {$whereStrPY}" ;
		$rs4 = $this->db_query($SQLCmd4) ;
		$wechat_price = $rs4[0]['sum_price'];	
		$return['wechat_price'] = (($wechat_price == '')?0:$wechat_price);	//微信支付總金額


		$SQLCmd7 = "SELECT DATE_FORMAT(leave_date,'%H') as hour,count(s_num)/{$statino_num} as housr_avgNum 
			 FROM log_battery_leave_return 
			 where {$whereStr}  AND leave_date IS NOT NULL {$wherelt}
			 GROUP BY DATE_FORMAT(leave_date,'%H')
			 ORDER BY DATE_FORMAT(leave_date,'%H')";
		$rs7 = $this->db_query($SQLCmd7) ;
		$hour_arr = array();
		if($rs7){
			foreach($rs7 as $key => $value){
				$hour_arr[$value['hour']] = $value['housr_avgNum'];
			}
		}
		$return['housr_avgNum'] = $hour_arr;

		$SQLCmd9 = "SELECT DATE_FORMAT(leave_date,'%H') as hour,count(s_num)/{$statino_num}/{$log_days} as housr_avgNum 
			 FROM log_battery_leave_return where {$whereStr} AND leave_date IS NOT NULL {$wherelt}
			 GROUP BY DATE_FORMAT(leave_date,'%H')
			 ORDER BY DATE_FORMAT(leave_date,'%H')";
		$rs9 = $this->db_query($SQLCmd9) ;
		$hour9_arr = array();
		if($rs9){
			foreach($rs9 as $key => $value){
				$hour9_arr[$value['hour']] = number_format(round($value['housr_avgNum'],4),4);
			}
		}
		$return['housr_average'] = $hour9_arr;

		//单日交换次数排行前十名
		$SQLCmd10 = "SELECT 
						b.sb_num,b.dayMaxNum, b.log_date, b.bss_id, b.location
							FROM (
									SELECT a.sb_num,a.log_date,MAX(a.daySUMNum) as dayMaxNum, a.bss_id, a.location
										FROM (
											SELECT lblr.leave_sb_num as sb_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as daySUMNum 
											FROM log_battery_leave_return lblr 
											left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num and tbss.status <> 'D'
											where {$whereStr} {$wherelt2} AND lblr.leave_date IS NOT NULL 
											GROUP BY lblr.leave_sb_num,SUBSTRING(lblr.leave_date,1,10) 
										) as a
							GROUP BY a.sb_num ) as b
							order by b.dayMaxNum desc
							limit 10";
		$rs10 = $this->db_query($SQLCmd10) ;
		$lo_arr = array();
		if($rs10){
			foreach($rs10 as $arrs){
				$lo_arr[$arrs['sb_num']]['dayMaxNum'] = $arrs['dayMaxNum'];
				$lo_arr[$arrs['sb_num']]['log_date'] = $arrs['log_date'];
				$lo_arr[$arrs['sb_num']]['bss_id'] = $arrs['bss_id'];
				$lo_arr[$arrs['sb_num']]['location'] = $arrs['location'];
			}
		}
		$return['dayMaxNum_10'] = $lo_arr;

		$SQLCmd  = "SELECT lao.*, top.top01, tbss.bss_id, tbss.location
					FROM log_alarm_online lao 
					LEFT JOIN tbl_operator top ON lao.so_num = top.s_num 
					LEFT JOIN tbl_battery_swap_station tbss ON lao.sb_num = tbss.s_num 
					WHERE lao.status != 3 {$whereStrSo}" ;
		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		$return['log_alarm_online'] = $rs;	//即時告警


		//查有在軌道上的電池狀態
		$SQLCmd = "SELECT tbst.*, tb.battery_capacity, tb.position
  						FROM tbl_battery tb 
						left join tbl_battery_swap_track tbst ON tbst.battery_id = tb.battery_id
  						where tb.status <> 'D' {$whereStrTB}" ;
		$rs = $this->db_query($SQLCmd);
		$battery_status_num = array();
		$battery_capacity_num = array();
		$battery_capacity_num[1] = 0;
		$battery_capacity_num[2] = 0;
		$battery_capacity_num[3] = 0;
		$battery_capacity_num[4] = 0;
		$battery_capacity_num[5] = 0;
		$battery_capacity_num[6] = 0;
		$battery_capacity_num[7] = 0;
		$battery_capacity_num[8] = 0;
		$battery_capacity_num[9] = 0;
		$battery_capacity_num[10] = 0;
		
		$battery_status_num['Y'] = 0;
		$battery_status_num['N'] = 0;
		$battery_status_num['S'] = 0;
		$battery_status_num['E'] = 0;
		//$battery_status_num['F'] = 0;
		$battery_status_num['V'] = 0;	//電動機車

		if(count($rs)>0){
			foreach($rs as $rs_arr){
				$class = "";
				 if($rs_arr['track_no'] != '' && $rs_arr['battery_id'] != ''){
					//飽電：綠色 充電中：橘色
					//故障：紅色 停用：灰色
					//電池交換軌道充電狀態(Y=充電中；N=充飽電；S=待機；F=故障) status(Y=啟用；N=停用；E=故障)
					if($rs_arr['status'] == 'N' || $rs_arr['status'] == 'E' ){
						$class = $rs_arr['status'];
					}else{
						if($rs_arr['column_charge'] == 'Y'){
							$class = 'Y';
						}else if($rs_arr['column_charge'] == 'F'){
							//故障
							$class = 'E';
						}else if($rs_arr['column_charge'] == 'N' || $rs_arr['column_charge'] == 'S'){
							$class = "N";
						}
					}
					
					if($class != ''){
						$battery_status_num[$class]++;
					}
				 }

				//電動機車
				 if($rs_arr['position'] == 'V'){
					$battery_status_num['V']++;
				 }

				 //電量
				 if($rs_arr['battery_capacity'] < 11){
					//電量1~10
					$battery_capacity_num[1]++;
				 }else if($rs_arr['battery_capacity'] >= 11 && $rs_arr['battery_capacity'] <= 20){
					//電量11~20
					$battery_capacity_num[2]++;
				 }else if($rs_arr['battery_capacity'] >= 21 && $rs_arr['battery_capacity'] <= 30){
					//電量21~30
					$battery_capacity_num[3]++;
				 }else if($rs_arr['battery_capacity'] >= 31 && $rs_arr['battery_capacity'] <= 40){
					//電量31~40
					$battery_capacity_num[4]++;
				 }else if($rs_arr['battery_capacity'] >= 41 && $rs_arr['battery_capacity'] <= 50){
					//電量41~50
					$battery_capacity_num[5]++;
				 }else if($rs_arr['battery_capacity'] >= 51 && $rs_arr['battery_capacity'] <= 60){
					//電量51~60
					$battery_capacity_num[6]++;
				 }else if($rs_arr['battery_capacity'] >= 61 && $rs_arr['battery_capacity'] <= 70){
					//電量61~70
					$battery_capacity_num[7]++;
				 }else if($rs_arr['battery_capacity'] >= 71 && $rs_arr['battery_capacity'] <= 80){
					//電量71~80
					$battery_capacity_num[8]++;
				 }else if($rs_arr['battery_capacity'] >= 81 && $rs_arr['battery_capacity'] <= 90){
					//電量81~90
					$battery_capacity_num[9]++;
				 }else if($rs_arr['battery_capacity'] >= 91 && $rs_arr['battery_capacity'] <= 100){
					//電量91~100
					$battery_capacity_num[10]++;
				 }
			}
		}
		
		$return['battery_status_num'] = $battery_status_num;	//電池狀態
		$return['battery_capacity'] = $battery_capacity_num;	//電池電力狀態

		return $return ;
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
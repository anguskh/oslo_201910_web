<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchange_oneday extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"tbss.bss_id"		=>  "Y",
			"tbss.district"		=>  "Y"
		);
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT 
						count(*) cnt 
					FROM log_battery_leave_return lblr 
					left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num  
					where {$whereStr} and tbss.status <> 'D' and lblr.leave_date IS NOT NULL 
					 GROUP BY lblr.leave_sb_num,SUBSTRING(lblr.leave_date,1,10)";
		$rs = $this->db_query($SQLCmd) ;
		return count($rs);
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by lblr.leave_date desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			if($field == 'daySUMNum'){
				$sql_orderby = " order by count(lblr.s_num) ".$orderby;
			}else{
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"tbss.bss_id"		=>  "Y",
			"tbss.district"		=>  "Y"
		);
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT 
						lblr.leave_sb_num as s_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as daySUMNum 
					FROM log_battery_leave_return lblr 
					left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
					where {$whereStr} and tbss.status <> 'D' and lblr.leave_date IS NOT NULL 
					 GROUP BY lblr.leave_sb_num,SUBSTRING(lblr.leave_date,1,10)
					 {$sql_orderby} 
					 LIMIT {$startRow} , {$offset}";
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	public function search_all( $whereStr = "")
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " 1=1 ";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"tbss.bss_id"		=>  "Y",
			"tbss.district"		=>  "Y"
		);
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

		$limit = ""; 
		if ( !empty( $search) ) $limit = setSearchLimit($search);
		$limit = (($limit=="")?"10":$limit);


		$return = array();

		$SQLCmd = "SELECT MAX(lblr.leave_date) as max_log_date,MIN(lblr.leave_date) as min_log_date
					FROM log_battery_leave_return lblr 
					left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num /*and tbss.status <> 'D' */
					where {$whereStr} AND lblr.leave_date IS NOT NULL and lblr.leave_sb_num != 0";
		$rs = $this->db_query($SQLCmd) ;
		$min_log_date = ((count($rs)>0)?substr($rs[0]['min_log_date'],0,10):"");
		$max_log_date = ((count($rs)>0)?substr($rs[0]['max_log_date'],0,10):"");
		$return[1] = $min_log_date;
		$return[2] = $max_log_date;

		$SQLCmd10 = "SELECT 
						b.sb_num,b.dayMaxNum, b.log_date, b.bss_id, b.location
							FROM (
									SELECT a.sb_num,a.log_date,MAX(a.daySUMNum) as dayMaxNum, a.bss_id, a.location
										FROM (
											SELECT lblr.leave_sb_num as sb_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as daySUMNum 
											FROM log_battery_leave_return lblr 
											left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num/* and tbss.status <> 'D'*/
											where {$whereStr} AND lblr.leave_date IS NOT NULL  and lblr.leave_sb_num != 0
											GROUP BY lblr.leave_sb_num,SUBSTRING(lblr.leave_date,1,10) 
										) as a
							GROUP BY a.sb_num ) as b
							order by b.dayMaxNum desc
							limit {$limit}";
		$rs10 = $this->db_query($SQLCmd10) ;
		$lo_arr = array();
		if($rs10){
			foreach($rs10 as $arrs){
				$lo_arr[$arrs['sb_num']]['dayMaxNum'] = $arrs['dayMaxNum'];
				$lo_arr[$arrs['sb_num']]['bss_id'] = $arrs['bss_id'];
				$lo_arr[$arrs['sb_num']]['location'] = $arrs['location'];
			}
		}
		$return[10] = $lo_arr;

		$SQLCmd11 = "SELECT 
						b.sb_num,b.dayMaxNum, b.log_date, b.bss_id, b.location
							FROM (
									SELECT a.sb_num,a.log_date,MIN(a.daySUMNum) as dayMaxNum, a.bss_id, a.location
										FROM (
											SELECT lblr.leave_sb_num as sb_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as daySUMNum 
											FROM log_battery_leave_return lblr 
											left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num /*and tbss.status <> 'D'*/
											where {$whereStr} AND lblr.leave_date IS NOT NULL  and lblr.leave_sb_num != 0
											GROUP BY lblr.leave_sb_num,SUBSTRING(lblr.leave_date,1,10) 
										) as a
							GROUP BY a.sb_num ) as b
							order by b.dayMaxNum asc 
							limit {$limit}";
		$rs11 = $this->db_query($SQLCmd11) ;
		$lo11_arr = array();
		if($rs11){
			foreach($rs11 as $arrs){
				$lo11_arr[$arrs['sb_num']]['dayMaxNum'] = $arrs['dayMaxNum'];
				$lo11_arr[$arrs['sb_num']]['bss_id'] = $arrs['bss_id'];
				$lo11_arr[$arrs['sb_num']]['location'] = $arrs['location'];
			}
		}
		$return[11] = $lo11_arr;


		return $return ;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
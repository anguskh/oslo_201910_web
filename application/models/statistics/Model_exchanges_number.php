<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchanges_number extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = " ";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"lblr.leave_date"	=> "Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbss', $setAlias);
		else $whereStr .= "" ;

		if(!empty( $search)){
			$SQLCmd = "SELECT 
							count(*) cnt 
						FROM log_battery_leave_return lblr 
						left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num  
						where tbss.status <> 'D' {$whereStr} and lblr.leave_date IS NOT NULL 
						 GROUP BY lblr.leave_sb_num";
			$rs = $this->db_query($SQLCmd) ;
			return count($rs);
		}else{
			$SQLCmd = "SELECT count(*) cnt 
							FROM tbl_battery_swap_station tbss 
							WHERE tbss.status <> 'D' 
							{$whereStr}" ;
			$rs = $this->db_query($SQLCmd) ;
			return $rs[0]["cnt"];
		}
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$o_type = 'N';
		if(!empty( $search)){
			$o_type = 'Y';
		}
		
		//排序动作
		if($o_type == 'Y'){
			$sql_orderby = " ORDER BY count(lblr.s_num) DESC ";
			$fn = get_fetch_class_random();
			$field = $this->session->userdata("{$fn}_".'field');
			$orderby = $this->session->userdata("{$fn}_".'orderby');
			if($field != "" && $orderby != ""){
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
		}else{
			$sql_orderby = " ORDER BY tbss.exchange_num DESC ";
			$fn = get_fetch_class_random();
			$field = $this->session->userdata("{$fn}_".'field');
			$orderby = $this->session->userdata("{$fn}_".'orderby');
			if($field != "" && $orderby != ""){
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		$setAlias = array(
			"lblr.leave_date"	=> "Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbss', $setAlias);
		else $whereStr .= "" ;

		if(!empty( $search)){
			$SQLCmd = "SELECT 
							lblr.leave_sb_num as s_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as exchange_num 
						FROM log_battery_leave_return lblr 
						left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
						where tbss.status <> 'D' {$whereStr} and lblr.leave_date IS NOT NULL 
						 GROUP BY lblr.leave_sb_num
						 {$sql_orderby} 
						 LIMIT {$startRow} , {$offset}";
		}else{

			$SQLCmd = "SELECT tbss.bss_id, tbss.exchange_num,tbss.location
							FROM tbl_battery_swap_station tbss
							where tbss.status <> 'D' 
							{$whereStr} {$sql_orderby} 
							LIMIT {$startRow} , {$offset}" ;
		}
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	public function search_all( $whereStr = "")
	{
		$return = array();

		$fn = get_fetch_class_random();
 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}


		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tbss.bss_id like '%{$search_txt}%'";
		}

		//搜寻对应
		$setAlias = array(
			"lblr.leave_date"	=> "Y"
		);		

		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tbss', $setAlias);
		else $whereStr .= "" ;

		if(!empty( $search)){
			$SQLCmd3 = "SELECT 
							lblr.leave_sb_num as s_num,tbss.bss_id,tbss.location,SUBSTRING(lblr.leave_date,1,10) as log_date,count(lblr.s_num) as exchange_num 
						FROM log_battery_leave_return lblr 
						left join tbl_battery_swap_station tbss ON tbss.s_num = lblr.leave_sb_num 
						where tbss.status <> 'D' {$whereStr} and lblr.leave_date IS NOT NULL 
						 GROUP BY lblr.leave_sb_num
						 ORDER BY count(lblr.s_num) DESC";
		}else{
			$SQLCmd3 = "SELECT bss_id,exchange_num,location FROM tbl_battery_swap_station WHERE status <> 'D' ORDER BY exchange_num DESC";
		}
		$rs3 = $this->db_query($SQLCmd3) ;
		$return[5] = ""; //單一換電站最多電池交換次數：
		$return[6] = ""; //單一換電站最少電池交換次數：
		$return[12] = array();
		if(count($rs3)>0){
			$maxNum = $rs3[0]['exchange_num'];	
			$return[5] = $maxNum; //單一換電站最多電池交換次數：
			$minNum = $rs3[count($rs3)-1]['exchange_num'];	
			$return[6] = $minNum; //單一換電站最少電池交換次數：
			$return[12] = $rs3;
		}

		return $return ;
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
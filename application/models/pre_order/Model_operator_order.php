<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_operator_order extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getoperatorAllCnt()
	{

		$whereStr = "";
		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');

		if($login_side == 0){
			//前台權限，只能看到自己的營運商資料
			$whereStr .= " and s_num = '{$login_to_num}'";
		}

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and s_num = {$to_flag_num}" ;
		}

		$SQLCmd = "select count(*) cnt 
									from tbl_operator 
									where status <> 'D' and top02 = '2' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDealerList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by top.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$whereStr = "";
		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');

		if($login_side == 0){
			//前台權限，只能看到自己的營運商資料
			$whereStr .= " and top.s_num = '{$login_to_num}'";
		}

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and top.s_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT top.*, count(tm.s_num) as card_count
  						FROM tbl_operator top 
						left join tbl_member tm ON tm.to_num = top.s_num and SUBSTR(tm.user_id,1,3) = 'MOD' and tm.status <> 'D'
						where top.status <> 'D' and top.top02 = '2' {$whereStr} 
						group by top.s_num
						{$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	public function getCard( $s_num = ""){
		$SQLCmd = "SELECT tm.*
  						FROM tbl_member tm
						where tm.status <> 'D' and SUBSTR(tm.user_id,1,3) = 'MOD'
						and tm.lease_type = 1
						and tm.to_num = {$s_num} " ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs;
	}


	/**
	 * 取得单一 operator_order info
	 */
	public function getoperatorInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_operator_order where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_operator_order", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 新增 tbl_operator_order
	 */
	public function insertData()
	{
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");

		$to_num = $this->input->post("to_num");
		$top08 = $this->input->post("top08");
		$card_num = $this->input->post("card_num");
		$lease_type = $this->input->post("lease_type");
		$lease_price = $this->input->post("lease_price");

		//平均分配次數
		//$aver_num = floor($top08/$card_num);
		//$remainder_num = $top08%$card_num;

		//檢查MOD開頭的卡片
		$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD'");

		//檢查營運商的代號
		$oper_arr = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$oper_code = $oper_arr[0]['top09'];
		$old_top08 = $oper_arr[0]['top08'];

		$title = 'MOD'.$oper_code;
		$sub_title = date("YmdHis");
		$title_len = strlen($title);

		/**
		 * 新增资料库
		 */
		//產出qrcode
		$qrcode_arr = array();
		$start_num = 1;
		for($i=1; $i<=$card_num; $i++){
			$tmp_qrocde = $title.$sub_title;
			$len = strlen($tmp_qrocde);
			$sub_qrocde = $sub_title.str_pad($start_num,(32-$len),"0",STR_PAD_LEFT);

			$md5_qrcode = md5($sub_qrocde);
			$qrcode = $title.substr($md5_qrcode,0,(32-$title_len));

			if(!in_array($qrcode,$mod_arr)){
				//$qrcode_arr[$qrcode] = $aver_num;
				$qrcode_arr[$qrcode] = 0;
			}else{
				$i--;
			}
			$start_num++;
		}

		//if($remainder_num > 0){
		//	foreach($qrcode_arr as $key=>$arr){
		//		if($remainder_num > 0){
		//			$qrcode_arr[$key]++;
		//			$remainder_num--;
		//		}else{
		//			break;
		//		}
		//	}
		//}
		
		$dataArr = array();
		$dataArr['to_num'] = $to_num;
		$dataArr['add_num'] = $top08;
		$dataArr['create_user'] = $this->session->userdata('user_sn');
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_ip'] = $this->input->ip_address();


		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "log_pre_order", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			//回寫回營運商次數加總
			$up_arr = array();
			$up_arr['top08'] = $old_top08 + $top08;
			$insert['update_user'] = $this->session->userdata('user_sn');
			$insert['update_date'] = "now()";
			$insert['update_ip'] = $this->input->ip_address();
			$upWhereStr = " s_num = '{$to_num}'";
			$this->db_update( "tbl_operator", $up_arr, $upWhereStr);
			//纪录修改资料
			$this->session->set_userdata("desc", $up_arr);
			$target_key = '[s_num: '.$to_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			//寫入卡片記錄
			$n=1;
			foreach($qrcode_arr as $key=>$arr){
				//改從post來的值
				$arr = $this->input->post("card_num_".$n);
				$name = $this->input->post("name_".$n);
				$insert = array();
				$insert['member_type'] = 'B';
				$insert['to_num'] = $to_num;
				$insert['name'] = ($name == '')?$title.str_pad($n,strlen($card_num),"0",STR_PAD_LEFT):$name;
				$insert['user_id'] = $key;
				$insert['lease_type'] = $lease_type;
				$insert['lease_price'] = (($lease_type == 2)?$lease_price:NULL);
				if($lease_type == 1){
					//预购
					$insert['lave_num'] = $arr;
					$insert['pre_order_num'] = $arr;
				}

				$insert['status'] = 'Y';
				$insert['create_user'] = $this->session->userdata('user_sn');
				$insert['create_date'] = $nowDate;
				$insert['create_ip'] = $this->input->ip_address();

				if($this->db_insert( "tbl_member", $insert )){
					//预购才需写log
					if($lease_type == 1){
						$SQLCmd = "SELECT s_num FROM tbl_member WHERE create_date = '{$nowDate}' AND create_user = {$this->session->userdata('user_sn')}";
						$rs = $this->db_query($SQLCmd);
						$tm_num = $rs[0]['s_num'];
						$insert_card = array();
						$insert_card['to_num'] = $to_num;
						$insert_card['tm_num'] = $tm_num;
						$insert_card['add_num'] = $arr;
						$insert_card['create_user'] = $this->session->userdata('user_sn');
						$insert_card['create_date'] = "now()";
						$insert_card['create_ip'] = $this->input->ip_address();

						$this->db_insert( "log_card_pre_order", $insert_card );
					}
				}
				$n++;
			}
			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	//批次新增卡片
	public function insertCardData(){
		date_default_timezone_set("Asia/Taipei");

		$to_num = $this->input->post("to_num");
		$card_num = $this->input->post("card_num");
		$lease_type = $this->input->post("lease_type");
		$lease_price = $this->input->post("lease_price");

		//平均分配次數
		$aver_num = 0;
		$remainder_num = 0;

		//檢查MOD開頭的卡片
		$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD'");

		//檢查營運商的代號
		$oper_arr = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$oper_code = $oper_arr[0]['top09'];

		$title = 'MOD'.$oper_code;
		$sub_title = date("YmdHis");
		$title_len = strlen($title);

		/**
		 * 新增资料库
		 */
		//產出qrcode
		$qrcode_arr = array();
		$start_num = 1;
		for($i=1; $i<=$card_num; $i++){
			$tmp_qrocde = $title.$sub_title;
			$len = strlen($tmp_qrocde);
			$sub_qrocde = $sub_title.str_pad($start_num,(32-$len),"0",STR_PAD_LEFT);

			$md5_qrcode = md5($sub_qrocde);
			$qrcode = $title.substr($md5_qrcode,0,(32-$title_len));

			if(!in_array($qrcode,$mod_arr)){
				$qrcode_arr[$qrcode] = $aver_num;
			}else{
				$i--;
			}
			$start_num++;
		}

		if($remainder_num > 0){
			foreach($qrcode_arr as $key=>$arr){
				if($remainder_num > 0){
					$qrcode_arr[$key]++;
					$remainder_num--;
				}else{
					break;
				}
			}
		}

		//寫入卡片記錄
		$n=1;
		foreach($qrcode_arr as $key=>$arr){
			$insert = array();
			$insert['member_type'] = 'B';
			$insert['to_num'] = $to_num;
			$insert['name'] = $title.str_pad($n,strlen($card_num),"0",STR_PAD_LEFT);
			$insert['user_id'] = $key;
			$insert['lease_type'] = $lease_type;
			$insert['lease_price'] = (($lease_type == 2)?$lease_price:NULL);
			if($lease_type == 1){
				//预购
				$insert['lave_num'] = $arr;
				$insert['pre_order_num'] = $arr;
			}

			$insert['status'] = 'Y';
			$insert['create_user'] = $this->session->userdata('user_sn');
			$insert['create_date'] = "now()";
			$insert['create_ip'] = $this->input->ip_address();

			$this->db_insert( "tbl_member", $insert );
			$n++;
		}
		$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
	}
	/**
	 * 修改 tbl_operator_order
	 */
	public function updateData()
	{
		date_default_timezone_set("Asia/Taipei");
		$nowDate = date("Y-m-d H:i:s");
		$to_num = $this->input->post("s_num");
		$top08 = $this->input->post("top08");
		$card_num = $this->input->post("card_num");
		$old_card_num = $this->input->post("old_card_num");

		//檢查營運商的代號
		$oper_arr = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$oper_code = $oper_arr[0]['top09'];
		$old_top08 = $oper_arr[0]['top08'];

		//撈出舊卡片
		$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD' and to_num = '{$to_num}' and lease_type = 1");
		$qrcode_arr = array();
		if($card_num > $old_card_num){
			//有新增卡片
			//檢查MOD開頭的卡片
			$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD'");

			$title = 'MOD'.$oper_code;
			$sub_title = date("YmdHis");
			$title_len = strlen($title);

			/**
			 * 新增资料库
			 */
			//產出qrcode
			$start_num = 1;
			for($i=1; $i<=($card_num - $old_card_num); $i++){
				$tmp_qrocde = $title.$sub_title;
				$len = strlen($tmp_qrocde);
				$sub_qrocde = $sub_title.str_pad($start_num,(32-$len),"0",STR_PAD_LEFT);

				$md5_qrcode = md5($sub_qrocde);
				$qrcode = $title.substr($md5_qrcode,0,(32-$title_len));

				if(!in_array($qrcode,$mod_arr)){
					$qrcode_arr[$start_num] = $qrcode;
				}else{
					$i--;
				}
				$start_num++;
			}
		}


		if($card_num > 0){
			$before_total_lave_num = 0;
			foreach($mod_arr as $qrcode => $arr){
				$before_total_lave_num += $arr['lave_num'];
			}

			$dataArr = array();
			$dataArr['to_num'] = $to_num;
			$dataArr['add_num'] = $top08;
			$dataArr['before_total_lave_num'] = $before_total_lave_num;
			$dataArr['create_user'] = $this->session->userdata('user_sn');
			$dataArr['create_date'] = $nowDate;
			$dataArr['create_ip'] = $this->input->ip_address();
			// $this->db_insert( tableName, 新增的栏位与资料 );
			$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
			$this->session->set_userdata("sql_start", true); 
			if ( $this->db_insert( "log_pre_order", $dataArr ) ) {
				//新增纪录档
				$target_key = '';
				$this->session->set_userdata("desc", $dataArr);
				$this->model_background->log_operating_insert('1', $target_key);
				
				//回寫回營運商次數加總
				$up_arr = array();
				$up_arr['top08'] = $old_top08 + $top08;
				$insert['update_user'] = $this->session->userdata('user_sn');
				$insert['update_date'] = "now()";
				$insert['update_ip'] = $this->input->ip_address();
				$upWhereStr = " s_num = '{$to_num}'";
				$this->db_update( "tbl_operator", $up_arr, $upWhereStr);
				
				//纪录修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key);

				//更新卡片記錄
				$s=1;
				for($i=1; $i<=$card_num; $i++){
					$member_num = $this->input->post("tm_num_".$i);
					$key = $this->input->post("user_id_".$i);
					$name = $this->input->post("name_".$i);
					$arr = $this->input->post("card_num_".$i);
					if($member_num != ''){
						//修改
						$m_whereStr = " s_num = '{$member_num}'";
						$up_member = array();
						if($name != ''){
							$up_member['name'] = $name;
						}
						$up_member['lave_num'] = $mod_arr[$key]['lave_num'] + $arr;
						$up_member['pre_order_num'] = $mod_arr[$key]['pre_order_num'] + $arr;
						$up_member['update_user'] = $this->session->userdata('user_sn');
						$up_member['update_date'] = $nowDate;
						$up_member['update_ip'] = $this->input->ip_address();
						$this->db_update( "tbl_member", $up_member, $m_whereStr);
						//纪录修改资料
						$this->session->set_userdata("desc", $up_member);
						$target_key = '[s_num: '.$member_num.']';
						$this->model_background->log_operating_insert('2', $target_key);

						//卡片明细
						$insert_card = array();
						$insert_card['to_num'] = $to_num;
						$insert_card['tm_num'] = $member_num;
						$insert_card['before_total_lave_num'] = $mod_arr[$key]['lave_num'];
						$insert_card['add_num'] = $arr;
						$insert_card['create_user'] = $this->session->userdata('user_sn');
						$insert_card['create_date'] = $nowDate;
						$insert_card['create_ip'] = $this->input->ip_address();

						$this->db_insert( "log_card_pre_order", $insert_card );
					}else{
						//新增
						$insert = array();
						$insert['member_type'] = 'B';
						$insert['to_num'] = $to_num;
						$insert['name'] = ($name == '')?$title.str_pad($n,strlen($card_num),"0",STR_PAD_LEFT):$name;
						$insert['user_id'] = $qrcode_arr[$s];
						$insert['lease_type'] = 1;
						//预购
						$insert['lave_num'] = $arr;
						$insert['pre_order_num'] = $arr;

						$insert['status'] = 'Y';
						$insert['create_user'] = $this->session->userdata('user_sn');
						$insert['create_date'] = $nowDate;
						$insert['create_ip'] = $this->input->ip_address();

						if($this->db_insert( "tbl_member", $insert )){
							//预购才需写log
							$SQLCmd = "SELECT s_num FROM tbl_member WHERE create_date = '{$nowDate}' AND create_user = {$this->session->userdata('user_sn')}";
							$rs = $this->db_query($SQLCmd);
							$tm_num = $rs[0]['s_num'];
							$insert_card = array();
							$insert_card['to_num'] = $to_num;
							$insert_card['tm_num'] = $tm_num;
							$insert_card['add_num'] = $arr;
							$insert_card['create_user'] = $this->session->userdata('user_sn');
							$insert_card['create_date'] = "now()";
							$insert_card['create_ip'] = $this->input->ip_address();

							$this->db_insert( "log_card_pre_order", $insert_card );
						}
						$s++;
					}
				}
				$this->redirect_alert("./index", $this->lang->line('edit_successfully')) ;
			} else {
				//失败修改资料
				$this->session->set_userdata("desc", $dataArr);
				$target_key = '[s_num: '.$s_num.']';
				$this->model_background->log_operating_insert('2', $target_key, false);
				
				$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
			}
		}else{
			//回寫回營運商次數加總
			$up_arr = array();
			$up_arr['top08'] = $old_top08 + $top08;
			$insert['update_user'] = $this->session->userdata('user_sn');
			$insert['update_date'] = "now()";
			$insert['update_ip'] = $this->input->ip_address();
			$upWhereStr = " s_num = '{$to_num}'";
			if($this->db_update( "tbl_operator", $up_arr, $upWhereStr)){
				//纪录修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key);

				$this->redirect_alert("./index", $this->lang->line('edit_successfully')) ;
			} else {
				//失败修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key, false);
				
				$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
			}
		}
	}


	/**
	 * 修改 tbl_operator_order
	 */
	public function updateData_20180804()
	{
		date_default_timezone_set("Asia/Taipei");

		$to_num = $this->input->post("s_num");
		$top08 = $this->input->post("top08");
		//加购 lease_type = 1

		//檢查MOD開頭的卡片
		$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD' and to_num = '{$to_num}' and lease_type = 1");
		$card_num = count($mod_arr);

		//檢查營運商目前次数
		$oper_arr = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$old_top08 = $oper_arr[0]['top08'];

		if($card_num > 0){
			//平均分配次數
			$aver_num = floor($top08/$card_num);
			$remainder_num = $top08%$card_num;

			$qrcode_arr = array();
			$before_total_lave_num = 0;
			foreach($mod_arr as $qrcode => $arr){
				$qrcode_arr[$qrcode] = $aver_num;
				$before_total_lave_num += $arr['lave_num'];
			}

			if($remainder_num > 0){
				foreach($qrcode_arr as $key=>$arr){
					if($remainder_num > 0){
						$qrcode_arr[$key]++;
						$remainder_num--;
					}else{
						break;
					}
				}
			}

			$dataArr = array();
			$dataArr['to_num'] = $to_num;
			$dataArr['add_num'] = $top08;
			$dataArr['before_total_lave_num'] = $before_total_lave_num;
			$dataArr['create_user'] = $this->session->userdata('user_sn');
			$dataArr['create_date'] = "now()";
			$dataArr['create_ip'] = $this->input->ip_address();
			// $this->db_insert( tableName, 新增的栏位与资料 );
			$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
			$this->session->set_userdata("sql_start", true); 
			if ( $this->db_insert( "log_pre_order", $dataArr ) ) {
				//新增纪录档
				$target_key = '';
				$this->session->set_userdata("desc", $dataArr);
				$this->model_background->log_operating_insert('1', $target_key);
				
				//回寫回營運商次數加總
				$up_arr = array();
				$up_arr['top08'] = $old_top08 + $top08;
				$insert['update_user'] = $this->session->userdata('user_sn');
				$insert['update_date'] = "now()";
				$insert['update_ip'] = $this->input->ip_address();
				$upWhereStr = " s_num = '{$to_num}'";
				$this->db_update( "tbl_operator", $up_arr, $upWhereStr);
				
				//纪录修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key);

				//更新卡片記錄
				$n=1;
				foreach($qrcode_arr as $key=>$arr){
					$member_num = $mod_arr[$key]['s_num'];
					$m_whereStr = " s_num = '{$member_num}'";
					$up_member = array();
					$up_member['lave_num'] = $mod_arr[$key]['lave_num'] + $arr;
					$up_member['pre_order_num'] = $mod_arr[$key]['pre_order_num'] + $arr;
					$up_member['update_user'] = $this->session->userdata('user_sn');
					$up_member['update_date'] = "now()";
					$up_member['update_ip'] = $this->input->ip_address();
					$this->db_update( "tbl_member", $up_member, $m_whereStr);
					//纪录修改资料
					$this->session->set_userdata("desc", $up_member);
					$target_key = '[s_num: '.$member_num.']';
					$this->model_background->log_operating_insert('2', $target_key);

					//卡片明细
					$insert_card = array();
					$insert_card['to_num'] = $to_num;
					$insert_card['tm_num'] = $member_num;
					$insert_card['before_total_lave_num'] = $mod_arr[$key]['lave_num'];
					$insert_card['add_num'] = $arr;
					$insert_card['create_user'] = $this->session->userdata('user_sn');
					$insert_card['create_date'] = "now()";
					$insert_card['create_ip'] = $this->input->ip_address();

					$this->db_insert( "log_card_pre_order", $insert_card );
				}
				$this->redirect_alert("./index", $this->lang->line('edit_successfully')) ;
			} else {
				//失败修改资料
				$this->session->set_userdata("desc", $dataArr);
				$target_key = '[s_num: '.$s_num.']';
				$this->model_background->log_operating_insert('2', $target_key, false);
				
				$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
			}
		}else{
			//回寫回營運商次數加總
			$up_arr = array();
			$up_arr['top08'] = $old_top08 + $top08;
			$insert['update_user'] = $this->session->userdata('user_sn');
			$insert['update_date'] = "now()";
			$insert['update_ip'] = $this->input->ip_address();
			$upWhereStr = " s_num = '{$to_num}'";
			if($this->db_update( "tbl_operator", $up_arr, $upWhereStr)){
				//纪录修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key);

				$this->redirect_alert("./index", $this->lang->line('edit_successfully')) ;
			} else {
				//失败修改资料
				$this->session->set_userdata("desc", $up_arr);
				$target_key = '[s_num: '.$to_num.']';
				$this->model_background->log_operating_insert('2', $target_key, false);
				
				$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
			}
		}
	}

	public function getDetailAllCnt( $to_num = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		$whereStr = "";
		/*$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and lpo.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		/*$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;*/

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd  = "SELECT count(*) cnt
								FROM log_pre_order lpo 
								where lpo.to_num = '{$to_num}' ".$whereStr;
	
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $to_num = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr  = '';

		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and lpo.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		/*$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;*/
		
		//排序动作
		$sql_orderby = " order by lpo.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

		$SQLCmd  = "SELECT *
								FROM log_pre_order lpo 
								where lpo.to_num = '{$to_num}' 
								".$whereStr."
								{$sql_orderby} 
								LIMIT {$startRow} , {$offset} ";
	
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}





}
/* End of file model_operator_order.php */
/* Location: ./application/models/model_operator_order.php */
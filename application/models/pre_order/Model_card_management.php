<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_card_management extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getSuboperatorAllCnt()
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');

		if($login_side == 0){
			//前台權限，只能看到自己的營運商資料
			$whereStr .= " and top.s_num = '{$login_to_num}'";
		}

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and top.s_num = {$to_flag_num}" ;
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tm', $setAlias);
		else $whereStr .= "" ;

		$SQLCmd = "SELECT  count(*) cnt 
  						FROM tbl_member tm
						left join tbl_operator top ON top.s_num = tm.to_num
						where tm.status <> 'D' and SUBSTR(tm.user_id,1,3) = 'MOD'
						{$whereStr}" ;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getSubDealerList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$login_side = $this->session->userdata('login_side');
		$login_to_num = $this->session->userdata('to_num');

		if($login_side == 0){
			//前台權限，只能看到自己的營運商資料
			$whereStr .= " and top.s_num = '{$login_to_num}'";
		}

 		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and top.s_num = {$to_flag_num}" ;
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"top.top01"			=>  "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'tm', $setAlias);
		else $whereStr .= "" ;

		//排序动作
		$sql_orderby = " order by tm.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$SQLCmd = "SELECT tm.*, top.top01
  						FROM tbl_member tm
						left join tbl_operator top ON top.s_num = tm.to_num
						where tm.status <> 'D' and SUBSTR(tm.user_id,1,3) = 'MOD' 
						{$whereStr}
						{$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 card_management info
	 */
	public function getCardMember( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_member where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_card_management", $whereArr ) ;
		return $rs ;
	}
	
	/**
	 * 新增 tbl_card_management
	 */
	public function insertData()
	{
		date_default_timezone_set("Asia/Taipei");

		$to_num = $this->input->post("to_num");
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		//檢查MOD開頭的卡片
		$mod_arr = $this->Model_show_list->getMemberDom(" and SUBSTR(user_id,1,3) = 'MOD'");

		//檢查營運商的代號
		$oper_arr = $this->Model_show_list->getoperatorList(" and s_num = '{$to_num}'");
		$oper_code = $oper_arr[0]['top09'];

		$title = 'MOD'.$oper_code;
		$sub_title = date("YmdHis");
		$title_len = strlen($title);

		/**
		 * 新增资料库
		 */

		//產出qrcode
		$qrcode_arr = array();
		$start_num = 1;
		$user_id = '';
		for($i=1; $i<=1; $i++){
			$tmp_qrocde = $title.$sub_title;
			$len = strlen($tmp_qrocde);
			$sub_qrocde = $sub_title.str_pad($start_num,(32-$len),"0",STR_PAD_LEFT);

			$md5_qrcode = md5($sub_qrocde);
			$qrcode = $title.substr($md5_qrcode,0,(32-$title_len));

			if(!in_array($qrcode,$mod_arr)){
				$user_id = $qrcode;
			}else{
				$i--;
			}
			$start_num++;
		}

		$insert = $dataArr;
		$insert['member_type'] = 'B';
		$insert['to_num'] = $to_num;
		$insert['user_id'] = $user_id;
		$insert['status'] = 'Y';
		$insert['create_user'] = $this->session->userdata('user_sn');
		$insert['create_date'] = "now()";
		$insert['create_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if($this->db_insert( "tbl_member", $insert )){
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $insert);
			$this->model_background->log_operating_insert('1', $target_key);
			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_card_management
	 */
	public function updateData()
	{
		date_default_timezone_set("Asia/Taipei");

		$s_num = $this->input->post("s_num");
		$to_num = $this->input->post("to_num");
		$name = $this->input->post("name");
		
		$colDataArr = array();
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$colDataArr[$key] = $value;
					break;
			}
		}

		$whereStr = "s_num = '{$s_num}'";
		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['update_date'] = "now()";
		$colDataArr['update_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_member", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}

	/**
	 * 删除 tbl_member
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_member", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}
	public function getDetailAllCnt( $tm_num = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		$whereStr = "";
		/*$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and lpo.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		/*$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;*/

		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd  = "SELECT count(*) cnt
								FROM log_card_pre_order lcpo  
								where lcpo.tm_num = '{$tm_num}' ".$whereStr;
	
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $tm_num = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr  = '';

		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and lpo.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		/*$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;*/
		
		//排序动作
		$sql_orderby = " order by lcpo.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}

	    
		$SQLCmd = "SELECT lcpo.*, top.top01
  						FROM log_card_pre_order lcpo
						left join tbl_operator top ON top.s_num = lcpo.to_num
						where lcpo.tm_num = '{$tm_num}'  {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	/**
	 * 取得匯出資料
	 */
	public function getexportdata()
	{
		$to_num = $this->input->post("to_num");

		$SQLCmd = "SELECT tbo.top01,IF(tm.lease_type=1,'预购','结算') as lease_type,tm.lease_price,tm.name,tm.user_id,tm.nick_name  
					FROM tbl_operator tbo 
					LEFT JOIN tbl_member tm ON tbo.s_num = tm.to_num 
					WHERE tbo.s_num = {$to_num} AND tm.status <> 'D' and SUBSTR(tm.user_id,1,3) = 'MOD'";
		$rs = $this->db_query($SQLCmd);

		return $rs;

	}	

	public function getleave_return($whereStr){
		$to_num = $this->input->post("to_num");
		$cancel_flag = $this->input->post("cancel_flag");

		//leave_battery_id
		$SQLCmd  = "SELECT lblr.*, tm.name as member_name, tm.user_id, tm.nick_name, tm.lease_type, tm.lease_price, ltop.top01,concat(l_tbss.location, '（',l_tbss.bss_id,'）') as leave_bss_id
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num
								LEFT JOIN tbl_operator ltop ON tm.to_num = ltop.s_num 
								where tm.to_num = {$to_num} and SUBSTR(tm.user_id,1,3) = 'MOD' {$whereStr}
								order by lblr.leave_date, tm.s_num";
		//echo $SQLCmd;
		$rs = $this->db_query($SQLCmd) ;
		$list_arr = array();
		$total_count[1] = 0;
		$total_count[2] = 0;
		$total_money[1] = 0;
		$total_money[2] = 0;
		$lblr_sn = array();
		if(count($rs)>0){
			$n=0;
			//把資訊整理在一起
			//1. 帳號ID 2.交易的電池序號  3.時間 4.站點    
			//累積交易次數
			//單次交易金額
			//總付款的金額
			foreach($rs as $rs_arr){
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['top01'] = $rs_arr['top01'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['member_name'] = $rs_arr['member_name'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['leave_bss_id'] = $rs_arr['leave_bss_id'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['battery_id'] = $rs_arr['leave_battery_id'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['system_log_date'] = $rs_arr['system_log_date'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['lease_price'] = $rs_arr['lease_price'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['lease_type'] = $rs_arr['lease_type'];
				$list_arr[$rs_arr['lease_type']][$rs_arr['s_num']][$n]['user_id'] = $rs_arr['user_id'];

				$total_count[$rs_arr['lease_type']]++;
				if($rs_arr['lease_type'] == '2'){
					$lblr_sn[] = $rs_arr['s_num'];
					$total_money[$rs_arr['lease_type']] += (1 * $rs_arr['lease_price']);
				}
				$n++;
			}
		}

		$return = array();
		$return[0] = $list_arr;
		$return[1] = $total_count;
		$return[2] = $total_money;
		$return[3] = $lblr_sn;
		return $return;
	}

	public function add_cancel_flag($lblr_sn){
		$UpIdStr = join( "','", $lblr_sn );
		$UpIdStr = "'".$UpIdStr."'";
		
		$whereStr = " s_num in ({$UpIdStr}) ";
		$dataArr = array (
			"cancel_flag"			=> "Y",
			"cancel_date"			=> "now()",
			"cancel_user"			=> $this->session->userdata('user_sn')
		) ;

		$this->db_update( "log_battery_leave_return", $dataArr, $whereStr );
	
	}

}
/* End of file model_card_management.php */
/* Location: ./application/models/model_card_management.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_check_battery_report extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function check_battery_report()
	{
		$SQLCmd = "SELECT config_set FROM sys_config WHERE config_code = 'battery_not_report_hour'";
		$rs = $this->db_query($SQLCmd);

		if($rs)
		{
			$check_hour = $rs[0]['config_set'];
			$SQLCmd = "SELECT battery_id FROM tbl_battery WHERE status <> 'D'";
			$rs = $this->db_query($SQLCmd);
			if($rs)
			{
				$SQLCmdS = "SELECT config_set FROM sys_config WHERE config_code = 'battery_not_report_hour'";
				$rsS = $this->db_query($SQLCmdS);
				if($rsS)
				{
					foreach ($rs as $key => $value) {
						$SQLCmdL = "SELECT ldi.unit_id,ldi.system_log_date FROM (SELECT unit_id,MAX(system_log_date) as system_log_date FROM log_driving_info WHERE unit_id = '{$value['battery_id']}') ldi WHERE TIMESTAMPDIFF(HOUR,ldi.system_log_date,NOW())>{$rsS[0]['config_set']} ORDER BY ldi.system_log_date DESC limit 1";
						$rsL = $this->db_query($SQLCmdL);
						if($rsL)
						{
							//若是DB中此電池沒有告警通知則寫入告警通知
							$SQLCmdW = "SELECT count(*) cnt FROM log_battery_warring WHERE battery_id = '{$rsL[0]['unit_id']}' AND status !=3 AND type=2";
							$rdsW = $this->db_query($SQLCmdW);
 
							if($rdsW[0]['cnt']==0)
							{
								$dataArrL = array();
								$dataArrL['log_date'] = $rsL[0]['system_log_date'];
								$dataArrL['battery_id'] = $rsL[0]['unit_id'];
								$dataArrL['type'] = 2;//狀態為此電池已經超過N小時未回報(2)
								$dataArrL['status'] = 1;//處理狀態預設為未檢視
								$this->db_insert("log_battery_warring",$dataArrL);
								$body = "{$rsL[0]['unit_id']} 此電池已經超過{$rsS[0]['config_set']}小時未回報";
							}
						}
					}
				}
				else
				{
					echo "找不到設定參數";
				}
				
			}
			else
			{
				echo "電池管理為空";
			}

		}

		$SQLCmd2 = "SELECT config_set FROM sys_config WHERE config_code = 'battery_not_in_bss_day'";
		$rs2 = $this->db_query($SQLCmd2);
		if($rs2)
		{
			$check_day = $rs2[0]['config_set'];
			$SQLCmdB = "SELECT battery_id,last_time_in_bss FROM tbl_battery WHERE status <> 'D' AND last_time_in_bss IS NOT NULL AND TIMESTAMPDIFF(DAY,last_time_in_bss,NOW())>{$rs2[0]['config_set']}";
			$rsB = $this->db_query($SQLCmdB);
			if($rsB)
			{
				foreach($rsB as $keyB => $valueB)
				{
					//若是DB中此電池沒有告警通知則寫入告警通知
					$SQLCmdW = "SELECT count(*) cnt FROM log_battery_warring WHERE battery_id = '{$rsB[0]['battery_id']}' AND status !=3 AND type=3";
					$rdsW = $this->db_query($SQLCmdW);

					if($rdsW[0]['cnt']==0)
					{
						$dataArrE = array();
						$dataArrE['log_date'] = $rsB[0]['last_time_in_bss'];
						$dataArrE['battery_id'] = $rsB[0]['battery_id'];
						$dataArrE['type'] = 3;//狀態為此電池已經超過N天未回租借站
						$dataArrE['status'] = 1;//處理狀態預設為未檢視
						$this->db_insert("log_battery_warring",$dataArrE);
						$body = "{$rsB[0]['battery_id']} 此電池已經超過{$rs2[0]['config_set']}天未回租借站";
					}
				}
				
			}
			
		}
		else
		{
			echo "找不到設定參數";
		}
	}

}

/* End of file Model_check_battery_report.php */

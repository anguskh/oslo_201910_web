<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_showad extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getConfigAllCnt()
	{
		$SQLCmd = "select count(*) cnt from sys_config where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	public function getfileurl($ad_num="")
	{
		$imgurlArr = array();
		if($ad_num!="")
		{
			//取得廣告圖片網址
			$SQLCmd = "SELECT IF(advertising_type=1,advertising_upload_img,advertising_upload_video) as fileStr,advertising_type FROM tbl_bss_advertising WHERE s_num = {$ad_num}";
			$rs = $this->db_query($SQLCmd);
			$imgurlArr = $rs;
			// if($rs)
			// {
			// 	$imgnameArr = explode(",",$rs[0]['fileStr']);
			// 	foreach($imgnameArr as $key => $value)
			// 	{
			// 		$dataArr[$key] = $ad_path."/".$value;
			// 	}
			// }
		}
		return $imgurlArr;
	}

	public function getadpath($ad_num="")
	{
		//取得廣告的路徑參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='advertising_file_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$web = $this->config->item('base_url');
			$ad_path = $web.substr($rsC[0]['config_set'], 2)."ad_{$ad_num}";
			return $ad_path;
		}
		else
		{
			return "";
		}
	}
}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
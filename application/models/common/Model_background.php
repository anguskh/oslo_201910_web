<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_background extends MY_Model {
	/**
	 * 取得某银行的所有专案清单
	 */
	public function check_user_account( $loginUser, $loginPass )
	{
		
		$SQLCmd  = "SELECT 
					su.user_sn, 
					su.user_id, 
					su.to_num, 
					su.tso_num, 
					su.default_language, 
					su.login_force_pwd, 
					su.pwd_deadline, 
					su.status, 
					su.login_retry_cnt, 
					su.user_name,  
					sg.group_sn, 
					sg.login_side,
					sg.to_flag,
					sg.card_flag,
					su.login_now,
					su.login_b_ip,
					su.view_area 
					FROM sys_user su 
					left join sys_user_group sug 
						on sug.user_sn = su.user_sn
					left join sys_group sg 
						on sug.group_sn = sg.group_sn 
					WHERE BINARY su.user_id = '{$loginUser}' 
						AND su.pwd = '{$loginPass}' 
						AND (su.status = '1' or su.status = '0') ";
		$rs = $this->db_query($SQLCmd) ;
		// echo $SQLCmd;
		return $rs ;
	}
	
	public function get_user_access($user_sn = ""){
		if($user_sn != ""){
			$SQLCmd = "	select sm.menu_directory, ifnull(suma.access_control, smg.access_control) as access_control
						from sys_user_group sug 
						left join sys_menu_group smg 
							on sug.group_sn = smg.group_sn 
						left join sys_menu sm 
							on smg.menu_sn = sm.menu_sn 
						left join sys_user_menu_access suma 
							on smg.menu_sn = suma.menu_sn 
							and sug.user_sn = suma.user_sn 
						where sug.user_sn = {$user_sn} ";
			$rs = $this->db_query($SQLCmd) ;
			return $rs ;
		}else{
			return false;
		}
	}
	
	//取得登入纪录session
	public function logindata_info($user_sn, $ip_address = ""){
		$where = "";
		if($ip_address != ""){
			$where = " and ip_address = '{$ip_address}'";
		}
		$SQLCmd = " select * from log_sessions 
					where user_sn = {$user_sn}".$where;
		$rs = $this->db_query($SQLCmd) ;
		return $rs;
	}
	
	public function record_logindata($user_sn)
	{
		$dataArr['session_id'] = $user_sn;
		$dataArr['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$dataArr['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$dataArr['last_activity'] = time();
		$dataArr['user_sn'] = $user_sn;
		$this->db_insert( "log_sessions", $dataArr );
		
		//取得新产生的号码, 并写入session, 提供验证
		$rs = $this->logindata_info($user_sn, $dataArr['ip_address']);
		if(count($rs) != 0){
			$session_sn = $rs[0]['session_sn'];
			//提供controller验证使用
			$this->session->set_userdata('session_sn', $session_sn);
			$this->session->set_userdata('last_activity', $dataArr['last_activity']);
			return $session_sn;
		}else{
			return 0;
		}
	}
	
	//更新登入纪录session 时间
	public function update_logindata($user_sn = "", $session_sn = "")
	{
		if($user_sn != ""){	//有传入资料
			$where = "user_sn = {$user_sn}";
		}else{	//无传入资料
			$where = "user_sn = {$this->session->userdata('user_sn')}";
		}
		
		$where = "user_sn = {$this->session->userdata('user_sn')}";
		$dataArr['last_activity'] = time();
		$this->db_update( "log_sessions", $dataArr ,$where);
		
		//将号码, 并写入session, 提供验证, 避免未登出的情况发生未登记至session
		$this->session->set_userdata('session_sn', $session_sn);
		$this->session->set_userdata('last_activity', $dataArr['last_activity']);
	}
	
	//删除登入纪录session
	public function destroy_logindata($user_sn)
	{
		$SQLCmd  = "DELETE FROM log_sessions 
					WHERE user_sn = {$user_sn} " ;
		$rs = $this->db_query($SQLCmd) ;
	}
	
	//检查登入session
	//检查是否为同IP登入,
	//一样则更新登入时间
	//不同则剔除前IP登入
	public function check_logindata($user_sn){
		$retuen = 0;
		//取得目前IP
		//$ip = $_SERVER['REMOTE_ADDR'];
		$ip = $this->get_client_ip();
		$SQLCmd = " select session_sn from log_sessions 
					where user_sn = {$user_sn} and ip_address = '{$ip}'";
		$rs = $this->db_query($SQLCmd) ;
		
		//print_r($rs);
		
		if(count($rs) == 0){ //找不到同IP, 清空 user_sn, 并新增
			$this->destroy_logindata($user_sn);
			$retuen = $this->record_logindata($user_sn);
		}else{
			$this->update_logindata($user_sn, $rs[0]['session_sn']);
			$retuen = $rs[0]['session_sn'];
		}
		return $retuen;
	}
	
	//取得真实IP
	public function get_client_ip(){
		foreach (array(
					'HTTP_CLIENT_IP',
					'HTTP_X_FORWARDED_FOR',
					'HTTP_X_FORWARDED',
					'HTTP_X_CLUSTER_CLIENT_IP',
					'HTTP_FORWARDED_FOR',
					'HTTP_FORWARDED',
					'REMOTE_ADDR') as $key) {
			if (array_key_exists($key, $_SERVER)) {
				foreach (explode(',', $_SERVER[$key]) as $ip) {
					$ip = trim($ip);
					//FILTER_FLAG_NO_PRIV_RANGE 私有ip 回传 fasle
					//FILTER_FLAG_NO_RES_RANGE 保留ip 回传false
					// if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
									// FILTER_FLAG_IPV4 |
									// FILTER_FLAG_NO_PRIV_RANGE |
									// FILTER_FLAG_NO_RES_RANGE)) {
					if ((bool) filter_var($ip, FILTER_VALIDATE_IP,
									FILTER_FLAG_IPV4)) {

						return $ip;
					}
				}
			}
		}
		return null;
	}
	
	public function getProjectSelectList()
	{
		$SQLCmd  = "SELECT project_sn, project_name
					FROM tbl_project
					WHERE status <> 'D'
					AND bank_sn = {$bankSN}";
		$rs = $this->db_query($SQLCmd) ;
		$returnStr = "<option value=\"\">-- 请选择 --</option>";
		/*while (!$rs->EOF) {
			$returnStr .= "<option value=\"".$rs->fields["project_sn"]."\">".$rs->fields["project_name"]."</option>";
			$rs->MoveNext();
		}*/

		return $returnStr ;
	}
	
	//取目前使用的资料库名称
	public function getDefDBname(){
		return $this->getDBname();
	}
	
	public function displayLanguage(){
		$SQLCmd = " SELECT config_set FROM sys_config 
					where config_code='display_language' " ;
		$rs = $this->db_query($SQLCmd) ;
		if(count($rs) > 0){	
			return $rs[0]['config_set'];
		}else{	//找不到直接给予预设
			return 'zh_tw';
		}
	}
	
	//端末机是否已初始化, 将查询upload档案状态是否为ok, 为ok就删掉该档案, 和对应的download档案
	//档案格式为upload 为 reader_sn.log 和 download 为 reader_sn.par
	//更新端末机状态
	public function checkInitialized(){
		date_default_timezone_set("Asia/Taipei");
		//测试用
		// $uppath = "./initial/UPLOAD/";
		// $upbakpath = "./initial/UPLOAD/bak/";
		// $dwpath = "./initial/DOWNLOAD/";
		
		//正式用
		$uppath = $this->session->userdata('tms_initial_upload_path');
		$upbakpath = $uppath.'bak/';
		$dwpath = $this->session->userdata('tms_initial_path');
		$upNGpath = $uppath.'NG/';

		if(is_dir($uppath) && is_dir($dwpath)){	//上传和下载资料夹存在
			//搜寻UPLOAD资料夹档案
			$tmp = scandir($uppath);
			$items = array();	//档案名称, 不含附档名
			$itemsNG = array();	//档案名称, 不含附档名
			$itemsFull = array();	//档案名称, 含附档名
			$itemsNGFull = array();	//档案名称, 含附档名
			$itemsMSG = array();  //OK档案内的资讯
			$itemsNGMSG = array();  //NG档案内的资讯
			$itemsTime = array();  //ok档案建立时间
			$itemsNGTime = array();  //NG档案建立时间
			 
			foreach($tmp as $key => $value){
				$ext = pathinfo($uppath.$value, PATHINFO_EXTENSION);
				//比对附档名 log
				if(strtolower($ext) == 'log'){
					$filename = pathinfo($uppath.$value, PATHINFO_BASENAME);
					//纪录reader_sn
					//ok
					$reader_sn = $this->getFileName($filename, 'ok');
					if($reader_sn != false){  //记录为ok的档名
						$items[] = $reader_sn;
						$itemsFull[] = $filename;
						//读取档案内容
						$itemsMSG[] = $this->readFileData($uppath.$value);
						//取得档案时间
						$itemsTime[] = date("Y-m-d H:i:s", filemtime($uppath.$value));
					}
					
					//ng
					$reader_sn = $this->getFileName($filename, 'ng');
					if($reader_sn != false){  //记录为ng的档名
						$itemsNG[] = $reader_sn;
						$itemsNGFull[] = $filename;
						//读取档案内容
						$itemsNGMSG[] = $this->readFileData($uppath.$value);
						//取得档案时间
						$itemsNGTime[] = date("Y-m-d H:i:s", filemtime($uppath.$value));
					}
				}
			}
			
			//将资讯写入资料库
			$this->insertMSG('ok', $items, $itemsMSG, $itemsTime);
			$this->insertMSG('ng', $itemsNG, $itemsNGMSG, $itemsNGTime);
			
			//删除UPLOAD档案
			foreach($itemsFull as $key => $value){
				$unlinkFile = $uppath.$value;
				//档案是否存在
				if(is_file($unlinkFile)){
					if(@!unlink($unlinkFile)){	//删除同reader_sn.par档案
						// echo $value.'删除失败';
						// echo '<br />';
					}else{
						// echo $value.'删除成功';
						// echo '<br />';
					}
				}else{
					// echo $value.'无此档案';
					// echo '<br />';
				}
			}
			
			//移动NG档案至UPLOAD底下的NG/西元年月日
			if(is_dir($upNGpath)){
				//日期路径
				date_default_timezone_set("Asia/Taipei");
				$datePath = date("Ymd");
				$datePath = $upNGpath.$datePath.'/';
				$this->checkdir($datePath);
				if(is_dir($datePath)){
					foreach($itemsNGFull as $key => $value){
						$moveFile = $uppath.$value;
						//$tarFile = $upNGpath.$value; //移动到NG里
						$tarFile = $datePath.$value;
						//档案是否存在
						if(is_file($moveFile)){
							if(@!rename($moveFile, $tarFile)){

							}else{

							}
						}else{

						}
					}
				}
			}
			
			//删除DOWNLOAD档案与更新状态
			foreach($items as $key => $value){
				$unlinkFile = $dwpath.$value.'.par';
				//档案是否存在
				if(is_file($unlinkFile)){
					if(@!unlink($unlinkFile)){	//删除同reader_sn.par档案
						// echo $value.'删除失败';
						// echo '<br />';
					}else{
						// echo $value.'删除成功';
						// echo '<br />';
						
						//更新状态
						$this->updateInitialized($value);
					}
				}else{
					// echo $value.'无此档案';
					// echo '<br />';
				}
			}
			
		}
		clearstatcache();
		
	}
	
	//读取档案资料
	function readFileData($file){
		$data = fopen($file, 'r');
		$contents = '';
		if ($data) {
			while (!feof($data)) {
				$contents .= fgets($data);
			}
			fclose($data);
		}
		return $contents;
	}
	
	//讯息写入资料库
	//[$option] ok, ng, [$items] 读卡机序号, [$itemsMSG] 讯息, [$itemsTime] 档案建立时间
	function insertMSG($option, $items, $itemsMSG, $itemsTime){
		$this->load->model("bank/model_terminal", "model_terminal") ;
		if($option == 'ok'){
			$STATUS = '1';  //成功
		}else{	//ng
			$STATUS = '0';  //失败
		}
		
		//回圈记录资料
		foreach($items as $key => $reader_sn){
			$READER_SN = $reader_sn;
			$MERCHANT_ID = "";
			$TERMINAL_ID = "";
			$INITIALIZE_DATE = $itemsTime[$key];
			$FAILURE_MESSAGE = $itemsMSG[$key];
			$rs = $this->model_terminal->getTerminalInfo_reader($reader_sn);
			if(count($rs) > 0){
				$MERCHANT_ID = $rs[0]['MERCHANT_ID'];
				$TERMINAL_ID = $rs[0]['TERMINAL_ID'];
			}
			
			//存到资料库
			$colDataArr = array(
				"READER_SN"			=> $READER_SN,
				"MERCHANT_ID"		=> $MERCHANT_ID,
				"TERMINAL_ID"		=> $TERMINAL_ID,
				"INITIALIZE_DATE"	=> $INITIALIZE_DATE,
				"FAILURE_MESSAGE"	=> $FAILURE_MESSAGE,
				"STATUS"			=> $STATUS
			) ;
			
			// $this->db_insert( tableName, 新增的栏位与资料 );
			if ( $this->db_insert( "log_reader_initialize", $colDataArr ) ) {
			} else {
			}
		}
	}
	
	//取档案名称, 找出后两码为ok or ng, 再取出reader_sn
	//$filename档名, $checkState要确认档名后两码的字串, 请用小写(如ok 或 ng)
	function getFileName($filename, $checkState){
		$filename = current(explode('.', $filename));
		$state = strtolower(substr($filename, -2));
		if($state == $checkState){
			$reader_sn = current(explode('_', $filename));
			return $reader_sn;
		}else{
			return false;
		}
	}
	
	/**
	 * 修改 t_terminal initialized 为 1
	 */
	public function updateInitialized($reader_sn = ""){
		$whereStr = " READER_SN='{$reader_sn}'" ;
		$colDataArr = array(
			"INITIALIZED"		=> '1',
		) ;
		
		if ( $this->db_update( "t_terminal", $colDataArr, $whereStr ) ) {
		} else {
			
		}	
	}
	
	//纪录历程资料, 目前只针对 电话配对, 密码修改
	public function log_operating_insert($mode = "", $keyvalue = "", $succ_or_fail = true){
		//if($mode != "" && $this->session->userdata("sql_start") == true){
		if($mode != ""){
			$function_name = "";
			$before_desc = $keyvalue;
			if(substr($keyvalue, 0, 4) == 'skip'){
				$before_desc = '';
			}
			$desc = $keyvalue;
			if(substr($keyvalue, 0, 4) == 'skip'){
				$desc = substr($keyvalue, 4);
			}
			
			//语系对照
			$before_desc = $this->setLang($before_desc);
			//语系对照
			$desc = $this->setLang($desc);
			
			$before_desc_arr = array();
			$desc_arr = array();
			switch($mode){
				case '0':	//浏览
					$desc_arr = $this->session->userdata('desc');
					break;
				case '1':	//新增
					$before_desc_arr = $this->session->userdata('before_desc');
					$desc_arr = $this->session->userdata('desc');
					break;
				case '2':	//修改
					$before_desc_arr = $this->session->userdata('before_desc');
					$desc_arr = $this->session->userdata('desc');
					break;
				case '3':	//删除
					$before_desc_arr = $this->session->userdata('before_desc');
					$desc_arr = $this->session->userdata('desc');
					break;
				default:
					break;
			}
			
			//全部记录
			// foreach($before_desc_arr as $key => $value){
				// if($before_desc != ""){
					// $before_desc .= ', ';
				// }
				// $before_desc .= $key.': '.$value;
			// }
			
			// foreach($desc_arr as $key => $value){
				// if($desc != ""){
					// $desc .= ', ';
				// }
				// $desc .= $key.': '.$value;
			// }
			
			//纪录比对前后修改哪些值
			if(is_array($desc_arr)){
				foreach($desc_arr as $key => $value){
					if($mode != 0){  //浏览或查询不用删除
						//避开记录
						if( strtolower($key) == "pwd" || 
							strtolower($key) == "old_pwd" ||
							strtolower($key) == "card_no_masked" ||
							strtolower($key) == "card_no" ||
							strtolower($key) == "last_activity" || 
							strtolower($key) == "login_retry_cnt"
							){
							continue;
						}
					}
					
					//语系对照
					$lang = $this->setLang($key);
					//开始记录
					if( isset($before_desc_arr[strtoupper($key)]) ||
						isset($before_desc_arr[strtolower($key)]) || 
						isset($before_desc_arr[$key]) ){
						
						if(isset($before_desc_arr[strtoupper($key)])){
							$key = strtoupper($key);
						}else if(isset($before_desc_arr[strtolower($key)])){
							$key = strtolower($key);
						}
						
						if($value != $before_desc_arr[$key]){
							if($before_desc != ""){
								//$before_desc .= ',';
								$before_desc .= '<br />';
							}
							$before_desc .= '['.$lang.': '.$before_desc_arr[$key].']';
							
							if($desc != ""){
								//$desc .= ',';
								$desc .= '<br />';
							}
							$value = str_replace('[:;]', ' ', $value);
							$desc .= '['.$lang.': '.$value.']';
						}
					}else{
						if($desc != ""){
							//$desc .= ',';
							$desc .= '<br />';
						}
						if(is_array($value)){
							$value = join(',', $value);
						}
						$value = str_replace('[:;]', ' ', $value);
						$desc .= '['.$lang.': '.$value.']';
					}

				}
			}
			if($succ_or_fail){	//成功或失败
				$status = '1';
			}else{
				//$before_desc = '<font color=red>['.$this->lang->line('failed_action').']</font>'.$before_desc;
				$status = '0';
				$desc = '<font color=red>['.$this->lang->line('failed_action').']</font><br />'.$desc;
			}
			
			//处理SQL指令, 移除掉敏感资料
			$sSQL = $this->session->userdata("sql_cmd");
			//$arr_SQL = explode(',', $sSQL);
			if($sSQL != ""){
				//echo $sSQL;
				if(strtoupper(substr($sSQL, 0, 6)) == 'INSERT'){  //新增
					$arr_SQL = explode(') VALUES (', $sSQL);
					$arr_column_SQL = explode('(', $arr_SQL[0]); //前半段栏位
					$value_SQL = substr($arr_SQL[1], 0, -1);  //后半段栏位值
					
					//前半段处理
					$title_SQL = $arr_column_SQL[0];  //最前面insert or edit
					$arr_column = explode(',', $arr_column_SQL[1]);
					//print_r($arr_column);
					//后半段处理
					$arr_value_SQL = explode(',', $value_SQL);

					//删除栏位
					foreach($arr_column as $key => $value){
						if( stripos($value, "pwd") || 
							stripos($value, "old_pwd") || 
							stripos($value, "card_no_masked") || 
							stripos($value, "card_no") ){
							unset($arr_column[$key]);  //删除栏位
							unset($arr_value_SQL[$key]);  //删除栏位值
						}
					}
					
					//重新组合SQL
					$sSQL = $title_SQL;
					$sSQL .= '('.join(',', $arr_column);
					$sSQL .= ') VALUES ('.join(',', $arr_value_SQL).')';
				}else if(strtoupper(substr($sSQL, 0, 6)) == 'UPDATE'){  //修改
					$arr_SQL = explode(' SET ', $sSQL);
					//前半段
					$title_SQL = $arr_SQL[0];  //最前面set之前
					$arr_tmp_SQL = explode('WHERE', $arr_SQL[1]);  //分两段, 
					$where_SQL = $arr_tmp_SQL[1];  //最后面where之后
					//echo $arr_tmp_SQL[0];
					$arr_tmp_SQL = explode("',", $arr_tmp_SQL[0]);  //中间value部分
					
					//print_r($arr_tmp_SQL);
					
					//删除栏位和资料
					foreach($arr_tmp_SQL as $key => $value){
						if( stripos($value, "pwd") || 
							stripos($value, "old_pwd") || 
							stripos($value, "card_no_masked") || 
							stripos($value, "card_no") ){
							unset($arr_tmp_SQL[$key]);  //删除栏位和值
						}
					}
					
					//print_r($arr_tmp_SQL);
					
					//重新组合SQL
					$sSQL = $title_SQL.' SET ';
					$sSQL .= join("',", $arr_tmp_SQL);
					$sSQL .= ' WHERE '.$where_SQL;
					//echo $sSQL;
				}else{  //其他
					
				}
				
			}
			//exit();
			//纪录历程资料, 写入电脑上另外的log记录档, 主要纪录SQL指令
			//$this->system_log_insert($sSQL);
			
			//写入资料库
			$colDataArr["operating_date"] = "now()";
			$colDataArr["user_sn"] = $this->session->userdata('user_sn');
			$colDataArr["mode"] = $mode;
			$colDataArr["function_name"] = $this->session->userdata('function_name');
			$colDataArr["sql"] = $sSQL;
			$colDataArr["before_desc"] = $before_desc;
			$colDataArr["desc"] = $desc;
			$colDataArr["ip_address"] = $this->get_client_ip();
			$colDataArr["status"] = $status;
			
			if($this->db_insert( "log_operating", $colDataArr )){
			}else{
			}
			
			$this->session->set_userdata("sql_cmd", '');
			$this->session->set_userdata('before_desc', '');
			$this->session->set_userdata("sql_start", false);  //sql不纪录
		}
	}
	
	//记录历史资料_查询部分
	function log_insert_search($search, $SQLCmd, $succ_or_fail = true){
		$colDataArr = array();
		if(is_array($search)){  //如果示阵列
			foreach($search as $key => $value){
				if($value != ""){  //空白资料不记录
					$colDataArr[$key] = $value;
				}
			}
		}else{
			if($search != ""){
				$colDataArr['searchData'] = $search;
			}
		}
		$this->session->set_userdata("desc", $colDataArr);
		$this->session->set_userdata("sql_cmd", $SQLCmd);
		$this->log_operating_insert('0', "", $succ_or_fail);
	}
	
	//对照语系档
	function setLang($key){
	    //处理特别字串
		$src_key = "";
		$arrEx = array();
		if(strpos($key, ':') != false){
			$src_key = $key;
			$arrEx = explode(':', $key);
			$arrEx[0] = str_replace('[', '', $arrEx[0]);
			$key = $arrEx[0];
		}

		//取得class
		$class = $this->router->fetch_class();
		
		$upper = strtoupper($key);
		$lower = strtolower($key);
		$classupper = $class.'_'.$upper;
		$classlower = $class.'_'.$lower;
		$org = $key;
		$classorg = $class.'_'.$org;
		
		if($this->lang->line($classupper) != ""){
			$lang = $this->lang->line($classupper);
		}else if($this->lang->line($classlower) != ""){
			$lang = $this->lang->line($classlower);
		}else if($this->lang->line($upper) != ""){
			$lang = $this->lang->line($upper);
		}else if ($this->lang->line($lower) != ""){
			$lang = $this->lang->line($lower);
		}else if ($this->lang->line($org) != ""){
			$lang = $this->lang->line($org);
		}else if ($this->lang->line($classorg) != ""){
			$lang = $this->lang->line($classorg);
		}else{
			$lang = $key;
		}
		//过滤掉<br />, 因为有些会有这标签
		$lang = str_replace('<br />', '', $lang);
		
		//有特殊处理, 把英文转换成对应语系
		if($src_key != ""){
			$lang = str_replace($key, $lang, $src_key);
		}
		
		return $lang;
	}
	
	//建立目录
	function checkdir($dir){
		//echo $dir;
		if(!is_dir($dir)){
			if(!mkdir($dir, 0777)){
				// $this->redirect_alert("./", "建立 {$dir} 资料夹失败") ;
				// exit();
			}
		}
	}
	
	//累加帐密错误尝试次数, 并回传目前次数
	public function	sum_retry_error($loginUser){
		
		$SQLCmd = "	select login_retry_cnt 
					from sys_user 
					where user_id = '{$loginUser}' ";
		$rs = $this->db_query($SQLCmd) ;

		if(count($rs) != '0'){
			$login_retry_cnt = (int)$rs[0]["login_retry_cnt"] + 1;
			$whereStr = " user_id='{$loginUser}'" ;
			$colDataArr = array(
				"login_retry_cnt"		=> $login_retry_cnt,
			) ;
			$this->db_update( "sys_user", $colDataArr, $whereStr );
			
			return $login_retry_cnt;
		}else{
			return -999;
		}
	}
	
	//重置尝试登入次数
	public function reset_retry($loginUser){
		
		$whereStr = " user_id='{$loginUser}'" ;
		$colDataArr = array(
			"login_retry_cnt"		=> 0,
		) ;
		$this->db_update( "sys_user", $colDataArr, $whereStr );
	}
	
	//检查使用者是否有权限存取此页面
	public function checkurl($url)
	{
		$n = 0;
		$this->load->helper('url');
		$SQLCmd = " SELECT menu_directory
					FROM sys_menu sm
					LEFT JOIN sys_menu_group smg 
						ON sm.menu_sn = smg.menu_sn
					LEFT JOIN sys_user_group sug 
						ON sug.group_sn = smg.group_sn
					WHERE sug.user_sn = {$this->session->userdata('user_sn')}";
		$rs = $this->db_query($SQLCmd);
		// print_r($rs);
		foreach($rs as $key => $valueArr)
		{
			foreach($valueArr as $key2 => $menuurl)
			{
				if(strpos($url,$menuurl)!==false)
				{
					$n++;
				}
			}
		}

		if($n==0)
		{
			if($this->session->userdata("login_way") == "nimda"){	//登入方式
				$redirectUrl = $this->config->item('base_url')."nimda/login";
			}else{
				$redirectUrl = $this->config->item('base_url')."login";
			}
			$this->redirect_alert($redirectUrl, $this->lang->line('check_url_error'));
		}
	}
	
	//资讯对应表
	public function alias_list($col, $data){
		if($data == ""){
			return "";
		}
		//SQL
		$aliasSQL = "";
		
		//分割, 多个栏位
		$arrCol = explode(',', $col);
		foreach($arrCol as $value){
			//分割, 有无化名
			$arrColName = explode('.', trim($value));
			$count = count($arrColName);
			
			$colTitle = "";
			$colName = "";
			if($count == 1){
				$colName = $arrColName[0];
			}else if($count == 2){
				$colTitle = $arrColName[0].'.';
				$colName = $arrColName[1];
			}else{
				return " 1=2 ";
			}
			
			//初始化表
			$colArr = array();
			$colArr =$this->alias_colName($colName, $data);

			//搜寻对应
			$getArr = array();
			foreach($colArr as $key => $value){
				//都转大写, 来对应
				if(strpos(strtoupper($value), strtoupper($data)) === false){
				}else{  //有对应到就写入
					$getArr[] = $key;
				}
			}
			
			//建制SQL
			foreach($getArr as $value){
				if($aliasSQL != ""){
					$aliasSQL .= " OR ";
				}
				$aliasSQL .= " {$colTitle}{$colName} LIKE '%{$value}%' ";
			}
		}
		if($aliasSQL == ""){
			$aliasSQL = " 1=2 ";
		}	
		return $aliasSQL;
	}
	
	//对应表, [case 对应必须为大写英文]
	public function alias_colName($colName, $data){
		$colArr = array();
		$fetch_class = $this->router->fetch_class();
		switch(strtoupper($colName)){
			//性别
			case 'SEX':
				$colArr = array (
					"M"			=> "{$this->lang->line('male')}",
					"F"			=> "{$this->lang->line('female')}"
				);
				break;
			//状态
			case 'STATUS':
				if( $fetch_class == 'terminal' or 
					$fetch_class == 'reader_pairing' ){
					$colArr = array (
						"0"			=> "{$this->lang->line('disable_2')}",
						"1"			=> "{$this->lang->line('enable')}",
						"2"			=> "{$this->lang->line('disableuse')}",
						"3"			=> "{$this->lang->line('lockup')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'merchant' or 
						 $fetch_class == 'bank' ){
					$colArr = array (
						"Y"			=> "{$this->lang->line('enable')}",
						"N"			=> "{$this->lang->line('disable')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'initial_password'){
					$colArr = array (
						"0"			=> "{$this->lang->line('initial_password_NOT_ACTIVE')}",
						"1"			=> "{$this->lang->line('initial_password_ACTIVE')}",
						"3"			=> "{$this->lang->line('lockup')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'risk_monitoring_management'){
					$colArr = array (
						"Y"			=> "{$this->lang->line('rmm_Y')}",
						"N"			=> "{$this->lang->line('rmm_N')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'exception_refund_merchant'){
					$colArr = array (
						"Y"			=> "{$this->lang->line('erm_Y')}",
						"N"			=> "{$this->lang->line('erm_N')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'exception_credit_card'){
					$colArr = array (
						"Y"			=> "{$this->lang->line('ecc_Y')}",
						"N"			=> "{$this->lang->line('ecc_N')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}else if($fetch_class == 'reader_initialize_log'){
					$colArr = array (
						"0"			=> "{$this->lang->line('fail')}",
						"1"			=> "{$this->lang->line('sucess')}",
					);
				}else{
					$colArr = array (
						"0"			=> "{$this->lang->line('disable')}",
						"1"			=> "{$this->lang->line('enable')}",
						"D"			=> "{$this->lang->line('delete')}"
					);
				}
				break;
			//方式
			case 'WAY':
				$colArr = array (
					"D"			=> "{$this->lang->line('ecc_WAY_D')}",
					"L"			=> "{$this->lang->line('ecc_WAY_L')}"
				);
				break;
			//控制类别
			case 'CONTROL_TYPE':
				$colArr = array (
					"1"			=> "{$this->lang->line('rmm_radio_CONTROL_TYPE_1')}",
					"2"			=> "{$this->lang->line('rmm_radio_CONTROL_TYPE_2')}",
					"3"			=> "{$this->lang->line('rmm_radio_CONTROL_TYPE_3')}",
					"4"			=> "{$this->lang->line('rmm_radio_CONTROL_TYPE_4')}"
				);
				break;
			//不检核
			case 'CHECK_PHONE_COUNT':
				if(is_numeric($data)){
					$colArr = array (
						"0"			=> "{$this->lang->line('terminal_No_Check_Phone')}", 
						"{$data}"	=> "{$data}"
					);
				}else{
					$colArr = array (
						"0"			=> "{$this->lang->line('terminal_No_Check_Phone')}"
					);
				}

				break;
			//是否下载
			case 'TMS_DOWNLOAD':
				$colArr = array (
					"Y"			=> "{$this->lang->line('yes')}",
					"N"			=> "{$this->lang->line('no')}"
				);
				break;
			//是否已初始化
			case 'INITIALIZED':
			//是否回报
			case 'REPORTABLE':
				$colArr = array (
					"1"			=> "{$this->lang->line('yes')}",
					"0"			=> "{$this->lang->line('no')}"
				);
				break;
			//前后台登入
			case 'LOGIN_SIDE_DISPLAY':
				$colArr = array (
					"0"			=> "{$this->lang->line('login_side_0')}",
					"1"			=> "{$this->lang->line('login_side_1')}",
					"2"			=> "{$this->lang->line('login_side_2')}"
				);
				break;
			//事件类别
			case 'EVENT_TYPE':	
				$colArr = array (
					"S"			=> "{$this->lang->line('system_event')}",
					"M"			=> "{$this->lang->line('merchant_event')}"
				);
				break;
			//操作模式
			case 'MODE':
				$colArr = array (
					"0"			=> "{$this->lang->line('select')}",
					"1"			=> "{$this->lang->line('add')}",
					"2"			=> "{$this->lang->line('edit')}",
					"3"			=> "{$this->lang->line('delete')}"
				);
				break;
			//app客户端名称
			case 'OWNER_ID':
				$colArr = array (
					"01"			=> "{$this->lang->line('app_check_NCCC')}",
					"02"			=> "{$this->lang->line('app_check_ESUN')}" 
				);
				break;
			//手机系统
			case 'OS_TYPE':
				$colArr = array (
					"A"			=> "{$this->lang->line('app_check_Android')}",
					"I"			=> "{$this->lang->line('app_check_iOS')}" 
				);
				break;
			//客户端版本
			case 'CLIENT_CODE':
				$colArr = array (
					"00"			=> "{$this->lang->line('app_check_Standard')}"
				);
				break;
			default:
				break;
		}
		return $colArr;
	}
	
	//检查输入的mail是否正确
	public function checkmail($email = "")
	{
		$email = addslashes($email);
		if($email=="")
		{
			return '0';
			exit;
		}
		// $email = $this->input->post('recoverEmail');

		$SQLCmd = "	SELECT su.user_id 
					FROM sys_user su 
					WHERE email_address = '{$email}' 
						AND status = '1'";
		$rs = $this->db_query($SQLCmd);
		if(count($rs)==0)
		{
			return '0';
		}
		else
		{
			return $rs[0]['user_id'];
		}
	}

	//传送mail
	public function sendemail($toemail="",$subject="",$Body="",$user_id="",$system = "")
	{
		$toemail = addslashes($toemail);
		$login_link = $this->uri->uri_string();
		$login_link = explode('/', $login_link);
		$count = count($login_link);
		$tmp = "";
		for($i=0; $i<$count-1; $i++){
			if($tmp == ""){
				$tmp .= $login_link[$i];
			}else{
				$tmp .= '/'.$login_link[$i];
			}
		}
		$login_link = $tmp;
		ini_set('date.timezone',"Asia/Taipei");
		$web = $this->config->item('base_url');
		$this->load->library('email');

		$smtp_host = "";
		$smtp_user = "";
		$smtp_pass = "";
		$SQLCmd = "SELECT * FROM sys_config WHERE status = 1";
		$rs = $this->db_query($SQLCmd);
		if(count($rs)>0)
		{
			foreach($rs as $key => $value)
			{
				switch($value['config_code']){
				  	case 'smtp_host':
					    $smtp_host = $value['config_set'];
					  	break;
				 	case 'smtp_user':
					    $smtp_user = $value['config_set'];
					 	break;
					case 'smtp_pass':
					    $smtp_pass = $value['config_set'];
					 	break;
					case 'smtp_port':
					    $smtp_port = $value['config_set'];
					 	break;
				  	default:
				}
			}
		}
		if($smtp_host == "" || $smtp_user == "" || $smtp_pass == "")
		{
			return "{$this->lang->line('login_email_error1')} {$system} {$this->lang->line('login_email_error2')}";
			exit;
		}
		$config['protocol'] = "smtp";
		$config['smtp_host'] = $smtp_host;
		$config['smtp_user'] = $smtp_user;
		$config['smtp_pass'] = $smtp_pass;
		$config['smtp_port'] = $smtp_port;
		$config['charset'] = "utf-8";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		$this->email->from('ecmoservice@gmail.com', $system.' System Manager');
		$this->email->to($toemail); 
		// $this->email->cc('another@another-example.com'); 
		// $this->email->bcc('them@their-example.com'); 
		
		$this->email->subject($subject);
		
		$newpassword = $this->generatorPassword();
		$Body = "{$system} {$this->lang->line('login_email_body1')} <br>
				{$this->lang->line('login_email_body2')} : {$user_id}<br>
				{$this->lang->line('login_email_body3')} : ".htmlentities($newpassword)."<br><br>
				{$this->lang->line('login_email_body4')} : <a href='{$web}{$login_link}' target='_blank'>{$web}{$login_link}</a>";
	
		$MD5_newpassword = MD5($newpassword);
		//$whereStr = "email_address = '{$toemail}'";
		//$dataArr['password'] = MD5($newpassword);
		//$dataArr['login_force_pwd'] = 1;
		//取得密码有效期限天数, 找不到值预设30天
		$change_password_day = '30';
		if($this->session->userdata('change_password_day')){
			$change_password_day = $this->session->userdata('change_password_day');
		}
		$years = date("Y"); //用date()函式取得目前年份格式0000
		$months = date("m"); //用date()函式取得目前月份格式00
		$days = date("d"); //用date()函式取得目前日期格式00
		$day = date("Y-m-d",mktime(0,0,0,$months,$days+$change_password_day,$years));
		//$dataArr['pwd_deadline'] = $day;
		//$this->db_update( "sys_user", $dataArr, $whereStr );
		
		$SQLCmd = "	update sys_user su 
					set pwd = '{$MD5_newpassword}', 
						login_force_pwd = '1', 
						pwd_deadline = '{$day}' 
					where email_address = '{$toemail}' ";
		$rs = $this->db_query($SQLCmd);
		

		// $mail->Body = $Body;
		// $Body = str_replace("<span style='font-size:24'>", "",$Body);
		// $Body = str_replace("<span style='color:blue'>", "",$Body);
		// $Body = str_replace("<B>", "",$Body);
		// $Body = str_replace("</B>", "",$Body);
		// $Body = str_replace("</span>", "",$Body);
		// $Body = str_replace("<BR>", "",$Body);
		$this->email->message($Body); 

		$this->email->send();

		return '1';
		//echo $this->email->print_debugger();
	}

	//传送mail (STATUS)
	public function sendemail_default($toemail="",$subject="",$Body="")
	{
		$toemail = addslashes($toemail);
		$login_link = $this->uri->uri_string();
		$login_link = explode('/', $login_link);
		$count = count($login_link);
		$tmp = "";
		for($i=0; $i<$count-1; $i++){
			if($tmp == ""){
				$tmp .= $login_link[$i];
			}else{
				$tmp .= '/'.$login_link[$i];
			}
		}
		$login_link = $tmp;
		ini_set('date.timezone',"Asia/Taipei");
		$web = $this->config->item('base_url');
		$this->load->library('email');

		$smtp_host = "";
		$smtp_user = "";
		$smtp_pass = "";
		$SQLCmd = "SELECT * FROM sys_config WHERE status = 1";
		$rs = $this->db_query($SQLCmd);
		if(count($rs)>0)
		{
			foreach($rs as $key => $value)
			{
				switch($value['config_code']){
				  	case 'smtp_host':
					    $smtp_host = $value['config_set'];
					  	break;
				 	case 'smtp_user':
					    $smtp_user = $value['config_set'];
					 	break;
					case 'smtp_pass':
					    $smtp_pass = $value['config_set'];
					 	break;
					case 'smtp_port':
					    $smtp_port = $value['config_set'];
					 	break;
				  	default:
				}
			}
		}
		if($smtp_host == "" || $smtp_user == "" || $smtp_pass == "")
		{
			return "{$this->lang->line('login_email_error1')} {$system} {$this->lang->line('login_email_error2')}";
			exit;
		}
		$config['protocol'] = "smtp";
		$config['smtp_host'] = $smtp_host;
		$config['smtp_user'] = $smtp_user;
		$config['smtp_pass'] = $smtp_pass;
		$config['smtp_port'] = $smtp_port;
		$config['charset'] = "utf-8";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		$this->email->from('ecmoservice@gmail.com', ' System Manager');
		$this->email->to($toemail); 
		// $this->email->cc('another@another-example.com'); 
		// $this->email->bcc('them@their-example.com'); 
		
		$this->email->subject($subject);
		$this->email->message($Body); 

		$rs = $this->email->send();

		return $rs;
		//echo $this->email->print_debugger();
	}

	//乱数产生密码
	function generatorPassword()
	{
	    $password_len = 7;
	    $password = '';

	    // remove o,0,1,l
	    $word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	    $len = strlen($word);

	    for ($i = 0; $i < $password_len; $i++) {
	        $password .= $word[rand() % $len];
	    }

	    return $password;
	}

	public function insert_user_log($user_sn, $login_before, $login_b_ip ){
		$colDataArr = array();
		if($login_before != ''){
			$colDataArr['login_before'] = $login_before;
		}
		if($login_b_ip != ''){
			$colDataArr['login_b_ip'] = $login_b_ip;
		}
		$colDataArr['login_now'] = "now()";
		$colDataArr['login_n_ip'] = $this->input->ip_address();

		$whereStr = "user_sn = '{$user_sn}'";

		$this->db_update( "sys_user", $colDataArr, $whereStr);
	}
	
	public function wechat_callback($postArr=array(),$posturl_param="",$api_chinese_name="")
	{
		//取得微信的網址參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$url = $rsC[0]['config_set'].$posturl_param;
		}
		else
		{
			$url = "";
		}

		$postStr = http_build_query($postArr);

		$dataArrLog = array();
		$dataArrLog['api_name'] = $postArr['action'];
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		date_default_timezone_set("Asia/Taipei");
		$nowDate = date("Y-m-d H:i:s");
		$dataArrLog['send_date'] = $nowDate;
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		// $SQLCmdLa = "select LAST_INSERT_ID() as log_sn FROM log_send_wechat_api";
		$SQLCmdLa = "select s_num as log_sn FROM log_send_wechat_api WHERE send_date = '{$nowDate}'";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		$output = curl_exec($ch);

		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}
}
/* End of file model_background.php */
/* Location: ./application/models/common/model_background.php */

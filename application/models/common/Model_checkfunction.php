<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Angus 2012.09.11

class Model_checkfunction extends MY_Model {
	private $_lang_first;
	
	public function checkfunction($checkArr = array())
	{
		if($this->session->userdata('default_language'))
		{
			$lan = $this->session->userdata('default_language');
		}
		else {
			$lan = $this->session->userdata('display_language');
		}
		
		$return = "";
		foreach($checkArr as $field => $checkDetail){
			//取值
			$tmpArr = explode(',', $field);
			$getLabel = trim($tmpArr[0]);  //取label
			$getValue = trim($tmpArr[1]);  //取value, 因为当时label和值, 栏位名称可能会取的不依样
			if($this->input->post("senddata")){
				$senddata = $this->input->post("senddata");
				$value = $senddata[$getValue];
			}else{
				$value = $this->input->post($getValue);
			}
			// echo $getValue.'::'.$value;
			// echo '<br />';
			foreach($checkDetail as $checkType => $ruleValue){  //$checkType检查方式, $ruleValue检查规则
				//读取语系, 组合讯息
				$lanArr = $this->lanMap($lan, $getLabel, $ruleValue);
				//开始检查
				$return .= $this->startCheck($lanArr, $value, $checkType, $ruleValue);
			}
		}
		return $return;
	}

	//$lan语系, $value被检查值, $checkType检查方式, $ruleValue检查规则
	function startCheck($lanArr, $value, $checkType, $ruleValue){
		$return = true;
		switch($checkType){
			case "required":  //必填
				if($value != ""){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "onlyNumberSp": 
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^[0-9]+$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "date" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "date2" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^\d{4}[\/\-](0?[1-9]|1[012])$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "time" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "equals" :
				$flag = false;
				//分割字串
				$tmpArr = explode(',', $ruleValue);
				if($this->input->post(trim($tmpArr[0])) == $this->input->post(trim($tmpArr[1]))){  //一样数值
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "minSize" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(mb_strlen($value)>=$ruleValue){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "maxSize" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(mb_strlen($value)<=$ruleValue){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "min" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(mb_strlen($value)>=$ruleValue){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "max" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(mb_strlen($value)<=$ruleValue){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "phone" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^([\+][0-9]{1,3}[ \.\-])?([\(]{1}[0-9]{2,6}[\)])?([0-9 \.\-\/]{3,20})((#|x|ext|extension)[ ]?[0-9]{1,4})?$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "email" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "integer" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^[\-\+]?\d+$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "number" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^[\-\+]?((([0-9]{1,3})([,][0-9]{3})*)|([0-9]+))?([\.]([0-9]+))?$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "ipv4" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^((([01]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))[.]){3}(([0-1]?[0-9]{1,2})|(2[0-4][0-9])|(25[0-5]))$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "url" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "onlyLetterSp" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^[a-zA-Z\ \']+$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "onlyLetterNumber" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^[0-9a-zA-Z]+$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "onlyAmount" :
				if($value == ""){  //没值, 跳过
					break;
				}
				if(preg_match("/^(([0-9]{1,7})|([0-9]{1,7}[.]{1,1}[0-9]{1,2}))$/", $value)){
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				break;
			case "justsomevalue" : 
				$flag = false;
				//分割字串
				$tmpArr = explode(',', $ruleValue);
				if($tmpArr[0] == ""){  //没有检查规则
					break;
				}else{
					foreach($tmpArr as $getRule){
						if($value == trim($getRule)){
							$flag = true;
						}
					}
				}
				//比对值
				if($flag === true){  //有比对到
					$return = true;
				}else{
					$return = $lanArr[$checkType];
				}
				
				break;
			default:
				$return = true;
				break;
		}
		
		$returnValue = "";
		if($return === true){  //正常

		}else{  //异常
			$returnValue .= $return.'\n';
		}
		return $returnValue;
	}
	
	//语系对应
	function lanMap($lan, $field, $ruleValue){
		//取得class
		$fetch_class = $this->router->fetch_class();
		//取得栏位对应语系
		$field_label = $this->lang->line("{$fetch_class}_{$field}");
		$field_label = str_replace("<br />", "", $field_label);
		$field_label = $field_label.':';
		$msgArr = array();
		if($lan == 'zh_tw'){
			$msgArr = array(
				"required"			=> "{$field_label}必填", 
				"onlyNumberSp"		=> "{$field_label}只能填数字",
				"date"				=> "{$field_label}无效的日期，格式必需为 YYYY-MM-DD",
				"equals"			=> "{$field_label}栏位内容不相符",
				"date2"				=> "{$field_label}无效的日期，格式必需为 YYYY-MM",
				"time"				=> "{$field_label}无效的时间，格式必需为 hh:mm:ss",
				"minSize"			=> "{$field_label}最少{$ruleValue}个字元",
				"maxSize"			=> "{$field_label}最多{$ruleValue}个字元",
				"min"				=> "{$field_label}最小值为{$ruleValue}",
				"max"				=> "{$field_label}最大值为{$ruleValue}",
				"phone"				=> "{$field_label}无效的电话号码",
				"email"				=> "{$field_label}信箱格式错误",
				"integer"			=> "{$field_label}不是有效的整数",
				"number"			=> "{$field_label}无效的数字",
				"ipv4"				=> "{$field_label}无效的 IP 位址",
				"url"				=> "{$field_label}Invalid URL",
				"onlyLetterSp"		=> "{$field_label}只接受英文字母大小写",
				"onlyLetterNumber"	=> "{$field_label}不接受特殊字元",
				"onlyAmount"		=> "{$field_label}金额格式错误！\n 整数时可以不输入小数点；\n 如果有输入小数点，同时填上小数点后１～２位金额！",
				"justsomevalue"		=> "{$field_label}选项中无此值！"
			);
		}else{
			$msgArr = array(
				"required"			=> "{$field_label}Can not be space!", 
				"onlyNumberSp"		=> "{$field_label}Only fill in numeric",
				"date"				=> "{$field_label}Invalid Date，Format is  YYYY-MM-DD",
				"equals"			=> "{$field_label}Fields do not match",
				"date2"				=> "{$field_label}Invalid Date，Format is YYYY-MM",
				"time"				=> "{$field_label}Invalid Time，Format is hh:mm:ss",
				"minSize"			=> "{$field_label}At least {$ruleValue} word(s)",
				"maxSize"			=> "{$field_label}At most {$ruleValue} word(s)",
				"min"				=> "{$field_label}the Minimum is {$ruleValue}",
				"max"				=> "{$field_label}the Maximun is {$ruleValue}",
				"phone"				=> "{$field_label}Invalid phone number",
				"email"				=> "{$field_label}Invalid Email",
				"integer"			=> "{$field_label}Invalid Integer",
				"number"			=> "{$field_label}Invalid number",
				"ipv4"				=> "{$field_label}Invalid IP address",
				"url"				=> "{$field_label}Invalid URL",
				"onlyLetterSp"		=> "{$field_label}Only accept uppercase and lowercase letters",
				"onlyLetterNumber"	=> "{$field_label}Special characters are not accepted",
				"onlyAmount"		=> "{$field_label}Amount format error！\n You can enter a integer without decimal；\n If there is a decimal point, fill Decimal places from 1 to 2！",
				"justsomevalue"		=> "{$field_label}Not the value！"
			);
		}
		return $msgArr;
	}
}
?>
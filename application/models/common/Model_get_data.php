<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Angus 2012.09.11

class Model_get_data extends MY_Model{

	public function get_wealth_term_no()
	{
		$machine_no = $this->input->post('machine_no');

		$query = "SELECT ms.machine_no, ms.wealth_term_no, ms.terminal_no, ms.check_result, bk.bank_name, ms.item_no, it.item_name, it.item_spec, ms.bank_no 
				  FROM machine_stock ms
				  left join bank bk ON bk.bank_no = ms.bank_no and bk.mark <> 'CL'
				  left join item it ON it.kind_no = ms.kind_no and it.item_no = ms.item_no and it.mark <> 'CL'
				  WHERE ms.machine_no = '{$machine_no}'
				  AND ms.mark <> 'CL' ";
		$rs = $this->db_query($query);
		return $rs;
	}

	public function get_machine_no()
	{
		$wealth_term_no = $this->input->post('wealth_term_no');

		$query = "SELECT ms.machine_no, ms.wealth_term_no, ms.terminal_no, ms.check_result, ms.item_no, bk.bank_name, it.item_name, it.item_spec, ms.bank_no
				  FROM machine_stock ms
				  left join bank bk ON bk.bank_no = ms.bank_no and bk.mark <> 'CL'
				  left join item it ON it.kind_no = ms.kind_no and it.item_no = ms.item_no and it.mark <> 'CL'
				  WHERE ms.wealth_term_no = '{$wealth_term_no}'
				  AND ms.mark <> 'CL' ";
		$rs = $this->db_query($query);
		return $rs;
	}	

	public function get_WtnoMno($terminal_no = "")
	{
		if($this->input->post('terminal_no') !="")
			$terminal_no = $this->input->post('terminal_no');

		$query = "SELECT item_no, machine_no, wealth_term_no, check_result 
				  FROM machine_stock
				  WHERE terminal_no = '{$terminal_no}'
				  AND mark <> 'CL' ";
		$rs = $this->db_query($query);
		return $rs;
	}
	
	public function get_StockNo(){
		$terminal_no = $this->input->post('terminal_no');
		$mcht_id = $this->input->post('mcht_id');
		$query = "SELECT machine_no, wealth_term_no
				  FROM property_main
				  WHERE terminal_no = '{$terminal_no}' and mcht_id = '{$mcht_id}'
				  AND mark <> 'CL' ";
		$rs = $this->db_query($query);
		return $rs;
	}
	
	public function get_last_mid(){
		$terminal_no = $this->input->post('terminal_no');
		$query = "SELECT 
					mcht_id
				  FROM install_main
				  WHERE terminal_no = '{$terminal_no}'
					AND mark <> 'CL' 
				  order by finish_date desc
				  limit 1 
				  ";
		$rs = $this->db_query($query);
		return $rs;
	}
	
	//暂时先留着
	public function get_WtnoMno2($terminal_no)
	{

		$query = "SELECT item_no, machine_no, wealth_term_no, check_result 
				  FROM machine_stock
				  WHERE terminal_no = '{$terminal_no}'
				  AND mark <> 'CL' ";
		$rs = $this->db_query($query);
		return $rs;
	}
	
	//暂时先留着
	public function insert_re($machine_no, $wealth_term_no, $terminal_no)
	{
		$insert = array();
		$insert['seq_no'] = '9';
		$insert['kind_no'] = 'AB';
		$insert['item_no'] = 'PT0005';
		$insert['rel_no'] = 'LI1051209007';
		$insert['release_reason'] = 'A004';
		$insert['use_mark'] = '02';
		$insert['release_date'] = '2016-12-09';
		$insert['mark'] = 'NA';
		$insert['machine_no'] = $machine_no;
		$insert['wealth_term_no'] = $wealth_term_no;
		$insert['terminal_no'] = $terminal_no;
		$insert['first_per'] = '291';
		$insert['first_date'] = "now()";

		return $this->db_insert( "release_machine_no", $insert );
		
	}

	public function check_machine_no(){
		$field = $this->input->post("field");
		$field_name = $this->input->post("machine_no");
		$no_name = $this->input->post("no_name");
		$ist_no = $this->input->post($no_name);
		$table = $this->input->post('table');
		$seq = $this->input->post("seq");
		
		$SQLCmd  = "select seq_no
					from {$table}
					where 
						{$field} = '{$field_name}'
						and {$no_name} = '{$ist_no}'
						and seq_no <> '{$seq}'
						and mark <> 'CL'";
		$rs = $this->db_query($SQLCmd);

		if(count($rs)>0){
			echo $rs[0]['seq_no'];
		}else{
			echo 'success';
		}
	
	}
}
?>
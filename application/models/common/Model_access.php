<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Angus 2012.09.11

class Model_access extends MY_Model{
	public function access_control_list()
	{
		//权限, key均是2的1~N次方,例1, 2, 4, 8, 16, 32, 64, 128, 256.....依次类推
		$access_control_list = array(
			"1" 		=> 	$this->lang->line('add'), 
			"2" 		=> 	$this->lang->line('edit'), 
			"8192" 		=> 	$this->lang->line('view'), 
			"4" 		=> 	$this->lang->line('delete'), 
			"128"		=> 	$this->lang->line('data_search'),
			"256"		=> 	$this->lang->line('save'), 
			"16384"		=> 	$this->lang->line('auto_change'), 
			"2048"		=> 	$this->lang->line('buy'), 
			"4096"		=> 	$this->lang->line('make_up')

		);
		return $access_control_list;
	}
	
	//$mode 为哪种状态下显示那些对应按钮
	//一般预设为浏览(brower)状态, 其他则为修改, 新增(edit)状态
	public function user_access_control($mode = "brower"){
		$web = $this->config->item('base_url');
		//比对此页面与使用者的权限功能
		$thispage_url = uri_string();
		$access = $this->session->userdata('user_access');
		// print_r($access);
		$access_control = 0;
		$no_cancel = false;  //取消按钮开关
		foreach($access as $access_key => $access_value){
			if($access_key != ""){
				if( strpos($thispage_url, $access_key) !== false && 
					strpos($thispage_url, $access_key) === 0){
					if('stock/inv_main_begin' == $access_key){
						$no_cancel = true;
					}
					$access_control = $access_value;
				}
			}
		}
		
		$access_control_list = $this->session->userdata('access_control_list');
		$temp = 'N';
		$html = "";
		$html .= <<<html
html;
		if($mode == 'brower'){  //浏览
			//纪录目前的URL, 提供取消返回使用
			$back_url = current_url();
			if($this->router->fetch_method() == "getlist"){  //避免返回后, 又弹出选择画面
				$back_url = str_replace("getlist", "index/0/", current_url());
			}
			$fn = get_fetch_class_random();
			$this->session->set_userdata("{$fn}_".'back_url', $back_url);  
			foreach($access_control_list as $access_key => $access_name){
				$control = $access_control & $access_key;
				switch ($control){
					case 1:  //新增
						$html .= <<<html
									<a  style="cursor:pointer" id="btAddition" class="buttoninActive">{$this->lang->line('add_1')}</a>
html;
						break;
					case 2:  //修改
						$html .= <<<html
									<span id="modify_enable" style="display:none">
												<a  style="cursor:pointer" id="btModification" class="buttoninActive">{$this->lang->line('edit_1')}</a>
									</span>
									<span id="modify_disable" style="display:none">
												<a id="btModification" class="buttonnoActive" disabled>{$this->lang->line('edit_1')}</a>
									</span>
html;
						break;
					case 4:  //删除
						$html .= <<<html
									<span id="delete_enable" style="display:none">
											<a  style="cursor:pointer" id="btDelete" class="buttoninActive bdelete">{$this->lang->line('delete_1')}</a>
									</span>
									<span id="delete_disable" style="display:none">
											<a id="btDelete" class="buttonnoActive bdelete">{$this->lang->line('delete_1')}</a>
									</span>

html;
						break;
					case 8:  //列印
						$html .= <<<html
								<span id="print_enable" style="display:none"><button id="btprint" onClick="javascript:print()"><img src="{$web}img/printer.png" width="32"><br>{$this->lang->line('print')}</img></button></span>
								<span id="print_disable" style="display:none"><button id="btprint" onClick="javascript:print()" disabled><img src="{$web}img/printer_d.png" width="32"><br>{$this->lang->line('print')}</img></button></span>		
html;
						break;
					case 16:  //合格厂商一览表
						$html .= <<<html
								<span id="print_ok_vender_enable" style="display:none"><button id="btprint_ok_vender" onClick="javascript:print_ok_vender()"><img src="{$web}img/printer.png" width="32"><br>{$this->lang->line('print_ok_vender')}</img></button></span>	
								<span id="print_ok_vender_disable" style="display:none"><button id="btprint_ok_vender" onClick="javascript:print_ok_vender()" disabled><img src="{$web}img/printer_d.png" width="32"><br>{$this->lang->line('print_ok_vender')}</img></button></span>	
html;
						break;
					case 32:  //料品名细表
						$html .= <<<html
								<span id="print_item_all_enable" style="display:none"><button id="btprint_item_all" onClick="javascript:print_item_all()"><img src="{$web}img/printer.png" width="32"><br>{$this->lang->line('print_item_all')}</img></button></span>	
								<span id="print_item_all_disable" style="display:none"><button id="btprint_item_all" onClick="javascript:print_item_all()" disabled><img src="{$web}img/printer_d.png" width="32"><br>{$this->lang->line('print_item_all')}</img></button></span>	
html;
						break;
					case 64:  //料品新旧对照表
						$html .= <<<html
								<span id="print_item_new_old_enable" style="display:none"><button id="btprint_item_new_old" onClick="javascript:print_item_new_old()"><img src="{$web}img/printer.png" width="32"><br>{$this->lang->line('print_item_new_old')}</img></button></span>	
								<span id="print_item_new_old_disable" style="display:none"><button id="btprint_item_new_old" onClick="javascript:print_item_new_old()" disabled><img src="{$web}img/printer_d.png" width="32"><br>{$this->lang->line('print_item_new_old')}</img></button></span>	
html;
						break;
					case 128:  //搜寻
						$html .= <<<html
									<span id="data_search_enable" style="display:none"><button id="btData_search"><img src="{$web}img/data_search.png" width="32"><br>{$this->lang->line('data_search')}</img></button></span>
									<span id="data_search_disable" style="display:none"><button id="btData_search" disabled><img src="{$web}img/data_search_d.png" width="32"><br>{$this->lang->line('data_search')}</img></button></span>
html;
						break;
					case 1024:  //重新计算库存
						$html .= <<<html
									<button class="" id="btRecount_inv"><img src="{$web}img/recount_inv.png" width="32"><br>{$this->lang->line('recount_inv')}</img></button>
html;
						break;
					case 4096:  //批次回报
						$this->session->set_userdata('make_up', 'Y');
						break;
					case 8192:  //修改
						$html .= <<<html
									<span id="view_enable" style="display:none">
										<a style="cursor:pointer" id="btView" class="buttoninActive">{$this->lang->line('view_1')}</a>
									</span>
									<span id="view_disable" style="display:none">
										<a id="btView" class="buttonnoActive" disabled>{$this->lang->line('view_1')}</a>
									</span>
html;
						break;
					case 16384:  //汇出excel
						$html .= <<<html
								<span id="export_excel_enable" style="display:none"><button id="btexport_excel" onClick="javascript:export_excel()"><img src="{$web}img/printer.png" width="32"><br>{$this->lang->line('export_excel')}</img></button></span>
								<span id="export_excel_disable" style="display:none"><button id="btexport_excel" onClick="javascript:export_excel()" disabled><img src="{$web}img/printer_d.png" width="32"><br>{$this->lang->line('export_excel')}</img></button></span>		
html;
						break;
					case 2048:  //加购
						$html .= <<<html
									<span id="modify_enable" style="display:none">
												<a style="cursor:pointer" id="btBuy" class="buttoninActive">{$this->lang->line('buy')}</a>
									</span>
									<span id="modify_disable" style="display:none">
												<a id="btBuy" class="buttonnoActive" disabled>{$this->lang->line('buy')}</a>
									</span>
html;
						break;

				}
			
			}
		}else if($mode == 'main'){
			
			foreach($access_control_list as $access_key => $access_name){
				$control = $access_control & $access_key;
				switch ($control){
					case 16384:  //存檔
					$temp = 'Y';
					break;
				
				}

			}
		
		}else{  //新增, 修改
			foreach($access_control_list as $access_key => $access_name){
				$control = $access_control & $access_key;
				//取出返回页面的网址
				$fn = get_fetch_class_random();
				$back_url = $this->session->userdata("{$fn}_".'back_url');  
				if($back_url == ""){
					$back_url = "{$web}main/";
				}
				switch ($control){
					case 256:  //存档
						if($mode == 'view'){  //检视
							// $html .= <<<html
								// <button class="" back_url="{$back_url}" id="btCancel"><img src="{$web}img/cancel.png" width="32"><br>{$this->lang->line('cancel')}</img></button>
// html;
						}else{
							if($no_cancel === true){  //不需要取消开关
								$html .= <<<html
										<a id="btSave" class="buttongreen">{$this->lang->line('save')}</a>

html;
							}else{
								$html .= <<<html
										<a style="cursor:pointer"  id="btCancel" class="buttondark"  back_url="{$back_url}" >{$this->lang->line('cancel')}</a>
										<a style="cursor:pointer"   id="btSave" class="buttongreen">{$this->lang->line('save')}</a>
html;
							}
						}
						

						break;
					case 512:  //前月转入
							$html .= <<<html
										<button class="" id="btTrans_inv"><img src="{$web}img/trans_inv.png" width="32"><br>{$this->lang->line('trans_inv')}</img></button>
html;
						break;
					case 8192:  //检视
						if($mode == 'view'){  //检视
							$html .= <<<html
										<a style="cursor:pointer"  id="btCancel" class="buttondark"  back_url="{$back_url}" >{$this->lang->line('cancel')}</a>
html;
						}
						break;
				}
			}
		}
		$html .= <<<html
html;
		if($mode == 'main'){
			return $temp;
		}else{
			return $html;
		}
	}

	//查当下是哪一页
	public function getNowMenuSn($menu_name){
			$whereStr = " and menu_name = '{$menu_name}'";
			$SQLCmd = "SELECT menu_sn, parent_menu_sn
					   FROM sys_menu
					   WHERE status <> 'D' and menu_directory != '#' {$whereStr}";
			$rs = $this->db_query($SQLCmd) ;

			return $rs;
	}

	public function showsubtitle($bView, $sn){
		if($bView){
			$show_sub_title = "查询";
		}else{
			$show_sub_title = ($sn!='')?'编辑':'新增';
		}

		return $show_sub_title;


	}

}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Angus 2012.09.11

class Model_show_list extends MY_Model{
	//取得经销商下拉选单资料
	public function getdealerList(){
		$SQLCmd = "SELECT s_num,tde01 FROM tbl_dealer WHERE status = 'Y'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得子经销商下拉选单
	public function getsubdealerList($td_num = ""){
		$whereStr = "";
		if($td_num != "")
		{
			$whereStr = "AND td_num = '{$td_num}'";
		}

		$SQLCmd = "SELECT s_num,tsde01,td_num FROM tbl_sub_dealer WHERE status = 'Y' {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得车辆下拉选单资料
	public function getvehicleList(){
		$SQLCmd = "SELECT s_num,unit_id FROM tbl_vehicle WHERE status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得营运商下拉选单资料
	public function getoperatorList($whereStr = ""){
		$SQLCmd = "SELECT s_num, top01, top08, top09
  						FROM tbl_operator where status = 'Y' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得子营运商下拉选单
	public function getsuboperatorList($to_num = ""){
		$whereStr = "";
		if($to_num != "")
		{
			$whereStr = "AND to_num = '{$to_num}'";
		}
		
		$SQLCmd = "SELECT s_num,tsop01,to_num FROM tbl_sub_operator WHERE status = 'Y' {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得換電站下拉选单资料
	public function getbatteryswapstationList($so_num = ""){
		if($so_num != "")
		{
			$whereStr = "AND so_num = '{$so_num}'";
		}

		$SQLCmd = "SELECT s_num, bss_id, concat(location,'（',bss_id,'）') as bss_id_name, so_num, bss_token, location  
  						FROM tbl_battery_swap_station where status = 'Y'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得換電站群組下拉选单资料
	public function getbatterygroupList($s_num = ""){
		if($s_num != "")
		{
			$whereStr = "AND s_num = '{$s_num}'";
		}

		$SQLCmd = "SELECT s_num, group_name
  						FROM tbl_battery_swap_station_group where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得租借项目下拉选单
	public function getleasepriceList(){
		$SQLCmd = "SELECT s_num, name
  						FROM tbl_lease_price where status = 'Y'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得會員微信資訊資料表
	public function getwechatinfoList($s_num){
		$SQLCmd = "SELECT *
  						FROM tbl_member_wechat_info where tm_num = '{$s_num}'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得會員資料表
	public function getMemberList($s_num = '', $whereStr_2 = ""){
		$whereStr = '';
		if($s_num != ''){
			$whereStr = " and s_num = '{$s_num}'";
		}
		if($whereStr_2 != ''){
			$whereStr .= $whereStr_2;
		}

		$SQLCmd = "SELECT s_num, name, user_id
  						FROM tbl_member where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得储值方式 T=時間；A=金額
	public function getstored_type(){
		$SQLCmd = "SELECT config_set FROM sys_config WHERE config_code = 'stored_type'";
		$rs = $this->db_query($SQLCmd);
		return $rs[0]['config_set'];
	}
	
	//取得該中文名稱對應的s_num
	public function getFiledsum($table, $field, $field_name, $field_val){
		$SQLCmd = "SELECT {$field} FROM {$table} WHERE {$field_name} = '{$field_val}' and status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得帐号管理人員姓名
	public function getSysuser(){
		$SQLCmd = "SELECT user_sn, user_name
  						FROM sys_user where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		$sysusername = array();
		if(count($rs)>0){
			foreach($rs as $rs_arr){
				$sysusername[$rs_arr['user_sn']] = $rs_arr['user_name'];
			
			}
		}
		return $sysusername;	
	}

	public function getConfigSet($config_code){
		$SQLCmd = "SELECT config_set
  						FROM sys_config 
						where config_code = '{$config_code}'";
		$rs = $this->db_query($SQLCmd) ;
		$config_set = '';
		if(count($rs)>0){
			$config_set = $rs[0]['config_set'];
		}
		return $config_set;	
	}

	//取得电池序號下拉选单资料
	public function getbatteryList(){
		$SQLCmd = "SELECT battery_id, battery_id
  						FROM tbl_battery where status <> 'D'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得所有卡片資料
	public function getMemberDom($whereStr = ""){
		$SQLCmd = "SELECT s_num, user_id, pre_order_num, lave_num
  						FROM tbl_member where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		$member = array();
		if(count($rs)>0){
			foreach($rs as $rs_arr){
				$member[$rs_arr['user_id']]['s_num'] = $rs_arr['s_num'];
				$member[$rs_arr['user_id']]['user_id'] = $rs_arr['user_id'];
				$member[$rs_arr['user_id']]['pre_order_num'] = $rs_arr['pre_order_num'];
				$member[$rs_arr['user_id']]['lave_num'] = $rs_arr['lave_num'];
			}
		}

		return $member;
	}

	//取得所有卡片資料
	public function getMemberCount($whereStr = ""){
		$SQLCmd = "SELECT count(*) cnt
  						FROM tbl_member where status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;

		return $rs[0]['cnt'];
	}

	public function check_province($province){
		$SQLCmd = "SELECT province_name, map_type
  						FROM tbl_province where map_province_name = '{$province}'";
		$rs = $this->db_query($SQLCmd) ;
		$arr = array();
		$arr[0] = '';
		$arr[1] = '';

		if(count($rs)>0){
			$arr[0] = $rs[0]['province_name'];
			$arr[1] = $rs[0]['map_type'];
		}
		return $arr;
	}

	public function nf_to_wf($strs, $types = ""){  //全形半形轉換
		 $nft = array(
			 "(", ")", "[", "]", "{", "}", ".", ",", ";", ":",
			 "-", "?", "!", "@", "#", "$", "%", "&", "|", "\\",
			 "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
			 "^", "_",
			 "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
			 "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
			 "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
			 "u", "v", "w", "x", "y", "z",
			 "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
			 "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			 "U", "V", "W", "X", "Y", "Z",
			 " "
		 );
		 $wft = array(
			 "（", "）", "〔", "〕", "｛", "｝", "﹒", "，", "；", "：",
			 "－", "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
			 "／", "＋", "＝", "＊", "～", "、", "、", "＂", "＜", "＞",
			 "︿", "＿",
			 "０", "１", "２", "３", "４", "５", "６", "７", "８", "９",
			 "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ",
			 "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
			 "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ",
			 "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ",
			 "Ｋ", "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ",
			 "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ",
			 "　"
		 );
	  
		 if ($types == '1'){
			 // 轉全形
			 $strtmp = str_replace($nft, $wft, $strs);
		 }else{
			 // 轉半形
			 $strtmp = str_replace($wft, $nft, $strs);
		 }
		 return $strtmp;
	 }

}
?>
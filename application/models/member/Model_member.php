<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_member extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getMemberAllCnt()
	{
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, '');
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and to_num = {$to_flag_num}" ;
		}

		$SQLCmd = "select count(*) cnt from tbl_member where status <> 'D' and SUBSTR(user_id,1,3) != 'MOD' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getMemberList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, '');
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
 		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and to_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT *
  						FROM tbl_member where status <> 'D' and SUBSTR(user_id,1,3) != 'MOD' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得指定　member_code　的 member info
	 */
	public function getMemberInfoAssign( $member_code = "" )
	{
		if ( !empty( $member_code) )	$whereStr = "where member_code='{$member_code}'" ;
		else $whereStr = "" ;

		$SQLCmd = "SELECT member_code, member_set FROM tbl_member {$whereStr}" ;
		// echo "SQLCmd=".$SQLCmd."<BR>";
		$rs = $this->db_query($SQLCmd) ;
		return $rs ;
	}
	
	/**
	 * 取得单一 member info
	 */
	public function getMemberInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_member where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_member", $whereArr ) ;
		return $rs ;
	}
	
	//sub_member要取得
	public function getMemberALL(){
		$SQLCmd = "SELECT s_num, tde01
  						FROM tbl_member where status <> 'D'
						order by s_num";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}
	
		/**
	 * 新增 tbl_member
	 */
	public function insertData()
	{
		date_default_timezone_set('Asia/Taipei');
		$nowDate = date("Y-m-d H:i:s");
		//检查栏位资料
		//$this->checkFieldData('add');
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		$card_expired_month = $this->input->post("card_expired_month");
		$card_expired_year = $this->input->post("card_expired_year");
		$dataArr['card_expired_date'] = $card_expired_year.'-'.$card_expired_month;

		$member_type_arr = $this->input->post("member_type");
		$member_type = implode (",", $member_type_arr);
		$dataArr['member_type']  = $member_type;
		//$dataArr['user_id'] = (($dataArr['personal_id'] != '')?md5($dataArr['personal_id']):'');

		$dataArr['user_id'] = (($dataArr['user_id'] != '')?str_pad($dataArr['user_id'],32,"0",STR_PAD_LEFT):'');

		//加密设定
		/*$encryption_arr = array('personal_id', 'birthday', 'account_no', 'card_expired_date', 'card_cvv');
		$key = md5('ebike'.$dataArr['mobile']);
		$key = substr($key, 0, 24);
		for($i=0; $i<count($encryption_arr); $i++){
			if($dataArr[$encryption_arr[$i]]  != ''){
				$string = $dataArr[$encryption_arr[$i]] ;
				//加密
				$dataArr[$encryption_arr[$i]]  = $this->encrypt($key, $string);
			}
		}*/
		
		//处理图片
		$img_array = array('pic_face','pic_id_front','pic_id_back','pic_driver_front','pic_driver_back');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$file = fopen($_FILES[$img_array[$i]]["tmp_name"], "rb");
				// 读入图片档资料
				$fileContents = fread($file, filesize($_FILES[$img_array[$i]]["tmp_name"])); 
				//关闭图片档
				fclose($file);
				// 图片档案资料编码
				$dataArr[$img_array[$i]]  =$fileContents;
			}
		}

		$dataArr['create_user'] = $this->session->userdata('user_sn');
		$dataArr['create_date'] = $nowDate;
		$dataArr['create_ip'] = $this->input->ip_address();

		// $this->db_insert( tableName, 新增的栏位与资料 );
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_member", $dataArr ) ) {
			//新增纪录档
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			//查新增后的sn
			$SQLCmd = "SELECT s_num FROM tbl_member WHERE create_date = '{$nowDate}' AND create_user = {$this->session->userdata('user_sn')}";
			$rs = $this->db_query($SQLCmd);
			$s_num = $rs[0]['s_num'];
			$whereStr = "s_num = '{$s_num}'";
			if($dataArr['status'] == 'Y'){
				//發MAIL通知
				/*$email = $dataArr['email'] ;
				$sendmail = $this->model_background->sendemail_default($email,"綠農創意注册会员审核成功通知","綠農創意注册会员审核成功通知");
				if($sendmail){
					//更新状态
					$update_arr = array();
					$update_arr['send_sms_mail'] = 'Y';
					$this->db_update( "tbl_member", $update_arr, $whereStr );
				}*/
			}
			$this->redirect_alert("./index", $this->lang->line('add_successfully')) ;
		} else {
			//失败纪录
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key, false);
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_member
	 */
	public function updateData()
	{

		$postdata = $this->input->post("postdata");
		$s_num = $this->input->post("s_num");
		
		$colDataArr = array();
		foreach($postdata as $key => $value)
		{
			$colDataArr[$key] = $value;
		}

		if(!isset($postdata['eng_staff']))
			$colDataArr['eng_staff'] = 0;
		$member_type_arr = $this->input->post("member_type");
		$member_type = implode (",", $member_type_arr);
		$colDataArr['member_type']  = $member_type;

		$whereStr = "s_num = '{$s_num}'";

		$card_expired_month = $this->input->post("card_expired_month");
		$card_expired_year = $this->input->post("card_expired_year");

		$colDataArr['card_expired_date'] = $card_expired_year.'-'.$card_expired_month;
		//$colDataArr['user_id'] = (($colDataArr['personal_id'] != '')?md5($colDataArr['personal_id']):'');
		$colDataArr['user_id'] = (($colDataArr['user_id'] != '')?str_pad($colDataArr['user_id'],32,"0",STR_PAD_LEFT):'');
		
		//加密设定
		/*$encryption_arr = array('personal_id', 'birthday', 'account_no', 'card_expired_date', 'card_cvv');
		$key = md5('ebike'.$colDataArr['mobile']);
		$key = substr($key, 0, 24);
		for($i=0; $i<count($encryption_arr); $i++){
			if($colDataArr[$encryption_arr[$i]]  != ''){
				$string = $colDataArr[$encryption_arr[$i]] ;
				//加密
				$colDataArr[$encryption_arr[$i]]  = $this->encrypt($key, $string);
			}
		}*/

		//处理图片
		$img_array = array('pic_face','pic_id_front','pic_id_back','pic_driver_front','pic_driver_back');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$file = fopen($_FILES[$img_array[$i]]["tmp_name"], "rb");
				// 读入图片档资料
				$fileContents = fread($file, filesize($_FILES[$img_array[$i]]["tmp_name"])); 
				//关闭图片档
				fclose($file);
				// 图片档案资料编码
				$colDataArr[$img_array[$i]]  =$fileContents;
			}
		}

		$colDataArr['update_user'] = $this->session->userdata('user_sn');
		$colDataArr['update_date'] = "now()";
		$colDataArr['update_ip'] = $this->input->ip_address();

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_member", $colDataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key);
			$old_send_sms_mail = $this->input->post("old_send_sms_mail");
			//如果狀態為1時，$colDataArr['user_id']
			if($old_send_sms_mail == 'N'){
				if($colDataArr['status'] == 'Y'){
					//發MAIL通知
					/*$email = $colDataArr['email'] ;
					$sendmail = $this->model_background->sendemail_default($email,"綠農創意注册会员审核成功通知","綠農創意注册会员审核成功通知");
					if($sendmail){
						//更新状态
						$update_arr = array();
						$update_arr['send_sms_mail'] = 'Y';
						$this->db_update( "tbl_member", $update_arr, $whereStr );
					}*/
				}
			}
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$s_num.']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	/**
	 * 删除 tbl_member
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"						=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_ip"				=> $this->input->ip_address(),
			"delete_date"			=> "now()"
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_member", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	public function encrypt($source,$toencrypt){  
		//加密用的key   

		$key = $source;  

		//使用3DES方法加密   

		$encryptMethod = MCRYPT_TRIPLEDES; 

		//初始化向量来增加安全性

		$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptMethod,MCRYPT_MODE_ECB), MCRYPT_RAND);  

		//使用mcrypt_encrypt函数加密，MCRYPT_MODE_ECB表示使用ECB模式

		$encrypted_toencrypt = mcrypt_encrypt($encryptMethod, $key, $toencrypt, MCRYPT_MODE_ECB,$iv);   

		//回传解密后字串

		return base64_encode($encrypted_toencrypt);  

	}  

	//解密函数撰写
	public function decrypt($source,$todecrypt) {  

		//解密用的key，必须跟加密用的key一样   

		$key = $source;  

		//解密前先解开base64码

		$todecrypt = base64_decode($todecrypt);

		//使用3DES方法解密

		$encryptMethod = MCRYPT_TRIPLEDES;  

		//初始化向量来增加安全性 

		$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptMethod,MCRYPT_MODE_ECB), MCRYPT_RAND);  

		//使用mcrypt_decrypt函数解密，MCRYPT_MODE_ECB表示使用ECB模式  

		$decrypted_todecrypt = mcrypt_decrypt($encryptMethod, $key, $todecrypt, MCRYPT_MODE_ECB,$iv);

		//回传解密后字串
		$decrypted_todecrypt = $this->removePadding($decrypted_todecrypt);
		return $decrypted_todecrypt;  
	}  

   //删除填充符
    public function removePadding( $str )
    {
        $len = strlen( $str );
        $newstr = "";
        $str = str_split($str);
        for ($i = 0; $i < $len; $i++ )
        {
            if ($str[$i] != chr( 0 ))
            {
                $newstr .= $str[$i];
            }
        }
        return $newstr;
    }

	public function getDetailAllCnt( $member_sn = '', $detail_type = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if($detail_type == 'record'){
			$whereStr = "";
			$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
			if($search_txt != ''){
				$whereStr .= " and tm.name like '%{$search_txt}%'";
			}

			//搜寻对应
			//ex "field_name"=> "Alias"
			$setAlias = array(
				"ltop.top01"	=> "Y",
				"ltd.tde01"		=> "Y",
				"rtop.top01"	=> "Y",
				"rtd.tde01"		=> "Y"
			);		
			if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
			else $whereStr .= "" ;

			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and tm.to_num = {$to_flag_num}" ;
			}

			// if ( !empty( $search) )	$whereStr .= "AND ";
			$SQLCmd  = "SELECT count(*) cnt
									FROM log_battery_leave_return lblr 
									LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
									LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
									LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
									LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
									LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
									LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
									LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
									LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
									LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
									LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
									LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
									where lblr.tm_num = '{$member_sn}' ".$whereStr;
		}else{
			$whereStr = "";
			//$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
			//if($search_txt != ''){
			//	$whereStr .= " and tm.name like '%{$search_txt}%'";
			//}

			//搜寻对应
			//ex "field_name"=> "Alias"
			/*$setAlias = array(
				"ltop.top01"	=> "Y",
				"ltd.tde01"		=> "Y",
				"rtop.top01"	=> "Y",
				"rtd.tde01"		=> "Y"
			);	*/	
			//if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lsbc', $setAlias);
			//else $whereStr .= "" ;

			// if ( !empty( $search) )	$whereStr .= "AND ";
			$SQLCmd  = "SELECT count(*) cnt
									FROM log_store_balance_charge lsbc 
									LEFT JOIN log_battery_leave_return lblr ON lsbc.blr_num = lblr.s_num
									where lblr.tm_num = '{$member_sn}' ".$whereStr;
		
		}
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $member_sn = '', $detail_type = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');
		if($detail_type == 'record'){

			if ( empty( $startRow ) ) $startRow = 0 ;
			$whereStr  = '';

			$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
			if($search_txt != ''){
				$whereStr .= " and tm.name like '%{$search_txt}%'";
			}

			//搜寻对应
			//ex "field_name"=> "Alias"
			$setAlias = array(
				"ltop.top01"	=> "Y",
				"ltd.tde01"		=> "Y",
				"rtop.top01"	=> "Y",
				"rtd.tde01"		=> "Y"
			);		
			if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
			else $whereStr .= "" ;
			
			//排序动作
			$sql_orderby = " order by lblr.s_num desc";
			$fn = get_fetch_class_random();
			$field = $this->session->userdata("{$fn}_".'field');
			$orderby = $this->session->userdata("{$fn}_".'orderby');
			if($field != "" && $orderby != ""){
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
			
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = $this->session->userdata('to_num'); 
			if($to_flag == '1'){
				$whereStr .= " and tm.to_num = {$to_flag_num}" ;
			}

			$SQLCmd  = "SELECT lblr.*
						,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
						,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name
						,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as rdo_name
						,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as rdso_name, tm.name as member_name, tv.unit_id, 
						concat(r_tbss.location, '（',r_tbss.bss_id,'）') as return_bss_id, 
						concat(l_tbss.location, '（',l_tbss.bss_id,'）') as leave_bss_id
									FROM log_battery_leave_return lblr 
									LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
									LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
									LEFT JOIN tbl_battery_swap_station l_tbss ON lblr.leave_sb_num = l_tbss.s_num 
									LEFT JOIN tbl_battery_swap_station r_tbss ON lblr.return_sb_num = r_tbss.s_num 
									LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
									LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
									LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
									LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
									LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
									LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
									LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
									LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
									where lblr.tm_num = '{$member_sn}' ".$whereStr."
									{$sql_orderby} 
									LIMIT {$startRow} , {$offset} ";
		}else{
			if ( empty( $startRow ) ) $startRow = 0 ;
			$whereStr  = '';

			//$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
			//if($search_txt != ''){
			//	$whereStr .= " and tm.name like '%{$search_txt}%'";
			//}

			//搜寻对应
			//ex "field_name"=> "Alias"
			/*$setAlias = array(
				"ltop.top01"	=> "Y",
				"ltd.tde01"		=> "Y",
				"rtop.top01"	=> "Y",
				"rtd.tde01"		=> "Y"
			);		*/
			//if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lsbc', $setAlias);
			//else $whereStr .= "" ;
			
			//排序动作
			$sql_orderby = " order by lblr.s_num desc";
			$fn = get_fetch_class_random();
			$field = $this->session->userdata("{$fn}_".'field');
			$orderby = $this->session->userdata("{$fn}_".'orderby');
			if($field != "" && $orderby != ""){
				$sql_orderby = " order by ".$field.' '.$orderby;
			}
			$SQLCmd  = "SELECT lsbc.*
									FROM log_store_balance_charge lsbc 
									LEFT JOIN log_battery_leave_return lblr ON lsbc.blr_num = lblr.s_num
									where lsbc.tm_num = '{$member_sn}'  ".$whereStr."
									{$sql_orderby} 
									LIMIT {$startRow} , {$offset} ";
		}
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	public function import(){
	
		/*$filename = "xml/member.txt";
		$str = "";
		//判斷是否有該檔案
		if(file_exists($filename)){
			$file = fopen($filename, "r");
			if($file != NULL){
				//當檔案未執行到最後一筆，迴圈繼續執行(fgets一次抓一行)
				while (!feof($file)) {
					$member_arr = explode(',',fgets($file));
					$user_id = str_pad($member_arr[0],32,"0",STR_PAD_LEFT);
					$openid = trim($member_arr[1]);
					$nickname = trim($member_arr[2]);
					//主檔
					//tmwi_num	//微信sn
					$name = trim($member_arr[2]);
					$mobile = trim($member_arr[3]);
					$account_no = trim($member_arr[4]);
					$deposit = trim($member_arr[8]);
					$lease_token = trim($member_arr[6]);

					$sql = "select tm.s_num, tmwi.s_num as tmwi_num
								from tbl_member tm
								left join tbl_member_wechat_info tmwi ON tmwi.tm_num = tm.s_num
								where tm.user_id = '{$user_id}' and tm.status <> 'D' limit 1";
					$rs = $this->db_query($sql) ;
					if(count($rs)>0){
						$tm_num = $rs[0]['s_num'];
						$tmwi_num = $rs[0]['tmwi_num'];

						if($tmwi_num == ''){
							$insert = array();
							$insert['openid'] = $openid;
							$insert['nickname'] = $nickname;
							$insert['tm_num'] = $tm_num;
							$this->db_insert( "tbl_member_wechat_info", $insert) ;


							//更新主檔
							$updates = array();
							$updates['personal_id'] = $openid;
							$updates['name'] = $name;
							$updates['mobile'] = $mobile;
							$updates['account_no'] = $account_no;
							$updates['deposit'] = $deposit;
							$updates['lease_token'] = $lease_token;
							$updates['member_register'] = 'W';
							$whereStr = "s_num = '{$tm_num}'";
							
							$this->db_update( "tbl_member", $updates, $whereStr );
						}else{
							echo '已有資料：'.$tmwi_num.'<br /> ';
						}
						
						$insert_arr = array();

					}else{
						$insert = array();
						$insert['member_type'] = 'B';
						$insert['user_id'] = $user_id;
						$insert['mobile'] = $mobile;
						$insert['account_no'] = $account_no;
						$insert['deposit'] = $deposit;
						$insert['lease_token'] = $lease_token;
						$insert['member_register'] = 'W';
						$insert['user_id'] = $user_id;
						$insert['name'] = $name;
						$insert['personal_id'] = $openid;
						$this->db_insert( "tbl_member", $insert) ;
						$SQLCmd2 = "SELECT LAST_INSERT_ID() as s_num FROM tbl_member";
						$rs2 = $this->db_query($SQLCmd2);
						$tm_num = $rs2[0]['s_num'];

						$insert = array();
						$insert['openid'] = $openid;
						$insert['nickname'] = $nickname;
						$insert['tm_num'] = $tm_num;
						$this->db_insert( "tbl_member_wechat_info", $insert) ;
						
						echo '查無資料：'.$user_id.' 並寫入：'.$tm_num.'<br />';
					}


//`id`, `openid`, `name`, `mobile`, `idcard`, `status`, `token`, `deposit_status`, `deposit_price`, `ctime`, `level`, `valid_date`
					$str .= fgets($file).'<br />';
				}
				fclose($file);
			}
		}*/
		//echo $str;
	
	
	
	}

}
/* End of file model_member.php */
/* Location: ./application/models/model_member.php */
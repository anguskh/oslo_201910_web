<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_boundbattery extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getBound_batteryAllCnt()
	{
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lcf');
		else $whereStr .= "" ;

		$SQLCmd = "select count(*) cnt 
					from tbl_member tm
					where tm.member_type = 'F' and tm.status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	public function select_battery_count(){
		$member_sn = $this->input->post("member_sn");
		$whereStr = " and tm_num = '{$member_sn}'";
		$SQLCmd = "select leave_battery_id
									from log_battery_leave_return 
									where return_date is null  {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		$return = array();
		$retrun[0] = count($rs);
		$battery_list = "";
		if(count($rs)>0){
			$n=1;
			foreach($rs as $rs_arr){
				$battery_list .= "電池序號".$n."：".$rs_arr['leave_battery_id'].'<br />';
				$n++;
			}
		}
		$retrun[1] = $battery_list;

		echo json_encode($retrun);
	}

	/**
	 * 取得参数清单
	 */
	public function getBound_batteryList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lcf');
		else $whereStr .= "" ;


		$SQLCmd = "SELECT tm.*, (select count(*) from log_battery_leave_return where tm_num = tm.s_num and return_date is null) as battery_count
  						FROM  tbl_member tm
						where tm.member_type = 'F' and tm.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		return $rs;
	}
	
		/**
	 * 新增 log_client_boundbattery
	 */
	public function insertData()
	{
		$tmpdataArr = "";
		$tmpdataArr = $this->input->post("postdata");
		$key_num = $this->input->post("key_num");
		//查user_sn的user_id
		$tm_arr = $this->Model_show_list->getMemberList($tmpdataArr['tm_num']);
		if(count($tm_arr)>0){
			$tmpdataArr['battery_user_id'] = $tm_arr[0]['user_id'];
		}
		
		$add_flag = "N";
		for($i=1; $i<=$key_num; $i++){
			if($this->input->post("leave_sb_num_".$i) != '' && $this->input->post("leave_battery_id_".$i) != ''){
				$dataArr = $tmpdataArr;
				

				$dataArr['leave_sb_num'] = $this->input->post("leave_sb_num_".$i);
				$dataArr['leave_battery_id'] = $this->input->post("leave_battery_id_".$i);
				$dataArr['create_user'] = $this->session->userdata("user_sn");
				$dataArr['system_log_date'] = "now()";
				$dataArr['leave_date'] = "now()";
				$dataArr['leave_status'] = 'W';//補登


				$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
				$this->session->set_userdata("sql_start", true); 
				if ( $this->db_insert( "log_battery_leave_return", $dataArr) ) {
					//纪录新增成功资料
					$target_key = '';
					$this->session->set_userdata("desc", $dataArr);
					$this->model_background->log_operating_insert('1', $target_key);
					$add_flag = "Y";
				} else {
					//纪录新增失败资料
					$this->session->set_userdata("desc", $dataArr);
					$target_key = '';
					$this->model_background->log_operating_insert('1', $target_key, false);
					
					$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
				}



			}

		}

		if($add_flag == "Y"){
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		}else{
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
}
/* End of file model_boundbattery.php */
/* Location: ./application/models/model_boundbattery.php */
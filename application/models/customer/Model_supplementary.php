<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_supplementary extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getSupplementaryAllCnt()
	{
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lcf');
		else $whereStr .= "" ;

		/*$SQLCmd = "select count(*) cnt 
					from log_client_supplementary lcf
					LEFT JOIN tbl_member tm ON tm.s_num = lcf.tm_num and tm.status <> 'D'
					where lcf.status <> 'D' {$whereStr}";
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];*/

		return 0;
	}

	/**
	 * 取得参数清单
	 */
	public function getSupplementaryList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		$search = $this->session->userdata("{$fn}_".'searchData');
		//搜寻对应
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lcf');
		else $whereStr .= "" ;

		$SQLCmd = "SELECT lcf.*, tm.name
  						FROM log_client_supplementary lcf
						LEFT JOIN tbl_member tm ON tm.s_num = lcf.tm_num and tm.status <> 'D'
						where lcf.status <> 'D' {$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset} " ;
		//$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		//return $rs;
		return array() ;
	}
	
		/**
	 * 新增 log_client_supplementary
	 */
	public function insertData()
	{
		$dataArr = "";
		$dataArr = $this->input->post("postdata");
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['system_log_date'] = "now()";
		$dataArr['leave_date'] = "now()";
		$dataArr['leave_status'] = 'W';//補登

		//查user_sn的user_id
		$tm_arr = $this->Model_show_list->getMemberList($dataArr['tm_num']);
		if(count($tm_arr)>0){
			$dataArr['battery_user_id'] = $tm_arr[0]['user_id'];
		}

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "log_battery_leave_return", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}
}
/* End of file model_supplementary.php */
/* Location: ./application/models/model_supplementary.php */
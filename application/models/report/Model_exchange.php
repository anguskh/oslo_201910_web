<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exchange extends MY_Model {
	
	/**
	 * 取得所有资料笔数 Total Count
	 */
	public function getAllCnt()
	{

		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and ldi.DorO_flag = 'O' and ldi.do_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT count(*) cnt 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id  and tb.status <> 'D'
						WHERE ldi.log_type = 'B' and tb.battery_id != '' {$whereStr}" ;

		$rs = $this->db_query($SQLCmd) ;



		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getList( $offset, $startRow = "" )
	{
		if ( empty( $startRow ) ) $startRow = 0 ;
		
		//排序动作
		$sql_orderby = " order by tb.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and ldi.unit_id like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		
		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'ldi');
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and ldi.DorO_flag = 'O' and ldi.do_num = {$to_flag_num}" ;
		}

		$SQLCmd = "SELECT ldi.unit_id as battery_id,
							tb.manufacture_date,
							CONCAT(ldi.latitude,',',ldi.longitude) as gps_position,
							CONCAT(ldi. battery_capacity,'%') as battery_capacity 
  						FROM `log_driving_info` ldi 
  						LEFT JOIN tbl_battery tb ON ldi.unit_id = tb.battery_id  and tb.status <> 'D'
						WHERE ldi.log_type = 'B' and tb.battery_id != ''
						{$whereStr} {$sql_orderby} 
  						LIMIT {$startRow} , {$offset}" ;

		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}
	
	/**
	 * 取得单一 battery info
	 */
	public function getInfo( $s_num = "" )
	{
		
		$whereArr = array ( "s_num" => $s_num ) ;
		$SQLCmd = "SELECT * FROM tbl_battery where s_num={$s_num}" ;
		$rs = $this->db_query($SQLCmd) ;
		// $rs = $this->db_quert_where( "tbl_battery", $whereArr ) ;
		return $rs ;
	}

	public function search_all( $whereStr = "")
	{
		$return = array();
		
		$whereStr2 = "";
		$whereStr3 = "";
		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and so_num = {$to_flag_num}" ;
			$whereStr2 .= " and so_num = {$to_flag_num}" ;
			$whereStr3 .= " where so_num = {$to_flag_num}" ;
		}

		//算起始天數
		$SQLCmd8 = "SELECT DATEDIFF(MAX(log_date),MIN(log_date)) as log_days 
			 FROM log_bss_info where {$whereStr}";
		$rs8 = $this->db_query($SQLCmd8) ;
		$log_days = $rs8[0]['log_days'];	//相差天數
		$log_days = (($log_days == '')?1:$log_days);

		$SQLCmd1 = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE status <> 'D' {$whereStr2}" ;
		$rs1 = $this->db_query($SQLCmd1) ;
		$statino_num = $rs1[0]['cnt'];	
		$return[0] = $statino_num; //換電站

		//換電站電池總計交換次數
		$SQLCmd9 = "SELECT SUM(exchange_num) as exchange_num FROM tbl_battery_swap_station {$whereStr3}";
		$rs9 = $this->db_query($SQLCmd9) ;
		$exchange_num = $rs9[0]['exchange_num'];
		$return[1] = $exchange_num;

		$SQLCmd2 = "SELECT SUM(exchange_num)/(DATEDIFF(MAX(log_date),MIN(log_date))) as avgNum FROM log_bss_info where {$whereStr}";
		$rs2 = $this->db_query($SQLCmd2) ;
		$avgNum = $rs2[0]['avgNum'];	
		$return[2] = $avgNum; //換電站電池平均每日交換次數

		//a5 = a3/a1 單一換電站平均電池交換次數
		$return[4] = round(($avgNum/$statino_num),4) ;

		$SQLCmd3 = "SELECT bss_id,exchange_num FROM tbl_battery_swap_station WHERE status <> 'D' {$whereStr2} ORDER BY exchange_num DESC";
		$rs3 = $this->db_query($SQLCmd3) ;
		$maxNum = $rs3[0]['exchange_num'];	
		$return[5] = $maxNum; //單一換電站最多電池交換次數：
		$minNum = $rs3[count($rs3)-1]['exchange_num'];	
		$return[6] = $minNum; //單一換電站最少電池交換次數：
		// $SQLCmd4 = "SELECT MIN(exchange_num) as minNum FROM tbl_battery_swap_station WHERE status <> 'D'";
		// $rs4 = $this->db_query($SQLCmd4) ;
		// $minNum = $rs4[0]['minNum'];	
		// $return[6] = $minNum; //單一換電站最少電池交換次數：

		$SQLCmd5 = "SELECT b.sb_num,MAX(b.dayMaxNum) as sb_dayMaxNum
		 FROM (SELECT a.sb_num,a.log_date,MAX(a.daySUMNum) as dayMaxNum
		 FROM (SELECT sb_num,SUBSTRING(log_date,1,10) as log_date,SUM(exchange_num) as daySUMNum 
		 FROM log_bss_info where {$whereStr}
		 GROUP BY sb_num,SUBSTRING(log_date,1,10) ) as a
		 GROUP BY a.sb_num ) as b";
		$rs5 = $this->db_query($SQLCmd5) ;
		$sb_dayMaxNum = $rs5[0]['sb_dayMaxNum'];	
		$return[7] = $sb_dayMaxNum; //單一換電站單日最多電池交換次數：

		$SQLCmd6 = "SELECT b.sb_num,MIN(b.dayMinNum) as sb_dayMinNum 
		 FROM (SELECT a.sb_num,a.log_date,MIN(a.daySUMNum) as dayMinNum
		 FROM (SELECT sb_num,SUBSTRING(log_date,1,10) as log_date,SUM(exchange_num) as daySUMNum 
		 FROM log_bss_info where {$whereStr}
		 GROUP BY sb_num,SUBSTRING(log_date,1,10) ) as a
		 GROUP BY a.sb_num ) as b";
		$rs6 = $this->db_query($SQLCmd6) ;
		$sb_dayMinNum = $rs6[0]['sb_dayMinNum'];	
		$return[8] = $sb_dayMinNum; //單一換電站單日最少電池交換次數：

		$SQLCmd7 = "SELECT DATE_FORMAT(log_date,'%H') as hour,SUM(exchange_num)/{$statino_num} as housr_avgNum 
			 FROM log_bss_info where {$whereStr}
			 GROUP BY DATE_FORMAT(log_date,'%H')
			 ORDER BY DATE_FORMAT(log_date,'%H')";
		$rs7 = $this->db_query($SQLCmd7) ;
		$hour_arr = array();
		if($rs7){
			foreach($rs7 as $key => $value){
				$hour_arr[$value['hour']] = $value['housr_avgNum'];
			}
		}
		$return[3] = $hour_arr;

		$SQLCmd9 = "SELECT DATE_FORMAT(log_date,'%H') as hour,SUM(exchange_num)/{$statino_num}/{$log_days} as housr_avgNum 
			 FROM log_bss_info where {$whereStr}
			 GROUP BY DATE_FORMAT(log_date,'%H')
			 ORDER BY DATE_FORMAT(log_date,'%H')";
		$rs9 = $this->db_query($SQLCmd9) ;
		$hour9_arr = array();
		if($rs9){
			foreach($rs9 as $key => $value){
				$hour9_arr[$value['hour']] = number_format(round($value['housr_avgNum'],4),4);
				
			}
		}
		$return[9] = $hour9_arr;

		$SQLCmd10 = "SELECT 
						b.sb_num,b.dayMaxNum, b.log_date, b.bss_id
							FROM (
									SELECT a.sb_num,a.log_date,MAX(a.daySUMNum) as dayMaxNum, a.bss_id
										FROM (
											SELECT lbi.sb_num,tbss.bss_id,SUBSTRING(lbi.log_date,1,10) as log_date,SUM(lbi.exchange_num) as daySUMNum 
											FROM log_bss_info lbi 
											left join tbl_battery_swap_station tbss ON tbss.s_num = lbi.sb_num and tbss.status <> 'D'
											where {$whereStr}
											GROUP BY lbi.sb_num,SUBSTRING(lbi.log_date,1,10) 
										) as a
							GROUP BY a.sb_num ) as b
							order by b.dayMaxNum desc
							limit 10";
		$rs10 = $this->db_query($SQLCmd10) ;
		$lo_arr = array();
		if($rs10){
			foreach($rs10 as $arrs){
				$lo_arr[$arrs['sb_num']]['dayMaxNum'] = $arrs['dayMaxNum'];
				$lo_arr[$arrs['sb_num']]['bss_id'] = $arrs['bss_id'];
			}
		}
		$return[10] = $lo_arr;

		$SQLCmd11 = "SELECT 
						b.sb_num,b.dayMaxNum, b.log_date, b.bss_id
							FROM (
									SELECT a.sb_num,a.log_date,MIN(a.daySUMNum) as dayMaxNum, a.bss_id
										FROM (
											SELECT lbi.sb_num,tbss.bss_id,SUBSTRING(lbi.log_date,1,10) as log_date,SUM(lbi.exchange_num) as daySUMNum 
											FROM log_bss_info lbi 
											left join tbl_battery_swap_station tbss ON tbss.s_num = lbi.sb_num and tbss.status <> 'D'
											where {$whereStr}
											GROUP BY lbi.sb_num,SUBSTRING(lbi.log_date,1,10) 
										) as a
							GROUP BY a.sb_num ) as b
							order by b.dayMaxNum asc 
							limit 10";
		$rs11 = $this->db_query($SQLCmd11) ;
		$lo11_arr = array();
		if($rs11){
			foreach($rs11 as $arrs){
				$lo11_arr[$arrs['sb_num']]['dayMaxNum'] = $arrs['dayMaxNum'];
				$lo11_arr[$arrs['sb_num']]['bss_id'] = $arrs['bss_id'];
			}
		}
		$return[11] = $lo11_arr;

		// $SQLCmd12 = "SELECT exchange_num FROM";
		// $rs11 = $this->db_query($SQLCmd11) ;

		$return[12] = $rs3;

		return $return ;
	}

	/**
	 * 新增 tbl_battery
	 */
	public function insertData()
	{
		
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("sd_num");
		}else{
			$dataArr['do_num'] = $this->input->post("so_num");
		}
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_insert( "tbl_battery", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_successfully')}") ;
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('add_failed')}") ;
		}
	}

	/**
	 * 修改 tbl_battery
	 */
	public function updateData()
	{
		//检查栏位资料
		// $this->checkFieldData('edit');
		
		$whereStr = " s_num={$this->input->post('s_num') }" ;
		$dataArr = "";
		$dataArr = $this->input->post("postdata");

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = $this->input->post("sd_num");
		}else{
			$dataArr['do_num'] = $this->input->post("so_num");
		}
		$dataArr['update_user'] = $this->session->userdata("user_sn");
		$dataArr['update_date'] = "now()";
		$dataArr['update_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_update( "tbl_battery", $dataArr, $whereStr ) ) {
			//纪录修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_successfully')}") ;
		} else {
			//失败修改资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '[s_num: '.$this->input->post("s_num").']';
			$this->model_background->log_operating_insert('2', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('edit_failed')}") ;
		}
	}
	
	//检查栏位资料
	function checkFieldData($act){
		$checkArr = array(
		);
		
		if($act == 'add'){
			
		}else{
			$checkArr["s_num, s_num"] = array("required" => "", "integer" => "");
		}
		
		$retuen = $this->model_checkfunction->checkfunction($checkArr);
		if($retuen != ''){
			$this->redirect_alert("./index", $retuen) ;
			//echo $retuen;
			exit();
		}
	}

	/**
	 * 删除 sys_user
	 */
	public function deleteData()
	{
		$ckbSelArr = $this->input->post("ckbSelArr");
		
		$delIdStr = join( "','", $ckbSelArr );
		$delIdStr = "'".$delIdStr."'";
		
		$whereStr = " s_num in ({$delIdStr}) ";
		$colDataArr = array (
			"status"				=> "D",
			"delete_user"			=> $this->session->userdata('user_sn'), 
			"delete_date"			=> "now()",
			"delete_ip"				=> $this->input->ip_address()
		) ;

		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		if ( $this->db_delete( "tbl_battery", $colDataArr, $whereStr ) ) {
			//纪录删除资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key);
		
			$this->redirect_alert("./index", "{$this->lang->line('delete_successfully')}") ;
		}else{
			//纪录删除失败资料
			$this->session->set_userdata("desc", $colDataArr);
			$target_key = '[s_num: '.$delIdStr.']';
			$this->model_background->log_operating_insert('3', $target_key, false);
			
			$this->redirect_alert("./index", "{$this->lang->line('delete_failed')}") ;
		}
	}

	public function getDetailAllCnt( $battery_id = '')
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData');
		$whereStr = "";
 		$search_txt = $this->session->userdata("{$fn}_".'search_txt'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;

		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tm.to_num = {$to_flag_num}" ;
		}


		// if ( !empty( $search) )	$whereStr .= "AND ";
		$SQLCmd  = "SELECT count(*) cnt
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where lblr.leave_battery_id = '{$battery_id}' ".$whereStr;
		$rs = $this->db_query($SQLCmd) ;
		return $rs[0]["cnt"];
	}

	/**
	 * 取得参数清单
	 */
	public function getDetailList( $offset, $startRow = "", $battery_id = '' )
	{
		$fn = get_fetch_class_random();
		$search = $this->session->userdata("{$fn}_".'searchData_detail');

		if ( empty( $startRow ) ) $startRow = 0 ;
		$whereStr  = '';

 		$search_txt = $this->session->userdata("{$fn}_".'search_txt_detail'); 
		if($search_txt != ''){
			$whereStr .= " and tm.name like '%{$search_txt}%'";
		}

		//搜寻对应
		//ex "field_name"=> "Alias"
		$setAlias = array(
			"ltop.top01"	=> "Y",
			"ltd.tde01"		=> "Y",
			"rtop.top01"	=> "Y",
			"rtd.tde01"		=> "Y"
		);		
		if ( !empty( $search) ) $whereStr .= setSearchData($search, 'lblr', $setAlias);
		else $whereStr .= "" ;
		
		//排序动作
		$sql_orderby = " order by lblr.s_num desc";
		$fn = get_fetch_class_random();
		$field = $this->session->userdata("{$fn}_".'field');
		$orderby = $this->session->userdata("{$fn}_".'orderby');
		if($field != "" && $orderby != ""){
			$sql_orderby = " order by ".$field.' '.$orderby;
		}
	    
		$to_flag = $this->session->userdata('to_flag'); 
		$to_flag_num = $this->session->userdata('to_num'); 
		if($to_flag == '1'){
			$whereStr .= " and tm.to_num = {$to_flag_num}" ;
		}

		$SQLCmd  = "SELECT lblr.*
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltd.tde01 WHEN 'O' THEN ltop.top01 END ) as ldo_name
					,(CASE lblr.leave_DorO_flag WHEN 'D' THEN ltsd.tsde01 WHEN 'O' THEN ltso.tsop01 END ) as ldso_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtd.tde01 WHEN 'O' THEN rtop.top01 END ) as rdo_name
					,(CASE lblr.return_DorO_flag WHEN 'D' THEN rtsd.tsde01 WHEN 'O' THEN rtso.tsop01 END ) as rdso_name, tm.name as member_name, tv.unit_id, tbss.bss_id as return_bss_id
								FROM log_battery_leave_return lblr 
								LEFT JOIN tbl_vehicle tv ON lblr.tv_num = tv.s_num 
								LEFT JOIN tbl_member tm ON lblr.tm_num = tm.s_num 
								LEFT JOIN tbl_battery_swap_station tbss ON lblr.return_sb_num = tbss.s_num 
								LEFT JOIN tbl_dealer ltd ON lblr.leave_do_num = ltd.s_num 
								LEFT JOIN tbl_sub_dealer ltsd ON lblr.leave_dso_num = ltsd.s_num 
								LEFT JOIN tbl_operator ltop ON lblr.leave_do_num = ltop.s_num 
								LEFT JOIN tbl_sub_operator ltso ON lblr.leave_dso_num = ltso.s_num 
								LEFT JOIN tbl_dealer rtd ON lblr.return_do_num = rtd.s_num 
								LEFT JOIN tbl_sub_dealer rtsd ON lblr.return_dso_num = rtsd.s_num 
								LEFT JOIN tbl_operator rtop ON lblr.return_do_num = rtop.s_num 
								LEFT JOIN tbl_sub_operator rtso ON lblr.return_dso_num = rtso.s_num 
								where lblr.leave_battery_id = '{$battery_id}' ".$whereStr."
								{$sql_orderby} 
  								LIMIT {$startRow} , {$offset} ";
		$rs = $this->db_query($SQLCmd) ;
		
		//记录查询log
		$this->model_background->log_insert_search('', $SQLCmd);
		
		return $rs ;
	}

	//確認电池序号唯一
	public function ajax_pk_battery_id(){
		//由jquery.validationEngine-zh_TW 函式"ajaxOldPassword" 取得資料
		$validateValue = $_GET['fieldValue'];
		$validateId = $_GET['fieldId'];
		$fieldnum = $_GET['fieldnum'];
		
		$result = $this->countbattery_id($validateValue, $fieldnum);
		$arrayToJs = array();
		//一定要帶回$validateId
		$arrayToJs[0] = $validateId;
		if($result === true){
			$arrayToJs[1] = true;	//無重複
		}else{
			$arrayToJs[1] = false;	//有重複
		}
		echo json_encode($arrayToJs);
	}

	//查出是否有重複
	public function countbattery_id($battery_id, $fieldnum){
		$where = "";
		if($battery_id != ''){
			if($fieldnum != ''){
				$where = " and tb.s_num != '{$fieldnum}'";
			}
			$SQLCmd = "	select count(*) as cnt
						from tbl_battery tb 
						where tb.battery_id = '{$battery_id}' and tb.status <> 'D' {$where}";
			$rs = $this->db_query($SQLCmd);		
			if($rs[0]['cnt'] == 0){  //無資料
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	/**
	 * upload_新增 tbl_battery
	 */
	public function upload_insertData($dataArr)
	{

		if($dataArr['DorO_flag'] == 'D'){
			$dataArr['do_num'] = is_null($dataArr['sd_num'])?'':$dataArr['sd_num'];
		}else{
			$dataArr['do_num'] = is_null($dataArr['so_num'])?'':$dataArr['so_num'];
		}
		unset($dataArr['sd_num']);
		unset($dataArr['so_num']);
		$dataArr['create_user'] = $this->session->userdata("user_sn");
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();
		$this->session->set_userdata('PageStartRow', $this->input->post("start_row"));
		$this->session->set_userdata("sql_start", true); 
		$add_flag = 'N';
		if ( $this->db_insert( "tbl_battery", $dataArr) ) {
			//纪录新增成功资料
			$target_key = '';
			$this->session->set_userdata("desc", $dataArr);
			$this->model_background->log_operating_insert('1', $target_key);
			$add_flag = 'Y';
		} else {
			//纪录新增失败资料
			$this->session->set_userdata("desc", $dataArr);
			$target_key = '';
			$this->model_background->log_operating_insert('1', $target_key, false);
			
		}
		return $add_flag;
	}

}
/* End of file model_config.php */
/* Location: ./application/models/model_config.php */
<?
	$get_full_url_random = get_full_url_random();
	$web = $this->config->item('base_url');
	$apiurl = "http://kroslo.anguskh.com/api/bss/oslobss";
	// $apiurl = "http://127.0.0.1/kroslo_api/bss/oslobss";
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
<script type="text/javascript" src="<?=$web?>js/common/common.js"></script>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="designer" content="" />
<meta name="viewport" content="width=device-width">
<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet">
<link href="<?=$web.$newcss;?>newcss/bootstrap.css" rel="stylesheet">
<script src="<?=$web;?>js/jquery-1.11.3.min.js"></script>
<script src="<?=$web;?>newjs/tooltip.js"></script>
<!--<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})
</script>-->
<!-- 可輸入可下拉 -->
<link rel="stylesheet" href="<?=$web?>js/easyui/easyui.css"/>
<script type="text/javascript" src="<?=$web?>js/easyui/jquery.easyui.min.js"></script> 
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<p class="btitle">機櫃模擬器(國際版)</p>
		<div class="section" style="overflow: hidden;">
			<div class="container-box col-md-3">
				<ul>
					<li>
						<label for="">請選擇機櫃序號：</label>
						<select id="bss_id">
							<?php
								foreach($bssInfo as $key => $value)
								{
									echo "<option value='{$value['bss_id']}' snum='{$value['s_num']}' bsstoken='{$value['bss_token']}'>{$value['bss_id']}({$value['location']})</option>";
								}
							?>
						</select>
						<!-- <div id="bss_qrcode">
							<img style="width:200px;height:200px;" src='<?=$web?>simulator/bss/createQR_Code/<?=$bssInfo[0]['bss_id']?>'/>
						</div> -->
					</li>
					<li>
						<button id='bss_report' name='bss_return'>監控回報</button>
					</li>
				</ul>
			</div>
			<div class="container-box col-md-6">
				<div class="battery_box" style="margin:0px;float:left;">
					<p id="bss_title" class="btitle"><?=$location;?>（<?=$bss_id;?>）</p>
					<!-- 銀白色長方形的電池圖=有電池、黑色長方形的電池圖=沒電池no 紅色 故障error-->
					<p class="info">
						<span class="error"></span><em>故障</em>
						<span class="stop"></span><em>停用</em>
						<span class="no"></span><em>空軌</em>
						<span class="charge"></span><em>充電中</em>
						<span></span><em>飽電</em>
					</p>
					<div class="machine">
						<div class="pad">
							<p class="point"></p>
							<div class="screen" id="screen"></div>
						</div>
						<ul id="bss" class="battery-track"><!--判面-->
		<?
								$battery_cell_name = array(""=>"","0"=>"N/A","1"=>"Over Temp");
								$bcu_name = array(""=>"","1"=>"正常","2"=>"异常","3"=>"停用");
								$status_name = array(""=>"", "0"=>"充电中","1"=>"饱电","2"=>"充电中无电流","3"=>"电池过温");
								$track_name = array(""=>"","1"=>"空轨且都正常","2"=>"有电池且都正常","3"=>"电池应取但未取出","4"=>"电池充电时间过长","5"=>"轨道停用","6"=>"CCB过温","7"=>"机柜温度过温");
								for($i=1; $i<=8; $i++){
									$onclick = "";
									$spandata = $i;
									$content = '';
									$class = '';
									$canuse = "";
									$battery_id= "";
									$trackdata = "";
									$s = 1;
									if(isset($dataInfo[$i])){
										if($dataInfo[$i]['column_charge'] == 'Y'){
											$class = 'charge';
										}else if($dataInfo[$i]['column_charge'] == 'F'){
											//故障
											$class = 'error';
										}else if($dataInfo[$i]['column_charge'] == 'N' || $dataInfo[$i]['column_charge'] == 'S'){
											$class = "";
										}

										if($dataInfo[$i]['column_park'] == 'Y'){
											if(!isset($dataInfo[$i])){
												$content = "";
											}else{
												if($dataInfo[$i]['battery_status']=="1")
												{
													$canuse = "canuse";
												}	
												else if($dataInfo[$i]['battery_status']=="0")	
												{
													$canuse = "canuse";
												}
												//echo $dataInfo[$i]['battery_status'];
												$battery_id = $dataInfo[$i]['battery_id'];
												$battery_voltage = $dataInfo[$i]['battery_voltage'];
												$battery_status = $dataInfo[$i]['battery_status'];
												$battery_amps = $dataInfo[$i]['battery_amps'];
												$battery_capacity = $dataInfo[$i]['battery_capacity'];
												$battery_temperature = $dataInfo[$i]['battery_temperature'];
												$track_status = $dataInfo[$i]['track_status'];
												$trackdata="{$battery_id},{$battery_voltage},{$battery_status},{$battery_amps},{$battery_capacity},{$battery_temperature},{$track_status}";
												$content = '电池序号：'.$dataInfo[$i]['battery_id'].'<br/>';
												$content .= '电池电压：'.$dataInfo[$i]['battery_voltage'].'<br/>';
												$content .= '电池状态：'.$status_name[$dataInfo[$i]['battery_status']].'<br/>';
												$content .= '电池电流：'.$dataInfo[$i]['battery_amps'].'<br/>';
												$content .= 'SOC：'.$dataInfo[$i]['battery_capacity'].'<br/>';
												$content .= '电池温度：'.$dataInfo[$i]['battery_temperature'].'<br/>';
												$content .= '轨道状态：'.$track_name[$dataInfo[$i]['track_status']].'<br/>';
											}
										}else if($dataInfo[$i]['column_park'] == 'N'){
											//沒電池(空軌)
											$class = 'no';
											$onclick = "returnBattery(this)";
											$spandata = '<button id="returnBt" onclick="returnbt(this)">'.$i.'</button>';
										}

										if($dataInfo[$i]['tbst_status'] == 'E'){
											//故障
											$class = 'error';
										}else if($dataInfo[$i]['tbst_status'] == 'N'){
											//電池交換軌道狀態(N 停用)
											$class = 'stop';
										}
										echo '<li>
												<span id="track'.$i.'" data-toggle="tooltip" data-placement="right" data-html="true" title data-original-title="'.$content.'" class="'.$class.' '.$canuse.'" batteryid="'.$battery_id.'" trackdata="'.$trackdata.'" >'.$spandata.'</span>
											</li>';

									}
								
								}
		?>
						</ul>
						<button id="display_returndata" class="modalInput" rel="#prompt"></button>
						<div class="footer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 300px;">
	<div style="background-color: white">
		<a href="#" class="close"></a>
		<div id="returndata">
			<div id="returntrackdata"></div>
			會員：
			<select class="easyui-combobox" id="userID" name="userID" width="500px">
				<option value=""></option>
			<?php
				foreach($memberInfo as $key => $value)
				{
					$userID = ltrim($value['user_id'],"0");
					echo "<option value='{$value['user_id']}'>{$value['name']}({$userID})</option>";
				}
			?>
			</select>
			<br>
			還電電池序號：
			<div id="returnbattery_div">
				<input id="returnBatteryID" name="returnBatteryID"><br>
			</div>
			還電狀態
			<select class="easyui-combobox" id="returnStatus" name="returnStatus">
				<option value=""></option>
				<option value="0">0:還電成功</option>
				<option value="1">1:機櫃還電門未打開</option>
				<option value="2">2:用戶未放入電池</option>
				<option value="3">3:放入非借出電池</option>
				<option value="4">4:取電門未打開</option>
				<option value="5">5:用戶未取出電池</option>
				<option value="6">6:讀不到電池</option>
			</select>
			<br>
			電池電量<input id="returnCapcity" name="returnCapcity">%<br>
			<input type="button" id="returnBtn" name="returnBtn" onclick="returnBattery()" value="確定">
		</div>
	</div>
</div>
<script type="text/javascript">

$j("#display_returndata").hide();

var nowUserID = "";
$j_1102('#userID').combobox({
	onChange: function(newValue,oldValue){
		changefunction(newValue);
	}
});

$j("#returnCapcity").val(Math.floor(Math.random()*30));
var userID = "";
function promptWin(track)
{
	$j('#prompt').css({
		'margin-top' : document.documentElement.clientHeight / 2 - $j('#prompt').height() / 2 - 90,
		'margin-left' : 0
	});
	
	var html = '<br>歸還軌道'+track+'<br><input type="hidden" id="track" name="track" value="'+track+'">';
	$j('#returntrackdata').html(html);
}

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: true
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#returnBatteryID").change(function(){
	var strMsg = "";
	var returnBatteryID = $(this).val();
	$j.ajax({
        type:'post',
        url: '<?=$web?>simulator/bss_international/getreturnUserID',
        data: {returnBatteryID:returnBatteryID,<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
        beforeSend:function(){
            // $('#loadingIMG1').show();
        },
        complete:function(){
            // $('#loadingIMG1').hide();
        },
        error: function(xhr) {
            // alert(xhr);
            strMsg += 'ajax錯誤';
            alert(strMsg);
        },
        success: function (response) {
            $j_1102('#userID').combobox('setValue',response);
        }
    }) ;
});

/**
 * 還電
 */
// $j('#returnBt').click(function () {
// 	promptWin($j(this).html());
// });

function returnbt(obj)
{
	$j("#display_returndata").html($j(obj).html());
	$j("#display_returndata").trigger("click");
	promptWin($j(obj).html());
}

$j('#bss_id').change(function(){
	// $j('#bss_qrcode').html("<img style='width:200px;height:200px;' src='<?=$web?>simulator/bss/createQR_Code/"+$j(this).val()+"'/>");
	reloadbss();
	$j("#screen").html("");
});

function reloadbss()
{
	$j("#returnCapcity").val(Math.floor(Math.random()*30));
	var strMsg = "";
	var bss_num = $j('#bss_id').find(':selected').attr('snum');
	$j.ajax({
        type:'post',
        url: '<?=$web?>simulator/bss_international/detail_view',
        data: {s_num:bss_num,<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
        dataType: "json",
        beforeSend:function(){
            // $('#loadingIMG1').show();
        },
        complete:function(){
            // $('#loadingIMG1').hide();
        },
        error: function(xhr) {
            // alert(xhr);
            strMsg += 'ajax錯誤';
            alert(strMsg);
        },
        success: function (response) {
            $j("#bss").html(response.bss);
            $j("#bss_title").html(response.bss_id+" "+response.location);
            $('[data-toggle="tooltip"]').tooltip();
   //          triggers = $j(".modalInput").overlay({
			// 	// some mask tweaks suitable for modal dialogs
			// 	mask: {
			// 		color: '#000',
			// 		loadSpeed: 200,
			// 		opacity: 0.7
			// 	},

			// 	closeOnClick: false, 
			// 	closeOnEsc: true
			// }).load();
            //隨機取得軌道借出
			// var x = getRandom($j(".canuse").size());
			// var brrow_trackNo = $j(".canuse:eq("+x+")").html();
			// alert(brrow_trackNo);
			// var brrow_battery = $j(".canuse:eq("+x+")").attr("batteryid");
			// alert(brrow_battery);
            //if(response == "Y")
            //    location.href = 'rentedbattery_success.html';
               //else
               //    location.href = 'rentedbattery_failure.html';
        }
    }) ;
}

function changefunction(user_id)
{
	var strMsg = "";
	$j.ajax({
        type:'post',
        url: '<?=$web?>simulator/bss_international/getuserbrrow',
        data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',user_id:user_id},
        beforeSend:function(){
            // $('#loadingIMG1').show();
        },
        complete:function(){
            // $('#loadingIMG1').hide();
        },
        error: function(xhr) {
            // alert(xhr);
            strMsg += 'ajax錯誤';
            alert(strMsg);
        },
        success: function (response) {
        	if(response!="")
        	{
        		var optionArr = new Array();
	        	optionArr = response.split(",");
	        	html = "";
	        	for(var i=0;i<optionArr.length;i++)
	        	{
	        		html += "<option value='"+optionArr[i]+"'>"+optionArr[i]+"</option>";
	        	}
	        	
	        	if(html!="")
	        	{
	        		html = '<select id="returnBatteryID" name="returnBatteryID" width="500px">'+html+'</select>';
	        		$j("#returnbattery_div").html(html);
	        	}
        	}
        	else
        	{
        		html ='<input id="returnBatteryID" name="returnBatteryID"><br>';
        		$j("#returnbattery_div").html(html);
        	}
 
            // $j("#returnBatteryID").val(response);
            //if(response == "Y")
            //    location.href = 'rentedbattery_success.html';
               //else
               //    location.href = 'rentedbattery_failure.html';
        }
    }) ;
}

function returnBattery()
{
	
	var today= getNowTime();
    var bss_token = $j("#bss_id").find(':selected').attr('bsstoken');
    var bss_id = $j("#bss_id").val();
    userID = $j_1102('#userID').combobox('getValue');
    var trackNo = $j('#track').val();
    var returnBatteryID = $j("#returnBatteryID").val();
    var returnStatus = $j_1102('#returnStatus').combobox('getValue');
    var returnCapcity = $j("#returnCapcity").val();
    if(userID=="")
    {
    	alert("請輸入user ID");
    	return;
    }
    if(returnBatteryID=="")
    {
    	alert("請輸入還電電池序號");
    	return;
    }
    if(returnStatus=="")
    {
    	alert("請輸入還電狀態");
    	return;
    }
    if(returnCapcity=="")
    {
    	alert("請輸入還電電池電量");
    	return;
    }
    	
    var strMsg = "";
    var userName = "";
    var obj3 = new Object;
    obj3.sa01 = today;//請求日期
    obj3.sa02 = bss_id;//借電站Token ID
    obj3.sa03 = returnBatteryID;//電池序號
    var json_text3 = JSON.stringify(obj3);
    $j.ajax({
	    type:'post',
	    url: '<?=$apiurl?>/dbde105d',
	    data: {JSONData:json_text3},
	    dataType: "json",
	    beforeSend:function(){
	        // $('#loadingIMG1').show();
	    },
	    complete:function(){
	        // $('#loadingIMG1').hide();
	    },
	    error: function(xhr) {
	        // alert(xhr);
	        strMsg += 'ajax錯誤';
	        alert(strMsg);
	    },
	    success: function (response) {
	    	if(response.rt_cd=='0000')
	        {
	        	if(response.ra_03=="N")
			    {
			    	alert("電池不合法,請查詢客服!");
			    }
			    else if(response.ra_04=="Y")
			    {
					alert("請先繳費,才可繼續換電!");
			    }
			    else
			    {
			    	userName = response.ra_02;
		        	var obj = new Object;
				    obj.br01 = today;//請求日期
				    obj.br02 = bss_token;//借電站Token ID
				    obj.br03 = userID;//user ID
				    obj.br04 = trackNo;//歸還電池的軌道編號
				    obj.br05 = returnBatteryID;//電池序號
				    obj.br06 = "";//換電站電池借電唯一交易序號
				    obj.br07 = returnStatus;//還電狀態
				    obj.br08 = returnCapcity;//還電電池電量
				    var json_text = JSON.stringify(obj);
				    $j.ajax({
				        type:'post',
				        url: '<?=$apiurl?>/cdc47729',
				        data: {JSONData:json_text},
				        dataType: "json",
				        beforeSend:function(){
				            // $('#loadingIMG1').show();
				        },
				        complete:function(){
				            // $('#loadingIMG1').hide();
				        },
				        error: function(xhr) {
				            // alert(xhr);
				            strMsg += 'ajax錯誤';
				            alert(strMsg);
				        },
				        success: function (response) {
				            if(response.rt_cd=='0000')
				            {
				            	$j("#returnBatteryID").val("");
					            // $j_1102('#userID').combobox('setValue',"");
					            $j('#track').val("");
					            $j("#returnBatteryID").val("");
					            $j_1102('#returnStatus').combobox('setValue',"");
					            $j("#returnCapcity").val("");
					            var x = getRandom($j(".canuse").size());
					            var brrow_trackNo = $j(".canuse:eq("+x+")").html();
					            $j('#track'+trackNo).attr("data-original-title","电池序号："+returnBatteryID+"<br></span>电池电压：12.50<br/>电池状态：充電中<br/>电池电流：<br/>SOC："+returnCapcity+"<br/>电池温度：50.00<br/>轨道状态：有电池且都正常<br/>");
					            $j('#track'+trackNo).attr("class","charge canuse");
					            $j('#track'+trackNo).attr("batteryid",returnBatteryID);
					            $j('#track'+trackNo).attr("trackdata",returnBatteryID+",12.50,0,20.2,"+returnCapcity+",50.00,2");
					            $j('#track'+trackNo).html(trackNo);
					            alert("還電成功");
					            var screenhtml = "還電資訊:<br>會員姓名:"+userName+"<br>租借時間:"+response.rt_01+"<br>歸還時間:"+response.rt_02+"<br>此次租借時間:"+response.rt_03;
					            $j("#screen").html(screenhtml);
					            triggers.eq(1).overlay().close();
					            //隨機取得軌道借出
					            if(brrow_trackNo==null)
					            {
					            	alert("沒有電池可借,請現場拿取新電池,謝謝!");
					            	$j("#bss_report").trigger("click");
					            	return;
					            }	
					            alert("第"+brrow_trackNo+"軌道的電池已經吐出,請取出後按下確定!");
					            today= getNowTime();
					            var obj2 = new Object;
					            var brrow_battery = $j('#track'+brrow_trackNo).attr("batteryid");
					            var trackdata = $j('#track'+brrow_trackNo).attr("trackdata");
								// alert(trackdata);
								var trackArr = new Array();
								trackArr = trackdata.split(',');
								if(trackArr.length == 7)
								{
									obj2.bs08 = trackArr[4];//借電電池電量
								}
								else
								{
									obj2.bs08 = "100";//借電電池電量
								}
					            obj2.bs01 = today;//請求日期
								obj2.bs02 = bss_token;//借電站Token ID
								obj2.bs03 = userID;//user ID
								obj2.bs04 = brrow_trackNo;//借出電池的軌道編號
								obj2.bs05 = brrow_battery;//電池序號
								obj2.bs06 = "";//換電站電池借電唯一交易序號
								obj2.bs07 = "0";//借電狀態
								
								var json_text2 = JSON.stringify(obj2);
					            $j.ajax({
								    type:'post',
								    url: '<?=$apiurl?>/d7bc8a37',
								    data: {JSONData:json_text2},
								    dataType: "json",
								    beforeSend:function(){
								        // $('#loadingIMG1').show();
								    },
								    complete:function(){
								        // $('#loadingIMG1').hide();
								    },
								    error: function(xhr) {
								        // alert(xhr);
								        strMsg += 'ajax錯誤';
								        alert(strMsg);
								    },
								    success: function (response) {
								        if(response.rt_cd=='0000')
								        {
								        	$j('#track'+brrow_trackNo).attr("data-original-title","");
								        	$j('#track'+brrow_trackNo).attr("class","no");
								        	$j('#track'+brrow_trackNo).attr("batteryid","");
								        	$j('#track'+brrow_trackNo).attr("trackdata","");
								        	$j('#track'+brrow_trackNo).html('<button id="returnBt" onclick="returnbt(this)">'+brrow_trackNo+'</button>');
								        	alert("借電成功"); 
								        	$j("#bss_report").trigger("click");
								        	// reloadbss();
								        }    
								        else
								            alert(response.rt_cd+" "+response.rt_msg);
								        //if(response == "Y")
								        //    location.href = 'rentedbattery_success.html';
								           //else
								           //    location.href = 'rentedbattery_failure.html';
								        // triggers.eq(1).overlay().close();
								    }
								}) ;
				            	
				            }    
				            else
				                alert(response.rt_cd+" "+response.rt_msg);
				            //if(response == "Y")
				            //    location.href = 'rentedbattery_success.html';
				               //else
				               //    location.href = 'rentedbattery_failure.html';
				        }
				    }) ;
				}
	        }
	        else
	        {
	        	alert(response.rt_cd+" "+response.rt_msg);
	        }
	    }
    });
}

function getRandom(x){
    return Math.floor(Math.random()*x);
};

//監控回報
$j("#bss_report").click(function(){
	var strMsg = "";
	var today= getNowTime();
	var dataArr = new Array();
    var dataArr2 = new Array();
    var dataArr3 = new Array();
    var obj = new Object;
    var obj2 = new Object;
    obj.em01 = today;//BSS請求日期
    obj.em02 = $j('#bss_id').val();;//BSS序號
    obj.em03 = $j("#bss_id").find(':selected').attr('bsstoken');//換電站Token ID
	obj.em04 = '';
    obj.em05 = '';//當前黑名單版本
	var start = 0;
	var s_arr = new Array('1','2','3','4','5','6','7','8');
	for(s=0; s<8; s++){
		var obj2 = new Object;
		var i = s_arr[s];
		obj2.em06 = i;//軌道編號
		var trackdata = $j("#track"+i).attr("trackdata");
		// alert(trackdata);
		var trackArr = new Array();
		trackArr = trackdata.split(',');
		if(trackArr.length == 7)
		{
			// obj2.em07 = trackArr[6];//軌道狀態
			if(trackArr[6]!="")
				obj2.em07 = trackArr[6];//軌道狀態
			else 
				obj2.em07 = "1";//軌道狀態
			obj2.em12 = trackArr[0];//電池序號
			if(trackArr[4]<100)
			{
				trackArr[4] = trackArr[4]+5;
				if(trackArr[4]>100)
				{
					trackArr[4] = 100;
				}
			}
			if(trackArr[4]==100)
				obj2.em14 = "1";//電池狀態
			else if(trackArr[2]!="")
				obj2.em14 = trackArr[2];//電池狀態
			else
				obj2.em14 = "1";//電池狀態
			obj2.em15 = trackArr[4];//電池容量百分比
			obj2.em16 = trackArr[5];//電池溫度
			obj2.em17 = trackArr[3];//電池電流
			obj2.em18 = trackArr[1];//電池電壓
			obj2.em19 = "";//充電次數
			obj2.em20 = "";//已充電時間
			obj2.em21 = "";//電池芯狀態
		}
		else
		{
			obj2.em07 = "1";//軌道狀態
			obj2.em12 = "";//電池序號
			obj2.em13 = "";//車輛使用者序號
			obj2.em14 = "";//電池狀態
			obj2.em15 = "";//電池容量百分比
			obj2.em16 = "";//電池溫度
			obj2.em17 = "";//電池電流
			obj2.em18 = "";//電池電壓
			obj2.em19 = "";//充電次數
			obj2.em20 = "";//已充電時間
			obj2.em21 = "";//電池芯狀態
		}

		dataArr2[start] = obj2;
		start++;
	}
	//obj.em_info = dataArr2;
	obj.em_info = dataArr3.concat(dataArr2);;
    console.log(obj);
    var strMsg = "ajax錯誤";
    var json_text = JSON.stringify(obj);
    $.ajax({
      type:'post',
      url: '<?=$apiurl?>/be015d74',
      data: {JSONData:json_text},
      dataType: "json",
      error: function(xhr) {
        strMsg += 'Ajax request發生錯誤[json_monitor_bg.php]:'+xhr+'\n請重試';
      },
      success: function (rs) {
        alert("回復狀態："+rs.rt_cd+"\n回覆訊息"+rs.rt_msg);
        reloadbss();
      }
    })
});

//迭代方式实现
function padding1(num, length) {
    for(var len = (num + "").length; len < length; len = num.length) {
        num = "0" + num;            
    }
    return num;
}

</script>

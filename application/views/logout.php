<?
	$web = $this->config->item('base_url');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
<a href="#" class="close"></a>
<form id="formID" action="<?=$web?>logout/logout_ok" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<div class="logoutbox">
		<p class="title"><?=$this->session->userdata('user_name');?><?=$this->lang->line('logout_title')?></p>
		<a href="#" id="btLoginOut" class="button"><?=$this->lang->line('logout_button')?></a>
	</div>
</form>
<script>
	$("#btLoginOut").click( function () {
		$("#formID").submit();
	});
</script>
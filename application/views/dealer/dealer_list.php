<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"tde01"				=>	"{$this->lang->line('dealer_tde01')}",
		"tde02"				=>	"{$this->lang->line('dealer_tde02')}",
		"tde03"				=>	"{$this->lang->line('dealer_tde03')}",
		"tde04"				=>	"{$this->lang->line('dealer_tde04')}",
		"tde05"				=>	"{$this->lang->line('dealer_tde05')}",
		"status"				=>	"{$this->lang->line('dealer_status')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData);

	$tde02_name = array('','直营','加盟','其它');
?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
	<!-- Search Starts Here -->
	<!-- 搜寻先放这 隐藏
	<div id="searchContainer">
		<a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
		<div style="clear:both"></div>
		<div id="searchBox">                
			<form id="searchForm" method="post" action="<?=$search_url?>">
				<div id="closeX" style="color:red;text-align:right;cursor: pointer;z-index:9999;top:0px;right:-20px;position:absolute;" ><img src="<?=$web?>img/delete.png" style="width:30px;height:30px;" /></div>
				<!--预防CSRF攻击-->
	<!-- 			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<fieldset2 id="body">
					<fieldset2>
						<?=$search_box?>
						<input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
					</fieldset2>
				</fieldset2>
			</form>
	<!-- 	</div -->
		<div class="box">
			<div class="topper">
			<p class="btitle"><?=$this->lang->line('dealer_management')?></p>
			<span class="topbtn column"><?=$user_access_control?></span>	
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="dealer">
					<tr class="GridViewScrollHeader">
						<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($dealerInfoRow as $key => $dealerInfoArr) {
		echo "				<tr class=\"GridViewScrollItem\">\n" ;
		
		$s_num = $dealerInfoArr["s_num"] ;
		$checkStr = "
<td>
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$s_num}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "s_num") {
				echo "					<td align=\"center\">".$dealerInfoArr[$enName]."</td>\n" ;
			} else if ($enName == "status") {
				switch($dealerInfoArr[$enName]) {
					case "Y":
						echo "					<td align=\"center\">{$this->lang->line('enable')}</td>\n" ;
						break;
					case "N":
						echo "					<td class=\"no\" align=\"center\">{$this->lang->line('disable')}</td>\n" ;
						break;
					case "D":
						echo "					<td  class=\"no\" align=\"center\">{$this->lang->line('delete')}</td>\n" ;
						break;
					default:
						echo "					<td align=\"center\"></td>\n" ;
						break;
				}
			}else if($enName == "tde02"){
				echo "					<td align=\"left\">".$tde02_name[$dealerInfoArr[$enName]]."</td>\n" ;

			}else {
				echo "					<td align=\"left\">".$dealerInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/dealer/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/dealer/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

</script>

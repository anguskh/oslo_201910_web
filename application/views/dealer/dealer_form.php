<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"tde01"					=> "",
		"tde02" 				=> "",
		"tde03"					=> "",
		"tde04"					=> "",
		"tde05"					=> "",
		"tde06"					=> "",
		"tde07"					=> "",
		"tde08"					=> "",
		"tde09"					=> "",
		"tde10"					=> "",
		"tde11"					=> "",
		"status"					=> "Y"
	);

	$dealerInfo = isset($dealerInfo) ? $dealerInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dealerInfo["s_num"]);

	$Sel1 = "";
	$Sel0 = "";
	if ($dealerInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($dealerInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($dealerInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			
			<p class="btitle"><?=$this->lang->line('dealer_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title">基本资料</p>
			

			<form id="formID" action="<?=$action?>" method="post">
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dealerInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('tde01')?></label>
							<input type="text" class="txt validate[required]" name="postdata[tde01]" id="tde01" value="<?=$dealerInfo["tde01"]?>">		
						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('tde02')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[tde02]" id="tde02" value="1" <?echo (($dealerInfo["tde02"]=='1')?'checked':''); ?>><?=$this->lang->line('tde02_1')?>
							<input type="radio" class="radio validate[required]" name="postdata[tde02]" id="tde02" value="2" <?echo (($dealerInfo["tde02"]=='2')?'checked':''); ?>><?=$this->lang->line('tde02_2')?>
							<input type="radio" class="radio validate[required]" name="postdata[tde02]" id="tde02"  value="3"<?echo (($dealerInfo["tde02"]=='3')?'checked':''); ?>><?=$this->lang->line('tde02_3')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[status]" id="status" value="Y" <?echo (($dealerInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('enable')?>
							<input type="radio" class="radio validate[required]" name="postdata[status]" id="status" value="N" <?echo (($dealerInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('disable')?>

						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('tde03')?></label>
							<input type="text" class="txt validate[required]" name="postdata[tde03]" id="tde03" value="<?=$dealerInfo['tde03']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>		
							<label for=""><span></span><?=$this->lang->line('tde04')?></label>
							<input type="text" class="txt" name="postdata[tde04]" id="tde04" value="<?=$dealerInfo['tde04']?>">
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('tde05')?></label>
							<input type="text" class="txt" name="postdata[tde05]" id="tde05" value="<?=$dealerInfo['tde05']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for=""><span></span><?=$this->lang->line('tde06')?></label>
							<input type="text" class="txt" name="postdata[tde06]" id="tde06" value="<?=$dealerInfo['tde06']?>">
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('tde07')?></label>
							<input type="text" class="txt" name="postdata[tde07]" id="tde07" value="<?=$dealerInfo['tde07']?>">
						</li>
					</ul>
				</div>
			</div>
			<div class="section container">
				<p class="title">帐务资料</p>
				<div class="section container">
					<div class="container-box" style="width:25%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('tde08')?></label>
								<input type="text" class="txt" name="postdata[tde08]" id="tde08" value="<?=$dealerInfo['tde08']?>">
							</li>
						</ul>
					</div>
					<div class="container-box" style="width:25%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('tde11')?></label>
								<input type="radio" class="radio first" name="postdata[tde11]" id="tde11" value="Y" <?echo (($dealerInfo["tde11"]=='Y')?'checked':''); ?>><?=$this->lang->line('tde11_Y')?>
								<input type="radio" class="radio" name="postdata[tde11]" id="tde11" value="N" <?echo (($dealerInfo["tde11"]=='N')?'checked':''); ?>><?=$this->lang->line('tde11_N')?>
							</li>
						</ul>
					</div>
					<div class="container-box" style="width:25%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('tde09')?></label>
								<input type="text" class="txt short" name="postdata[tde09]" id="tde09" value="<?=$dealerInfo['tde09']?>" maxlength="3">
							</li>
						</ul>
					</div>
					<div class="container-box" style="width:25%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('tde10')?></label>
								<input type="text" class="txt" name="postdata[tde10]" id="tde10" value="<?=$dealerInfo['tde10']?>">
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"td_num"				=> "",
		"tsde01"					=> "",
		"tsde02" 				=> "",
		"tsde03"					=> "",
		"tsde04"					=> "",
		"tsde05"					=> "",
		"tsde06"					=> "",
		"tsde07"					=> "",
		"tsde08"					=> "",
		"tsde09"					=> "",
		"tsde10"					=> "",
		"tsde11"					=> "",
		"status"					=> "Y"
	);

	$sub_dealerInfo = isset($sub_dealerInfo) ? $sub_dealerInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $sub_dealerInfo["s_num"]);

	$Sel1 = "";
	$Sel0 = "";
	if ($sub_dealerInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($sub_dealerInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($sub_dealerInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('sub_dealer_management')?><span><?=$show_sub_title;?></span></p>
			<form id="formID" action="<?=$action?>" method="post">
			<div style="float:left; width:75%;">
				<p class="title">基本资料</p>
				<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$sub_dealerInfo['s_num']?>" />
					<div class="container-box" style="width:33%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('td_num')?></label>
								<select name="postdata[td_num]" id="td_num">
									<option value="">选择<?=$this->lang->line('td_num')?></option>
	<?
									foreach($dealer_list as $d_arr){
										echo '<option value="'.$d_arr['s_num'].'" '.(($sub_dealerInfo["td_num"]==$d_arr['s_num'])?'selected':'').'>'.$d_arr['tde01'].'</option>';
									}		
	?>
								</select>
							</li>
							<li>
								<label for="" class="required"><span></span><?=$this->lang->line('tsde01')?></label>
								<input type="text" class="txt validate[required]" name="postdata[tsde01]" id="tsde01" value="<?=$sub_dealerInfo["tsde01"]?>">		
							</li>
							<li>
								<label for="" class="required"><span></span><?=$this->lang->line('tsde02')?></label>
								<input type="radio" class="radio first validate[required]" name="postdata[tsde02]" id="tsde02" value="1" <?echo (($sub_dealerInfo["tsde02"]=='1')?'checked':''); ?>><?=$this->lang->line('tsde02_1')?>
								<input type="radio" class="radio validate[required]" name="postdata[tsde02]" id="tsde02" value="2" <?echo (($sub_dealerInfo["tsde02"]=='2')?'checked':''); ?>><?=$this->lang->line('tsde02_2')?>
								<input type="radio" class="radio validate[required]" name="postdata[tsde02]" id="tsde02"  value="3"<?echo (($sub_dealerInfo["tsde02"]=='3')?'checked':''); ?>><?=$this->lang->line('tsde02_3')?>
							</li>
						</ul>
					</div>
					<div class="container-box" style="width:34%;">
						<ul>
							<li>
								<label for="" class="required"><span></span><?=$this->lang->line('status')?></label>
								<input type="radio" class="radio first validate[required]" name="postdata[status]" id="status" value="Y" <?echo (($sub_dealerInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('enable')?>
								<input type="radio" class="radio validate[required]" name="postdata[status]" id="status" value="N" <?echo (($sub_dealerInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('disable')?>
							</li>
							<li>
								<label for="" class="required"><span></span><?=$this->lang->line('tsde03')?></label>
								<input type="text" class="txt validate[required]" name="postdata[tsde03]" id="tsde03" value="<?=$sub_dealerInfo['tsde03']?>">
							</li>
							<li>		
								<label for=""><span></span><?=$this->lang->line('tsde04')?></label>
								<input type="text" class="txt" name="postdata[tsde04]" id="tsde04" value="<?=$sub_dealerInfo['tsde04']?>">
							</li>
						</ul>
					</div>
					<div class="container-box" style="width:33%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('tsde05')?></label>
								<input type="text" class="txt" name="postdata[tsde05]" id="tsde05" value="<?=$sub_dealerInfo['tsde05']?>">
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('tsde06')?></label>
								<input type="text" class="txt" name="postdata[tsde06]" id="tsde06" value="<?=$sub_dealerInfo['tsde06']?>">
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('tsde07')?></label>
								<input type="text" class="txt" name="postdata[tsde07]" id="tsde07" value="<?=$sub_dealerInfo['tsde07']?>">
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div style="float:left; width:25%;">
				<div class="section container">
					<p class="title">帐务资料</p>
					<div class="section container">
						<div class="container-box">
							<ul>
								<li>
									<label for=""><span></span><?=$this->lang->line('tsde09')?></label>
									<input type="text" class="txt short" name="postdata[tsde09]" id="tsde09" value="<?=$sub_dealerInfo['tsde09']?>" maxlength="3">
								</li>
								<li>
									<label for=""><span></span><?=$this->lang->line('tsde10')?></label>
									<input type="text" class="txt" name="postdata[tsde10]" id="tsde10" value="<?=$sub_dealerInfo['tsde10']?>">
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
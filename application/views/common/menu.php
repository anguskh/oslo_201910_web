<?
$this->load->helper('url');
//转成阵列, 我的最爱
$favor_data = explode(',', $favor_data);
$web = $this->config->item('base_url');

?>
</header>
<!-- 登出 页面 -->
<!-- logout input dialog -->
<div class="modal" id="prompt_logout" style="width: 350px; height: 70px; display:none;">
</div>
<script type="text/javascript">
    /**
     * 登出按钮
     */
    $j("#logout").click( function () {
        var strMsg = "" ;
		
        $j.ajax({
            type:'post',
            url: '<?= $web ?>logout/logout_confirm',
            data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
            error: function(xhr) {
                strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
                alert(strMsg);
            },
            success: function (response) {
                $j('#prompt_logout').html(response);
            }
        }) ;
    });
	
//预设执行
//移除星星
//removeFavor();

//将连结网址的function后面加上乱码
$j("#menu_area  a:not(.dislink)").on("mouseover", function (event) {
	console.log('bb');
	var web = '<?=$web?>';
	var class_url = $j(this).attr("class_url");
	var function_url = $j(this).attr("function_url");
	if(function_url != "" || class_url == 'main'){
		//乱数
		var random_url = create_random_url();
		function_url = function_url+'__'+random_url;
		var web_url = web+class_url+'/'+function_url;
		
		//首页分开处理
		if(class_url == 'main'){
			web_url = web+class_url;
		}
		//取代掉原本的网址
		$j(this).attr("href", web_url);
	}
});
</script>
<div id="container" style="overflow-x: auto;">
	<nav id="column-left">
		<ul id="menu">
	<?
		//將網址拆成class和function
		function setAttrUrl($menu_directory){
			$tmpArr = explode('/', $menu_directory);
			$class_url = "";
			$function_url = "";
			if(isset($tmpArr[0])){
				$class_url = $tmpArr[0];
			}
			if(isset($tmpArr[1])){
				$function_url = $tmpArr[1];
			}
			return " class_url='{$class_url}' function_url='{$function_url}' ";
		}

		$s = 0;
		foreach ($menuInfoRow as $key => $menuInfoArr) {
			$menuInfoArr['menu_icon'] = "";
			$s++;
			if ($menuInfoArr["parent_menu_sn"] == 0) {
				
				$menu_directory = $menuInfoArr["menu_directory"];
				$class_fn = setAttrUrl($menu_directory);

				if ($menu_directory == '#'){
					echo '<li id="menu-system">
								<a href="#collapse'.$s.'" class="parent"><i class="'.$menuInfoArr["icon_class"].'"></i><span>'.$menuInfoArr["menu_name"].'</span></a>
								<ul id="collapse'.$s.'" class="collapse">';
				}else{
					echo '<li id="menu_area"><a href="'.$web.$menu_directory.'">'.(($menuInfoArr["icon_class"]!='')?'<i class="'.$menuInfoArr["icon_class"].'"></i>':'').'<span>'.$menuInfoArr["menu_name"].'</span></a></li>';
				}
				foreach($menuInfoRow as $key1 => $secondmenuInfoArr)
				{
					
					$menu_directory = $secondmenuInfoArr["menu_directory"];
					$class_fn = setAttrUrl($menu_directory);
					
					if($secondmenuInfoArr["parent_menu_sn"] == $menuInfoArr["menu_sn"] && $secondmenuInfoArr["second_menu_sn"]==0 )
					{
						$m_style = "";
						if($menu_directory == '#'){
							//$url1 = current_url() ."/".$menu_directory."/";
							$url1 ="";
							$dislink = "class='dislink parent'";
							$m_style = "style='color:black;'";
						}else{
							$url1 = $web.$menu_directory."/";
							$dislink = "";
						}

						$img = '<li id="menu_area"><a '.(($url1 != '')?'href="'.$url1.'"':'').'><i class="fas fa-angle-double-right"></i>'.$secondmenuInfoArr["menu_name"].'</a></li>';

						$n = 0;
						$second = "";
						foreach($menuInfoRow as $key2 => $thirdmenuInfoArr)
						{
							if($thirdmenuInfoArr["second_menu_sn"] == $secondmenuInfoArr["menu_sn"])
							{
								$menu_directory = $thirdmenuInfoArr["menu_directory"];
								$class_fn = setAttrUrl($menu_directory);
								if($n==0)
								{
									$second = "<ul>";
									$n++;
								}
								$second .= '<li id="menu_area"><a href="'.$web.$menu_directory.'">'.(($thirdmenuInfoArr["icon_class"]!='')?'<i class="'.$thirdmenuInfoArr["icon_class"].'"></i>':'').''.$thirdmenuInfoArr["menu_name"].'</a></li>';
							}
						}
						if($n!=0)
							$second .= "</ul>";
						echo $img;
						echo $second;
						echo "</li>\n";
					}
				}
				echo "							</ul>\n";
				echo "						</li>\n";
			} 
		}
	?>
		</ul>
	</nav>
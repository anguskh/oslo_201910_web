<?
	$web = $this->config->item('base_url');
	
	// 取得table:t_acquire_bank_id的资料
	$terminalSelectListRow = array();
	if( $this->session->userdata('default_bank_sn') != 0 )
	{
		$bankSelectListRow = $this->model_bank->getBankSelectList( $this->session->userdata('default_bank_sn') ) ;
		$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $this->session->userdata('default_bank_sn') ) ;
	}
	else
	{
		$bankSelectListRow = $this->model_bank->getBankSelectList() ;
		$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $bankSelectListRow[0]['ACQUIRE_BANK_ID'] ) ;
			
		if($this->session->userdata('selMerchantSN')){
			$terminalSelectListRow = $this->model_terminal->getTerminalListNoPage($this->session->userdata('selMerchantSN'));
		}
	}	

	switch ($this->router->fetch_class()) {
		case 'reader_initialize_log': 
			$prjActionStr = "{$web}inquiry/reader_initialize_log/getlist";
			
			$bankActionStr = "{$web}bank/terminal/select_merchant";
			//自动完成
			$mer_autoStr = "{$web}bank/terminal/autocomplete_merchant";
			$ter_autoStr = "{$web}bank/terminal/autocomplete_terminal";
			
			$merchantActionStr = "{$web}bank/terminal/select_termainal";
			
			//取得预设端末机
			$terminalSelectListRow = $this->model_terminal->getTerminalListNoPage($this->session->userdata('selMerchantSN'), " <> 'D' ") ;
			break;
		default:	// 没有值，直接结束
			exit();
			break;
	}
	
	$bankdisabled = "";
	$merchantdisabled = "";
	$merchantdisable_display = "";
	
	if($this->session->userdata('selBankSN') != '' && $this->session->userdata('login_side') == '0'){	//有使用者有银行, 并且是前台登入
		$bankdisabled = "disabled";
	}

	if( $this->session->userdata('selMerchantSN') != '' && 
		$this->session->userdata('login_side') == '0' &&
		$this->session->userdata('user_merchant') != '' ){	//有银行, 商店使用者, 并且是前台登入
		$bankdisabled = "disabled";
		$merchantdisabled = "disabled";
		$merchantdisable_display = " style='display:none' ";
	}
	
	$Sel1 = "";
	$Sel0 = "";
	$Selall = "";
	$selStatus = $this->session->userdata('selStatus');
	if($selStatus == 'all' || $selStatus == ""){
		$Selall = " checked";
	}else if ($selStatus == '1') {
		$Sel1 = " checked";
	} else if ($selStatus == '0') {
		$Sel0 = " checked";
	}
	
	date_default_timezone_set("Asia/Taipei");
	$initial_date = $this->session->userdata('initial_date');
	if($initial_date == ""){
		$initial_date = date("Y-m-d");
	}
	$initial_date_end = $this->session->userdata('initial_date_end');
?>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/date_input.css"/>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/range_input.css"/>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<!--重新载入validationEngine, 自订中文化日期 start-->
<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
<!--重新载入validationEngine, 自订中文化日期 end-->

<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->

<a href="#" class="close"></a>
<form id="formSelect" action="<?=$prjActionStr?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
		<fieldset>
	  		<p>
				<label><?=$this->lang->line('reader_initialize_log_initial_date')?></label>
				<input type="date" class="validate[required]" name="initial_date" id="initial_date"  style="width: 120px;" value="<?=$initial_date?>" />～ 
				<input type="date" class="validate[custom[date]]" name="initial_date_end" id="initial_date_end" style="width: 120px;" value="<?=$initial_date_end?>" >　　　
			</p>
			<p>
				<label><?=$this->lang->line('reader_initialize_log_STATUS')?></label>
				<input name="selStatus" type="radio" value="all"<?=$Selall?>><span style='font-size: 18px'><?=$this->lang->line('reader_initialize_log_all')?></span>
				<input name="selStatus" type="radio" value="1"<?=$Sel1?>><span style='font-size: 18px'><?=$this->lang->line('reader_initialize_log_1')?></span>
				<input name="selStatus" type="radio" value="0"<?=$Sel0?>><span style='font-size: 18px'><?=$this->lang->line('reader_initialize_log_0')?></span>
			</p>
			<p>
				<?php
					if(count($bankSelectListRow) != 0){
						echo "<input type='hidden' name='selBankSN' id='selBankSN' value='{$bankSelectListRow[0]['ACQUIRE_BANK_ID']}' />";
					}else{
						echo "<input type='hidden' name='selBankSN' id='selBankSN' value='' />";
					}
				?>
			</p>
			<p>
				<label><?=$this->lang->line('select_merchant')?></label>
				<input name="merID" id="merID" style="width: 120px;" value="<?=$this->session->userdata('selMerchantSN')?>" placeholder="<?=$this->lang->line('please_input_merchant_id')?>" <?=$merchantdisabled?> >
				<select name="selMerchantSN" id="selMerchantSN" style="width: 280px;" <?=$merchantdisabled?>>
<?
	if($this->session->userdata('selMerchantSN') == "NULL")
			$selectall = " selected";
		else
			$selectall = "";
	if( !empty( $merchantSelectListRow ) )
	{
		echo "					<option value=\"novalue\">{$this->lang->line('select_merchant_help')}　</option>\n";
		foreach ($merchantSelectListRow as $key => $merchantSelectListArr) {
		if( $this->session->userdata('selMerchantSN') == $merchantSelectListArr["MERCHANT_ID"] || count($merchantSelectListRow)==1 )
			$selected = " selected";
		else
			$selected = "";
			
			//补空白
			$show_MERCHANT_ID = '['.$merchantSelectListArr["MERCHANT_ID"].']';
			$length = 13-strlen($show_MERCHANT_ID);
			$space = "";
			for($i = 1; $i<=$length; $i++){
				$space .= '&ensp;';
			}
			$show_MERCHANT_ID = $show_MERCHANT_ID.$space;
			
			echo "<option value=\"{$merchantSelectListArr["MERCHANT_ID"]}\" {$selected}>{$show_MERCHANT_ID}{$merchantSelectListArr["MERCHANT_NAME_CHINESE"]}</option>\n";
		}
	}
	else
	{
		echo "					<option value=\"\">{$this->lang->line('select_merchant_help')}　</option>\n";
	}
	
?>
				</select>
			</p>
			<p>
				<label><?=$this->lang->line('select_terminal')?></label>
				<input name="terID" id="terID" style="width: 120px;" maxlength="8"  value="<?=$this->session->userdata('selTerminalSN')?>" placeholder="<?=$this->lang->line('please_input_terminal_id')?>" >
<?
	//选择端末机
	echo <<<html
			<select name="selTerminalSN" class="" id="selTerminalSN" style="width: 280px;">
html;
		if( !empty( $merchantSelectListRow ) ){
			echo "					<option value=\"\">{$this->lang->line('select_terminal_help')}　</option>\n";
			foreach ($terminalSelectListRow as $key => $termainlSelectListArr) {
				if( $this->session->userdata('selTerminalSN') == $termainlSelectListArr["TERMINAL_ID"] )
					$selected = " selected";
				else
					$selected = "";
				echo "<option value=\"{$termainlSelectListArr["TERMINAL_ID"]}\" {$selected}>{$termainlSelectListArr["TERMINAL_ID"]}</option>\n";
			}
		}else{
			echo "					<option value=\"\">{$this->lang->line('select_terminal_help')}　</option>\n";
		}
	echo <<<html
			</select>
html;
				
?>		
			</p>
		</fieldset>
		<br>	
		</td></tr>
		<tr><td align="center">
			<p><input id="submit_data" type="submit" value="<?=$this->lang->line('confirm')?>"></p>
		</td></tr>
	</table>
</form>
<!-- 可以先将资料show在这里看对不对 <div id="htmlMsg"></div> 可以拿掉 -->
<div id="htmlMsg"></div>
<!--自动完成的CSS设定-->
<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script type="text/javascript">
	$j("#initial_date").dateinput({ lang: 'zh-TW', trigger: false, format: 'yyyy-mm-dd', offset: [0, 0] });
	$j("#initial_date_end").dateinput({ lang: 'zh-TW', trigger: false, format: 'yyyy-mm-dd', offset: [0, 0] });
	$j("#initial_date").val('<?=$initial_date?>');
	$j("#initial_date_end").val('<?=$initial_date_end?>');
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formSelect").validationEngine();
	});
	
$j('#submit_data').click(function () {
	
	//时间
	var startDate = $j("#initial_date").val();
	startDate = startDate.replace(/-/g, "/");
	startDate = Date.parse(startDate).valueOf();
	var endDate = $j("#initial_date_end").val();
	endDate = endDate.replace(/-/g, "/");
	endDate = Date.parse(endDate).valueOf();
	if(startDate > endDate){	//起日不可大于迄日
		alert('起日不可大于迄日!');
		return false;
		event.preventDefault();
	}
	
	$j(this).submit();
});
	
//stert 商店自动完成===============================================
//input自动连结select, 让select和 input同步
function mer_connectSelect(merchantID){
	var count = $j("#selMerchantSN option[value='"+merchantID+"']").length;

	if(count == 1){
		$j("#selMerchantSN option[value='"+merchantID+"']").attr("selected", true);
	}else{
		$j("#selMerchantSN option").eq(0).attr("selected", true);
	}	
}

//select自动连结input, 让input和 select同步
$j("#selMerchantSN").change(function(){
	var merchantID = $j(this).val();
	if(merchantID == "novalue"){
		$j("#merID").val("");
	}else{
		$j("#merID").val(merchantID);
	}
	selectMerchant();
}); 

//商店, 取的栏位资料
function get_data() {
    var data = {
        'merID': $("#merID").val()
    };
    return data;
};

//商店, 自动完成
$("#merID").autocomplete({ 
	//单一参数方法, 变数名称为term
	//source: "<?=$mer_autoStr?>", 
	//多参数方法, 变数名称可自取
	source: function(request, response) {
		$j.getJSON("<?=$mer_autoStr?>", get_data(), response);
	}, 
	minLength: 1,
	close: function() {	//选择清单后动作
		var merchantID = $j(this).val();
		mer_connectSelect(merchantID);
		selectMerchant();
	}
}); 

//边动作边搜寻
$("#merID").keyup(function(){
	var merchantID = $j(this).val();
	mer_connectSelect(merchantID);
	selectMerchant();
});

//商店变更动做
function selectMerchant(){
	var merchant_sn = $j("#selMerchantSN").val();
	$j("#terID").val("");
	$j.ajax({
		type:'post',
		url: '<?=$merchantActionStr?>',
		data: {merchant_sn:merchant_sn, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j('#selTerminalSN').html(response);
			$j("#selTerminalSN").trigger("change");
		}
	}) ;
}

//end 商店自动完成===============================================

//stert 端末机自动完成===============================================
//input自动连结select, 让select和 input同步
function ter_connectSelect(terminalID){
	var count = $j("#selTerminalSN option[value='"+terminalID+"']").length;

	if(count == 1){
		$j("#selTerminalSN option[value='"+terminalID+"']").attr("selected", true);
	}else{
		$j("#selTerminalSN option").eq(0).attr("selected", true);
	}	
}

//select自动连结input, 让input和 select同步
$j("#selTerminalSN").change(function(){
	var terminalID = $j(this).val();
	$j("#terID").val(terminalID);
}); 

//端末机, 取的栏位资料
function get_data() {
	//alert($j("#merID").val()+':'+$j("#terID").val());
    var data = {
        'merID': $j("#merID").val(),
		'terID': $j("#terID").val()
    };
    return data;
};

//端末机, 自动完成
$("#terID").autocomplete({ 
	//多参数方法, 变数名称可自取
	source: function(request, response) {
		$j.getJSON("<?=$ter_autoStr?>", get_data(), response);
	}, 
	minLength: 1,
	close: function() {	//选择清单后动作
		var terminalID = $j(this).val();
		ter_connectSelect(terminalID);
	}
}); 

//边动作边搜寻
$("#terID").keyup(function(){
	var terminalID = $j(this).val();
	ter_connectSelect(terminalID);
});

//end 端末机自动完成===============================================

</script>

<!--修正自动完成 载入 jquery.tools.min.js 后, 关闭form后导致选单出错-->
<script type="text/javascript">
	var $j = jQuery.noConflict();
</script>
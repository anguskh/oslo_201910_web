<?
	$web = $this->config->item('base_url');

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$batteryswapsSelectListRow = $this->Model_show_list->getbatteryswapstationList() ;
	$vehicleSelectListRow = $this->Model_show_list->getvehicleList() ;

	switch ($this->router->fetch_class()) {
		case 'Log_battery_exchange': 
			$prjActionStr = "{$web}inquiry/{$fn}/getlist";
			break;
		case 'Log_battery_info': 
			$prjActionStr = "{$web}inquiry/{$fn}/getlist";
			break;
		default:	// 没有值，直接结束
			exit();
			break;
	}

	$selso_num = $this->session->userdata("{$fn}_".'selso_num');
	$selsb_num = $this->session->userdata("{$fn}_".'selsb_num');
	$selsv_num = $this->session->userdata("{$fn}_".'selsv_num');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/date_input.css"/>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/range_input.css"/>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<!--重新载入validationEngine, 自订中文化日期 start-->
<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
<!--重新载入validationEngine, 自订中文化日期 end-->

<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->
<form id="formSelect" action="<?=$prjActionStr?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<div style="border-radius: 10px; background:#fff;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td>
			<fieldset>
				<p style="padding: 20px 10px 10px 10px;"">
					<label style="display:inline-block; width:150px;"><?=$this->lang->line('select_operator_class')?></label>
					<select name="selso_num" id="selso_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
	<?
		if( !empty( $operatorsSelectListRow ) )
		{
			echo "					<option value=\"novalue\">{$this->lang->line('select_operator_class')}　</option>\n";
			foreach ($operatorsSelectListRow as $key => $SelectListArr) {
			if( $selso_num == $SelectListArr["s_num"] )
				$selected = " selected";
			else
				$selected = "";
				echo "<option value=\"{$SelectListArr["s_num"]}\" {$selected}>{$SelectListArr["top01"]}</option>\n";
			}
		}
		else
		{
			echo "					<option value=\"\">{$this->lang->line('select_operator_class')}　</option>\n";
		}
		
?>
					</select>
				</p>
				<p style="padding: 0 10px 10px 10px;">
					<label style="display:inline-block; width:150px;"><?=$this->lang->line('select_batteryswaps_class')?></label>
					<select name="selsb_num" id="selsb_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
	<?
		if( !empty( $batteryswapsSelectListRow ) )
		{
			echo "					<option value=\"novalue\">{$this->lang->line('select_batteryswaps_class')}　</option>\n";
			foreach ($batteryswapsSelectListRow as $key => $SelectListArr) {
			if( $selsb_num == $SelectListArr["s_num"] )
				$selected = " selected";
			else
				$selected = "";
				echo "<option value=\"{$SelectListArr["s_num"]}\" {$selected}>{$SelectListArr["bss_id"]}</option>\n";
			}
		}
		else
		{
			echo "					<option value=\"\">{$this->lang->line('select_batteryswaps_class')}　</option>\n";
		}
		
?>
					</select>
				</p>
<?
	if($this->router->fetch_class() == 'Log_battery_exchange'){
?>
				<p style="padding: 0 10px 10px 10px;">
					<label style="display:inline-block; width:150px;"><?=$this->lang->line('select_vehicle_class')?></label>
					<select name="selsv_num" id="selsv_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
	<?
		if( !empty( $vehicleSelectListRow ) )
		{
			echo "					<option value=\"novalue\">{$this->lang->line('select_vehicle_class')}　</option>\n";
			foreach ($vehicleSelectListRow as $key => $SelectListArr) {
			if( $selsv_num == $SelectListArr["s_num"] )
				$selected = " selected";
			else
				$selected = "";
				echo "<option value=\"{$SelectListArr["sv_num"]}\" {$selected}>{$SelectListArr["unit_id"]}</option>\n";
			}
		}
		else
		{
			echo "					<option value=\"\">{$this->lang->line('select_vehicle_class')}　</option>\n";
		}
		
?>
					</select>
				</p>
<?
	}
?>
			</fieldset>
			</td></tr>
			<tr><td align="center">
				<p style="padding:10px 10px 20px 10px;"><input id="submit_data" type="submit" value="<?=$this->lang->line('confirm')?>" style="background: #66BA44; border-radius: 16px; height: 32px; width: 130px; border: 0; color: #fff; font-size: 14px;"></p>
			</td></tr>
		</table>
	</div>
</form>
<!-- 可以先将资料show在这里看对不对 <div id="htmlMsg"></div> 可以拿掉 -->
<div id="htmlMsg"></div>
<!--自动完成的CSS设定-->
<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formSelect").validationEngine();
	});
	
$j('#submit_data').click(function () {
	$j(this).submit();
});
	
</script>

<!--修正自动完成 载入 jquery.tools.min.js 后, 关闭form后导致选单出错-->
<script type="text/javascript">
	var $j = jQuery.noConflict();
</script>
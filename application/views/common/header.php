<?
	$web = $this->config->item('base_url');
	if($this->session->userdata('login_side') == '0')
		$title = $this->lang->line('login_user_title');
	else
		$title = $this->lang->line('login_nimda_title');
	
	$html_title = $this->lang->line('html_title');
		
	//检查有无权限进入此页面
	$this->load->helper('url');
	$this->model_background->checkurl(uri_string());
	
	// $systemInfoRow = $this->session->userdata('exist_platform');
	// print_r($_SESSION);
	// echo 'A:'.$DBname;

	$DBname = $this->session->userdata('DBname');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<!doctype html>
<html lang="zh-TW">
<head>
	<meta charset="utf-8">
	<title>綠農創意管理系统</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="designer" content="" />
	<meta name="viewport" content="width=device-width">


	<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
	<link href="<?=$web.$newcss;?>newcss/bootstrap.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" type="text/css" href="<?=$web?>css/validationEngine.jquery.css" />
	<link rel="stylesheet" type="text/css" href="<?=$web.$newcss;?>newcss/fontawesome.css"/>
	<link href="<?=$web.$newcss;?>newcss/jquery.fancybox.css" rel="stylesheet" type="text/css">

	<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
	<script src="<?=$web?>js/jquery.tools.min.js"></script>
	<script src="<?=$web?>js/common/random_url.js"></script>
	<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=$web?>js/jquery.dateinput.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=$web?>newjs/jquery.fancybox.js"></script>

	
	<!-- Load the Mootools Framework -->
	<!-- script src="<?=$web?>js/mootools.js" type="text/javascript" charset="utf-8"></script -->
	<!-- script src="<?=$web?>js/jquery.fixedheadertable.js"></script>
	<link href="<?=$web?>css/defaultTheme.css" rel="stylesheet" media="screen" / -->

	<script type="text/javascript">
		var $j = jQuery.noConflict();
	</script>
	<!-- link rel="stylesheet" type="text/css" href="<?=$web?>css/search.css" / -->
	<script src="<?=$web?>js/search.js" type="text/javascript" charset="utf-8"></script>
	<title id="html_title"><?=$html_title?></title>
</head>

<body>
<header>
	<script type= "text/javascript" >
		// 测试是否有include jQuery
		// alert(window.jQuery.fn.jquery);
	</script>
<?php 
	$systemInfoRow = $this->session->userdata('exist_platform');
	$compare_battery_id = $this->session->userdata('compare_battery_id');
	// print_r($systemInfoRow);
	// print_r($_SESSION);
?>
	<!-- 系统标题 Start -->
	<div class="topper">
		<a type="button" id="button-menu" class="pull-left"><span class="fa fa-bars"></span></a>
		<a href="<?=$web?>main" class="logo"></a>
		<div style="position: absolute; top: 12px; left: 340px; width:215px; padding: 2px 5px; color: #FFFFFF; border: solid 1px; border-radius: 5px; background:#FF3333;">
			目前不比对还电电池序号
		</div>
		<span class="login modalInput"><b><i class="fas fa-user"></i> <?=$this->session->userdata('user_name');?></b><a href='' data-fancybox data-src="<?= $web ?>logout/logout_confirm" href="javascript:;"><?=$this->lang->line('logout')?></a></span>
	</div>
<?
	if($compare_battery_id == 'N'){
?>

<?
	}
?>
	<!-- 系统标题 End -->
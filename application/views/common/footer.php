<?
	$web = $this->config->item('base_url');
?>
		<!-- <footer>
			©2019 綠農創意 版权所有
		</footer> -->
	</body>
	<!-- script type="text/javascript" src="<?=$web?>newjs/retina.min.js"></script -->
	<script>
		//retinajs();
	</script>
</html>
<?
	//逾时时间
	if($this->session->userdata('session_expiration')){
		$session_expiration = $this->session->userdata('session_expiration')*1000;
	}else{
		$session_expiration = $this->config->item('sess_expiration')*1000;
	}
	
	if($this->session->userdata('login_side') == 1)
		$logout_url = $this->config->item('base_url')."nimda/";
	else
		$logout_url = $this->config->item('base_url');
?>
<?
	// print_r($priNoList);
	// echo count($priNoList);
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification";
	if(isset($bView)){
		if($bView == true){
			$action = "{$get_full_url_random}/view";
		}
	}
	if(isset($priNoList)){
		$total_count = count($priNoList);
		$tmp = "";
		foreach($priNoList as $key => $value){
			//显示label
			$label = "";
			foreach($value as $showkey => $showvalue){
				if($label == ""){
					$label .= $showvalue;
				}else{
					$label .= ' | '.$showvalue;
				}
			}
			//预设选择
			$selected = "";
			if(isset($priNoNow)){
				if($value['priNo'] == $priNoNow){
					$selected = " selected";
				}
			}
			
			$tmp .= "<option value=\"{$value['priNo']}\" {$selected}>{$label}</option>";
		}
	}
?>
	<!-- begin google tracking code -->
	<script src="<?=$web?>js/ga.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        // var pageTracker = _gat._getTracker("UA-2180518-1");
        // pageTracker._initData();
        // pageTracker._trackPageview();
    </script>
	<!-- end google tracking code -->

<script type="text/javascript">
$j(document).ready(function() {
	//没分页就把分页移除, 避免有留白
	page_clear();
    $j("#listTable tr:nth-child(even)").addClass("even");
	
	//变更网页title, 改成为每个功能的名称
	//功能title
	var fn_title = $j("#container").find("td div").eq(0).text();
	fn_title = fn_title.trim();
	//网页title
	var html_title = $j("#html_title").text();
	$j("#html_title").text(html_title+'-'+fn_title);
	
<?php
	if($this->uri->segment(1) == "main")
	{
		echo '$j("#html_title").text("电动机车管理系统");';
	}
?>
		
<?
	if(isset($priNoList)){  //有快捷单号
?>
		//启动combobox select开关
		var combobox_flag = false;
		//快捷单号
		fn_updw(0);
		//easy ui
		$j_1102('#priNoList').combobox({
			onChange: function(newValue,oldValue){
				// console.log('onChange:'+value);
				// console.log('newValue:'+newValue);
				// console.log('oldValue:'+oldValue);
			},
			onClick: function(obj){
				// console.log('onClick:'+obj.value);
			},
			onSelect: function(obj){
				// var result = fn_change_page_check_data();
				// console.log('result2:'+result);
				// if(result){
					// console.log('跳页中');
					// console.log(value);
					// if(value != ''){
						// chngePage(value);
					// }
				// }
				console.log('onSelect');
				console.log(combobox_flag);
				if(combobox_flag){
					console.log('onSelect:'+obj.value);
					$j("#priNoList option[value='"+obj.value+"']").prop("selected", true);
					$j("#priNoList").trigger("change");
				}
			}
		});
		
		combobox_flag = true;  //跑完combobox再启动, 避免初始化过程中触发到onSelect
<?
	}
?>

//ready 和onload都执行这程式, 避免有漏
<?
	//检视
	if(isset($bView)){
		if($bView){
?>
		fn_disable_all();
<?
		}
	}
?>


});	

//最后执行, onload事件都会等到整个视窗里所有资源都已经全部下载后才会执行
$j(window).load(function(){
	//把選單固定再隱藏的最外層位置, 避免圖曾被蓋掉
	//menu_hold();

<?
	//检视
	if(isset($bView)){
		if($bView){
?>
		fn_disable_all();
<?
		}
	}
?>

	//把控制面板固定住
	//stuck_apnel();

});

</script>

<script language="javascript">

//没分页就把分页移除, 避免有留白
function page_clear(){
	var pagination_text = $j("div.pagination").text();
	pagination_text = pagination_text.trim();
	if(pagination_text == ''){
		$j("div.pagination").remove();
	}
}

//自动计算时间, 逾时自动登出
function Msg(){
	alert("<?=$this->lang->line('timeout_msg')?>");
	location="<?=$logout_url?>";
}
//闲置20分钟，Session预设是20分钟
window.setInterval("Msg()", <?=$session_expiration?>);
</script>

<script type="text/javascript" src="<?=$web;?>newjs/gridviewscroll.js"></script>
<script src="<?=$web?>js/common/common.js" type="text/javascript" charset="utf-8"></script>
<!--<script src="<?=$web?>js/websocket.js" type="text/javascript"></script>-->
<link rel=stylesheet type="text/css" href="<?=$web?>css/toastr/toastr.min.css">
<script src="<?=$web?>js/toastr/toastr.min.js" type="text/javascript"></script>
<script language="javascript">
//页数输入检核
$j(".input_page").keypress(function(event){
	var pageurl = $j(this).attr('url');
	//最大页数
	var maxpage = 0;
	maxpage = parseInt($j(this).attr('maxpage'));
	//每页笔数
	var pagerows = 0;
	pagerows = parseInt($j(this).attr('pagerows'));
	
	//目前输入
	var getVal = $j(this).val();
	if ( event.which == 13 ) { //Enter
		if (isNaN(getVal)){  //检测是否为数字, 非数字
			getVal = maxpage;
			$j(this).val(getVal);
		}else{  //数字
			if(getVal>maxpage || getVal == ""){ //不可超过最大页数
				getVal = maxpage;
				$j(this).val(maxpage);
			}
		}
		var showpage = (getVal-1) * pagerows;
		location = pageurl + '/' + showpage;
	}else{
		return ValidateNumber2();
	}
});

//執行websocket接收即時訊息
if(typeof(WebSocket)=='undefined'){
	alert('你的浏览器不支持 WebSocket ，推荐使用Google Chrome 或者 Mozilla Firefox');	
}
(function(){
	var key='all',mkey;
	var users={};
	var url='ws://<?=$this->session->userdata("websocket_ip")?>:<?=$this->session->userdata("websocket_port")?>';
	var so=false,n=false;
	// var lus=A.$('us'),lct=A.$('ct');
	function st(){
        //创建socket，注意URL的格式：ws://ip:端口
		so=new WebSocket(url);
        //握手监听函数
		so.onopen=function(){
            //状态为1证明握手成功，然后把client自定义的名字发送过去
			if(so.readyState==1){
				// so.send('type=add&w_msg=');
			}
		}
	    
        //握手失败或者其他原因连接socket失败，则清除so对象并做相应提示操作
		so.onclose=function(){
			so=false;
			st();
		}
		
        //数据接收监听，接收服务器推送过来的信息，返回的数据给msg，然后进行显示
		so.onmessage=function(msg){
			eval('var da='+msg.data);
			var obj=false,c=false;
			if(da.type=='add'){
				//设置显示配置
		        var messageOpts = {
		            "closeButton" : true,//是否显示关闭按钮
		            "timeOut" : "false",
		            "extendedTimeOut" : "5000",
		        };
		        toastr.options = messageOpts;
		        toastr.options.onclick = function(){window.location='<?=$web?>monitor/monitor_alarm_online';};
				toastr.error(da.w_msg);
				// alert(da.w_msg);
			}
		}
	}

	st();
	
})();

</script>
<?
	if(isset($priNoList)){  //有快捷单号
?>
<script language="javascript">
function chngePage(val){
	console.log('跳页中');
	var goUrl = "<?=$action?>/"+val;
	console.log(goUrl);
	location = goUrl;
}

//快捷单号
$j("#priNoList").change(function(){
	console.log('change');
	var page_val = $j(this).val();
	
	var save_flag = 'N';
	//个功能页的检查function [fn_change_page_check_data]
	save_flag = fn_change_page_check_data();
	
	console.log('fn_change_page_check_data:'+save_flag);
	//先提醒 请先存档 再执行
	if(save_flag === 'N'){
		reset();
		alertify.confirm("请先存档才可执行", function (e) {
			if(e){
				//存档
				var result = fn_change_page_save();
				console.log('result:'+result);
				if(result){
					ready_change();
					return true;
				}else{
					console.log('存档失败');
					return false;
				}
			}else{  //不存档
				console.log('不存档');
				return false;
			}
		});
	}else if(save_flag === false){  //代表检查必填没过
		return true;
	}else{ // 'Y' or true , 通过可换页
		ready_change();
		return true;
	}
	
	function ready_change(){
		console.log('准备跳页');
		chngePage(page_val);
	}
	
});

$j("#priNoList_up").click(function(){
	fn_updw(-1);
	$j("#priNoList").trigger("change");
});

$j("#priNoList_dw").click(function(){
	fn_updw(1);
	$j("#priNoList").trigger("change");
});

function fn_updw(option){
	var idx = $j("#priNoList option:selected").index();
	console.log(idx);
	var cnt = $j("#priNoList option").length;
	console.log(cnt);
	idx = idx + option;
	if(idx <= 0){
		idx = 0;
		$j("#priNoList_up").hide();
	}else if(idx >= cnt-1){
		idx = cnt-1;
		$j("#priNoList_dw").hide();
	}else{
		$j("#priNoList_up").show();
		$j("#priNoList_up").show();
	}
	
	$j("#priNoList option").eq(idx).prop("selected", true);
}
</script>
<?
	}
?>

<script src="<?=$web?>newjs/jquery-3.3.1.js"></script>
<script src="<?=$web?>newjs/common2.js"></script>
<script src="<?=$web?>newjs/bootstrap.min.js"></script>



<?
	$web = $this->config->item('base_url');

	$userSelectListRow = $this->model_user->getUserListNoPage() ;

	switch ($this->router->fetch_class()) {
		case 'log_operating': 
			$prjActionStr = "{$web}inquiry/{$fn}/getlist";
			//自动完成
			$mer_autoStr = "{$web}nimda/user/autocomplete_user";
			break;
		default:	// 没有值，直接结束
			exit();
			break;
	}
	
	$bankdisabled = "";
	$merchantdisabled = "";
	
	if($this->session->userdata('selBankSN') != '' && $this->session->userdata('login_side') == '0'){	//有使用者有银行, 并且是前台登入
		$bankdisabled = "disabled";
	}

	if( $this->session->userdata('selMerchantSN') != '' && 
		$this->session->userdata('login_side') == '0' &&
		$this->session->userdata('user_merchant') != '' ){	//有银行, 商店使用者, 并且是前台登入
		$bankdisabled = "disabled";
		$merchantdisabled = "disabled";
	}
	
	$Sel1 = "";
	$Sel0 = "";
	$Selall = "";
	$selStatus = $this->session->userdata('selStatus');
	if($selStatus == 'all' || $selStatus == ""){
		$Selall = " checked";
	}else if ($selStatus == '1') {
		$Sel1 = " checked";
	} else if ($selStatus == '0') {
		$Sel0 = " checked";
	}
	
	date_default_timezone_set("Asia/Taipei");
	$start_date = $this->session->userdata("{$fn}_".'start_date');
	if($start_date == ""){
		$start_date = date("Y-m-d");
	}
	$end_date = $this->session->userdata("{$fn}_".'end_date');
	
	$selUserID = $this->session->userdata("{$fn}_".'selUserID');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/date_input.css"/>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/range_input.css"/>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<!--重新载入validationEngine, 自订中文化日期 start-->
<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
<!--重新载入validationEngine, 自订中文化日期 end-->

<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->
<form id="formSelect" action="<?=$prjActionStr?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<div style="border-radius: 10px; background:#fff;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td>
			<fieldset>
				<p style="padding: 20px 10px 10px 10px;">
					<label style="display:inline-block; width:130px;"><?=$this->lang->line('select_choose_a_date')?></label>
					<input type="date" class="validate[required]" name="start_date" id="start_date"  style="width: 100px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;" value="<?=$start_date?>" />～ 
					<input type="date" class="validate[custom[date]]" name="end_date" id="end_date" style="width: 100px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;" value="<?=$end_date?>" >　　　
				</p>
				<p style="padding: 0 10px 10px 10px;">
					<label style="display:inline-block; width:130px;"><?=$this->lang->line('select_user')?></label>
					<input name="userID" id="userID" style="width: 100px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;" value="<?=$selUserID?>" placeholder="<?=$this->lang->line('please_input_user_id')?>" >
					<select name="selUserID" id="selUserID" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
	<?
		if($selUserID == "NULL")
				$selectall = " selected";
			else
				$selectall = "";
		if( !empty( $userSelectListRow ) )
		{
			echo "					<option value=\"novalue\">{$this->lang->line('select_user_help')}　</option>\n";
			foreach ($userSelectListRow as $key => $SelectListArr) {
			if( $selUserID == $SelectListArr["user_id"] || count($userSelectListRow)==1 )
				$selected = " selected";
			else
				$selected = "";
				
				//补空白
				$show_user_ID = '['.$SelectListArr["user_id"].']&ensp;';
				
				echo "<option value=\"{$SelectListArr["user_id"]}\" {$selected}>{$show_user_ID}{$SelectListArr["emp_name"]}</option>\n";
			}
		}
		else
		{
			echo "					<option value=\"\">{$this->lang->line('select_user_help')}　</option>\n";
		}
		
?>
					</select>
				</p>
			</fieldset>
			</td></tr>
			<tr><td align="center">
				<p style="padding:10px 10px 20px 10px;"><input id="submit_data" type="submit" style="background: #66BA44; border-radius: 16px; height: 32px; width: 130px; border: 0; color: #fff; font-size: 14px;" value="<?=$this->lang->line('confirm')?>"></p>
			</td></tr>
		</table>
	</div>
</form>
<!-- 可以先将资料show在这里看对不对 <div id="htmlMsg"></div> 可以拿掉 -->
<div id="htmlMsg"></div>
<!--自动完成的CSS设定-->
<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script type="text/javascript">
	$j("#start_date").dateinput({ lang: 'zh-TW', trigger: false, format: 'yyyy-mm-dd', offset: [0, 0] });
	$j("#end_date").dateinput({ lang: 'zh-TW', trigger: false, format: 'yyyy-mm-dd', offset: [0, 0] });
	$j("#start_date").val('<?=$start_date?>');
	$j("#end_date").val('<?=$end_date?>');
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formSelect").validationEngine();
	});
	
$j('#submit_data').click(function () {
	
	//时间
	var startDate = $j("#start_date").val();
	startDate = startDate.replace(/-/g, "/");
	startDate = Date.parse(startDate).valueOf();
	var endDate = $j("#end_date").val();
	endDate = endDate.replace(/-/g, "/");
	endDate = Date.parse(endDate).valueOf();
	if(startDate > endDate){	//起日不可大于迄日
		alert('起日不可大于迄日!');
		return false;
		event.preventDefault();
	}
	
	$j(this).submit();
});
	
//stert 自动完成===============================================
//input自动连结select, 让select和 input同步
function mer_connectSelect(merchantID){
	var count = $j("#selUserID option[value='"+merchantID+"']").length;

	if(count == 1){
		$j("#selUserID option[value='"+merchantID+"']").attr("selected", true);
	}else{
		$j("#selUserID option").eq(0).attr("selected", true);
	}	
}

//select自动连结input, 让input和 select同步
$j("#selUserID").change(function(){
	var merchantID = $j(this).val();
	if(merchantID == "novalue"){
		$j("#userID").val("");
	}else{
		$j("#userID").val(merchantID);
	}
}); 

//商店, 取的栏位资料
function get_data() {
    var data = {
        'userID': $j("#userID").val()
    };
    return data;
};

//自动完成
$("#userID").autocomplete({ 
	//单一参数方法, 变数名称为term
	//source: "<?=$mer_autoStr?>", 
	//多参数方法, 变数名称可自取
	source: function(request, response) {
		$j.getJSON("<?=$mer_autoStr?>", get_data(), response);
	}, 
	minLength: 1,
	close: function() {	//选择清单后动作
		var merchantID = $j(this).val();
		mer_connectSelect(merchantID);
	}
}); 

//边动作边搜寻
$("#userID").keyup(function(){
	var merchantID = $j(this).val();
	mer_connectSelect(merchantID);
});


//end 自动完成===============================================


</script>

<!--修正自动完成 载入 jquery.tools.min.js 后, 关闭form后导致选单出错-->
<script type="text/javascript">
	var $j = jQuery.noConflict();
</script>
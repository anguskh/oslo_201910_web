<?
	$web = $this->config->item('base_url');
	
	$browser_ver = "";
	if(strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 9.0"))
		$browser_ver = "Internet Explorer 9.0";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 8.0"))
		$browser_ver = "Internet Explorer 8.0";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 7.0"))
		$browser_ver = "Internet Explorer 7.0";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"MSIE 6.0"))
		$browser_ver = "Internet Explorer 6.0";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"Firefox"))
		$browser_ver = "Firefox";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"Chrome"))
		$browser_ver = "Chrome";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"Safari"))
		$browser_ver = "Safari";
	else if(strpos($_SERVER["HTTP_USER_AGENT"],"Opera"))
		$browser_ver = "Opera";
	else 
		$browser_ver = $_SERVER["HTTP_USER_AGENT"];
		
	if($browser_ver == "Internet Explorer 7.0"){
		
	}
	
?>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>

		</title>
<style>
	body{font-family:"楷体_GB2312","新宋体","Times New Roman",Georgia,Serif;} 
	.title{font-size:20pt;line-height:19pt;} 
	.content{font-size:16pt;line-height:19pt;}
	#footer{  position: fixed; width: 100%;  bottom: 0; z-index: 1; background: #dcdcdc; text-align: center; margin-left: -10px}
	#qrcode{width: 70px;height: 70px;}
	#td_qrcode{text-align: center;}
<?
	if($browser_ver == "Internet Explorer 7.0"){	//针对IE7作分散对齐
?>
		.footer_day{font-size:16pt;line-height:24pt;text-align:justify;text-justify:distribute-all-lines;text-align-last:justify;}
<?
	}else{
?>
		.footer_day{font-size:16pt;line-height:24pt;text-align:justify;text-justify: distribute-all-lines;}
<?
	}
?>
	span {display: inline-block /* Opera */; padding-left: 100%; }
	/*table{margin-top:50px}*/
	table{
		white-space: normal;
		line-height: normal;
		font-weight: normal;
		font-size: medium;
		font-variant: normal;
		font-style: normal;
		color: -webkit-text;
		text-align: start;
	}
	table td{
		padding-top: 3px;
		padding-bottom: 3px;
	}
	#title_logo{
		text-align: left;
	}
</style>
<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
	</head>
	<body onLoad="">
		<img id='title_logo' src='<?=$web?>/img/terminal_first_logo.png' />
		<center>
<?
	$i = 0;
	$page_break ="<P style='page-break-after:always'></P>";
	foreach($arr_tid as $key => $value){
		if($i != 0){
			echo $page_break;
		}
		$i++;
?>
				<table border=0 width='600px' align='center' CELLPADDING='5'>
					<tr>
						<td align='center' class='title' colspan=6><p lang='zh-TW'><B>
								第一银行MPOS行动收单业务
							</B></p></td>
							</tr><tr>
							<td align='center' class='title' colspan=6><p lang='zh-TW'><B>
								MPOS读卡机启用通知书
							</B></p></td>
							</tr><tr>
							<td align='left' class='content' colspan=6><p lang='zh-TW'>
								亲爱的特约商店您好：
							</p></td>
							</tr><tr>
							<td align='left' colspan=6 class='content'><p lang='zh-TW'>
								欢迎　贵宝号成为本行MPOS行动收单业务特约商店(特店代号：<?=$value['MERCHANT_ID']?>，特店名称：<?=$value['MERCHANT_NAME_CHINESE']?>)，为利于　贵宝户作业，请您依下列步骤完成MPOS读卡机启用作业：
							</p></td>
							</tr><tr>
							<td colspan=1 class='content' valign='top' align='left' var><p lang='zh-TW'>
								一、	
							</p></td><td colspan=5 class='content' align='left'><p lang='zh-TW'>
								贵宝户MPOS读卡机端末编号为<?=$value['TERMINAL_ID']?>之初始密码为<?=$value['initpwd']?>，将用于设定MPOS读卡机使用者密码，请务必妥善保管您的初始密码及本通知书。
							</p></td>
							</tr><tr>
							<td colspan=1 class='content' valign='top' align='left' var><p lang='zh-TW'>
								二、	
							</p></td><td align='left' class='content' colspan=6><p lang='zh-TW'>
								设定MPOS读卡机，请先下载本行MPOS行动收单业务应用程式(APP)，下载网址为<?=$this->session->userdata('apk_url')?>或扫描下方QR Code连结网址，进行安装。
							</p></td>
							</tr><tr>
							<td colspan=6 class='content' valign='top' align='left' var id='td_qrcode'>
								<?="<img id='qrcode' src=\"{$web}login/createQR_Code\"/>"?>
							</td>
							</tr><tr>
							<td colspan=1 class='content' valign='top' align='left' var><p lang='zh-TW'>
								三、	
							</p></td><td align='left' class='content' colspan=6><p lang='zh-TW'>
								请指定使用本通知书所列初始密码设定您的使用者密码，后续每笔信用卡交易时，在输入您所设定之使用者密码后即可进行交易。
							</p></td>
							</tr><tr>
							<td colspan=1 class='content' valign='top' align='left' var><p lang='zh-TW'>
								四、	
							</p></td><td align='left' class='content' colspan=6><p lang='zh-TW'>
								本行MPOS行动收单业务，目前仅开放接受VISA、MasterCard及JCB等信用卡(限磁条及晶片卡)进行交易，检附操作简表(如附表)请查收。
							</p></td>
							</tr><tr>
							<td colspan=1 class='content' valign='top' align='left' var><p lang='zh-TW'>
								五、	
							</p></td><td align='left' class='content' colspan=6><p lang='zh-TW'>
								贵宝户如有操作使用任何问题，请先拨打本行所提供之24小时维修专线:0912-000-042，或于上班时间拨打本行客服电话(02)2173-1988分机6955及6810。
							</p></td>
							</tr><tr>
							<td align='left' class='content' colspan=6><p lang='zh-TW'>
								敬祝
							</p></td>
							</tr><tr>
							</tr><tr>
							<td class='content' colspan=1 ></td><td class='content' align='left' colspan=5><p lang='zh-TW'>
								鸿    图    大    展
							</p></td>
							</tr><tr>
							</tr><tr>
							<td class='content' align='right' colspan=6><p lang='zh-TW'>
								第一银行 信用卡处   敬启
							</p></td>
							</tr><tr>
							</tr><tr>
							<td width='px' align='left' class='footer_day' colspan=6><p lang='zh-TW' class="p_footer_day" >
								
							</p>
							</td>
							</tr></table>
							
<?

	}
	
?>

		</center>
		<div id='footer'><input onclick="document.getElementById('footer').style.display='none'; self.print(); self.close();" type="button" value='列印'></div>
	</body></html>
	
<script type="text/javascript">
	var today = new Date();
	var year = chineseNumber(today.getFullYear()-1911);
	var month = chineseNumber(today.getMonth()+1);
	var day = chineseNumber(today.getDate());
	
function chineseNumber(number,lowerorsupper){  
    //转换值是否为整数  
    if(!isNaN(parseInt(number * 1))){  
        //--------------  
        // 定义变数  
        //--------------  
        //小写的中文数字  
        var chineseNumber_lower = ('〇一二三四五六七八九').split('');  
        //大写的中文数字  
        var chineseNumber_upper = ('零壹贰参肆伍陆柒捌玖').split('');  
        //数词单位阵列  
        var chineseOrder = ('十百千元万亿兆京垓秭穣沟涧正载').split('');   
        //定义储存转换后的数字结果阵列  
        var transformNumber = new Array();  
        //逆转数字后的数字阵列  
        var numberAsString = new Array();  
        //用来记录移动位数的索引(从tail开始)  
        var orderFlag = 3;  
        //--------------  
        // 数字处理  
        //--------------  
        //将数字字串化  
        number = number+'';  
        //逆转数字后储入阵列  
        for (var i=number.length-1; i>=0; i-- ){  
            numberAsString[numberAsString.length++] = number.charAt(i);  
        }  
        //针对每个英文数字处理  
        for(var i=0; i<numberAsString.length; i++){  
            //产生对应的中文数字，并且依大小写有所不同  
            if(lowerorsupper == 'upper'){  
                numberAsString[i] = chineseNumber_upper[numberAsString[i]];   
            } else {  
                numberAsString[i] = chineseNumber_lower[numberAsString[i]];  
            }  
            //添加数词  
            switch((i+1)%4){  
                case 1:  
                    transformNumber[numberAsString.length-i] = numberAsString[i];  
                break;  
                case 2:  
                    transformNumber[numberAsString.length-i] = numberAsString[i];  
                break;  
                case 3:  
                    transformNumber[numberAsString.length-i] = numberAsString[i];  
                break  
                case 0:  
                    transformNumber[numberAsString.length-i] = numberAsString[i];  
                break;  
            }  
            //每处理四个数字后移动位数索引  
            if((i+1)%4 == 0){  
                orderFlag++;  
            }  
        }  
        //回传转换后的中文数字  
        return transformNumber.join('');  
    } else {  
        return '数字必需为整数';  
    }  
}  
<?
	if($browser_ver == "Internet Explorer 7.0"){	//针对IE7作分散对齐
?>
		$(".p_footer_day").html("民 国 "+year+" 年 "+month+" 月 "+day+" 日 ");
<?
	}else{
?>
		$(".p_footer_day").html("民 国 "+year+" 年 "+month+" 月 "+day+" 日 <span></span>");
<?
	}
?>

</script>
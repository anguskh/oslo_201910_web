<?
	$web = $this->config->item('base_url');

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$sub_operatorSelectListRow = $this->Model_show_list->getsuboperatorList() ;
	$vehicleSelectListRow = $this->Model_show_list->getvehicleList() ;

	switch ($this->router->fetch_class()) {
		case 'Log_driving': 
			$prjActionStr = "{$web}inquiry/{$fn}/getlist";
			break;
		default:	// 没有值，直接结束
			exit();
			break;
	}

	$selto_num = $this->session->userdata("{$fn}_".'selto_num');
	$seltso_num = $this->session->userdata("{$fn}_".'seltso_num');
	$selsv_num = $this->session->userdata("{$fn}_".'selsv_num');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<link href="<?=$web;?>newcss/default.css" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/date_input.css"/>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/range_input.css"/>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<!--重新载入validationEngine, 自订中文化日期 start-->
<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
<!--重新载入validationEngine, 自订中文化日期 end-->

<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->
<form id="formSelect" action="<?=$prjActionStr?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<div style="border-radius: 10px; background:#fff;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td>
			<fieldset>
				<p style="padding: 20px 10px 10px 10px;">
					<label style="display:inline-block; width:130px;"><?=$this->lang->line('select_operator_class')?></label>
					<select name="selto_num" id="to_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;" onchange="change_sub_num('to_num', 'tso_num');">
						<?=create_select_option($operatorsSelectListRow, $selto_num, $this->lang->line('select_operator_class'));?>
					</select>
				</p>
				<p style="padding: 0 10px 10px 10px;">
					<label style="display:inline-block; width:130px;"><?=$this->lang->line('select_suboperator_class')?></label>
					<select name="seltso_num" id="tso_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
						<?=create_select_option($sub_operatorSelectListRow, $seltso_num, $this->lang->line('select_suboperator_class'), array("to_num"));?>
					</select>
				</p>
				<p style="padding: 0 10px 10px 10px;">
					<label style="display:inline-block; width:130px;"><?=$this->lang->line('select_vehicle_class')?></label>
					<select name="selsv_num" id="selsv_num" style="width: 180px; border-radius: 3px; height: 32px; line-height: 32px; border: 1px solid #ccc; color: #333;">
	<?
		if( !empty( $vehicleSelectListRow ) )
		{
			echo "					<option value=\"novalue\">{$this->lang->line('select_vehicle_class')}　</option>\n";
			foreach ($vehicleSelectListRow as $key => $SelectListArr) {
			if( $selsv_num == $SelectListArr["s_num"] )
				$selected = " selected";
			else
				$selected = "";
				echo "<option value=\"{$SelectListArr["sv_num"]}\" {$selected}>{$SelectListArr["unit_id"]}</option>\n";
			}
		}
		else
		{
			echo "					<option value=\"\">{$this->lang->line('select_vehicle_class')}　</option>\n";
		}
		
?>
					</select>
				</p>
			</fieldset>
			</td></tr>
			<tr><td align="center">
				<p style="padding:10px 10px 20px 10px;"><input id="submit_data" type="submit" value="<?=$this->lang->line('confirm')?>" style="background: #66BA44; border-radius: 16px; height: 32px; width: 130px; border: 0; color: #fff; font-size: 14px;"></p>
			</td></tr>
		</table>
	</div>
</form>
<!-- 可以先将资料show在这里看对不对 <div id="htmlMsg"></div> 可以拿掉 -->
<div id="htmlMsg"></div>
<!--自动完成的CSS设定-->
<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
var $j = jQuery.noConflict();
</script>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formSelect").validationEngine();
	});
	
$j('#submit_data').click(function () {
	$j(this).submit();
});

function change_sub_num(filed_1, filed_2){
	var filed_val = $j("#"+filed_1).val();
	
	if(filed_val == ''){
		$j("#"+filed_2+" option").show();
	}else{
		$j("#"+filed_2+ "option:selected").removeAttr("selected");
		$j("#"+filed_2+" option").hide();
		$j("#"+filed_2+" option["+filed_1+"='"+filed_val+"']").show();
		$j("#"+filed_2+" option[value='']").show();
		$j("#"+filed_2+" option[value='']").prop('selected', true);
	}
}
	
</script>

<!--修正自动完成 载入 jquery.tools.min.js 后, 关闭form后导致选单出错-->
<script type="text/javascript">
	var $j = jQuery.noConflict();
</script>
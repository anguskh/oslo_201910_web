<?

	$web = $this->config->item('base_url');
	$bankdisabled = "";
	$merchantdisable = "";
	
	if($this->session->userdata('selBankSN') != '' && $this->session->userdata('login_side') == '0'){	//有使用者有银行, 并且是前台登入
		$bankdisabled = "disabled";
	}
	
	$merchantdisable_display = "";
	if( $this->session->userdata('selMerchantSN') != '' && 
		$this->session->userdata('login_side') == '0' && 
		$this->session->userdata('user_merchant') != '' ){	//有银行, 商店使用者, 并且是前台登入
		$bankdisabled = "disabled";
		$merchantdisable = "disabled";
		$merchantdisable_display = " style='display:none' ";
	}
	
	// 取得table:tbl_bank的资料
	if( $this->session->userdata('default_bank_sn') != 0 )
	{
		$bankSelectListRow = $this->model_bank->getBankSelectList( $this->session->userdata('default_bank_sn') ) ;
		if($merchantdisable === "disabled"){
			$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $this->session->userdata('default_bank_sn'), $this->session->userdata('user_merchant')) ;
		}else{
			$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $this->session->userdata('default_bank_sn') ) ;
		}
	}
	else
	{
		$bankSelectListRow = $this->model_bank->getBankSelectList() ;
		if($merchantdisable === "disabled"){
			$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $bankSelectListRow[0]['ACQUIRE_BANK_ID'], $this->session->userdata('user_merchant')) ;
		}else{
			$merchantSelectListRow = $this->model_merchant->getMerchantListNoPage( $bankSelectListRow[0]['ACQUIRE_BANK_ID'] ) ;
		}
	}
	
	//取得端末机
	$terminalSelectListRow = $this->model_terminal->getTerminalListNoPage($this->session->userdata('selMerchantSN'), " <> 'D' ") ;

	$terClass = "";
	switch ($this->router->fetch_class()) {
		case 'initial_password': // 商店初始化密码查询
			$bankActionStr = "{$web}bank/terminal/select_merchant";
			//自动完成
			$mer_autoStr = "{$web}bank/terminal/autocomplete_merchant";
			$ter_autoStr = "{$web}bank/terminal/autocomplete_terminal";
			
			$merchantActionStr = "{$web}inquiry/initial_password/select_termainal";
			$prjActionStr = "{$web}inquiry/initial_password/bank_merchant_terminal_list";
			break;
			
		case 'terminal': // 商店初始化密码查询
			$bankActionStr = "{$web}bank/terminal/select_merchant";
			//自动完成
			$mer_autoStr = "{$web}bank/terminal/autocomplete_merchant";
			$ter_autoStr = "{$web}bank/terminal/autocomplete_terminal";
			
			$merchantActionStr = "{$web}bank/terminal/select_termainal";
			$prjActionStr = "{$web}bank/terminal/bank_terminal_list";
			break;
			
		case 'phone_match': //手机匹配
			$bankActionStr = "{$web}bank/terminal/select_merchant";
			//自动完成
			$mer_autoStr = "{$web}bank/terminal/autocomplete_merchant";
			$ter_autoStr = "{$web}bank/terminal/autocomplete_terminal";
			
			$merchantActionStr = "{$web}bank/terminal/select_termainal";
			$prjActionStr = "{$web}bank/phone_match/data_list";
			
			//取得预设端末机
			$terminalSelectListRow = $this->model_terminal->getTerminalListNoPage($this->session->userdata('selMerchantSN'), " <> 'D' and  MERCHANT_ID <> '' and ACQUIRE_BANK_ID <> '' ") ;
			
			//必填
			$terClass = ' class="validate[required]" ';
			break;
			
		case 'log_operating': // 历程纪录查询
			//自动完成
			$mer_autoStr = "{$web}bank/terminal/autocomplete_merchant";
			$ter_autoStr = "{$web}bank/terminal/autocomplete_terminal";
			
			$merchantActionStr = "{$web}bank/terminal/select_termainal";

			$bankActionStr = "{$web}inquiry/log_operating/select_merchant";
			$prjActionStr = "{$web}inquiry/log_operating/getlist";
			break;

		default:	// 没有值，直接结束
			exit();
			break;
	}
	
	$stock = "";
	if($this->session->userdata('selMerchantSN') == "stock"){
		$stock = "selected";
	}
	
	
?>
<script src="<?=$web?>js/jquery.tools.min.js"></script>
<!--重新载入validationEngine, 自订中文化日期 start-->
<script src="<?=$web?>js/jquery.validationEngine-zh_TW_1.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=$web?>js/jquery.tools.date-zh-TW.js" type="text/javascript" charset="utf-8"></script>
<!--重新载入validationEngine, 自订中文化日期 end-->

<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->

<!--<a href="#" class="close"></a>-->
<form id="formSelect" action="<?=$prjActionStr?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>
		<fieldset>
			<p>
				<?php
					if(count($bankSelectListRow) != 0){
						echo "<input type='hidden' name='selBankSN' id='selBankSN' value='{$bankSelectListRow[0]['ACQUIRE_BANK_ID']}' />";
					}else{
						echo "<input type='hidden' name='selBankSN' id='selBankSN' value='' />";
					}
				?>
			</p>
			<p <?=$merchantdisable_display?>>
<?
	$class = "";
	if($this->router->fetch_class() != "log_operating"){
		$class = "validate[required]";
	}
?>
				<label><?=$this->lang->line('select_merchant')?></label>
				<input name="merID" id="merID" style="width: 120px;" value="<?=$this->session->userdata('selMerchantSN')?>" placeholder="<?=$this->lang->line('please_input_merchant_id')?>" <?=$merchantdisable?> >
				<select name="selMerchantSN" id="selMerchantSN" class="<?=$class?>" style="width: 300px;" <?=$merchantdisable?>>
<?
	if($this->session->userdata('selMerchantSN') == "NULL")
			$selectall = " selected";
		else
			$selectall = "";
	if( !empty( $merchantSelectListRow ) )
	{
		if($this->router->fetch_class() != "log_operating"){	//记录档页面
			echo "					<option value=\"\">{$this->lang->line('select_merchant_help')}　</option>\n";
		}else{
			echo "					<option value=\"\">{$this->lang->line('select_all_help')}　</option>\n";
		}
		
		if($this->router->fetch_class() == "terminal" && $this->session->userdata('login_side') == '1'){	//端末机页面, 和由后台登入, 才可选库存
			echo "					<option value=\"stock\" {$stock}>{$this->lang->line('select_stock_help')}　</option>\n";
		}
		foreach ($merchantSelectListRow as $key => $merchantSelectListArr) {
		if( $this->session->userdata('selMerchantSN') == $merchantSelectListArr["MERCHANT_ID"] || count($merchantSelectListRow)==1 )
			$selected = " selected";
		else
			$selected = "";
			
			//补空白
			$show_MERCHANT_ID = '['.$merchantSelectListArr["MERCHANT_ID"].']';
			$length = 13-strlen($show_MERCHANT_ID);
			$space = "";
			for($i = 1; $i<=$length; $i++){
				$space .= '&ensp;';
			}
			$show_MERCHANT_ID = $show_MERCHANT_ID.$space;
			
			echo "<option value=\"{$merchantSelectListArr["MERCHANT_ID"]}\" {$selected}>{$show_MERCHANT_ID}{$merchantSelectListArr["MERCHANT_NAME_CHINESE"]}</option>\n";
		}
	}
	else
	{
		echo "					<option value=\"\">{$this->lang->line('select_merchant_help')}　</option>\n";
	}
?>
				</select>
			</p>
			
			<p>
				<label><?=$this->lang->line('select_terminal')?></label>
				<input name="terID" id="terID" style="width: 120px;" maxlength="8" value="<?=$this->session->userdata('selTerminalSN')?>" placeholder="<?=$this->lang->line('please_input_terminal_id')?>" >
				<select name="selTerminalSN" id="selTerminalSN" style="width: 300px;" <?=$terClass?> >
<?
	if( !empty( $terminalSelectListRow ) )
	{
		echo "					<option value=\"\">{$this->lang->line('select_terminal_help')}　</option>\n";
		foreach ($terminalSelectListRow as $key => $terminalSelectListArr) {
		//if( $this->session->userdata('selTerminalSN') == $terminalSelectListArr["TERMINAL_ID"] || count($terminalSelectListRow)==1 )
		if( $this->session->userdata('selTerminalSN') == $terminalSelectListArr["TERMINAL_ID"] )
			$selected = " selected";
		else
			$selected = "";
			echo "<option value=\"{$terminalSelectListArr["TERMINAL_ID"]}\" {$selected}>{$terminalSelectListArr["TERMINAL_ID"]}</option>\n";
		}
	}
	else
	{
		echo "					<option value=\"\">{$this->lang->line('select_terminal_help')}　</option>\n";
	}
?>
				</select>
			</p>
			
		</fieldset>
		</td>
		</tr>
		<tr><td align="center">
			<p><input type="submit" value="<?=$this->lang->line('confirm')?>"></p>
		</td></tr>
	</table>
</form>
<!-- 可以先将资料show在这里看对不对 <div id="htmlMsg"></div> 可以拿掉 -->
<div id="htmlMsg"></div>

<!--自动完成的CSS设定-->
<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formSelect").validationEngine();
		
		//确认无误后, 打开disabled, 避免无post出去
		$j(":submit").on('click',function(){
			//注册, 先检查, 避免submit先送出
			$j("#formSelect").validationEngine('attach');
			var tmp =$j("#formSelect").validationEngine('validate');
			if(tmp){
				$j("#merID").attr("disabled", false);
				$j("#selMerchantSN").attr("disabled", false);
			}
		});
		
		$j('#selBank').css("height", "");
		$j('#selBank').css({
			'margin-top' : document.documentElement.clientHeight / 2 - $j('#selBank').height() / 2 - 90,
			'margin-left' : 0
		});
		
	});
	
//$j('#selBankSN').hide();
<?php
	if(count($bankSelectListRow)==1 && count($merchantSelectListRow)==1)
	{
?>
		// $j('#formSelect').attr("action", "<?=$prjActionStr?>") ;
		// $j('#formSelect').submit() ;
<?php
	}
?>
$j('#selBankSN').change(function () {
	var bank_sn = $j(this).val() ;
	$j.ajax({
		type:'post',
		url: '<?=$bankActionStr?>',
		data: {bank_sn:bank_sn, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j('#selMerchantSN').html(response);
		}
	}) ;
}) ;


//stert 商店自动完成===============================================
//input自动连结select, 让select和 input同步
function mer_connectSelect(merchantID){
	var count = $j("#selMerchantSN option[value='"+merchantID+"']").length;

	if(count == 1){
		$j("#selMerchantSN option[value='"+merchantID+"']").attr("selected", true);
	}else{
		$j("#selMerchantSN option").eq(0).attr("selected", true);
	}	
}

//select自动连结input, 让input和 select同步
$j("#selMerchantSN").change(function(){
	var merchantID = $j(this).val();
	if(merchantID == "novalue"){
		$j("#merID").val("");
	}else{
		$j("#merID").val(merchantID);
	}
	selectMerchant();
}); 

//商店, 取的栏位资料
function get_data() {
    var data = {
        'merID': $("#merID").val()
    };
    return data;
};

//商店, 自动完成
$("#merID").autocomplete({ 
	//单一参数方法, 变数名称为term
	//source: "<?=$mer_autoStr?>", 
	//多参数方法, 变数名称可自取
	source: function(request, response) {
		$j.getJSON("<?=$mer_autoStr?>", get_data(), response);
	}, 
	minLength: 1,
	close: function() {	//选择清单后动作
		var merchantID = $j(this).val();
		mer_connectSelect(merchantID);
		selectMerchant();
	}
}); 

//边动作边搜寻
$("#merID").keyup(function(){
	var merchantID = $j(this).val();
	mer_connectSelect(merchantID);
	selectMerchant();
});

//商店变更动做
function selectMerchant(){
	var merchant_sn = $j("#selMerchantSN").val();
	$j("#terID").val("");
	$j.ajax({
		type:'post',
		url: '<?=$merchantActionStr?>',
		data: {merchant_sn:merchant_sn, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j('#selTerminalSN').html(response);
			$j("#selTerminalSN").trigger("change");
		}
	}) ;
}

//end 商店自动完成===============================================

//stert 端末机自动完成===============================================
//input自动连结select, 让select和 input同步
function ter_connectSelect(terminalID){
	var count = $j("#selTerminalSN option[value='"+terminalID+"']").length;

	if(count == 1){
		$j("#selTerminalSN option[value='"+terminalID+"']").attr("selected", true);
	}else{
		$j("#selTerminalSN option").eq(0).attr("selected", true);
	}	
}

//select自动连结input, 让input和 select同步
$j("#selTerminalSN").change(function(){
	var terminalID = $j(this).val();
	$j("#terID").val(terminalID);
}); 

//端末机, 取的栏位资料
function get_data() {
	//alert($j("#merID").val()+':'+$j("#terID").val());
    var data = {
        'merID': $j("#merID").val(),
		'terID': $j("#terID").val()
    };
    return data;
};

//端末机, 自动完成
$("#terID").autocomplete({ 
	//多参数方法, 变数名称可自取
	source: function(request, response) {
		$j.getJSON("<?=$ter_autoStr?>", get_data(), response);
	}, 
	minLength: 1,
	close: function() {	//选择清单后动作
		var terminalID = $j(this).val();
		ter_connectSelect(terminalID);
	}
}); 

//边动作边搜寻
$("#terID").keyup(function(){
	var terminalID = $j(this).val();
	ter_connectSelect(terminalID);
});

//end 端末机自动完成===============================================

//$j("#selMerchantSN").trigger("change");
//$j("#selTerminalSN").trigger("change");

</script>
<!--修正自动完成 载入 jquery.tools.min.js 后, 关闭form后导致选单出错-->
<script type="text/javascript">
	var $j = jQuery.noConflict();
</script>
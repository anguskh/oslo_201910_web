<?
	$web = $this->config->item('base_url');
	// $colInfo = is_array($colInfo) ? $colInfo : array() ;
	
	header('X-Frame-Options: DENY'); 
	header('X-XSS-Protection: 1; mode=block'); 
	header('X-Content-Type-Options: nosniff'); 

	//防止暂存
	// Expires in the past
	header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
	// Always modified
	header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
	// HTTP/1.1
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	// HTTP/1.0
	header("Pragma: no-cache");

	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title><?=$this->lang->line('login_user_title');?></title>
	
	<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
	<link href="<?=$web.$newcss;?>newcss/jquery.fancybox.css" rel="stylesheet" type="text/css">
	<link href="<?=$web.$newcss;?>newcss/fontawesome.css" rel="stylesheet" type="text/css">
	
	<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
	<script src="<?=$web?>js/jquery.tools.min.js"></script>
	<script src="<?=$web?>newjs/jquery.fancybox.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
</head>
<body id="login">
<!--新登入-->
	<div class="loginbox">
		<div id="form_wrapper" class="form_wrapper" >
			<!--登入-->
			<form id="login" method="post" class="login active" >
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<p><img src="<?=$web?>images/login.png" alt=""/></p>
				<p class="title">绿农创意管理系统<span>營運商管理</span></p>
				<div id="div_loginUser">
					<span><i class="fas fa-user"></i></span><input type="text" name="loginUser" id="loginUser" placeholder="<?=$this->lang->line('login_id');?>" TabIndex="1" class="txt">
				</div>
				<div id="div_loginpass">
					<span><i class="fas fa-lock"></i></span><input type="password" name="loginPass" id="loginPass" placeholder="<?=$this->lang->line('login_password');?>" TabIndex="2" value="" class="txt"/>
					<!-- span class="error">This is an error</span -->
				</div>
				<div id="checkvcode">
					<span><i class="fas fa-check-double"></i></span><input type="text" name="vcode" id="vcode" placeholder="<?=$this->lang->line('security_Input');?>" TabIndex="3" value="" class="txt"/>
					<!-- span class="error">This is an error</span -->
					<div id="checkimg">
						<img src="<?=$web?>simplecaptcha" id="captcha" />
						<a href="#" onclick="document.getElementById('captcha').src='<?=$web?>simplecaptcha?'+Math.random();" id="change-image">
							<i class="fas fa-redo"></i>
						</a>
					</div>
				</div>
				<div class="bottom" id="login_botton">
					<input type="submit" name="submit" id="login_submit" value="<?=$this->lang->line('login_button');?>" TabIndex="4" class="button">
					<div class="clear"></div>
				</div>
				<a class="forgot" href="" data-fancybox data-src="<?= $web ?>login/login_forgot" href="javascript:;">忘记密码 <span>？</span></a>
			</form>
		</div>
		<p class="copyright">
			©<?php echo date("Y"); ?> 綠農創意 版权所有
		</p>
	</div>

	<!-- The JavaScript -->
	<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>-->
		
	<script type="text/javascript">
		$(function() {
				//the form wrapper (includes all forms)
			var $form_wrapper	= $('#form_wrapper'),
				//the current form is the one with class active
				$currentForm	= $form_wrapper.children('form.active'),
				//the change form links
				$linkform		= $form_wrapper.find('.linkform');
					
			//get width and height of each form and store them for later						
			$form_wrapper.children('form').each(function(i){
				var $theForm	= $(this);
				//solve the inline display none problem when using fadeIn fadeOut
				if(!$theForm.hasClass('active'))
					$theForm.hide();
				$theForm.data({
					width	: $theForm.width(),
					height	: $theForm.height()
				});
			});
			
			//set width and height of wrapper (same of current form)
			setWrapperWidth();
			
			/*
			clicking a link (change form event) in the form
			makes the current form hide.
			The wrapper animates its width and height to the 
			width and height of the new current form.
			After the animation, the new form is shown
			*/
			$linkform.bind('click',function(e){
				var $link	= $(this);
				var target	= $link.attr('rel');
				$currentForm.fadeOut(400,function(){
					//remove class active from current form
					$currentForm.removeClass('active');
					//new current form
					$currentForm= $form_wrapper.children('form.'+target);
					//animate the wrapper
					$form_wrapper.stop()
									.animate({
									width	: $currentForm.data('width') + 'px',
									height	: $currentForm.data('height') + 'px'
									},500,function(){
									//new form gets class active
									$currentForm.addClass('active');
									//show the new form
									$currentForm.fadeIn(400);
									});
				});
				e.preventDefault();
			});
			
			function setWrapperWidth(){
				$form_wrapper.css({
					width	: $currentForm.data('width') + 'px',
					height	: $currentForm.data('height') + 'px'
				});
			}
			
			/*
			for the demo we disabled the submit buttons
			if you submit the form, you need to check the 
			which form was submited, and give the class active 
			to the form you want to show
			*/
			$form_wrapper.find('input[type="submit"]')
							.click(function(e){
							e.preventDefault();
							});	
		});
	</script>
        
	<!-- JavaScript includes -->
	<script src="<?=$web?>js/login_script.js"></script>
	<script src="<?=$web?>js/jquery.placeholder.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){
			// 帮有 placeholder 属性的输入框加上提示效果
			$('input[placeholder]').placeholder();
		});

		$('#login_submit').click(function () {
			formSubmit();
		});

		function formSubmit() {
			$('#login_submit').prop("disabled", true);
			if (typeof (JSON) == 'undefined') {
				//如果浏览器不支援JSON则载入json2.js
				$.getScript('<?=$web?>js/json2.js');
			}
			/* AJAX 比对资料库 */
			$.post("<?=$web?>login/login_check", {
				loginUser: $('#loginUser').val(),
				loginPass: $('#loginPass').val(),
				vcode : $('#vcode').val(), 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'
			}, function( response ) {	//判断群组登入前后台权限
				//obj[0]状态, obj[1]剩下尝试次数
				var obj = JSON.parse(response);
				var status = obj[0];
				var retry_count = obj[1];
				// alert(status);
				// alert(retry_count);
				status = status.toString();
				if( status === '2' ) {	//异常
					var error_msg = "";
					if(retry_count == -999){
						error_msg = "<?=$this->lang->line('login_account_password_error');?>";
					}else if ( retry_count <=0 ){	//超过最大使用次数
						error_msg = "<?=$this->lang->line('login_account_password_error');?>\n<?=$this->lang->line('login_account_lock_out');?>";
					}else{
						error_msg = "<?=$this->lang->line('login_account_password_error');?>\n<?=$this->lang->line('login_remain');?> "+obj[1]+" <?=$this->lang->line('login_count');?>";
					}
					alert(error_msg) ;
					$('#loginUser').val($('#loginUser').val());
					$('#loginPass').val("");
					$('#vcode').val("");
					$("#change-image").trigger("click");
					$('#login_submit').prop("disabled", false);
					//location.href = "login";
				} else if ( status === '0' ) {	//login_side = 1才可登入后台, 0 为前台
					//alert('<?=$this->session->userdata('session_sn')?>');
					location.href = "<?=$web?>main";
				} else if ( status === '3' ) {	//帐号被停用
					alert("<?=$this->lang->line('login_account_disabled');?>") ;
					$('#loginUser').val($('#loginUser').val());
					$('#loginPass').val($('#loginPass').val());
					$('#vcode').val("");
					$("#change-image").trigger("click");
					$('#login_submit').prop("disabled", false);
				} else if ( status === '4' ) {	//验证码错误
					alert("<?=$this->lang->line('login_verify_code_error');?>") ;
					$('#loginUser').val($('#loginUser').val());
					//$('#loginPass').val($('#loginPass').val());
					$('#loginPass').val("");
					$('#vcode').val("");
					$("#change-image").trigger("click");
					$('#login_submit').prop("disabled", false);
				} else if ( status === '9' ) {	//IP错误
					alert("<?=$this->lang->line('login_ip_error');?>") ;
					$('#loginUser').val($('#loginUser').val());
					$('#loginPass').val("");
					$('#login_submit').prop("disabled", false);
					//location.href = "login";
				}   else if ( status === '10' ) {	//ip不符合网段设定
					alert("营运商尚未设定，请洽管理员") ;
					$('#loginUser').val($('#loginUser').val());
					$('#loginPass').val("");
					location.href = "<?=$web?>login";
					$('#login_submit').prop("disabled", false);
				} else {	//其他
					alert("<?=$this->lang->line('login_account_password_error');?>") ;
					$('#loginUser').val($('#loginUser').val());
					$('#loginPass').val("");
					location.href = "<?=$web?>login";
					$('#login_submit').prop("disabled", false);
				}
			});
		}
		$(document).ready( function() {
			$("#loginUser").focus();
		});

		$("#vcode").keyup(function(){
			var val = $(this).val();
			//alert(val);
			$(this).val(val.toUpperCase());
		});

		var obj_forgot = $("#forgot");
		var obj_backlogin = $("#backlogin");
		var obj_div_loginUser = $("#div_loginUser");
		var obj_div_loginpass = $("#div_loginpass");
		var obj_div_email = $("#div_email");
		var obj_login_submit = $("#login_submit");
		var obj_mail_submit = $("#mail_submit");
		var obj_email = $('#email');
		var obj_checkvcode = $('#checkvcode');
		var obj_checkimg = $('#checkimg');

		var fadeSpeed = 300;
		obj_forgot.click(function(){
			obj_login_submit.fadeOut(fadeSpeed);
			obj_div_loginUser.fadeOut(fadeSpeed);
			obj_checkvcode.fadeOut(fadeSpeed);
			obj_checkimg.fadeOut(fadeSpeed);
			obj_div_loginpass.fadeOut(fadeSpeed, function(){
				obj_div_email.fadeIn();
				obj_mail_submit.fadeIn();
				obj_forgot.hide();
				obj_backlogin.show();
			});
		});

		obj_backlogin.click(function(){
			obj_mail_submit.fadeOut(fadeSpeed);
			obj_div_email.fadeOut(fadeSpeed, function(){
				obj_div_loginUser.fadeIn();
				obj_div_loginpass.fadeIn();
				obj_checkvcode.fadeIn();
				obj_checkimg.fadeIn();
				obj_login_submit.fadeIn();
				obj_backlogin.hide();
				obj_forgot.show();
			});
		});

		obj_mail_submit.click(function(){
			/* AJAX 比对资料库 */
			$.post("<?=$web?>nimda/login/forgotemail", {
				recoverEmail: obj_email.val()
			}, function( response ) {
				if( response == "0" ) {
					obj_email.val("");
					alert("<?=$this->lang->line('login_email_wrong_email_address');?>");
				}
				else if(response == "1"){
					obj_email.val("");
					alert("<?=$this->lang->line('login_email_right_email_address');?>");
				}
				else
				{
					alert(response);
				}
			});
		});
	</script>
</body>
</html>
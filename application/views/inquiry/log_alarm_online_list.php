<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"edit_status"		=>	"编辑",
		"top01"				=>	"{$this->lang->line('so_num')}",
		"bss_id"			=>	"{$this->lang->line('bss_id_name')}",
		"log_date"			=>	"{$this->lang->line('log_date')}",
		"bss_token_id"		=>	"{$this->lang->line('bss_token_id')}",
		"type"				=>	"{$this->lang->line('type')}",
		"status"			=>	"{$this->lang->line('status')}",
		"status_desc"		=>	"{$this->lang->line('status_desc')}",
		"view_date"			=>	"{$this->lang->line('view_date')}",
		"view_users"		=>	"{$this->lang->line('view_users')}",
		"process_date"		=>	"{$this->lang->line('process_date')}",
		"process_user"		=>	"{$this->lang->line('process_user')}",
		"finish_date"		=>	"{$this->lang->line('finish_date')}",
		"finish_user"		=>	"{$this->lang->line('finish_user')}"
		
	);

	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	
	$searchArr = array (
		"top.top01"		=>  "{$this->lang->line('select_operator_class')}",
		"tbss.bss_id"	=>	"{$this->lang->line('select_batteryswaps_class')}"
	);
	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$batteryswapstationListRow = $this->Model_show_list->getbatteryswapstationList() ;
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"top.top01"		=>  $operatorsSelectListRow,
		"tbss.bss_id"	=>	$batteryswapstationListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	//echo 'A:'.$this->session->userdata('start_date');

	$type_name = array('','火灾','淹水','无轨道可归还',"前台時間與系統時間相差{$this->session->userdata('bss_time_min')}分鐘以上", "轨道取电未关门");
	$status_name = array('','未检视','处理中', '已完成');


?>
<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('log_alarm_online_inquiry')?></p>
			<div class="topbtn column">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="換電站Token ID" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_alarm_online">
			<tr class="GridViewScrollHeader">
			<?
				$no_list = '';
				$n=1;
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				foreach ($colInfo["colName"] as $enName => $chName) {
					$style = "";
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" {$style}>
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 

					$n++;
				}
?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						$sn = $patternfilefieldInfoArr['alarm_online_sn'];
						$n = 1;
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == 'type'){
								if($patternfilefieldInfoArr[$enName] == "5"){
									$patternfilefieldInfoArr[$enName] = "第".$patternfilefieldInfoArr["not_close_track_no"].$type_name[$patternfilefieldInfoArr[$enName]];
								}else{
									$patternfilefieldInfoArr[$enName] = $type_name[$patternfilefieldInfoArr[$enName]];
								}
							}else if($enName == 'status'){
								$patternfilefieldInfoArr[$enName] = $status_name[$patternfilefieldInfoArr[$enName]];
							}else if($enName == 'edit_status'){
								$patternfilefieldInfoArr[$enName] = '<a data-fancybox data-src="#editfancy" class="edit" href="javascript:;" onclick="edit_online('.$sn.');"></a>';
							}else if($enName == 'view_users'){
								$user_name_arr = '';
								$user_name = array();
								$viewUser = '';
								if($patternfilefieldInfoArr[$enName] != ''){
									$user_name_arr = explode(',',$patternfilefieldInfoArr[$enName]);
									$user_count = count($user_name_arr);
									for($i=0; $i<$user_count; $i++){
										if(isset($sysusername[$user_name_arr[$i]])){
											$user_name[] = $sysusername[$user_name_arr[$i]];
										}
									}

									$viewUser = join( ",", $user_name );
								}
								$patternfilefieldInfoArr[$enName] = $viewUser;
							}else if($enName == 'process_user'){
								$process_user = $patternfilefieldInfoArr[$enName];
								if(isset($sysusername[$patternfilefieldInfoArr[$enName]])){
									$process_user = $sysusername[$patternfilefieldInfoArr[$enName]];
								
								}
								$patternfilefieldInfoArr[$enName] = $process_user;
							}else if($enName == 'finish_user'){
								$finish_user = $patternfilefieldInfoArr[$enName];
								if(isset($sysusername[$patternfilefieldInfoArr[$enName]])){
									$finish_user = $sysusername[$patternfilefieldInfoArr[$enName]];
								
								}
								$patternfilefieldInfoArr[$enName] = $finish_user;
							}

							echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";

							$n++;
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
                            '.$no_list.'
                        </tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}


?>
			</table>

        </form>
        <!-- 分页区 -->
        <div class="pagebar">
			<?= $pageInfo["html"] ?>
        </div>
    </div>
</div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->

<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<div style="display: none;" id="editfancy">
	<div id="edit_label" class="edit_label">
		
	</div>
</div>

<script type="text/javascript">
	function edit_online(sn){
        $j.ajax({
            type:'post',
            url: '<?= $web ?>inquiry/log_alarm_online/select_edit_label',
			data: { fn: '<?=$fn?>',
					sn: sn,
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
            error: function(xhr) {
                strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
                alert(strMsg);
            },
            success: function (response) {
				$j("#editfancy").show();
                $j('#edit_label').html(response);
            }
        }) ;
	}

<?
//if ($this->session->userdata("{$fn}_".'selto_num') == "" && $this->session->userdata("{$fn}_".'seltso_num') == "" && $this->session->userdata("{$fn}_".'selsv_num') == "") {
?>
	 $j(document).ready(function() {
		//凍結窗格(4欄)
		gridview(4,true);
	 	// promptWin();

		//	var triggersOnload = $j("#selBank").overlay({
        //         mask: {
       //             color: '#000',
       //              loadSpeed: 200,
       //             opacity: 0.7
       //          },
	 	//		closeOnClick: false, 
	 	//		closeOnEsc: false, 
       //        load: true
       //    });
			
	 ///		$j('a.close, #modal').live('click', function() {
				// close the overlay
	//			triggersOnload.eq(0).overlay().close();
	//			return false;
 	//		});
	});
<?
//}
?>

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});


    /**
     * 重选
     */
    $j('#btReload').click(function () {
        promptWin();
    });

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());

        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_alarm_online/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_alarm_online/search" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
// document.onkeydown = killesc; 
// function   killesc() 

// {   
// 	if(window.event.keyCode==27)   
// 	{   
// 		window.event.keyCode=0;   
// 		window.event.returnValue=false;   
// 	}   
// } 

</script>

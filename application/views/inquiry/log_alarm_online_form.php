<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"alarm_online_sn"		=> "",
		"so_num"				=> "",
		"sb_num" 				=> "",
		"log_date"				=> "",
		"bss_token_id"			=> "",
		"type"					=> "",
		"status"				=> "",
		"status_desc"			=> ""
	);
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
	<body id="login">
		<form id="formID" action="<?=$action?>" method="post">
		<!--预防CSRF攻击-->
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		<INPUT TYPE="hidden" NAME="s_num" value="<?=$dataInfo['alarm_online_sn'];?>">
		<INPUT TYPE="hidden" NAME="source_type" value="<?=$source_type;?>">
		<div class="section alarmbox" style="width: 270px; padding:0;">
			<p class="title">即时告警通知记录</p>
			<div class="container-box" style="float:none;">
				<ul style="width:100%;">
					<li>
						<label><?=$this->lang->line('so_num')?></label>
						<select id='so_num' style="width:200px;" disabled>
							<?=create_select_option($batteryswapstationList, $dataInfo["so_num"], $this->lang->line('battery_swap_ccb_select_operator'), array('so_num'));?>
						</select>
					</li>
					<li>
						<label><?=$this->lang->line('sb_num')?></label>
						<select id='sb_num' style="width:200px;" disabled>
							<?=create_select_option($batteryswapstationList, $dataInfo["sb_num"], $this->lang->line('battery_swap_ccb_select_station'), array('sb_num'));?>
						</select>
					</li>
					<li>
						<label><?=$this->lang->line('log_date')?></label>
						<input type="text" class="txt" style="width: 100%;" name="postdata[log_date]" id="log_date" value="<?=$dataInfo["log_date"]?>" disabled>		
					</li>
					<li>
						<label><?=$this->lang->line('bss_token_id')?></label>
						<input type="text" class="txt" style="width: 100%;" id="bss_token_id" value="<?=$dataInfo["bss_token_id"]?>" disabled>		
					</li>
					<li>
						<label><?=$this->lang->line('type')?></label>
						<input type="radio" class="radio first" name="postdata[type]" id="type" value="1" <?echo (($dataInfo["type"]=='1')?'checked':''); ?>><?=$this->lang->line('type_1')?>
						<input type="radio" class="radio" name="postdata[type]" id="type" value="2" <?echo (($dataInfo["type"]=='2')?'checked':''); ?>><?=$this->lang->line('type_2')?>
						<input type="radio" class="radio" name="postdata[type]" id="type" value="3" <?echo (($dataInfo["type"]=='3')?'checked':''); ?>><?=$this->lang->line('type_3')?>
						<input type="radio" class="radio" name="postdata[type]" id="type" value="5" <?echo (($dataInfo["type"]=='5')?'checked':''); ?>><?=$this->lang->line('type_5')?>
					</li>
					<li>
						<label><?=$this->lang->line('status')?></label>
						<input type="radio" class="radio first" name="postdata[status]" id="status" value="1" <?echo (($dataInfo["status"]=='1')?'checked':''); ?>><?=$this->lang->line('status_1')?>
						<input type="radio" class="radio" name="postdata[status]" id="status" value="2" <?echo (($dataInfo["status"]=='2')?'checked':''); ?>><?=$this->lang->line('status_2')?>
						<input type="radio" class="radio" name="postdata[status]" id="status" value="3" <?echo (($dataInfo["status"]=='3')?'checked':''); ?>><?=$this->lang->line('status_3')?>
						<INPUT TYPE="hidden" NAME="old_status" value="<?=$dataInfo["status"];?>">
					</li>
					<li>
						<label><?=$this->lang->line('status_desc')?></label>
						<input type="text" class="txt validate[status_desc]" name="postdata[status_desc]" id="status_desc" value="<?=$dataInfo["status_desc"]?>">		
					</li>
				</ul>
			</div>
		</div>
		<div class="sectin buttonbar"><a style="cursor:pointer" id="btSave" class="buttongreen">存档</a></div>
		</form>
	</body>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
	});
	
	//储存
	$j("#btSave").click( function () {
		$j("#formID").submit();
	});

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
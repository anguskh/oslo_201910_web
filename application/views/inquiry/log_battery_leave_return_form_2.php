<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"							=>			"",
		"system_log_date"		=>			"",
		"leave_sb_num"			=>			"",
		"tm_num"						=>			"",
		"leave_date"					=>			"",
		"leave_track_no"			=>			"",
		"leave_battery_id"		=>			""
	);

	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
?>
<!-- script src="<?=$web?>js/jquery.twzipcode.js"></script -->
<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->


		<div class="section alarmbox" style="padding:0;">	
			<p class="title"><?=$this->lang->line('log_battery_leave_return_inquiry')?><span>补登</span></p>
			<p style="color: #777;">电池序号：<?=$dataInfo['leave_battery_id']?></p>
			<p style="color: #777;">会员名称：<?=$dataInfo['member_name']?>（<?=$dataInfo['battery_user_id']?>）</p>
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<INPUT TYPE="hidden" NAME="webs" id="webs" value="<?=$web;?>">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
			<input type="hidden" name="leave_date" value="<?=$dataInfo['leave_date']?>" />
			<div class="container-box col-md-6">
				<ul>
					<li>
						<label for=""><?=$this->lang->line('log_battery_leave_return_return_sb_num')?></label>
						<select name="postdata[return_sb_num]" id='return_sb_num' style="width:240px;">
<?
							foreach($batteryswapstationList as $key => $val){
								echo '<option value="'.$val['s_num'].'" val_name="'.$val['bss_id_name'].'">'.$val['bss_id_name'].'</option>';
							}
?>
						</select>
					</li>
					<li>
						<label for=""><?=$this->lang->line('log_battery_leave_return_return_date')?></label>
						<input type="datetime-local" class="txt" id="return_date" name="postdata[return_date]" value="<?=$dataInfo["return_date"]?>" style="width:240px;">
					</li>
				</ul>
			</div>
			<div class="container-box col-md-6">
				<ul>
					<li>
						<label for=""><?=$this->lang->line('log_battery_leave_return_return_track_no')?></label>
						<select name="postdata[return_track_no]" id='return_track_no' >
							<option value=""></option>
<?
							for($i=1; $i<=12; $i++){
								echo '<option value="'.$i.'">'.$i.'</option>';
							}
?>								
						</select>
					</li>
					<li>
						<label for=""><?=$this->lang->line('log_battery_leave_return_return_battery_id')?></label>
						<input class="txt" id="return_battery_id" name="postdata[return_battery_id]" value="<?=$dataInfo["return_battery_id"]?>" style="width:240px;">
					</li>
				</ul>
			</div>
			<div class="sectin buttonbar" id="body">
				<a style="cursor:pointer" id="btSave" class="buttongreen">存档</a>
			</div>
			</form>
		</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script src="<?=$web;?>js/common/common.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		//储存和取消
		save_cancel();
		show_combobox("#return_sb_num",250);
	});

</script>
<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
function fn_save(){
	//檢查電池序號有沒有填對
	var return_battery_id = '<?=$dataInfo["leave_battery_id"]?>';
	if( $j("#return_battery_id").val() == '' || return_battery_id != $j("#return_battery_id").val()){
		alert('归还电池序号有误');
	}else{
		$j("#formID").submit();
	}
}

</script>

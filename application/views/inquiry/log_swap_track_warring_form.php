<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"					=> "",
		"log_date"				=> "",
		"bss_id"				=> "",
		"track_no"				=> "",
		"type"					=> "",
		"status"				=> "",
		"status_desc"			=> ""
	);
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
	<body id="login">
		<form id="formID" action="<?=$action?>" method="post">
		<!--预防CSRF攻击-->
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		<INPUT TYPE="hidden" NAME="s_num" value="<?=$dataInfo['s_num'];?>">
		<div class="section alarmbox" style="padding:0;">
			<p class="title">电池即时告警通知</p>
			<div class="container-box">
				<ul>
					<li>
						<label><span></span><?=$this->lang->line('log_date')?></label>
						<input type="text" class="txt" name="postdata[log_date]" id="log_date" value="<?=$dataInfo["log_date"]?>" disabled>		
					</li>
					<li>
						<label><span></span><?=$this->lang->line('bss_id')?></label>
						<input type="text" class="txt" id="bss_id" value="<?=$dataInfo["bss_id"]?>" disabled>		
					</li>
					<li>
						<label><span></span><?=$this->lang->line('track_no')?></label>
						<input type="text" class="txt" id="track_no" value="<?=$dataInfo["track_no"]?>" disabled>		
					</li>
					<li>
						<label><span></span><?=$this->lang->line('type')?></label>
						<input type="radio" class="radio first" name="postdata[type]" id="type" value="E" <?echo (($dataInfo["type"]=='E')?'checked':''); ?>><?=$this->lang->line('type_E')?>
						<input type="radio" class="radio" name="postdata[type]" id="type" value="N" <?echo (($dataInfo["type"]=='N')?'checked':''); ?>><?=$this->lang->line('type_N')?>
					</li>
					<li>
						<label><span></span><?=$this->lang->line('status')?></label>
						<input type="radio" class="radio first" name="postdata[status]" id="status" value="1" <?echo (($dataInfo["status"]=='1')?'checked':''); ?>><?=$this->lang->line('status_1')?>
						<input type="radio" class="radio" name="postdata[status]" id="status" value="2" <?echo (($dataInfo["status"]=='2')?'checked':''); ?>><?=$this->lang->line('status_2')?>
						<input type="radio" class="radio" name="postdata[status]" id="status" value="3" <?echo (($dataInfo["status"]=='3')?'checked':''); ?>><?=$this->lang->line('status_3')?>
						<INPUT TYPE="hidden" NAME="old_status" value="<?=$dataInfo["status"];?>">
					</li>
					<li>
						<label><span></span><?=$this->lang->line('status_desc')?></label>
						<input type="text" class="txt validate[status_desc]" name="postdata[status_desc]" id="status_desc" value="<?=$dataInfo["status_desc"]?>">		
					</li>
				</ul>
			</div>
		</div>
		<div class="sectin buttonbar"><a style="cursor:pointer" id="btSave" class="buttongreen">存档</a></div>
		</form>
	</body>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
	});
	
	//储存
	$j("#btSave").click( function () {
		$j("#formID").submit();
	});

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"s_num"							=>  "{$this->lang->line('s_num')}",
		"do_num"						=>	"{$this->lang->line('do_num')}",
		"dso_num"						=>	"{$this->lang->line('dso_num')}",
		"unit_id"						=>	"{$this->lang->line('log_driving_battery_id')}",
		"battery_voltage"				=>	"{$this->lang->line('battery_voltage')}",
		"battery_amps"					=>	"{$this->lang->line('battery_amps')}",
		"battery_temperature"			=>	"{$this->lang->line('battery_temperature')}",
		//"environment_temperature"		=>	"{$this->lang->line('environment_temperature')}",
		"battery_capacity"				=>	"{$this->lang->line('battery_capacity')}",
		"battery_station_position"		=>	"{$this->lang->line('battery_station_position')}",
		"battery_gps_manufacturer"		=>	"{$this->lang->line('battery_gps_manufacturer')}",
		"battery_gps_version"			=>	"{$this->lang->line('battery_gps_version')}",
		
	);

	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	
	$searchArr = array (
		"do_num"	=>  "{$this->lang->line('select_operator_class')}",
		"dso_num"	=>	"{$this->lang->line('select_suboperator_class')}",
		"ldi.do_num"	=>  "{$this->lang->line('select_dealer_class')}",
		"ldi.dso_num"	=>	"{$this->lang->line('select_subdealer_class')}",
		"sv_num"	=>	"{$this->lang->line('select_vehicle_class')}"
	);
	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$sub_operatorSelectListRow = $this->Model_show_list->getsuboperatorList() ;
	$vehicleSelectListRow = $this->Model_show_list->getvehicleList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;
	$sub_dealerSelectListRow = $this->Model_show_list->getsubdealerList() ;
	
	$show_array = array('op' => $operatorsSelectListRow, 'sub_op' => $sub_operatorSelectListRow, 'dealer' => $dealerSelectListRow, 'sub_dealer' => $sub_dealerSelectListRow);
	$s_name = array('op'=>'top01', 'sub_op'=>'tsop01', 'dealer'=>'tde01', 'sub_dealer'=>'tsde01');
	$all_array = array();

	if(isset($operatorsSelectListRow)){
		foreach($show_array as $key=>$arr){
			if(count($arr)>0){
				foreach($arr as $val){
					$all_array[$key][$val['s_num']] = $val[$s_name[$key]];
				}
			}
		}
	}


	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"do_num"		=>  $operatorsSelectListRow,
		"dso_num"		=>	$sub_operatorSelectListRow,
		"ldi.do_num"	=>  $dealerSelectListRow,
		"ldi.dso_num"	=>	$sub_dealerSelectListRow,
		"sv_num"		=>	$vehicleSelectListRow
	);

	$fourInout = array(
		"do_num"		=>  'Y',
		"xdso_num"		=>	'Y',
		"ldi.do_num"		=>	'Y',
		"ldi.dso_num"		=>	'Y'
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType, array(), array(), array(), array(), $fourInout);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	//echo 'A:'.$this->session->userdata('start_date');
?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('log_driving_inquiry')?></p>
			<div class="topbtn column">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="电池序号" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_driving_info">
				<tr class="GridViewScrollHeader">		
				<?
					//让标题栏位有排序功能
				    foreach ($colInfo["colName"] as $enName => $chName) {
						if($enName == "driving_date"){
							$style = " style='width: 100px;' " ;
						}else if($enName == "MERCHANT_ID"){
							$style = " style='width: 130px;' " ;
						}else if($enName == "user_name"){
							$style = " style='width: 80px;' " ;
						}else if($enName == "function_name"){
							$style = " style='width: 90px;' " ;
						}else if($enName == "mode"){
							$style = " style='width: 60px;' " ;
						}else if($enName == "desc"){
							$style = " style='width: 300px;' " ;
						}else if($enName == "before_desc"){
							$style = " style='width: 300px;' " ;
						}else{
							$style = "";
						}
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
				        echo "					<th align=\"center\" {$style}>
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
				    }
				?>
				</tr>
					<?
						if(isset($InfoRow)){

							foreach ($InfoRow as $key => $InfoArr) {
								echo "				<tr class=\"GridViewScrollItem\">\n" ;


								$checkStr = " ";
								echo $checkStr;
								foreach ($colInfo["colName"] as $enName => $chName) {
									if($enName == "battery_temperature" || $enName == "environment_temperature")
									{
										echo "					<td align=\"center\" class=\"\" >" . $InfoArr[$enName] ."度</td>\n";
									}else if($enName == "battery_capacity"){
										echo "					<td align=\"center\" class=\"\" >" . $InfoArr[$enName] ."%</td>\n";
									}else if($enName == 'do_num'){
										$do_name = "";
										
										if($InfoArr['DorO_flag'] == 'O'){
											$do_name = ((isset($all_array['op'][$InfoArr[$enName]]))?$all_array['op'][$InfoArr[$enName]]:'');
										}else{
											$do_name = ((isset($all_array['dealer'][$InfoArr[$enName]]))?$all_array['dealer'][$InfoArr[$enName]]:'');
										}
										echo "					<td align=\"center\" class=\"\" style='color:black;' >" . $do_name . "</td>\n";
									}else if($enName == 'dso_num'){
										$dso_name = "";
										if($InfoArr['DorO_flag'] == 'O'){
											$dso_name = ((isset($all_array['sub_op'][$InfoArr[$enName]]))?$all_array['sub_op'][$InfoArr[$enName]]:'');
										}else{
											$dso_name = ((isset($all_array['sub_dealer'][$InfoArr[$enName]]))?$all_array['sub_dealer'][$InfoArr[$enName]]:'');
										
										}
										echo "					<td align=\"center\" class=\"\" style='color:black;' >" . $dso_name . "</td>\n";
									}else {
										echo "					<td align=\"center\" class=\"\" style='color:black;' >" . $InfoArr[$enName] . "</td>\n";
									}
								}
								echo "				</tr>\n";
							}
						}else{
							echo '<td align="center" colspan="9" style="color:#ff0000;">请先查询后才会有资料出现</td>';
						}
                    ?>
            </table>
        </form>
        <!-- 分页区 -->
        <div class="pagebar">
			<?= $pageInfo["html"] ?>
        </div>
    </div>
</div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->

<div class="modal" id="selBank" style="width: 450px; height: 140px;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">

    function promptWin()
    {
        $j('#selBank').css({
            'margin-top' : document.documentElement.clientHeight / 2 - $j('#selBank').height() / 2 - 90,
            'margin-left' : 0
        });

        $j.ajax({
            type:'post',
            url: '<?= $web ?>inquiry/Log_driving/select_log_driving',
			data: { fn: '<?=$fn?>',
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
            error: function(xhr) {
                strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
                alert(strMsg);
            },
            success: function (response) {
                $j('#selBank').html(response);
            }
        }) ;
    }

<?
//if ($this->session->userdata("{$fn}_".'selto_num') == "" && $this->session->userdata("{$fn}_".'seltso_num') == "" && $this->session->userdata("{$fn}_".'selsv_num') == "") {
?>
	 $j(document).ready(function() {
	 	// promptWin();

		//	var triggersOnload = $j("#selBank").overlay({
        //         mask: {
       //             color: '#000',
       //              loadSpeed: 200,
       //             opacity: 0.7
       //          },
	 	//		closeOnClick: false, 
	 	//		closeOnEsc: false, 
       //        load: true
       //    });
			
	 ///		$j('a.close, #modal').live('click', function() {
				// close the overlay
	//			triggersOnload.eq(0).overlay().close();
	//			return false;
 	//		});
		//凍結窗格
		gridview(0,false);
	});
<?
//}
?>

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});


    /**
     * 重选
     */
    $j('#btReload').click(function () {
        promptWin();
    });

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());

        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_driving/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_driving/search" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
// document.onkeydown = killesc; 
// function   killesc() 

// {   
// 	if(window.event.keyCode==27)   
// 	{   
// 		window.event.keyCode=0;   
// 		window.event.returnValue=false;   
// 	}   
// } 


</script>

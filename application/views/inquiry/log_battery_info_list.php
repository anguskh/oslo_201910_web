<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"do_name"				=>	"{$this->lang->line('log_battery_info_do_num')}",
		"bss_id"						=>	"{$this->lang->line('log_battery_info_bss_id_name')}",
		"log_date"				=>	"{$this->lang->line('log_battery_info_log_date')}",
		"track_no"				=>	"{$this->lang->line('log_battery_info_track_no')}",
		"battery_id"				=>	"{$this->lang->line('log_battery_info_battery_id')}",
		"vehicle_user_id"	=>	"{$this->lang->line('log_battery_info_vehicle_user_id')}",
		"battery_voltage"				=>	"{$this->lang->line('log_battery_info_battery_voltage')}",
		"battery_cell_status"		=>	"{$this->lang->line('log_battery_info_battery_cell_status')}",
		"battery_amps"					=>	"{$this->lang->line('log_battery_info_battery_amps')}",
		"battery_capacity"			=>	"{$this->lang->line('log_battery_info_battery_capacity')}",
		"battery_temperature"	=>	"{$this->lang->line('log_battery_info_battery_temperature')}",
		"charge_cycles"					=>	"{$this->lang->line('log_battery_info_charge_cycles')}",
		"electrify_time"					=>	"{$this->lang->line('log_battery_info_electrify_time')}",
		"status"						=>	"{$this->lang->line('log_battery_info_status')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";

	$searchArr = array (
		"top.top01"	=>  "{$this->lang->line('select_operator_class')}",
		"td.tde01"	=>  "{$this->lang->line('select_dealer_class')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"top.top01"		=>  $operatorsSelectListRow,
		"td.tde01"		=>  $dealerSelectListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$exchange_type_name = array('离线交换','连线交换');
	$status_name = array('充电中','饱电');
	
	//echo 'A:'.$this->session->userdata('start_date');
?>
<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('log_battery_info_inquiry')?></p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="电池序号" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_info">
				<tr class="GridViewScrollHeader">
<?
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				$n=1;
				
				$no_list = '';
				foreach ($colInfo["colName"] as $enName => $chName) {
					
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" scope=\"col\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}

?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName=='bss_id'){
								echo "					<td align=\"center\" class=\"\" >".$patternfilefieldInfoArr['location'].(($patternfilefieldInfoArr['bss_id'] != '')?'（'.$patternfilefieldInfoArr['bss_id'].'）':'')."</td>\n";
							}else{
								echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
							}
						
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
                            '.$no_list.'
                        </tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">
$j(document).ready(function() {
	$j('a.close, #modal').live('click', function() {
	// close the overlay
	triggersOnload.eq(0).overlay().close();
	return false;
	});
	
	//凍結窗格(3欄)
	gridview();
});

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_battery_info/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_battery_info/search" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();
</script>

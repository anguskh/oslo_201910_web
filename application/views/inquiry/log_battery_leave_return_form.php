<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"					=>			"",
		"system_log_date"		=>			"",
		"leave_sb_num"			=>			"",
		"tm_num"				=>			"",
		"leave_date"			=>			"",
		"leave_track_no"		=>			"",
		"leave_battery_id"		=>			""
	);

	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
?>
<script src="<?=$web?>js/jquery.tools.min.js"></script>
<script type="text/javascript">
//將jquery.tools.min.js 的jquery 改為$auto, 避免與原始版本的jquery衝突
var $auto = jQuery.noConflict();
</script>
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<!-- 下拉搜尋 多筆勾選 -->
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>
<!-- 可輸入可下拉 -->
<link rel="stylesheet" href="<?=$web?>js/easyui/easyui.css"/>
<script type="text/javascript" src="<?=$web?>js/easyui/jquery.easyui.min.js"></script> 
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>

<!-- script src="<?=$web?>js/jquery.twzipcode.js"></script -->
<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
		
			<p class="btitle"><?=$this->lang->line('log_battery_leave_return_inquiry')?><span><?=$show_sub_title;?></span></p>
		
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<INPUT TYPE="hidden" NAME="webs" id="webs" value="<?=$web;?>">
			<div class="section">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="leftbox edit2">
					<ul>
						<li>
							<label for=""><span></span><?=$this->lang->line('log_battery_leave_return_leave_sb_num')?></label>
							<select name="postdata[leave_sb_num]" id='leave_sb_num' style="width:240px;">
<?
								foreach($batteryswapstationList as $key => $val){
									echo '<option value="'.$val['s_num'].'" val_name="'.$val['bss_id_name'].'">'.$val['bss_id_name'].'</option>';
								}
?>
							</select>
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('log_battery_leave_return_tm_num')?></label>
							<select name="postdata[tm_num]" id='tm_num' style="width:240px;">
								<?=create_select_option($memberList, $dataInfo["tm_num"]);?>
							</select>
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('log_battery_leave_return_leave_date')?></label>
							<input type="datetime-local" class="txt" id="leave_date" name="postdata[leave_date]" value="<?=$dataInfo["leave_date"]?>" style="width:240px;">
						</li>

				</div>
				<div class="rightbox edit2">
					<ul>
						<li>
							<label for=""><span></span><?=$this->lang->line('log_battery_leave_return_leave_track_no')?></label>
							<select name="postdata[leave_track_no]" id='leave_track_no' >
<?
								for($i=1; $i<=12; $i++){
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
?>								
							</select>
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('log_battery_leave_return_leave_battery_id')?></label>
							<select name="postdata[leave_battery_id]" id='leave_battery_id' style="width:240px;">
								<?=create_select_option($batteryList, $dataInfo["leave_battery_id"]);?>
							</select>
						</li>
					</ul>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
		show_combobox("#leave_battery_id",250);
		show_combobox("#leave_sb_num",250);
		show_combobox("#tm_num",250);
	});

	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>
<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
</script>
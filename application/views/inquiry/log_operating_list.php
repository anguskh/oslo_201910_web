<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"operating_date"		=>	"{$this->lang->line('log_operating_operating_date')}",
		"function_name"			=>	"{$this->lang->line('log_operating_function_name')}",
		"user_name"				=>	"{$this->lang->line('log_operating_user_sn')}",
		"mode"					=>	"{$this->lang->line('log_operating_mode')}",
		"before_desc"			=>	"{$this->lang->line('log_operating_before_desc')}",
		"desc"					=>	"{$this->lang->line('log_operating_desc')}",
		"ip_address"			=>	"{$this->lang->line('log_operating_ip_address')}",
		"status"				=>	"{$this->lang->line('log_operating_status')}",
		//"sql"					=>	"{$this->lang->line('system_operation_log_sql')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	$userSelectListRow = $this->model_user->getUserListNoPage() ;


	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		'operating_date' => 'date',
		'user_name' => $userSelectListRow
	);
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData, $fieldType);
	
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	//echo 'A:'.$this->session->userdata('start_date');
?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('log_operating_inquiry')?></p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="使用者名称" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_operating">
					<tr class="GridViewScrollHeader">
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == "operating_date"){
								$style = " style='width: 100px;' " ;
							}else if($enName == "MERCHANT_ID"){
								$style = " style='width: 130px;' " ;
							}else if($enName == "user_name"){
								$style = " style='width: 80px;' " ;
							}else if($enName == "function_name"){
								$style = " style='width: 90px;' " ;
							}else if($enName == "mode"){
								$style = " style='width: 60px;' " ;
							}else if($enName == "desc"){
								$style = " style='width: 300px;' " ;
							}else if($enName == "before_desc"){
								$style = " style='width: 300px;' " ;
							}else{
								$style = "";
							}
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
					if(isset($InfoRow)){
						foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
							echo "				<tr class=\"GridViewScrollItem\">\n" ;


							$checkStr = " ";
							echo $checkStr;
							foreach ($colInfo["colName"] as $enName => $chName) {
								if ($enName == "mode") {
									switch ($patternfilefieldInfoArr[$enName]) {
										case "0":
											echo "					<td align=\"center\"><font color=blue>{$this->lang->line('browser')}</font></td>\n";
											break;
										case "1":
											echo "					<td align=\"center\"><font color=blue>{$this->lang->line('add')}</font></td>\n";
											break;
										case "2":
											echo "					<td align=\"center\"><font color=red>{$this->lang->line('edit')}</font></td>\n";
											break;
										case "3":
											echo "					<td align=\"center\"><font color=red>{$this->lang->line('delete')}</font></td>\n";
											break;
									}
								} else if($enName == "desc" || $enName == "before_desc"){
									echo "					<td align=\"left\" class=\"autobreak descclass\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
								} else if($enName == "function_name"){
									$function_name = $patternfilefieldInfoArr[$enName];
									if(isset($menuData[$function_name])){
										$function_name = $menuData[$function_name];
									}
									echo "					<td align=\"left\" class=\"autobreak descclass\" >" . $function_name . "</td>\n";
								} else if($enName == "status"){
									switch ($patternfilefieldInfoArr[$enName]) {
										case "0":	//失败
											echo "					<td align=\"center\"><font color=red>{$this->lang->line('failed_action')}</font></td>\n";
											break;
										case "1":	//成功
											echo "					<td align=\"center\"><font color=blue>{$this->lang->line('success_action')}</font></td>\n";
											break;
										default:
											echo "					<td align=\"center\"><font color=blue></font></td>\n";
											break;
									}
								} else {
									echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
								}
							}
							echo "				</tr>\n";
						}
					}else{
					
						echo '<td align="center" colspan="8" style="color:#ff0000;">请先查询后才会有资料出现</td>';
					
					}
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<script type="text/javascript">

    function promptWin()
    {
        $j('#selBank').css({
            'margin-top' : document.documentElement.clientHeight / 2 - $j('#selBank').height() / 2 - 90,
            'margin-left' : 0
        });

        $j.ajax({
            type:'post',
            url: '<?= $web ?>inquiry/log_operating/select_log_operating',
			data: { fn: '<?=$fn?>',
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
            error: function(xhr) {
                strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
                alert(strMsg);
            },
            success: function (response) {
                $j('#selBank').html(response);
            }
        }) ;
    }

<?
//if ($this->session->userdata("{$fn}_".'start_date') == "") {
?>
	$j(document).ready(function() {
		//promptWin();

         //   var triggersOnload = $j("#selBank").overlay({
        //        mask: {
        //            color: '#000',
        //            loadSpeed: 200,
        //            opacity: 0.7
        //        },
		//		closeOnClick: false, 
		//		closeOnEsc: false, 
       //         load: true
       //     });
			
		//	$j('a.close, #modal').live('click', function() {
		//	// close the overlay
		//	triggersOnload.eq(0).overlay().close();
		//	return false;
		//	});

		//凍結窗格
		gridview(0,false);
    });
<?
//}
?>

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 重选
     */
    //$j('#btReload').click(function () {
    //    promptWin();
    //});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_operating/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_operating/search" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();


</script>
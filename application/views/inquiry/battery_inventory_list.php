<?
	$web = $this->config->item('base_url');

	
	$get_full_url_random = get_full_url_random();
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
<link href="<?=$web.$newcss;?>newcss/dashboard.css" rel="stylesheet" type="text/css">
<!-- Bootstrap Js CDN -->
<script src="<?=$web;?>newjs/gauge.min.js"></script>
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="<?=$web.$newcss;?>newcss/bootstrap.min.css">
<script src="<?=$web;?>newjs/bootstrap.min.js"></script>
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<!-- Page Content Holder -->
<div id="dashboard" class="wrapper">
	<div id="content">
		<div class="container2">
			<div class="row">
				<div class="col-lg-3">
					<div class="card orange">
						<i class="fas fa-car-battery"></i>
						<p>
							电池总数量
							<strong><?=$battery_all[0]?></strong>
						</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card orange">
						<i class="fas fa-motorcycle"></i>
						<p>
							车机上电池数量
							<strong><?=$battery_all[1]?></strong>
						</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card blue">
						<i class="fas fa-building"></i>
						<p>
							机柜电池数量数量
							<strong><?=$battery_all[2]?></strong>
						</p>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card green">
						<i class="fas fa-location-arrow"></i>
						<p>
							其他电池数量
							<strong><?=$battery_all[3]?></strong>
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="container-default">
						<h2 class="col-lg-12">
							<i class="fas fa-location-arrow"></i>
							其他电池明細
						</h2>
						<table  class="other_batter_detail">
							<thead>
								<tr>
									<td>电池序号</td>
									<td>地圖</td>
									<td>最后一次回报时间</td>
								</tr>
							</thead>
							<tbody>
<?	
								if($battery_all[3] > 0){
									foreach($battery_all[4] as $arr){
										echo '<tr>
														<td>'.$arr['battery_id'].'</td>
														<td><a onclick="btn_map('.$arr['s_num'].');" class="map"></a></td>
														<td>'.$arr['last_report_datetime'].'</td>
													</tr>';
									}
								
								}


?>

							</tbody>
						</table> 
					</div>
				</div>   
			</div>
		</div>
	</div>
</div>
<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){

});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

function btn_map(sn){
	//亂數
	var random_url = create_random_url();

	mywin=window.open("","detail_map","left=100,top=100,width=1050,height=500"); 
	mywin.location.href = '<?=$web?>device/battery__'+random_url+'/detail_map/'+sn;

}

</script>

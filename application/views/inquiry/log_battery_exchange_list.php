<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"top_name"				=>	"{$this->lang->line('log_battery_exchange_so_num')}",
		"bss_id"						=>	"{$this->lang->line('log_battery_exchange_sb_num_name')}",
		"unit_id"				=>	"{$this->lang->line('log_battery_exchange_sv_num')}",
		"vehicle_code"		=>	"{$this->lang->line('log_battery_exchange_vehicle_code')}",
		"vehicle_user_id"	=>	"{$this->lang->line('log_battery_exchange_vehicle_user_id')}",
		"no_track_no"			=>	"{$this->lang->line('log_battery_exchange_no_track_no')}",
		"no_battery_pack_sn"	=>	"{$this->lang->line('log_battery_exchange_no_battery_pack_sn')}",
		"yes_track_no"				=>	"{$this->lang->line('log_battery_exchange_yes_track_no')}",
		"yes_battery_pack_sn"	=>	"{$this->lang->line('log_battery_exchange_yes_battery_pack_sn')}",
		"exchange_date"			=>	"{$this->lang->line('log_battery_exchange_date')}",
		"exchange_type"			=>	"{$this->lang->line('log_battery_exchange_exchange_type')}"
	);
	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$batteryswapsSelectListRow = $this->Model_show_list->getbatteryswapstationList() ;
	$vehicleSelectListRow = $this->Model_show_list->getvehicleList() ;
	$colArr = array (
		"so_num"					=>	"{$this->lang->line('select_operator_class')}",
		"sb_num"					=>	"{$this->lang->line('select_batteryswaps_class')}",
		"sv_num"					=>	"{$this->lang->line('select_vehicle_class')}"
	);
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		'so_num' => $operatorsSelectListRow,
		'sb_num' => $batteryswapsSelectListRow,
		'sv_num' => $vehicleSelectListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData, $fieldType);

	$exchange_type_name = array('离线交换','连线交换');
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	//echo 'A:'.$this->session->userdata('start_date');
?>
<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('log_battery_exchange_inquiry')?></p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="机车编号" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_exchange">
					<tr class="GridViewScrollHeader">
<?
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				$n=1;
				$no_list = '';
				foreach ($colInfo["colName"] as $enName => $chName) {
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					 echo "					<th align=\"center\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}

?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
						 if($enName == "bss_id"){
								echo "					<td align=\"left\">".$patternfilefieldInfoArr['location'].'（'.$patternfilefieldInfoArr['bss_id']."）</td>\n" ;
							}else{
								echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
							}
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
							'.$no_list.'
						</tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">

	$j(document).ready(function() {
		//凍結窗格
		gridview(2, true);
	});

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());

        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_battery_exchange/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_operating/search" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();


</script>
<?
	$web = $this->config->item('base_url');
	$show_num = 3;
	if($this->session->userdata('make_up') == 'Y'){
		$show_num = 4;
		$colArr = array (
			"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
			"unit_id"				=>	"{$this->lang->line('log_battery_leave_return_tv_num')}",
			"member_name"			=>	"{$this->lang->line('log_battery_leave_return_tm_num')}",
			"edits"					=>	"补登",
			"ldo_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_do_num')}",
			"ldso_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_dso_num')}",
			"leave_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_leave_sb_num_name')}",
			"battery_user_id"		=>	"{$this->lang->line('log_battery_leave_return_battery_user_id')}",
			"leave_request_date"	=>	"{$this->lang->line('log_battery_leave_return_leave_request_date')}",
			"leave_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_leave_coordinate')}",
			"leave_date"			=>	"{$this->lang->line('log_battery_leave_return_leave_date')}",
			"leave_track_no"		=>	"{$this->lang->line('log_battery_leave_return_leave_track_no')}",
			"leave_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_leave_battery_id')}",
			"leave_status"			=>	"{$this->lang->line('log_battery_leave_return_leave_status')}",
			"rdo_name"				=>	"{$this->lang->line('log_battery_leave_return_return_do_num')}",
			"rdso_name"				=>	"{$this->lang->line('log_battery_leave_return_return_dso_num')}",
			"return_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_return_sb_num_name')}",
			"return_request_date"	=>	"{$this->lang->line('log_battery_leave_return_return_request_date')}",
			"return_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_return_coordinate')}",
			"return_date"			=>	"{$this->lang->line('log_battery_leave_return_return_date')}",
			"return_track_no"		=>	"{$this->lang->line('log_battery_leave_return_return_track_no')}",
			"return_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_return_battery_id')}",
			"return_status"			=>	"{$this->lang->line('log_battery_leave_return_return_status')}",
			"usage_time"			=>	"{$this->lang->line('log_battery_leave_return_usage_time')}",
			"charge_amount"			=>	"{$this->lang->line('log_battery_leave_return_charge_amount')}",
			"trail"					=>  "{$this->lang->line('log_battery_trail')}"
		);
	}else{
		$colArr = array (
			"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
			"unit_id"				=>	"{$this->lang->line('log_battery_leave_return_tv_num')}",
			"member_name"			=>	"{$this->lang->line('log_battery_leave_return_tm_num')}",
			"ldo_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_do_num')}",
			"ldso_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_dso_num')}",
			"leave_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_leave_sb_num_name')}",
			"battery_user_id"		=>	"{$this->lang->line('log_battery_leave_return_battery_user_id')}",
			"leave_request_date"	=>	"{$this->lang->line('log_battery_leave_return_leave_request_date')}",
			"leave_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_leave_coordinate')}",
			"leave_date"			=>	"{$this->lang->line('log_battery_leave_return_leave_date')}",
			"leave_track_no"		=>	"{$this->lang->line('log_battery_leave_return_leave_track_no')}",
			"leave_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_leave_battery_id')}",
			"leave_status"			=>	"{$this->lang->line('log_battery_leave_return_leave_status')}",
			"rdo_name"				=>	"{$this->lang->line('log_battery_leave_return_return_do_num')}",
			"rdso_name"				=>	"{$this->lang->line('log_battery_leave_return_return_dso_num')}",
			"return_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_return_sb_num_name')}",
			"return_request_date"	=>	"{$this->lang->line('log_battery_leave_return_return_request_date')}",
			"return_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_return_coordinate')}",
			"return_date"			=>	"{$this->lang->line('log_battery_leave_return_return_date')}",
			"return_track_no"		=>	"{$this->lang->line('log_battery_leave_return_return_track_no')}",
			"return_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_return_battery_id')}",
			"return_status"			=>	"{$this->lang->line('log_battery_leave_return_return_status')}",
			"usage_time"			=>	"{$this->lang->line('log_battery_leave_return_usage_time')}",
			"charge_amount"			=>	"{$this->lang->line('log_battery_leave_return_charge_amount')}",
			"trail"					=>  "{$this->lang->line('log_battery_trail')}"
		);
	
	}
	
	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";

	$searchArr = array (
		"system_log_date"	=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
		//"tv.unit_id"		=>  "{$this->lang->line('log_battery_leave_return_tv_num')}",
		"battery_user_id"	=>  "{$this->lang->line('log_battery_leave_return_battery_user_id')}",
		"l_tbss.bss_id"	=>  "{$this->lang->line('log_battery_leave_return_leave_sb_num_name')}",
		"leave_battery_id"	=>  "{$this->lang->line('log_battery_leave_return_leave_battery_id')}",
		"r_tbss.bss_id"	=>  "{$this->lang->line('log_battery_leave_return_return_sb_num_name')}",
		"return_battery_id"	=>  "{$this->lang->line('log_battery_leave_return_return_battery_id')}",

	);

	//$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	//$dealerSelectListRow = $this->Model_show_list->getdealerList() ;
	$batteryswapsSelectListRow = $this->Model_show_list->getbatteryswapstationList() ;

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"system_log_date"	=>  "date",
		"l_tbss.bss_id"		=>  $batteryswapsSelectListRow,
		"r_tbss.bss_id"		=>  $batteryswapsSelectListRow
	);
	$threeInout = array(
		"l_tbss.bss_id"		=>  'Y',
		"r_tbss.bss_id"		=>  'Y'
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType,array(),'',array(),$threeInout);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$exchange_type_name = array('离线交换','连线交换');
	$status_name = array('充电中','饱电');
	
	//echo 'A:'.$this->session->userdata('start_date');
?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>
<script src="<?=$web?>js/jquery.tools.min.js"></script>
<script type="text/javascript">
//將jquery.tools.min.js 的jquery 改為$auto, 避免與原始版本的jquery衝突
var $auto = jQuery.noConflict();
</script>
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<!-- 下拉搜尋 多筆勾選 -->
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>
<!-- 可輸入可下拉 -->
<link rel="stylesheet" href="<?=$web?>js/easyui/easyui.css"/>
<script type="text/javascript" src="<?=$web?>js/easyui/jquery.easyui.min.js"></script> 
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('log_battery_leave_return_inquiry')?></p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="会员名称" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<?=$user_access_control?>
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_leave_return">
				<tr class="GridViewScrollHeader">
<?
				$no_list = '';

				$show_err = '';
				$span_arr = array();
				//让标题栏位有排序功能
				foreach ($colInfo["colName"] as $enName => $chName) {
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center style='display: inline-block;'><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}
				
?>
					</tr>
<?
				if(isset($InfoRow)){
					$s = 1;

					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						$sn = $patternfilefieldInfoArr['s_num'];
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							$align="center";
							if($enName=='leave_bss_id' || $enName=='return_bss_id'){
								$align="left";
							}
							if($enName=='trail')
							{
								if($patternfilefieldInfoArr['gps_path_track']!="")
									echo "					<td class=\"track\" ><a class=\"icon_track\" onclick=\"btn_map({$patternfilefieldInfoArr['s_num']});\"></a></td>\n";
								else
									echo "					<td align=\"{$align}\" class=\"\" ></td>\n";
							}
							else if($enName=='leave_status')
							{
								$data = "";
								if($patternfilefieldInfoArr[$enName]!="")
								{
									switch($patternfilefieldInfoArr[$enName])
									{
										case 0:
											$data = "成功";
											break;
										case 1:
											$data = "机柜还电门未打开";
											break;
										case 2:
											$data = "用户未放入电池";
											break;
										case 3:
											$data = "放入非借出电池";
											break;
										case 4:
											$data = "取电门未打开";
											break;
										case 5:
											$data = "用户未取出电池";
											break;
										case "F":
											$data = "使用微信绑定";
											break;
										case "W":
											$data = "补登";
											break;
									}

									if($patternfilefieldInfoArr[$enName] == 'W'){
										$data = "补登";
									}else if($patternfilefieldInfoArr[$enName] == 'F'){
										$data = "使用微信绑定";
									}else if($patternfilefieldInfoArr[$enName] == 'A'){
										$data = "APP借电";
									}

								}
								echo "					<td align=\"{$align}\" class=\"\" >" . $data . "</td>\n";
							}else if($enName=='return_status')
							{
								$data = "";
								if($patternfilefieldInfoArr[$enName]!="")
								{
									switch($patternfilefieldInfoArr[$enName])
									{
										case 0:
											$data = "成功";
											break;
										case 1:
											$data = "机柜还电门未打开";
											break;
										case 2:
											$data = "用户未放入电池";
											break;
										case 3:
											$data = "放入非借出电池";
											break;
										case 4:
											$data = "取电门未打开";
											break;
										case 5:
											$data = "用户未取出电池";
											break;
										case "F":
											$data = "使用微信绑定";
											break;
										case 'W':
											$data = "补登";
											break;
									}

									if($patternfilefieldInfoArr[$enName] == 'W'){
										$data = "补登";
									}else if($patternfilefieldInfoArr[$enName] == 'F'){
										$data = "使用微信绑定";
									}else if($patternfilefieldInfoArr[$enName] == 'A'){
										$data = "APP归还";
									}
								}
								echo "					<td align=\"{$align}\" class=\"\" >" . $data . "</td>\n";
							}else if($enName=='edits' ){
								
								if($patternfilefieldInfoArr['return_date'] == '' || $patternfilefieldInfoArr['return_date'] == '0000-00-00 00:00:00'){
									echo '	<td class="" ><a data-fancybox data-src="#editfancy" href="javascript:;" onclick="edit_leave_return('.$sn.');"><i class="fas fa-edit"></i></a></td>'	;
								}else{
									echo '	<td align="{$align}" class="" ></td>'	;
								}
							}
							else
								echo "					<td align=\"{$align}\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}

				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
                            '.$no_list.'
                        </tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

?>
			</table>

			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<div style="display: none;" id="editfancy">
	<div id="edit_label" class="edit_label">
		
	</div>
</div>


<script type="text/javascript">
$j(document).ready(function() {
	$j('a.close, #modal').live('click', function() {
	// close the overlay
	triggersOnload.eq(0).overlay().close();
	return false;
	});

	//凍結窗格(3欄)
	gridview(<?=$show_num;?>,true);
});

function edit_leave_return(sn){
	$j.ajax({
		type:'post',
		async: false,
		url: '<?= $web ?>inquiry/log_battery_leave_return/select_edit_label',
		data: { fn: '<?=$fn?>',
				sn: sn,
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j("#editfancy").show();
			$j('#edit_label').html(response);
		}
	}) ;
}


var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_battery_leave_return/search" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?= $web ?>inquiry/log_battery_leave_return/search" ) ;
        $j("#searchForm").submit() ;
    });

	/**
	 * 新增按钮
	 */
	$j("#btAddition").click( function () {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr("action", "<?=$add_url?>");
		$j("#listForm").submit();
	});


	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();

function btn_map(sn){
	//亂數
	var random_url = create_random_url();

	mywin=window.open("","路线轨迹","left=100,top=100,width=1050,height=500"); 
	mywin.location.href = '<?=$web?>member/member__'+random_url+'/detail_map_trail/'+sn;

}
</script>
<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"to_num"			=> "",
		"top08"				=> "",
		"card_num"			=> "",
		"lease_price"		=> "",
	);

	$operator_orderInfo = isset($operator_orderInfo) ? $operator_orderInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $s_num);

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

	if($bView){
		$show_sub_title = "查询";
	}else{
		$show_sub_title = ($s_num!='')?'加购':'新增';
	}

	$disabled = "";
	if($s_num != ''){
		$disabled  = "disabled";
		$operator_orderInfo['card_num'] = count($CardInfo);
	}
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
		
			<p class="btitle">
				<?=$this->lang->line('operator_order_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title">基本资料</p>
		
			<form id="formID" action="<?=$action?>" method="post">
			<div class="section">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$s_num?>" />
				<div class="leftbox edit">
					<ul>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('to_num')?></label>
							<select name="to_num" id="to_num" class="validate[required]" <?=$disabled;?>>
								<option value="">选择<?=$this->lang->line('to_num')?></option>
<?
								foreach($operator_list as $d_arr){
									echo '<option value="'.$d_arr['s_num'].'" '.(($s_num==$d_arr['s_num'])?'selected':'').' top09 = "'.$d_arr['top09'].'">'.$d_arr['top01'].'</option>';
								}		
?>
							</select>
						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('lease_type')?></label>
							<input type="radio" class="radio first validate[required]" name="lease_type" id="lease_type" value="1" checked onclick="change_lease(this.value)" <?=$disabled;?>><?=$this->lang->line('lease_type_1')?>
							
							<input type="radio" class="radio validate[required]" name="lease_type" id="lease_type" value="2" onclick="change_lease(this.value)" <?=$disabled;?>><?=$this->lang->line('lease_type_2')?>
							
						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('card_num')?></label>
							<input type="text" class="txt validate[required]" name="card_num" id="card_num" value="<?=$operator_orderInfo["card_num"]?>" onblur="aver_card('card_num', 'top08');" <?=$disabled;?>>	
							<INPUT TYPE="hidden" id="temp_card_num" value="<?=$operator_orderInfo["card_num"]?>">
							<INPUT TYPE="hidden" name="old_card_num" value="<?=$operator_orderInfo["card_num"]?>">
						</li>
						<li id="avg_show">
							<label for="" class="required"><span></span>自动平均分配</label>
							<INPUT TYPE="radio" NAME="avg" id="avg_1" value="1" onclick="change_avg('1')">是
							<INPUT TYPE="radio" NAME="avg" id="avg_2" value="2" onclick="change_avg('2')">否
							<INPUT TYPE="hidden" NAME="avg_type" id="avg_type" value="">
						</li>
						<li id="top08_show">
							<label for="" class="required"><span></span><?=$this->lang->line('top08')?></label>
							<input type="text" class="txt validate[required]" name="top08" id="top08" value="<?=$operator_orderInfo["top08"]?>" onblur="aver_card('top08', 'card_num');">		
							<INPUT TYPE="hidden" id="temp_top08" value="">
						</li>
						<li id="lease_price_show" style="display:none;">
							<label for="" class="required"><span></span><?=$this->lang->line('lease_price')?></label>
							<input type="text" class="txt validate[required]" name="lease_price" id="lease_price" value="<?=$operator_orderInfo["lease_price"]?>">		
						</li>
					</ul>
				</div>
				<div class="rightbox" id="div_lease_type_1">
					<ul class="order-amount">
						<li>预购总数量<span id="total_num">0</span></li>
						<li>剩馀非分配数量<span id="last_num">0</span><INPUT TYPE="hidden" id="last_top08"></li>
						<li><a style="cursor: pointer;" class="order-add" onclick="add_tr();">+ 新增卡片</a></li>
					</ul>
					<table class="order-card" id="card_table">
						<tr>
							<th>序号</th>
							<th>卡片名称</th>
							<th>数量</th>
<?
							if($s_num != ''){
								echo '<th>目前剩餘数量</th>';
							}
?>
							
						</tr>
<?
						$n=1;
						if(isset($CardInfo)){
							foreach($CardInfo as $key=>$arr){
								echo '<tr class="tr_card">';
								echo '<td>'.$n.'</td>';
								echo '<td><input type="text" id="name_'.$n.'" name="name_'.$n.'" value="'.$arr['name'].'"></td>';
								echo '<td><a onclick="change_qty(\'cut\', '.$n.');">－</a><input type="text" class="txt" name="card_num_'.$n.'" id="card_num_'.$n.'" style="width:50px;" onblur="change_qty(\'qty\', '.$n.');"><INPUT TYPE="hidden" id="tmp_num_'.$n.'"><a onclick="change_qty(\'add\', '.$n.');">＋</a></td>';
								if($s_num != ''){
									echo '<td>'.$arr['lave_num'].'</td>';
								}
								echo '<INPUT TYPE="hidden" NAME="tm_num_'.$n.'" id="tm_num_'.$n.'" value="'.$arr['s_num'].'">';
								echo '<INPUT TYPE="hidden" NAME="user_id_'.$n.'" id="user_id_'.$n.'" value="'.$arr['user_id'].'">';
								echo '</tr>';
								$n++;
							}
						}
?>
					</table>
				</div>
			</div>
			<INPUT TYPE="hidden" NAME="c_num" id="c_num" value="<?=$c_num;?>">
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		var last_top08 = $j("#last_top08").val();
		last_top08 = Number(last_top08);
		if(last_top08 > 0){
			alert("剩馀非分配数量未分配完全，请确认！");
		}else{
			$j("#to_num").prop("disabled",false);
			$j("#card_num").prop("disabled",false);
			//把lease_type写入隐藏栏位lease_type
			$j("#formID").submit();
		}
	}

	function change_lease(leave_type){
		if(leave_type == '1'){
			$j("#lease_price_show").css('display','none');
			$j("#lease_price").attr('css','');
			$j("#top08_show").css('display','block');
			$j("#top08_show").attr('css','txt validate[required]');
			$j("#avg_show").css('display','block');
			$j("#div_lease_type_1").css('display','block');
			//重新再定義張數
			aver_card('card_num', 'top08');
		}else{
			$j("#lease_price_show").css('display','block');
			$j("#lease_price").attr('css','txt validate[required]');
			$j("#top08_show").css('display','none');
			$j("#top08_show").attr('css','');
			$j("#div_lease_type_1").css('display','none');
			$j("#avg_show").css('display','none');

		}
	}

	function aver_card(now_field, two_field){
		var to_num = $j("#to_num").val();
		var top09 = $j("#to_num option:selected").attr('top09');
		top09 = (top09 != undefined)?top09:'';
		var name_title = 'MOD' + top09;

		var card_num = $j("#card_num").val();
		var temp_card_num = $j("#temp_card_num").val();
		var top08 = $j("#top08").val();
		var temp_top08 = $j("#temp_top08").val();

		var flag = 'N';

		if(now_field == 'card_num'){
			//改變卡片數量
			if(card_num != temp_card_num){
				$j('.tr_card').remove();
				for(i=1; i<=card_num; i++){
					add_tr_list(i, name_title, card_num.length);
				}
				$("#temp_card_num").val(card_num);
				if(top08 > 0){
					flag = 'Y';
				}
			}
		}
		
		if(now_field == 'top08' && !$j("#avg_1").prop("checked")){
			if(top08 != temp_top08){
				$j("#total_num").html(top08);
				$j("#last_num").html(top08);
				$j("#last_top08").val(top08);

				//重新分配次數 为0
				for(i=0; i<card_num; i++){
					$j("#card_num_"+(i+1)).val(0);
					$j("#tmp_num_"+(i+1)).val(0);
				}
			}
		}else{
			if(now_field == 'top08' || flag == 'Y'){
				if(top08 != temp_top08){

					//改變次數
					parseInt(card_num);
					parseInt(top08);
					//平均分配次數
					var aver_num = parseInt(top08/card_num);
					var remainder_num = top08%card_num;

					var aver_arr = [];

					for(i=0; i<card_num; i++){
						aver_arr[i] = aver_num;
					}

					if(remainder_num > 0){
						$j.each(aver_arr, function( key, value ) {
							if(remainder_num>0){
								aver_arr[key]++;
								remainder_num--;
							}else{
								return;
							}
						});
					}

					//重新分配次數
					for(i=0; i<card_num; i++){
						$j("#card_num_"+(i+1)).val(aver_arr[i]);
						$j("#tmp_num_"+(i+1)).val(aver_arr[i]);
					}

					$j("#total_num").html(top08);
					$j("#last_top08").val(0);
					$j("#last_num").html(0);
				}

			}
		}

		$j("#temp_top08").val(top08);
	}
	
	function add_tr_list(i, name_title, now_length){
		var c_num =  $j("#c_num").val();
		var title_num = Number(c_num) +  Number(i);
		var s_num = '<?=$s_num?>';
		

		var tr = '<tr class="tr_card">';
		tr += '<td>'+i+'</td>';
		tr += '<td><input type="text" id="name_'+i+'" name="name_'+i+'" value="'+((name_title + padding1(title_num, now_length)))+'"></td>';
		tr += '<td><a onclick="change_qty(\'cut\', '+i+');">－</a><input type="text" class="txt" name="card_num_'+i+'" id="card_num_'+i+'" style="width:50px;" onblur="change_qty(\'qty\', '+i+');"><INPUT TYPE="hidden" id="tmp_num_'+i+'"><a onclick="change_qty(\'add\', '+i+');">＋</a></td>';
		if(s_num != ''){
			tr += '<td></td>';
		}
		tr += '<INPUT TYPE="hidden" NAME="tm_num_'+i+'" id="tm_num_'+i+'">';
		tr += '<INPUT TYPE="hidden" NAME="user_id_'+i+'" id="user_id_'+i+'">';
		tr += '</tr>';
		$j('#card_table').append(tr);
	}

	function add_tr(){
		var to_num = $j("#to_num").val();
		var top09 = $j("#to_num option:selected").attr('top09');
		top09 = (top09 != undefined)?top09:'';
		var name_title = 'MOD' + top09;

		var card_num = $j("#card_num").val();
		parseInt(card_num);
		card_num++;
		$j("#card_num").val(card_num);
		$j("#temp_card_num").val(card_num);
		add_tr_list(card_num, name_title, card_num.length);
	}

    //迭代方式实现
    function padding1(num, length) {
        for(var len = (num + "").length; len < length; len = num.length) {
            num = "0" + num;            
        }
        return num;
    }

	function change_qty(type, i){
		var now_card_num = $j("#card_num_"+i).val();
		var card_num = $j("#card_num").val();
		Number(card_num);
		var now_card_num = $j("#card_num_"+i).val();
		Number(now_card_num);
		var old_card_num = $j("#tmp_num_"+i).val();
		Number(old_card_num);
		var last_top08 = $j("#last_top08").val();
		last_top08 = Number(last_top08);
		var top08 = $j("#top08").val();
		parseInt(top08);
		var key_num = 1;
		Number(key_num);
		if(type == 'qty'){
			//手key數量
			if(now_card_num != old_card_num && now_card_num >= 0){
				if(old_card_num > now_card_num){
					//減
					key_num = old_card_num-now_card_num;
					type = 'cut_qty';
				}else{
					//加
					key_num = now_card_num-old_card_num;
					type = 'add';
				}
			}
		}
		if(type == 'add'){
			if(key_num > last_top08){
				alert('剩馀非分配數量不足！');
				$j("#card_num_"+i).val(old_card_num);
			}else{
				last_top08 -= key_num;
				old_card_num = Number(old_card_num)+key_num;
			}
		}else if(type == 'cut' || type == 'cut_qty'){
			if(((top08>0) && (now_card_num > 0)) || (key_num > 0 && type == 'cut_qty')){
				last_top08 += key_num;
				old_card_num -= key_num;
			}
		}

		$j("#card_num_"+i).val(old_card_num);
		$j("#tmp_num_"+i).val(old_card_num);
		$j("#last_top08").val(last_top08);
		$j("#last_num").html(last_top08);
	}

	function change_avg(avg_type){
		$j("#avg_type").val(avg_type);
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
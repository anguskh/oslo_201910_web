<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
		"unit_id"				=>	"{$this->lang->line('log_battery_leave_return_tv_num')}",
		"member_name"			=>	"{$this->lang->line('log_battery_leave_return_tm_num')}",
		"ldo_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_do_num')}",
		"ldso_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_dso_num')}",
		"leave_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_leave_sb_num_name')}",
		"battery_user_id"		=>	"{$this->lang->line('log_battery_leave_return_battery_user_id')}",
		"leave_request_date"	=>	"{$this->lang->line('log_battery_leave_return_leave_request_date')}",
		"leave_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_leave_coordinate')}",
		"leave_date"			=>	"{$this->lang->line('log_battery_leave_return_leave_date')}",
		"leave_track_no"		=>	"{$this->lang->line('log_battery_leave_return_leave_track_no')}",
		"leave_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_leave_battery_id')}",
		"rdo_name"				=>	"{$this->lang->line('log_battery_leave_return_return_do_num')}",
		"rdso_name"				=>	"{$this->lang->line('log_battery_leave_return_return_dso_num')}",
		"return_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_return_sb_num_name')}",
		"return_request_date"	=>	"{$this->lang->line('log_battery_leave_return_return_request_date')}",
		"return_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_return_coordinate')}",
		"return_date"			=>	"{$this->lang->line('log_battery_leave_return_return_date')}",
		"return_track_no"		=>	"{$this->lang->line('log_battery_leave_return_return_track_no')}",
		"return_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_return_battery_id')}",
		"usage_time"			=>	"{$this->lang->line('log_battery_leave_return_usage_time')}",
		"charge_amount"			=>	"{$this->lang->line('log_battery_leave_return_charge_amount')}",
		"trail"					=>  "{$this->lang->line('log_battery_trail')}"
	);
	
	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/detail_search";

	$searchArr = array (
		"ltop.top01"	=>  "(借){$this->lang->line('select_operator_class')}",
		"ltd.tde01"		=>  "(借){$this->lang->line('select_dealer_class')}",
		"rtop.top01"	=>  "(还){$this->lang->line('select_operator_class')}",
		"rtd.tde01"		=>  "(还){$this->lang->line('select_dealer_class')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData_detail');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"ltop.top01"	=>  $operatorsSelectListRow,
		"ltd.tde01"		=>  $dealerSelectListRow,
		"rtop.top01"	=>  $operatorsSelectListRow,
		"rtd.tde01"		=>  $dealerSelectListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt_detail');

	$exchange_type_name = array('离线交换','连线交换');
	$status_name = array('充电中','饱电');
	
	//echo 'A:'.$this->session->userdata('start_date');
?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<code>
			<div class="topbtn">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="姓名" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttonblack smore active"><span>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								<INPUT TYPE="hidden" NAME="member_sn" id="member_sn" value="<?=$member_sn;?>">
								<INPUT TYPE="hidden" NAME="d_type" id="d_type" value="<?=$d_type;?>">
							</form>
						</div>
					</div>
				</div>
			</div>
			<p class="btitle"><?=$this->lang->line('card_management_management')?>【<?=$member_name;?>】</p>
			<ul class="tabs">
				<li class="on"><a href="<?=$get_full_url_random;?>/detail_record/<?=$member_sn?>/0/<?=$main_startRow?>">租借历程记录</a></li>
			</ul>
			</code>
			<div class="tabbox">
				<div class="tab_container">
					<form id="listForm" name="listForm" action="">
						<!--预防CSRF攻击-->
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
						<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
						<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_leave_return">
						<tr class="GridViewScrollHeader">
	<?
						$no_list = '';
						$start_row = $this->session->userdata('PageStartRow');
						$show_err = '';
						$span_arr = array();
						//让标题栏位有排序功能
						foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/detail_record/".$member_sn."/".$start_row."/".$main_startRow."?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center style='display: inline-block;'><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
							echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
							$no_list .= '<td style="padding:0;border:0;"></td>'; 
						}
						
?>
					</tr>
<?
					if(isset($InfoRow)){
						$s = 1;
						foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
							echo "				<tr class=\"GridViewScrollItem\">\n" ;
							foreach ($colInfo["colName"] as $enName => $chName) {
								if($enName=='trail')
								{
									if($patternfilefieldInfoArr['gps_path_track']!="")
										echo "					<td class=\"track\" ><a class=\"icon_track\" onclick=\"btn_map({$patternfilefieldInfoArr['s_num']});\"></a></td>\n";
									else
										echo "					<td align=\"center\" class=\"\" ></td>\n";
								}
								else
								{
									echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";
								}
							}
							echo "				</tr>\n";

							$s++;
						}
						if(count($InfoRow) == 0){
							//無資料時要給空白
							echo '<tr class="GridViewScrollItem" style="height:0;">
									'.$no_list.'
								</tr>';
						}
					}else{
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
						echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
					}
?>
					</table>

				</form>

				</div>
			</div>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>

			<a href="<?=$get_full_url_random.'/index/'.$main_startRow?>" class="backbtn">回上一页</a>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">
	$j(document).ready(function() {
		$j('a.close, #modal').live('click', function() {
		// close the overlay
		triggersOnload.eq(0).overlay().close();
		return false;
		});

		//凍結窗格(3欄)
		gridview(3,true);

	});

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();

function btn_map(sn){
	//亂數
	var random_url = create_random_url();

	mywin=window.open("","路线轨迹","left=100,top=100,width=1050,height=500"); 
	mywin.location.href = '<?=$web?>member/member__'+random_url+'/detail_map_trail/'+sn;

}

</script>
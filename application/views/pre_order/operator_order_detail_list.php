<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"create_date"				=>	"{$this->lang->line('operator_order_create_date')}",
		"before_total_lave_num"		=>	"{$this->lang->line('operator_order_before_total_lave_num')}",
		"add_num"					=>	"{$this->lang->line('operator_order_add_num')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData);

	$tsop02_name = array('','直营','加盟','其它');
?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
	<!-- Search Starts Here -->
	<!-- 搜寻先放这 隐藏
	<div id="searchContainer">
		<a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
		<div style="clear:both"></div>
		<div id="searchBox">                
			<form id="searchForm" method="post" action="<?=$search_url?>">
				<div id="closeX" style="color:red;text-align:right;cursor: pointer;z-index:9999;top:0px;right:-20px;position:absolute;" ><img src="<?=$web?>img/delete.png" style="width:30px;height:30px;" /></div>
				<!--预防CSRF攻击-->
	<!-- 			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<fieldset2 id="body">
					<fieldset2>
						<?=$search_box?>
						<input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
					</fieldset2>
				</fieldset2>
			</form>
	<!-- 	</div -->
		<div class="box">
		
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('operator_order_management')?>【<?=$top_name;?>】</p>
		</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="operator_order">
					<tr class="GridViewScrollHeader">
<?
						$start_row = $this->session->userdata('PageStartRow');

						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/op_detail/".$to_num."/".$start_row."/".$main_startRow."?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							$width = "";
							if($enName == 'create_date'){
								$width = "250";
							}
							//栏位显示
                            echo "					<th align=\"center\" width=\"{$width}\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($InfoRow as $key => $operatorInfoArr) {
		echo "				<tr class=\"GridViewScrollItem\">\n" ;
		$s_num = $operatorInfoArr["s_num"] ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "before_total_lave_num" || $enName == "add_num") {
				echo "					<td align=\"right\">".number_format($operatorInfoArr[$enName])."</td>\n" ;
			}else if ($enName == "create_date") {
				echo "					<td align=\"center\" width=\"250\">".$operatorInfoArr[$enName]."</td>\n" ;
			}else{
				echo "					<td align=\"left\">".$operatorInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
			<a href="<?=$get_full_url_random.'/index/'.$main_startRow?>" class="backbtn">回上一页</a>

		</div>
    </div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});
</script>

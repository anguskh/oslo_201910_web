<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"top01"			=>	"{$this->lang->line('operator_order_to_num')}",
		"top08"			=>	"{$this->lang->line('operator_order_top08')}",
		"card_count"	=>	"{$this->lang->line('operator_card_count')}",
		"status"		=>	"{$this->lang->line('operator_order_status')}",
		"op_detail"		=>	"{$this->lang->line('operator_order_detail')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	$action = "{$get_full_url_random}/modification_db";	
	
	//新增卡片
	$action_card = "{$get_full_url_random}/additionCard";	

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData);

	$tsop02_name = array('','直营','加盟','其它');

	$login_side = $this->session->userdata('login_side');

?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
	<!-- Search Starts Here -->
	<!-- 搜寻先放这 隐藏
	<div id="searchContainer">
		<a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
		<div style="clear:both"></div>
		<div id="searchBox">                
			<form id="searchForm" method="post" action="<?=$search_url?>">
				<div id="closeX" style="color:red;text-align:right;cursor: pointer;z-index:9999;top:0px;right:-20px;position:absolute;" ><img src="<?=$web?>img/delete.png" style="width:30px;height:30px;" /></div>
				<!--预防CSRF攻击-->
	<!-- 			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<fieldset2 id="body">
					<fieldset2>
						<?=$search_box?>
						<input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
					</fieldset2>
				</fieldset2>
			</form>
	<!-- 	</div -->
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('operator_order_management')?></p>
				<span class="topbtn">
					<?=$user_access_control?>
					<a style="cursor:pointer" id="btAdditioncard" class="buttoninActive"><i class="fas fa-plus"></i></a>
				</span>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="operator_order">
					<tr class="GridViewScrollHeader">
<?
						//if($login_side > 0){
?>
							<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
<?
						//}
?>
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($suboperatorInfoRow as $key => $operatorInfoArr) {
		echo "				<tr class=\"GridViewScrollItem\">\n" ;
		
		$s_num = $operatorInfoArr["s_num"] ;
		$checkStr = "
<td align=\"center\">
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$s_num}\" >
	<INPUT TYPE=\"hidden\" id=\"top08_{$s_num}\" value=\"{$operatorInfoArr["card_count"]}\">
</td>
		" ;

		//if($login_side > 0){
		//	echo $checkStr ;
		//}
		echo $checkStr ;

		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "op_detail") {
				$main_startrow = $this->session->userdata('PageStartRow');
				echo '<td align="left" id="noclick"><a href="'.$get_full_url_random.'/op_detail/'.$s_num.'/0/'.$main_startrow.'" class="record"></a></td>';
			} else if ($enName == "status") {
				switch($operatorInfoArr[$enName]) {
					case "Y":
						echo "					<td align=\"center\">{$this->lang->line('enable')}</td>\n" ;
						break;
					case "N":
						echo "					<td align=\"center\">{$this->lang->line('disable')}</td>\n" ;
						break;
					case "D":
						echo "					<td  class=\"no\" align=\"center\">{$this->lang->line('delete')}</td>\n" ;
						break;
					default:
						echo "					<td align=\"center\"></td>\n" ;
						break;
				}
			}else {
				echo "					<td align=\"left\">".$operatorInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
	<a data-fancybox data-src="#hidden-content-1" href="javascript:;" class="buttonblack simport" id="s_click" style="display:none;">搜尋</a>
	<a data-fancybox data-src="#hidden-content-2" href="javascript:;" class="buttonblack simport" id="card_click" style="display:none;">搜尋_2</a>
	<div style="display: none;" id="hidden-content-1" class="import_box">
		<form id="add_list" action="<?=$action;?>" method="post" target="_parent">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<ul>
				<li style="margin: 0 0 15px 0">
					<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>营运商</label>
					<select id="tmp_to_num"  style="width: 220px;  height:32px; background:#fff; border: 1px solid #ccc;" disabled>
<?
						foreach($operator_list as $d_arr){
							echo '<option value="'.$d_arr['s_num'].'">'.$d_arr['top01'].'</option>';
						}		
?>
					</select>
				</li>
				<li style="margin: 0 0 15px 0">
					<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>加购次数</label>
					<input type="text" class="txt validate[required]" name="top08" id="top08" value="">
				</li>
				<INPUT TYPE="hidden" NAME="s_num" id="s_num" value="">
			</ul>
			<a style="cursor:pointer" id="btEdit" class="send">加购</a>
		</form>
	</div>
	<div style="display: none;" id="hidden-content-2" class="import_box">
		<form id="add_card_list" action="<?=$action_card;?>" method="post" target="_parent">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<?
			$to_flag = $this->session->userdata('to_flag'); 
			$to_flag_num = "";
			$disabled = "";
			if($to_flag == '1'){
				$to_flag_num = $this->session->userdata('to_num');
				$disabled = "disabled";
			}


?>
				<div class="section">
					<div class="container-box">
						<ul>
							<li style="margin: 0 0 15px 0">
								<label for="" class="required" style="width: 90px;"><span></span>营运商</label>
								<select id="to_num" name="to_num" style="width: 220px;  height:32px; background:#fff; border: 1px solid #ccc;" <?=$disabled;?>>
			<?
									foreach($operator_list as $d_arr){
										$selected = "";
										if($to_flag_num != ''){
											if($d_arr['s_num'] == $to_flag_num){
												$selected = "selected";
											}
										}

										echo '<option value="'.$d_arr['s_num'].'" '.$selected.'>'.$d_arr['top01'].'</option>';
									}		
			?>
								</select>
							</li>
							<li style="margin: 0 0 15px 0">
								<label for="" class="required"><span></span><?=$this->lang->line('lease_type')?></label>
								<input type="radio" class="radio first validate[required]" name="lease_type" id="lease_type" value="1" checked onclick="change_lease(this.value)" style="display:inline-block;"><?=$this->lang->line('lease_type_1')?>
								<input type="radio" class="radio validate[required]" name="lease_type" id="lease_type" value="2" onclick="change_lease(this.value)" style="display:inline-block;"><?=$this->lang->line('lease_type_2')?>
							</li>
							<li style="margin: 0 0 15px 0">
								<label for="" class="required" style="width: 90px;"><span></span>卡片张数</label>
								<input type="text" class="txt validate[required]" name="card_num" id="card_num" value="">
							</li>
							<li style="margin: 0 0 15px 0; display:none;" id="lease_price_show" >
								<label for="" class="required" style="display: inline-block; width: 90px;"><span></span><?=$this->lang->line('lease_price')?></label>
								<input type="text" class="txt validate[required]" name="lease_price" id="lease_price">		
							</li>
						</ul>
					</div>
				</div>
				<div class="sectin buttonbar">
					<a style="cursor:pointer" id="btCardEdit" class="send">新增</a>
				</div>	
		</form>
	</div>
<script type="text/javascript">
function change_lease(leave_type){
	if(leave_type == '1'){
		$j("#lease_price_show").css('display','none');
	}else{
		$j("#lease_price_show").css('display','inline');
	}
}

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});


/**
 * 加购按钮
 */
$j("#btBuy").click( function () {
	var to_num = getCkbVal();
	//檢查預購次數 = 0 跳新增
	if($j("#top08_"+to_num).val()>0){
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr("action", "<?=$edit_url?>");
		$j("#listForm").submit();
	}else{
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr("action", "<?=$add_url?>");
		$j("#listForm").submit();
	}


	//$j("#tmp_to_num").val(to_num);
	//$j("#s_num").val(to_num);
	//$j("#s_click").click();
});

$j("#btEdit").click( function () {
	var top08 = $j("#top08").val();
	if(top08 == ''){
		alert('加购次数不可空白！');
	}else{
		$j("#add_list").submit();
	}
});

//批次新增卡片
$j("#btAdditioncard").click( function () {
	$j("#card_click").click();
});

$j("#btCardEdit").click( function () {
	$j("#to_num").attr('disabled', false);
	var card_num = $j("#card_num").val();
	if(card_num == ''){
		alert('卡片张数不可空白！');
	}else{
		$j("#add_card_list").submit();
	}
});


/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/operator/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/operator/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});
</script>

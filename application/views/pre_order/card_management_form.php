<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"to_num"			=> "",
		"name"				=> "",
		"user_id"			=> "",
		"pre_order_num"		=> "",
		"lave_num"			=> "",
		"nick_name"			=> "",
		"mobile"			=> "",
		"province"			=> "",
		"district"			=> "",
		"city"				=> "",
		"zip_code"			=> "",
		"address"			=> "",
		"lease_type"		=> "1",
		"lease_price"		=> ""
	);

	$card_managementInfo = isset($card_managementInfo) ? $card_managementInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $card_managementInfo["s_num"]);

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

	if($bView){
		$show_sub_title = "查询";
	}else{
		$show_sub_title = ($card_managementInfo["s_num"]!='')?'编辑':'新增';
	}
	
	$disabled = "";
	if($card_managementInfo["s_num"] != ''){
		$disabled = "disabled";
	}
	$login_side = $this->session->userdata('login_side');
	
	$name_disabled = "";
	if($login_side == 0){
		$name_disabled  = "disabled";
	}

?>
<script type="text/javascript" src="<?=$web?>js/cityselectcn/distpicker.js"></script>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
		
			<p class="btitle">
				<?=$this->lang->line('card_management_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title">基本资料</p>
		
			<form id="formID" action="<?=$action?>" method="post">
			<div class="section">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" id="s_num" value="<?=$card_managementInfo['s_num']?>" />
				<div class="leftbox edit" style="width: 100%">
					<ul>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('to_num')?></label>
							<select name="to_num" id="to_num" class="validate[required]" <?=$disabled;?>>
								<option value="">选择<?=$this->lang->line('to_num')?></option>
<?
								foreach($operator_list as $d_arr){
									echo '<option value="'.$d_arr['s_num'].'" '.(($card_managementInfo["to_num"]==$d_arr['s_num'])?'selected':'').'>'.$d_arr['top01'].'</option>';
								}		
?>
							</select>
							※营运商无法选择时，请先到营运商预购次数管理建立预购次数
						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('lease_type')?></label>
							<input type="radio" class="radio first" name="postdata[lease_type]" id="lease_type" value="1" onclick="change_lease(this.value)" <?=(($card_managementInfo["lease_type"] == 1)?'checked':'')?> <?=$disabled;?>><?=$this->lang->line('lease_type_1')?>
							<input type="radio" class="radio" name="postdata[lease_type]" id="lease_type" value="2" onclick="change_lease(this.value)" <?=(($card_managementInfo["lease_type"] == 2)?'checked':'')?> <?=$disabled;?>><?=$this->lang->line('lease_type_2')?>
						</li>
						<li id="lease_price_show" style="display:none;">
							<label for="" class="required"><span></span><?=$this->lang->line('lease_price')?></label>
							<input type="text" class="txt validate[required]" name="postdata[lease_price]" id="lease_price" value="<?=$card_managementInfo["lease_price"]?>" <?=$name_disabled;?>>		
						</li>
						<li>
							<label for="" class="required"><span></span><?=$this->lang->line('name')?></label>
							<input type="text" class="txt validate[required]" name="postdata[name]" id="name" value="<?=$card_managementInfo["name"]?>" style="width:362px;" <?=$name_disabled;?>>		
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('nick_name')?></label>
							<input type="text" class="txt" name="postdata[nick_name]" id="nick_name" value="<?=$card_managementInfo["nick_name"]?>" style="width:362px;">		
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('mobile')?></label>
							<input type="text" class="txt" id="mobile" name="postdata[mobile]" value="<?=$card_managementInfo["mobile"]?>" style="width:362px;">
						</li>
						<li>
							<label for="" id="label_zipcode"><span></span><?=$this->lang->line('city')?></label>
							<div id="cnzipcode">
								<select class="form-control" NAME="postdata[province]" id="province"></select>
								<select class="form-control" NAME="postdata[city]" id="city"></select>
								<select class="form-control" NAME="postdata[district]" id="district"></select>
							</div>
						</li>
						<li>
							<label for="" id="label_address"><span></span><?=$this->lang->line('address')?></label>
							<input type="text" class="txt long" id="address" name="postdata[address]" value="<?=$card_managementInfo["address"]?>" style="width:362px;">
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('user_id')?></label>
							<input type="text" class="txt" name="user_id" id="user_id" value="<?=$card_managementInfo["user_id"]?>" style="width:362px;" disabled>	※系统自动生成	
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('pre_order_num')?></label>
							<input type="text" class="txt" name="pre_order_num" id="pre_order_num" value="<?=$card_managementInfo["pre_order_num"]?>" style="width:362px;" disabled>		
						</li>
						<li>
							<label for=""><span></span><?=$this->lang->line('lave_num')?></label>
							<input type="text" class="txt" name="lave_num" id="lave_num" value="<?=$card_managementInfo["lave_num"]?>" style="width:362px;" disabled>		
						</li>
					</ul>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		if($j("#s_num").val() != ''){
			$j("#lease_type").attr('name', '');
		}
		if(<?=$login_side?> == 0){
			$j("#name").attr('name', '');
			$j("#lease_price").attr('name', '');
		}
		$j("#formID").submit();
	}

	function change_lease(leave_type){
		if(leave_type == '1'){
			$j("#lease_price_show").css('display','none');
		}else{
			$j("#lease_price_show").css('display','inline');
		}
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

	$j(function(){
		var province = '<?=$card_managementInfo["province"]?>';
		var city = '<?=$card_managementInfo["city"]?>';
		var district = '<?=$card_managementInfo["district"]?>';
		$j("#cnzipcode").distpicker({
			province: province,
			city: city,
			district: district
		});
	});

</script>
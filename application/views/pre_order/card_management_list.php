<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"top01"			=>	"{$this->lang->line('card_management_to_num')}",
		"name"			=>	"{$this->lang->line('card_management_name')}",
		"user_id"		=>	"{$this->lang->line('card_management_qrocde')}",
		"lease_type"	=>	"{$this->lang->line('card_management_lease_type')}",
		"lease_price"	=>	"{$this->lang->line('card_management_lease_price')}",
		"pre_order_num"		=>	"{$this->lang->line('card_management_pre_order_num')}",
		"lave_num"		=>	"{$this->lang->line('card_management_lave_num')}",
		"op_detail"		=>	"{$this->lang->line('card_management_detail')}",
		"detail_record"		=>	"{$this->lang->line('card_management_detail_record')}"
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	$action = "{$get_full_url_random}/modification_db";	

	$export_url = "{$get_full_url_random}/export";
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$login_side = $this->session->userdata('login_side');
	if($login_side == 0){
		$searchArr = array (
			"user_id"		=>	"{$this->lang->line('card_management_qrocde')}"
		);
	}else{
		$searchArr = array (
			"top.top01"		=>	"{$this->lang->line('card_management_to_num')}",
			"user_id"		=>	"{$this->lang->line('card_management_qrocde')}"
		);
	}
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"top.top01"			=>  $operator_list
	);

	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$lease_type_name = array('','加购','月结');
?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
		<div class="box">
		<code>
			<div class="topbtn">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="卡片名称" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<a data-fancybox="" data-src="#hidden-content-1" href="javascript:;" class="buttonblack simport">汇出</a>
					<?=$user_access_control?>
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttonblack smore active"><span>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
			<p class="btitle"><?=$this->lang->line('card_management_management')?></p>
		</code>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="card_management">
					<tr class="GridViewScrollHeader">
						<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($suboperatorInfoRow as $key => $InfoArr) {
		if($InfoArr["lease_type"] == 2){
			$InfoArr["pre_order_num"] = '';
			$InfoArr["lave_num"] = '';
		}
		echo "				<tr class=\"GridViewScrollItem\">\n" ;
		
		$s_num = $InfoArr["s_num"] ;
		$checkStr = "
<td align=\"center\">
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$s_num}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "op_detail") {
				$main_startrow = $this->session->userdata('PageStartRow');
				if($InfoArr["lease_type"] == 1){
					echo '<td align="left" id="noclick"><a href="'.$get_full_url_random.'/card_detail/'.$s_num.'/0/'.$main_startrow.'" class="record"></a></td>';
				}else{
					echo '<td></td>';
				}
			} else if ($enName == "detail_record") {
				$main_startrow = $this->session->userdata('PageStartRow');
				echo '<td align="left" id="noclick"><a href="'.$get_full_url_random.'/detail_record/'.$s_num.'/0/'.$main_startrow.'" class="record"></a></td>';
			}  else if ($enName == "pre_order_num" || $enName == "lave_num" || $enName == "lease_price" ) {
				echo "					<td align=\"right\">".$InfoArr[$enName]."</td>\n" ;
			} else if ($enName == "status") {
				switch($InfoArr[$enName]) {
					case "Y":
						echo "					<td align=\"center\">{$this->lang->line('enable')}</td>\n" ;
						break;
					case "N":
						echo "					<td align=\"center\">{$this->lang->line('disable')}</td>\n" ;
						break;
					case "D":
						echo "					<td  class=\"no\" align=\"center\">{$this->lang->line('delete')}</td>\n" ;
						break;
					default:
						echo "					<td align=\"center\"></td>\n" ;
						break;
				}
			} else if ($enName == "lease_type") {
				echo "					<td align=\"center\">".$lease_type_name[$InfoArr[$enName]]."</td>\n" ;
			}else {
				echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
	<a data-fancybox data-src="#hidden-content-1" href="javascript:;" class="buttonblack simport" id="s_click" style="display:none;">搜尋</a>
	<div style="display: none;" id="hidden-content-1" class="import_box">
		<form id="exportForm" method="post" target="_parent">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<ul>
				<li style="margin: 0 0 15px 0">
					<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>匯出類型</label>
					<input type="radio" class="radio first validate[required]" name="export_type" id="export_type" value="1" checked>卡片　
					<input type="radio" class="radio first validate[required]" name="export_type" id="export_type" value="2">交易
				</li>
				<li style="margin: 0 0 15px 0">
					<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>营运商</label>
					<select id="to_num" name="to_num" style="width: 294px;  height:32px; background:#fff; border: 1px solid #ccc;">
<?
						foreach($operator_list as $d_arr){
							echo '<option value="'.$d_arr['s_num'].'">'.$d_arr['top01'].'</option>';
						}		
?>
					</select>
				</li>
				<div id="trans_option">
					<li style="margin: 0 0 15px 0">
						<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>匯出區間</label>
						<select id="date_type" name="date_type" style="width: 294px;  height:32px; background:#fff; border: 1px solid #ccc;" onchange="change_date(this.value);">
							<option value="1">自訂</option>
							<option value="2">週</option>
							<option value="3">月</option>
							<option value="4">季</option>
						</select>
					</li>
					<li style="margin: 0 0 15px 0">
						<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>日期</label>
						<span id="date_type_1">
							<input type="date" id="date_start" name="date_start">
							<input type="date" id="date_end" name="date_end">
						</span>
						<span id="date_type_2" style="display:none;">
							<input type="week" id="date_week" name="date_week">
						</span>
						<span id="date_type_3" style="display:none;">
							<input type="month" id="date_month" name="date_month">
						</span>
						<span id="date_type_4" style="display:none;">
							<select id="date_season" name="date_season" style="width: 294px;  height:32px; background:#fff; border: 1px solid #ccc;">
								<option value="1">第一季（1~3月）</option>
								<option value="2">第二季（4~6月）</option>
								<option value="3">第三季（7~9月）</option>
								<option value="4">第四季（10~12月）</option>
							</select>
						</span>
					</li>
					<li style="margin: 0 0 15px 0">
						<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>交易类型</label>
						<input type="radio" class="radio first validate[required]" name="trans_type" id="trans_type" value="1" checked>全部　
						<input type="radio" class="radio first validate[required]" name="trans_type" id="trans_type" value="2">预购　
						<input type="radio" class="radio first validate[required]" name="trans_type" id="trans_type" value="3">月结　
					</li>
					<li style="margin: 0 0 15px 0">
						<label for="" class="required" style="display: inline-block; width: 90px;"><span></span>结算</label>
						<INPUT TYPE="checkbox" name="cancel_flag" id="cancel_flag" value="1">
					</li>
				</div>
			</ul>
			<a style="cursor:pointer" id="btExport" class="send">匯出</a>
		</form>
	</div>

<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<script>
	function getweek(date_week){
       var getvalue = date_week;
       var yearweekArr = getvalue.split("-");
       var year = yearweekArr[0];
       var week = yearweekArr[1].substring(1);
       // alert(yearweekArr[0]);
       dateRange(year,week);
       // alert(getvalue);
   	}

	function getNowFormatDate(theDate) 
	{ 
		var day = theDate; 
		var Year = 0; 
		var Month = 0; 
		var Day = 0; 
		var CurrentDate = ""; 
		// 初始化时间 
		Year= day.getFullYear();// ie火狐下都可以 
		Month= day.getMonth()+1; 
		Day = day.getDate(); 
		CurrentDate += Year + "-"; 
		if (Month >= 10 ) 
		{ 
			CurrentDate += Month + "-"; 
		} 
		else 
		{ 
			CurrentDate += "0" + Month + "-"; 
		} 
		if (Day >= 10 ) 
		{ 
			CurrentDate += Day ; 
		} 
		else 
		{ 
			CurrentDate += "0" + Day ; 
		} 
		return CurrentDate; 
	} 

	function isInOneYear(_year,_week){ 
		if(_year == null || _year == '' || _week == null || _week == ''){ 
			return true; 
		} 
		var theYear = getXDate(_year,_week,4).getFullYear(); 
			if(theYear != _year){ 
			return false; 
		} 
		return true; 
	} 

	// 获取日期范围显示 
	function getDateRange(_year,_week){ 
		var beginDate; 
		var endDate; 
		if(_year == null || _year == '' || _week == null || _week == ''){ 
			return ""; 
		} 
		beginDate = getXDate(_year,_week-1,1); 
		endDate = getXDate(_year,(_week - 1 + 1),7); 
		$j('#date_start').val(getNowFormatDate(beginDate));
		$j('#date_end').val(getNowFormatDate(endDate));
		//return getNowFormatDate(beginDate) + " 至 "+ getNowFormatDate(endDate); 
	} 

	// 这个方法将取得某年(year)第几周(weeks)的星期几(weekDay)的日期 
	function getXDate(year,weeks,weekDay){ 
		// 用指定的年构造一个日期对象，并将日期设置成这个年的1月1日 
		// 因为计算机中的月份是从0开始的,所以有如下的构造方法 
		var date = new Date(year,"0","1"); 

		// 取得这个日期对象 date 的长整形时间 time 
		var time = date.getTime(); 

		// 将这个长整形时间加上第N周的时间偏移 
		// 因为第一周就是当前周,所以有:weeks-1,以此类推 
		// 7*24*3600000 是一星期的时间毫秒数,(JS中的日期精确到毫秒) 
		time+=(weeks-1)*7*24*3600000; 

		// 为日期对象 date 重新设置成时间 time 
		date.setTime(time); 
			return getNextDate(date,weekDay); 
		} 
		// 这个方法将取得 某日期(nowDate) 所在周的星期几(weekDay)的日期 
		function getNextDate(nowDate,weekDay){ 
		// 0是星期日,1是星期一,... 
		weekDay%=7; 
		var day = nowDate.getDay(); 
		var time = nowDate.getTime(); 
		var sub = weekDay-day; 
		if(sub <= 0){ 
			sub += 7; 
		} 
		time+=sub*24*3600000; 
		nowDate.setTime(time); 
		return nowDate; 
	}

	//日期处理 
	function dateRange(year,week){ 
		// var _year = $("#_year").val(); 
		// var _week = $("#_week").val(); 
		if(isInOneYear(year,week)){ 
			var showDate = getDateRange(year,week); 
			//alert(showDate);
		} else{ 
			//alert(year+"年无"+week+"周，请重新选择"); 
		} 
	}

</script>

<script type="text/javascript">
function change_date(date_type){
	$j("#date_type_1").css('display','none');
	$j("#date_type_2").css('display','none');
	$j("#date_type_3").css('display','none');
	$j("#date_type_4").css('display','none');
	$j("#date_type_"+date_type).css('display','inline');

}
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});


/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/operator/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/operator/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j("#trans_option").hide();

$j('input[name=export_type]').click(function(){
	if($j('input[name=export_type]:checked').val()==1)
		$j("#trans_option").hide();
	else
		$j("#trans_option").show();
});

//匯出卡片資料
$j("#btExport").click(function(){
	if($j("#date_type").val() == 2){
		getweek($j("#date_week").val());
	}

	$j("#exportForm").attr( "method", "POST" ) ;
	$j("#exportForm").attr( "action", "<?=$export_url?>" ) ;
	$j("#exportForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});
</script>

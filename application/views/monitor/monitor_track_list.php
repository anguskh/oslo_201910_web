<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"top01"						=>	"{$this->lang->line('monitor_to_name')}",
		"location"					=>	"{$this->lang->line('monitor_location')}",
		"bss_id"					=>  "{$this->lang->line('monitor_bss_id')}",	
		"track_no"					=>	"{$this->lang->line('monitor_track_no')}",
		"battery_id"				=>	"{$this->lang->line('monitor_battery_id')}",
		"battery_capacity"			=>	"{$this->lang->line('monitor_battery_capacity')}",
		"battery_temperature"		=>	"{$this->lang->line('monitor_battery_temperature')}",
		"battery_voltage"			=>	"{$this->lang->line('monitor_battery_voltage')}",
		"battery_amps"				=>	"{$this->lang->line('monitor_battery_amps')}",
		"charge_cycles"				=>  "{$this->lang->line('monitor_charge_cycles')}",
		"status"					=>	"{$this->lang->line('monitor_battery_status')}",
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();

?>
<!-- 列表区 -->
		<div id='listdiv'>
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="monitor">
				<tr class="GridViewScrollHeader">
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr class=\"GridViewScrollItem\">\n" ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr[$enName])
						{
							case '0':
								$data = "充电中";
								break;
							case '1':
								$data = "饱电";
								break;
							default:
								$data = $InfoArr[$enName];
								break;
						}
						echo "					<td align=\"left\">".$data."</td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		</div>

<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});


</script>

<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"province"					=>	"{$this->lang->line('monitor_province')}",
		"city"						=>	"{$this->lang->line('monitor_city')}",
		"district"					=>  "{$this->lang->line('monitor_district')}",
		"bss_id"					=>  "{$this->lang->line('monitor_bss_id')}",	
		"track_no"					=>	"{$this->lang->line('monitor_track_no')}",
		"battery_id"				=>	"{$this->lang->line('monitor_battery_id')}",
		"battery_temperature"		=>	"{$this->lang->line('monitor_battery_temperature')}",
		"battery_voltage"			=>	"{$this->lang->line('monitor_battery_voltage')}",
		"battery_amps"				=>	"{$this->lang->line('monitor_battery_amps')}",
		"battery_capacity"			=>	"{$this->lang->line('monitor_battery_capacity')}",
		"status"					=>	"{$this->lang->line('monitor_status')}",
		"status_desc"				=>  "{$this->lang->line('monitor_remark')}",
		"user_name"					=>	"{$this->lang->line('monitor_user_name')}",
		"user_id"					=>	"{$this->lang->line('monitor_user_id')}",
		"sb_num"					=>	"{$this->lang->line('monitor_icon')}",
	);
	$colInfo = array("colName" => $colArr);
	
	$colArr2 = array (
		"edit_status"				=>	"编辑",
		"log_date"					=>	"{$this->lang->line('monitor_log_date')}",
		"battery_id"				=>	"{$this->lang->line('monitor_battery_id')}",
		"status"					=>	"{$this->lang->line('monitor_status')}",
		"status_desc"				=>	"{$this->lang->line('monitor_remark')}",
	);
	$colInfo2 = array("colName" => $colArr2);

	$colArr3 = array (
		"edit_status"				=>	"编辑",
		"log_date"					=>	"{$this->lang->line('monitor_log_date')}",
		"battery_id"				=>	"{$this->lang->line('monitor_battery_id')}",
		"status"					=>	"{$this->lang->line('monitor_status')}",
		"status_desc"				=>	"{$this->lang->line('monitor_remark')}",
	);
	$colInfo3 = array("colName" => $colArr3);

	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"top.top01"		=>  "{$this->lang->line('select_operator_class')}",
		"tbss.bss_id"	=>  "{$this->lang->line('monitor_bss_id')}",
		"column_park"	=>  "{$this->lang->line('monitor_column_park')}",
		"column_charge"	=>  "{$this->lang->line('monitor_column_charge')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$batteryswapsSelectListRow = $this->Model_show_list->getbatteryswapstationList() ;
	
	$p_name = '';
	if(isset($province)){
		foreach($province as $key=>$arr){
			if($key != ''){
				if(isset($err_province[$key])){
					$p_name .= "{name: '".$key."', value: 2},";
				}else{
					$p_name .= "{name: '".$key."', value: 1},";
				}
			}
		
		}
	}

	$c_name = '';
	if(isset($city)){
		foreach($city as $key=>$arr){
			if($key != ''){
				if(isset($err_city[$key])){
					$c_name .= "{name: '".$key."', value: 2},";
				}else{
					$c_name .= "{name: '".$key."', value: 1},";
				}
			}
		
		}
	}
?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
<script type="text/javascript" src="<?=$web?>js/cityselectcn/distpicker.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->

<script src="<?=$web?>newjs/slider.js"></script>  
<script src="<?=$web?>newjs/echarts.min.js"></script>
<script src="<?=$web?>newjs/china.js"></script>    
<!-- script src="<?=$web?>newjs/map.js"></script -->
<script>
function make_map(){
    const map = {"云南":"#","内蒙古":"#","吉林":"#","四川":"#","宁夏":"#","安徽":"#","山东":"#","山西":"#","广东":"#","广西":"#","新疆":"#","江苏":"#","江西":"#","河北":"#","河南":"#","浙江":"#","海南":"#","湖北":"#","湖南":"#","甘肃":"#","直辖市":"#","福建":"#福建","西藏":"#","贵州":"#","辽宁":"#","陕西":"#","青海":"#","黑龙江":"#","天津":"#","北京":"#","上海":"#","重庆":"#","香港":"#","澳门":"#","北京":"#"};
    const achart = echarts.init(document.getElementById("china"));
    var option =  {
        "backgroundColor": "#f8f8f8", 
        "title": [
        {
            "textStyle": {
            "color": "#000",
            "fontSize": 18
            },
            "subtext": "",
            "text": "中国",
            "top": "auto",
            "subtextStyle": {
            "color": "#aaa",
            "fontSize": 12
            },
            "left": "auto"
        }
        ],
        "legend": [
        {
            "selectedMode": "multiple",
            "top": "top",
            "orient": "horizontal",
            "data": [
            ""
            ],
            "left": "center",
            "show": true
        }
        ],
        "series": [
            {
                "mapType": "china",
                "data": [ //0为未营运，1为营运中，2为错误

					<?
						echo $p_name;	
					?>
                ],
                "name": "",
                "symbol": "circle",
                "showLegendSymbol": false,
                "type": "map",
                "roam": false,
                "label": {
                    "normal": {
                        "show": false
                    }
                }
            }
        ],
        "visualMap": {
            type: "piecewise",
            pieces: [
                {value: 0, label: '未营运', color: "#eee"},
                {value: 1, label: '营运中', color: "#a1d69e"},
                {value: 2, label: '即时告警错误', color: "#f29394"}
            ],
            "textStyle": {
                'color': '#333'
            }
        }
    };
    achart.setOption(option);
    achart.on('click', function(params){
        const filename = map[params.name];
		//alert(params.name);
		//make_map();
		$j("#province").val('');
		$j("#city").val('');
		$j("#district").val('');
		$j("#li_rovince").html('<div id="p_rovince" class="map"></div>');
		$j("#li_city").html('<div id="c_city" class="map"></div>');
		$j("#c_city").html('');
		make_smap( params.name , 'p_rovince');
		$j("#province").val(params.name);
		change_area();
       // window.location.href = filename;
    });
};

function make_smap(cityname, dom_id){
	const map = {};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
		"backgroundColor": "#f8f8f8",
        "title": [
            {
	            "textStyle": {
					"color": "#000",
					"fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""				
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "series": [
            {
	            "mapType": cityname,
	            "data": [
					<?
						echo $c_name;	
					?>
	            ],
	            "name": "",
				"symbol": "circle",
				"showLegendSymbol": false,
	            "type": "map",
				"roam": false,
				"label": {
					"normal": {
					  "show": false
					}
				  }
            }
		],
		"visualMap": {
			type: "piecewise",
			pieces: [
				{value: 0, label: '未营运', color: "#eee"},
				{value: 1, label: '营运中', color: "#a1d69e"},
				{value: 2, label: '即时告警错误', color: "#f29394"}
			],
			"textStyle": {
				'color': '#333'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
		$j("#city").val('');
		$j("#district").val('');
        const filename = map[params.name];
		if($j("#province").val() != '北京' && $j("#province").val() != '天津' && $j("#province").val() != '上海' && $j("#province").val() != '重庆'){
			make_smap2(params.name, 'c_city');
		}
		$j("#city").val(params.name);
		change_area();
       // window.location.href = filename + ".html";
    });
}
function make_smap2(cityname, dom_id){
	const map = {};
	achart = echarts.init(document.getElementById(dom_id));
    var option =  {
		"backgroundColor": "#f8f8f8",
        "title": [
            {
	            "textStyle": {
					"color": "#000",
					"fontSize": 18
	            },
	            "subtext": "",
	            "text": cityname,
	            "top": "auto",
	            "subtextStyle": {
	                "color": "#aaa",
	                "fontSize": 12
	            },
	            "left": "auto"
            }
        ],
        "legend": [
            {
	            "selectedMode": "multiple",
	            "top": "top",
	            "orient": "horizontal",
	            "data": [
	                ""				
	            ],
	            "left": "center",
	            "show": true
            }
        ],
        "series": [
            {
	            "mapType": cityname,
	            "data": [
					<?
						echo $c_name;	
					?>
	            ],
	            "name": "",
				"symbol": "circle",
				"showLegendSymbol": false,
	            "type": "map",
				"roam": false,
				"label": {
					"normal": {
					  "show": false
					}
				  }
            }
		],
		"visualMap": {
			type: "piecewise",
			pieces: [
				{value: 0, label: '未营运', color: "#eee"},
				{value: 1, label: '营运中', color: "#a1d69e"},
				{value: 2, label: '即时告警错误', color: "#f29394"}
			],
			"textStyle": {
				'color': '#333'
			}
		}
    };
	achart.setOption(option);
    achart.on('click', function(params){
		$j("#district").val('');
        const filename = map[params.name];
		$j("#district").val(params.name);

		change_area();
        //window.location.href = filename + ".html";
    });
}
</script>

<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('monitor_management')?></p>
			<div class="topbtn column">
				<div class="normalsearch" style="width:auto;">
					<div id="cnzipcode">
						<INPUT TYPE="hidden" id="province" value="">
						<INPUT TYPE="hidden" id="city" value="">
						<INPUT TYPE="hidden" id="district" value="">
						<!-- select class="form-control" NAME="province" id="province"></select>
						<select class="form-control" NAME="city" id="city"></select>
						<select class="form-control" NAME="district" id="district"></select -->

						<input type="text" name="bss_id" id="bss_id" placeholder="換電站序号" value="" class="txt">
						<!-- <input type="button" id="btClear2" class="clear" value="清 空" /> -->
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
				</div>
			</div>
		</div>
		<p class="tablehead">租借站軌道監控</p>
<!-- 列表区 -->
		<div id="abgne-tw-tools">
			<div class="search">
				<p id="btn"><span id="search_btn" style="cursor: pointer;">切換地圖</span></p>
				<div class="search_box">
					<ul class="mapbox">
						<li>
							<div id="china" class="map"></div>
							<script>
								make_map();
							</script>
						</li>
						<li id="li_rovince">
							<div id="p_rovince" class="map"></div>
						</li>
						<li id="li_city">
							<div id="c_city" class="map"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		 <!--監控地圖 結束-->

		<div id='listdiv'>
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="monitor">
				<tr class="GridViewScrollHeader">
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr class=\"GridViewScrollItem\">\n" ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr[$enName])
						{
							case '1':
								$data = "未檢視";
								break;
							case '2':
								$data = "處理中";
								break;
							case '3':
								$data = "已完成";
								break;
						}
						echo "					<td align=\"left\">".$data."</td>\n" ;
					}else if($enName == "bss_id"){
						echo "					<td align=\"left\">".$InfoArr['location']."<br>(".$InfoArr[$enName].")</a></td>\n" ;
					}else if($enName == "sb_num"){
						echo "					<td align=\"left\"><a href='#' class='watch' onclick='getbssinfo(\"{$InfoArr[$enName]}\")'></a></td>\n" ;
					}else if($enName == "battery_temperature"){
						$battery_temperature = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_temperature = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_temperature."</td>\n" ;
					}else if($enName == "battery_voltage"){
						$battery_voltage = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_voltage = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_voltage."</td>\n" ;
					}else if($enName == "battery_amps"){
						$battery_amps = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_amps = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_amps."</td>\n" ;
					}else if($enName == "battery_capacity"){
						$battery_capacity = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_capacity = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_amps."</td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}

				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		</div>
		<br>
		<br>
		<p class="tablehead">電池<?=$this->session->userdata('battery_not_report_hour')?>小時未回報清單</p>
		<div id='listdiv2'>
		<form id="listForm2" name="listForm2" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable2" cellpadding="0" cellspacing="0" border="0" class="monitor2">
				<tr class="GridViewScrollHeader">
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo2["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($battery_InfoRow as $key => $InfoArr2) {
				$sn = $InfoArr2['s_num'];
				echo "				<tr class=\"GridViewScrollItem\">\n" ;
				foreach ($colInfo2["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr2[$enName])
						{
							case '1':
								$data = "未檢視";
								break;
							case '2':
								$data = "處理中";
								break;
							case '3':
								$data = "已完成";
								break;
						}
						echo "					<td align=\"left\">".$data."</td>\n" ;
					}else if($enName == 'edit_status'){
						echo '<td align="center"><a data-fancybox data-src="#editfancy" class="edit" href="javascript:;" onclick="edit_battery_warring('.$sn.');"></a></td>';
					}else if($enName == "sb_num"){
						echo "					<td align=\"left\"><a href='#' onclick='getbssinfo(\"{$InfoArr[$enName]}\")'>".$InfoArr[$enName]."</a></td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr2[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		</div>
		<br>
		<br>
		<p class="tablehead">電池<?=$this->session->userdata('battery_not_in_bss_day')?>天未回租借站清單</p>
		<div id='listdiv2'>
		<form id="listForm2" name="listForm2" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable3" cellpadding="0" cellspacing="0" border="0" class="monitor2">
				<tr class="GridViewScrollHeader">
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo2["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($battery_nobss_InfoRow as $key => $InfoArr2) {
				$sn = $InfoArr2['s_num'];
				echo "				<tr class=\"GridViewScrollItem\">\n" ;
				foreach ($colInfo2["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr2[$enName])
						{
							case '1':
								$data = "未檢視";
								break;
							case '2':
								$data = "處理中";
								break;
							case '3':
								$data = "已完成";
								break;
						}
						echo "					<td align=\"left\">".$data."</td>\n" ;
					}else if($enName == 'edit_status'){
						echo '<td align="center"><a data-fancybox data-src="#editfancy" class="edit" href="javascript:;" onclick="edit_battery_warring('.$sn.');"></a></td>';
					}else if($enName == "sb_num"){
						echo "					<td align=\"left\"><a href='#' onclick='getbssinfo(\"{$InfoArr[$enName]}\")'>".$InfoArr[$enName]."</a></td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr2[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		</div>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<div style="display: none;" id="editfancy">
	<div id="edit_label" class="edit_label">
		
	</div>
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 搜寻
 */
// $j('#btSearch, #btSearch2').click(function () {
// 	$j("#loadingIMG").show();

// 	//sumit前要更新 s_search_txt
// 	$j("#search_txt").val($j("#s_search_txt").val());

// 	$j("#searchForm").attr( "method", "POST" ) ;
// 	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
// 	$j("#searchForm").submit() ;
// });

$j("#btSearch2").click(function(){
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getqueryList',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',bss_id:$j("#bss_id").val()},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
});

function getbssinfo(sb_num)
{
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getbss',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',sb_num:sb_num},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
}

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
	//凍結窗格,listTable2
	gridview(0,false,'listTable2');
	//凍結窗格,listTable3
	gridview(0,false,'listTable3');
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

$j(function(){
	/*var province = '';
	var city = '';
	var district = '';
	$j("#cnzipcode").distpicker({
		province: province,
		city: city,
		district: district
	});*/
});

/*$j('#province').change(function(){
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getqueryList',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',province:$j("#province").val()},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
});

$j('#city').change(function(){
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getqueryList',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',province:$j("#province").val(),city:$j("#city").val()},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
});

$j('#district').change(function(){
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getqueryList',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',province:$j("#province").val(),city:$j("#city").val(),district:$j("#district").val()},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
});*/

function change_area(){
	$j.ajax({
		type:'post',
		url: '<?=$web?>monitor/monitor/getqueryList',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',province:$j("#province").val(),city:$j("#city").val(),district:$j("#district").val()},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#listdiv').html(response);
		}
	});
}

function edit_battery_warring(sn){
    $j.ajax({
        type:'post',
        url: '<?= $web ?>inquiry/log_battery_warring/select_edit_label',
		data: { fn: '<?=$fn?>',
				sn: sn,
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
        error: function(xhr) {
            strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
            alert(strMsg);
        },
        success: function (response) {
			$j("#editfancy").show();
            $j('#edit_label').html(response);
        }
    }) ;
}
</script>
<!-- provinces -->
<script src="<?=$web?>newjs/echarts-china-provinces-js/anhui.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/fujian.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/gansu.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/guangdong.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/guangxi.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/guizhou.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/hainan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/hebei.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/heilongjiang.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/henan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/hubei.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/hunan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/jiangsu.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/jiangxi.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/jilin.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/liaoning.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/neimenggu.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/ningxia.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/qinghai.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/shandong.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/shanghai.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/shanxi.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/shanxi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/sichuan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/taiwan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/xinjiang.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/xizang.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/yunnan.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/zhejiang.js"></script>
<script src="<?=$web?>newjs/echarts-china-provinces-js/chongqing.js"></script>
<!-- city -->
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_an1_qing4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_bang4_bu4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_bo2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_chi2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_chu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_fu4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_he2_fei2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_huai2_bei3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_huai2_nan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_huang2_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_liu4_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_ma3_an1_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_su4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_tong2_ling2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_wu2_hu2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/an1_hui1_xuan1_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/aomen.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/beijing.js"></script>

<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_fu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_long2_yan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_nan2_ping2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_ning2_de2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_pu3_tian2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_quan2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_san1_ming2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_sha4_men2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/fu2_jian4_zhang1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_bai2_yin2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_ding4_xi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_gan1_nan2_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_jia1_yu4_guan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_jin1_chang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_jiu3_quan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_lan2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_lin2_xia4_hui2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_long3_nan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_ping2_liang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_qing4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_tian1_shui3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_wu3_wei1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gan1_su4_zhang1_ye4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_chao2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_dong1_guan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_dong1_sha1_qun2_dao3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_fo2_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_guang3_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_he2_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_hui4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_jiang1_men2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_jie1_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_mao4_ming2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_mei2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_qing1_yuan3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_shan4_tou2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_shan4_wei3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_shao2_guan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_shen1_zhen4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_yang2_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_yun2_fu2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_zhan4_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_zhao4_qing4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_zhong1_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_dong1_zhu1_hai3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_bai3_se4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_bei3_hai3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_chong2_zuo3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_fang2_cheng2_gang3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_gui4_gang3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_gui4_lin2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_he2_chi2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_he4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_lai2_bin1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_liu3_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_nan2_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_qin1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_wu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/guang3_xi1_yu4_lin2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_an1_shun4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_bi4_jie2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_gui4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_liu4_pan2_shui3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_qian2_dong1_nan2_miao2_zu2_tong1_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_qian2_nan2_bu4_yi1_zu2_miao2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_qian2_xi1_nan2_bu4_yi1_zu2_miao2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_tong2_ren2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/gui4_zhou1_zun1_yi4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_bai2_sha1_li2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_bao3_ting2_li2_zu2_miao2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_chang1_jiang1_li2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_cheng2_mai4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_dan1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_ding4_an1_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_dong1_fang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_hai3_kou3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_le4_dong1_li2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_lin2_gao1_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_ling2_shui3_li2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_qiong2_hai3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_qiong2_zhong1_li2_zu2_miao2_zu2_zi4_zhi4_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_san1_sha1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_san1_ya4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_tun2_chang1_xian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_wan4_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_wen2_chang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hai3_nan2_wu3_zhi3_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_bao3_ding4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_cheng2_de2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_han2_dan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_heng2_shui3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_lang2_fang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_qin2_huang2_dao3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_shi2_jia1_zhuang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_tang2_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_xing2_tai2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_bei3_zhang1_jia1_kou3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_an1_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_cang1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_he4_bi4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_ji4_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_jiao1_zuo4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_kai1_feng1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_luo4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_nan2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_ping2_ding3_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_pu2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_san1_men2_xia2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_shang1_qiu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_ta4_he2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_xin1_xiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_xin4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_xu3_chang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_zheng4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_zhou1_kou3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/he2_nan2_zhu4_ma3_dian4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_da4_qing4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_da4_xing1_an1_ling2_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_ha1_er3_bin1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_he4_gang3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_hei1_he2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_ji1_xi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_jia1_mu4_si1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_mu3_dan1_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_qi1_tai2_he2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_qi2_qi2_ha1_er3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_shuang1_ya1_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_sui1_hua4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hei1_long2_jiang1_yi1_chun1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_e4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_en1_shi1_tu3_jia1_zu2_miao2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_huang2_gang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_huang2_shi2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_jing1_men2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_jing1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_qian2_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_shen2_nong2_jia4_lin2_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_shi2_yan4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_sui2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_tian1_men2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_wu3_han4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_xian1_tao2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_xian2_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_xiang1_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_xiao4_gan3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_bei3_yi2_chang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_chang2_de2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_chang2_sha1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_chen1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_heng2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_huai2_hua4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_lou2_di3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_shao4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_xiang1_tan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_xiang1_xi1_tu3_jia1_zu2_miao2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_yi4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_yong3_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_yue4_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_zhang1_jia1_jie4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/hu2_nan2_zhu1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_bai2_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_bai2_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_chang2_chun1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_ji2_lin2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_liao2_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_si4_ping2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_song1_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_tong1_hua4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ji2_lin2_yan2_bian1_zhao1_xian1_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_chang2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_huai2_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_lian2_yun2_gang3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_nan2_jing1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_nan2_tong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_su1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_su4_qian1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_tai4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_wu2_xi2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_xu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_yan2_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_yang2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_su1_zhen4_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_fu3_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_gan4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_ji2_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_jing3_de2_zhen4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_jiu3_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_nan2_chang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_ping2_xiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_shang4_rao2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_xin1_yu2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_yi2_chun1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/jiang1_xi1_ying1_tan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_an1_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_ben3_xi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_da4_lian2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_dan1_dong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_fu3_shun4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_fu4_xin1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_hu2_lu2_dao3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_jin3_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_liao2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_pan2_jin3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_shen3_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_tie3_ling2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_ying2_kou3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/liao2_ning2_zhao1_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_a1_la1_shan4_meng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_ba1_yan4_nao4_er3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_bao1_tou2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_chi4_feng1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_e4_er3_duo1_si1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_hu1_he2_hao4_te4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_hu1_lun2_bei4_er3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_tong1_liao2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_wu1_hai3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_wu1_lan2_cha2_bu4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_xi2_lin2_guo1_le4_meng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/nei4_meng2_gu3_xing1_an1_meng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ning2_xia4_gu4_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ning2_xia4_shi2_zui3_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ning2_xia4_wu2_zhong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ning2_xia4_yin2_chuan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/ning2_xia4_zhong1_wei4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_guo3_luo4_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_hai3_bei3_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_hai3_dong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_hai3_nan2_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_hai3_xi1_meng2_gu3_zu2_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_huang2_nan2_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_xi1_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/qing1_hai3_yu4_shu4_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_bin1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_de2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_dong1_ying2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_he2_ze2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_ji4_nan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_ji4_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_lai2_wu2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_liao2_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_lin2_yi2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_qing1_dao3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_ri4_zhao4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_tai4_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_wei1_hai3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_wei2_fang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_yan1_tai2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_zao3_zhuang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_dong1_zi1_bo2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_chang2_zhi4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_da4_tong2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_jin4_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_jin4_zhong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_lin2_fen2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_lv3_liang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_shuo4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_tai4_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_xin1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_yang2_quan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan1_xi1_yun4_cheng2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_an1_kang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_bao3_ji1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_han4_zhong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_shang1_luo4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_tong2_chuan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_wei4_nan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_xi1_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_xian2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_yan2_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shan3_xi1_yu2_lin2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/shanghai.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_a1_ba4_cang2_zu2_qiang1_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_ba1_zhong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_cheng2_du1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_da2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_de2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_gan1_zi1_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_guang3_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_guang3_yuan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_le4_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_liang2_shan1_yi2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_lu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_mei2_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_mian2_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_nan2_chong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_nei4_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_pan1_zhi1_hua1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_sui4_ning2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_ya3_an1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_yi2_bin1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_zi1_yang2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/si4_chuan1_zi4_gong4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/tianjin.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_a1_li3_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_chang1_du1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_la1_sa4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_lin2_zhi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_na4_qu1_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_ri4_ka1_ze2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xi1_cang2_shan1_nan2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xianggang.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_a1_ke4_su1_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_a1_la1_er3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_a1_le4_tai4_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ba1_yin1_guo1_leng2_meng2_gu3_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_bei3_tun2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_bo2_er3_ta3_la1_meng2_gu3_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_chang1_ji2_hui2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ha1_mi4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_he2_tian2_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ka1_shi2_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ke3_ke4_da2_la1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ke4_la1_ma3_yi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ke4_zi1_le4_su1_ke1_er3_ke4_zi1_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_kun1_yu4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_shi2_he2_zi3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_shuang1_he2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_ta3_cheng2_di4_qu1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_tie3_men2_guan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_tu2_mu4_shu1_ke4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_tu3_lu3_fan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_wu1_lu3_mu4_qi2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_wu3_jia1_qu2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/xin1_jiang1_yi1_li2_ha1_sa4_ke4_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_bao3_shan1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_chu3_xiong2_yi2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_da4_li3_bai2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_de2_hong2_dai3_zu2_jing3_po3_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_di2_qing4_cang2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_hong2_he2_ha1_ni2_zu2_yi2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_kun1_ming2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_li4_jiang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_lin2_cang1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_nu4_jiang1_li4_su4_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_pu3_er3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_qu1_jing4.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_wen2_shan1_zhuang4_zu2_miao2_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_xi1_shuang1_ban3_na4_dai3_zu2_zi4_zhi4_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_yu4_xi1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/yun2_nan2_zhao1_tong1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_hang2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_hu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_jia1_xing1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_jin1_hua2.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_li4_shui3.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_ning2_bo1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_qu2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_shao4_xing1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_tai2_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_wen1_zhou1.js"></script>
<script src="<?=$web?>newjs/echarts-china-cities-js/zhe4_jiang1_zhou1_shan1.js"></script>
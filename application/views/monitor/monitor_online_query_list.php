<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"edit_status"		=>	"编辑",
		"top01"				=>	"{$this->lang->line('so_num')}",
		"bss_id"			=>	"{$this->lang->line('bss_id_name')}",
		"log_date"			=>	"{$this->lang->line('log_date')}",
		"bss_token_id"		=>	"{$this->lang->line('bss_token_id')}",
		"type"				=>	"{$this->lang->line('type')}",
		"status"			=>	"{$this->lang->line('status')}",
		"status_desc"		=>	"{$this->lang->line('status_desc')}",
		"view_date"			=>	"{$this->lang->line('view_date')}",
		"view_users"		=>	"{$this->lang->line('view_users')}",
		"process_date"		=>	"{$this->lang->line('process_date')}",
		"process_user"		=>	"{$this->lang->line('process_user')}",
		"finish_date"		=>	"{$this->lang->line('finish_date')}",
		"finish_user"		=>	"{$this->lang->line('finish_user')}"
		
	);

	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	
	$searchArr = array (
		"top.top01"		=>  "{$this->lang->line('select_operator_class')}",
		"tbss.bss_id"	=>	"{$this->lang->line('select_batteryswaps_class')}"
	);
	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$batteryswapstationListRow = $this->Model_show_list->getbatteryswapstationList() ;
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"top.top01"		=>  $operatorsSelectListRow,
		"tbss.bss_id"	=>	$batteryswapstationListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	//echo 'A:'.$this->session->userdata('start_date');

	$type_name = array('','火灾','淹水','无轨道可归还',"前台時間與系統時間相差{$this->session->userdata('bss_time_min')}分鐘以上");
	$status_name = array('','未检视','处理中', '已完成');


?>
<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>

		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_alarm_online">
			<tr class="GridViewScrollHeader">
			<?
				$no_list = '';
				$n=1;
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				foreach ($colInfo["colName"] as $enName => $chName) {
					$style = "";
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" {$style}>
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 

					$n++;
				}
?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						$sn = $patternfilefieldInfoArr['alarm_online_sn'];
						$n = 1;
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == 'type'){
								$patternfilefieldInfoArr[$enName] = $type_name[$patternfilefieldInfoArr[$enName]];
							}else if($enName == 'status'){
								$patternfilefieldInfoArr[$enName] = $status_name[$patternfilefieldInfoArr[$enName]];
							}else if($enName == 'edit_status'){
								$patternfilefieldInfoArr[$enName] = '<a data-fancybox data-src="#editfancy" class="edit" href="javascript:;" onclick="edit_online('.$sn.');"></a>';
							}else if($enName == 'view_users'){
								$user_name_arr = '';
								$user_name = array();
								$viewUser = '';
								if($patternfilefieldInfoArr[$enName] != ''){
									$user_name_arr = explode(',',$patternfilefieldInfoArr[$enName]);
									$user_count = count($user_name_arr);
									for($i=0; $i<$user_count; $i++){
										if(isset($sysusername[$user_name_arr[$i]])){
											$user_name[] = $sysusername[$user_name_arr[$i]];
										}
									}

									$viewUser = join( ",", $user_name );
								}
								$patternfilefieldInfoArr[$enName] = $viewUser;
							}else if($enName == 'process_user'){
								$process_user = $patternfilefieldInfoArr[$enName];
								if(isset($sysusername[$patternfilefieldInfoArr[$enName]])){
									$process_user = $sysusername[$patternfilefieldInfoArr[$enName]];
								
								}
								$patternfilefieldInfoArr[$enName] = $process_user;
							}else if($enName == 'finish_user'){
								$finish_user = $patternfilefieldInfoArr[$enName];
								if(isset($sysusername[$patternfilefieldInfoArr[$enName]])){
									$finish_user = $sysusername[$patternfilefieldInfoArr[$enName]];
								
								}
								$patternfilefieldInfoArr[$enName] = $finish_user;
							}

							echo "					<td align=\"center\" class=\"\" >" . $patternfilefieldInfoArr[$enName] . "</td>\n";

							$n++;
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}

?>
			</table>

        </form>

<script type="text/javascript">


	$j(document).ready(function() {
		//凍結窗格(4欄)
		gridview(4,true);
	});


</script>

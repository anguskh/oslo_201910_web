<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"province"					=>	"{$this->lang->line('monitor_province')}",
		"city"						=>	"{$this->lang->line('monitor_city')}",
		"district"					=>  "{$this->lang->line('monitor_district')}",
		"bss_id"					=>  "{$this->lang->line('monitor_bss_id')}",	
		"track_no"					=>	"{$this->lang->line('monitor_track_no')}",
		"battery_id"				=>	"{$this->lang->line('monitor_battery_id')}",
		"battery_temperature"		=>	"{$this->lang->line('monitor_battery_temperature')}",
		"battery_voltage"			=>	"{$this->lang->line('monitor_battery_voltage')}",
		"battery_amps"				=>	"{$this->lang->line('monitor_battery_amps')}",
		"battery_capacity"			=>	"{$this->lang->line('monitor_battery_capacity')}",
		"status"					=>	"{$this->lang->line('monitor_status')}",
		"status_desc"				=>  "{$this->lang->line('monitor_remark')}",
		"user_name"					=>	"{$this->lang->line('monitor_user_name')}",
		"user_id"					=>	"{$this->lang->line('monitor_user_id')}",
		"sb_num"					=>	"{$this->lang->line('monitor_icon')}",
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();

?>
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="monitor">
				<tr class="GridViewScrollHeader">
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr class=\"GridViewScrollItem\">\n" ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr[$enName])
						{
							case '1':
								$data = "未檢視";
								break;
							case '2':
								$data = "處理中";
								break;
							case '3':
								$data = "已完成";
								break;
						}
						echo "					<td align=\"left\">".$data."</td>\n" ;
					}else if($enName == "bss_id"){
						echo "					<td align=\"left\">".$InfoArr['location']."<br>(".$InfoArr[$enName].")</a></td>\n" ;
					}else if($enName == "sb_num"){
						echo "					<td align=\"left\"><a href='#' class='watch' onclick='getbssinfo(\"{$InfoArr[$enName]}\")'>".$InfoArr[$enName]."</a></td>\n" ;
					}else if($enName == "battery_temperature"){
						$battery_temperature = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_temperature = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_temperature."</td>\n" ;
					}else if($enName == "battery_voltage"){
						$battery_voltage = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_voltage = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_voltage."</td>\n" ;
					}else if($enName == "battery_amps"){
						$battery_amps = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_amps = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_amps."</td>\n" ;
					}else if($enName == "battery_capacity"){
						$battery_capacity = "";
						if(isset($battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName])){
							$battery_capacity = $battery_info[$InfoArr['battery_id']][$InfoArr['track_no']][$enName];
						}
						echo "					<td align=\"left\">".$battery_amps."</td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>

<script>
	$j(document).ready(function(){
		//凍結窗格
		gridview(0,false);
	});
</script>
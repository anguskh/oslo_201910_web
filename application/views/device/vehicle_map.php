<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        body, html {width: 100%;height: 100%;margin:0;font-family:"微软雅黑";}
        #allmap{width:100%;height:100%;}
        p{margin-left:5px; font-size:14px;}
    </style>
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <title>给多个点添加信息窗口</title>
</head>
<?php 
	$web = $this->config->item('base_url');
?>
<body>
    <div id="allmap"><?=$error;?></div>
</body>
</html>
<script>
<?
	if($error == ''){
?>

    window.onload=init;
        function init() {
            var center = new qq.maps.LatLng(<?=$latitude;?> , <?=$longitude;?>);
            var map = new qq.maps.Map(document.getElementById("allmap"), {
                center: center,
                zoom: 14
            });

		var data_info = [
<?php 
			$web = $this->config->item('base_url');
			/*foreach($stationInfo as $key => $value)
			{
				echo "[{$value['longitude']},{$value['latitude']},'{$value['bss_id']}'],";
			}*/

			echo "[$latitude, $longitude,'$unit_id'],";
?>
			// [113.264531,23.157003,'广州火车站'],[113.330934,23.113401,'广州塔（赤岗塔）'],[113.312213,23.147267,'广州动物园'],[113.372867,23.134274,'天河公园']
			]; 


            var anchor = new qq.maps.Point(20, 20),
                size = new qq.maps.Size(40, 40),
                origin = new qq.maps.Point(0, 0),
                icon = new qq.maps.MarkerImage(
                    "<?=$web;?>images/location_motor.png",
                    size,
                    origin,
                    anchor
                );

            for(var i=0;i<data_info.length;i++){
                //创建一个Marker
                var marker = new qq.maps.Marker({
                    //设置Marker的位置坐标
                    position: new qq.maps.LatLng(data_info[i][0],data_info[i][1]),
                    //设置显示Marker的地图
                    map: map
                });
     
                //设置Marker的可见性，为true时可见,false时不可见，默认属性为true
                marker.setVisible(true);
                //设置Marker是否可以被拖拽，为true时可拖拽，false时不可拖拽，默认属性为false
                marker.setDraggable(false);
                ////设置Marker自定义图标的属性，size是图标尺寸，该尺寸为显示图标的实际尺寸，origin是切图坐标，该坐标是相对于图片左上角默认为（0,0）的相对像素坐标，anchor是锚点坐标，描述经纬度点对应图标中的位置
                
                marker.setIcon(icon);
                var content = '<div style="text-align:center;white-space:nowrap;margin:10px;">'+data_info[i][2]+'</div>';
                addClickHandler(content,marker);
            }

            
            function addClickHandler(content,marker)
            {
               //添加信息窗口
                var info = new qq.maps.InfoWindow({
                    map: map
                });
                //标记Marker点击事件
                qq.maps.event.addListener(marker, 'click', function() {
                    info.open();
                    info.setContent(content);
                    // info.setContent('<div style="text-align:center;white-space:nowrap;' +
                    //     'margin:10px;">单击标记</div>');
                    info.setPosition(marker.getPosition());
                }); 
            }
        }
<?
	}
?>
</script>
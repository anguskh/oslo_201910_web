<?
	$web = $this->config->item('base_url');
	$colArr = array(
		'status'				=>	"{$this->lang->line('vehicle_status')}",
		'to_num'				=>	"{$this->lang->line('vehicle_operator')}",
		'tso_num'				=>	"{$this->lang->line('vehicle_sub_operator')}",
		'td_num'				=>	"{$this->lang->line('vehicle_dealer')}",
		'tsd_num'				=>	"{$this->lang->line('vehicle_sub_dealer')}",
		'unit_id'				=>	"{$this->lang->line('vehicle_unit_id')}",
		'vehicle_code'			=>	"{$this->lang->line('vehicle_code')}",
		'manufacture_date'		=>	"{$this->lang->line('vehicle_manufacture_date')}",
		'license_plate_number'	=>	"{$this->lang->line('vehicle_license_plate_number')}",
		'engine_number'			=>	"{$this->lang->line('vehicle_engine_number')}",
		'car_machine_id'		=>	"{$this->lang->line('vehicle_car_machine_id')}",
		'vehicle_model'			=>	"{$this->lang->line('vehicle_model')}",
		'vehicle_color'			=>	"{$this->lang->line('vehicle_color')}",
		'4g_ip'					=>	"{$this->lang->line('vehicle_4g_ip')}",
		'bluetooth_name'		=>	"{$this->lang->line('vehicle_bluetooth_name')}",
		'bluetooth_mac'			=>	"{$this->lang->line('vehicle_bluetooth_mac')}",
		'lora_sn'				=>	"{$this->lang->line('vehicle_lora_sn')}",
		'vehicle_user_id'		=>	"{$this->lang->line('vehicle_user_id')}",
		'vehicle_type'			=>	"{$this->lang->line('vehicle_type')}",
		'use_type'				=>	"{$this->lang->line('vehicle_use_type')}",
		'vehicle_charge'		=>	"{$this->lang->line('vehicle_charge')}",
		'charge_count'			=>	"{$this->lang->line('vehicle_charge_count')}",
		'latitude'				=>	"{$this->lang->line('vehicle_latitude')}",
		'longitude'				=>	"{$this->lang->line('vehicle_longitude')}"
	);
	$colInfo = array("colName" => $colArr);

	$dor_name = array(""=>"", "D"=>"经销商", "O"=>"营运商");
	$get_full_url_random = get_full_url_random();
	//匯入网址
	$upload_url2 = "{$get_full_url_random}/upload";
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<link href="<?=$web.$newcss;?>newcss/fixedtable.css" rel="stylesheet" type="text/css">

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<code>
		<span class="topbtn">
			<a href="#" id="fileReturn" class="buttonblack sback">返回</a>
			<a id="fileUpload" class="buttonblack simport">汇入</a>
		</span>
		<p class="btitle"><?=$this->lang->line('vehicle_management_upload')?></p>
		</code>
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />



<?
				$table_1_head = '';
				$table_2_head = '<th><div class="fixws"></div></th>';
				$span_arr = array();
				$n=1;

				//让标题栏位有排序功能
				foreach ($colInfo["colName"] as $enName => $chName) {
					//栏位显示
					/*echo "<th align=\"center\">
							<a fieldname='{$enName}'>$chName</a>
						  </th>\n";*/

					$span_arr[$enName] = "<a fieldname='{$enName}'>$chName</a>";

					$table_2_head .= '<th><div class="fixw">'.$span_arr[$enName].'</div></th>';

					$n++;
				}

				$table_1_head .= '<thead>
									<tr>
										<th><div class="fixws"><input type="checkbox" name="ckbAll" id="ckbAll" value=""></div></th>
										<th><div class="fixw">'.$span_arr['status'] .'</div></th>
									</tr>
								</thead>';
				$table_1_content = '';
				$table_2_content = '';

				if(isset($InfoRow)){
					$num=1;
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						$checkStr = "<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$num}\" >" ;

						if($patternfilefieldInfoArr['cart_flag'] == 'N'){
							$checkStr = '';
						}

						$n = 1;
						$table_2_content .= '<tr>';
						$table_2_content .= '<td>'.$checkStr.'</td>';
						foreach ($colInfo["colName"] as $enName => $chName) {
							$table_2_content .= '<td>'.$patternfilefieldInfoArr[$enName].'</td>';
							$n++;
						}
						$table_2_content .= '</tr>';

						$table_1_content .= '
										<tr>
											<td>'.$checkStr.'</td>
											<td>'.$patternfilefieldInfoArr['status'].'</td>
										</tr>';
						$num++;
					}
				}



				echo '<div class="table-responsive fixedbox fixedbox2">
				<table class="fixed-column fixedtb vehicle_import">
					'.$table_1_head.'
		            <tbody>
						'.$table_1_content.'
		            </tbody>
		    	</table>
				<table class="table table-striped table-bordered table-hover table-condensed fixedtb vehicle_import">
					<thead>
						<tr>
							'.$table_2_head.'
						</tr>
					</thead>
					<tbody>
						'.$table_2_content.'
					</tbody>			
				</table>
			</div>';




?>

	
		</form>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//功能按钮显示控制
	button_display();
});


/*返回*/
$j("#fileReturn").click( function () {
	history.back()
});

/*
**上傳
*/
$j("#fileUpload").click( function () {
	var selcount = getCkbVal();
	if(selcount!=''){
		if ( confirm("是否要將勾選清單匯入？") ) {
			$j("#listForm").attr( "method", "POST" ) ;
			$j("#listForm").attr( "action", "<?=$upload_url2?>" ) ;
			$j("#listForm").submit() ;
		}
	}
});


</script>

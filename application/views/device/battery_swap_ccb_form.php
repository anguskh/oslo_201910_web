<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"so_num"			=> "",
		"sb_num"			=> "",
		"ccb_id"			=> "",
		"status"			=> "Y"
		
	);

	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('battery_swap_ccb_management')?><span><?=$show_sub_title;?></span></p>
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('battery_swap_ccb_operator')?></label>
							<select name="postdata[so_num]" id='so_num' style="width:200px;" onchange="change_sb_num(this.value);">
								<?=create_select_option($operatorList, $dataInfo["so_num"], $this->lang->line('battery_swap_ccb_select_operator'));?>
							</select>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('battery_swap_ccb_station')?></label>
							<select name="postdata[sb_num]" id='sb_num' style="width:200px;">
								<?=create_select_option($batteryswapstationList, $dataInfo["sb_num"], $this->lang->line('battery_swap_ccb_select_station'), array('so_num'));?>
							</select>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('battery_swap_ccb_ccb_id')?></label>
							<input type="text" class="txt" name="postdata[ccb_id]" id="ccb_id"value="<?=$dataInfo['ccb_id']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('battery_swap_ccb_status')?></label>
							<input type="radio" name="postdata[status]" id="statusY" class="radio first" value="Y" <?echo (($dataInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('battery_swap_ccb_statusY')?>
							<input type="radio" name="postdata[status]" id="statusN" class="radio" value="N" <?echo (($dataInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('battery_swap_ccb_statusN')?>
							<input type="radio" name="postdata[status]" id="statusE" class="radio" value="E" <?echo (($dataInfo["status"]=='E')?'checked':''); ?>>
							<?=$this->lang->line('battery_swap_ccb_statusE')?>
						</li>
					</ul>
				</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

function change_sb_num(filed_val){
	if(filed_val == ''){
		$j("#sb_num option").show();
	}else{
		$j("#sb_num option:selected").removeAttr("selected");
		$j("#sb_num option").hide();
		$j("#sb_num option[so_num='"+filed_val+"']").show();

		$j("#sb_num option[value='']").show();
		$j("#sb_num option[value='']").prop('selected', true);
	}
}
</script>
<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"advertising_explain"			=>	"{$this->lang->line('advertising_explain')}",
		"advertising_updating_date"		=>	"{$this->lang->line('advertising_updating_date')}",
		"advertising_address"			=>	"{$this->lang->line('advertising_address')}",
		"total_num"						=>	"{$this->lang->line('total_num')}",
		"notify"						=>	"{$this->lang->line('notify')}",
		"status"						=>  "{$this->lang->line('advertising_status')}",
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	//明細列表網址
	$detail_url = "{$get_full_url_random}/bssdetaillist/";
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('bss_advertising_management')?></p>
			<div class="topbtn column">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="说明" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<?=$user_access_control?>
						<form id="searchForm" method="post" action="<?=$search_url?>">
							<!--预防CSRF攻击-->
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
						</form>
				</div>
			</div>
		</div>
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="vehicle">
				<tr class="GridViewScrollHeader">
					<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr class=\"GridViewScrollItem\">\n" ;

				$data_sn = $InfoArr["s_num"] ;
				$checkStr = "
				<td>
					<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$data_sn}\" >
				</td>
				" ;
				echo $checkStr ;
				$total_num = 0;
				foreach ($colInfo["colName"] as $enName => $chName) {
					if($enName == "total_num"){
						$total_num = $InfoArr[$enName];
						$data = $InfoArr[$enName];
						if($total_num!=0)
							$data = "<a href='#' class='all_bss' value='{$InfoArr['s_num']}'>{$data}</a>";
						echo '<td align="center">'.$data.'</td>';
					}else if($enName == "notify"){
						$percen = 0;
						if($total_num!=0)
						{
							$percen = round(($InfoArr[$enName]/$total_num)*100,2);
						}
						$data1 = "<span style='color:green'>{$InfoArr[$enName]}({$percen}%)</span>";
						if($percen!=0)
						{
							$data = "<a href='#' class='notify_bss' value='{$InfoArr['s_num']}'>{$data1}</a>";
						}
						else
						{
							$data = "{$data1}";
						}
						
						$percen2 = 0;
						$not_notify = $total_num-$InfoArr[$enName];
						if($total_num!=0)
						{
							$percen2 = 100-$percen;
						}

						$data2 = "<span style='color:red'>{$not_notify}({$percen2}%)</span>";
						if($percen2!=0)
						{
							$data .= "<br><a href='#' class='not_notify_bss' value='{$InfoArr['s_num']}'>{$data2}</a>";
						}
						else
						{
							$data .= "<br>{$data2}";
						}

						echo '<td align="center">'.$data.'</td>';
					}else if($enName == "status"){
						switch($InfoArr[$enName]) {
							case "1":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('enable')}</font></td>\n" ;
								break;
							case "0":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('disable')}</font></td>\n" ;
								break;
							case "D":
								echo "					<td align=\"center\"><font color=red>{$this->lang->line('delete')}</font></td>\n" ;
								break;
							default:
								echo "					<td align=\"center\"><font color=red></font></td>\n" ;
								break;
						}
					}else{
						echo "<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		<form id="detail_from">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" id="bss_version_num" name="bss_version_num">
			<input type="hidden" id="detail_type" name="detail_type">
		</form>
		<!-- 分页区 -->
		<div class="pagebar">
			<?=$pageInfo["html"]?>
		</div>
	</div>
</div>
<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

$j("#id_upload_file").click(function(){
	$j("#upload_file").click();
});


function checkFile(file, file_id) { 
	var value = file.value; 
	var isCheckImageType = true;  //是否检查副档名 
	var re = /\.(xls)$/i;  //允许的图片副档名 
	if (isCheckImageType && !re.test(value)) { 
		alert("只允许上传xls的文件檔"); 
		$j('#'+file_id+'_txt').val('');
	} else { 
		$j('#'+file_id+'_txt').val($j('#'+file_id).val());
	} 
} 

//所有明細
$j(".all_bss").click(function(){
	var bss_version_num = $j(this).attr('value');
	$j("#bss_version_num").val(bss_version_num);
	$j("#detail_type").val("1");
	$j("#detail_from").attr( "method", "POST" ) ;
	$j("#detail_from").attr( "action", "<?=$detail_url;?>"+bss_version_num ) ;
	$j("#detail_from").submit() ;
});

//已通知明細
$j(".notify_bss").click(function(){
	var bss_version_num = $j(this).attr('value');
	$j("#bss_version_num").val(bss_version_num);
	$j("#detail_type").val("2");
	$j("#detail_from").attr( "method", "POST" ) ;
	$j("#detail_from").attr( "action", "<?=$detail_url;?>"+bss_version_num ) ;
	$j("#detail_from").submit() ;
});

//未通知明細
$j(".not_notify_bss").click(function(){
	var bss_version_num = $j(this).attr('value');
	$j("#bss_version_num").val(bss_version_num);
	$j("#detail_type").val("3");
	$j("#detail_from").attr( "method", "POST" ) ;
	$j("#detail_from").attr( "action", "<?=$detail_url;?>"+bss_version_num ) ;
	$j("#detail_from").submit() ;
});

//已更新明細
$j(".update_bss").click(function(){
	var bss_version_num = $j(this).attr('value');
	$j("#bss_version_num").val(bss_version_num);
	$j("#detail_type").val("4");
	$j("#detail_from").attr( "method", "POST" ) ;
	$j("#detail_from").attr( "action", "<?=$detail_url;?>"+bss_version_num ) ;
	$j("#detail_from").submit() ;
});

//未更新明細
$j(".not_update_bss").click(function(){
	var bss_version_num = $j(this).attr('value');
	$j("#bss_version_num").val(bss_version_num);
	$j("#detail_type").val("5");
	$j("#detail_from").attr( "method", "POST" ) ;
	$j("#detail_from").attr( "action", "<?=$detail_url;?>"+bss_version_num ) ;
	$j("#detail_from").submit() ;
});

</script>

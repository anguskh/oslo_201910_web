<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"								=> "",
		"advertising_type"					=> "1",
		"advertising_upload_img"			=> "",
		"advertising_upload_video"			=> "",
		"advertising_updating_date"			=> "",
		"advertising_explain"				=> "",
		"all_sb_num"						=> "",
		"status"							=> "1"
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";

	$Sel1 = "";
	$Sel0 = "";
	if ($dataInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($dataInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($dataInfo["status"] == "0") {
		$Sel0 = " checked";
	}

	$img_checked = "checked";
	$video_checked = "";
	$filename = "";
	if($dataInfo["advertising_type"]==1)
	{
		$filename = $dataInfo["advertising_upload_img"];
	}	
	else if($dataInfo["advertising_type"]==2)
	{
		$filename = $dataInfo["advertising_upload_video"];
		$img_checked = "";
		$video_checked = "checked";
	}
		
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
.leftdiv{
	float:left;
}

.sol-option {
    float: left;
    padding: 5px 10px;
    display: block;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
<!-- 下拉搜尋 多筆勾選 -->
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('bss_advertising_management')?><span><?=$show_sub_title;?></span></p>
		<!-- <p class="btitle"><?=$this->lang->line('bss_manage_management')?><span><?=$show_sub_title;?></span></p> -->
		<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$StartRow?>" />
			<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
			<div class="section container">
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('advertising_explain')?></label>
							<input type="text" class="txt" style="width:220px;" id="advertising_explain" name="postdata[advertising_explain]" value="<?=$dataInfo["advertising_explain"]?>">
						</li>
						<li>
							<label for=""><?=$this->lang->line('advertising_updating_date')?></label>
							<input type="date" class="txt" id="advertising_updating_date" name="postdata[advertising_updating_date]" value="<?=$dataInfo["advertising_updating_date"]?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:32%;">
					<ul>
						<li style="height:53px;">
							<label for=""><?=$this->lang->line('advertising_type')?></label>
							<input type="radio" id="advertising_type_img" name="postdata[advertising_type]" value='1' <?=$img_checked?>> 图片 
							<!-- <input type="radio" id="advertising_type_video" name="postdata[advertising_type]" value='2' <?=$video_checked?>> 影片 -->
						</li>
						<li>
							<label for=""  class="required">上传</label>
							<input class="txt" type="text" id="file_txt" name="file_txt" class="tb validate[required]" style="width:220px;" value="<?=$filename?>" readonly>
							<input type="file" name="advertising_file[]" id="advertising_file" required="required" draggable="true" class="imgbtn" style="display:none;" multiple="multiple">
							<input type="button" value="浏览" id="id_advertising_file" class="btn">
						</li>
						<li>
							<?php
								if($filename!="")
								{
									if($dataInfo["advertising_type"]==1)
									{
										$filenameArr = explode(",",$filename);
										foreach($filenameArr as $keyI => $valueI)
										{
											$dir = $this->session->userdata('advertising_file_url')."ad_{$dataInfo['s_num']}/";
											$file=$dir.$valueI;
											if(file_exists($file))
											{
												$filePath = $web.substr($dir, 2).$valueI;
												echo "<div class='leftdiv'>";
												echo "<img src='{$filePath}' height='100px' width='100px' />";
												echo "<input type='button' id='deletefilebtn' name = 'deletefilebtn' onclick='deletefile(this,\"{$file}\",\"{$valueI}\")' value='刪除'>";
												echo "</div>";
											}	
										}
									}
								}
							?>
						<li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="">租借站</label>
							<select multiple="multiple" id="all_sb_num" name="all_sb_num[]" style="width: 368px;">
<?
								$all_arr = $select_batteryswapstation;
								foreach($batteryswapstationList as $i_arr){
									$selected = "";
									if($all_arr!="")
									{
										if(in_array($i_arr['s_num'], $all_arr)){
											$selected = "selected";
										}
									}
									echo '<option value="'.$i_arr['s_num'].'" '.$selected.'>'.$i_arr['bss_id_name'].'</option>';
								}
?>
							</select>	
							<div id="current-selection-all_sb_num" class="sol-current-selection">
							</div>
						</li>
						<li>
							<label for=""><?=$this->lang->line('advertising_status')?></label>
							<input  name="postdata[status]" type="radio" class="radio first"  value="1" <?=$Sel1?>><?=$this->lang->line('enable')?>
							<input  name="postdata[status]"  type="radio" class="radio"  value="0" <?=$Sel0?>><?=$this->lang->line('disable')?>
						</li>
					</ul>
				</div>
			</div>
		</form>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		var advertising_file = $j("#advertising_file").val();
		// var strFileName=bss_file.replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi,"$1");  //正则表达式获取文件名，不带后缀
		// var FileExt=bss_file.replace(/.+\./,"");   //正则表达式获取后缀
		var flag = 'N';
		var s_num = "<?=$dataInfo['s_num']?>";
		// var filename = strFileName+'.'+FileExt;
		var filename = $j('#file_txt').val();
		var file_type = $j('input:radio[name=postdata[advertising_type]]:checked').val();

		if(s_num == ''){
			if(advertising_file == ''){
				alert('檔案必須上傳！');
			}else{
				//檢查檔案有沒有重複或不存在
				$j.ajax({
					type:'post',
					url: '<?= $web ?>device/bss_advertising/check_file_name',
					async :false,
					data: { 
							s_num: '<?=$dataInfo['s_num']?>',
							filename: filename,
							file_type: file_type,
							<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
					error: function(xhr) {
						strMsg = '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
						alert(strMsg);
					},
					success: function (response) {
						if(response == 'N'){
							alert("檔案名稱重覆！");
						}else{
							flag = 'Y';
						}
					}
				}) ;
			}
		}else{
			flag = 'Y';
		}

		if(flag == 'Y'){
			$j("#formID").submit();
		}
	}

	changeupload();
	$j('input:radio[name=postdata[advertising_type]]').click(function(){
		$j('#file_txt').val("");
		$j('#advertising_file').val("");
		changeupload();
	});
	
	function changeupload()
	{
		if($j('input:radio[name=postdata[advertising_type]]:checked').val()==1)
		{
			$j('#advertising_file').attr("accept","image/png, image/jpeg");
		}
		else if($j('input:radio[name=postdata[advertising_type]]:checked').val()==2)
		{
			$j('#advertising_file').attr("accept","video/*");
		}
	}

	$j("#advertising_file").change(function(){
		if (event.target.files.length > 0) {
			var file_name = "";
	        for (var i = 0; i < event.target.files.length; i++) {
                var file = event.target.files[i];
                if(file.name.indexOf(",")!="-1")
                {
                	alert("上傳檔名不可有逗號");
                	return;
                }
               	if(event.target.files[i].size>1024*1024)
               	{
               		alert("上傳檔案不可大於1M");
               		return;
               	}
                if(file_name=="")
                	file_name = file.name;
                else
                	file_name = file_name+","+file.name;
            }
            $j('#file_txt').val(file_name);
        }
	});

	function deletefile(thisobj,file,filename)
	{
		if(confirm("您確定要刪除此檔案?"))
		{
			var filenameArr = $j("#file_txt").val().split(",");
			var file_type = $j('input:radio[name=postdata[advertising_type]]:checked').val();
			filenameArr.remove(filename);
			var file_name = "";
		    for (var i = 0; i < filenameArr.length; i++) {
	            if(file_name=="")
	            	file_name = filenameArr[i];
	            else
	            	file_name = file_name+","+filenameArr[i];
	        }
			//刪除檔案
			$j.ajax({
				type:'post',
				url: '<?= $web ?>device/bss_advertising/deletefile',
				async :false,
				data: { 
						s_num: '<?=$dataInfo['s_num']?>',
						filename: file_name,
						file_type: file_type,
						filepath: file,
						<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
				error: function(xhr) {
					strMsg = '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
					alert(strMsg);
				},
				success: function (response) {
					if(response == 'N'){
						alert("刪除失敗");
					}else{
						$j('#file_txt').val(file_name);
						$j(thisobj).parent().remove()
						alert("刪除成功");
					}
				}
			}) ;
		}
	}

	Array.prototype.indexOf = function(val) {
		for (var i = 0; i < this.length; i++) {
			if (this[i] == val) return i;
		}
		return -1;
	};

	Array.prototype.remove = function(val) {
		var index = this.indexOf(val);
			if (index > -1) {
			this.splice(index, 1);
		}
	};
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#id_advertising_file").click(function(){
	$j("#advertising_file").click();
});
//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
//alert($j.fn.jquery);  //顯示目前$j是誰在用

var arr = new Array('group_sb_num','all_sb_num');

var mutiselectObjArr = new Array();
var y = 0;

arr.forEach(function(value) {
	mutiselectObjArr[y] = $j_1102('#'+value).searchableOptionList({
		showSelectAll: true,
		maxHeight:'700px',
		events: {
			maxwidth: '1100', //設定寬度
			nowId: value
		},
		texts: {
			noItemsAvailable: '查无此资料',
			selectNone: '清除选择',
			selectAll: '全选',
			searchplaceholder: '选择租借站'
		}
	});

	y++;
});

$j_1102(window).load(function(){
	// do{
		$j_1102('.sol-option').css('width','20%');
	// }while ($j_1102('.sol-option').length==0)
});

</script>
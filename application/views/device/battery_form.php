<?
	$web = $this->config->item('base_url');
	$to_flag = $this->session->userdata('to_flag'); 
	$to_flag_num = "";
	$disabled = "";
	$DorO_flag= 'D';
	if($to_flag == '1'){
		$to_flag_num = $this->session->userdata('to_num');
		$disabled = "disabled";
		$DorO_flag= 'O';
	}

	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"DorO_flag"			=> $DorO_flag,
		"do_num"			=> $to_flag_num,
		"sv_num"			=> "",
		"battery_id"		=> "",
		"manufacture_date"	=> "",
		"position"			=> "",
		"exchange_count"	=> "",
		"status"			=> "0",
		"battery_health"	=> "",	//電池健康度
		"battery_capacity"	=> "",	//电量
		"battery_station_position"	=> "",	
		"battery_gps_manufacturer"	=> "",	
		"battery_gps_version"		=> "",	
		"sys_no"			=> "2"	//預設2.0
		
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('battery_management')?><span><?=$show_sub_title;?></span></p>
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label>厂商类型</label>
							<input type="radio" name="postdata[DorO_flag]" id="DorO_flagD" class="radio first" value="D" <?php echo (($dataInfo["DorO_flag"]=='D')?'checked':''); ?> <?=$disabled;?> onclick="changeDorflag('D','O');">经销商<input type="radio" name="postdata[DorO_flag]" id="DorO_flagO" class="radio"  onclick="changeDorflag('O','D');" <?php echo (($dataInfo["DorO_flag"]=='O')?'checked':''); ?> value="O" <?=$disabled;?>>营运商
						</li>
						<li  id="dorflag_D" style="display:<?php echo (($dataInfo["DorO_flag"]=='D')?'block':'none'); ?>;" class="required">
							<label><?=$this->lang->line('battery_dealer')?></label>
							<select name="sd_num" class="validate[required]" id='sd_num' style="width:200px;" >
									<?=create_select_option($dealerList, $dataInfo["do_num"], '');?>
							</select>
						</li>
						<li id="dorflag_O" style="display:<?php echo (($dataInfo["DorO_flag"]=='O')?'block':'none'); ?>;">
							<label><?=$this->lang->line('battery_operator')?></label>
							<select name="so_num" id='so_num' style="width:100px;" <?=$disabled;?>>
									<?=create_select_option($operatorList, $dataInfo["do_num"], '');?>
							</select>
						</li>
						<li>
							<label><?=$this->lang->line('battery_M30')?></label>
							<select name="postdata[sv_num]" id='sv_num' style="width:200px;">
								<?=create_select_option($vehicleList, $dataInfo["sv_num"], '');?>	
							</select>
						</li>
						<li>
							<label class="required"><?=$this->lang->line('sys_no')?></label>
							<input type="radio" name="postdata[sys_no]" id="sys_no" class="radio first validate[required]" value="1" <?php echo (($dataInfo["sys_no"]=='1')?'checked':''); ?>>1.0<input type="radio" name="postdata[sys_no]" id="sys_no" class="radio validate[required]" value="2" <?php echo (($dataInfo["sys_no"]=='2')?'checked':''); ?>>2.0
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:24%;">
					<ul>
						<li style="height:60px;">
							<label><?=$this->lang->line('battery_station_position')?></label>
							<input type="text" class="txt" name="postdata[battery_station_position]" id="battery_station_position" value="<?=$dataInfo['battery_station_position']?>">
						</li>
						<li>
							<label><?=$this->lang->line('battery_gps_manufacturer')?></label>
							<input type="text" class="txt" name="postdata[battery_gps_manufacturer]" id="battery_gps_manufacturer" value="<?=$dataInfo['battery_gps_manufacturer']?>">
						</li>
						<li>
							<label><?=$this->lang->line('battery_gps_version')?></label>
							<input type="text" class="txt" name="postdata[battery_gps_version]" id="battery_gps_version" value="<?=$dataInfo['battery_gps_version']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:32%;">
					<ul>
						<li>
							<label class="required"><?=$this->lang->line('battery_id')?></label>
							<input type="text" class="txt validate[required], ajax[ajax_pk_battery_id, <?=$dataInfo['s_num']?>]" style="width:285px;" name="postdata[battery_id]" id="battery_id" maxlength="32" value="<?=$dataInfo['battery_id']?>">
						</li>
						<li>
							<label class="required"><?=$this->lang->line('battery_manufacture_date')?></label>
							<input type="date" class="txt validate[required]" name="postdata[manufacture_date]" id="manufacture_date" value="<?=$dataInfo['manufacture_date']?>">
						</li>
						<li>
							<label class="required"><?=$this->lang->line('battery_position')?></label>
							<input type="radio" name="postdata[position]" id="positionB" class="radio first validate[required]" value="B" <?php echo (($dataInfo["position"]=='B')?'checked':''); ?>>BSS<input type="radio" name="postdata[position]" id="positionV" class="radio validate[required]" value="V" <?php echo (($dataInfo["position"]=='V')?'checked':''); ?>>电动机车<input type="radio" name="postdata[position]" id="positionO" class="radio validate[required]" value="O" <?php echo (($dataInfo["position"]=='O')?'checked':''); ?>>其他
						</li>
						<!-- li>
							<label><?=$this->lang->line('battery_exchange_count')?></label>
							<input type="text" class="txt" name="postdata[exchange_count]" id="exchange_count" maxlength="5" value="<?=$dataInfo['exchange_count']?>">
						</li -->
					</ul>
				</div>
				<div class="container-box" style="width:19%;">
					<ul>
						<li>
							<label>SOC</label>
							<input type="text" class="txt" name="postdata[battery_capacity]" id="battery_capacity" value="<?=$dataInfo['battery_capacity']?>">
						</li>
						<li>
							<label>SOH</label>
							<input type="text" class="txt" name="postdata[battery_health]" id="battery_health" value="<?=$dataInfo['battery_health']?>">
						</li>
						<li class="line2">
							<label><?=$this->lang->line('battery_status')?></label>
							<select name="postdata[status]" id='status' style="width:200px;">
								<?php
									$ck1 = "";
									$ck2 = "";
									$ck3 = "";
									$ck4 = "";
									$ck5 = "";
									$ck6 = "";
									$ck7 = "";
									switch($dataInfo['status'])
									{
										case 0:
											$ck1 = "selected";
											break;
										case 1:
											$ck2 = "selected";
											break;
										case 2:
											$ck3 = "selected";
											break;
										case 3:
											$ck4 = "selected";
											break;
										case 4:
											$ck5 = "selected";
											break;
										case 5:
											$ck6 = "selected";
											break;
										case 6:
											$ck7 = "selected";
											break;
									}
								?>
								<option value='0' <?=$ck1?>><?=$this->lang->line('battery_status0')?></option>
								<option value='1' <?=$ck2?>><?=$this->lang->line('battery_status1')?></option>
								<option value='2' <?=$ck3?>><?=$this->lang->line('battery_status2')?></option>
								<option value='3' <?=$ck4?>><?=$this->lang->line('battery_status3')?></option>
								<option value='4' <?=$ck5?>><?=$this->lang->line('battery_status4')?></option>
								<option value='5' <?=$ck6?>><?=$this->lang->line('battery_status5')?></option>
								<option value='6' <?=$ck7?>><?=$this->lang->line('battery_status6')?></option>
							</select>
							<!-- <input type="radio" class="radio first">正常(充电中)<input type="radio" class="radio">已满电<input type="radio" class="radio">故障<input type="radio" class="radio">维修<input type="radio" class="radio">报废 -->
						</li>
					</ul>
				</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#DorO_flagD").attr('disabled', false);
		$j("#DorO_flagO").attr('disabled', false);
		$j("#so_num").attr('disabled', false);
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

function changeDorflag(open_filed, close_filed){
	$j("#dorflag_"+open_filed).css('display','block');
	$j("#dorflag_"+close_filed).css('display','none');
}
</script>
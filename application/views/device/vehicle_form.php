<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"					=> "",
		"DorO_flag"				=> "O",
		"do_num"				=> "",
		"dso_num"				=> "",
		"unit_id"				=> "",
		"vehicle_code"			=> "",
		"manufacture_date"		=> "",
		"license_plate_number"	=> "",
		"engine_number"			=> "",
		"car_machine_id"		=> "",
		"vehicle_model"			=> "",
		"vehicle_color"			=> "",
		"4g_ip"					=> "",
		"bluetooth_name"		=> "",
		"bluetooth_mac"			=> "",
		"lora_sn"				=> "",
		"vehicle_user_id"		=> "",
		"vehicle_type"			=> "",
		"use_type"				=> "",
		"vehicle_charge"		=> "",
		"charge_count"			=> "",
		"latitude"				=> "",
		"longitude"				=> "",
		"status"				=> ""	
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$msg = "";
	$class = "validate[required]";
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";

?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		
		<p class="btitle"><?=$this->lang->line('vehicle_management')?><span><?=(($dataInfo["s_num"]!='')?'编辑':'新增')?></span></p>
		
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" id="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li style="height:53px;">
							<label><span></span>厂商类型</label>
							<input type="radio" name="postdata[DorO_flag]" id="DorO_flagO" class="radio first" value="O" <?php echo (($dataInfo["DorO_flag"]=='O')?'checked':''); ?> onclick="changeDorflag('O','D');">营运商<input type="radio" name="postdata[DorO_flag]" id="" class="radio"  onclick="changeDorflag('D','O');" <?php echo (($dataInfo["DorO_flag"]=='D')?'checked':''); ?> value="D">经销商
						</li>
						<li class="dorflag_O" style="display:<?php echo (($dataInfo["DorO_flag"]=='O')?'block':'none'); ?>;">
							<label><span></span><?=$this->lang->line('vehicle_operator')?></label>
							<select name="to_num"id="to_num" onchange="change_sub_num('to_num', 'tso_num');" style="width:240px;">
									<?=create_select_option($operatorList, $dataInfo["do_num"], $this->lang->line('select_operator_class'));?>
							</select>
						</li>
						<li class="dorflag_O" style="display:<?php echo (($dataInfo["DorO_flag"]=='O')?'block':'none'); ?>;">
							<label><span></span><?=$this->lang->line('vehicle_sub_operator')?></label>
							<select name="tso_num" id='tso_num' style="width:240px;">
									<?=create_select_option($sub_operator_list, $dataInfo["dso_num"], $this->lang->line('select_suboperator_class'), array("to_num"));?>
							</select>
						</li>
						<li class="dorflag_D" style="display:<?php echo (($dataInfo["DorO_flag"]=='D')?'block':'none'); ?>;">
							<label><span></span><?=$this->lang->line('vehicle_dealer')?></label>
							<select name="td_num" id='td_num' style="width:240px;"  onchange="change_sub_num('td_num', 'tsd_num');">
									<?=create_select_option($dealerList, $dataInfo["do_num"], $this->lang->line('select_dealer_class'));?>
							</select>
						</li>
						<li class="dorflag_D" style="display:<?php echo (($dataInfo["DorO_flag"]=='D')?'block':'none'); ?>;">
							<label><span></span><?=$this->lang->line('vehicle_sub_dealer')?></label>
							<select name="tsd_num" id='tsd_num' style="width:240px;">
									<?=create_select_option($sub_dealer_list, $dataInfo["dso_num"], $this->lang->line('select_subdealer_class'), array("td_num"));?>
							</select>
						</li>
						<li class="line2">
							<label><span></span><?=$this->lang->line('vehicle_status')?></label>
							<select name="postdata[status]" id='status' style="width:240px;" onchange="change_dorflag(this.value)">
								<?php
									$ck1 = "";
									$ck2 = "";
									$ck3 = "";
									$ck4 = "";
									$ck5 = "";
									$ck6 = "";
									$ck7 = "";
									switch($dataInfo['status'])
									{
										case 1:
											$ck1 = "selected";
											break;
										case 2:
											$ck2 = "selected";
											break;
										case 3:
											$ck3 = "selected";
											break;
										case 4:
											$ck4 = "selected";
											break;
										case 5:
											$ck5 = "selected";
											break;
										case 6:
											$ck5 = "selected";
											break;
										case 7:
											$ck5 = "selected";
											break;
									}
								?>
								<option value='1' <?=$ck1?>><?=$this->lang->line('vehicle_status1')?></option>
								<option value='2' <?=$ck2?>><?=$this->lang->line('vehicle_status2')?></option>
								<option value='3' <?=$ck3?>><?=$this->lang->line('vehicle_status3')?></option>
								<option value='4' <?=$ck4?>><?=$this->lang->line('vehicle_status4')?></option>
								<option value='5' <?=$ck5?>><?=$this->lang->line('vehicle_status5')?></option>
								<option value='6' <?=$ck6?>><?=$this->lang->line('vehicle_status6')?></option>
								<option value='7' <?=$ck7?>><?=$this->lang->line('vehicle_status7')?></option>
							</select>
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_unit_id')?></label>
							<input type="text" class="txt" name="postdata[unit_id]" id="unit_id" maxlength="20" style="width:240px;" value="<?=$dataInfo['unit_id']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_code')?></label>
							<input type="text" class="txt" name="postdata[vehicle_code]" id="vehicle_code" maxlength="12" style="width:240px;" value="<?=$dataInfo['vehicle_code']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_manufacture_date')?></label>
							<input type="date" class="txt" name="postdata[manufacture_date]" id="manufacture_date" style="width:240px;" value="<?=$dataInfo['manufacture_date']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_license_plate_number')?></label>
							<input type="text" class="txt" name="postdata[license_plate_number]" id="license_plate_number" maxlength="8" style="width:240px;" value="<?=$dataInfo['license_plate_number']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_engine_number')?></label>
							<input type="text" class="txt" name="postdata[engine_number]" id="engine_number" maxlength="25" style="width:240px;" value="<?=$dataInfo['engine_number']?>">
						</li>
						<li>
							<label class="required"><span></span><?=$this->lang->line('vehicle_car_machine_id')?></label>
							<input type="text" class="txt validate[required], ajax[ajax_pk_car_machine_id, <?=$dataInfo['s_num']?>]" style="width:240px;" name="postdata[car_machine_id]" id="car_machine_id" maxlength="10" style="width:100px;" value="<?=$dataInfo['car_machine_id']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_model')?></label>
							<input type="text" class="txt" name="postdata[vehicle_model]" id="vehicle_model" maxlength="1" style="width:240px;" value="<?=$dataInfo['vehicle_model']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_color')?></label>
							<input type="text" class="txt" name="postdata[vehicle_color]" id="vehicle_color" maxlength="1" style="width:240px;" value="<?=$dataInfo['vehicle_color']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_4g_ip')?></label>
							<input type="text" class="txt" name="postdata[4g_ip]" id="4g_ip" maxlength="15" style="width:240px;" value="<?=$dataInfo['4g_ip']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_bluetooth_name')?></label>
							<input type="text" class="txt" name="postdata[bluetooth_name]" id="bluetooth_name" maxlength="20" style="width:240px;" value="<?=$dataInfo['bluetooth_name']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_lora_sn')?></label>
							<input type="text" class="txt" name="postdata[lora_sn]" id="lora_sn" maxlength="20" style="width:240px;" value="<?=$dataInfo['lora_sn']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_user_id')?></label>
							<input type="text" class="txt" name="postdata[vehicle_user_id]" id="vehicle_user_id" maxlength="10" style="width:240px;" value="<?=$dataInfo['vehicle_user_id']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_type')?></label>
							<input type="radio" name="postdata[vehicle_type]" id="vehicle_typeB" class="radio first" value="B" <?echo (($dataInfo["vehicle_type"]=='B')?'checked':''); ?>><?=$this->lang->line('vehicle_typeB')?>
							<input type="radio" name="postdata[vehicle_type]" id="vehicle_typeM" class="radio" value="M" <?echo (($dataInfo["vehicle_type"]=='M')?'checked':''); ?>><?=$this->lang->line('vehicle_typeM')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li class="line2"> 
							<label><span></span><?=$this->lang->line('vehicle_use_type')?></label>
							<input type="radio" name="postdata[use_type]" id="vehicle_use_typeN" class="radio first" value="N" <?echo (($dataInfo["use_type"]=='N')?'checked':''); ?>><?=$this->lang->line('vehicle_use_typeN')?>
							<input type="radio" name="postdata[use_type]" id="vehicle_use_typeS" class="radio" value="S" <?echo (($dataInfo["use_type"]=='S')?'checked':''); ?>><?=$this->lang->line('vehicle_use_typeS')?>
							<input type="radio" name="postdata[use_type]" id="vehicle_use_typeP" class="radio" value="P" <?echo (($dataInfo["use_type"]=='P')?'checked':''); ?>><?=$this->lang->line('vehicle_use_typeP')?>
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_charge')?></label>
							<input type="radio" name="postdata[vehicle_charge]" id="vehicle_chargeY" class="radio first" value="Y" <?echo (($dataInfo["vehicle_charge"]=='Y')?'checked':''); ?>><?=$this->lang->line('vehicle_chargeY')?>
							<input type="radio" name="postdata[vehicle_charge]" id="vehicle_chargeN" class="radio" value="N" <?echo (($dataInfo["vehicle_charge"]=='N')?'checked':''); ?>><?=$this->lang->line('vehicle_chargeN')?>
							<input type="radio" name="postdata[vehicle_charge]" id="vehicle_chargeS" class="radio" value="S" <?echo (($dataInfo["vehicle_charge"]=='S')?'checked':''); ?>><?=$this->lang->line('vehicle_chargeS')?>
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_charge_count')?></label>
							<input type="text" class="txt" name="postdata[charge_count]" id="charge_count" maxlength="7" style="width:240px;">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_latitude')?></label>
							<input type="text" class="txt" name="postdata[latitude]" id="latitude" style="width:240px;" value="<?=$dataInfo['latitude']?>">
						</li>
						<li>
							<label><span></span><?=$this->lang->line('vehicle_longitude')?></label>
							<input type="text" class="txt" name="postdata[longitude]" id="longitude" style="width:240px;" value="<?=$dataInfo['longitude']?>">
						</li>
					</ul>
				</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

//经销商事件
//if($j("#sd_num").val()!="")
//{
//	var td_num = $j("#td_num").val();
//	var tsd_num = "";<dataInfo['tsd_num']>
//	getsubdealerselect(td_num,tsd_num);
//}

//经销商事件
//$j("#sd_num").change(function(){
//	getsubdealerselect($j(this).val());
//});

//function getsubdealerselect(td_num,tsd_num = ""){
//     $j.ajax({
//		type:'post',
//		url: '<?=$web?>device/vehicle/select_sub_dealer',
//		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',td_num:td_num,tsd_num:tsd_num},
//		async:false,
//		error: function(xhr) {
//			strMsg += 'Ajax request发生错误[background.php]\n请重试';
//			alert(strMsg);
//		},
//		success: function (response) {
//			$j('#tsd_num').html(response);
//		}
//	}) ;
//}

function change_dorflag(field_val){
	//$j(".dorflag_2").css('display','none');
	//$j(".dorflag_4").css('display','none');
	//$j(".dorflag_"+field_val).css('display','block');
}

function changeDorflag(open_filed, close_filed){
	$j(".dorflag_O").css('display','none');
	$j(".dorflag_D").css('display','none');
	$j(".dorflag_"+open_filed).css('display','block');
}


function change_sub_num(filed_1, filed_2){
	var filed_val = $j("#"+filed_1).val();
	
	
	if(filed_val == ''){
		$j("#"+filed_2+" option").show();
	}else{
		$j("#"+filed_2+ "option:selected").removeAttr("selected");
		$j("#"+filed_2+" option").hide();
		$j("#"+filed_2+" option["+filed_1+"='"+filed_val+"']").show();

		$j("#"+filed_2+" option[value='']").show();
		$j("#"+filed_2+" option[value='']").prop('selected', true);
	}
}

</script>
<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"gps_file"				=> "",
		"gps_content"	=> ""
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		
		<p class="btitle"><?=$this->lang->line('gps_manage_management')?><span><?=$show_sub_title;?></span></p>
		
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for=""><span></span><?=$this->lang->line('gps_content')?></label>
							<input type="text" class="txt" id="gps_content" name="postdata[gps_content]" value="<?=$dataInfo["gps_content"]?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for=""  class="required"><span></span>上传<?=$this->lang->line('gps_file')?></label>
							<input class="txt" type="text" id="file_txt" name="file_txt" class="tb validate[required]" value="<?=$dataInfo['gps_file']?>" readonly>
							<input type="file" onchange="$j('#file_txt').val($j('#gps_file').val());" name="gps_file" id="gps_file" class="imgbtn" style="display:none;">
<?
							if($dataInfo['s_num'] == ''){
?>
								<input type="button" value="浏览" id="id_gps_file" class="btn">
<?
							}	
?>
						</li>
					</ul>
				</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		var gps_file = $j("#gps_file").val();
		var strFileName=gps_file.replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi,"$1");  //正则表达式获取文件名，不带后缀
		var FileExt=gps_file.replace(/.+\./,"");   //正则表达式获取后缀
		var flag = 'N';
		var s_num = "<?=$dataInfo['s_num']?>";
		var filename = strFileName+'.'+FileExt;

		if(s_num == ''){
			if(gps_file == ''){
				alert('檔案必須上傳！');
			}else{
				//檢查檔案有沒有重複或不存在
				$j.ajax({
					type:'post',
					url: '<?= $web ?>device/gps_manage/check_file_name',
					async :false,
					data: { 
							s_num: '<?=$dataInfo['s_num']?>',
							filename: filename,
							<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
					error: function(xhr) {
						strMsg = '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
						alert(strMsg);
					},
					success: function (response) {
						if(response == 'N'){
							alert("檔案名稱重覆！");
						}else{
							flag = 'Y';
						}
					}
				}) ;
			}
		}else{
			flag = 'Y';
		}

		if(flag == 'Y'){
			$j("#formID").submit();
		}
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#id_gps_file").click(function(){
	$j("#gps_file").click();
});
</script>
<?
	$web = $this->config->item('base_url');

	$dataname = array('DorO_flag','sd_num','so_num','sv_num','battery_id','manufacture_date','position','status');


	$web = $this->config->item('base_url');
	$colArr = array (
		"DorO_flag"			=>	"{$this->lang->line('battery_DorO_flag')}",
		"sd_num"			=>	"{$this->lang->line('battery_dealer')}",
		"so_num"			=>	"{$this->lang->line('battery_operator')}",
		"sv_num"			=>	"{$this->lang->line('battery_M30')}",
		"battery_id"		=>	"{$this->lang->line('battery_id')}",
		"manufacture_date"	=>	"{$this->lang->line('battery_manufacture_date')}",
		"position"			=>	"{$this->lang->line('battery_position')}",
		"status"			=>	"{$this->lang->line('battery_status')}",
		"sys_no"			=>	"{$this->lang->line('sys_no')}"
	);
	$colInfo = array("colName" => $colArr);

	$dor_name = array(""=>"", "D"=>"经销商", "O"=>"营运商");
	$get_full_url_random = get_full_url_random();
	//匯入网址
	$upload_url2 = "{$get_full_url_random}/upload";

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<code>
		<span class="topbtn">
			<a href="#" id="fileReturn" class="buttonblack sback">返回</a>
			<a id="fileUpload" class="buttonblack simport">汇入</a>
		</span>
		<p class="btitle"><?=$this->lang->line('battery_management_upload')?></p>
		</code>
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="battery_import">
				<tr>
					<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
					<th>序号</th>
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//栏位显示
		                echo "<th align=\"center\">
								<a fieldname='{$enName}'>$chName</a>
							  </th>\n";
		            }
		?>
				</tr>
		<?
			$num=1;
			foreach ($InfoRow as $key => $InfoArr) {
				echo "<tr>\n" ;
				$checkStr = "<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$num}\" >" ;
				if($InfoArr['cart_flag'] == 'N'){
					$checkStr = '';
				}

				echo '<td align="center">'.$checkStr.'</td>' ;
				echo "<td align=\"center\">".$num."</td>\n" ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					echo "<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
				}
				echo "</tr>\n" ;
				$num++;
			}
			
		?>
			</table>
		</form>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//功能按钮显示控制
	button_display();
});


/*返回*/
$j("#fileReturn").click( function () {
	history.back()
});

/*
**上傳
*/
$j("#fileUpload").click( function () {
	var selcount = getCkbVal();
	if(selcount!=''){
		if ( confirm("是否要將勾選清單匯入？") ) {
			$j("#listForm").attr( "method", "POST" ) ;
			$j("#listForm").attr( "action", "<?=$upload_url2?>" ) ;
			$j("#listForm").submit() ;
		}
	}
});


</script>

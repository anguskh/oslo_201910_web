<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"DorO_flag"						=>  "{$this->lang->line('DorO_flag')}",
		"do_name"						=>  "{$this->lang->line('vehicle_dealer')}/{$this->lang->line('vehicle_operator')}",
		"dso_name"					=>  "{$this->lang->line('vehicle_sub_dealer')}/{$this->lang->line('vehicle_sub_operator')}",
		"vehicle_code"				=>	"{$this->lang->line('vehicle_code')}",
		"license_plate_number"		=>	"{$this->lang->line('vehicle_license_plate_number')}",
		"car_machine_id"			=>	"{$this->lang->line('vehicle_car_machine_id')}",
		"unit_id"					=>	"{$this->lang->line('vehicle_unit_id')}",
		"status"					=>	"{$this->lang->line('vehicle_status')}",
		"map"					=>	"地图"
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	//匯入网址
	$upload_url = "{$get_full_url_random}/check_upload";


	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"top.top01"		=>  "{$this->lang->line('select_operator_class')}",
		"td.tde01"		=>  "{$this->lang->line('select_dealer_class')}",
		"license_plate_number"		=>  "{$this->lang->line('vehicle_license_plate_number')}",
		"car_machine_id"		=>  "{$this->lang->line('vehicle_car_machine_id')}",
		"unit_id"		=>  "{$this->lang->line('vehicle_unit_id')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;
	$positionRow= array("B"=>"BSS", "V"=>"电动机车");

	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"top.top01"			=>  $operatorsSelectListRow,
		"td.tde01"			=>  $dealerSelectListRow
	);

	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$dor_name = array(""=>"", "D"=>"经销商", "O"=>"营运商");
?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
	<div class="topper">
		<p class="btitle"><?=$this->lang->line('vehicle_management')?></p>
		<div class="topbtn column">
			<div class="normalsearch">
				<input type="text" class="txt searchData" id="s_search_txt" placeholder="车架号码" value="<?=$s_search_txt;?>">
				<input type="button" id="btClear2" class="clear" value="清 空" />
				<input type="button" id="btSearch2" class="search" value="搜 寻" />
			</div>
			<div class="btnbox">
				<?=$user_access_control?>
				<a data-fancybox data-src="#hidden-content-1" href="javascript:;" class="buttoninActive simport"><i class="fas fa-file-import"></i></a>
				<!-- Search Starts Here -->
				<div id="searchContainer">
					<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>고급<?=$this->lang->line('search')?></span></a>
					<div id="searchBox">                
						<form id="searchForm" method="post" action="<?=$search_url?>">
							<!--预防CSRF攻击-->
							<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
							<fieldset2 id="body">
								<fieldset2>
									<?=$search_box?>
								</fieldset2>
							</fieldset2>
							<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="vehicle">
				<tr class="GridViewScrollHeader">
					<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
							
		<?
					//让标题栏位有排序功能
		            foreach ($colInfo["colName"] as $enName => $chName) {
						//排序动作
						$url_link = $get_full_url_random;
						$order_link = "";
						$orderby_url = $url_link."/index?field=".$enName."&orderby=";
						if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
							$symbol = '▲';
							$next_orderby = "desc";
						}else{
							$symbol = '▼';
							$next_orderby = "asc";
						}
						if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
							$orderby_url .= $next_orderby;
							$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
						}else{
							$orderby_url .= "asc";
						}
						
						//栏位显示
		                echo "					<th align=\"center\">
														<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
														{$order_link}
												</th>\n";
		            }
		?>
				</tr>
		<?
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr class=\"GridViewScrollItem\">\n" ;

				$data_sn = $InfoArr["s_num"] ;
				$checkStr = "
		<td>
			<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$data_sn}\" >
		</td>
				" ;
				echo $checkStr ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					if ($enName == "status") {
						switch($InfoArr[$enName]) {
							case "1":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('vehicle_status1')}</font></td>\n" ;
								break;
							case "2":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('vehicle_status2')}</font></td>\n" ;
								break;
							case "3":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('vehicle_status3')}</font></td>\n" ;
								break;
							case "4":
								echo "					<td align=\"center\"><font color=blue>{$this->lang->line('vehicle_status4')}</font></td>\n" ;
								break;
							case "5":
								echo "					<td align=\"center\"><font color=red>{$this->lang->line('vehicle_status5')}</font></td>\n" ;
								break;
							case "6":
								echo "					<td align=\"center\"><font color=red>{$this->lang->line('vehicle_status6')}</font></td>\n" ;
								break;
							case "7":
								echo "					<td align=\"center\"><font color=red>{$this->lang->line('vehicle_status7')}</font></td>\n" ;
								break;
						}
					}else if($enName == "DorO_flag"){
						echo "					<td align=\"left\">".$dor_name[$InfoArr[$enName]]."</td>\n" ;

					}else if($enName == "map"){
						$main_startrow = $this->session->userdata('PageStartRow');
						echo '<td align="left" id="noclick"><a onclick="btn_map('.$data_sn.');" class="map"></a></td>';
					}else {
						echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>
			</table>
		</form>
		<!-- 分页区 -->
		<div class="pagebar">
			<?=$pageInfo["html"]?>
		</div>
	</div>
</div>
<div style="display: none;" id="hidden-content-1" class="import_box">
	<form id="upload_list" action="<?=$upload_url?>" method="post" enctype="multipart/form-data" target="_parent">
		<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
		<p>匯入檔案<span>(<a href="<?=$web?>excel/vehicle_example.xls">下載範例檔</a>)</span></p>
		<input type="text" class="txt" id="upload_file_txt" value="">
		<input type="file" name="upload_file" onchange="checkFile(this, 'upload_file');" id="upload_file" style="display:none;">
		<input type="button" value="浏览" id="id_upload_file" class="btn" value="瀏覽">
		<button type="submit" class="send">送出</button>
	</form>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

function btn_map(sn){
	//亂數
	var random_url = create_random_url();

	mywin=window.open("","detail_map","left=100,top=100,width=1050,height=500"); 
	mywin.location.href = '<?=$web?>device/vehicle__'+random_url+'/detail_map/'+sn;
}

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

$j("#id_upload_file").click(function(){
	$j("#upload_file").click();
});


function checkFile(file, file_id) { 
	var value = file.value; 
	var isCheckImageType = true;  //是否检查副档名 
	var re = /\.(xls)$/i;  //允许的图片副档名 
	if (isCheckImageType && !re.test(value)) { 
		alert("只允许上传xls的文件檔"); 
		$j('#'+file_id+'_txt').val('');
	} else { 
		$j('#'+file_id+'_txt').val($j('#'+file_id).val());
	} 
} 

</script>

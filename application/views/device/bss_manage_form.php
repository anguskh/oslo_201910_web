<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"bss_file"			=> "",
		"bss_version_date"	=> "",
		"bss_version_no"	=> "",
		"bss_content"		=> "",
		"up_flag"			=> "0",
		"group_sb_num"		=> "",
		"all_sb_num"		=> "",
		"checksum"		=> ""
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}

.sol-option {
    float: left;
    padding: 5px 10px;
    display: block;
}
</style>
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
<!-- 下拉搜尋 多筆勾選 -->
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('bss_manage_management')?><span><?=$show_sub_title;?></span></p>		
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="section container">
					<div class="container-box" style="width:20%;">
						<ul>
							<li>
								<label for=""><span></span><?=$this->lang->line('bss_content')?></label>
								<input type="text" class="txt" id="bss_content" name="postdata[bss_content]" value="<?=$dataInfo["bss_content"]?>">
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('bss_version_no')?></label>
								<input type="text" class="txt" id="bss_version_no" name="postdata[bss_version_no]" value="<?=$dataInfo["bss_version_no"]?>">
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('bss_version_date')?></label>
								<input type="date" class="txt" id="bss_version_date" name="postdata[bss_version_date]" value="<?=$dataInfo["bss_version_date"]?>">
							</li>
						</ul>
					</div>		
					<div class="container-box" style="width:20%;">
						<ul>
							<li>
								<label for=""  class="required"><span></span>上传<?=$this->lang->line('bss_file')?></label>
								<input class="txt" type="text" id="file_txt" name="file_txt" class="tb validate[required]" style="width:110px;" value="<?=$dataInfo['bss_file']?>" readonly>
								<input type="file" onchange="$j('#file_txt').val($j('#bss_file').val());" name="bss_file" id="bss_file" class="imgbtn" style="display:none;">
<?
								if($dataInfo['s_num'] == ''){
?>
									<input type="button" value="浏览" id="id_bss_file" class="btn">
<?
								}	
?>
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('up_flag')?></label>
								<input type="checkbox" id="up_flag" name="postdata[up_flag]" value="1" <?echo (($dataInfo["up_flag"]=='1')?'checked':'')?>>
							</li>
							<li>
								<label for=""><span></span><?=$this->lang->line('bss_checksum')?></label>
								<input type="text" class="txt" id="checksum" name="postdata[checksum]" value="<?=$dataInfo["checksum"]?>">
							</li>
						</ul>
					</div>		
					<div class="container-box" style="width:28%; padding:0 2% 0 0;">
						<ul>
							<li style="margin:0;">
								<label for=""><span></span>租借站群组</li>
								<select multiple="multiple" id="group_sb_num" name="group_sb_num[]" style="width: 385px;">
	<?
									$all_arr = explode(',',$dataInfo['group_sb_num']);
									foreach($batterygroupList as $i_arr){
										$selected = "";
										if(in_array($i_arr['s_num'], $all_arr)){
											$selected = "selected";
										}
										echo '<option value="'.$i_arr['s_num'].'" '.$selected.'>'.$i_arr['group_name'].'</option>';
									}
	?>
								</select>	
							</li>
						</ul>
						<div id="current-selection-group_sb_num" class="sol-current-selection">
						</div>
					</div>
					<div class="container-box" style="width:28%; padding:0 0 0 2%;">					
						<ul>
							<li style="margin:0;">
								<label for=""><span></span>租借站</li>
								<select multiple="multiple" id="all_sb_num" name="all_sb_num[]" style="width: 385px;">
	<?
									$all_arr = explode(',',$dataInfo['all_sb_num']);
									foreach($batteryswapstationList as $i_arr){
										$selected = "";
										if(in_array($i_arr['s_num'], $all_arr)){
											$selected = "selected";
										}
										echo '<option value="'.$i_arr['s_num'].'" '.$selected.'>'.$i_arr['bss_id_name'].'</option>';
									}
	?>
								</select>
							</li>	
						</ul>
						<div id="current-selection-all_sb_num" class="sol-current-selection">
						</div>
					</div>
				</div>
			</form>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		var bss_file = $j("#bss_file").val();
		var strFileName=bss_file.replace(/^.+?\\([^\\]+?)(\.[^\.\\]*?)?$/gi,"$1");  //正则表达式获取文件名，不带后缀
		var FileExt=bss_file.replace(/.+\./,"");   //正则表达式获取后缀
		var flag = 'N';
		var s_num = "<?=$dataInfo['s_num']?>";
		var filename = strFileName+'.'+FileExt;

		if(s_num == ''){
			if(bss_file == ''){
				alert('檔案必須上傳！');
			}else{
				//檢查檔案有沒有重複或不存在
				$j.ajax({
					type:'post',
					url: '<?= $web ?>device/bss_manage/check_file_name',
					async :false,
					data: { 
							s_num: '<?=$dataInfo['s_num']?>',
							filename: filename,
							<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
					error: function(xhr) {
						strMsg = '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
						alert(strMsg);
					},
					success: function (response) {
						if(response == 'N'){
							alert("檔案名稱重覆！");
						}else{
							flag = 'Y';
						}
					}
				}) ;
			}
		}else{
			flag = 'Y';
		}

		if(flag == 'Y'){
			$j("#formID").submit();
		}
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#id_bss_file").click(function(){
	$j("#bss_file").click();
});
//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
//alert($j.fn.jquery);  //顯示目前$j是誰在用

var arr = new Array('group_sb_num','all_sb_num');

var mutiselectObjArr = new Array();
var y = 0;

arr.forEach(function(value) {
	mutiselectObjArr[y] = $j_1102('#'+value).searchableOptionList({
		showSelectAll: true,
		maxHeight:'700px',
		events: {
			maxwidth: '1100', //設定寬度
			nowId: value
		},
		texts: {
			noItemsAvailable: '查无此资料',
			selectNone: '清除选择',
			selectAll: '全选',
			searchplaceholder: '选择租借站'
		}
	});

	y++;
});

$j_1102(window).load(function(){
	// do{
		$j_1102('.sol-option').css('width','20%');
	// }while ($j_1102('.sol-option').length==0)
});
</script>
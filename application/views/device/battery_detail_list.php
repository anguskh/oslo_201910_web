<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"unit_id"				=>	"{$this->lang->line('log_battery_leave_return_tv_num')}",
		"member_name"			=>	"{$this->lang->line('log_battery_leave_return_tm_num')}",
		"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
		"ldo_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_do_num')}",
		"ldso_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_dso_num')}",
		"leave_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_leave_sb_num_name')}",
		"battery_user_id"		=>	"{$this->lang->line('log_battery_leave_return_battery_user_id')}",
		"leave_request_date"	=>	"{$this->lang->line('log_battery_leave_return_leave_request_date')}",
		"leave_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_leave_coordinate')}",
		"leave_date"			=>	"{$this->lang->line('log_battery_leave_return_leave_date')}",
		"leave_track_no"		=>	"{$this->lang->line('log_battery_leave_return_leave_track_no')}",
		"leave_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_leave_battery_id')}",
		"leave_status"			=>	"{$this->lang->line('log_battery_leave_return_leave_status')}",
		"rdo_name"				=>	"{$this->lang->line('log_battery_leave_return_return_do_num')}",
		"rdso_name"				=>	"{$this->lang->line('log_battery_leave_return_return_dso_num')}",
		"return_bss_id"			=>	"{$this->lang->line('log_battery_leave_return_return_sb_num_name')}",
		"return_request_date"	=>	"{$this->lang->line('log_battery_leave_return_return_request_date')}",
		"return_coordinate"		=>	"{$this->lang->line('log_battery_leave_return_return_coordinate')}",
		"return_date"			=>	"{$this->lang->line('log_battery_leave_return_return_date')}",
		"return_track_no"		=>	"{$this->lang->line('log_battery_leave_return_return_track_no')}",
		"return_battery_id"		=>	"{$this->lang->line('log_battery_leave_return_return_battery_id')}",
		"return_status"			=>	"{$this->lang->line('log_battery_leave_return_return_status')}",
		"usage_time"			=>	"{$this->lang->line('log_battery_leave_return_usage_time')}",
		"charge_amount"			=>	"{$this->lang->line('log_battery_leave_return_charge_amount')}",
		"trail"					=>  "{$this->lang->line('log_battery_trail')}"
	);
	
	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/detail_search";

	$searchArr = array (
		"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
		"ltop.top01"	=>  "(借){$this->lang->line('select_operator_class')}",
		"ltd.tde01"		=>  "(借){$this->lang->line('select_dealer_class')}",
		"rtop.top01"	=>  "(还){$this->lang->line('select_operator_class')}",
		"rtd.tde01"		=>  "(还){$this->lang->line('select_dealer_class')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"system_log_date"	=>	"date",
		"ltop.top01"	=>  $operatorsSelectListRow,
		"ltd.tde01"		=>  $dealerSelectListRow,
		"rtop.top01"	=>  $operatorsSelectListRow,
		"rtd.tde01"		=>  $dealerSelectListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$exchange_type_name = array('离线交换','连线交换');
	$status_name = array('充电中','饱电');
	
	//echo 'A:'.$this->session->userdata('start_date');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>
<link href="<?=$web.$newcss;?>newcss/fixedtable.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('battery_detail_management')?> <?=$battery_id;?></p>
			<div class="topbtn column">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="姓名" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								<INPUT TYPE="hidden" NAME="battery_sn" id="battery_sn" value="<?=$battery_sn;?>">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
			
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				
<?
				$show_err = '';
				$span_arr = array();
				$start_row = $this->session->userdata('PageStartRow');
				//让标题栏位有排序功能
				foreach ($colInfo["colName"] as $enName => $chName) {
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/detail/".$battery_sn."/".$start_row."/".$main_startRow."?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center style='display: inline-block;'><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
				   /* echo "					<th align=\"center\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";*/

					$span_arr[$enName] = "<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>{$order_link}";
				}
				

				$table_list1 = '
					<table class="table table-striped table-bordered table-hover table-condensed fixed-column fixedtb">';

				$table_list1 .= '
						<thead>
						  <tr>
							<th><div class="fixwb">'.$span_arr['unit_id'].'</div></th>
							<th><div class="fixwbx">'.$span_arr['member_name'].'</div></th>
						  </tr>
						  <tr>
							<th><div class="fixwb">'.$span_arr['system_log_date'].'</div></th>
							<th><div class="fixwbx">'.$span_arr['battery_user_id'].'</div></th>
						  </tr>
						</thead>
						<tbody>';

				$table_list2 = '
					<table class="table table-striped table-bordered table-hover table-condensed fixedtb">';
				$table_list2 .= '
						<thead>
							<tr>
								<th><div class="fixwb">'.$span_arr['unit_id'].'</div></th>
								<th><div class="fixwbx">'.$span_arr['system_log_date'] .'</div></th>
								<th><div class="fixw">'.$span_arr['ldo_name'] .'</div></th>
								<th><div style="width:600px">'.$span_arr['leave_bss_id'] .'</div></th>	
								<th><div class="fixw">'.$span_arr['leave_request_date'] .'</div></th>
								<th><div class="fixw">'.$span_arr['leave_coordinate'] .'</div></th>
								<th><div class="fixw">'.$span_arr['leave_track_no'] .'</div></th>
								<th><div class="fixw">'.$span_arr['rdo_name'] .'</div></th>
								<th><div class="fixw">'.$span_arr['return_request_date'] .'</div></th>
								<th><div class="fixw">'.$span_arr['leave_status'] .'</div></th>
								<th><div class="fixwb">'.$span_arr['return_track_no'] .'</div></th>
								<th><div class="fixwb">'.$span_arr['usage_time'] .'</div></th>
								<th><div class="fixwb">'.$span_arr['trail'] .'</div></th>
							</tr>
							<tr>
								<th><div class="fixwb">'.$span_arr['member_name'] .'</div></th>
								<th><div class="fixwbx">'.$span_arr['battery_user_id'] .'</div></th>
								<th><div class="fixw">'.$span_arr['ldso_name'] .'</div></th>
								<th><div style="width:600px">'.$span_arr['return_bss_id'] .'</div></th>
								<th><div class="fixw">'.$span_arr['leave_date'] .'</div></th>
								<th><div class="fixw">'.$span_arr['return_coordinate'] .'</div></th>
								<th><div class="fixw">'.$span_arr['leave_battery_id'] .'</div></th>
								<th><div class="fixw">'.$span_arr['rdso_name'].'</div></th>
								<th><div class="fixw">'.$span_arr['return_date'].'</div></th>
								<th><div class="fixw">'.$span_arr['return_status'] .'</div></th>
								<th><div class="fixwb">'.$span_arr['return_battery_id'].'</div></th>
								<th><div class="fixwb">'.$span_arr['charge_amount'].'</div></th>
								<th><div class="fixwb"></div></th>
							</tr>
						</thead>
						<tbody>';
?>	
<?
				if(isset($InfoRow)){
					$s = 1;
					foreach ($InfoRow as $key => $patternfilefieldInfoArr) {
						$class = (($s/2==0)?' class="even"':'');
						if($patternfilefieldInfoArr['gps_path_track']!="")
						{
							$trail = "<td class=\"track\" ><a class=\"icon_track\" onclick=\"btn_map({$patternfilefieldInfoArr['s_num']});\"></a></td>\n";
						}
						else
						{
							$trail = "<td></td>";
						}
						$table_list1 .= '
							
								<tr>
									<td>'.$patternfilefieldInfoArr['tv_num'] .'</td>
									<td>'.$patternfilefieldInfoArr['member_name'] .'</td>
								</tr>
								<tr>
									<td>'.$patternfilefieldInfoArr['system_log_date'] .'</td>
									<td>'.$patternfilefieldInfoArr['battery_user_id'] .'</td>
								</tr>
							';
							
							$data = "";
							if($patternfilefieldInfoArr['leave_status'] != "")
							{
								switch($patternfilefieldInfoArr['leave_status'])
								{
									case 0:
										$data = "成功";
										break;
									case 1:
										$data = "机柜还电门未打开";
										break;
									case 2:
										$data = "用户未放入电池";
										break;
									case 3:
										$data = "放入非借出电池";
										break;
									case 4:
										$data = "取电门未打开";
										break;
									case 5:
										$data = "用户未取出电池";
										break;
									case "F":
										$data = "使用微信绑定";
										break;
								}

								$patternfilefieldInfoArr['leave_status'] = $data;
							}
							$data = "";
							if($patternfilefieldInfoArr['return_status'] != "")
							{
								switch($patternfilefieldInfoArr['return_status'])
								{
									case 0:
										$data = "成功";
										break;
									case 1:
										$data = "机柜还电门未打开";
										break;
									case 2:
										$data = "用户未放入电池";
										break;
									case 3:
										$data = "放入非借出电池";
										break;
									case 4:
										$data = "取电门未打开";
										break;
									case 5:
										$data = "用户未取出电池";
										break;
									case "F":
										$data = "使用微信绑定";
										break;
								}

								$patternfilefieldInfoArr['return_status'] = $data;
							}

						$table_list2 .= '
							<tr '.$class.'>
								<td></td>
								<td></td>
								<td>'.$patternfilefieldInfoArr['ldo_name'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_bss_id'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_request_date'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_coordinate'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_track_no'].'</td>
								<td>'.$patternfilefieldInfoArr['rdo_name'].'</td>
								<td>'.$patternfilefieldInfoArr['return_request_date'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_status'].'</td>
								<td>'.$patternfilefieldInfoArr['return_track_no'].'</td>
								<td>'.$patternfilefieldInfoArr['usage_time'].'</td>
								'.$trail.'
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td>'.$patternfilefieldInfoArr['ldso_name'].'</td>
								<td>'.$patternfilefieldInfoArr['return_bss_id'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_date'].'</td>
								<td>'.$patternfilefieldInfoArr['return_coordinate'].'</td>
								<td>'.$patternfilefieldInfoArr['leave_battery_id'].'</td>
								<td>'.$patternfilefieldInfoArr['rdso_name'].'</td>
								<td>'.$patternfilefieldInfoArr['return_date'].'</td>
								<td>'.$patternfilefieldInfoArr['return_status'].'</td>
								<td>'.$patternfilefieldInfoArr['return_battery_id'].'</td>
								<td>'.$patternfilefieldInfoArr['charge_amount'].'</td>
								<td></td>
							</tr>';
						$s++;
					}
				}else{
					$show_err = '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

				$table_list1 .= '</tbody></table>';
				$table_list2 .= '</tbody></table>';
			
			if($show_err == ''){
				echo '<div class="table-responsive fixedbox">';
				echo $table_list1;
				echo $table_list2;
				echo '</div>';
			}else{
				echo $show_err;
			}
?>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>

			<a href="<?=$get_full_url_random.'/index/'.$main_startRow?>" class="backbtn">回上一页</a>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">
	$j(document).ready(function() {
		$j('a.close, #modal').live('click', function() {
			// close the overlay
			triggersOnload.eq(0).overlay().close();
			return false;
		});
		//凍結窗格
		gridview(0,false);
	});

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();

function btn_map(sn){
	//亂數
	var random_url = create_random_url();

	mywin=window.open("","路线轨迹","left=100,top=100,width=1050,height=500"); 
	mywin.location.href = '<?=$web?>device/battery__'+random_url+'/detail_map_trail/'+sn;

}

</script>
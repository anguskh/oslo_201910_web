<?
	$web = $this->config->item('base_url');
	//echo 'sess11:'.$this->session->userdata('session_sn');
	// $colInfo = is_array($colInfo) ? $colInfo : array() ;
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

/* 测试 Session
    $session = $this->db->get('log_sessions')->result_array();
    foreach ($session as $sessions) {
        $sessio = $sessions['last_activity'] + 7200;
        echo $sessio . "time<BR>";
        $custom_data = $sessions['user_data'];
		$custom_data = @unserialize(strip_slashes($custom_data));
		print_r($custom_data);
    }
*/
	if (	$this->session->userdata('default_language') == "zh_tw" || 
			( 	$this->session->userdata('default_language') == "" && 
				$this->session->userdata('display_language') == "zh_tw"	)
		) //中文
	{
		$merchant_merchant_name_title = $this->lang->line('merchant_merchant_name_chinese');
	}else{	//英文
		$merchant_merchant_name_title = $this->lang->line('merchant_merchant_name_english');
	}	
	
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

	//比对更新密码日期, 只要小于现在日期就强制换密码
	date_default_timezone_set("Asia/Taipei");
	$pwd_deadline = $this->session->userdata("pwd_deadline");
	//$pwd_deadline = date('Y-m-d', strtotime($pwd_deadline));

	$pwd_deadline = new DateTime($pwd_deadline);
	$pwd_deadline = $pwd_deadline->format('Y-m-d');
	$now = date('Y-m-d');
	
	$change = true;
	//echo $pwd_deadline;
	
	if( $pwd_deadline < $now){
		$change = true;
	}else{
		$change = false;
	}
	
	if($this->session->userdata('login_side') == "0"){	//前台登入
		$merchant_title = $this->lang->line('last_10_rows_of_modify_merchant_data');
	}else{
		$merchant_title = $this->lang->line('last_10_rows_of_modify_merchant');
	}

	//echo '日期:'.$this->session->userdata("pwd_deadline");
	//解析我的最爱资料
	//$favor = explode(',', $favor);
	//print($favor);
	//echo $this->session->userdata("login_force_pwd");


	$show_menu_list = 'Y';
	$show_detail = 'N';
	$login_side = $this->session->userdata('login_side');
	$to_flag = $this->session->userdata('to_flag');
	$card_flag = $this->session->userdata('card_flag');
	$login_to_num = $this->session->userdata('to_num');

	if($to_flag == 1 && $card_flag == 1){
		//前台權限，只能看到自己的營運商資料
		$show_menu_list = 'N';
		$show_detail = 'Y';
	}

	if($show_menu_list == 'Y'){
?>
    <div class="wrapper">
		<ul class="menulist">
<?
			foreach($menuInfoRow as $menu_arr){
				if($menu_arr['menu_directory'] != '#' && $menu_arr['main_flag'] == 1){

					$menu_directory = $menu_arr["menu_directory"];
					$class_fn = setAttrUrl($menu_directory);
					echo '<li>
									<a href="'.$web.$menu_directory.'/" '.$class_fn.'></a>
									<p class="title">'.$menu_arr["menu_name"].'</p>
									<p>'.$menu_arr["menu_content"].'</p>
									<span>
										'.(($menu_arr['menu_image'] != '')
											?'<img src="'.$web.'img/shortcut/'.$menu_arr['menu_image'].'" alt="">'
											:'').'
									</span>
								</li>';
				}
			}
?>
		</ul>
    </div>
<?
	
	}
	if($show_detail == 'Y'){
?>
	<!--功能按钮显示控制 start-->
	<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
	<!--功能按钮显示控制 end-->
	<link href="<?=$web.$newcss;?>newcss/style.css" rel="stylesheet" type="text/css">
	<!-- Bootstrap Js CDN -->
	<script src="<?=$web;?>newjs/gauge.min.js"></script>
	<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="<?=$web.$newcss;?>newcss/bootstrap.min.css">
	<script src="<?=$web;?>newjs/bootstrap.min.js"></script>
	<script language="javascript">
		//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
		var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
		//alert($j.fn.jquery);  //顯示目前$j是誰在用
	</script>
	<div id="dashboard">
		<div id="content">
			<div class="container2">
				<div class="row">

					<div class="col-lg-3">
						<div class="card2 green">
							<p>
								總預購次數
								<strong><?=number_format($op_statistics[0]);?></strong>
							</p>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="card2 orange">
							<p>
								已使用預購次數
								<strong><?=number_format($op_statistics[1]);?></strong>
							</p>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="card2 blue">
							<p>
								剩餘預購次數
								<strong><?=number_format($op_statistics[2]);?></strong>
							</p>
						</div>
					</div>
					<div class="col-lg-3">
						<div class="card2 red">
							<p>
								流通電池數（尚未歸還數）
								<strong><?=number_format($op_statistics[3]);?></strong>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
						<div class="col-lg-12">
							<div class="container-default">
								<h2 class="col-lg-12">
									<i class="glyphicon glyphicon-map-marker"></i>目前位置地图
								</h2>
								<table class="cardinfo">
									<thead>
										<tr>
											<td>卡片名称</td>
											<td>qrcode内容</td>
											<td>电池序号</td>
											<td>地图</td>
										</tr>
									</thead>
									<tbody>
<?
										if($op_statistics[3]>0){
											foreach($op_statistics[4] as $key=>$arr){
												echo '<tr>
														<td>'.$arr['member_name'].'</td>
														<td>'.$arr['user_id'].'</td>
														<td>'.$arr['leave_battery_id'].'</td>
														<td>'.(($arr['battery_s_num'] != '')?'<a onclick="btn_map('.$arr['battery_s_num'].');" class="map"></a>':'').'</td>
													</tr>';
											
											}
										}					
?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="container-default">
							<h2 class="col-lg-12">
								<i class="glyphicon glyphicon-stats"></i>卡片使用排行
								<div class="time-section">
									<select id="date_type" name="date_type" style="width: 60px;  height:32px; background:#fff; border: 1px solid #ccc;" onchange="change_date(this.value);">
										<option value="1">自訂</option>
										<option value="2">週</option>
										<option value="3">月</option>
										<option value="4">年</option>
									</select>
									<span id="date_type_1">
										<input type="date" id="date_start" name="date_start">
										<input type="date" id="date_end" name="date_end">
									</span>
									<span id="date_type_2" style="display:none;">
										<input type="week" id="date_week" name="date_week">
									</span>
									<span id="date_type_3" style="display:none;">
										<input type="month" id="date_month" name="date_month">
									</span>
									<span id="date_type_4" style="display:none;">
										<input type="input" id="date_year" name="date_year" value="<?echo date("Y");?>"  style="width: 60px;  height:32px; background:#fff; border: 1px solid #ccc;" maxlength="4">
									</span>
									<a style="cursor:pointer" id="btSearchCard" class="send">查詢</a>
								</div>
							</h2>
							<div id="canvas-holder2" style="display: block; width:80%; margin: 0 auto;">
								<canvas id="canvas2"></canvas>
							</div>
<?
							$k_labels_arr = array();
							$k_labels_all_arr = array();
							$k_color = array();
							$val_arr = array();
							foreach($op_statistics[5] as $key=>$val){
								$k_labels_arr[] = (($val['member_name'] == '')?$key:$val['member_name']);
								$val_arr[] = $val['cnt'];
							}
							
							$k_labels = join( "','", $k_labels_arr );
							$k_labels = "'".$k_labels."'";

							$vals = join( "','", $val_arr );
							$vals = "'".$vals."'";
?>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="container-default">
							<h2 class="col-lg-12">
								<i class="glyphicon glyphicon-time"></i>
								卡片各時段使用排行
							</h2>

<?
							$xAxes_arr = array();
							$a4_arr = array();
							foreach($op_statistics[6] as $key=>$val){
								$xAxes_arr[] = $key;
								$a4_arr[] = $val;
							}
							$xAxes = join( "','", $xAxes_arr );
							$xAxes = "'".$xAxes."'";

							$a4 = join( "','", $a4_arr );
							$a4 = "'".$a4."'";
?>							
							<div id="canvas-holder" style="display: inline-block; width:80%;">
								<canvas id="canvas"></canvas>
							</div>

						</div>
					</div>   
				</div>
			</div>
		</div>
	</div>
<?
										
									
	}									
?>

	<a data-fancybox data-src="#editfancy" class="edit" href="javascript:;" id="click_force_pwd"></a>
	<div style="display: none;" id="editfancy">
		<div id="edit_label" class="edit_label">
			
		</div>
	</div>

<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});

// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <= k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}
	return false;
});


//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
}

function remove_a(){
	$j("#prompt a").remove();
}

<?
	if($this->session->userdata('login_side') == "1"){	//后端登入, 右边tab宽度修正
?>
	$j("#accordion .pane").css({
		'height': '455px'
	});
<?
	}
?>

//强制更改密码
<?
	if($this->session->userdata("login_force_pwd") == '1' || $change == true){
?>
		$j("#click_force_pwd").fancybox({
			closeClickOutside: false
		});
		$j('#click_force_pwd').click();
		//alert("<?=$this->session->userdata("login_force_pwd")?>");
		$j.ajax({
			type:'post',
			url: '<?=$web?>nimda/password/change_password',
			data: {
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',
					'change_type':'main'
				  },
			error: function(xhr) {
				strMsg = '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
				alert(strMsg);
			},
			success: function (response) {
				$j("#editfancy").show();
                $j('#edit_label').html(response);
				$j(".fancybox-close-small").remove();
			}
		}) ;

		//移除关闭
		//setTimeout("remove_a()",300);
<?
	}else{
		$change_access = $this->session->userdata('change_access');
		if($auto_change_access == 'Y' && $change_access == ''){
			//写入session
			$this->session->set_userdata('change_access', 'Y');
?>
			window.location.href = '<?=$web;?>monitor/monitor_alarm_online'; // 跳转到B目录

<?
		
		}
		
	}
?>

//如果没有任何我的最爱捷径, 则提示使用者
function fn_favor_tip(){
	var cpanel_IE = $j("#cpanel_IE");
	var count = cpanel_IE.find(".icon").length;

	if(count == 0){
		cpanel_IE.html('<div id="favor_tip"><?=$this->lang->line('favor_tip')?></div>');
	}
}
</script>
<!--我的最爱拖曳排序-->
<script>
	var obj = '';
	var tdindex = '';
	var trindex = '';
	var maxtdindex = '';
	var maxtrindex = '';
	var clonetd = '';
	//拖曳开始
    function dragstartHandler(event) {
		obj = $j(event.currentTarget);
		//复制一份拖曳之td出来
		clonetd=$j(event.currentTarget).clone();
		tdindex = $j(event.currentTarget).index();
		trindex = $j(event.currentTarget).parent().index();
		maxtdindex = parseInt($j(event.currentTarget).parent().children("td").length)-1;
		maxtrindex = parseInt($j(event.currentTarget).parent().parent().children("tr").length)-1;
    }
	//拖曳目标
    function dragoverHandler(event) {
        event.preventDefault();
    }
	
	//拖曳结束之动作
    function dropHandler(event) {
		//取得拖曳目标
		var targetobj = $j(event.currentTarget);
		var tmphtml = $j(event.currentTarget).html();
		//若拖曳目标td与原本拖曳之td为不同之td才执行动作
		if(obj.html()!=tmphtml)
		{
			clonetd.attr("colspan","1");
			//将拖曳之td加在目标td之前
			$j(event.currentTarget).before(clonetd);
			//判断是否在同一列
			if(trindex == targetobj.parent().index())
			{
				//若拖曳td为此列最后一个或比目标td还后面时则tdindex+1
				if(tdindex == maxtdindex || tdindex >= targetobj.index())
					tdindex = parseInt(tdindex)+1;
				$j('#template').find('tr').eq(trindex).find('td').eq(tdindex).remove();	
			}
			else
			{
				$j('#template').find('tr').eq(trindex).find('td').eq(tdindex).remove();	
				//拖曳td与目标td不同列时判断拖曳td是否为最后一个，若是则删除拖曳td之tr
				if(maxtdindex==0)
					$j('#template_table').find('tr').eq(trindex).remove();
			}
			//重新将所有template的td加上拖曳动作以及getid异动作	
			getdataformat();
		}
    }
	//将所有template的td加上拖曳动作以及getid异动作	
	getdataformat();
	function getdataformat(){
		tmpdata = new Array();
		var getid = 0;
		$j('#template td').each(function(){
			$j(this).attr("getid",getid);
			$j(this).attr("ondragover","dragoverHandler(event)");
			$j(this).attr("ondrop","dropHandler(event)");
			tmpdata.push($j(this).attr("menu_sn"));
			getid++;
		});
		if(tmpdata.length > 0){
			$j.ajax({
				type:'post',
				url: '<?= $web ?>nimda/menu/addfavor',
				data: {arr_menu_sn: tmpdata, 
						<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
				async:false, 
				error: function(xhr) {
					strMsg += 'Ajax request发生错误[background.php]\n请重试';
					alert(strMsg);
				},
				success: function (response) {
					// alert("aa");
					$j('#cpanel_IE.favorarea').html(response);
					tmpdata = new Array();
					getid = 0;
					$j('#template td').each(function(){
						$j(this).attr("getid",getid);
						$j(this).attr("ondragover","dragoverHandler(event)");
						$j(this).attr("ondrop","dropHandler(event)");
						tmpdata.push($j(this).attr("menu_sn"));
						getid++;
					});
				}
			}) ;
		}
	}
</script>
<!--我的最爱拖曳排序-->
<?
	if($show_detail == 'Y'){
?>
	<script type="text/javascript" src="<?=$web?>js/chart/Chart.bundle.js"></script> 
	<script type="text/javascript" src="<?=$web?>js/chart/utils.js"></script> 
	<script>
		var color = Chart.helpers.color;
		var barChartData = {
			labels: [<?=$k_labels;?>],
			datasets: [{
				label: '卡片使用排行',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
					<?=$vals;?>
				]
			},]

		};
		var config = {
			type: 'line',
			data: {
				labels: [<?=$xAxes?>],
				datasets: [{
					label: '換電站電池平均時段交換次數',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						<?=$a4?>
					],
					fill: false,
				}, ]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: ''	//上方title
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: ''	//橫title
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: ''	//緃title
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);

			var ctx2 = document.getElementById('canvas2').getContext('2d');
			window.myBar = new Chart(ctx2, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: ''
					}
				}
			});

		};
		//window.myLine.update(); 更新

		function btn_map(sn){
			//亂數
			var random_url = create_random_url();

			mywin=window.open("","detail_map","left=100,top=100,width=1050,height=500"); 
			mywin.location.href = '<?=$web?>device/battery__'+random_url+'/detail_map/'+sn;

		}

		//匯出卡片資料
		$j("#btSearchCard").click(function(){
			if($j("#date_type").val() == 2){
				getweek($j("#date_week").val());
			}
			var date_type = $j("#date_type").val();
			var date_month = $j("#date_month").val();
			var date_year = $j("#date_year").val();
			var date_start = $j("#date_start").val();
			var date_end = $j("#date_end").val();

			$j.ajax({
				type:'post',
				dataType: "json",
				async: false,
				url: '<?= $web ?>nimda/menu/ChangeCardList',
				data: {
						date_type:date_type,
						date_month:date_month,
						date_year:date_year,
						date_start:date_start,
						date_end:date_end,
						<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'
					  },
				error: function(xhr) {
					strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
					alert(strMsg);
				},
				success: function (response) {
					$j('#canvas-holder2').html('').html('<canvas id="canvas2"></canvas>');
					var barChartData = {
						labels: [],
						datasets: [{
							label: '卡片使用排行',
							backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
							borderColor: window.chartColors.red,
							borderWidth: 1,
							data: [
							]
						},]

					};
					$j.each(response[0], function( index, value ) {
						barChartData.labels.push(value);
					});
					$j.each(response[1], function( index, value ) {
						barChartData.datasets[0].data.push(value);
					});
					
					var ctx2 = document.getElementById('canvas2').getContext('2d');
					window.myBar = new Chart(ctx2, {
						type: 'bar',
						data: barChartData,
						options: {
							responsive: true,
							legend: {
								position: 'top',
							},
							title: {
								display: true,
								text: ''
							}
						}
					});
					window.myBar.update(); 
				}
			}) ;
		});
	</script>
<?
}	
?>

<script>
	function getweek(date_week){
       var getvalue = date_week;
       var yearweekArr = getvalue.split("-");
       var year = yearweekArr[0];
       var week = yearweekArr[1].substring(1);
       // alert(yearweekArr[0]);
       dateRange(year,week);
       // alert(getvalue);
   	}

	function getNowFormatDate(theDate) 
	{ 
		var day = theDate; 
		var Year = 0; 
		var Month = 0; 
		var Day = 0; 
		var CurrentDate = ""; 
		// 初始化时间 
		Year= day.getFullYear();// ie火狐下都可以 
		Month= day.getMonth()+1; 
		Day = day.getDate(); 
		CurrentDate += Year + "-"; 
		if (Month >= 10 ) 
		{ 
			CurrentDate += Month + "-"; 
		} 
		else 
		{ 
			CurrentDate += "0" + Month + "-"; 
		} 
		if (Day >= 10 ) 
		{ 
			CurrentDate += Day ; 
		} 
		else 
		{ 
			CurrentDate += "0" + Day ; 
		} 
		return CurrentDate; 
	} 

	function isInOneYear(_year,_week){ 
		if(_year == null || _year == '' || _week == null || _week == ''){ 
			return true; 
		} 
		var theYear = getXDate(_year,_week,4).getFullYear(); 
			if(theYear != _year){ 
			return false; 
		} 
		return true; 
	} 

	// 获取日期范围显示 
	function getDateRange(_year,_week){ 
		var beginDate; 
		var endDate; 
		if(_year == null || _year == '' || _week == null || _week == ''){ 
			return ""; 
		} 
		beginDate = getXDate(_year,_week-1,1); 
		endDate = getXDate(_year,(_week - 1 + 1),7); 
		$j('#date_start').val(getNowFormatDate(beginDate));
		$j('#date_end').val(getNowFormatDate(endDate));
		//return getNowFormatDate(beginDate) + " 至 "+ getNowFormatDate(endDate); 
	} 

	// 这个方法将取得某年(year)第几周(weeks)的星期几(weekDay)的日期 
	function getXDate(year,weeks,weekDay){ 
		// 用指定的年构造一个日期对象，并将日期设置成这个年的1月1日 
		// 因为计算机中的月份是从0开始的,所以有如下的构造方法 
		var date = new Date(year,"0","1"); 

		// 取得这个日期对象 date 的长整形时间 time 
		var time = date.getTime(); 

		// 将这个长整形时间加上第N周的时间偏移 
		// 因为第一周就是当前周,所以有:weeks-1,以此类推 
		// 7*24*3600000 是一星期的时间毫秒数,(JS中的日期精确到毫秒) 
		time+=(weeks-1)*7*24*3600000; 

		// 为日期对象 date 重新设置成时间 time 
		date.setTime(time); 
			return getNextDate(date,weekDay); 
		} 
		// 这个方法将取得 某日期(nowDate) 所在周的星期几(weekDay)的日期 
		function getNextDate(nowDate,weekDay){ 
		// 0是星期日,1是星期一,... 
		weekDay%=7; 
		var day = nowDate.getDay(); 
		var time = nowDate.getTime(); 
		var sub = weekDay-day; 
		if(sub <= 0){ 
			sub += 7; 
		} 
		time+=sub*24*3600000; 
		nowDate.setTime(time); 
		return nowDate; 
	}

	//日期处理 
	function dateRange(year,week){ 
		// var _year = $("#_year").val(); 
		// var _week = $("#_week").val(); 
		if(isInOneYear(year,week)){ 
			var showDate = getDateRange(year,week); 
			//alert(showDate);
		} else{ 
			//alert(year+"年无"+week+"周，请重新选择"); 
		} 
	}

	function change_date(date_type){
		$j("#date_type_1").css('display','none');
		$j("#date_type_2").css('display','none');
		$j("#date_type_3").css('display','none');
		$j("#date_type_4").css('display','none');
		$j("#date_type_"+date_type).css('display','inline');
	}

</script>

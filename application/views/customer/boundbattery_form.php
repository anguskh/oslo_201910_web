<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"					=>			"",
		"system_log_date"		=>			"",
		"leave_sb_num"			=>			"",
		"tm_num"				=>			"",
		"leave_date"			=>			"",
		"leave_track_no"		=>			"",
		"leave_battery_id"		=>			""
	);

	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
?>
<script src="<?=$web?>js/jquery.tools.min.js"></script>
<script type="text/javascript">
//將jquery.tools.min.js 的jquery 改為$auto, 避免與原始版本的jquery衝突
var $auto = jQuery.noConflict();
</script>
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<!-- 下拉搜尋 多筆勾選 -->
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>
<!-- 可輸入可下拉 -->
<link rel="stylesheet" href="<?=$web?>js/easyui/easyui.css"/>
<script type="text/javascript" src="<?=$web?>js/easyui/jquery.easyui.min.js"></script> 
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>

<!-- script src="<?=$web?>js/jquery.twzipcode.js"></script -->
<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
		
			<p class="btitle"><?=$this->lang->line('boundbattery_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title"></p>
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<INPUT TYPE="hidden" NAME="webs" id="webs" value="<?=$web;?>">
			<div class="section">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box col-md-6">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('log_battery_leave_return_tm_num')?></label>
							<select name="postdata[tm_num]" id='tm_num' style="width:450px;" onchange="check_member(this.value);">
								<option value=""></option>
								<?
									foreach($memberList as $arr){
										echo '<option value="'.$arr['s_num'].'">'.$arr['name'].'（'.$arr['user_id'].'）</option>';
									}
								?>
							</select>
						</li>
					</ul>

				</div>
				<div class="container-box col-md-6" id="div_content">
					<div class="div_panel" style="display:none;">
					<ul>
						<input type="hidden"orgname="seq_no" name="seq_no_1" id="seq_no_1" value="1">
						<li>
							<label for=""><?=$this->lang->line('log_battery_leave_return_leave_sb_num')?></label>
							<select orgname="leave_sb_num" name="leave_sb_num_1" id='leave_sb_num_1' style="width:450px;">
								<option value=""></option>
<?
								foreach($batteryswapstationList as $key => $val){
									echo '<option value="'.$val['s_num'].'" val_name="'.$val['bss_id_name'].'">'.$val['bss_id_name'].'</option>';
								}
?>
							</select>
						</li>
						<li>
							<label for=""><?=$this->lang->line('log_battery_leave_return_leave_battery_id')?></label>
							<select orgname="leave_battery_id" name="leave_battery_id_1" id='leave_battery_id_1' style="width:240px;">
								<?=create_select_option($batteryList, $dataInfo["leave_battery_id"]);?>
							</select>
						</li>

					</ul>
					</div>
				</div>
				<INPUT TYPE="hidden" NAME="key_num" id="key_num" value="1">
				<div class="container-box col-md-6" id="show_error">
				</div>

			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
		show_combobox("#leave_battery_id_1",250);
		show_combobox("#leave_sb_num_1",450);
		show_combobox("#tm_num",450, '', '','Y');
	});

	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>
<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
function check_member(member_sn){
	//檢查該會員目前可綁定幾顆電池
	var strMsg = '';
	var ok_count = 2;
	parseInt(ok_count);
	if(member_sn != ''){
		 $j.ajax({
			type:'post',
			dataType: "json",
			url: '<?=$web?>customer/bound_battery/select_battery_count',
			data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',member_sn:member_sn},
			async:false,
			error: function(xhr) {
				strMsg += 'Ajax request发生错误[background.php]\n请重试';
				alert(strMsg);
			},
			success: function (response) {
				var b_count = parseInt(response[0]);
				var b_list = response[1];
				if(b_count < ok_count){
					if($j("#key_num").val() != b_count){
						//清空後面的
						$j( ".div_2" ).remove();
					}
					$j("#div_content").css("display","inline");
					$j("#show_error").html('');
					for(i=b_count; i<ok_count; i++){
						if(i == b_count){
							//把第一個打開
							$j("#div_content .div_panel").css("display","inline");
						}else{
							$j("#div_content .div_panel").eq(0).clone(true).appendTo("#div_content");
							var last_seq = $j("#div_content").find("[orgname=seq_no]:last").val();
							new_setno = parseInt(last_seq)+1;
							$j("#div_content .div_panel").last().attr("class", "div_"+new_setno);
							
							var obj_new = $j("#div_content .div_"+new_setno);
							resetName("div_content", "div_", new_setno, "input");
							resetName("div_content", "div_", new_setno, "select");
							$j(".div_"+new_setno).find('.combo').remove();

							show_combobox("#leave_battery_id_"+new_setno,250);
							show_combobox("#leave_sb_num_"+new_setno,450);
							$j("#seq_no_2").val(new_setno);
							$j("#key_num").val(new_setno);
						}
					}
					$j("#btSave").css("display","inline-block");
					if(b_count > 0){
						$j("#show_error").html('目前已綁定'+b_count+'顆電池<br>'+b_list);
					}

				}else{
					//目前無可綁定電池
					$j("#div_content").css("display","none");
					$j("#show_error").html('目前已綁定'+b_count+'顆電池<br>'+b_list);
					$j("#btSave").css("display","none");
					//序號回復
					var parent = 'div_content';
					//取得最後一個序號
					var last_seq = $j("#"+parent).find("[orgname=seq_no]:last").val();
					parseInt(last_seq);

					for(i=last_seq; i>1; i--){
						$j(".div_"+i).remove();
					}
				}
			}
		}) ;
	}
}
function resetName(parent, child, setno, type){
	var obj_div = $j("#"+parent+" ."+child+setno);
	var all_input = obj_div.find(type);

	all_input.each(function(){
		var orgname = $j(this).attr("orgname");
		var type = $j(this).attr("type");
		// console.log(orgname);
		if(type == 'radio'){
			$j(this).attr("id", orgname+'_'+setno);
			$j(this).val(setno);
		}else{
			var new_name = orgname+'_'+setno;
			$j(this).attr("name", new_name);
			$j(this).attr("id", new_name);
		}
	});
}

</script>
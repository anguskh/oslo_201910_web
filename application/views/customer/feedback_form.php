<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"					=>			"",
		"tm_num"				=>			"",
		"log_date"				=>			"",
		"question_item"			=>			"",
		"question_additional"	=>			"",
		"coordinate"			=>			"",
		"status"				=>			""
	);

	$feedbackInfo = isset($feedbackInfo) ? $feedbackInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $feedbackInfo["s_num"]);
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

?>
<script type="text/javascript" src="<?=$web?>js/cityselectcn/distpicker.js"></script>

<!-- script src="<?=$web?>js/jquery.twzipcode.js"></script -->
<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('feedback_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title"></p>
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<INPUT TYPE="hidden" NAME="webs" id="webs" value="<?=$web;?>">
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$feedbackInfo['s_num']?>" />
				<div class="container-box" style="width: 30%;">
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('tm_num')?></label>
							<select name="postdata[tm_num]" id="tm_num" class="validate[required]" >
								<?=create_select_option($member_list, $feedbackInfo["tm_num"], $this->lang->line('select_member_class'));?>
							</select>
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" id="status" name="postdata[status]" value="N" <?=(($feedbackInfo["status"] == 'N')?'checked':'')?>><?=$this->lang->line('status_N')?>
							<input type="radio" class="radio validate[required]" id="status" name="postdata[status]"value="P"<?=(($feedbackInfo["status"] == 'P')?'checked':'')?>><?=$this->lang->line('status_P')?>
							<input type="radio" class="radio validate[required]" id="status" name="postdata[status]"value="S"<?=(($feedbackInfo["status"] == 'S')?'checked':'')?>><?=$this->lang->line('status_S')?>
						</li>
						<li>
							<label for=""><?=$this->lang->line('coordinate')?></label>
							<input type="text" class="txt" id="coordinate" name="postdata[coordinate]" value="<?=$feedbackInfo["coordinate"]?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width: 45%;">
					<ul>
						<li>
<?
							$log_date = "";
							if($feedbackInfo["log_date"] != ''){
								$log_date = substr($feedbackInfo["log_date"],0,10)."T".substr($feedbackInfo["log_date"],11,8);
							}
?>
							<label for="" class="required"><?=$this->lang->line('log_date')?></label>
							<input type="datetime-local" class="validate[required]" id="log_date" name="postdata[log_date]" value="<?=$log_date ?>">
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('question_item')?></label>
							<INPUT TYPE="radio" id="question_item" NAME="postdata[question_item]" value="1" class="radio first validate[required]" <?=(($feedbackInfo["question_item"] == '1')?'checked':'')?>><?=$this->lang->line('question_item_1')?>
							<INPUT TYPE="radio" id="question_item" NAME="postdata[question_item]" value="2" class="radio validate[required]" <?=(($feedbackInfo["question_item"] == '2')?'checked':'')?>><?=$this->lang->line('question_item_2')?>
							<INPUT TYPE="radio" id="question_item" NAME="postdata[question_item]" value="3" class="radio validate[required]" <?=(($feedbackInfo["question_item"] == '3')?'checked':'')?>><?=$this->lang->line('question_item_3')?>
							<INPUT TYPE="radio" id="question_item" NAME="postdata[question_item]" value="4" class="radio validate[required]" <?=(($feedbackInfo["question_item"] == '4')?'checked':'')?>><?=$this->lang->line('question_item_4')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width: 25%;">
					<ul>
						<li class="auto">
							<label for="" class="required"><?=$this->lang->line('question_additional')?></label>
							<textarea name="postdata[question_additional]" id="question_additional" cols="4" maxlength="100" rows="25" class="small" style="width:100%; height:110px;"><?=$feedbackInfo['question_additional']?></textarea>
						</li>	
					</ul>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	//储存
	function fn_save(){
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>

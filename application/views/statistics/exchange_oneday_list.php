<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"bss_id"		=>	"換電站地点名称<br />（換電站序号）",
		"log_date"		=>  "日期",
		"daySUMNum"		=>  "交换次数"
	);

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	//匯出网址
	$upload_url = "{$get_full_url_random}/check_download";

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"leave_date"	=>  "系统纪录时间",
		"show_limit"	=>	"顯示幾名",
		"tbss.bss_id"	=>	"測試機櫃<br />(逗號區格)",
		"tbss.district"			=>	"區域(逗號區格)"
	);
	$fieldType = array(
		"leave_date"		=>  'date',
		"show_limit"		=>  'limit',
		"tbss.bss_id"		=>  'onok',
		"tbss.district"		=>  'onok'
	);

	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	
	$search_date_range = $chart_list[1]."～".$chart_list[2];

	$fn = get_fetch_class_random();
	$search = $this->session->userdata("{$fn}_".'searchData');
	$key_num = "";
	if ( !empty( $search) ) $key_num = setSearchLimit($search);
	$key_num = (($key_num=="")?10:$key_num);
?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('exchange_oneday_management')?><span id="search_date_range"><?=$search_date_range;?></span></p>
			<div class="topbtn">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="电池換電站序号" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		

<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<div style="width: 85%; margin: 0 auto; overflow-x:auto;">
					<div id="canvas-holder" style="width:<?=(($key_num>20)?'150':'100')?>%;">
						<canvas id="canvas2"></canvas>
					</div>
				</div>
				<div style="width: 85%; margin: 0 auto; overflow-x:auto;">
					<div id="canvas-holder" style="width:<?=(($key_num>20)?'150':'100')?>%;">
						<canvas id="canvas3"></canvas>
					</div>
				</div>
				<!-- div style="height:450px; text-align: center;">
				</div -->

				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_info">
				<tr class="GridViewScrollHeader">
<?
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				$n=1;
				
				$no_list = '';
				foreach ($colInfo["colName"] as $enName => $chName) {
					
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" scope=\"col\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}

?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $InfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == 'bss_id'){
							
								echo "					<td align=\"center\" class=\"\" >".$InfoArr['location']."（" . $InfoArr[$enName] . "）</td>\n";

							}else{
							
								echo "					<td align=\"center\" class=\"\" >" . $InfoArr[$enName] . "</td>\n";
							}
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
                            '.$no_list.'
                        </tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

?>
			</table>



		</form>
		<!-- 分页区 -->
		<div class="pagebar">
			<?=$pageInfo["html"]?>
		</div>
	</div>
</div>
<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);

<?
	if(!empty($searchData)){
?>
		
		$j("#search_date_range").html($("input[name='searchData_0_0']" ).val() + "～"+$("input[name='searchData_0_1']" ).val());

<?
	}	
?>

});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

$j("#id_upload_file").click(function(){
	$j("#upload_file").click();
});

</script>
<script type="text/javascript" src="<?=$web?>js/chart/Chart.bundle.js"></script> 
<script type="text/javascript" src="<?=$web?>js/chart/utils.js"></script> 
<?
	// $color = array('red','orange','yellow','green','blue','purple','grey','#FF00FF','#191970','#000000');
	$color = array('#FF0000','#FF4500','#FFFF00','#32CD32','#0000CD','#800080','#A9A9A9','#FF00FF','#191970','#000000');
	$k_labels_arr = array();
	$k_labels_all_arr = array();
	$k_color = array();
	$val_arr = array();
	foreach($chart_list[10] as $key=>$val){
		if($val['location'] != ''){
			$strlen = mb_strlen($val['location']);
			$val['location'] = mb_substr($val['location'],0,20).'\n'.mb_substr($val['location'],20,$strlen);
		}

		//$k_labels_arr[] = (($val['bss_id'] == '')?$key:$val['bss_id']);
		$k_labels_arr[] = (($val['location'] == '')?$key:$val['location']);
		$k_labels_all_arr[] = (($val['location'] == '')?$key:$val['location'].'（'.$val['bss_id'].'）');
		$val_arr[] = $val['dayMaxNum'];
		// $k_color[] = 'window.chartColors.'.$color[rand(0,6)];
	}
	
	$k_labels = join( "','", $k_labels_arr );
	$k_labels = "'".$k_labels."'";

	$k_labels_all = join( "','", $k_labels_all_arr );
	$k_labels_all = "'".$k_labels_all."'";

	$vals = join( "','", $val_arr );
	$vals = "'".$vals."'";

	$kin_labels_arr = array();
	$kin_labels_all_arr = array();
	$kin_color = array();
	$valin_arr = array();

	foreach($chart_list[11] as $key=>$val){
		if($val['location'] != ''){
			$strlen = mb_strlen($val['location']);
			$val['location'] = mb_substr($val['location'],0,20).'\n'.mb_substr($val['location'],20,$strlen);
		}

		//$kin_labels_arr[] = (($val['bss_id'] == '')?$key:$val['bss_id']);
		$kin_labels_arr[] = (($val['location'] == '')?$key:$val['location']);
		$kin_labels_all_arr[] = (($val['location'] == '')?$key:$val['location'].'（'.$val['bss_id'].'）');
		$valin_arr[] = $val['dayMaxNum'];
		$kin_color[] = 'window.chartColors.'.$color[rand(0,6)];
	}
	
	$kin_labels = join( "','", $kin_labels_arr );
	$kin_labels = "'".$kin_labels."'";

	$kin_labels_all = join( "','", $kin_labels_all_arr );
	$kin_labels_all = "'".$kin_labels_all."'";

	$valin = join( "','", $valin_arr );
	$valin = "'".$valin."'";
?>
	<script>
		var color = Chart.helpers.color;
		var barChartData = {
			/*labels_all: [<?=$k_labels_all;?>],*/
			labels: [<?=$k_labels_all;?>],
			datasets: [{
				label: '電池交换次数',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
					<?=$vals;?>
				]
			},]

		};
		var barChartData2 = {
			/*labels_all: [<?=$kin_labels_all;?>],*/
			labels: [<?=$kin_labels_all;?>],
			datasets: [{
				label: '電池交换次数',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				borderWidth: 1,
				data: [
					<?=$valin;?>
				]
			},]

		};

		window.onload = function() {
			var ctx2 = document.getElementById('canvas2').getContext('2d');
			window.myBar = new Chart(ctx2, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: '單一換電站單日最多電池交换次数'
					}
				},
				plugins: [{
				beforeInit: function (chart) {
					chart.data.labels.forEach(function (e, i, a) {
					if (/\n/.test(e)) {
					  a[i] = e.split(/\n/)
					}
				  })
				}
			  }]
			});


			var ctx3 = document.getElementById('canvas3').getContext('2d');
			window.myBar = new Chart(ctx3, {
				type: 'bar',
				data: barChartData2,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: '單一換電站單日最少電池交换次数'
					}
				},
				plugins: [{
				beforeInit: function (chart) {
					chart.data.labels.forEach(function (e, i, a) {
					if (/\n/.test(e)) {
					  a[i] = e.split(/\n/)
					}
				  })
				}
			  }]
			});

		};

	</script>
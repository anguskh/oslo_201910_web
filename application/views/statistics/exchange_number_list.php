<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"bss_id"		=>	"換電站地点名称<br />（換電站序号）",
		"exchange_num"	=>  "交换次数"
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	//匯出网址
	$upload_url = "{$get_full_url_random}/check_download";

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"lblr.leave_date"	=>  "系统纪录时间",
		"show_limit"			=>	"顯示幾名",
		"bss_id"			=>	"測試機櫃(逗號區格)",
		"district"					=>	"區域(逗號區格)"
	);
	$fieldType = array(
		"lblr.leave_date"		=>  'date',
		"show_limit"		=>  'limit',
		"bss_id"		=>  'onok',
		"district"				=>  'onok'
	);

	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$fn = get_fetch_class_random();
	$search = $this->session->userdata("{$fn}_".'searchData');
	$key_num = "";
	if ( !empty( $search) ) $key_num = setSearchLimit($search);
	$key_num = (($key_num=="")?10:$key_num);

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('exchange_number_management')?></p>
			<div class="topbtn">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="电池換電站序号" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
				 				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<div style="width: 85%; margin: 0 auto; overflow-x:auto;">
				<div id="canvas-holder" style="width:<?=(($key_num>20)?'150':'100')?>%;">
					<canvas id="canvas4"></canvas>
				</div>
			</div>
			<div style="width: 85%; margin: 0 auto; overflow-x:auto;">
				<div id="canvas-holder" style="width:<?=(($key_num>20)?'150':'100')?>%;">
					<canvas id="canvas5"></canvas>
				</div>
			</div>
			<!-- div style="height:450px; text-align: center;">
			</div -->
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_info">
				<tr class="GridViewScrollHeader">
	<?
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				$n=1;
				
				$no_list = '';
				foreach ($colInfo["colName"] as $enName => $chName) {
					
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" scope=\"col\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}

	?>
					</tr>
	<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $InfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == 'bss_id'){
								echo "					<td align=\"center\" class=\"\" >".$InfoArr['location']."（" . $InfoArr[$enName] . "）</td>\n";
							}else{
								echo "					<td align=\"center\" class=\"\" >" . $InfoArr[$enName] . "</td>\n";
							}
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
							'.$no_list.'
						</tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

	?>
			</table>

		</form>
		<!-- 分页区 -->
		<div class="pagebar">
			<?=$pageInfo["html"]?>
		</div>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

$j("#id_upload_file").click(function(){
	$j("#upload_file").click();
});

</script>
<script type="text/javascript" src="<?=$web?>js/chart/Chart.bundle.js"></script> 
<script type="text/javascript" src="<?=$web?>js/chart/utils.js"></script> 

<?

	foreach($chart_list[12] as $key => $value)
	{
		if($key<$key_num)
		{
			if($value['location'] != ''){
				$strlen = mb_strlen($value['location']);
				$value['location'] = mb_substr($value['location'],0,20).'\n'.mb_substr($value['location'],20,$strlen);
			}

			//$pie_max_labels_arr[$key] = (($value['bss_id'] == '')?$key:$value['bss_id']);
			$pie_max_labels_arr[$key] = (($value['location'] == '')?$key:$value['location']);
			$pie_max_labels_all_arr[$key] = (($value['location'] == '')?$key:$value['location'].'（'.$value['bss_id'].'）');

			$pie_max_values_arr[$key] = $value['exchange_num'];
			//$pie_max_color_arr[$key] = $color[$key];
		}
	}

	$pie_max_labels = join( "','", $pie_max_labels_arr );
	$pie_max_labels = "'".$pie_max_labels."'";

	$pie_max_labels_all = join( "','", $pie_max_labels_all_arr );
	$pie_max_labels_all = "'".$pie_max_labels_all."'";

	$pie_max_values = join( ",", $pie_max_values_arr );
	//$pie_max_color = join( "','", $pie_max_color_arr );
	//$pie_max_color = "'".$pie_max_color."'";

	$min_excahnge_arr = array_reverse($chart_list[12]);
	foreach($min_excahnge_arr  as $key => $value)
	{
		if($key<$key_num)
		{
			//$pie_min_labels_arr[$key] = (($value['bss_id'] == '')?$key:$value['bss_id']);
			if($value['location'] != ''){
				$strlen = mb_strlen($value['location']);
				$value['location'] = mb_substr($value['location'],0,20).'\n'.mb_substr($value['location'],20,$strlen);
			}
			$pie_min_labels_arr[$key] = (($value['location'] == '')?$key:$value['location']);
			$pie_min_labels_all_arr[$key] = (($value['location'] == '')?$key:$value['location'].'（'.$value['bss_id'].'）');
			$pie_min_values_arr[$key] = $value['exchange_num'];
			//$pie_min_color_arr[$key] = $color[$key];
		}
	}
	sort($pie_min_values_arr);

	$pie_min_labels = join( "','", $pie_min_labels_arr );
	$pie_min_labels = "'".$pie_min_labels."'";

	$pie_min_labels_all = join( "','", $pie_min_labels_all_arr );
	$pie_min_labels_all = "'".$pie_min_labels_all."'";

	$pie_min_values = join( ",", $pie_min_values_arr );
	//$pie_min_color = join( "','", $pie_min_color_arr );
	//$pie_min_color = "'".$pie_min_color."'";
?>
	<script>

		var color = Chart.helpers.color;
		var barChartData = {
			/*labels_all: [<?=$pie_max_labels_all;?>],*/
			labels: [<?=$pie_max_labels_all;?>],
			datasets: [{
				label: '電池交换次数',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: [
					<?=$pie_max_values;?>
				]
			},]

		};
		var barChartData2 = {
			/*labels_all: [<?=$pie_min_labels_all;?>],*/
			labels: [<?=$pie_min_labels_all;?>],
			datasets: [{
				label: '電池交换次数',
				backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
				borderColor: window.chartColors.blue,
				borderWidth: 1,
				data: [
					<?=$pie_min_values;?>
				]
			},]

		};

		window.onload = function() {
			var ctx2 = document.getElementById('canvas4').getContext('2d');
			window.myBar = new Chart(ctx2, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: '單一換電站電池交换次数前<?=$key_num;?>名'
					}
				},
				plugins: [{
				beforeInit: function (chart) {
					chart.data.labels.forEach(function (e, i, a) {
					if (/\n/.test(e)) {
					  a[i] = e.split(/\n/)
					}
				  })
				}
			  }]
			});


			var ctx3 = document.getElementById('canvas5').getContext('2d');
			window.myBar = new Chart(ctx3, {
				type: 'bar',
				data: barChartData2,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: '單一換電站電池交换次数最後<?=$key_num;?>名'
					}
				},
				plugins: [{
				beforeInit: function (chart) {
					chart.data.labels.forEach(function (e, i, a) {
					if (/\n/.test(e)) {
					  a[i] = e.split(/\n/)
					}
				  })
				}
			  }]
			});
		};
	</script>
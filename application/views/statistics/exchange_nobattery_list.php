<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"bss_id"				=>	$this->lang->line('bss_id'),
		"log_date_start"		=>  $this->lang->line('log_date_start'),
		"log_date_end"			=>  $this->lang->line('log_date_end'),
		"minutes"				=>  $this->lang->line('minutes')
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	//匯出网址
	$upload_url = "{$get_full_url_random}/check_download";

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"log_date_start"	=>  "系统纪录时间"
	);
	$fieldType = array(
		"log_date_start"		=>  'date'
	);

	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');
	$search_date_range = $chart_list[1]."～".$chart_list[2];

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<div class="wrapper">
	<div class="box">
		<div class="topper">
			<p class="btitle"><?=$this->lang->line('exchange_nobattery_management')?><span id="search_date_range"><?=$search_date_range;?></span></p>
			<div class="topbtn column">
				<div class="normalsearch">
					<input type="text" class="txt searchData" id="s_search_txt" placeholder="电池換電站序号" value="<?=$s_search_txt;?>">
					<input type="button" id="btClear2" class="clear" value="清 空" />
					<input type="button" id="btSearch2" class="search" value="搜 寻" />
				</div>
				<div class="btnbox">
					<!-- Search Starts Here -->
					<div id="searchContainer">
						<a href="#" id="searchButton" class="buttoninActive"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
						<div id="searchBox">                
							<form id="searchForm" method="post" action="<?=$search_url?>">
								<!--预防CSRF攻击-->
								<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
								<fieldset2 id="body">
									<fieldset2>
										<?=$search_box?>
									</fieldset2>
								</fieldset2>
								<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- 列表区 -->
		<form id="listForm" name="listForm" action="">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<div style="margin: 0 auto;">
				<canvas id="canvas" height="150"></canvas>
			</div>
			<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="log_battery_info">
				<tr class="GridViewScrollHeader">
<?
				$show_err = '';
				$table_1_head = '';
				$table_2_head = '';
				$span_arr = array();
				//让标题栏位有排序功能
				$n=1;
				
				$no_list = '';
				foreach ($colInfo["colName"] as $enName => $chName) {
					
					//排序动作
					$url_link = $get_full_url_random;
					$order_link = "";
					$orderby_url = $url_link."/index?field=".$enName."&orderby=";
					if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
						$symbol = '▲';
						$next_orderby = "desc";
					}else{
						$symbol = '▼';
						$next_orderby = "asc";
					}
					if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
						$orderby_url .= $next_orderby;
						$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
					}else{
						$orderby_url .= "asc";
					}
					
					//栏位显示
					echo "					<th align=\"center\" scope=\"col\">
													<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
													{$order_link}
											</th>\n";
					$no_list .= '<td style="padding:0;border:0;"></td>'; 
				}

?>
					</tr>
<?
				if(isset($InfoRow)){
					foreach ($InfoRow as $key => $InfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == 'bss_id'){
								echo "					<td align=\"center\" class=\"\" >".$InfoArr['location']."（" . $InfoArr[$enName] . "）</td>\n";
							}else if($enName == 'minutes'){
								//$time = $InfoArr['minutes'] / 60 / 60;
								if( $InfoArr['minutes'] < 86400 ){//如果不到一天
									$time = gmstrftime('0天　%H:%M:%S',$InfoArr['minutes']);
								}else{
									 $time_tmp = explode(' ', gmstrftime('%j %H %M %S', $InfoArr['minutes']));
									 $time = ($time_tmp[0]-1).'天　'.$time_tmp[1].':'.$time_tmp[2].':'.$time_tmp[3];
								 }
								echo "					<td align=\"center\" class=\"\" >".$time."</td>\n";
							}else{
								echo "					<td align=\"center\" class=\"\" >" . $InfoArr[$enName] . "</td>\n";
							}
						}
						echo "				</tr>\n";
					}

					if(count($InfoRow) == 0){
						//無資料時要給空白
						echo '<tr class="GridViewScrollItem" style="height:0;">
								'.$no_list.'
							</tr>';
					}
				}else{
					echo '<tr class="GridViewScrollItem" style="height:0;">
                            '.$no_list.'
                        </tr>';
					echo '<p><font style="color:#ff0000;">请先查询后才会有资料出现</font></p>';
				}

?>
			</table>
		</form>
		<!-- 分页区 -->
		<div class="pagebar">
			<?=$pageInfo["html"]?>
		</div>
	</div>
</div>

<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);

});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});
</script>
<script type="text/javascript" src="<?=$web?>js/chart/Chart.bundle.js"></script> 
<script type="text/javascript" src="<?=$web?>js/chart/utils.js"></script> 
<?
	$xAxes_arr = array();
	$a4_arr = array();
	
	foreach($chart_list[3] as $key=>$val){
		$xAxes_arr[] = $key;
		$a4_arr[] = $val;
	}
	$xAxes = join( "','", $xAxes_arr );
	$xAxes = "'".$xAxes."'";

	$a4 = join( "','", $a4_arr );
	$a4 = "'".$a4."'";

	$bss_line ="";
	if($chart_list[4])
	{
		foreach($chart_list[4] as $key4 => $value4)
		{
			$color = "#".str_pad(base_convert(rand(0,255),10,16),2,"0",STR_PAD_LEFT).str_pad(base_convert(rand(0,255),10,16),2,"0",STR_PAD_LEFT).str_pad(base_convert(rand(0,255),10,16),2,"0",STR_PAD_LEFT);
			$data_arr = array();
			foreach($xAxes_arr as $Arrnum => $day1)
			{
				$data_arr[$Arrnum] = 0;
			}
			foreach($value4 as $day => $value)
			{
				foreach($xAxes_arr as $Arrnum => $day1)
				{
					if($day == $day1)
					{
						$data_arr[$Arrnum] = $value;
						break;
					}
				}
			}

			$data = join( "','", $data_arr );
			$data = "'".$data."'";

			$bss_id_name = $value4['location'].'（'.$key4.'）';
			$bss_line .= "{
							label: '{$bss_id_name}(時)',
							backgroundColor: '{$color}',
							borderColor: '{$color}',
							data: [
								{$data}
							],
							fill: false,
						},";
		}
	}

?>
<script>
	var config = {
		type: 'line',
		data: {
			labels: [<?=$xAxes?>],
			datasets: [<?=$bss_line?>]
			// , {
			// 	label: '單一換電站電池平均時段交換次數',
			// 	fill: false,
			// 	backgroundColor: window.chartColors.blue,
			// 	borderColor: window.chartColors.blue,
			// 	data: [
			// 		
			// 	],
			// }]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: ''	//上方title
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: ''	//橫title
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: ''	//緃title
					}
				}]
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);
	};
	//window.myLine.update(); 更新
</script>
<?
	$web = $this->config->item('base_url');

	
	$get_full_url_random = get_full_url_random();
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
<link href="<?=$web.$newcss;?>newcss/dashboard.css" rel="stylesheet" type="text/css">
<!-- Bootstrap Js CDN -->
<script src="<?=$web;?>newjs/gauge.min.js"></script>
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="<?=$web.$newcss;?>newcss/bootstrap.min.css">
<script src="<?=$web;?>newjs/bootstrap.min.js"></script>
<script language="javascript">
	//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
	var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
	//alert($j.fn.jquery);  //顯示目前$j是誰在用
</script>

<!-- <link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/> -->
<!-- Page Content Holder -->
<div id="dashboard" class="wrapper">
	<div id="content">
		<div class="container2">
			<h3><i class="fas fa-chart-bar"></i>機櫃狀態總覽</h3>
			<div class="row">
				<div class="col-lg-4">
					<div class="card red">
						<i class="fas fa-building"></i>
						<p>
							<?=$this->lang->line('all_station_count');?>
							<strong><?=number_format($chart_list['statino_num'])?></strong>
						</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card orange">
						<i class="fas fa-car-battery"></i>
						<p>
							<?=$this->lang->line('all_battery_count');?>
							<strong><?=number_format($chart_list['battery_num'])?></strong>
						</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card blue">
						<i class="fas fa-motorcycle"></i>
						<p>
							<?=$this->lang->line('all_vehicle_count');?>
							<strong><?=number_format($chart_list['vehicle_num'])?></strong>
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-body text-center">
							<h2><?=$this->lang->line('all_station_num');?></h2>
							<p class="battery-change"><?=number_format($chart_list['battery_exchange_count'])?> 次</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-body text-center">
							<h2><?=$this->lang->line('all_vehicle_km');?></h2>
							<p class="total-kilometers">0 公里</p>
						</div>
					</div>
				</div>
                <div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-body text-center">
							<h2><?=$this->lang->line('all_emissions');?></h2>
							<p class="total-co2">0 公斤</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="panel panel-default">
						<div class="panel-body text-center">
							<h2><?=$this->lang->line('all_weChatpay');?></h2>
							<p class="total-price"><?=(($chart_list['wechat_price']>999)?number_format($chart_list['wechat_price']):$chart_list['wechat_price'])?> 元</p>
						</div>
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-lg-6">
					<div class="container-default">
						<h2 class="col-lg-12">
						<i class="fas fa-car-battery"></i>
							<?=$this->lang->line('battery_status_statistics');?>
						</h2>
						<div>
							<canvas id="canvas3"></canvas>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="container-default">
						<h2 class="col-lg-12">
							<i class="fas fa-battery-half"></i>
							<?=$this->lang->line('battery_power_statistics');?>
						</h2>
						<div>
							<canvas id="canvas4"></canvas>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="container-default">
						<h2 class="col-lg-12">
						<i class="far fa-chart-bar"></i>
							<?=$this->lang->line('power_average');?>
						</h2>
						<div class="chart-container">
							<div style="width:100%;">
								<canvas id="canvas"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="container-default">
						<h2 class="col-lg-12">
							<i class="fas fa-exclamation-triangle"></i>
							<?=$this->lang->line('monitor_management_online');?>
						</h2>
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<td><?=$this->lang->line('so_num');?></td>
										<td><?=$this->lang->line('sb_num');?></td>
										<td><?=$this->lang->line('log_date');?></td>
										<td><?=$this->lang->line('bss_token_id');?></td>
										<td><?=$this->lang->line('type');?></td>
										<td><?=$this->lang->line('status');?></td>
										<td><?=$this->lang->line('status_desc');?></td>
										<!--<td><?=$this->lang->line('view_date');?></td>
										<td><?=$this->lang->line('view_users');?></td>-->
										<td><?=$this->lang->line('process_date');?></td>
										<td><?=$this->lang->line('process_user');?></td>
										<!--<td><?=$this->lang->line('finish_date');?></td>
										<td><?=$this->lang->line('finish_user');?></td>-->
									</tr>
								</thead>
								<tbody>
	<?
								if(count($chart_list['log_alarm_online'])){
									$type_name = array('',$this->lang->line('type_1'),$this->lang->line('type_2'),$this->lang->line('type_3'),$this->lang->line('type_4_1').$this->session->userdata('bss_time_min').$this->lang->line('type_4_2'));
									$status_name = array('','<div class="progress thin">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
													<span class="sr-only">0% Complete (success)</span>
												</div>
											</div>','<div class="progress thin">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
													<span class="sr-only">40% Complete (success)</span>
												</div>
											</div>', '<div class="progress thin">
												<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
													<span class="sr-only">100% Complete (success)</span>
												</div>
											</div>');
									$colArr = array (
										"top01"				=>	"{$this->lang->line('so_num')}",
										"bss_id"			=>	"{$this->lang->line('sb_num')}",
										"log_date"			=>	"{$this->lang->line('log_date')}",
										"bss_token_id"		=>	"{$this->lang->line('bss_token_id')}",
										"type"				=>	"{$this->lang->line('type')}",
										"status"			=>	"{$this->lang->line('status')}",
										"status_desc"		=>	"{$this->lang->line('status_desc')}",
										"process_date"		=>	"{$this->lang->line('process_date')}",
										"process_user"		=>	"{$this->lang->line('process_user')}"
									);
									$colInfo = array("colName" => $colArr);
									foreach($chart_list['log_alarm_online'] as $key => $arr){
										echo "<tr>" ;
										foreach ($colInfo["colName"] as $enName => $chName) {
											if($enName == 'status'){
												$arr[$enName] = $status_name[$arr[$enName]];
											}else if($enName == 'type'){
												$arr[$enName] = $type_name[$arr[$enName]];
											}else if($enName == 'bss_id'){
												if($arr[$enName] != ''){
													$arr[$enName] = $arr['location'].'（'.$arr[$enName].'）';
												}
											}else if($enName == 'process_user'){
												$process_user = $arr[$enName];
												if(isset($sysusername[$arr[$enName]])){
													$process_user = $sysusername[$arr[$enName]];
												
												}
												$arr[$enName] = $process_user;
											}
											echo "<td>" . $arr[$enName] . "</td>";
										}
										echo "</tr>";
									}
								}	
	?>
								</tbody>
							</table> 
						</div>
					</div>
				</div>   
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="container-default">
						<h2 class="col-lg-12">
							<i class="fas fa-crown"></i><?=$this->lang->line('battery_exchange_ranking');?>
						</h2>
						<div id="canvas-holder" style="display: inline-block; width:80%;">
							<canvas id="canvas2"></canvas>
						</div>
<?

						if(count($chart_list['dayMaxNum_10'])>0){
							echo '<table>
									<thead>
										<tr>
											<td>'.$this->lang->line('bss_id').'</td>
											<td>'.$this->lang->line('log_date').'</td>
											<td>'.$this->lang->line('sum_exchangenum').'</td>
										</tr>
									</thead>
									<tbody>';
									foreach($chart_list['dayMaxNum_10'] as $key=>$val){
										echo '<tr>
													<td>'.(($val['bss_id'] == '')?'':$val['location'].'（'.$val['bss_id'].'）').'</td>
													<td>'.$val['log_date'].'</td>
													<td>'.$val['dayMaxNum'].'</td>
												</tr>';
									
									}
							echo	'</tbody>
								  </table>';
						}

?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 新增/修改 页面 -->
<!-- config input dialog -->
<div class="modal" id="prompt" style="width: 350px; height: 100px;">
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){

});

$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});
</script>
<script type="text/javascript" src="<?=$web?>js/chart/Chart.bundle.js"></script> 
<script type="text/javascript" src="<?=$web?>js/chart/utils.js"></script> 
<?
	$xAxes_arr = array();
	$a4_arr = array();
	$a10_arr = array();
	foreach($chart_list['housr_avgNum'] as $key=>$val){
		$xAxes_arr[] = $key;
		$a4_arr[] = $val;
	}
	foreach($chart_list['housr_average'] as $key=>$val){
		$a10_arr[] = $val;
	}

	$xAxes = join( "','", $xAxes_arr );
	$xAxes = "'".$xAxes."'";

	$a4 = join( "','", $a4_arr );
	$a4 = "'".$a4."'";

	$a10 = join( "','", $a10_arr );
	$a10 = "'".$a10."'";

	// $color = array('red','orange','yellow','green','blue','purple','grey','#FF00FF','#191970','#000000');
	$color = array('#FF0000','#FF4500','#FFFF00','#32CD32','#0000CD','#800080','#A9A9A9','#FF00FF','#191970','#000000');
	$k_labels_arr = array();
	$k_labels_all_arr = array();
	$k_color = array();
	$val_arr = array();
	foreach($chart_list['dayMaxNum_10'] as $key=>$val){
		//$k_labels_arr[] = (($val['bss_id'] == '')?$key:$val['bss_id']);
		$k_labels_arr[] = (($val['location'] == '')?$key:$val['location']);
		$k_labels_all_arr[] = (($val['location'] == '')?$key:$val['location'].'（'.$val['bss_id'].'）');
		$val_arr[] = $val['dayMaxNum'];
		// $k_color[] = 'window.chartColors.'.$color[rand(0,6)];
	}
	
	$k_labels = join( "','", $k_labels_arr );
	$k_labels = "'".$k_labels."'";

	$k_labels_all = join( "','", $k_labels_all_arr );
	$k_labels_all = "'".$k_labels_all."'";

	$vals = join( "','", $val_arr );
	$vals = "'".$vals."'";


	//电池统计数量
	//Y=充電中；N=充飽電；S=待機；F=故障
	$battery_label = array('Y'=>$this->lang->line('battery_statusY'), 'N'=>$this->lang->line('battery_statusN'), 'S' => $this->lang->line('battery_statusS') ,'E'=>$this->lang->line('battery_statusE'), 'V'=>$this->lang->line('battery_statusV'));
	$battery_color = array('Y'=>'#ffc295', 'N'=>'#9fd79c', 'S' => '#8c8c8c' ,'E'=>'#f49292' ,'V'=>'#46a3ff');
	$bAxes_arr = array();
	$battery_arr = array();
	$battery_color_arr = array();
	if(count($chart_list['battery_status_num'])>0){
		foreach($chart_list['battery_status_num'] as $key=>$val){
			$bAxes_arr[] = $battery_label[$key].'('.$val.')';
			$battery_arr[] = $val;
			$battery_color_arr[] = $battery_color[$key];
		}

		$b_labels = join( "','", $bAxes_arr );
		$b_labels = "'".$b_labels."'";

		$b_color = join( "','", $battery_color_arr );
		$b_color = "'".$b_color."'";

		$b_vals = join( "','", $battery_arr );
		$b_vals = "'".$b_vals."'";
	}

	$cAxes_arr = array();
	$capacity_arr = array();
	$capacity_label = array('','0~10%','11~20%','21~30%','31~40%','41~50%','51~60%','61~70%','71~80%','81~90%','91~100%');
	$capacity_color = array('','#e0e0e0','#ffb5b5','#ff95ca','	#ff77ff','	#be77ff','	#6a6aff','	#46a3ff','	#4dffff	','	#ffaf60','	#53ff53');

	if(count($chart_list['battery_capacity'])>0){
		foreach($chart_list['battery_capacity'] as $key=>$val){
			$cAxes_arr[] = $capacity_label[$key].'('.$val.')';
			$capacity_arr[] = $val;
			$capacity_color_arr[] = 'color(chartColors.a'.$key.').alpha(0.5).rgbString()';
		}
		$c_labels = join( "','", $cAxes_arr );
		$c_labels = "'".$c_labels."'";

		$c_color = join( ",", $capacity_color_arr );
		//$c_color = "'".$c_color."'";


		$c_vals = join( "','", $capacity_arr );
		$c_vals = "'".$c_vals."'";
	}

?>
<script>
	var color = Chart.helpers.color;
	var barChartData = {
		/*labels_all: [<?=$k_labels_all;?>],*/
		labels: [<?=$k_labels_all;?>],
		datasets: [{
			label: '<?=$this->lang->line('battery_change_nums')?>',
			backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
			borderColor: window.chartColors.red,
			borderWidth: 1,
			data: [
				<?=$vals;?>
			]
		},]

	};
	var config = {
		type: 'line',
		data: {
			labels: [<?=$xAxes?>],
			datasets: [{
				label: '<?=$this->lang->line('exchanges_average_num')?>',
				backgroundColor: window.chartColors.red,
				borderColor: window.chartColors.red,
				data: [
					<?=$a4?>
				],
				fill: false,
			}, {
				label: '<?=$this->lang->line('one_exchanges_average')?>',
				fill: false,
				backgroundColor: window.chartColors.blue,
				borderColor: window.chartColors.blue,
				data: [
					<?=$a10?>
				],
			}]
		},
		options: {
			responsive: true,
			title: {
				display: true,
				text: ''	//上方title
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: ''	//橫title
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: ''	//緃title
					}
				}]
			}
		}
	};

	var config_pie = {
		type: 'pie',
		data: {
			datasets: [{
				data: [
					<?=$b_vals?>
				],
				backgroundColor: [<?=$b_color?>],
				label: 'Dataset 1'
			}],

			//飽電：綠色 充電中：橘色
			//故障：紅色 停用：灰色
			labels: [<?=$b_labels;?>]
		},
		options: {
			responsive: true
		}
	};

	var color = Chart.helpers.color;
	var config_pie2 = {
		data: {
			datasets: [{
				data: [
					<?=$c_vals;?>
				],
				backgroundColor: [
					<?=$c_color;?>
				],
				label: 'My dataset' // for legend
			}],
			labels: [<?=$c_labels?>]
		},
		options: {
			responsive: true,
			legend: {
				position: 'right',
			},
			title: {
				display: true,
				text: ''
			},
			scale: {
				ticks: {
					beginAtZero: true
				},
				reverse: false
			},
			animation: {
				animateRotate: false,
				animateScale: true
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);

		var ctx2 = document.getElementById('canvas2').getContext('2d');
		window.myBar = new Chart(ctx2, {
			type: 'bar',
			data: barChartData,
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: true,
					text: ''
				}
			}
		});

		var ctx3 = document.getElementById('canvas3').getContext('2d');
		window.myPie = new Chart(ctx3, config_pie);
		var ctx4 = document.getElementById('canvas4').getContext('2d');
		window.myPolarArea = Chart.PolarArea(ctx4, config_pie2);

	};
	//window.myLine.update(); 更新
</script>


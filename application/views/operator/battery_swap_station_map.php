<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no, width=device-width">
    <title>电池管理-地图</title>
    <link rel="stylesheet" href="https://a.amap.com/jsapi_demos/static/demo-center/css/demo-center.css"/>
    <style>
        html, body, #container {
            height: 100%;
            width: 100%;
        }

        .amap-icon img{
            width: 25px;
            height: 34px;
        }

		.amap-info-contentContainer{
			font-size: 16px;
		}
    </style>
</head>
<body>
<?
	$get_full_url_random = get_full_url_random();
?>
<div id="container"></div>
<div class="info">
    <div id="centerCoord"></div>
    <div id="tips"></div>
</div>
<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.11&key=59b39aae5ab47b9a99ad555535698481"></script>
<?
	$web = $this->config->item('base_url');
?>

<script type="text/javascript">
<?
	if($error == ''){
?>
	var map = new AMap.Map('container', {
	  resizeEnable: false, //是否监控地图容器尺寸变化
	  zoom: 16, //初始地图级别
	  center: [<?=$longitude;?> , <?=$latitude;?>] //初始地图中心点
	});

    var infoWindow = new AMap.InfoWindow({offset: new AMap.Pixel(10, -20)});
<?php 
		foreach($stationInfo as $key => $value)
		{
			$v_image = '';
			$s_flag = 'N';
			if($value['virtual_map'] != ''){
				$v_image = "<img src=\"{$get_full_url_random}/getImageSrc/{$value["s_num"]}/virtual_map\" height=\"160\">";
				$s_flag = 'Y';
			}
?>
			var marker = new AMap.Marker({
				icon: new AMap.Icon({            
						image: "<?=$web;?>images/location_station.png",
						size: new AMap.Size(40, 40), //图标大小
						imageSize: new AMap.Size(40,40)
				}),    
				position: <?echo "[{$value['longitude']},{$value['latitude']}]";?>,
				map: map
			});
			marker.content = <?echo "'{$value['bss_id_name']}'";?>;
			marker.content += '<br>'+<?echo "'{$v_image}'"?>;
			marker.on('click', markerClick);
			marker.emit('click', {target: marker});
<?
		}
?>
    function markerClick(e) {
        infoWindow.setContent(e.target.content);
        infoWindow.open(map, e.target.getPosition());
    }
    map.setFitView(<?=$longitude;?> , <?=$latitude;?>);
<?
	}
?>

</script>
</body>
</html>
<?
	$web = $this->config->item('base_url');

	$to_flag = $this->session->userdata('to_flag'); 
	$to_flag_num = "";
	$disabled = "";
	if($to_flag == '1'){
		$to_flag_num = $this->session->userdata('to_num');
		$disabled = "disabled";
	}
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"so_num"			=> $to_flag_num,
		"location"			=> "",
		"note"				=> "",
		"bss_id"			=> "",
		"bss_token"			=> "",
		"ip_type"			=> "",
		"ip"				=> "",
		"last_online"		=> "",
		"version_no"		=> "",
		"version_date"		=> "",
		"track_quantity"	=> "",
		"latitude"			=> "",
		"longitude"			=> "",
		"virtual_map"		=> "",
		"exchange_num"		=> "",
		"status"			=> "Y",
		"bss_no"			=> "",	//機櫃編號
		"province"			=> "",	//省
		"city"				=> "",	//城市
		"district"			=> "",	//區
		"station_no"		=> "",	//1.0 OR 2.0
		"sim_no"		=> ""	
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);


	$track_quant = $this->session->userdata('track_quantity');

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
<script type="text/javascript" src="<?=$web?>js/cityselectcn/distpicker.js"></script>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('battery_swap_station_management')?><span><?=$show_sub_title;?></span></p>
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box" style="width: 43%;">
					<ul>
						<li>
							<label><?=$this->lang->line('battery_swap_station_operator')?></label>
							<select name="postdata[so_num]" id='so_num' style="width:205px;" <?=$disabled;?>>
								<option value=''><?=$this->lang->line('battery_swap_station_select_operator')?></option>
								<?php
									foreach($operatorList as $key => $value)
									{
										$selected = "";
										if($dataInfo['so_num'] == $value['s_num'])
										{
											$selected = "selected";
										}
										echo "<option value='{$value['s_num']}' {$selected}>{$value['top01']}</option>";
									}
								?>
							</select>
						</li>
						<li class="auto" style="overflow:hidden;">
							<label for="" id="label_zipcode"><?=$this->lang->line('city')?></label>
							<div id="cnzipcode" style="float:left; width: 92%;">
								<select class="form-control" NAME="postdata[province]" id="province"></select><br/>
								<select class="form-control" NAME="postdata[city]" id="city"></select><br/>
								<select class="form-control" NAME="postdata[district]" id="district"></select>
							</div>
						</li>
						<li>
							<ul style="overflow:hidden;">
								<li style="float:left; width:50%; margin:0;">
									<label><?=$this->lang->line('battery_swap_station_latitude')?></label>
									<input type="text" class="txt" style="widtH: 72%;" name="postdata[latitude]" id="latitude" value="<?=$dataInfo['latitude']?>">
								</li>
								<li style="float:left; width:50%; margin:0;">
									<label><?=$this->lang->line('battery_swap_station_longitude')?></label>
									<input type="text" class="txt" style="widtH: 72%;" name="postdata[longitude]" id="longitude" value="<?=$dataInfo['longitude']?>">
								</li>
							</ul>
						</li>
						<li class="auto">
							<label for=""><?=$this->lang->line('battery_swap_station_virtual_map')?></label>
							<p class="reality">
								<span id="upload_virtual_map">
								<?php
									$del_button = '';
									if($dataInfo["virtual_map"] != ''){
										echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$dataInfo["s_num"].'/virtual_map" class="preview" style="max-width: 420px; max-height: 250px;">';
									}else{
										echo '<img src="'.$web.'images/reality.png" class="preview" style="max-width: 420px; max-height: 250px;">';
									}
								?>
								</span><br/>
								<br/>
								<input class="txt"  style="margin: 10px 0 0 0;" type="text" id="virtual_map_txt" name="virtual_map_txt" class="tb" value="" readonly>
								<input type="file" onchange="$j('#virtual_map_txt').val($j('#virtual_map').val());" name="virtual_map" id="virtual_map" style="display:none;" class="upl">
								<input type="button" value="浏览" id="id_virtual_map" class="btn">
								<span class="notice">(建议尺寸：420px * 250px)</span>
							</p>
						</li>
					</ul>
				</div>
				<div style="width: 57%; overflow:hidden;">
					<div class="container-box" style="width: 100%;">
						<ul>
							<li>
								<label><?=$this->lang->line('battery_swap_station_bss_token')?></label>
								<input type="text" class="txt" style="width:93%;" name="postdata[bss_token]" id="bss_token" maxlength="32" value="<?=$dataInfo['bss_token']?>">
							</li>
						</ul>
					</div>
					<div class="container-box" style="width: 37%;">
						<ul>
							<li>
								<label><?=$this->lang->line('battery_swap_station_location')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[location]" id="location" value="<?=$dataInfo['location']?>">
							</li>
							<li>
								<label class="required"><?=$this->lang->line('battery_swap_station_bss_id')?></label>
								<input type="text" class="txt validate[required], ajax[ajax_pk_bss_id, <?=$dataInfo['s_num']?>]" style="width:160px;" name="postdata[bss_id]" id="bss_id" maxlength="10" value="<?=$dataInfo['bss_id']?>">
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_ip_type')?></label>
								<input type="radio" id="ip_type" name="postdata[ip_type]" class="radio first" value="F" <?=(($dataInfo["ip_type"] == 'F')?'checked':'');?>><?=$this->lang->line('ip_type_F')?>
								<input type="radio" id="ip_type" name="postdata[ip_type]" class="radio" value="C" <?=(($dataInfo["ip_type"] == 'C')?'checked':'');?>><?=$this->lang->line('ip_type_C')?>
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_ip')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[ip]" id="ip" maxlength="80" value="<?=$dataInfo['ip']?>">
							</li>
						</ul>
					</div>
					<div class="container-box" style="width: 30%;">
						<ul>
							<li>
								<label><?=$this->lang->line('battery_swap_station_last_online')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[last_online]" id="last_online" value="<?=$dataInfo['last_online']?>">
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_version_no')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[version_no]" id="version_no" maxlength="4" value="<?=$dataInfo['version_no']?>">
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_version_date')?></label>
								<input type="date" class="txt" style="width:160px;" name="postdata[version_date]" id="version_date" maxlength="4" value="<?=$dataInfo['version_date']?>">
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_track_quantity')?></label>
								<select name="postdata[track_quantity]" id="track_quantity" >

<?php
								$track_quant = $this->session->userdata('track_quantity');
								$quant_arr = explode(",",$track_quant);
								if(count($quant_arr)>0){
									for($i=0; $i<(count($quant_arr)); $i++){
										$checked = "";
										if($dataInfo['track_quantity'] == $quant_arr[$i]){
											$checked = "selected";
										}
										echo '<option value="'.$quant_arr[$i].'" '.$checked.'>'.$quant_arr[$i].'</option>';
									}
								}
?>


								</select>
								<!-- select name="postdata[track_quantity]" id="track_quantity" >
									<?php
										//預設50 不可修改
										$ck6 = "";
										$ck12 = "";
										$ck18 = "";
										$ck24 = "";
										$ck30 = "";
										$ck36 = "";
										$ck50 = "";
										switch($dataInfo['track_quantity'])
										{
											case 6:
												$ck6 = "selected";
												break;
											case 12:
												$ck12 = "selected";
												break;
											case 18:
												$ck18 = "selected";
												break;
											case 24:
												$ck24 = "selected";
												break;
											case 30:
												$ck30 = "selected";
												break;
											case 36:
												$ck36 = "selected";
												break;
											case 50:
												$ck50 = "selected";
												break;
										}
									?>
									<!-- <option value='6' <?=$ck6?>>6</option> -->
									<!-- option value='12' <?=$ck12?>>12</option>
									<!-- <option value='18' <?=$ck18?>>18</option> -->
									<!-- option value='24' <?=$ck24?>>24</option>
									<!-- <option value='30' <?=$ck30?>>30</option> -->
									<!--option value='36' <?=$ck36?>>36</option>
									<!-- <option value='50' <?=$ck50?>>50</option> -->
								</select>
							</li>
						</ul>
					</div>
					<div class="container-box" style="width: 33%;">
						<ul>
							
							<li>
								<label><?=$this->lang->line('battery_swap_station_exchange_num')?></label>
								<input type="text" class="txt" style="width:160px;" id="exchange_num" value="<?=$dataInfo['exchange_num']?>" disabled>
							</li>
							<!-- li>
								<label><?=$this->lang->line('battery_swap_station_bss_no')?></label>
								<input type="text" class="txt" name="postdata[bss_no]" id="bss_no"value="<?=$dataInfo['bss_no']?>">
							</li -->
							
							<li>
								<label><?=$this->lang->line('battery_swap_station_station_no')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[station_no]" id="station_no" maxlength="4" value="<?=$dataInfo['station_no']?>">
							</li>
							<li>
								<label><?=$this->lang->line('sim_no')?></label>
								<input type="text" class="txt" style="width:160px;" name="postdata[sim_no]" id="sim_no" maxlength="200" value="<?=$dataInfo['sim_no']?>">
							</li>
							<li>
								<label><?=$this->lang->line('battery_swap_station_status')?></label>
								<input type="radio" name="postdata[status]" id="statusY" class="radio first" value="Y" <?echo (($dataInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('battery_swap_station_statusY')?><input type="radio" name="postdata[status]" id="statusN" class="radio" value="N" <?echo (($dataInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('battery_swap_station_statusN')?>
								<!-- <input type="radio" class="radio first">正常(充电中)<input type="radio" class="radio">已满电<input type="radio" class="radio">故障<input type="radio" class="radio">维修<input type="radio" class="radio">报废 -->
							</li>
						</ul>
					</div>
					<div class="container-box" style="width: 100%;">
						<ul>
							<li>
								<label><?=$this->lang->line('battery_swap_station_note')?></label>
								<textarea name="postdata[note]" id="note" cols="4" maxlength="100" rows="25" class="small" style="width:93%;"><?=$dataInfo['note']?></textarea>
							</li>
						</ul>
					</li>
				</div>	
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	$j(function(){
		var province = '<?=$dataInfo["province"]?>';
		var city = '<?=$dataInfo["city"]?>';
		var district = '<?=$dataInfo["district"]?>';
		$j("#cnzipcode").distpicker({
			province: province,
			city: city,
			district: district
		});
	});
	

	//储存
	function fn_save(){
		$j("#so_num").attr('disabled', false);
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
$j("#id_virtual_map").click(function(){
	$j("#virtual_map").click();
});

$j(function (){
 
    function format_float(num, pos)
    {
        var size = Math.pow(10, pos);
        return Math.round(num * size) / size;
    }
 
    function preview(input) {
 
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $j('.preview').attr('src', e.target.result);
                var KB = format_float(e.total / 1024, 2);
               // $j('.size').text("檔案大小：" + KB + " KB");
            }
 
            reader.readAsDataURL(input.files[0]);
        }
    }
 
    $j("body").on("change", ".upl", function (){
        preview(this);
    })
    
})

$j("body").on("change", ".upl", function (){
    preview(this);
})
 
/**
 * 預覽圖
 * @param   input 輸入 input[type=file] 的 this
 */
function preview(input) {
 
    // 若有選取檔案
    if (input.files && input.files[0]) {
 
        // 建立一個物件，使用 Web APIs 的檔案讀取器(FileReader 物件) 來讀取使用者選取電腦中的檔案
        var reader = new FileReader();
 
        // 事先定義好，當讀取成功後會觸發的事情
        reader.onload = function (e) {
            
            console.log(e);
 
            // 這裡看到的 e.target.result 物件，是使用者的檔案被 FileReader 轉換成 base64 的字串格式，
            // 在這裡我們選取圖檔，所以轉換出來的，會是如 『data:image/jpeg;base64,.....』這樣的字串樣式。
            // 我們用它當作圖片路徑就對了。
            $j('.preview').attr('src', e.target.result);
 
            // 檔案大小，把 Bytes 轉換為 KB
            var KB = format_float(e.total / 1024, 2);
            $j('.size').text("檔案大小：" + KB + " KB");
        }
 
        // 因為上面定義好讀取成功的事情，所以這裡可以放心讀取檔案
        reader.readAsDataURL(input.files[0]);
    }
}
 
/**
 * 格式化
 * @param   num 要轉換的數字
 * @param   pos 指定小數第幾位做四捨五入
 */
function format_float(num, pos)
{
    var size = Math.pow(10, pos);
    return Math.round(num * size) / size;
}
</script>
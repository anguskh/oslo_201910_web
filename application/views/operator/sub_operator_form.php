<?
	$web = $this->config->item('base_url');
	$to_flag = $this->session->userdata('to_flag'); 
	$to_flag_num = "";
	$disabled = "";
	if($to_flag == '1'){
		$to_flag_num = $this->session->userdata('to_num');
		$disabled = "disabled";
	}
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"to_num"			=> $to_flag_num,
		"tsop01"			=> "",
		"tsop02" 			=> "",
		"tsop03"			=> "",
		"tsop04"			=> "",
		"tsop05"			=> "",
		"tsop06"			=> "",
		"tsop07"			=> "",
		"tsop08"			=> "",
		"tsop09"			=> "",
		"status"			=> "Y"
	);

	$sub_operatorInfo = isset($sub_operatorInfo) ? $sub_operatorInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $sub_operatorInfo["s_num"]);

	$Sel1 = "";
	$Sel0 = "";
	if ($sub_operatorInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($sub_operatorInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($sub_operatorInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

	if($bView){
		$show_sub_title = "查询";
	}else{
		$show_sub_title = ($sub_operatorInfo["s_num"]!='')?'编辑':'新增';
	}
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('sub_operator_management')?><span><?=$show_sub_title;?></span></p>
			<form id="formID" action="<?=$action?>" method="post">
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$sub_operatorInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<p class="title">基本资料</p>
					<ul>
						<li>
							<label for=""><?=$this->lang->line('to_num')?></label>
							<select name="postdata[to_num]" id="to_num" <?=$disabled;?>>
								<option value="">选择<?=$this->lang->line('to_num')?></option>
<?
								foreach($operator_list as $d_arr){
									echo '<option value="'.$d_arr['s_num'].'" '.(($sub_operatorInfo["to_num"]==$d_arr['s_num'])?'selected':'').'>'.$d_arr['top01'].'</option>';
								}		
?>
							</select>
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('tsop01')?></label>
							<input type="text" class="txt validate[required]" style="width:230px;" name="postdata[tsop01]" id="tsop01" value="<?=$sub_operatorInfo["tsop01"]?>">		
						</li>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('tsop02')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[tsop02]" id="tsop02" value="1" <?echo (($sub_operatorInfo["tsop02"]=='1')?'checked':''); ?>><?=$this->lang->line('tsop02_1')?>
							<input type="radio" class="radio validate[required]" name="postdata[tsop02]" id="tsop02" value="2" <?echo (($sub_operatorInfo["tsop02"]=='2')?'checked':''); ?>><?=$this->lang->line('tsop02_2')?>
							<input type="radio" class="radio validate[required]" name="postdata[tsop02]" id="tsop02"  value="3"<?echo (($sub_operatorInfo["tsop02"]=='3')?'checked':''); ?>><?=$this->lang->line('tsop02_3')?>
						</li>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[status]" id="status" value="Y" <?echo (($sub_operatorInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('enable')?>
							<input type="radio" class="radio validate[required]" name="postdata[status]" id="status" value="N" <?echo (($sub_operatorInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('disable')?>

						</li>
					</ul>
				</div>
				<div class="container-box" style="width:43%;">
					<p class="title">&nbsp;</p>
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('tsop03')?></label>
							<input type="text" class="txt long2 validate[required]" name="postdata[tsop03]" id="tsop03" value="<?=$sub_operatorInfo['tsop03']?>">
						</li>
					</ul>
					<ul style="margin:0; padding:0; overflow:hidden;">
						<li style="float:left; width:43%;">
							<label for=""><?=$this->lang->line('tsop04')?></label>
							<input type="text" class="txt" style="width:100%;" name="postdata[tsop04]" id="tsop04" value="<?=$sub_operatorInfo['tsop04']?>">
						</li>
						<li style="float:left; width:5%;">&nbsp;</li>
						<li style="float:left; width:43%;">
							<label for=""><?=$this->lang->line('tsop05')?></label>
							<input type="text" class="txt" style="width:100%;" name="postdata[tsop05]" id="tsop05" value="<?=$sub_operatorInfo['tsop05']?>">
						</li>
					</ul>
					<ul>
						<li class="auto">
							<label for=""><?=$this->lang->line('tsop06')?></label>
							<input type="text" class="txt long2" name="postdata[tsop06]" id="tsop06" value="<?=$sub_operatorInfo['tsop06']?>">
						</li>
						<li class="auto">
							<label for=""><?=$this->lang->line('tsop07')?></label>
							<input type="text" class="txt long2" name="postdata[tsop07]" id="tsop07" value="<?=$sub_operatorInfo['tsop07']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<p class="title">帐务资料</p>
					<div class="container-box">
						<ul>
							<li>
								<label for=""><?=$this->lang->line('tsop08')?></label>
								<input type="text" class="txt" name="postdata[tsop08]" id="tsop08" value="<?=$sub_operatorInfo['tsop08']?>" maxlength="3">
							</li>
							<li>
								<label for=""><?=$this->lang->line('tsop09')?></label>
								<input type="text" class="txt" name="postdata[tsop09]" id="tsop09" value="<?=$sub_operatorInfo['tsop09']?>">
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
	</div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#to_num").attr('disabled', false);
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
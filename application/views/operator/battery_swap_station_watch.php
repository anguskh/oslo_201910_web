<?
	$get_full_url_random = get_full_url_random();
	$web = $this->config->item('base_url');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>上海綠農創意科技</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width">
		<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet">
		<link href="<?=$web.$newcss;?>newcss/bootstrap.css" rel="stylesheet">
		<script src="<?=$web;?>js/jquery-1.11.3.min.js"></script>
		<script src="<?=$web;?>newjs/tooltip.js"></script>
		<script type="text/javascript">
			$(function () {
				$('[data-toggle="tooltip"]').tooltip()
			})
		</script>	
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="battery_box">
			<p class="btitle"><?=$location;?>（<?=$bss_id;?>）</p>
			<!-- 銀白色長方形的電池圖=有電池、黑色長方形的電池圖=沒電池no 紅色 故障error-->
			<p class="info">
				<span class="error"></span><em>故障</em>
				<span class="stop"></span><em>停用</em>
				<span class="no"></span><em>空軌</em>
				<span class="charge"></span><em>充電中</em>
				<span></span><em>飽電</em>
			</p>
			<div class="machine">
				<div class="pad">
					<p class="point"></p>
					<div class="screen"></div>
				</div>
				<ul class="battery-track"><!--判面-->
<?
						$battery_cell_name = array(""=>"","0"=>"N/A","1"=>"Over Temp");
						$bcu_name = array(""=>"","1"=>"正常","2"=>"异常","3"=>"停用");
						$status_name = array(""=>"", "0"=>"充电中","1"=>"饱电","2"=>"充电中无电流","3"=>"电池过温");
						$track_name = array(""=>"","1"=>"空轨且都正常","2"=>"有电池且都正常","3"=>"电池应取但未取出","4"=>"电池充电时间过长","5"=>"轨道停用","6"=>"CCB过温","7"=>"机柜温度过温");
						for($i=1; $i<=8; $i++){
							
							$content = '';
							$class = '';
							if(isset($dataInfo[$i])){
								if($dataInfo[$i]['column_charge'] == 'Y'){
									$class = 'charge';
								}else if($dataInfo[$i]['column_charge'] == 'F'){
									//故障
									$class = 'error';
								}else if($dataInfo[$i]['column_charge'] == 'N' || $dataInfo[$i]['column_charge'] == 'S'){
									$class = "";
								}

								if($dataInfo[$i]['column_park'] == 'Y'){
									if(!isset($dataInfo[$i])){
										$content = "";
									}else{
										//echo $dataInfo[$i]['battery_status'];
										$content = '电池序号：'.$dataInfo[$i]['battery_id'].'<br/>';
										$content .= '电池电压：'.$dataInfo[$i]['battery_voltage'].'<br/>';
										$content .= '电池状态：'.$status_name[$dataInfo[$i]['battery_status']].'<br/>';
										$content .= '电池电流：'.$dataInfo[$i]['battery_amps'].'<br/>';
										$content .= 'SOC：'.$dataInfo[$i]['battery_capacity'].'<br/>';
										$content .= '电池温度：'.$dataInfo[$i]['battery_temperature'].'<br/>';
										$content .= '轨道状态：'.$track_name[$dataInfo[$i]['track_status']].'<br/>';
									}
								}else if($dataInfo[$i]['column_park'] == 'N'){
									//沒電池(空軌)
									$class = 'no';
								}

								if($dataInfo[$i]['tbst_status'] == 'E'){
									//故障
									$class = 'error';
								}else if($dataInfo[$i]['tbst_status'] == 'N'){
									//電池交換軌道狀態(N 停用)
									$class = 'stop';
								}

								echo '<li>
										<span data-toggle="tooltip" data-placement="right" data-html="true" title data-original-title="'.$content.'" class="'.$class.'">'.$i.'</span>
									  </li>';

							}
						
						}
?>
					</ul>
					<div class="footer"></div>
					
			</div>
		</div>
	</body>
</html>

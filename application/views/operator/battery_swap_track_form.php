<?
	$web = $this->config->item('base_url');
	$to_flag = $this->session->userdata('to_flag'); 
	$to_flag_num = "";
	$disabled = "";
	if($to_flag == '1'){
		$to_flag_num = $this->session->userdata('to_num');
		$disabled = "disabled";
	}

	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"so_num"			=> $to_flag_num,
		"sb_num"			=> "",
		"track_no"			=> "",
		"column_park"		=> "Y",
		"column_charge"		=> "Y",
		"battery_id"		=> "",
		"status"			=> "Y"
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('battery_swap_track_management')?><span><?=$show_sub_title;?></span></p>
		<div class="section">
			<form id="formID" action="<?=$action?>" method="post">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="section container">
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label><?=$this->lang->line('battery_swap_track_operator')?></label>
							<select name="postdata[so_num]" id='so_num' style="width:200px;" <?=$disabled;?>>
								<option value=''><?=$this->lang->line('battery_swap_track_select_operator')?></option>
								<?php
									foreach($operatorList as $key => $value)
									{
										$selected = "";
										if($dataInfo['so_num'] == $value['s_num'])
										{
											$selected = "selected";
										}
										echo "<option value='{$value['s_num']}' {$selected}>{$value['top01']}</option>";
									}
								?>
							</select>
						</li>
						<li>
							<label><?=$this->lang->line('battery_swap_track_bss_id')?></label>
							<select name="postdata[sb_num]" id='sb_num' style="width:200px;" disabled>
							</select>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:26%;">
					<ul>
						<li>
							<label><?=$this->lang->line('battery_swap_track_no')?></label>
							<input type="text" class="txt" name="postdata[track_no]" id="track_no" maxlength="2" value="<?=$dataInfo['track_no']?>">
						</li>
						<li>
							<label><?=$this->lang->line('battery_swap_track_column_park')?></label>
							<input type="radio" name="postdata[column_park]" id="column_parkY" class="radio first" value="Y" <?echo (($dataInfo["column_park"]=='Y')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_parkY')?>
							<input type="radio" name="postdata[column_park]" id="column_parkN" class="radio" value="N" <?echo (($dataInfo["column_park"]=='N')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_parkN')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:29%;">
					<ul>
						<li>
							<label><?=$this->lang->line('battery_swap_track_column_charge')?></label>
							<input type="radio" name="postdata[column_charge]" id="column_chargeY" class="radio first" value="Y" <?echo (($dataInfo["column_charge"]=='Y')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_chargeY')?>
							<input type="radio" name="postdata[column_charge]" id="column_chargeN" class="radio" value="N" <?echo (($dataInfo["column_charge"]=='N')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_chargeN')?>
							<input type="radio" name="postdata[column_charge]" id="column_chargeS" class="radio" value="S" <?echo (($dataInfo["column_charge"]=='S')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_chargeS')?>
							<input type="radio" name="postdata[column_charge]" id="column_chargeF" class="radio" value="F" <?echo (($dataInfo["column_charge"]=='F')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_column_chargeF')?>
						</li>
						<li>
							<label><?=$this->lang->line('battery_swap_track_battery_id')?></label>
							<input type="text" class="txt" name="postdata[battery_id]" id="battery_id" maxlength="32" value="<?=$dataInfo['battery_id']?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:20%;">
					<ul>
						<li class="line2">
							<label><?=$this->lang->line('battery_swap_track_status')?></label>
							<input type="radio" name="postdata[status]" id="statusY" class="radio first" value="Y" <?echo (($dataInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_statusY')?>
							<input type="radio" name="postdata[status]" id="statusN" class="radio" value="N" <?echo (($dataInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_statusN')?>
							<input type="radio" name="postdata[status]" id="statusE" class="radio" value="E" <?echo (($dataInfo["status"]=='E')?'checked':''); ?>><?=$this->lang->line('battery_swap_track_statusE')?>
							<!-- <input type="radio" class="radio first">正常(充电中)<input type="radio" class="radio">已满电<input type="radio" class="radio">故障<input type="radio" class="radio">维修<input type="radio" class="radio">报废 -->
						</li>
					</ul>
				</div>
			</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#so_num").attr('disabled', false);
		$j("#sb_num").removeAttr("disabled");
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

//营运商事件
if($j("#so_num").val()!="")
{
	var so_num = $j("#so_num").val();
	var sb_num = "<?=$dataInfo['sb_num']?>";
	getbatteryswapstationselect(so_num,sb_num);
}

//营运商事件
$j("#so_num").change(function(){
	getbatteryswapstationselect($j(this).val());
});

function getbatteryswapstationselect(so_num,sb_num = ""){
     $j.ajax({
		type:'post',
		url: '<?=$web?>operator/battery_swap_track/select_battery_swap_station',
		data: {<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>',so_num:so_num,sb_num:sb_num},
		async:false,
		error: function(xhr) {
			strMsg += 'Ajax request发生错误[background.php]\n请重试';
			alert(strMsg);
		},
		success: function (response) {
			$j('#sb_num').html(response);
		}
	}) ;
}
</script>
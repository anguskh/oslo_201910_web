<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"top01"					=> "",
		"top02" 				=> "",
		"top03"					=> "",
		"top04"					=> "",
		"top05"					=> "",
		"top06"					=> "",
		"top07"					=> "",
		"top09"					=> "",
		"status"				=> "Y"
	);

	$operatorInfo = isset($operatorInfo) ? $operatorInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $operatorInfo["s_num"]);

	$Sel1 = "";
	$Sel0 = "";
	if ($operatorInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($operatorInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($operatorInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('operator_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title"></p>
			<form id="formID" action="<?=$action?>" method="post">
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$operatorInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('top01')?></label>
							<input type="text" class="txt validate[required]" name="postdata[top01]" id="top01" value="<?=$operatorInfo["top01"]?>" maxlength="50">		
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('top09')?></label>
							<input type="text" class="txt validate[required]" name="postdata[top09]" id="top09" value="<?=$operatorInfo["top09"]?>" maxlength="10">		
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('top02')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[top02]" id="top02" value="1" <?echo (($operatorInfo["top02"]=='1')?'checked':''); ?>><?=$this->lang->line('top02_1')?>
							<input type="radio" class="radio validate[required]" name="postdata[top02]" id="top02" value="2" <?echo (($operatorInfo["top02"]=='2')?'checked':''); ?>><?=$this->lang->line('top02_2')?>
							<input type="radio" class="radio validate[required]" name="postdata[top02]" id="top02"  value="3"<?echo (($operatorInfo["top02"]=='3')?'checked':''); ?>><?=$this->lang->line('top02_3')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('top03')?></label>
							<input type="text" class="txt validate[required]" name="postdata[top03]" id="top03" value="<?=$operatorInfo['top03']?>"  maxlength="10">
						</li>
						<li>		
							<label for=""><?=$this->lang->line('top04')?></label>
							<input type="text" class="txt" name="postdata[top04]" id="top04" value="<?=$operatorInfo['top04']?>" maxlength="20">
						</li>
						<li>
							<label for=""><?=$this->lang->line('top05')?></label>
							<input type="text" class="txt" name="postdata[top05]" id="top05" value="<?=$operatorInfo['top05']?>" maxlength="10">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:50%;">
					<ul>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[status]" id="status" value="Y" <?echo (($operatorInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('enable')?>
							<input type="radio" class="radio validate[required]" name="postdata[status]" id="status" value="N" <?echo (($operatorInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('disable')?>
						</li>
						<li class="auto">
							<label for=""><?=$this->lang->line('top06')?></label>
							<input type="text" class="txt long2" name="postdata[top06]" id="top06" value="<?=$operatorInfo['top06']?>" maxlength="50">
						</li>
						<li class="auto">
							<label for=""><?=$this->lang->line('top07')?></label>
							<input type="text" class="txt long2" name="postdata[top07]" id="top07" value="<?=$operatorInfo['top07']?>" maxlength="50">
						</li>
					</ul>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

</script>
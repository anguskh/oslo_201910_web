<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"so_num"			=> "",
		"sso_num" 			=> "",
		"type"				=> "",
		"name"				=> "",
		"price"				=> "",
		"method"			=> "",
		"remark"			=> "",
		"status"			=> ""
	);

	$lease_priceInfo = isset($lease_priceInfo) ? $lease_priceInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $lease_priceInfo["s_num"]);

	$Sel1 = "";
	$Sel0 = "";
	if ($lease_priceInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($lease_priceInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($lease_priceInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	
	
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('lease_price_management')?><span><?=$show_sub_title;?></span></p>
			<p class="title"></p>
			<form id="formID" action="<?=$action?>" method="post">
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$lease_priceInfo['s_num']?>" />
				<div class="container-box" style="width:25%;">
					<ul>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('type')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[type]" id="type" value="C" <?echo (($lease_priceInfo["type"]=='C')?'checked':''); ?> onclick="change_span('type_C');" ><?=$this->lang->line('type_1')?>
							<input type="radio" class="radio validate[required]" name="postdata[type]" id="type" value="P" <?echo (($lease_priceInfo["type"]=='P')?'checked':''); ?> onclick="change_span('type_P');" ><?=$this->lang->line('type_2')?>
						</li>
						<span id="type_P" style="display:<?echo (($lease_priceInfo["type"]=='P')?'block':'none')?>;"
						>
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('so_num')?></label>
								<select name="postdata[so_num]" id="so_num"  onchange="change_sub_num('so_num', 'sso_num', 'to_num');">
									<?=create_select_option($operator_list, $lease_priceInfo["so_num"], $this->lang->line('select_operator_class'));?>
								</select>
							</li>
							<li>
								<label for=""><?=$this->lang->line('sso_num')?></label>
								<select name="postdata[sso_num]" id="sso_num">
									<?=create_select_option($sub_operator_list, $lease_priceInfo["sso_num"], $this->lang->line('select_suboperator_class'), array("to_num"));?>
								</select>
							</li>
						</span>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('name')?></label>
							<input type="text" class="txt validate[required]" name="postdata[name]" id="name" value="<?=$lease_priceInfo['name']?>"  maxlength="30">
						</li>
						<li>		
							<label for=""><?=$this->lang->line('price')?></label>
							<input type="text" class="txt" name="postdata[price]" id="price" value="<?=$lease_priceInfo['price']?>" maxlength="5">
						</li>
						<li>		
							<label for=""><?=$this->lang->line('method')?></label>
							<input type="text" class="txt" name="postdata[method]" id="method" value="<?=$lease_priceInfo['method']?>" maxlength="1">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:25%;">
					<ul>
						<li class="auto">
							<label for=""><?=$this->lang->line('remark')?></label>
							<TEXTAREA class="txt" style="height:111px;" name="postdata[remark]" id="remark" maxlength="100"><?=$lease_priceInfo['remark']?></TEXTAREA>
						</li>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" name="postdata[status]" id="status" value="Y" <?echo (($lease_priceInfo["status"]=='Y')?'checked':''); ?>><?=$this->lang->line('enable')?>
							<input type="radio" class="radio validate[required]" name="postdata[status]" id="status" value="N" <?echo (($lease_priceInfo["status"]=='N')?'checked':''); ?>><?=$this->lang->line('disable')?>
						</li>
					</ul>
				</div>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			</form>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

function change_span(id_span){
	$j("#type_P").css('display','none');
	$j("#"+id_span).css('display','block');
}

function change_sub_num(filed_1, filed_2, filed_name){
	var filed_val = $j("#"+filed_1).val();
	
	if(filed_val == ''){
		$j("#"+filed_2+" option").show();
	}else{
		$j("#"+filed_2+ "option:selected").removeAttr("selected");
		$j("#"+filed_2+" option").hide();
		$j("#"+filed_2+" option["+filed_name+"='"+filed_val+"']").show();

		$j("#"+filed_2+" option[value='']").show();
		$j("#"+filed_2+" option[value='']").prop('selected', true);
	}
}
</script>
<?
	$web = $this->config->item('base_url');
	
	$colArr = array (
		"l_r_type"				=>	"{$this->lang->line('log_battery_l_r_type')}",
		"member_name"			=>	"{$this->lang->line('log_battery_leave_return_tm_num')}",
		"ldo_name"				=>	"{$this->lang->line('log_battery_leave_return_leave_do_num')}",
		"battery_id"			=>	"{$this->lang->line('log_battery_leave_return_battery_id')}",
		"request_date"			=>	"{$this->lang->line('log_battery_leave_return_leave_request_date')}",
		"status"				=>	"{$this->lang->line('log_battery_leave_return_leave_status')}",
		"usage_time"			=>	"{$this->lang->line('log_battery_leave_return_usage_time')}",
		"system_log_date"		=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}"
	);
	$colInfo = array("colName" => $colArr);
	
	$fn = get_fetch_class_random();
	
	$get_full_url_random = get_full_url_random();
	
	//查询网址
	$search_url = "{$get_full_url_random}/detail_search";

	$searchArr = array (
		"system_log_date"	=>	"{$this->lang->line('log_battery_leave_return_system_log_date')}",
		"ltop.top01"	=>  "{$this->lang->line('select_operator_class')}",
		"ltd.tde01"		=>  "{$this->lang->line('select_dealer_class')}",
		"rtop.top01"	=>  "{$this->lang->line('select_operator_class')}",
		"rtd.tde01"		=>  "{$this->lang->line('select_dealer_class')}"
	);

	$operatorsSelectListRow = $this->Model_show_list->getoperatorList() ;
	$dealerSelectListRow = $this->Model_show_list->getdealerList() ;

	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$fieldType = array(
		"system_log_date"	=>	"date",
		"ltop.top01"	=>  $operatorsSelectListRow,
		"ltd.tde01"		=>  $dealerSelectListRow,
		"rtop.top01"	=>  $operatorsSelectListRow,
		"rtd.tde01"		=>  $dealerSelectListRow
	);

	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt_detail');

	$exchange_type_name = array('离线交换','连线交换');
	$status_name = array('充电中','饱电');
	
	//echo 'A:'.$this->session->userdata('start_date');
?>

<style type="text/css"> 
.autobreak{
	word-break: break-all;
}
.descclass{
	vertical-align: top;
}
</style>
<link href="<?=$web.$newcss;?>newcss/fixedtable.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('battery_detail_management')?> <?=$bss_name;?>（<?=$bss_id;?>）【<?=$track_no?>】</p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="姓名" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
									<INPUT TYPE="hidden" NAME="s_num" id="s_num" value="<?=$s_num;?>">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				

				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="battery_swap_track">
					<tr class="GridViewScrollHeader">
						<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
								
			<?
						$start_row = $this->session->userdata('PageStartRow');

						//让标题栏位有排序功能
						foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/detail/".$s_num."/".$start_row."/".$main_startRow."?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center ><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
							echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
						}
			?>
					</tr>
			<?
				if(isset($InfoRow[0]['msg']))
				{
					$count = count($colInfo["colName"])+1;
					echo "<trclass=\"GridViewScrollItem\">\n<td colspan='{$count}' style='color:red'>{$InfoRow[0]['msg']}</td></tr>";
				}
				else
				{
					foreach ($InfoRow as $key => $InfoArr) {
						echo "				<tr class=\"GridViewScrollItem\">\n" ;

						$data_sn = $InfoArr["s_num"] ;
						$checkStr = "
				<td align=\"center\">
					<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$data_sn}\" >
				</td>
						" ;
						echo $checkStr ;
						foreach ($colInfo["colName"] as $enName => $chName) {
							if($enName == "l_r_type"){
								echo "					<td align=\"center\">".$InfoArr[$enName]."</td>\n" ;
							}
							else if($enName=='status')
							{
								$data = "";
								if($InfoArr[$enName]!="")
								{
									switch($InfoArr[$enName])
									{
										case 0:
											$data = "成功";
											break;
										case 1:
											$data = "机柜还电门未打开";
											break;
										case 2:
											$data = "用户未放入电池";
											break;
										case 3:
											$data = "放入非借出电池";
											break;
										case 4:
											$data = "取电门未打开";
											break;
										case 5:
											$data = "用户未取出电池";
											break;
										case "F":
											$data = "使用微信绑定";
											break;
									}
								}
								echo "					<td align=\"center\" class=\"\" >" . $data . "</td>\n";
							}else {
								echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
							}
						}
						echo "				</tr>\n" ;
					}
				}
				
			?>
				</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>

			<a href="<?=$get_full_url_random.'/index/'.$main_startRow?>" class="backbtn">回上一页</a>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="selBank" style="width: 450px; height: 140px; display:none;"></div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>

<script type="text/javascript">
	$j(document).ready(function() {
		$j('a.close, #modal').live('click', function() {
			// close the overlay
			triggersOnload.eq(0).overlay().close();
			return false;
		});
		//凍結窗格
		gridview(0,false);
	});

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {

	// close the overlay
	var k=1;
	for(var i=0 ;i <=k; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

    /**
     * 搜寻
     */
    $j('#btSearch, #btSearch2').click(function () {
		$j("#loadingIMG").show();

		//sumit前要更新 s_search_txt
		$j("#search_txt").val($j("#s_search_txt").val());
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

    $j('#searchData').change(function () {
        $j("#searchForm").attr( "method", "POST" ) ;
        $j("#searchForm").attr( "action", "<?=$search_url;?>" ) ;
        $j("#searchForm").submit() ;
    });

	//关闭ESC键功能
document.onkeydown = killesc; 
function   killesc() 

{   
	if(window.event.keyCode==27)   
	{   
		window.event.keyCode=0;   
		window.event.returnValue=false;   
	}   
} 

$j(".tr_function_name").hide();

</script>
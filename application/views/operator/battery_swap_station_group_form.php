<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"s_num"				=> "",
		"group_name"		=> "",
		"all_sb_num"		=> ""
	);
	
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $dataInfo["s_num"]);

	$msg = "";
	$class = "validate[required]";

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
label{
	display: inline;

}
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 500px;
	margin: 0px auto;
	float: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
<!-- 下拉搜尋 多筆勾選 -->
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>

<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('battery_swap_station_group_management')?><span><?=$show_sub_title;?></span></p>
		<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$dataInfo['s_num']?>" />
				<div class="container-box" style="width:48%;">
					<ul>
						<li>
							<label><?=$this->lang->line('battery_swap_station_group_group_name')?></label>
							<input type="text" class="txt" name="postdata[group_name]" id="group_name" value="<?=$dataInfo['group_name']?>" style="width:100%;">
						</li>
						<li>
							<label><?=$this->lang->line('battery_swap_station_group_all_sb_num')?></label>
							<select multiple="multiple" id="all_sb_num" name="all_sb_num[]" style="width: 385px;">
<?
								$all_arr = explode(',',$dataInfo['all_sb_num']);
								foreach($batteryswapstationList as $i_arr){
									$selected = "";
									if(in_array($i_arr['s_num'], $all_arr)){
										$selected = "selected";
									}
									echo '<option value="'.$i_arr['s_num'].'" '.$selected.'>'.$i_arr['bss_id_name'].'</option>';
								}
?>
							</select>	
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:4%;">
					&nbsp;
				</div>
				<div class="container-box" style="width:48%;">
					<div id="current-selection" class="sol-current-selection">
					</div>
				</div>
			</form>
		</div>
		<div class="sectin buttonbar" id="body">
			<?=$user_access_control?>
		</div>
	</div>
</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});

	//储存
	function fn_save(){
		$j("#formID").submit();
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
//alert($j.fn.jquery);  //顯示目前$j是誰在用

var arr = new Array('all_sb_num');

var mutiselectObjArr = new Array();
var y = 0;

arr.forEach(function(value) {
	mutiselectObjArr[y] = $j_1102('#'+value).searchableOptionList({
		showSelectAll: true,
		events: {
			maxwidth: '620' //設定寬度
		},
		texts: {
			noItemsAvailable: '查无此资料',
			selectNone: '清除选择',
			selectAll: '全选',
			searchplaceholder: '选择租借站'
		}
	});

	y++;
});

</script>
<?php
	$web = $this->config->item('base_url');
	if(count($fileurlArr)==0)
	{
		$fist_imgurl = "{$web}advertising/gray.jpg";
	}
	else
	{
		$fist_imgurl = $fileurlArr[0];
	}
?>
<html>
	<head>
		<title>魔力廣告</title>
		<script src="<?=$web?>/js/jquery-1.8.2.min.js"></script>
		<script src="<?=$web?>/js/index_kv.js"></script>
	</head>
	<style>
	.index_newsList {
		position: absolute;
		top: 50%;
		right: 5%;
	}

	.index_newsList li {
  		float: left;
  	}
  	.index_newsList li {
  		background-repeat: no-repeat;
  		display: block;
  	
  		text-indent: -9999px;
  	}
  	.index_articleL {
  	
  		float: left;
  	}

  	#adimg{
  		width:100%;
  		height:100%;
  	}

  	body{
  		margin:0;
  	}
	</style>
	<body>
		<div class="divleft">
			<!--<img class="centerimg" src="./img/indeximg/index_center1.png">-->
			<section class="index_content">
				<img id="adimg" src="<?=$fist_imgurl?>" alt="test">
				<ul class="index_newsList">
					<?php 
						$contentArr = $fileurlArr;
						// print_r($contentArr);
						$i = 0;
						foreach($contentArr as $key => $value)
						{
							if(trim($value) != "")
							{
								if($key == 0)
								{
									$class = "current";
									$i++;
								}
								else
								{
									$class = "";
								}

								echo "<li class='{$class}'>{$value}</li>";
							}
						}
					?>
			    </ul>
		 	</session>
		</div>
	</body>
</html>
<?
	$web = $this->config->item('base_url');
	
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"shortcut_sn"			=> "",
		"menu_sn"				=> "",
		"shortcut_image_name"	=> "",
		"shortcut_sequence"		=> "",
		"status"				=> "1"
	);

	$shortcutInfo = isset($shortcutInfo) ? $shortcutInfo : $spaceArr ;

	if($shortcutInfo["shortcut_sn"] != "")
		$Disable = "disabled";

	$Sel1 = "";
	$Sel0 = "";
	if ($shortcutInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($shortcutInfo["status"] == "0") {
		$Sel0 = " checked";
	}
?>
<a href="#" class="close"></a>
<form id="formID" action="<?=$web?>nimda/shortcut/modification_db" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<input type="hidden" name="start_row" value="<?=$StartRow?>" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<fieldset>
	  		<p>
				<label><?=$this->lang->line('shortcut_menu_name')?></label>
				<select name="selMenuSN" <?=$Disable?>>
					<option value="">-- <?=$this->lang->line('shortcut_select_menu')?> --</option>			
<?
					foreach ($menuSelectListRow as $key => $menuSelectListArr)
					{
						if($shortcutInfo["menu_sn"] == $menuSelectListArr["menu_sn"])
							$selected = " selected";
						else
							$selected = "";
						echo "<option value=\"{$menuSelectListArr["menu_sn"]}\" {$selected}>{$menuSelectListArr["menu_name"]}</option>\n";
					}
?>
				</select>
			</p>
			<p>
				<label class="required"><?=$this->lang->line('shortcut_image')?></label>
				<input type="text" class="validate[required] text-input" style="width: 285px;" maxlength="30" name="txtChImageName" placeholder="<?=$this->lang->line('shortcut_input_image')?>" value="<?=$shortcutInfo["shortcut_image_name"]?>">
				<input type="hidden" name="shortcut_sn" value="<?=$shortcutInfo["shortcut_sn"]?>" />
			</p>
			<p>
				<label><?=$this->lang->line('shortcut_sequence')?></label>
				<input type="text" class="validate[required,minSize[1],custom[onlyNumberSp]]" style="width: 80px;" maxlength="2" name="txtChSequence" placeholder="<?=$this->lang->line('shortcut_input_sequence')?>" value="<?=$shortcutInfo["shortcut_sequence"]?>">
			</p>
			<p>
				<label><?=$this->lang->line('shortcut_status')?></label>
				<input name="selStatus" type="radio" value="1"<?=$Sel1?>><span style='font-size: 18px'><?=$this->lang->line('enable')?><?=$this->lang->line('space')?></span>
				<input name="selStatus" type="radio" value="0"<?=$Sel0?>><span style='font-size: 18px'><?=$this->lang->line('disable')?></span>
			</p>
		</fieldset>
		<br>
		<tr><td align="center">
			<p><input type="submit" value="<?=$this->lang->line('confirm')?>"></p>
		</td></tr>
	</table>
</form>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
	});
</script>
<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"menu_sn"				=> "",
		"menu_name"				=> "",
		"menu_name_english" 	=> "",
		"menu_directory"		=> "",
		"menu_image"			=> "",
		"menu_content"			=> "",
		"parent_menu_sn"		=> "",
		"second_menu_sn"		=> "",
		"login_side_display"	=> "2",
		"menu_sequence"			=> "",
		"icon_class"		=> "",
		"access_control"		=> "",
		"status"				=> ""
	);

	$menuInfo = isset($menuInfo) ? $menuInfo : $spaceArr ;

	$Sel1 = "";
	$Sel0 = "";
	if ($menuInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($menuInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($menuInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	$login_side_display0 = "";
	$login_side_display1 = "";
	$login_side_display2 = "";
	if($menuInfo["login_side_display"] == "0"){
		$login_side_display0 = " checked";
	}else if($menuInfo["login_side_display"] == "1"){
		$login_side_display1 = " checked";
	}else{
		$login_side_display2 = " checked";
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

	$show_sub_title = $this->model_access->showsubtitle($bView, $menuInfo["menu_sn"]);
?>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('menu_management')?><span><?=$show_sub_title;?></span></p>
			<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<div class="container-box" style="width:33%;">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('menu_parent_name')?></label>
							<select name="selParentMenuSN" style="width: 320px;">
								<option value="">-- <?=$this->lang->line('menu_select_parent_name')?> --</option>			
			<?
								foreach ($menuSelectListRow as $key => $menuSelectListArr)
								{
									if($menuInfo["parent_menu_sn"] == $menuSelectListArr["menu_sn"])
										$selected = " selected";
									else
										$selected = "";
									echo "<option value=\"{$menuSelectListArr["menu_sn"]}\" {$selected}>{$menuSelectListArr["menu_name"]}</option>\n";
								}
			?>
							</select>
						</li>
						<li>
							<label for=""><?=$this->lang->line('menu_second_menu_sn')?></label>
							<select name="selSecondMenuSN" style="width: 320px;">
								<option value="">-- <?=$this->lang->line('menu_select_second_name')?> --　</option>			
			<?
								foreach ($menuSelectSecondListRow as $key => $menuSelectListArr)
								{
									if($menuInfo["second_menu_sn"] == $menuSelectListArr["menu_sn"])
										$selected = " selected";
									else
										$selected = "";
									echo "<option value=\"{$menuSelectListArr["menu_sn"]}\" {$selected}>{$menuSelectListArr["menu_name"]}</option>\n";
								}
			?>
							</select>
						</li>
						<li>
							<label for=""><?=$this->lang->line('menu_access_control')?></label>
							<input type="hidden" name="org_access_control" value="<?=$menuInfo["access_control"]?>" />
							<div class="div_access_area">	
<?
								$i = 1;
								$access_control = $menuInfo["access_control"];
								
								foreach($arr_access_control_list as $key => $value){
									$checked = '';
									if(($access_control & $key) === $key){
										$checked = 'checked';
									}
									echo "<input name=\"access_control[]\" type=\"checkbox\" value=\"{$key}\" {$checked} class=\"chk\">{$value}{$this->lang->line('space')}";
									if($i%3 == 0){
										echo '<br />';
									}
									$i++;
								}
?>
							</div>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:29%;">
					<ul>
						<li>
							<label for="" class="required"><?=$this->lang->line('menu_name')?></label>
							<input type="text" class="txt validate[required] text-input" style="width:295px;" maxlength="20" name="txtChMenuName" value="<?=$menuInfo["menu_name"]?>">
							<input type="hidden" name="menu_sn" id="menu_sn" value="<?=$menuInfo["menu_sn"]?>" />
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('menu_menu_name_english')?></label>
							<input type="text" class="txt validate[required] text-input" style="width:295px;" maxlength="40" name="txtChMenuEnName" value="<?=$menuInfo["menu_name_english"]?>">
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('menu_menu_directory')?></label>
							<input type="text" class="txt validate[required] text-input" style="width:295px;" maxlength="50" name="txtChImageDirectory" value="<?=$menuInfo["menu_directory"]?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:16%;">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('menu_menu_sequence')?></label>
							<input type="text" class="txt validate[required,minSize[1],custom[onlyNumberSp]]" maxlength="3" name="txtChSequence" value="<?=$menuInfo["menu_sequence"]?>">
						</li>
						<li>
							<label for=""><?=$this->lang->line('menu_menu_content')?></label>
							<input type="text" class="txt" maxlength="100" name="txtmenu_content" value="<?=$menuInfo["menu_content"]?>">
						</li>
						<li>
							<label for=""><?=$this->lang->line('icon_class')?></label>
							<input type="text" class="txt" maxlength="100" name="icon_class" value="<?=$menuInfo["icon_class"]?>">
						</li>
					</ul>
				</div>
				<div class="container-box" style="position:relative; width:22%;" id='upmenuimg'>
					<span class="show" id="show_default_img" style="position:absolute; top:0; right:0; width:40px; height:40px;">
							<div id="defalut_img" style="display:<?echo (($menuInfo["menu_image"] == '')?'flex':'none')?>;">
								<img src="<?=$web?>images/icon_account.png" alt="">
							</div>
							<div id="upload_img">
							<?php
								$del_button = '';
								if($menuInfo["menu_image"] != ''){
									echo "<img src='{$web}img/shortcut/{$menuInfo["menu_image"]}'>";
									echo "<input type='hidden' name='menu_image' id='menu_image' value='{$menuInfo["menu_image"]}'>";
									$del_button  = "<input type='button' class='btn' name='deletemenuimg' id='deletemenuimg' value='{$this->lang->line('delete_img')}'>";
								}
							?>
							</div>
					</span>
					<ul>
						<li>
							<label for=""><?=$this->lang->line('menu_menu_image')?><br><span class="notice">100px * 100px</span></label>
							<div id="del_button" style="display:<?=(($del_button != '')?'block':'none')?>;">
								<?echo $del_button;?>
							</div>
							<div id="show_images" style="display:<?=(($del_button == '')?'block':'none')?>;">
								<input type="file" onchange="$j('#file_txt').val($j('#menu_image').val());" name="menu_image" id="menu_image" class="imgbtn" style="display:none;">
								<input type="text" class="txt" value="" id="file_txt" name="file_txt" readonly><input type="button" value="浏览" class="btn" id="id_image_large">
							</div>
						</li>
						<li>
							<label for=""><?=$this->lang->line('menu_login_side_display')?></label>
							<input  name="sellogin_side_display" type="radio" class="radio first"  value="0" <?=$login_side_display0?>><?=$this->lang->line('login_side_0')?><?=$this->lang->line('space')?>
							<input  name="sellogin_side_display" type="radio" class="radio"  value="1" <?=$login_side_display1?>><?=$this->lang->line('login_side_1')?><?=$this->lang->line('space')?>
							<input  name="sellogin_side_display" type="radio" class="radio"  value="2" <?=$login_side_display2?>><?=$this->lang->line('login_side_2')?><?=$this->lang->line('space')?>
						</li>
						<li>
							<label for=""><?=$this->lang->line('menu_status2')?></label>
							<input name="selStatus"  type="radio" class="radio first"  value="1"<?=$Sel1?>><?=$this->lang->line('enable')?><?=$this->lang->line('space')?>
							<input name="selStatus" type="radio" class="radio" value="0"<?=$Sel0?>><?=$this->lang->line('disable')?>
						</li>
					</ul>
				</div>
				</form>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		
		//储存和取消
		save_cancel();
	});
	
	$j('#deletemenuimg').click(function(){
			var menu_sn = $j('#menu_sn').val() ;
			var menu_image = 'menu_image';
			
			$j.ajax({
				type:'post',
				url: '<?=$web?>nimda/menu/deletemenuimg',
				data: {menu_sn:menu_sn,menu_image:menu_image, 
						<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
				error: function(xhr) {
					strMsg += 'Ajax request发生错误[background.php]\n请重试';
					alert(strMsg);
				},
				success: function (response) {
					if(response != "")
					{
						$j("#defalut_img").css('display','block');
						$j("#show_images").css('display','block');
						$j("#del_button").css('display','none');
						$j("#upload_img").html('');
					}
				}
			}) ;
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#id_image_large").click(function(){
	$j("#menu_image").click();
});
</script>
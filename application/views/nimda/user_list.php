<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"user_name"				=>	"{$this->lang->line('user_user_name')}",
		"user_id"				=>	"{$this->lang->line('user_user_id')}",
		"group_name"			=>	"{$this->lang->line('user_group_sn')}",
		"status"				=>	"{$this->lang->line('user_status')}"
	);
	$colInfo = array("colName" => $colArr);

	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";	
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData);

?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
		<!-- 先隐藏
		<!-- Search Starts Here -->
		<!-- <div id="searchContainer">
			<a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
			<div style="clear:both"></div>
			<div id="searchBox">                
				<form id="searchForm" method="post" action="<?=$search_url?>">
					<div id="closeX" style="color:red;text-align:right;cursor: pointer;z-index:9999;top:0px;right:-20px;position:absolute;" ><img src="<?=$web?>img/delete.png" style="width:30px;height:30px;" /></div>
					<!--预防CSRF攻击-->
		<!-- 			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					<fieldset2 id="body">
						<fieldset2>
							<?=$search_box?>
							<input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
						</fieldset2>
					</fieldset2>
				</form>
			</div>
		</div>
		<!-- Search Ends Here -->
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('user_management')?></p>
				<div class="topbtn"><?=$user_access_control?></div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="user">
					<tr class="GridViewScrollHeader">
						<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($userInfoRow as $key => $userInfoArr) {
		echo "				<tr class=\"GridViewScrollItem\">\n" ;

		$user_sn = $userInfoArr["user_sn"] ;
		$checkStr = "
<td  align=\"center\">
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$user_sn}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "sex") {
				switch($userInfoArr[$enName]) {
					case "M":
						echo "					<td align=\"center\">{$this->lang->line('male')}</td>\n" ;
						break;
					case "F":
						echo "					<td align=\"center\">{$this->lang->line('female')}</td>\n" ;
						break;
				}
			} else if ($enName == "mobile" || $enName == "group_name") {
				echo "					<td align=\"center\">".$userInfoArr[$enName]."</td>\n" ;
			} else if ($enName == "merchant_name_chinese") {
				echo "					<td align=\"center\">".$userInfoArr["merchant_name_chinese"]."</td>\n" ;
			} else if ($enName == "status") {
				switch($userInfoArr[$enName]) {
					case "1":
						echo "					<td align=\"center\"><font color=blue>{$this->lang->line('enable')}</font></td>\n" ;
						break;
					case "0":
						echo "					<td align=\"center\"><font color=blue>{$this->lang->line('disable')}</font></td>\n" ;
						break;
					case "D":
						echo "					<td align=\"center\"><font color=red>{$this->lang->line('delete')}</font></td>\n" ;
						break;
					default:
						echo "					<td align=\"center\"><font color=red></font></td>\n" ;
						break;
				}
			} else {
				echo "					<td align=\"left\">".$userInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<!-- div class="modal" id="prompt" style="width: 750px; height: 475px;">
</div -->
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});


/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

// $j('#searchData').change(function () {
	// $j("#searchForm").attr( "method", "POST" ) ;
	// $j("#searchForm").attr( "action", "<?=$web?>nimda/user/search" ) ;
	// $j("#searchForm").submit() ;
// });

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});


$j("#btExport").click( function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$web?>nimda/export/export" ) ;
	$j("#searchForm").submit() ;
	// $j.ajax({
		// type:'post',
		// url: '<?=$web?>nimda/export/export',
		// data: {StartRow:<?=$this->session->userdata('PageStartRow')?>, 
				// <?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		// error: function(xhr) {
			// strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			// alert(strMsg);
		// },
		// success: function (response) {
		// }
	// }) ;
});

</script>

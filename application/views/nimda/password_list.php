<?
	$web = $this->config->item('base_url');
	
	$ActionLink = $web."nimda/password/change_password_db";
		
	if($this->session->userdata("pwd_deadline") == "0000-00-00"){	//首次登入
		$title = $this->lang->line('password_first_login');
	}else{
		$title = '';
	}
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<div class="wrapper">
	<div class="box">
		<p class="btitle"><?=$this->lang->line('password_change')?></p>
		<div class="section">
		<form id="formID" action="<?=$ActionLink?>" method="post">	<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<div class="container-box pwdbox">
				<fieldset>
					<ul>
						<li>
							<label class="required inb"><?=$this->lang->line('password_current')?></label>
							<input type="password" class="validate[required] text-input txt" maxlength="12"  name="current_pwPswd" id="current_pwPswd" placeholder="<?=$this->lang->line('password_input_current')?>">
						</li>
						<li>
							<label class="inb"><?=$this->lang->line('password_new')?></label>
							<input type="password" class="validate[required,minSize[7],maxSize[12],custom[checkpassword],ajax[ajaxOldPassword]] text-input txt" maxlength="12" name="pwPswd" id="pwPswd" placeholder="<?=$this->lang->line('password_input_new')?>">
						</li>
						<li>
							<label class="inb"><?=$this->lang->line('password_new_confirm')?></label>
							<input type="password" class="validate[required,equals[pwPswd]] text-input txt" maxlength="12" name="pwPswd2" placeholder="<?=$this->lang->line('password_input_new_confirm')?>">
						</li>
					</ul>
					
					
				</fieldset>
			</div>
		</div>
		<div class="sectin buttonbar">
				<input type="submit" value="<?=$this->lang->line('confirm')?>"  class="buttondark">
		</div>
		</form>
	</div>
</div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="chgPassword" style="width: 225px; height: 185px;"></div>
<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});


$j(document).ready(function() {
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
	});
	
	//储存和取消
	save_cancel();
	
	//锁住form, Enter无法处发送出
	//$j('#formID').attr("onsubmit", "return false");
	
	//按下sumit
	//储存
	function fn_save(event){
		// var sMsg = "";
		// sMsg = sMsg + "按下\n";
		// alert(sMsg);
		// sMsg = sMsg + "送出\n";
		var current_pwPswd = $j("#current_pwPswd").val();
		//sMsg = sMsg + "ajax 确认旧密码是否正确\n";
		//alert(sMsg);
		//确认旧密码是否正确
		$j.ajax({
			type:'post',
			url: '<?=$web?>nimda/password/check_current_password',
			data: {current_pwPswd: current_pwPswd, 
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
			async: false,
			error: function(xhr) {
				//sMsg = sMsg + "ajax有误\n";
				//alert(sMsg);
				strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
				alert(strMsg);
			},
			success: function (response) {
				//sMsg = sMsg + "ajax 回传:"+response+"\n";
				//alert(sMsg);
				if(response == '1'){	//继续执行
					//$j('#formID').attr("onsubmit", "return true");
					//sMsg = sMsg + "资料正确, 执行submit\n";
					//alert(sMsg);
					//$j("#formID").submit();	// 不需要再submit一次
					
				}else{
					//sMsg = sMsg + "资料有误\n";
					//alert(sMsg);
					alert('<?=$this->lang->line('password_current_error')?>');
					$j("#current_pwPswd").val("");
					event.preventDefault();
					//return false;
				}
			}
		}) ;
	}
	
});
</script>

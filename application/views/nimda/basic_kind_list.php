<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"kind_no"				=>	"{$this->lang->line('basic_kind_kind_no')}",
		"kind_name"				=>	"{$this->lang->line('basic_kind_kind_name')}", 
		"sys_mark"				=>	"{$this->lang->line('basic_kind_sys_mark')}", 
		"remark"				=>	"{$this->lang->line('basic_kind_remark')}" 
	);
	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();
	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($colArr, $searchData);
?>

<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>
	<div id="container">
		<div id="body">
			<!-- 以下为内容 -->
			<code>
				<table width="100%" border="0">
					<tr>
						<td align="left">
							<div style="font-size: 28px; color:#464D57; position:relative; top:3px"><?=$this->lang->line('basic_kind_management')?></div>
						</td>
						<!--使用功能权限-->
						<?=$user_access_control?>
					
						<td style="text-align:right;width:60px">
				            <!-- Search Starts Here -->
				            <div id="searchContainer">
				                <a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
				                <div style="clear:both"></div>
				                <div id="searchBox">                
				                    <form id="searchForm" method="post" action="<?=$search_url?>">
				                    	<div id="closeX" style="color:red;text-align:right;cursor: pointer;z-index:9999;top:0px;right:-20px;position:absolute;" ><img src="<?=$web?>img/delete.png" style="width:30px;height:30px;" /></div>
										<!--预防CSRF攻击-->
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				                        <fieldset2 id="body">
				                            <fieldset2>
				                                <?=$search_box?>
				                                <input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
				                            </fieldset2>
				                        </fieldset2>
				                    </form>
				                </div>
				            </div>
				            <!-- Search Ends Here -->
						</td>
					</tr>
				</table>
			</code>
<!-- 列表区 -->
			<div id="listArea">
<form id="listForm" name="listForm" action="">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" width="100%"  border="0" cellspacing="1" cellpadding="3">
				<tr>
					<th width="1%">
						<input type="checkbox" name="ckbAll" id="ckbAll" value="">
					</th>
					
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							
							//栏位显示
                            echo "					<th align=\"center\">
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
				</tr>
<?
	foreach ($InfoRow as $key => $InfoArr) {
		echo "				<tr>\n" ;

		$kind_no = $InfoArr["kind_no"] ;
		$checkStr = "
<td>
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$kind_no}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if($enName == "kind_no") {
				echo "					<td align=\"center\">".$InfoArr[$enName]."</td>\n" ;
			} else {
				echo "					<td align=\"center\">".$InfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
</form>
<!-- 分页区 -->
<div class="pagination">
	<?=$pageInfo["html"]?>
</div>
			</div>
		</div>
	</div>

<!-- 新增/修改 页面 -->
<!-- user input dialog -->
<div class="modal" id="prompt" style="width: 460px; height: 350px;">
</div>
<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//功能按钮显示控制
	button_display();
});

</script>

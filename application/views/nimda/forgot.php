<?
	$web = $this->config->item('base_url');
	$newcss = "";
	if($this->session->userdata('display_language') == 'zh_kr'){
		$newcss = "kr_";
	}

?>
<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>优力电驱动系统有限公司</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width">
		<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
		<link href="<?=$web.$newcss;?>newcss/default.css" rel="stylesheet" media="screen">
		<link href="<?=$web.$newcss;?>newcss/fontawesome.css" rel="stylesheet" media="screen">
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>
	<body id="forgot">
		<form id="formID">
			<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<div class="forgotbox">
				<p><i class="fas fa-question-circle"></i>忘记密码</p>
				请输入注册时所提供的 e-mail<br/>
				<input type="text" id="recoverEmail" name="recoverEmail">
				<a class="button" onclick="forgotmail();">送出</a>
			</div>
		</form>
	</body>
</html>
<script>
	function forgotmail(){
			var recoverEmail = $("#recoverEmail").val();
			/* AJAX 比对资料库 */
			$.post("<?=$web?>nimda/login/forgotemail", {
				recoverEmail: recoverEmail, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'
			}, function( response ) {
				if( response == "0" ) {
					alert("<?=$this->lang->line('login_email_wrong_email_address');?>");
				}
				else if(response == "1"){
					alert("<?=$this->lang->line('login_email_right_email_address');?>");
					parent.location.href = "<?=$web;?>nimda/login";

				}
				else
				{
					alert(response);
				}
			});
	}
</script>
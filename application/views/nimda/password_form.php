<?
	$web = $this->config->item('base_url');

	if($mode == "front")
		$ActionLink = $web."testing/run_test/change_password_db";
	else
		$ActionLink = $web."nimda/password/change_password_db";
		
	if($this->session->userdata("pwd_deadline") == "0000-00-00"){	//首次登入
		$title = $this->lang->line('password_first_login');
	}else{
		$title = '';
	}
?>
	<div class="box pwd">
		
		<p class="btitle">修改密码</p>
		
		<div class="section">
		<form id="formID" action="<?=$ActionLink?>" method="post">	<!--预防CSRF攻击-->
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<div class="allbox password">
				<div id="title"><label id="label_title"><?=$title?></label></div>
		<fieldset>
			<p>
				<label class="required"><?=$this->lang->line('password_current')?></label>
				<input type="password" class="validate[required] text-input txt" style="width: 180px;" maxlength="12"  name="current_pwPswd" id="current_pwPswd" placeholder="<?=$this->lang->line('password_input_current')?>">
			</p>
			<p>
				<label><?=$this->lang->line('password_new')?></label>
				<input type="password" class="validate[required,minSize[7],maxSize[12],custom[checkpassword],ajax[ajaxOldPassword]] text-input txt" style="width: 180px;" maxlength="12" name="pwPswd" id="pwPswd" placeholder="<?=$this->lang->line('password_input_new')?>">
			</p>
			<p>
				<label><?=$this->lang->line('password_new_confirm')?></label>
				<input type="password" class="validate[required,equals[pwPswd]] text-input txt" maxlength="12" style="width: 180px;"  name="pwPswd2" placeholder="<?=$this->lang->line('password_input_new_confirm')?>">
			</p>
		</fieldset>
			</div>
		</div>
		<div class="sectin buttonbar">
			<p><input type="submit" value="<?=$this->lang->line('confirm')?>" class="buttongreen"></p>
		</div>
		</form>
	</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
	});
	
	//锁住form, Enter无法处发送出
	//$j('#formID').attr("onsubmit", "return false");
	
	//按下sumit
	$j(":submit").click(function(event){
		var change_type = "<?=$change_type?>";
		// var sMsg = "";
		// sMsg = sMsg + "按下\n";
		// alert(sMsg);
		// sMsg = sMsg + "送出\n";
		var current_pwPswd = $j("#current_pwPswd").val();
		//sMsg = sMsg + "ajax 确认旧密码是否正确\n";
		//alert(sMsg);
		//确认旧密码是否正确
		$j.ajax({
			type:'post',
			url: '<?=$web?>nimda/password/check_current_password',
			data: {current_pwPswd: current_pwPswd, 
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
			async: false,
			error: function(xhr) {
				//sMsg = sMsg + "ajax有误\n";
				//alert(sMsg);
				strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
				alert(strMsg);
			},
			success: function (response) {
				//sMsg = sMsg + "ajax 回传:"+response+"\n";
				//alert(sMsg);
				if(response == '1'){	//继续执行
					if(change_type == 'main'){
						//$j('#formID').attr("onsubmit", "return true");
						//sMsg = sMsg + "资料正确, 执行submit\n";
						//alert(sMsg);
						$j("#formID").submit();	// 不需要再submit一次
					}
					
				}else{
					//sMsg = sMsg + "资料有误\n";
					//alert(sMsg);
					alert('<?=$this->lang->line('password_current_error')?>');
					$j("#current_pwPswd").val("");
					event.preventDefault();
					//return false;
				}
			}
		}) ;
	});

</script>
<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"sn"				=> "", 
		"kind_no"			=> "",
		"kind_name"			=> "", 
		"remark"			=> "", 
		"sys_mark"			=> "Y"
	);

	if(isset($dataInfo)){  //修改
		$insert = false;
	}else{  //新增
		$insert = true;
	}
	$dataInfo = isset($dataInfo) ? $dataInfo : $spaceArr ;
	
	//类别明细
	$basic_codeArr[0] = array(
		"code_no" 			=> "", 
		"code_name" 		=> "", 
		"remark" 			=> "", 
		"sys_mark" 			=> "Y"
	);
	$basic_code_list = isset($basic_code_list) ? $basic_code_list : $basic_codeArr ;
	
	if($insert){ //新增 
		$ajax_pk_kind_no = ", ajax[ajax_pk_kind_no]";
		$kind_no_readonly = "";
	}else{  //修改
		$ajax_pk_kind_no =  "";
		$kind_no_readonly = " readonly ";
	}

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
?>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<script type="text/javascript">
//将jquery.tools.min.js 的jquery 改为$auto, 避免与原始版本的jquery冲突
var $auto = jQuery.noConflict();
</script>
<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->

<!--#formID input:text自动产生hint start-->
<script type="text/javascript" src="<?=$web?>js/common/input_hint.js"></script> 
<!--#formID input:text自动产生hint end-->

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->


<style type="text/css"><?/* <!-- 页签css设定 --> */?>
	ul, li {
		margin: 0;
		padding: 0;
		list-style: none;
	}
	.abgne_tab {
		/** clear: left; **/
		width: 100%;
		margin: 10px 0;

	}
	ul.tabs {
		width: 100%;
		height: 32px;
		border-bottom: 1px solid #999;
		border-left: 1px solid #999;
	}
	ul.tabs li {
		float: left;
		height: 31px;
		line-height: 31px;
		overflow: hidden;
		position: relative;
		margin-bottom: -1px;	/* 让 li 往下移来遮住 ul 的部份 border-bottom */
		border: 1px solid #999;
		border-left: none;
		background: #e1e1e1;
	}
	ul.tabs li a {
		display: block;
		padding: 0 20px;
		color: #000;
		border: 1px solid #fff;
		text-decoration: none;
	}
	ul.tabs li a:hover {
		background: #ccc;
	}
	ul.tabs li.active  {
		background: #fff;
		border-bottom: 1px solid #fff;
	}
	ul.tabs li.active a:hover {
		background: #fff;
	}
	div.tab_container {
		/** clear: left; **/
		width: 100%;
		border: 1px solid #999;
		border-top: none;
		background: #fff;
		height: 260px;
	}
	div.tab_container .tab_content {
		padding: 20px;
	}
	.tab_content{
		/*
		height: 210px;
		width: 420px;
		overflow-y: auto;
		*/
	}
	.tab_content_detail{
		height: 210px;
		width: 420px;
		overflow-y: auto;
	}
	div.tab_container .tab_content h2 {
		margin: 0 0 20px;
	}
	.clear {
	  clear : left;
	  height: 0px;
	}
	.disabled{
		color: #919191;
	}
	.display_none{
		display: none;
	}
</style>

<style type="text/css"> 
.display_inline{
	display: inline-block;
}
.label_top{
	margin-top: 0px;
}
.p_bottom{
	margin-bottom: 0px;
}
.reset_margin2{
	margin-bottom: 0px;
}
.f_left0{
	margin-left: 0px;
}
.display_none{
	display: none;
}
.display_block{
	display: block;
}
.mtop{
	margin-top: 20px;
}
.f_button{
	width: 27px;
}
.p_empty{
	height: 42px;
}
#remark{
	width: 181px;
	height: 57px;
	resize: none;
}
</style>

<link rel="stylesheet" type="text/css" href="<?=$web?>css/cpanel.css"/>

	<div id="container">
		<div id="body">
			<!-- 以下为内容 -->
			
				<table width="100%" border="0">
					<tr>
						<td align="left">
							<div style="font-size: 28px; color:#464D57; position:relative; top:3px"><?=$this->lang->line('basic_kind_management')?></div>
						</td>
						<?=$user_access_control?>
					</tr>
				</table>
			

<form id="formID" action="<?=$action?>" method="post">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
	<input type="hidden" name="start_row" value="<?=$StartRow?>" />
	<input type="hidden" name="sn" value="<?=$dataInfo["sn"]?>" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td>
<!--分页tag-->
<div class="abgne_tab">
	<ul class="tabs">
		<li><a href="#tab1" tab_link="#tab1"><?=$this->lang->line('basic_kind_label_tab1')?></a></li>
		<li><a href="#tab2" tab_link="#tab2"><?=$this->lang->line('basic_kind_label_tab2')?></a></li>
	</ul>
	<div class="tab_container">
		<div id="tab1">
			<fieldset>
				<p class="display_inline">
					<label><?=$this->lang->line('basic_kind_label_kind_no')?></label>
					<input type="text" id="kind_no" class="validate[required,minSize[1], <?=$ajax_pk_kind_no?>] <?=$kind_no_readonly?>" style="width: 50px;" maxlength="2" name="postdata[kind_no]" placeholder="" value="<?=$dataInfo["kind_no"]?>" <?=$kind_no_readonly?>>
				</p>
				<p class="display_inline">
					<label><?=$this->lang->line('basic_kind_label_sys_mark')?></label>
					<input type="text" id="sys_mark" class="" style="width: 50px;" maxlength="1" name="postdata[sys_mark]" placeholder="" value="<?=$dataInfo["sys_mark"]?>">
				</p>
				<p>
					<label class="required"><?=$this->lang->line('basic_kind_label_kind_name')?></label>
					<input type="text" id="kind_name" class="validate[required]" style="width: 182px;" maxlength="20" name="postdata[kind_name]" placeholder="" value="<?=$dataInfo["kind_name"]?>">
				</p>
				<p>
					<label><?=$this->lang->line('basic_kind_label_remark')?></label>
					<textarea id="remark" name="postdata[remark]" placeholder=""><?=$dataInfo["remark"]?></textarea>
				</p>

			</fieldset>
		</div>
		<div id="tab2" class="tab_content">
			<div><img class="add_ter" parent="tab2" src="<?=$web?>img/add.png" width="24"></img></div>
			<div class="tab_content_detail">
<?
		//类别明细
		$hidden_sn = 0;
		$mtop= "";
		foreach($basic_code_list as $key => $value){
			$hidden_sn = $key;
			
?>
			<div class="div_<?=$key?> div_panel" setno="<?=$key?>">
				<fieldset>
					<fieldset class="f_left0 f_button">
<?
			$add_display = "display_none";
			if($key == 0){
				$add_display = "";
			}else{
				$mtop = "mtop";
			}
?>
						<br class="<?=$add_display?>"  />
						<img class="del_ter <?=$mtop?>" parent="tab2" setno="<?=$key?>" src="<?=$web?>img/delete2.png" width="24"></img>
					</fieldset>
					<fieldset class="f_left0">
						<label><?=$this->lang->line('basic_code_label_code_no')?></label>
						<input type="text" class="" style="width: 60px;" maxlength="6" orgname="code_no" name="code_no_<?=$key?>" placeholder="" value="<?=$value["code_no"]?>">
					</fieldset>
					<fieldset class="f_left0">
						<label><?=$this->lang->line('basic_code_label_code_name')?></label>
						<input type="text" class="" style="width: 100px;" maxlength="30" orgname="code_name" name="code_name_<?=$key?>" placeholder="" value="<?=$value["code_name"]?>">
					</fieldset>
					<fieldset class="f_left0">
						<label><?=$this->lang->line('basic_code_label_remark')?></label>
						<input type="text" class="" style="width: 100px;" maxlength="255" orgname="remark" name="remark_<?=$key?>" placeholder="" value="<?=$value["remark"]?>">
					</fieldset>
					<fieldset class="f_left0">
						<label><?=$this->lang->line('basic_code_label_sys_mark')?></label>
						<input type="text" class="" style="width: 20px;" maxlength="1" orgname="sys_mark" name="sys_mark_<?=$key?>" placeholder="" value="<?=$value["sys_mark"]?>">
					</fieldset>
				</fieldset>
			</div>
<?
		}
?>			
			<input type="hidden" class="" name="hidden_sn" id="hidden_sn" value="<?=$hidden_sn?>">
		</div>
		</div>
	</div>
</div>
		</td>
		</tr>
	</table>
</form>

<!-- 分页区 -->
<div class="pagination">
</div>
		</div>
	</div>

<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script>
		// perform JavaScript after the document is scriptable.
		$j(function() {
			// setup ul.tabs to work as tabs for each div directly under div.panes
			$j("ul.tabs").tabs("div.panes > div");
		});
</script>

<script type="text/javascript"><?/* <!-- 页签javascript设定 --> */?>
	$j(function(){
		// 预设显示第一个 Tab
		var _showTab = 0;
		var $defaultLi = $j('ul.tabs li').eq(_showTab).addClass('active');
		$j($defaultLi.find('a').attr('href')).siblings().hide();
		
		// 当 li 页签被点击时...
		// 若要改成滑鼠移到 li 页签就切换时, 把 click 改成 mouseover
		$j('ul.tabs li').click(function() {
			// 找出 li 中的超连结 href(#id)
			var $this = $j(this);
			var	_clickTab = $this.find('a').attr('href');
			var _tab_link = $this.find('a').attr('tab_link');
 			// 把目前点击到的 li 页签加上 .active
			// 并把兄弟元素中有 .active 的都移除 class
			$this.addClass('active').siblings('.active').removeClass('active');

			// 淡入相对应的内容并隐藏兄弟元素
			$j(_clickTab).stop(false, true).fadeIn().siblings().hide();
			// 淡入相对应的内容并隐藏兄弟元素 for ie 7,8
			$j(_tab_link).stop(false, true).fadeIn().siblings().hide();

			return false;
		}).find('a').focus(function(){
			this.blur();
		});
	});
</script>

<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//自动产生input提示
		input_hint('<?=$this->lang->line('please_input')?>');
		//储存和取消
		save_cancel();
	});
	

//新增清单
$j(".add_ter").click(function(){
	var parent = $j(this).attr("parent");
	var obj_hidden_sn = $j("#"+parent).find("#hidden_sn");
	var setno = $j(this).attr("setno");
	//增加序号
	var new_setno =parseInt(obj_hidden_sn.val()) + 1; 
	obj_hidden_sn.val(new_setno);
	
	//复制
	$j("#"+parent+" .div_panel").first().clone(true).appendTo("#"+parent+" .tab_content_detail");
	//改编号
	$j("#"+parent+" .div_panel").last().attr("class", "div_"+new_setno);
	var obj_new = $j("#"+parent+" .div_"+new_setno);
	obj_new.attr("setno", new_setno);
	obj_new.addClass("div_panel");
	obj_new.find(".add_ter").attr("setno", new_setno)
	obj_new.find(".del_ter").attr("setno", new_setno)
	//隐藏新增
	obj_new.find(".add_ter").hide();
	obj_new.find("br").hide();
	//删除位子修正
	obj_new.find(".del_ter").addClass("mtop");
	//显示删除
	obj_new.find(".del_ter").show();
	//清除复制input的资料, 重新命名input
	clearData(parent, new_setno);
	//重新命名input
	resetName(parent, new_setno);
});

$j(".del_ter").click(function(){
	var parent = $j(this).attr("parent");
	var count = $j("#"+parent+" .div_panel").length;
	if(count == 1){  //最后一笔资料只需要清除
		var setno = $j("#"+parent+" .div_panel").attr("setno");
		clearData(parent, setno);
	}else{  //删除
		var setno = $j(this).attr("setno");
		$j("#"+parent+" .div_"+setno).remove();
		//重新定位新增删除按钮
		$j("#"+parent+" .div_panel").first().find(".add_ter").show();
		$j("#"+parent+" .div_panel").first().find("br").show();
		$j("#"+parent+" .div_panel").first().find(".del_ter").removeClass("mtop");
	}
});

//清空资料, 和重新命名
function clearData(parent, setno){
	var obj_div = $j("#"+parent+" .div_"+setno);
	var all_input = obj_div.find("input");
	all_input.val("");
}

//重新命名input
function resetName(parent, setno){
	var obj_div = $j("#"+parent+" .div_"+setno);
	var all_input = obj_div.find("input");
	
	all_input.each(function(){
		var orgname = $j(this).attr("orgname");
		var new_name = orgname+'_'+setno;
		$j(this).attr("name", new_name);
	});
}

//送出, 检查有无资料未填写
	//储存
	function fn_save(){
		var tmp = true;
		var formID = $j("#formID");
		var li = $j(".tabs li");
		formID.validationEngine('attach');
		li.each(function(index){
			$j(this).trigger("click");
			tab_index = index;
			$j('#formID').submit();
			if(!flag){  //有问题的地方停住显示
				return false;
			}
		});
	}

</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
</script>
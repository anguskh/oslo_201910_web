<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"menu_name"				=>	"{$this->lang->line('shortcut_menu_name')}",
		"shortcut_image_name"	=>	"{$this->lang->line('shortcut_input_image')}",
		"shortcut_sequence"		=>	"{$this->lang->line('shortcut_sequence')}",
		"status"				=>	"{$this->lang->line('shortcut_status')}"
	);
	$colInfo = array("colName" => $colArr);
?>
	<div id="container">
		<div id="body">
			<!-- 以下为内容 -->
			<code>
				<table width="100%" border="0">
					<tr>
						<td align="left">
							<div style="font-size: 28px; color:#464D57; position:relative; top:3px"><?=$this->lang->line('shortcut_management')?></div>
						</td>
						<td align="right">
							<button class="modalInput" rel="#prompt" id="btAddition"><img src="<?=$web?>img/add.png" width="32"><br><?=$this->lang->line('add')?></img></button>
							<span id="modify_enable" style="display:none"><button class="modalInput" rel="#prompt" id="btModification"><img src="<?=$web?>img/edit.png" width="32"><br><?=$this->lang->line('edit')?></img></button></span>
							<span id="modify_disable" style="display:none"><button class="modalInput" rel="#prompt" id="btModification" disabled><img src="<?=$web?>img/edit_d.png" width="32"><br><?=$this->lang->line('edit')?></img></button></span>
							<span id="delete_enable" style="display:none"><button id="btDelete"><img src="<?=$web?>img/trashb.png" width="32"><br><?=$this->lang->line('delete')?></img></button></span>
							<span id="delete_disable" style="display:none"><button id="btDelete" disabled><img src="<?=$web?>img/trashb_d.png" width="32"><br><?=$this->lang->line('delete')?></img></button></span>
						</td>
						<td style="text-align:right;border-right:#C6C6C6 solid 2px;width:5px"></td>
						<td style="text-align:right;width:120px">
							<span id="up_enable" style="display:none"><button id="btUp"><img src="<?=$web?>img/up.png" width="32"><br><?=$this->lang->line('up')?></img></button></span>
							<span id="up_disable" style="display:none"><button id="btUp" disabled><img src="<?=$web?>img/up_d.png" width="32"><br><?=$this->lang->line('up')?></img></button></span>
							<span id="down_enable" style="display:none"><button id="btDown"><img src="<?=$web?>img/down.png" width="32"><br><?=$this->lang->line('down')?></img></button></span>
							<span id="down_disable" style="display:none"><button id="btDown" disabled><img src="<?=$web?>img/down_d.png" width="32"><br><?=$this->lang->line('down')?></img></button></span>
						</td>
						<td style="text-align:right;border-right:#C6C6C6 solid 2px;width:5px"></td>
						<td style="text-align:right;width:60px">
				            <!-- Search Starts Here -->
				            <div id="searchContainer">
				                <a href="#" id="searchButton"><span><br><br><br><?=$this->lang->line('search')?></span><em></em></a>
				                <div style="clear:both"></div>
				                <div id="searchBox">                
				                    <form id="searchForm" method="post" action="<?=$web?>nimda/shortcut/search">
										<!--预防CSRF攻击-->
										<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				                        <fieldset2 id="body">
				                            <fieldset2>
				                                <?=$search_box?>
				                                <input type="button" id="btSearch" value="<?=$this->lang->line('search_button')?>" />
				                            </fieldset2>
				                        </fieldset2>
				                    </form>
				                </div>
				            </div>
				            <!-- Search Ends Here -->
						</td>
					</tr>
				</table>
			</code>
<!-- 列表区 -->
			<div id="listArea">
<form id="listForm" name="listForm" action="">
	<!--预防CSRF攻击-->
	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
			<table id="listTable" width="100%"  border="0" cellspacing="1" cellpadding="3">
				<tr>
					<th width="1%">
						<input type="checkbox" name="ckbAll" id="ckbAll" value="">
					</th>
					
<?
	foreach ($colInfo["colName"] as $enName => $chName)
	{
		echo "					<th align=\"center\">$chName</th>\n" ;
	}
?>
				</tr>
<?
	foreach ($shortcutInfoRow as $key => $shortcutInfoArr) {
		echo "				<tr>\n" ;

		$shortcut_sn = $shortcutInfoArr["shortcut_sn"] ;
		$checkStr = "
<td>
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$shortcut_sn}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			if ($enName == "status") {
				switch($shortcutInfoArr[$enName]) {
					case "1":
						echo "					<td align=\"center\"><font color=blue>{$this->lang->line('enable')}</font></td>\n" ;
						break;
					case "0":
						echo "					<td align=\"center\"><font color=blue>{$this->lang->line('disable')}</font></td>\n" ;
						break;
					case "D":
						echo "					<td align=\"center\"><font color=red>{$this->lang->line('delete')}</font></td>\n" ;
						break;
				}
			} else if ($enName == "shortcut_sequence") {
				echo "					<td align=\"center\">".$shortcutInfoArr[$enName]."</td>\n" ;
			} else {
				echo "					<td align=\"left\">".$shortcutInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
</form>
<!-- 分页区 -->
<div class="pagination">
	<?=$pageInfo["html"]?>
</div>
			</div>
		</div>
	</div>

<!-- 新增/修改 页面 -->
<!-- shortcut input dialog -->
<div class="modal" id="prompt" style="width: 330px; height: 340px;"></div>
<script type="text/javascript">

$j("#modify_enable").hide();
$j("#modify_disable").show();
$j("#delete_enable").hide();
$j("#delete_disable").show();
$j("#up_enable").hide();
$j("#up_disable").show();
$j("#down_enable").hide();
$j("#down_disable").show();

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});

// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j('#prompt').empty();
	var strMsg = "" ;

	$j('#prompt').css({
		'margin-top' : document.documentElement.clientHeight / 2 - $j('#prompt').height() / 2 - 90,
		'margin-left' : 0
	});

	$j.ajax({
		type:'post',
		url: '<?=$web?>nimda/shortcut/addition',
		data: {StartRow:<?=$this->session->userdata('PageStartRow')?>, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j('#prompt').html(response);
		}
	}) ;
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j('#prompt').empty();
	var strMsg = "" ;
	var selCkbVal = getCkbVal() ;

	$j('#prompt').css({
		'margin-top' : document.documentElement.clientHeight / 2 - $j('#prompt').height() / 2 - 90,
		'margin-left' : 0
	});

	$j.ajax({
		type:'post',
		url: '<?=$web?>nimda/shortcut/modification',
		data: {shortcut_sn:selCkbVal, StartRow:<?=$this->session->userdata('PageStartRow')?>, 
				<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
		error: function(xhr) {
			strMsg += '<?=$this->lang->line('ajax_request_an_error_occurred')?>';
			alert(strMsg);
		},
		success: function (response) {
			$j('#prompt').html(response);
		}
	}) ;
	
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$web?>nimda/shortcut/delete_db" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/shortcut/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/shortcut/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$web?>nimda/shortcut/search" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$web?>nimda/shortcut/search" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 全选设定
 */
$j("#ckbAll").click( function () {
	var flag = $j(this).attr('checked');
	var cnt = 0 ;

	$j(".ckbSel").each( function () {
		this.checked = flag ;
	});

	$j(".ckbSel").each( function () {
		if ( this.checked ) cnt ++ ;
	});

	if ( cnt == 1 ) {
		$j("#btModification").attr("disabled", false );
		$j("#modify_enable").show();
		$j("#modify_disable").hide();
		$j("#up_enable").show();
		$j("#up_disable").hide();
		$j("#down_enable").show();
		$j("#down_disable").hide();
	} else {
		$j("#btModification").attr("disabled", true );
		$j("#modify_enable").hide();
		$j("#modify_disable").show();
		$j("#up_enable").hide();
		$j("#up_disable").show();
		$j("#down_enable").hide();
		$j("#down_disable").show();
	}
	$j("#btDelete").attr("disabled", !$j(this).attr('checked') );
	if( $j(this).attr('checked') ) {
		$j("#delete_enable").show();
		$j("#delete_disable").hide();
	} else {
		$j("#delete_enable").hide();
		$j("#delete_disable").show();
	}
});

/**
 * 检查checkbox数量，对删除/修改按钮进行控制
 */
$j('.ckbSel,#listTable').click( function () {
	var cnt = 0 ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) cnt ++ ;
	});

	if ( cnt == 1 ) {
		$j("#btModification").attr("disabled", false );
		$j("#modify_enable").show();
		$j("#modify_disable").hide();
		$j("#btDelete").attr("disabled", false );
		$j("#delete_enable").show();
		$j("#delete_disable").hide();
		$j("#up_enable").show();
		$j("#up_disable").hide();
		$j("#down_enable").show();
		$j("#down_disable").hide();
	} else if ( cnt > 1 ) {
		$j("#btModification").attr("disabled", true );
		$j("#modify_enable").hide();
		$j("#modify_disable").show();
		$j("#up_enable").hide();
		$j("#up_disable").show();
		$j("#down_enable").hide();
		$j("#down_disable").show();
	} else {
		$j("#btModification").attr("disabled", true );
		$j("#modify_enable").hide();
		$j("#modify_disable").show();
		$j("#btDelete").attr("disabled", true );
		$j("#delete_enable").hide();
		$j("#delete_disable").show();
		$j("#up_enable").hide();
		$j("#up_disable").show();
		$j("#down_enable").hide();
		$j("#down_disable").show();
	}
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){

 $j("#listTable tr").slice(1).each(function(){

  var p = this;

  $j(this).children().slice(1).click(function(){

   $j($j(p).children()[0]).children().each(function(){

    if(this.type=="checkbox"){

     if(!this.checked){

      this.checked = true;

      //$(p).children()[0].style.backgroundColor="#FFCC80";

      //$j(this).parent().parent().addClass("seldColor");

     }else{

      this.checked = false;

      //$(p).children()[0].style.backgroundColor="#FFFFFF";

      //$j(this).parent().parent().removeClass("seldColor");

     }

    }

   });

  });

 });

});

</script>

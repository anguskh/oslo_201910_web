<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"config_sn"		=> "",
		"config_code"	=> "",
		"config_desc"	=> "",
		"config_set"	=> "",
		"input_size"	=> ""
	);
	
	$configInfo = isset($configInfo) ? $configInfo : $spaceArr ;
	
	$msg = "";
	$class = "validate[required]";
	switch($configInfo["config_code"]){
		case 'allow_ip_segment':	//后台允许登入网段
			$msg = '<font size="2" color="red">'.$this->lang->line('config_allow_ip_segment_msg').'</font>';
			$class = ""; 
			break;
		case 'foreground_pc_name':  //前台电脑名称
		case 'background_pc_name':  //后台电脑名称
			$msg = '<font size="2" color="red">'.$this->lang->line('config_allow_pc_name').'</font>';
			$class = ""; 
			break;
		case 'watermark_text':  //浮水印
			$class = ""; 
			break;
		default:
			break;
	}
	
	// $setWidth = $configInfo["input_size"] * 10;
	// if($setWidth >320){
		// $setWidth = 320;
	// }
	$setWidth = 320;
	
	$config_desc = $configInfo["config_desc"];
	if($configInfo["after_desc"] != ""){
		$config_desc .= ' / '.$configInfo["after_desc"];
	}
	
	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";

	$show_sub_title = $this->model_access->showsubtitle($bView, $configInfo["config_sn"]);
?>

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
<style>
#config_set{
	margin-top:5px
}
.config_desc{
	line-height: 110%;
}
fieldset{
	width: 200px;
	margin: 0px auto;
	float: none;
}
</style>
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('config_management')?><span><?=$show_sub_title;?></span></p>
			<div class="section">
			<form id="formID" action="<?=$action?>" method="post">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />

				<div class="container-box configbox">
					<ul>
						<li>
							<label class="config_desc"><?=$config_desc?></label>
							<label class="config_desc"><?=$msg?></label>
							<input type="hidden" name="config_sn" value="<?=$configInfo["config_sn"]?>" />
<?
	if($configInfo["config_code"] == "emv_server_type"){
		$config_set = $configInfo["config_set"];
		$SelT = "";
		$SelP = "";
		if($config_set == "T"){
			$SelT = " checked ";
		}else if($config_set == "P"){
			$SelP = " checked ";
		}else{
			$SelT = " checked ";
		}
?>
				<input name="config_set" type="radio" value="T"<?=$SelT?>><span style='font-size: 18px'><?=$this->lang->line('config_T')?>　</span>
				<input name="config_set" type="radio" value="P"<?=$SelP?>><span style='font-size: 18px'><?=$this->lang->line('config_P')?>　</span>
<?
	}else if($configInfo["config_code"] == "watermark_size"){
?>
		<input id="config_set" type="text" class="txt validate[required,custom[onlyNumberSp]]" style="width: <?=$setWidth?>px;" maxlength="<?=$configInfo["input_size"]?>" name="config_set" placeholder="<?=$this->lang->line('config_please_input')?><?=$configInfo["config_desc"]?>" value="<?=$configInfo["config_set"]?>">				
<?
	}else{
?>
		<input id="config_set" type="text" class="txt <?=$class?>" style="width: <?=$setWidth?>px;" maxlength="<?=$configInfo["input_size"]?>" name="config_set" placeholder="<?=$this->lang->line('config_please_input')?><?=$configInfo["config_desc"]?>" value="<?=$configInfo["config_set"]?>">
<?
	}
?>

						</li>
					</ul>
				</div>
			</form>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();
	});
	
	//储存
	function fn_save(){
		$j("#formID").submit();
	}
	
<?
	if( $configInfo["config_code"] == 'allow_ip_segment' ||
		$configInfo["config_code"] == 'foreground_pc_name' || 
        $configInfo["config_code"] == 'background_pc_name' ){	//后台允许登入网段
?>
		$j("#prompt").css("height", '120px');
	
<?
	}else{
?>
		$j("#prompt").css("height", '100px');
<?
	}
?>
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});
</script>
<?
	$web = $this->config->item('base_url');
	// 不想写两个view page 就要定义有用到而没有值的参数
	$spaceArr = array (
		"sn"					=> "",
		"user_sn"				=> "",
		"user_id"				=> "",
		"pwd"					=> "",
		"email_address"			=> "",
		"user_name"				=> "",
		"to_num"				=> "",
		"tso_num"				=> "",
		"email"					=> "",
		"group_sn"				=> "",
		"status"				=> "",
		"login_force_pwd" 		=> "1",
		"view_area"		=> ""
	);

	if(isset($userInfo)){
		$insert = false;
	}else{
		$insert = true;
	}
	$userInfo = isset($userInfo) ? $userInfo : $spaceArr ;
	
	$defMsg = "";
	if($insert){
		$defMsg = $this->lang->line('select_user2');
	}
	
	if($userInfo['user_id'] == ''){
		$ajax_pk_user_id = ", ajax[ajax_pk_user_id]";
		$user_id_readonly = "";
	}else{  
		$ajax_pk_user_id =  "";
		$user_id_readonly = " readonly ";
	}

	// 取得table:tbl_supplier的资料
	//$supplierSelectListRow = $this->model_supplier->getSupplierSelectList() ;
	// 取得table:tbl_group的资料
	
	
	//群组清单
	$groupSelectListRow = $this->model_group->getGroupSelectList("",$this->session->userdata('group_sn'), $insert, $userInfo["group_sn"]) ;
	
	$force1 = "";
	$force0 = "";
	if($userInfo["login_force_pwd"] == "1"){
		$force1 = " checked";
	}else if($userInfo["login_force_pwd"] == "0"){
		$force0 = " checked";
	}else{
		$force0 = " checked";
	}
	
	$Sel1 = "";
	$Sel0 = "";
	if ($userInfo["status"] == "") {
		$Sel1 = " checked";
	} else if ($userInfo["status"] == "1") {
		$Sel1 = " checked";
	} else if ($userInfo["status"] == "0") {
		$Sel0 = " checked";
	}
	
	$group = "";
	if($userInfo["group_sn"] == '1'){
		$group = 'disabled';
	}

	$show_sub_title = $this->model_access->showsubtitle($bView, $userInfo["group_sn"]);

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";
	
	//部门自动搜寻
	$dept_auto_url = "{$web}common/common/autocomplete_dept";
	//办公室地点自动搜寻
	$location_auto_url = "{$web}common/common/autocomplete_location";
	//国家地区自动搜寻
	$district_auto_url = "{$web}common/common/autocomplete_district";
	//乡镇县市自动搜寻
	$city_auto_url = "{$web}common/common/autocomplete_city";
	//职位自动搜寻
	$position_auto_url = "{$web}common/common/autocomplete_position";

	$all_areaArr = array('北京市','天津市','河北省','山西省','内蒙古自治区','辽宁省','吉林省','黑龙江省','上海市',
						'江苏省','浙江省','安徽省','福建省','江西省','山东省','河南省','湖北省','湖南省',
						'广东省','广西壮族自治区','海南省','重庆市','四川省','贵州省','云南省','西藏自治区',
						'陕西省','甘肃省','青海省','宁夏回族自治区','新疆维吾尔自治区',//710000: '台湾省',
						'香港特别行政区','澳门特别行政区');
?>

<script src="<?=$web?>js/jquery.tools.min.js"></script>
<script type="text/javascript">
//将jquery.tools.min.js 的jquery 改为$auto, 避免与原始版本的jquery冲突
var $auto = jQuery.noConflict();
</script>
<!--自动完成UI载入start-->
<link rel="stylesheet" href="<?=$web?>css/ui/jquery.ui.autocomplete.css"/> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.core.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.widget.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.position.js"></script> 
<script type="text/javascript" src="<?=$web?>js/ui/jquery.ui.autocomplete.js"></script> 
<!--自动完成UI载入end-->

<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->

<!--#formID input:text自动产生hint start-->
<script type="text/javascript" src="<?=$web?>js/common/input_hint.js"></script> 
<!--#formID input:text自动产生hint end-->

<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->

<!--日期设定start-->
<link rel="stylesheet" type="text/css" href="<?=$web?>css/date_input.css"/>
<!--日期设定end-->

<!--自动完成的CSS设定-->

<!-- 下拉搜尋 多筆勾選 -->
<script src="<?=$web?>js/searchable/jquery-1.10.2.js"></script>
<link href="<?=$web?>js/searchable/search_sol.css" rel="stylesheet">
<script src="<?=$web?>js/searchable/sol.js"></script>

<style>
   .ui-autocomplete {
		max-height: 200px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
		/* add padding to account for vertical scrollbar */
		padding-right: 20px;
	} 
</style>

<style type="text/css"> 
#formID p{
	margin-bottom : auto;
}
#selGroupSN, #selBankSN, #selMerchantSN{
	width: 200px;
}	
#button_div{
	position: absolute; 
	top: 100%;
	margin-top: -60px;
	margin-left: 190px;
}
.display_inline{
	display: inline-block;
}


input[type=radio] {
	display: inline-block;
	font-size: 16px;
	line-height: 1em;
	margin: 0.20em 0.20em;
	padding: 0;
	width: 1.0em;
	height: 1.0em;
	-webkit-border-radius: 2em;
	-moz-border-radius: 2em;
	vertical-align: -30%;
}

input[type=radio]:checked {
	background: url("data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%008%00%00%008%08%02%00%00%00'%E4%ACI%00%00%00%19tEXtSoftware%00Adobe%20ImageReadyq%C9e%3C%00%00%03%99IDATx%DA%EC%98AKrA%14%86%BBj%A6%9F%11%04R%B9ID%A1%82%16%11n%A4m%8Bh%11%ED%04%D7%FE%8Bh%95%D8%0F%A8e%7F%40%DADT%B4%8FvA%1BA%DA%84%06%ADZ'%95%A9%BD%DF%7D%F10%DF%CDl%E6*%1F%08s%16r%EF8%F7%CEs%DFsf%CE%99q2%99%CC%C48X%60bL%CC%82ZP%0BjA-%A8%05%B5%A0%16%D4%82%9AXh%24o%E9v%BB%03%FEu%1Cg%04%A0%C3%BCE%F8%F0%92%9FX%D5%F7%0F3V(%18%0C%0EC%A9%FE%AA%ED%C2%E4%B8%26-%BEY%FD%80z%10%85%D2s%ED%7C%B3apC0%7F%94b%9DN%07-%CB%CB%CB%F9%7C~cc%23%99L%E2%B6%D1h%DC%DE%DEV*%95%87%87%07%60%05%02%81%BE%C4%FA%E6d%B3YSP%B1v%BB%0D%CAp8%BC%BF%BF%BF%B3%B3%F3%F1%F1%D1j%B5%C8%0D%B2%C9%C9I%FCu~~~pp%80%BF%D0%02%EFyp%0D%40s%B9%9C%0F%CA%8Ek%00%85CNNNVVV%DE%DE%DE%FA%3E%12%89Dj%B5Z%B1X%FC%FC%FC%04h%C05%1F%AC%C1t%3A%1D41%19%86%DC%7B%7B%7Bp%F7%FB%FB%BB%F3%83%E1c%12%89%C4%FC%FC%FC%CD%CDM%C85%BC%24%D43%FDqCp%90%91%9C%14%12%B7%B8XZZ%DA%DD%DD%7D%7D%7D%1D%AC%0D%3E%03%DDNOO%1F%1F%1F%19%00%9E%0F%D6%9ALSSS%FA%A0%A4%C4%18%04-%14%0A%08J%9D%C1%D0%0D%9D%CB%E52%10!%8D%CA%AA%0B%8Ax%D7%EC%CA%B8%E4%DBq%81%98%5B%5B%5Bc%8B%CE%B3%EB%EB%EBxP%FC.%F1%3AbP%99%E6LB%1Cfaa%01%B8%9A%AA%CC%CD%CD%89%DF%85U%DF%FB%06%A0%22'%AE%C1G1%8Cf.%25%04%22%BC%2F%A2%EA%82j%C6(A%A9%1F%A5E%D8%BD%BC%BC%C4%E3%F1%C1%15%89%24Rtf%80%8A%81%D5%00%D4%B7%A2%18%E6%FE%FE~kk%8B%2B%FC%AFZ%A23%3D%CED%F0%3F%40)-F%3A%3B%3B%DB%DE%DE%D6Q%14%0F%A2%B3h%E9%07%D4%C8%F5%8C*%DE%C2%FB%F5z%FD%EA%EA%0A%AC%83E%C5%23%E8%86%CE%D3%D3%D3%11%D70%A81%A8%FE%82%CF%15%94%AE%E7%82%0A%5D%8F%8E%8E%16%17%17WWW%07%D4%A3%D5j%15%DD%041%EC%9A%AC%A6%BA)tssS3s%AA%E9Dd%00%EB%F5%F5%F5%EC%EC%2C%B2%94T%9Fb%E8%7FyyY*%95%20%5E%2C%16%FB%E3%9A%E0%92U3%91%3A%87%87%87F%F9%13d%98%EF%C8%8A%A8B%9A%CD%26%F2g%D3%B5T*%85%18%40-%06%81%D1%FF%E9%E9%E9%EE%EE%EE%E2%E2%02%F5%1E%F9%08%FA%DD%EF%23v%3D%3D%CB0U%8B%20*%8DQ%9F%9F%9F%8F%8F%8F%3De%1E%98fff%A2%D1(%10%A3%AEQKYD%F5Wb%83%C2%99%A2%06%FE5RblH%85%A2%13z%0B(%DBA%16%E9%99Ji%94%96%CC%B6%22%14U%82U(%A9%1C%ABf%96%D2%D2%8D%2B%11qeI%A2%D3M%13%9B%D9%9E%89%8ArA%95%AC%8D%E1%5B%AE%B5%5D%93%CD%1D%3B%A8yH%ADEL%0Bg%E3%ED%B2%3A%A3%05%17%1C%ED%9Ey%40%E5%7B%C4%DD%B2v%9A%81%9ARr%7B%89%C1%04%88%1A%13%11%17j%3Bq%D5%98%F6%BD%B93%3E)%11VUZ%16~%EA%8EY%80%04NMB~%B6%CB%3E%CE%02d%C3D%C1d%E5%EA%7B%00%E1%994%FE%0F%20%7C%9F%B1%A8%B8%7FS%DCo%93r%C8%13%A8a%0F%C9Fr%00f%CFG-%A8%05%B5%A0%16%D4%82%8E%1F%E8%97%00%03%00%2CK3%C0%95%A6%DFv%00%00%00%00IEND%AEB%60%82") no-repeat center center;
	-webkit-background-size: 16px 16px;
	-moz-background-size: 16px 16px;
}
#div_access_control{
	width: 100%;
	height: auto;
	overflow: auto;
}

.subfirst{
	display: none;
}
#div_access_control table td{
	padding: 5px;
}
.td_control{
	width: 20%;
}
.main{
	background-color: #fafad2;
}
input.sub{
	margin-top: -3px;
	float: left;
	display:none;
}
input.sub_control{
	/*margin-top: -3px;
	float: left;*/
}
#control_label{
	text-align: center;
	background-color: #fafad2;
}
.td_sub{
	width: 20%;
}
#remark{
	height: 110px;
	width: 160px;
	resize: none;
}
.display_none{
	display: none;
}
</style>
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('user_management')?><span><?=$show_sub_title;?></span></p>
			<div class="section container">
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<div class="container-box" style="width:22%;">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('user_user_name')?></label>
							<input type="text" class="txt kr-220" name="postdata[user_name]" id="user_name" value='<?=$userInfo["user_name"]?>'>
							<input type="hidden" name="user_sn" value="<?=$userInfo["user_sn"]?>" />
						</li>
						<li>
							<label for=""><?=$this->lang->line('user_user_id')?></label>
							<input type="text" id="user_id" class="txt validate[required,minSize[4]<?=$ajax_pk_user_id?>] kr-220" name="postdata[user_id]" maxlength="20" placeholder="<?=$this->lang->line('user_input_account')?>" value="<?=$userInfo["user_id"]?>" >
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('user_pwd')?></label>
							<input type="password" class="txt validate[required] kr-220" maxlength="12"  name="postdata[pwd]" id="pwPswd" placeholder="<?=$this->lang->line('user_input_pwd')?>" value="<?=$userInfo["pwd"]?>">
						</li>
						<li>
							<label for="" class="required"><?=$this->lang->line('user_email')?></label>
							<input type="text" class="txt validate[required] kr-220" id="email_address" name="postdata[email_address]" value="<?=$userInfo["email_address"]?>">
						</li>

						<li>
							<label for=""><?=$this->lang->line('user_login_force_pwd')?></label>
							<input  name="postdata[login_force_pwd]" type="radio" class="radio first"  value="1" <?=$force1?>><?=$this->lang->line('enable')?>
							<input  name="postdata[login_force_pwd]"  type="radio" class="radio"  value="0" <?=$force0?>><?=$this->lang->line('disable')?>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:30%;">
					<ul>
						<li style="height:53px;">
							<label for=""><?=$this->lang->line('user_status2')?></label>
							<input  name="postdata[status]" type="radio" class="radio first"  value="1" <?=$Sel1?>><?=$this->lang->line('enable')?>
							<input  name="postdata[status]"  type="radio" class="radio"  value="0" <?=$Sel0?>><?=$this->lang->line('disable')?>
						</li>
						<li>
							<div id="div_group">
								<input id="orgGroupSN" type="hidden" value="<?=$userInfo["group_sn"]?>">
								<label for="" class="required"><?=$this->lang->line('user_group_sn')?></label>
								<select name="postdata[group_sn]" id="selGroupSN" class="validate[required]" style="width:240px;">
									<option value=""><?=$this->lang->line('user_select_group')?></option>			
<?
									foreach ($groupSelectListRow as $key => $groupSelectListArr)
									{
										$Selected = "";
										$to_flag = $groupSelectListArr["to_flag"];
										if($userInfo["group_sn"] == $groupSelectListArr["group_sn"])
											$Selected = " SELECTED";
										echo "<option to_flag=\"{$to_flag}\" login_side=\"{$groupSelectListArr["login_side"]}\" value=\"{$groupSelectListArr["group_sn"]}\"{$Selected}>{$groupSelectListArr["group_name"]}</option>\n";
									}
?>
								</select>
							</div>
						</li>
						<li>
							<label for="" id="to_num_cls"><?=$this->lang->line('to_num')?></label>
							<select name="postdata[to_num]" id="to_num" onchange="change_sub_num('to_num', 'tso_num');" style="width:240px;">
								<?=create_select_option($operator_list, $userInfo["to_num"], $this->lang->line('select_operator_class'));?>
							</select>
						</li>
						<li>
							<label for=""><?=$this->lang->line('tso_num')?></label>
							<select name="postdata[tso_num]" id="tso_num">
								<?=create_select_option($sub_operator_list, $userInfo["tso_num"], $this->lang->line('select_suboperator_class'), array("to_num"));?>
							</select>
						</li>
						<li style="padding:0 20px 0 0;">
							<label for=""><?=$this->lang->line('user_view_area')?></label>
							<select multiple="multiple" id="all_area" name="all_area[]" style="width: 385px;">
<?
								$all_arr = explode(',',$userInfo['view_area']);
								foreach($all_areaArr as $i_arr => $i_value){
									$selected = "";
									if(in_array($i_value, $all_arr)){
										$selected = "selected";
									}
									echo '<option value="'.$i_value.'" '.$selected.'>'.$i_value.'</option>';
								}
?>
							</select>	
							<div>
								<div id="current-selection-all_area" class="sol-current-selection">
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:48%;">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('user_access')?></label>
							<!--权限控管-->
							<div id="div_access_control" class="table_menu">
								<!-- 选单区域 -->
							</div>
						</li>
					</ul>
				</div>
				</form>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
		</div>
    </div>
<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>

<script type="text/javascript">
var flag = false;
var tab_index = 0;

	$j('#div_bank').hide();
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		$j(":date").dateinput({ lang: 'zh-TW', trigger: false, format: 'yyyy-mm-dd', offset: [0, 0] });
		
		//储存和取消
		save_cancel();
	});

	$j("#selGroupSN").change(function(){
		var to_flag = $j("#selGroupSN option[value="+this.value+"]").attr('to_flag');
		if(to_flag == 1){
			$j("#to_num").addClass("validate[required]");
			$j("#to_num_cls").addClass("required");

		}else{
			$j("#to_num").removeClass("validate[required]");
			$j("#to_num_cls").removeClass("required");
		}
		access_control();
	});
	
	//权限控管
	function access_control(){
		var group_sn = $j('#selGroupSN').val();
		var orgGroupSN = $j('#orgGroupSN').val();
		var user_sn = $j('#user_sn').val();
		$j.ajax({
			type:'post',
			async: false,
			url: '<?=$web?>nimda/user/getGroupMenu',
			data: { group_sn: group_sn, 
					user_sn: user_sn, 
					org_group_sn: orgGroupSN, 
					<?=$this->security->get_csrf_token_name();?>: '<?=$this->security->get_csrf_hash();?>'},
			error: function(xhr) {
				strMsg += 'Ajax request发生错误[background.php]\n请重试';
				alert(strMsg);
			},
			success: function (response) {
				$j("#div_access_control").html(response);
			}
		}) ;
	}

	function change_sub_num(filed_1, filed_2){
		var filed_val = $j("#"+filed_1).val();
		
		
		if(filed_val == ''){
			$j("#"+filed_2+" option").show();
		}else{
			$j("#"+filed_2+ "option:selected").removeAttr("selected");
			$j("#"+filed_2+" option").hide();
			$j("#"+filed_2+" option["+filed_1+"='"+filed_val+"']").show();

			$j("#"+filed_2+" option[value='']").show();
			$j("#"+filed_2+" option[value='']").prop('selected', true);
		}
	}
	//储存
	function fn_save(){
		$j("#selGroupSN").prop("disabled", false);
		checkpwd();
		$j('#formID').submit();
	}

//如果密码栏位本身已有值, 代表是MD5产生的乱数
//避开没改密码, 反而被密码字数限制的状况
// function password_def(){
	// var pwPswd = $j("#pwPswd");
	// var pwPswd2 = $j("#pwPswd2");
	// var pwPswd_len = pwPswd.val().length;
	// var pwPswd2_len = pwPswd2.val().length;
	// if(pwPswd_len != 32 || pwPswd2_len != 32){
		// //alert(1);
		// //console.log(1);
		// if(pwPswd.hasClass("nocheck")){  //不检查
		// }else{  //检查
			// pwPswd.removeAttr("class");
			// pwPswd2.removeAttr("class");
			// pwPswd.addClass("validate[required,minSize[7],maxSize[12],custom[checkpassword]] text-input");
			// pwPswd2.addClass("validate[required,minSize[7],maxSize[12],equals[pwPswd]] text-input");
		// }
	// }else if(pwPswd_len == 32 && pwPswd2_len == 32){
		// //alert(2);
		// //console.log(2);
		// pwPswd.removeAttr("class");
		// pwPswd2.removeAttr("class");
	// }
// }
function checkpwd(){
	var pwd = "<?=$userInfo['pwd']?>";
	var pwPswd = $j("#pwPswd").val();

	if(pwPswd != pwd || pwPswd == ''){
			$j("#pwPswd").removeClass("validate[required]");
			$j("#pwPswd").addClass("validate[required,minSize[7],maxSize[12],custom[checkpassword]]");
	}else{
			$j("#pwPswd").removeClass("validate[required,minSize[7],maxSize[12],custom[checkpassword]]");
			$j("#pwPswd").addClass("validate[required]");
	}
}
$j("#pwPswd").blur(function(){
	checkpwd();
});

// $j("#pwPswd2").keyup(function(){
	// password_def();
// });

// $j(":submit").click(function(){
	// password_def();
// });


//预设执行
// password_def();
access_control();

//自动完成连接至input和select元件
link_obj('dept_no_input', 'dept_no', '<?=$dept_auto_url?>');
link_obj('location_input', 'location', '<?=$location_auto_url?>');
link_obj('district_input', 'district', '<?=$district_auto_url?>');
link_obj('city_input', 'city', '<?=$city_auto_url?>');
link_obj('position_input', 'position', '<?=$position_auto_url?>');
link_obj('charge_area_input', 'charge_area', '<?=$location_auto_url?>');
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

//將jquery-1.10.2.js 和 原本的jquery 分開別名, 避免出問題
var $j_1102 = $.noConflict(true); //true 完全將 $ 移還給原本jquery
//alert($j.fn.jquery);  //顯示目前$j是誰在用

var arr = new Array('all_area');

var mutiselectObjArr = new Array();
var y = 0;

arr.forEach(function(value) {
	mutiselectObjArr[y] = $j_1102('#'+value).searchableOptionList({
		showSelectAll: true,
		events: {
			maxwidth: '325', //設定寬度
			nowId: value
		},
		texts: {
			noItemsAvailable: '查无此资料',
			selectNone: '清除选择',
			selectAll: '全选',
			searchplaceholder: '选择省份'
		}
	});

	y++;
});

</script>
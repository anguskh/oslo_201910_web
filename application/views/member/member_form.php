<?
	$web = $this->config->item('base_url');
	
	// 不想写两个 view page 就要定义有用到而没有值的参数
	$spaceArr = array (
			"s_num"					=>			"",
			"tlp_num"				=>			"",
			"td_num"				=>			"",
			"tsd_num"				=>			"",
			"to_num"				=>			$this->session->userdata("to_num"),
			"tso_num"				=>			$this->session->userdata("tso_num"),
			"member_register"		=>			"P",
			"tmwi_num"				=>			"",
			"member_type"			=>			"",
			"name"					=>			"",
			"personal_id"			=>			"",
			"user_id"				=>			"",
			"birthday"				=>			"",
			"gender"				=>			"",
			"mobile"				=>			"",
			"phone"					=>			"",
			"email"					=>			"",
			"province"				=>			"",
			"district"				=>			"",
			"city"					=>			"",
			"zip_code"				=>			"",
			"address"				=>			"",
			"pic_face"				=>			"",
			"pic_id_front"			=>			"",
			"pic_id_back"			=>			"",
			"pic_driver_front"		=>			"",
			"pic_driver_back"		=>			"",
			"driver_license_type"	=>			"",
			"driver_license_number"	=>			"",
			"nationality"			=>			"158",
			"pay_type"				=>			"",
			"account_no"			=>			"",
			"card_expired_date"		=>			"",
			"card_cvv"				=>			"",
			"lease_type"			=>			"",
			"type_day"				=>			"",
			"type_month"			=>			"",
			"type_year"				=>			"",
			"deposit"				=>			"",
			"lease_status"			=>			"",
			"lease_token"			=>			"",
			"verification_code"		=>			"",
			"status"				=>			"",
			"send_sms_mail"			=>			"",
			"referrals_name"		=>			"",
			"eng_staff"				=>			"0",
			"stored_value"			=>			"",
			"remaining_time"		=>			"",
			"change_count"			=>			""
			
	);

	$memberInfo = isset($memberInfo) ? $memberInfo : $spaceArr ;
	$show_sub_title = $this->model_access->showsubtitle($bView, $memberInfo["s_num"]);
	
	$mtL_checked = '';
	$mtl_checked = '';
	$mtl_display = 'none';
	$mts_checked = '';
	$mts_display = 'none';
	$mtc_checked = '';
	$mtb_checked = '';
	$mtcb_display  = 'none';
	$member_type_arr =explode(',', $memberInfo['member_type']);
	foreach($member_type_arr  as $arr){
		if($arr == 'A'){
				$mtL_checked = 'checked';
				$mtl_display  = 'block';
		}else if($arr == 'B'){
				$mtl_checked = 'checked';
				$mtl_display  = 'block';
		}else if($arr== 'C'){
				$mts_checked = 'checked';
				$mts_display  = 'block';
		}else if($arr== 'D'){
				$mtc_checked = 'checked';
				$mtcb_display  = 'block';
		}else if($arr== 'E'){
				$mtb_checked = 'checked';
				$mtcb_display  = 'block';
		}else if($arr== 'F'){
				//可綁定多顆電池
				$mtb_checked = 'checked';
				$mtcb_display  = 'block';
		}
	}

	$eng_staff_checked = "";
	if($memberInfo['eng_staff']==1)
	{
		$eng_staff_checked = "checked";
	}
	//解密设定
	/*$encryption_arr = array('personal_id', 'birthday', 'account_no', 'card_expired_date', 'card_cvv');
	$key = md5('ebike'.$memberInfo['mobile']);
	$key = substr($key, 0, 24);
	for($i=0; $i<count($encryption_arr); $i++){
		if($memberInfo[$encryption_arr[$i]]  != ''){
			$string = $memberInfo[$encryption_arr[$i]] ;
			//加密
			$memberInfo[$encryption_arr[$i]]  = $this->Model_member->decrypt($key, $string);
		}
	}*/

	//post action网址
	$get_full_url_random = get_full_url_random();
	$action = "{$get_full_url_random}/modification_db";	

	$spaceArr2 = array (
			"access_token"		=>			"",
			"openid"			=>			"",
			"scope"				=>			"",
			"nickname"			=>			"",
			"sex"				=>			"",
			"privilege"			=>			"",
			"unionid"			=>			"",
			"province"			=>			"",
			"city"				=>			"",
			"country"			=>			"",
			"headimgurl"		=>			"",
			"privilege"			=>			"",
			"unionid"			=>			""
	);
	//如果是微信登入的要塞一些值
	//$wechatInfo = count($wechatInfo)>0 ? $wechatInfo : $spaceArr2 ;
	if($memberInfo['member_register'] == 'W'){
		if($wechatInfo['sex'] != ''){
			if($wechatInfo['sex'] == '1'){
				$memberInfo['gender'] = 'M';
			}else if($wechatInfo['sex'] == '2'){
				$memberInfo['gender'] = 'F';
			}
		}
		
		//檢查主檔地址有有 有的話以主檔為主
		if($memberInfo['province'] == ''){
			$memberInfo['province'] = $wechatInfo['province'];
			$memberInfo['city'] = $wechatInfo['city'];
		}
	}else{
		$wechatInfo = $spaceArr2 ;
	}

	$member_register_only = '';
	if($memberInfo['s_num'] != ''){
		$member_register_only = 'disabled';
	}
?>
<script type="text/javascript" src="<?=$web?>js/cityselectcn/distpicker.js"></script>

<!-- script src="<?=$web?>js/jquery.twzipcode.js"></script -->
<!--自动完成input自动连结start-->
<script type="text/javascript" src="<?=$web?>js/common/auto_link_input.js"></script> 
<!--自动完成input自动连结end-->
<!--#formID 存档和取消 start-->
<script type="text/javascript" src="<?=$web?>js/common/save_cancel.js"></script> 
<!--#formID 存档和取消 end-->
    <div class="wrapper">
		<div class="box">
			<p class="btitle"><?=$this->lang->line('member_management')?><span><?=$show_sub_title;?></span></p>
			<form id="formID" action="<?=$action?>" method="post" enctype="multipart/form-data">
			<INPUT TYPE="hidden" NAME="webs" id="webs" value="<?=$web;?>">
			<p class="title">基本资料</p>
			<div class="section container">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$StartRow?>" />
				<input type="hidden" name="s_num" value="<?=$memberInfo['s_num']?>" />
				<div style="float:left; width:38%; overflow: hidden;">
					<div class="container-box" style="width:50%;">
						
						<span class="show" style="width:108px; height:135px;" id="show_default_pic_face">
								<div id="defalut_pic_face" style="display:<?echo (($memberInfo["pic_face"] == '')?'block':'none')?>;">
									<img src="<?=$web;?>images/mug_shot.jpg" alt="">
								</div>
								<div id="upload_pic_face">
								<?php
									$del_button = '';
									if($memberInfo["pic_face"] != ''){
										echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$memberInfo["s_num"].'/pic_face" height="160">';
									}
								?>
								</div>
						</span>
						<ul>
							<li>
								<label for="">上传<?=$this->lang->line('pic_face')?><br><span class="notice">160px * 200px</span></label>
								<input class="txt short" type="text" id="pic_face_txt" name="pic_face_txt" class="tb" value="" readonly>
								<input type="file" onchange="checkFile(this, 'pic_face');" name="pic_face" id="pic_face" style="display:none;">
								<input type="button" value="浏览" id="id_pic_face" class="btn">
							</li>
						
						</ul>
						
					</div>
					
					<div class="container-box" style="width:50%;">
						<ul>
							<li>
								<label for="" class="required"><?=$this->lang->line('name')?></label>
								<input type="text" class="txt validate[required]" id="names" name="postdata[name]" value="<?=$memberInfo["name"]?>" maxlength="50">
							</li>
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('gender')?></label>
								<input type="radio" id="gender" name="postdata[gender]" value="M" class="radio first" <?=(($memberInfo["gender"]=='M')?'checked':'')?>><?=$this->lang->line('gender_M')?>
								<input type="radio" id="gender" name="postdata[gender]" value="F" class="radio" <?=(($memberInfo["gender"]=='F')?'checked':'')?>><?=$this->lang->line('gender_F')?>
							</li>
							<li>
								<label for=""><?=$this->lang->line('birthday')?></label>
								<input type="date" class="txt" id="birthday" name="postdata[birthday]" value="<?=$memberInfo["birthday"]?>">
							</li>	
						</ul>
					</div>
					<div class="container-box" style="width:100%;">
						<ul>
							<li style="padding: 0 25px 0 0;">
								<label for="" id="label_zipcode"><?=$this->lang->line('city')?></label>
								<div id="cnzipcode">
									<select class="form-control" NAME="postdata[province]" id="province"></select>
									<select class="form-control" NAME="postdata[city]" id="city"></select>
									<select class="form-control" NAME="postdata[district]" id="district"></select>
								</div>

								<!--span id="twzipcode"></span>
								<!-- INPUT TYPE="hidden" NAME="postdata[zip_code]" id="zip_code">
								<INPUT TYPE="hidden" NAME="postdata[city]" id="city">
								<INPUT TYPE="hidden" NAME="postdata[district]" id="district" -->
							</li>
							<li>
								<label for="" id="label_address"><?=$this->lang->line('address')?></label>
								<input type="text" class="txt" id="address" style="width:356px;" name="postdata[address]" value="<?=$memberInfo["address"]?>">
							</li>
						</ul>
					</div>
				</div>
				<div style="float:left; width:36%; overflow: hidden;">
					<div class="container-box" style="width:50%;">
						<ul>
							<li>
								<label for=""><?=$this->lang->line('mobile')?></label>
								<input type="text" class="txt" id="mobile" name="postdata[mobile]" value="<?=$memberInfo["mobile"]?>" maxlength="11">
							</li>
							<!-- <li>
								<label for=""><?=$this->lang->line('phone')?></label>
								<input type="text" class="txt" id="phone" name="postdata[phone]" value="<?=$memberInfo["phone"]?>" maxlength="20">
							</li> -->
						</ul>
					</div>
					<div class="container-box" style="width:50%;">
						<ul>	
							<li>
								<label for=""><?=$this->lang->line('phone')?></label>
								<input type="text" class="txt" id="phone" name="postdata[phone]" value="<?=$memberInfo["phone"]?>" maxlength="20">
							</li>
							
							
						</ul>
					</div>
					<div class="container-box" style="width:100%;">
					<ul>
						<li>
								<label for=""><?=$this->lang->line('referrals_name')?></label>
								<input type="text" class="txt" id="referrals_name" style="width:345px;" name="postdata[referrals_name]" value="<?=$memberInfo["referrals_name"]?>">
							</li>
						<li>
							<label for=""><?=$this->lang->line('email')?></label>
							<input type="text" class="txt" id="email" style="width:345px;" name="postdata[email]" value="<?=$memberInfo["email"]?>">
						</li>
						<li>
							<label for=""><?=$this->lang->line('personal_id')?></label>
							<input type="text" class="txt" id="personal_id" style="width:345px;" name="postdata[personal_id]" value="<?=$memberInfo["personal_id"]?>">
						</li>	
						<li>
							<label for=""><?=$this->lang->line('user_id')?></label>
							<input type="text" class="txt" name="postdata[user_id]" id="user_id"  style="width:345px;" value="<?=$memberInfo["user_id"]?>" maxlength="32">
						</li>
					</ul>
				</div>
				</div>
				<div class="container-box" style="width:26%;">
					<ul style="position:relative;">
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('member_type')?></label>
							<!-- input type="checkbox" id="member_type" name="member_type[]" class="radio first validate[required]" value="A" onclick="change_member_span('');" <?=$mtL_checked?>><?=$this->lang->line('member_type_A')?>
							-->
							<input type="checkbox" id="member_type" name="member_type[]" class="radio validate[required]" value="B" onclick="change_member_span('');" <?=$mtl_checked?>><?=$this->lang->line('member_type_B')?>
							<!-- input type="checkbox" id="member_type" name="member_type[]" class="radio validate[required]" value="C" onclick="change_member_span('');" <?=$mts_checked?>><?=$this->lang->line('member_type_C')?>
							<input type="checkbox" id="member_type" name="member_type[]" class="radio validate[required]" value="D" onclick="change_member_span('');" <?=$mtc_checked?>><?=$this->lang->line('member_type_D')?>
							<input type="checkbox" id="member_type" name="member_type[]" class="radio validate[required]" value="E" onclick="change_member_span('');" <?=$mtb_checked?>><?=$this->lang->line('member_type_E')?>
							--><br>
							<input type="checkbox" id="member_type" name="member_type[]" class="radio validate[required]" value="F" onclick="change_member_span('');" <?=$mtb_checked?>><?=$this->lang->line('member_type_F')?>
						</li>
						<span id="member_type_A" class="member_type" style="display:<?=$mtl_display;?>;position:absolute; top:320px; left:0;">
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('tlp_num')?></label>
								<select name="postdata[tlp_num]" id="tlp_num">
									<?=create_select_option($tlp_list, $memberInfo["tlp_num"], $this->lang->line('select_tlp_class'));?>
								</select>
							</li>
						</span>
						<span id="member_type_C" class="member_type" style="display:<?=$mts_display;?>;position:absolute; top:320px; left:0;">
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('td_num')?></label>
								<select name="postdata[td_num]" id="td_num"  onchange="change_sub_num('td_num', 'tsd_num');">
									<?=create_select_option($dealer_list, $memberInfo["td_num"], $this->lang->line('select_dealer_class'));?>
								</select>
							</li>
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('tsd_num')?></label>
								<select name="postdata[tsd_num]" id="tsd_num">
									<?=create_select_option($sub_dealer_list, $memberInfo["tsd_num"], $this->lang->line('select_subdealer_class'), array("td_num"));?>
								</select>
							</li>
						</span>
						<span id="member_type_DE" class="member_type" style="display:<?=$mtcb_display;?>;position:absolute; top:320px; left:0;"
						>
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('to_num')?></label>
								<select name="postdata[to_num]" id="to_num" onchange="change_sub_num('to_num', 'tso_num');">
									<?=create_select_option($operator_list, $memberInfo["to_num"], $this->lang->line('select_operator_class'));?>
								</select>
							</li>
							<li style="height:53px;">
								<label for=""><?=$this->lang->line('tso_num')?></label>
								<select name="postdata[tso_num]" id="tso_num">
									<?=create_select_option($sub_operator_list, $memberInfo["tso_num"], $this->lang->line('select_suboperator_class'), array("to_num"));?>
								</select>
							</li>
						</span>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('status')?></label>
							<input type="radio" class="radio first validate[required]" id="status" name="postdata[status]" value="R" <?=(($memberInfo["status"] == 'R')?'checked':'')?> onclick="check_label();"><?=$this->lang->line('status_0')?>
							<input type="radio" class="radio validate[required]" id="status" name="postdata[status]"value="Y"<?=(($memberInfo["status"] == 'Y')?'checked':'')?> onclick="check_label();"><?=$this->lang->line('status_1')?>
							<input type="radio" class="radio validate[required]" id="status" name="postdata[status]"value="N"<?=(($memberInfo["status"] == 'N')?'checked':'')?> onclick="check_label();"><?=$this->lang->line('status_2')?>
						</li>
						<li style="height:53px;">
							<label for="" class="required"><?=$this->lang->line('member_register')?></label>
							<input type="radio" class="radio first validate[required]" id="member_register" name="postdata[member_register]" value="P" <?=(($memberInfo["member_register"] == 'P')?'checked':'')?> <?=$member_register_only;?>><?=$this->lang->line('member_register_P')?>
							<input type="radio" class="radio validate[required]" id="member_register" name="postdata[member_register]"value="W"<?=(($memberInfo["member_register"] == 'W')?'checked':'')?> <?=$member_register_only;?>><?=$this->lang->line('member_register_W')?>
						</li>
						<li>
							<label for="">人員<br></label>
							<input type="checkbox" id="eng_staff" name="postdata[eng_staff]" value="1" <?=$eng_staff_checked?>>
							<?=$this->lang->line('eng_staff')?>
						</li>
					</ul>
				</div>
			</div>
<?

			//if($memberInfo['member_register'] == 'W'){
?>
			
			<div class="section container">
				<div class="container-box" style="width:20%;">
					<p class="title">微信资讯</p>
					<ul>
						<li>
							<label for=""><?=$this->lang->line('member_access_token')?></label>
							<input type="text" class="txt" id="access_token" name="access_token" value="<?=$wechatInfo['access_token']?>" readonly>
						</li>
						<li>
							<label for=""><?=$this->lang->line('member_openid')?></label>
							<input type="text" class="txt" id="openid" name="openid" value="<?=$wechatInfo['openid']?>" readonly>
						</li>
						<li>
							<label for=""><?=$this->lang->line('member_scope')?></label>
							<input type="text" class="txt" id="scope" name="scope" value="<?=$wechatInfo['scope']?>" readonly>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:20%;">
					<p class="title">&nbsp;</p>
					<ul>
						<li>
							<label for=""><?=$this->lang->line('member_nickname')?></label>
							<input type="text" class="txt" id="nickname" name="wechet[nickname]" value="<?=$wechatInfo['nickname']?>">
						</li>
						<li>
							<label for=""><?=$this->lang->line('member_privilege')?></label>
							<input type="text" class="txt" id="privilege" name="privilege" value="<?=$wechatInfo['privilege']?>" readonly>
						</li>
						<li>
							<label for=""><?=$this->lang->line('member_unionid')?></label>
							<input type="text" class="txt" id="unionid" name="unionid" value="<?=$wechatInfo['unionid']?>" readonly>
						</li>
					</ul>
				</div>
				<div class="container-box" style="width:20%;">
					<p class="title">帐务资讯</p>
					<ul>
						<li>
							<label for=""><?=$this->lang->line('pay_type')?></label>
							<input type="radio" class="radio first" id="pay_type" name="postdata[pay_type]" value="W" <?=(($memberInfo['pay_type']=='W')?'checked':'');?>><?=$this->lang->line('pay_type_W')?>

							<!-- 
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="A" <?=(($memberInfo['pay_type']=='A')?'checked':'');?>><?=$this->lang->line('pay_type_A')?>
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="1" <?=(($memberInfo['pay_type']=='1')?'checked':'');?>><?=$this->lang->line('pay_type_1')?>
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="2" <?=(($memberInfo['pay_type']=='2')?'checked':'');?>><?=$this->lang->line('pay_type_2')?>
							<br />
							<input type="radio" class="radio first" id="pay_type" name="postdata[pay_type]" value="3" <?=(($memberInfo['pay_type']=='3')?'checked':'');?>><?=$this->lang->line('pay_type_3')?>
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="4" <?=(($memberInfo['pay_type']=='4')?'checked':'');?>><?=$this->lang->line('pay_type_4')?>
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="5" <?=(($memberInfo['pay_type']=='5')?'checked':'');?>><?=$this->lang->line('pay_type_5')?>
							<input type="radio" class="radio" id="pay_type" name="postdata[pay_type]" value="6" <?=(($memberInfo['pay_type']=='6')?'checked':'');?>><?=$this->lang->line('pay_type_6')?>
							-->
						</li>
						<!-- li>
							<label for=""><?=$this->lang->line('account_no')?></label>
							<input type="text" class="txt" id="account_no" name="postdata[account_no]" value="<?=$memberInfo['account_no'];?>" maxlength="48">
						</li -->
<?
						//T=時間；A=金額
						if($stored_type == 'T'){
?>
						<li>
							<label for=""><?=$this->lang->line('remaining_time')?></label>
							<input type="text" class="txt" id="remaining_time" name="postdata[remaining_time]" value="<?=$memberInfo['remaining_time'];?>">
						</li>
<?
						}else{							
?>
						<li>
							<label for=""><?=$this->lang->line('stored_value')?></label>
							<input type="text" class="txt" id="stored_value" name="postdata[stored_value]" value="<?=$memberInfo['stored_value'];?>">
						</li>
						
<?
						}	
?>
					</ul>
				
				<!-- div class="container-box col-md-6">
					<ul>
						<li>
							<label for=""><?=$this->lang->line('card_expired_date')?></label>
<?
							$card_date_arr = explode('-' ,$memberInfo['card_expired_date']);
							$card_year= '';
							$card_month = '';

							if(	count($card_date_arr )> 1){
								$card_year= $card_date_arr [0];
								$card_month = $card_date_arr [1];
							}
?>
							<select name="card_expired_month" id="card_expired_month">
<?
								for($m=1; $m<=12; $m++){
									$month = (($m<10)?'0':'').$m; 
									echo '<option value="'.$month.'" '.(($month==$card_month)?'selected':'').'>'.$month.'</option>';
								}
?>
							</select><?=$this->lang->line('month')?>
							<select name="card_expired_year" id="card_expired_year">
<?
								$now_year = date("Y");
								for($y=0; $y<11; $y++){
									$year = $now_year + $y;
									echo '<option value="'.$year.'" '.(($year==$card_year)?'selected':'').'>'.$year.'</option>';
								}
?>
							</select><?=$this->lang->line('year')?>
						</li>
						<li>
							<label for=""><?=$this->lang->line('card_cvv')?></label>
							<input type="text" class="txt short"  name="postdata[card_cvv]" id="card_cvv" value="<?=$memberInfo['card_cvv'];?>" maxlength = "3">
						</li>
					</ul>
				</div -->
			</div>
<?
			//}	
?>
			<!-- p class="title">证件资料</p>
			<div class="section">
				<div class="container-box col-md-6">
					<ul>
						<li class="card">
							<label for=""><?=$this->lang->line('pic_id_front')?></label>
							<p>
								<span id="show_default_pic_id_front">
										<span id="defalut_pic_id_front" style="display:<?echo (($memberInfo["pic_id_front"] == '')?'block':'none')?>;">
											<img src="<?=$web;?>images/id_front.jpg" alt="">
										</span>
										<span id="upload_pic_id_front">
										<?php
											$del_button = '';
											if($memberInfo["pic_id_front"] != ''){
												echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$memberInfo["s_num"].'/pic_id_front" height="175">';
											}
										?>
										</span>
								</span>
								<span class="notice">(建议尺寸：275px * 175px)</span><br/>
								<input class="txt" type="text" id="pic_id_front_txt" name="pic_id_front_txt" class="tb" value="" readonly>
								<input type="file" onchange="$j('#pic_id_front_txt').val($j('#pic_id_front').val());" name="pic_id_front" id="pic_id_front" style="display:none;">
								<input type="button" value="浏览" id="id_pic_id_front" class="btn">
							</p>
						</li>
						<li class="card">
							<label for=""><?=$this->lang->line('pic_id_back')?></label>
							<p>
								<span id="show_default_pic_id_back">
										<span id="defalut_pic_id_back" style="display:<?echo (($memberInfo["pic_id_back"] == '')?'block':'none')?>;">
											<img src="<?=$web;?>images/id_back.jpg" alt="">
										</span>
										<span id="upload_pic_id_back">
										<?php
											$del_button = '';
											if($memberInfo["pic_id_back"] != ''){
												echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$memberInfo["s_num"].'/pic_id_back" height="175">';
											}
										?>
										</span>
								</span>
								<span class="notice">(建议尺寸：275px * 175px)</span><br/>
								<input class="txt" type="text" id="pic_id_back_txt" name="pic_id_back_txt" class="tb" value="" readonly>
								<input type="file" onchange="$j('#pic_id_back_txt').val($j('#pic_id_back').val());" name="pic_id_back" id="pic_id_back" style="display:none;">
								<input type="button" value="浏览" id="id_pic_id_back" class="btn">
							</p>
						</li>
						<li class="auto">
							<label for=""><?=$this->lang->line('nationality')?></label>

							<select name="postdata[nationality]" id="nationality" class="txt short">
								
							</select>
						</li>
					</ul>
				</div>
				<div class="container-box col-md-6">
					<ul>
						<li class="card">
							<label for=""><?=$this->lang->line('pic_driver_front')?></label>
							<p>
								<span id="show_default_pic_driver_front">
										<span id="defalut_pic_driver_front" style="display:<?echo (($memberInfo["pic_driver_front"] == '')?'block':'none')?>;">
											<img src="<?=$web;?>images/license_front.jpg" alt="">
										</span>
										<span id="upload_pic_driver_front">
										<?php
											$del_button = '';
											if($memberInfo["pic_driver_front"] != ''){
												echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$memberInfo["s_num"].'/pic_driver_front" height="175">';
											}
										?>
										</span>
								</span>
								<span class="notice">(建议尺寸：275px * 175px)</span><br/>
								<input class="txt" type="text" id="pic_driver_front_txt" name="pic_driver_front_txt" class="tb" value="" readonly>
								<input type="file" onchange="$j('#pic_driver_front_txt').val($j('#pic_driver_front').val());" name="pic_driver_front" id="pic_driver_front" style="display:none;">
								<input type="button" value="浏览" id="id_pic_driver_front" class="btn">
							</p>
						</li>
						<li class="card">
							<label for=""><?=$this->lang->line('pic_driver_back')?></label>
							<p>
								<span id="show_default_pic_driver_back">
										<span id="defalut_pic_driver_back" style="display:<?echo (($memberInfo["pic_driver_back"] == '')?'block':'none')?>;">
											<img src="<?=$web;?>images/license_back.jpg" alt="">
										</span>
										<span id="upload_pic_driver_back">
										<?php
											$del_button = '';
											if($memberInfo["pic_driver_back"] != ''){
												echo '<img src="'.$get_full_url_random.'/getImageSrc/'.$memberInfo["s_num"].'/pic_driver_back" height="175">';
											}
										?>
										</span>
								</span>
								<span class="notice">(建议尺寸：275px * 175px)</span><br/>
								<input class="txt" type="text" id="pic_driver_back_txt" name="pic_driver_back_txt" class="tb" value="" readonly>
								<input type="file" onchange="$j('#pic_driver_back_txt').val($j('#pic_driver_back').val());" name="pic_driver_back" id="pic_driver_back" style="display:none;">
								<input type="button" value="浏览" id="id_pic_driver_back" class="btn">
							</P>
						</li>
						<li>
							<label for="" id="label_driver_license_type"><?=$this->lang->line('driver_license_type')?></label>
							<input type="radio" id="driver_license_type" name="postdata[driver_license_type]" class="radio first" value="C" <?=(($memberInfo["driver_license_type"] == 'C')?'checked':'');?>><?=$this->lang->line('driver_license_type_2')?>
							<input type="radio" id="driver_license_type" name="postdata[driver_license_type]" class="radio" value="T" <?=(($memberInfo["driver_license_type"] == 'T')?'checked':'');?>><?=$this->lang->line('driver_license_type_0')?>
							<input type="radio" id="driver_license_type" name="postdata[driver_license_type]" class="radio" value="I" <?=(($memberInfo["driver_license_type"] == 'I')?'checked':'');?>><?=$this->lang->line('driver_license_type_1')?>
						</li>
						<li>
							<label for="" id="label_driver_license_number"><?=$this->lang->line('driver_license_number')?></label>
							<input type="text" class="txt" id="driver_license_number" name="postdata[driver_license_number]" value="<?=$memberInfo["driver_license_number"]?>">
						</li>
					</ul>
				</div>
			</div -->
			<div class="container-box" style="width:20%;">				
				<p class="title">租借资讯</p>
				<ul>
					<li>
						<label for=""><?=$this->lang->line('member_deposit')?></label>

						$ <input type="text" class="txt" id="deposit" name="postdata[deposit]" value="<?=$memberInfo['deposit'];?>" >
					</li>
					<li>
						<label for=""><?=$this->lang->line('change_count')?></label>
						<input type="text" class="txt" id="change_count" name="postdata[change_count]" value="<?=$memberInfo['change_count'];?>" readonly>
					</li>
					<li>
						<label for=""><?=$this->lang->line('lease_token')?></label>
						<input type="text" class="txt" id="lease_token" name="postdata[lease_token]" value="<?=$memberInfo['lease_token'];?>">
					</li>
				</ul>
			</div>	
			<div class="container-box" style="width:20%;">
				<p class="title">&nbsp;</p>
				<ul>
					<li>
						<label for=""><?=$this->lang->line('verification_code')?></label>
						<input type="text" class="txt" id="verification_code" name="postdata[verification_code]" value="<?=$memberInfo['verification_code'];?>">
					</li>
					<li>
						<label for=""><?=$this->lang->line('lease_status')?></label>
						<input type="radio" class="radio first" id="lease_status" name="postdata[lease_status]" value="L" <?=(($memberInfo['lease_status']=='L')?'checked':'');?>><?=$this->lang->line('lease_status_0')?>
						<input type="radio" id="lease_status" name="postdata[lease_status]" class="radio" value="N" <?=(($memberInfo['lease_status']=='N')?'checked':'');?>><?=$this->lang->line('lease_status_1')?>
					</li>
					<li>
						<label for=""><?=$this->lang->line('send_sms_mail')?></label>
						<input type="radio" class="radio first" id="send_sms_mail" name="postdata[send_sms_mail]" value="N" <?=(($memberInfo['send_sms_mail']=='N')?'checked':'');?>><?=$this->lang->line('send_sms_mail_0')?>
						<input type="radio" class="radio" id="send_sms_mail" name="postdata[send_sms_mail]" value="Y" <?=(($memberInfo['send_sms_mail']=='Y')?'checked':'');?>><?=$this->lang->line('send_sms_mail_1')?>
				</li>	
						
				</ul>
			</div>
			<div class="sectin buttonbar" id="body">
				<?=$user_access_control?>
			</div>
			<INPUT TYPE="hidden" NAME="old_send_sms_mail" value="<?=$memberInfo['send_sms_mail'];?>">
			</form>
		</div>
    </div>


<?
	//取得语系
	if($this->session->userdata('default_language')){
		$lang = $this->session->userdata('default_language');
	}else{
		$lang = $this->session->userdata('display_language');
	}
	if($lang == ""){
		$lang = $this->config->item('language');
	}
?>
<script type="text/javascript">
	$j(document).ready(function() {
		//设定script语系
		<?='$j.validationEngineLanguage.newLang_'.$lang.'();'?>
		$j("#formID").validationEngine();
		//储存和取消
		save_cancel();

		//国码
		var now_nationality = '<?=$memberInfo["nationality"];?>';
		$j.getJSON("<?=$web?>js/isojson.js",function(result){
			$j.each(result, function(i, field){
				$j("#nationality").append("<option value='" + field['country-code'] + "'>" + field['name'] + "</option>");

				if(now_nationality ==field['country-code'] ){
					 $j("#nationality  [value='"+now_nationality+"']").prop("selected", true);
				}
			});
		});
	});

	$j(function(){
		var province = '<?=$memberInfo["province"]?>';
		var city = '<?=$memberInfo["city"]?>';
		var district = '<?=$memberInfo["district"]?>';
		$j("#cnzipcode").distpicker({
			province: province,
			city: city,
			district: district
		});

		// 地址选择
		/*var zipcode = '<?=$memberInfo["zip_code"]?>';
		$j('#twzipcode').twzipcode({
			'zipcodeSel': zipcode,
			'countyName'   : 'county',		// 预设值为 county
			'districtName' : 'district',	// 预设值为 district
			'zipcodeName'  : 'zipcode',		// 预设值为 zipcode
			'css': [
				'county txt',		//县市
				'district txt',		// 乡镇市区
				'zipcode txt'		// 邮递区号
			]
		});*/
	});
	
	//储存
	function fn_save(){
		//把邮区等等的值塞到hidden
		/*var zip_code = $j('#twzipcode').find('input[name=zipcode]').val();
		var city = $j('#twzipcode').find('select[name=county]').val();
		var district = $j('#twzipcode').find('select[name=district]').val();

		$j('#zip_code').val(zip_code);
		$j('#city').val(city);
		$j('#district').val(district);

		if($('input[id="status"]:checked').val() == 'Y'){
			$j("#address").addClass("validate[required]");
			$j( "input[name='zipcode']" ).addClass("validate[required]");
			$j("#driver_license_type").addClass("validate[required]");
			$j("#driver_license_number").addClass("validate[required]");
			$j("#label_address").addClass("required");
			$j("#label_driver_license_type").addClass("required");
			$j("#label_driver_license_number").addClass("required");
		}else{
			$j("#address").removeClass("validate[required]");
			$j( "input[name='zipcode']" ).removeClass("validate[required]");
			$j("#driver_license_type").removeClass("validate[required]");
			$j("#driver_license_number").removeClass("validate[required]");
		}*/
		
		//2017/8/21 這些欄位不用必填
		/*if($j('input[id="status"]:checked').val() == 'Y'){
			$j("#address").addClass("validate[required]");
			$j("#province").addClass("validate[required]");
			$j("#city").addClass("validate[required]");
			$j("#driver_license_type").addClass("validate[required]");
			$j("#driver_license_number").addClass("validate[required]");
			$j("#label_address").addClass("required");
			$j("#label_driver_license_type").addClass("required");
			$j("#label_driver_license_number").addClass("required");
		}else{
			$j("#address").removeClass("validate[required]");
			$j("#province").removeClass("validate[required]");
			$j("#city").removeClass("validate[required]");
			$j("#driver_license_type").removeClass("validate[required]");
			$j("#driver_license_number").removeClass("validate[required]");
		}*/

		$j("#formID").submit();
	}

	function check_label(){
		/*if($j('input[id="status"]:checked').val() == 'Y'){
			$j("#label_zipcode").addClass("required");
			$j("#label_address").addClass("required");
			$j("#label_driver_license_type").addClass("required");
			$j("#label_driver_license_number").addClass("required");
		}else{
			$j("#label_zipcode").removeClass("required");
			$j("#label_address").removeClass("required");
			$j("#label_driver_license_type").removeClass("required");
			$j("#label_driver_license_number").removeClass("required");
		}*/
	}
</script>

<script type="text/javascript">
var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

$j("#id_pic_face").click(function(){
	$j("#pic_face").click();
});
$j("#id_pic_id_front").click(function(){
	$j("#pic_id_front").click();
});
$j("#id_pic_id_back").click(function(){
	$j("#pic_id_back").click();
});
$j("#id_pic_driver_front").click(function(){
	$j("#pic_driver_front").click();
});
$j("#id_pic_driver_back").click(function(){
	$j("#pic_driver_back").click();
});

function change_member_span(){
	$j(".member_type").css('display','none');
	var open = '';

	$j('input[name="member_type[]"]:checked').each(function() {
		open =$j(this).val();
		open = ((open == 'A' || open == 'B')?'A':open);
		open = ((open == 'D' || open == 'E')?'DE':open);
		$j("#member_type_"+open ).css('display','block');
	});
	//$j("#"+id_span).css('display','block');
}
function change_lease_span(id_span, lang_type){
	$j(".lease_type").css('display','none');
	$j("#"+id_span).css('display','block');

	$j(".deposit").css('display','none');
	$j("#"+lang_type).css('display','block');
}

function change_sub_num(filed_1, filed_2){
	var filed_val = $j("#"+filed_1).val();
	
	
	if(filed_val == ''){
		$j("#"+filed_2+" option").show();
	}else{
		$j("#"+filed_2+ "option:selected").removeAttr("selected");
		$j("#"+filed_2+" option").hide();
		$j("#"+filed_2+" option["+filed_1+"='"+filed_val+"']").show();

		$j("#"+filed_2+" option[value='']").show();
		$j("#"+filed_2+" option[value='']").prop('selected', true);
	}
}
</script>

<!-- script src="http://code.jquery.com/jquery-1.4.2.min.js" type="text/javascript"></script -->

<script language="JavaScript" type="text/javascript">
	//这里控制要检查的项目，true表示要检查，false表示不检查 
	var isCheckImageType = true;  //是否检查图片副档名 
	var isCheckImageWidth = true;  //是否检查图片宽度 
	var isCheckImageHeight = true;  //是否检查图片高度 
	var isCheckImageSize = true;  //是否检查图片档案大小 

	var ImageSizeLimit = 1024*1024;  //上传上限，单位:byte 
	var ImageWidthLimit = 1200;  //图片宽度上限 
	var ImageHeightLimit = 1000;  //图片高度上限 

	function checkFile(file, file_id) { 
		var value = file.value; 
		var re = /\.(jpg|png)$/i;  //允许的图片副档名 
		if (isCheckImageType && !re.test(value)) { 
			alert("只允许上传jpg或png影像档"); 
			 $j('#'+file_id+'_txt').val('');
		} else { 
			var f = file.files[0];
				var reader = new FileReader();
				reader.onload = function (e) {
					var data = e.target.result;
					//加载图片获取图片真实宽度和高度
					var image = new Image();
					image.onload=function(){
						var width = image.width;
						var height = image.height;
						var size = f.size;

						if(size > ImageSizeLimit){
							alert('图档限1MB以下');
							 $j('#'+file_id+'_txt').val('');
						}else{
							 $j('#'+file_id+'_txt').val($j('#'+file_id).val());
						}
						//alert(width+'======'+height+"====="+f.size);
					};
					image.src= data;
			   };
			 reader.readAsDataURL(f);
		} 
	} 
</script>
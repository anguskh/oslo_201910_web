<?
	$web = $this->config->item('base_url');
	if($stored_type == 'T'){
		$colArr = array (
			"user_id"			=>	"User ID",
			"name"				=>	"{$this->lang->line('member_name')}",
			"member_type"		=>	"{$this->lang->line('member_member_type')}",
			"phone"				=>	"{$this->lang->line('member_phone')}",
			"mobile"			=>	"{$this->lang->line('member_mobile')}",
			"city"				=>	"{$this->lang->line('member_city')}",
			"deposit"			=>	"{$this->lang->line('member_deposit')}",
			"remaining_time"	=>	"{$this->lang->line('member_remaining_time')}",
			"status"			=>	"{$this->lang->line('member_status')}",
			"change_count"			=>	"{$this->lang->line('change_count')}",
			"battery_recording"	=>	"租借历程",
			"log_pay"			=>	"储值扣款记录"
			
		);
	}else{
		$colArr = array (
			"user_id"			=>	"User ID",
			"name"				=>	"{$this->lang->line('member_name')}",
			"member_type"		=>	"{$this->lang->line('member_member_type')}",
			"phone"				=>	"{$this->lang->line('member_phone')}",
			"mobile"			=>	"{$this->lang->line('member_mobile')}",
			"city"				=>	"{$this->lang->line('member_city')}",
			"deposit"			=>	"{$this->lang->line('member_deposit')}",
			"stored_value"		=>	"{$this->lang->line('member_stored_value')}",
			"status"			=>	"{$this->lang->line('member_status')}",
			"change_count"		=>	"{$this->lang->line('change_count')}",
			"battery_recording"	=>	"租借历程",
			"log_pay"			=>	"储值扣款记录"
		);
	
	}

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"user_id"				=>  "User ID",
		"phone"					=>	"{$this->lang->line('member_phone')}",
		"mobile"				=>	"{$this->lang->line('member_mobile')}",
		"status"				=>	"{$this->lang->line('status')}"
	);
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$status_arr= array("R"=>"审核中", "Y"=>"启用", "N"=>"停用");
	$fieldType = array(
		"status"		=>  $status_arr
	);


	$searchData = $this->session->userdata("{$fn}_".'searchData');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData, $fieldType);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt');

	$tde02_name = array('','直营','加盟','其它');
	$member_type_name = array( 
		"A"=>"车辆租借",
		"B"=>"換電",
		"C"=>"销售",
		"D"=>"充电",
		"E"=>"換電",
		"F"=>"多颗电池"
	);
?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('member_management')?></p>
				<div class="topbtn column">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="姓名" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear2" class="clear" value="清 空" />
						<input type="button" id="btSearch2" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<?=$user_access_control?>
						<!-- Search Starts Here -->
						<div id="searchContainer">
							<a href="#" id="searchButton" class="buttoninActive smore active"><span><i class="fas fa-caret-right"></i>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<form id="listForm" name="listForm" action="">
				<!--预防CSRF攻击-->
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
				<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="member">
					<tr class="GridViewScrollHeader">
						<th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th>
<?
						//让标题栏位有排序功能
                        foreach ($colInfo["colName"] as $enName => $chName) {
							//排序动作
							$url_link = $get_full_url_random;
							$order_link = "";
							$orderby_url = $url_link."/index?field=".$enName."&orderby=";
							if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
								$symbol = '▲';
								$next_orderby = "desc";
							}else{
								$symbol = '▼';
								$next_orderby = "asc";
							}
							if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
								$orderby_url .= $next_orderby;
								$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
							}else{
								$orderby_url .= "asc";
							}
							$class = "";
							switch($enName)
							{
								case "user_id":
									$class = "class='id'";
									break;
								case "name":
									$class = "class='name'";
									break;
								case "member_type":
									$class = "class='type'";
									break;
								case "phone":
									$class = "class='tel'";
									break;
								case "mobile":
									$class = "class='mobile'";
									break;
								case "city":
									$class = "class='city'";
									break;
								case "deposit":
									$class = "class='rent'";
									break;
								case "stored_value":
									$class = "class='time'";
									break;
								case "status":
									$class = "class='status'";
									break;
								case "battery_recording":
									$class = "class='log'";
									break;
								case "log_pay":
									$class = "class='record'";
									break;
							}
							//栏位显示
                            echo "					<th align=\"center\" {$class}>
															<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
															{$order_link}
													</th>\n";
                        }
?>
					</tr>
<?
	foreach ($memberInfoRow as $key => $memberInfoArr) {
		echo "				<tr class=\"GridViewScrollItem\">\n" ;
		
		$s_num = $memberInfoArr["s_num"] ;
		$checkStr = "
<td>
	<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$s_num}\" >
</td>
		" ;
		echo $checkStr ;
		foreach ($colInfo["colName"] as $enName => $chName) {
			
			$class = "";
			switch($enName)
			{
				case "user_id":
					$class = "class='id'";
					break;
				case "name":
					$class = "class='name'";
					break;
				case "member_type":
					$class = "class='type'";
					break;
				case "phone":
					$class = "class='tel'";
					break;
				case "mobile":
					$class = "class='mobile'";
					break;
				case "city":
					$class = "class='city'";
					break;
				case "deposit":
					$class = "class='rent'";
					break;
				case "stored_value":
					$class = "class='time'";
					break;
				case "status":
					$class = "class='status'";
					break;
				case "battery_recording":
					$class = "class='log'";
					break;
				case "log_pay":
					$class = "class='record'";
					break;
			}

			if ($enName == "s_num") {
				echo "					<td align=\"center\" {$class}>".$memberInfoArr[$enName]."</td>\n" ;
			} else if ($enName == "status") {
				switch($memberInfoArr[$enName]) {
					case "R":
						echo "					<td align=\"center\" {$class}>{$this->lang->line('status_0')}</td>\n" ;
						break;
					case "Y":
						echo "					<td align=\"center\" {$class}>{$this->lang->line('status_1')}</td>\n" ;
						break;
					case "N":
						echo "					<td class=\"no,status\" align=\"center\">{$this->lang->line('status_2')}</td>\n" ;
						break;
					case "D":
						echo "					<td  class=\"no,status\" align=\"center\">{$this->lang->line('delete')}</td>\n" ;
						break;
					default:
						echo "					<td align=\"center\" {$class}></td>\n" ;
						break;
				}
			}else if($enName == "tde02"){
				echo "					<td align=\"left\" {$class}>".$tde02_name[$memberInfoArr[$enName]]."</td>\n" ;

			}else if($enName == "battery_recording"){
				$main_startrow = $this->session->userdata('PageStartRow');
				echo '<td align="left" id="noclick" '.$class.'><a href="'.$get_full_url_random.'/detail_record/'.$s_num.'/0/'.$main_startrow.'"><i class="fas fa-list-alt"></i></a></td>';

			}else if($enName == "log_pay"){
				$main_startrow = $this->session->userdata('PageStartRow');
				echo '<td align="left" id="noclick" '.$class.'><a href="'.$get_full_url_random.'/detail_pay/'.$s_num.'/0/'.$main_startrow.'"><i class="fas fa-file-invoice-dollar"></i></a></td>';

			}else if($enName == 'member_type'){
				$member_type_arr =explode(',', $memberInfoArr[$enName]);
				
				$member_type_names = '';
				if($memberInfoArr[$enName] != ''){
					foreach($member_type_arr  as $arr){
						$member_type_names[] = $member_type_name[$arr];
					}
					$member_type_names = join( ",", $member_type_names );
				}
				
				echo "					<td align=\"left\" {$class}>".$member_type_names."</td>\n" ;
			}else if($enName == 'user_id'){
				
				echo "					<td align=\"left\" {$class}>".$memberInfoArr[$enName]."</td>\n" ;
			}else if($enName == 'change_count'){
				echo "					<td align=\"center\" {$class}>".$memberInfoArr[$enName]."</td>\n" ;
			}else {
				echo "					<td align=\"left\" {$class}>".$memberInfoArr[$enName]."</td>\n" ;
			}
		}
		echo "				</tr>\n" ;
	}
	
?>
			</table>
			</form>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
		</div>
    </div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/member/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/member/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

</script>

<?
	$web = $this->config->item('base_url');

	$colArr = array (
		"type"					=>	"{$this->lang->line('log_store_balance_charge_type')}",
		"exchange_date"			=>	"{$this->lang->line('log_store_balance_charge_exchange_date')}",
		"charge_amount"			=>	"{$this->lang->line('log_store_balance_charge_charge_amount')}",
		"store_balance_before"	=>	"{$this->lang->line('log_store_balance_charge_store_balance_before')}",
		"store_balance_after"		=>	"{$this->lang->line('log_store_balance_charge_store_balance_after')}"
	);

	$colInfo = array("colName" => $colArr);
	
	$get_full_url_random = get_full_url_random();
	//查询网址
	$search_url = "{$get_full_url_random}/search";
	//新增网址
	$add_url = "{$get_full_url_random}/addition";
	//修改网址
	$edit_url = "{$get_full_url_random}/modification";
	//检视网址
	$view_url = "{$get_full_url_random}/view";
	//删除网址
	$del_url = "{$get_full_url_random}/delete_db";
	
	//将searchData填入目前搜寻栏位
	$fn = get_fetch_class_random();

	$searchArr = array (
		"phone"					=>	"{$this->lang->line('member_phone')}",
		"mobile"				=>	"{$this->lang->line('member_mobile')}",
		"status"				=>	"{$this->lang->line('status')}"
	);
	//指定栏位类别, 提供搜寻栏位建立 array('栏位名称'=> '栏位类型', ....)
	$status_arr= array("R"=>"审核中", "启用"=>"Y","停用"=>"N");
	/*$fieldType = array(
		"status"		=>  $status_arr
	);*/


	$searchData = $this->session->userdata("{$fn}_".'searchData_detail');
	//建立搜寻栏位
	$search_box = create_search_box($searchArr, $searchData);
	$s_search_txt = $this->session->userdata("{$fn}_".'search_txt_detail');

	$pay_type_name = array( 
		"S"=>"储值",
		"C"=>"扣款",
		"D"=>"押金",
		"M"=>"租借時間(分鐘)",
		"RD"=>"退押金",
		"RS"=>"退儲值"
	);
?>
<!--功能按钮显示控制 start-->
<script type="text/javascript" src="<?=$web?>js/common/button_display.js"></script> 
<!--功能按钮显示控制 end-->
    <div class="wrapper">
		<div class="box">
			<div class="topper">
				<p class="btitle"><?=$this->lang->line('member_management_'.$d_type)?> <?=$member_name;?></p>
				<p class="title"></p>
				<!-- div class="topbtn">
					<div class="normalsearch">
						<input type="text" class="txt searchData" id="s_search_txt" placeholder="姓名" value="<?=$s_search_txt;?>">
						<input type="button" id="btClear" class="clear" value="清 空" />
						<input type="button" id="btSearch" class="search" value="搜 寻" />
					</div>
					<div class="btnbox">
						<!-- Search Starts Here -->
						<!-- div id="searchContainer">
							<a href="#" id="searchButton" class="buttonblack smore active"><span>进阶<?=$this->lang->line('search')?></span></a>
							<div id="searchBox">                
								<form id="searchForm" method="post" action="<?=$search_url?>">
									<!--预防CSRF攻击-->
									<!-- input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
									<fieldset2 id="body">
										<fieldset2>
											<?=$search_box?>
										</fieldset2>
									</fieldset2>
									<INPUT TYPE="hidden" NAME="search_txt" id="search_txt">
								</form>
							</div>
						</div>
					</div>
				</div -->
			</div>
			<ul class="tabs">
				<li><a href="<?=$get_full_url_random;?>/detail_record/<?=$member_sn?>/0/<?=$main_startRow?>">租借历程记录</a></li>
				<li class="on"><a href="<?=$get_full_url_random;?>/detail_pay/<?=$member_sn?>/0/<?=$main_startRow?>">储值扣款记录</a></li>
			</ul>
			<div class="tabbox">
				<div class="tab_container">
					<form id="listForm" name="listForm" action="">
						<!--预防CSRF攻击-->
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
						<input type="hidden" name="start_row" value="<?=$this->session->userdata('PageStartRow')?>" />
						<table id="listTable" cellpadding="0" cellspacing="0" border="0" class="price">
							<tr class="GridViewScrollHeader">
								<!-- th><input type="checkbox" name="ckbAll" id="ckbAll" value=""></th -->
		<?
								$start_row = $this->session->userdata('PageStartRow');

								//让标题栏位有排序功能
								foreach ($colInfo["colName"] as $enName => $chName) {
									//排序动作
									$url_link = $get_full_url_random;
									$order_link = "";
									$orderby_url = $url_link."/detail_pay/".$member_sn."/".$start_row."/".$main_startRow."?ffield=".$enName."&orderby=";
									if($this->session->userdata(get_fetch_class_random().'_orderby') == "asc"){
										$symbol = '▲';
										$next_orderby = "desc";
									}else{
										$symbol = '▼';
										$next_orderby = "asc";
									}
									if(strtoupper($this->session->userdata(get_fetch_class_random().'_field')) == strtoupper($enName)){
										$orderby_url .= $next_orderby;
										$order_link = "<center><a class='desc' fieldname='{$enName}'href='{$orderby_url}'><font color='red'>{$symbol}</font></a></center>";
									}else{
										$orderby_url .= "asc";
									}
									
									//栏位显示
									echo "					<th align=\"center\">
																	<a class='orderby' fieldname='{$enName}' href='{$orderby_url}'>$chName</a>
																	{$order_link}
															</th>\n";
								}
		?>
							</tr>
		<?
			foreach ($InfoRow as $key => $InfoArr) {
				echo "				<tr  class=\"GridViewScrollItem\">\n" ;
				
				$s_num = $InfoArr["s_num"] ;
				$checkStr = "
		<!-- td>
			<input type=\"checkbox\" class=\"ckbSel\" name=\"ckbSelArr[]\" value=\"{$s_num}\" >
		</td -->
				" ;
				echo $checkStr ;
				foreach ($colInfo["colName"] as $enName => $chName) {
					
					if ($enName == "s_num") {
						echo "					<td align=\"center\">".$InfoArr[$enName]."</td>\n" ;
					}else if ($enName == "type") {
						echo "					<td align=\"center\">".$pay_type_name[$InfoArr[$enName]]."</td>\n" ;
					}else {
						echo "					<td align=\"left\">".$InfoArr[$enName]."</td>\n" ;
					}
				}
				echo "				</tr>\n" ;
			}
			
		?>

					</table>
					</form>
				</div>
			</div>
			<div class="pagebar">
				<?=$pageInfo["html"]?>
			</div>
			<a href="<?=$get_full_url_random.'/index/'.$main_startRow?>" class="backbtn">回上一页</a>
		</div>
    </div>
<div id="loadingIMG" class="loading" style="display: none;">
	<div id="img_label" class="img_label">资料处理中，请稍后。</div>
</div>
<script type="text/javascript">

var triggers = $j(".modalInput").overlay({
	// some mask tweaks suitable for modal dialogs
	mask: {
		color: '#000',
		loadSpeed: 200,
		opacity: 0.7
	},

	closeOnClick: false, 
	closeOnEsc: false
});
  
// 关闭弹出视窗
$j('a.close, #modal').live('click', function() {
	// close the overlay
	var k=triggers.length;
	for(var i=0 ;i <=k-1; i++)
	{
		triggers.eq(i).overlay().close();
	}

	return false;
});

/**
 * 新增按钮
 */
$j("#btAddition").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$add_url?>");
	$j("#listForm").submit();
});

/**
 * 修改按钮
 */
$j("#btModification").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$edit_url?>");
	$j("#listForm").submit();
});

/**
 * 检视按钮
 */
$j("#btView").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr("action", "<?=$view_url?>");
	$j("#listForm").submit();
});

/**
 * 删除按钮
 */
$j("#btDelete").click( function () {
	if ( confirm("<?=$this->lang->line('confirm_delete')?>") ) {
		$j("#listForm").attr( "method", "POST" ) ;
		$j("#listForm").attr( "action", "<?=$del_url?>" ) ;
		$j("#listForm").submit() ;
	}
});

/**
 * 上移按钮
 */
$j("#btUp").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/member/sequence_up_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 下移按钮
 */
$j("#btDown").click( function () {
	$j("#listForm").attr( "method", "POST" ) ;
	$j("#listForm").attr( "action", "<?=$web?>nimda/member/sequence_down_db" ) ;
	$j("#listForm").submit() ;
});

/**
 * 搜寻
 */
$j('#btSearch, #btSearch2').click(function () {
	$j("#loadingIMG").show();

	//sumit前要更新 s_search_txt
	$j("#search_txt").val($j("#s_search_txt").val());

	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

$j('#searchData').change(function () {
	$j("#searchForm").attr( "method", "POST" ) ;
	$j("#searchForm").attr( "action", "<?=$search_url?>" ) ;
	$j("#searchForm").submit() ;
});

/**
 * 取得被选取的SN
 */
function getCkbVal()
{
	var val = "" ;
	$j(".ckbSel").each( function () {
		if ( this.checked ) val = this.value ;
	});
	
	return val ;
}

//选任意地方都可勾选checkbox
$j(document).ready(function(){
	//凍結窗格
	gridview(0,false);
});
$j(window).load(function(){
	//等到整个视窗里所有资源都已经全部下载后才会执行
	//功能按钮显示控制
	button_display();
});

</script>

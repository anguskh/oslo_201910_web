<!DOCTYPE html>
<?php
    $gpsArr = array();
	$newgpsArr = array();
    if($logInfo)
    {
        if($logInfo[0]['gps_path_track']==""){
            echo "此紀錄無軌跡";
        }else{
            $gpsArr = explode(';',$logInfo[0]['gps_path_track']);

			for($i=0; $i<count($gpsArr); $i++){
			
				$tmp_gps = explode(',',$gpsArr[$i]);
				if(count($tmp_gps)>1){
					$newgpsArr[] = $tmp_gps[1].','.$tmp_gps[0];
				}
			}
		}
			
    }
    else
    {
        
    }
?>
<html>
 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>行車路线轨迹</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <style type="text/css">
        html,
        body {
            height: 100%;
            margin: 0px;
            padding: 0px
        }
        #container {
            width: 100%;
            height: 100%
        }
    </style>
</head>
 
<body>

    <div id="container"></div>
    <script type="text/javascript" src='https://webapi.amap.com/maps?v=1.4.11&key=59b39aae5ab47b9a99ad555535698481'></script>
    <!-- UI组件库 1.0 -->
    <script src="https://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
    <script type="text/javascript">
    //创建地图
    var map = new AMap.Map('container', {
        zoom: 3
    });

    AMapUI.load(['ui/misc/PathSimplifier', 'lib/$'], function(PathSimplifier, $) {

        if (!PathSimplifier.supportCanvas) {
            alert('当前环境不支持 Canvas！');
            return;
        }

        var pathSimplifierIns = new PathSimplifier({
            zIndex: 100,
            //autoSetFitView:false,
            map: map, //所属的地图实例

            getPath: function(pathData, pathIndex) {

                return pathData.path;
            },
            getHoverTitle: function(pathData, pathIndex, pointIndex) {

                if (pointIndex >= 0) {
                    //point 
                    return pathData.name + '，点：' + pointIndex + '/' + pathData.path.length;
                }

                return pathData.name + '，点数量' + pathData.path.length;
            },
            renderOptions: {

                renderAllPointsIfNumberBelow: 100 //绘制路线节点，如不需要可设置为-1
            }
        });

        window.pathSimplifierIns = pathSimplifierIns;

        //设置数据
        pathSimplifierIns.setData([{
            name: '路线',
            path: [

<?php
            for($i=0;$i<count($newgpsArr);$i++)
            {
                echo "[{$newgpsArr[$i]}],";
            }
?>
                
                
            ]
        }]);

        //对第一条线路（即索引 0）创建一个巡航器
        var navg1 = pathSimplifierIns.createPathNavigator(0, {
            loop: false, //循环播放
            speed: 500 //巡航速度，单位千米/小时
        });

        navg1.start();
    });
    </script>

</body>
</html>
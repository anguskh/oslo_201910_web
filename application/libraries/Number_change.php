<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Number_change {

   	public function getChineseNumber($money){

	    $ar = array("零", "壹", "贰", "参", "肆", "伍", "陆", "柒", "捌", "玖") ;
	    $cName = array("", "", "拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟");
	    $conver = "";
	    $cLast = "" ;
	    $cZero = 0;
	    $i = 0;
	    for ($j = strlen($money) ; $j >=1 ; $j--){  
			$cNum = intval(substr($money, $i, 1));
			$cunit = $cName[$j]; //取出位数
			if ($cNum == 0) { //判断取出的数字是否为0,如果是0,则记录共有几0
			 $cZero++;
			 if (strpos($cunit,"万亿") >0 && ($cLast == "")){ // '如果取出的是万,亿,则位数以万亿来补
			  $cLast = $cunit ;
			 }      
			}else {
			if ($cZero > 0) {// '如果取出的数字0有n个,则以零代替所有的0
			  if (strpos("万亿", substr($conver, strlen($conver)-2)) >0) {
			     $conver .= $cLast; //'如果最后一位不是亿,万,则最后一位补上"亿万"
			  }
			  $conver .=  "零" ;
			  $cZero = 0;
			  $cLast = "" ;
			}
			 $conver = $conver.$ar[$cNum].$cunit; // '如果取出的数字没有0,则是中文数字+单位          
			}
			$i++;
	    }  
	  //'判断数字的最后一位是否为0,如果最后一位为0,则把万亿补上
	    if (strpos("万亿", substr($conver, strlen($conver)-2)) >0) {
	       $conver .=$cLast; // '如果最后一位不是亿,万,则最后一位补上"亿万"
	    }
	    return $conver;
	}
}
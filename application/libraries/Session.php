<?php
class Session {

	//function session(){ for php7修正
	public function __construct()
	{
		$this->_set_session();
	}

	function _set_session(){
		session_start();
	}

	function sess_destroy(){
		session_destroy();
	}
	
	function getDBname(){
		//给予session加上DB名称
		//DB名称.'_'.自定义名称
		$CI =& get_instance();
		$CI->load->model("common/model_background", "model_background") ;
		return $CI->model_background->getDefDBname();
	}

	//增加所有平台session
	function set_all_userdata($session_name,$session_value){
		$_SESSION[$session_name] = $session_value;
	}

	function set_userdata($session_name,$session_value){
		$DBName = $this->getDBname();
		$session_name = $DBName.'_'.$session_name;
		$_SESSION[$session_name] = $session_value;
	}

	function userdata($session_name){
		$DBName = $this->getDBname();
		$session_name = $DBName.'_'.$session_name;
		if(isset($_SESSION[$session_name]))
			return $_SESSION[$session_name];
		return false;
	}

	function unset_userdata($session_name){
		$DBName = $this->getDBname();
		$session_name = $DBName.'_'.$session_name;
		if(isset($_SESSION[$session_name]))
			unset($_SESSION[$session_name]);
	}

}
?>
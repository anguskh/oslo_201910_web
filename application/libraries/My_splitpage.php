<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_splitpage
{
	private $_Total_rows;	// 总笔数
	private $_Page_rows;	// 每页笔数
	private $_PageCnt;		// 总分页数
	private $_NowPage;		// 现在页数
	private $_StartRow;		// 现在笔数
	private $_CurrentPage;	// 现在页面
	private $_OtherNum;	// 是否还有后面的页码
	private $_lang;
	private $_new_URL;
	private $_para;
	
	public function __construct( $spConfigArr )
	{
		$CI =& get_instance();
		
		if($CI->session->userdata('default_language'))
		{
			$this->_lang = $CI->session->userdata('default_language');
		}
		else {
			$this->_lang = $CI->session->userdata('display_language');
		}
		$this->_CurrentPage = 'index';
		$this->_Page_rows = $spConfigArr["base_pageRow"];
		
		$this->reset_url();
	}
	
	public function reset_url(){
		$CI =& get_instance();
		$random_function = $CI->uri->segment(2);
		$random_function = explode('__',$random_function);
		$random_name = "";
		if(isset($random_function[1])){
			$random_name = $random_function[1];
			if($CI->router->fetch_method() != 'index' && $CI->router->fetch_method() != ''){
				$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'__'.$random_name.'/'.$CI->router->fetch_method().'/'.$this->_CurrentPage;
			}else{
				$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'__'.$random_name.'/'.$this->_CurrentPage;
			}
		}else{
			if($CI->router->fetch_method() != 'index' && $CI->router->fetch_method() != ''){
				$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/'.$CI->router->fetch_method().'/'.$this->_CurrentPage;
			}else{
				$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/'.$this->_CurrentPage;
			}

			//$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/'.$this->_CurrentPage;
		}
		//$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/'.$CI->router->fetch_method();
		//$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/index';
		
		$para = $CI->uri->segment(5);
		if($para != "" && $CI->router->fetch_method()!="bssdetaillist"){
			$this->_para = $para;
		}else{
			$this->_para = "";
		}
	}

	/**
	 * 
	 */	
	public function getPageAreaArr( $totalRow, $startRow = 0, $currentPage = "")
	{
		$pageAreaArr = array() ;
		$this->_Total_rows = $totalRow ;
		$this->_StartRow = $startRow ;
		if($currentPage != ""){
			$this->_CurrentPage = $currentPage;
		}else{
			$this->_CurrentPage = 'index';
		}
		if($currentPage != ''){
			//重新指定網址
			$this->reset_url();
		}
		$pagebuttonArr = $this->makePageButton();

		$pageAreaArr["html"]  = "";
		foreach( $pagebuttonArr as $buttonStr )
		{
			$pageAreaArr["html"] .= $buttonStr ;
		}
		
		$pageAreaArr["js"] = "";

		return $pageAreaArr ;
	}
	
	private function makePageButton()
	{
		$btnArr = array();
		$this->_PageCnt = ceil($this->_Total_rows / $this->_Page_rows ) ;

		array_push( $btnArr, $this->retFirstButton() ) ;
		array_push( $btnArr, $this->retPreviousButton() ) ;
		
		for ( $i = 0; $i < $this->_PageCnt; $i++ )
		{
			$this->retPageButton( $i ) ;
			if($this->retPageButton( $i ) != ""){
				array_push( $btnArr , $this->retPageButton( $i ) ) ;
			}
		}
		
		if($this->_OtherNum == true){	//显示有后面其他页码, 以...显示
			array_push( $btnArr , '...' ) ;
		}
		
		// array_push( $btnArr, $this->retNextButton() ) ;
		// array_push( $btnArr, $this->retLastButton( $i ) ) ;
		
		
		array_push( $btnArr, $this->retNextButton() ) ;
		array_push( $btnArr, $this->retLastButton( $i ) ) ;
		array_push( $btnArr, $this->retInputPage( $i ) ) ;

		return $btnArr ;
	}
	
	private function retPageButton( $pageNum )
	{
		$showNum = $pageNum + 1;
		$moveIndex = $pageNum * $this->_Page_rows;
		//echo 'index:'.$pageNum;
		if($this->_Total_rows > $this->_Page_rows)
		{
			//分页显是最多笔数 共 11笔
			$min = $this->_StartRow - ($this->_Page_rows * 5);
			$max = $this->_StartRow + ($this->_Page_rows * 5);
			
			if($moveIndex >= $min && $moveIndex <= $max){ //在区间内才显示
				if ( $this->_StartRow == $moveIndex ) {
					$this->_NowPage = $showNum;
					$strButton = "	<span class=\"disabled\">{$showNum}</span>" ;
				} else {
					if ($this->_StartRow < 1)
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$showNum}</a>" ;
					else if ($pageNum < 1)
						$strButton = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\">{$showNum}</a>" ;
					else
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$showNum}</a>" ;
				}
				
				if(($this->_PageCnt -1) != $pageNum){	//无出现最后分页码
					$this->_OtherNum = true;
				}else{
					$this->_OtherNum = false;
				}
				
			}else{
				$strButton = "";
			}

			return $strButton ;
		}
	}
	
	/**
	 * 处理第一页
	 */
	private function retFirstButton()
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
	
		$strButton = "";
		if($this->_StartRow == 0)
			$strButton = "" ;
		else
			
			$strButton = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_first_page')}</a>" ;

		return $strButton ;
	}
	
	/**
	 * 处理上一页
	 */
	private function retPreviousButton()
	{
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
	
		$startRow = $this->_StartRow - $this->_Page_rows ;

		if ( $this->_StartRow == 0  or $this->_StartRow < 0 ) {
			$strPrevious = "";
		} else if ( $startRow <= 0 ) {
			$strPrevious = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_prev_page')}</a>";
		} else if ( $startRow  > 0 ) {
			$strPrevious = "	<a href=\"{$this->_new_URL}/{$startRow}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_prev_page')}</a>";
		}
		
		return $strPrevious;
	}
	
	/**
	 * 处理下一页
	 */
	private function retNextButton()
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
		
		$nextNum = $this->_StartRow + $this->_Page_rows ;
		$startRow = $this->_Total_rows - $this->_StartRow ;

		$strNext = "";
		if ( $this->_StartRow == 0 ) {
			if ( $startRow > $this->_Page_rows )
			{
				$len = strlen($_SERVER['REQUEST_URI']);
				$URI_last_byte = $_SERVER['REQUEST_URI'][$len-1];
				if($URI_last_byte != '/')
					$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_next_page')}</a>" ;
				else
					$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_next_page')}</a>" ;
			}
		} else if ( $nextNum < $this->_Total_rows ) {
			$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_next_page')}</a>" ;
		}

		return $strNext ;
	}
	
	/**
	 * 处理最后一页
	 */
	private function retLastButton( $pageNum )
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);

		$showNum = $pageNum - 1;
		$moveIndex = $showNum * $this->_Page_rows;

		if($this->_Total_rows)
		{
			$strButton = "";
			if ( $this->_StartRow != $moveIndex ) {
				if($this->_StartRow == 0)
				{
					$len = strlen($_SERVER['REQUEST_URI']);
					$URI_last_byte = $_SERVER['REQUEST_URI'][$len-1];
					if($URI_last_byte != '/')
						//$strButton = "	<a href=\"".current_url()."/..//{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						//$strButton = "	<a href=\"".current_url()."/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						//$strButton = "	<a href=\"".current_url()."/../{$this->_CurrentPage}/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_last_page')}</a>" ;
						// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
					else
						//$strButton = "	<a href=\"".current_url()."/index/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_last_page')}</a>" ;
						// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
				}
				else
					//$strButton = "	<a href=\"".current_url()."/../{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
					$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\" class=\"txtp\">{$CI->lang->line('common_last_page')}</a>" ;
					// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
			}
			return $strButton ;
		}
	}
	
	//可输入分页
	private function retInputPage( $pageNum ){
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
		
		$showNum = $pageNum - 1;
		
		$moveIndex = $showNum * $this->_Page_rows;
		
		$pagelen = strlen($this->_PageCnt);
		$setWidth = 11;
		if($pagelen <= 2){
			$inputWidth = $setWidth * 2;
		}else{
			$inputWidth = $setWidth * $pagelen;
		}
		
		if($this->_PageCnt > 1){
			$strButton = " <input style=\"width: {$inputWidth}px;\" maxlength=\"{$pagelen}\" class=\"input_page\" pagerows=\"{$this->_Page_rows}\" maxpage=\"{$this->_PageCnt}\" url=\"{$this->_new_URL}\" value=\"{$this->_NowPage}\"> / {$this->_PageCnt}{$CI->lang->line('common_page')}";
			return $strButton ;
		}	
	}
}
// END Pagination Class

/* End of file MY_Splitpage.php */
/* Location: ./application/libraries/MY_Splitpage.php */

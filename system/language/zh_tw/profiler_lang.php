<?php

$lang['profiler_database'] = '资料库';
$lang['profiler_controller_info'] = '类别/方法';
$lang['profiler_benchmarks'] = '评测';
$lang['profiler_queries'] = '指令';
$lang['profiler_get_data'] = 'GET 资料';
$lang['profiler_post_data'] = 'POST 资料';
$lang['profiler_uri_string'] = 'URI 字串';
$lang['profiler_memory_usage'] = '记忆体使用';
$lang['profiler_config'] = '设定变数';
$lang['profiler_session_data']	= 'SESSION 资料';
$lang['profiler_headers'] = 'HTTP标头';
$lang['profiler_no_db'] = '目前无法载入资料库驱动';
$lang['profiler_no_queries'] = '没有可执行指令';
$lang['profiler_no_post'] = '没有 POST 资料存在';
$lang['profiler_no_get'] = '没有 GET 资料存在';
$lang['profiler_no_uri'] = '没有 URI 字串存在';
$lang['profiler_no_memory'] = '无法取得记忆体使用';
$lang['profiler_no_profiles'] = '无性能分析资料 - 所有性能分析工具区块已经被关闭.';
$lang['profiler_section_hide']	= '隐藏';
$lang['profiler_section_show']	= '显示';

/* End of file profiler_lang.php */
/* Location: ./system/language/zh-TW/profiler_lang.php */

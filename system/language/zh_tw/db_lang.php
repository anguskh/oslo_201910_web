<?php

$lang['db_invalid_connection_str'] = '无法从连结取得资料库设定.';
$lang['db_unable_to_connect'] = '无法连结资料库.';
$lang['db_unable_to_select'] = '无法选择指定资料库: %s';
$lang['db_unable_to_create'] = '无法建立指定资料库: %s';
$lang['db_invalid_query'] = '指令无效.';
$lang['db_must_set_table'] = '必须设定资料表.';
$lang['db_must_use_set'] = '必须使用 "set" 指令来更新纪录.';
$lang['db_must_use_index'] = '批次更新必须指定索引值供比对';
$lang['db_batch_missing_index'] = '批次更新的一或多笔资料缺少指定索引';
$lang['db_must_use_where'] = '必须包含 "where" 指令才能更新纪录.';
$lang['db_del_must_use_where'] = '必须包含 "where" 或 "like" 才能删除纪录.';
$lang['db_field_param_missing'] = '必须提供表格中的名称做参数才能取得栏位.';
$lang['db_unsupported_function'] = '资料库不支援此功能.';
$lang['db_transaction_failure'] = '交易失败: 还原动作.';
$lang['db_unable_to_drop'] = '无法丢弃指定资料库.';
$lang['db_unsuported_feature'] = '资料库平台不支援此功能.';
$lang['db_unsuported_compression'] = '主机不支援此档案压缩格式.';
$lang['db_filepath_error'] = '路径无法写入档案.';
$lang['db_invalid_cache_path'] = '快取路径资料夹无法写入.';
$lang['db_table_name_required'] = '必须提供资料表名称.';
$lang['db_column_name_required'] = '必须提供列名称.';
$lang['db_column_definition_required'] = '必须提供列的定义.';
$lang['db_unable_to_set_charset'] = '无法设定用户端连结字元: %s';
$lang['db_error_heading'] = '资料库错误发生';

/* End of file db_lang.php */
/* Location: ./system/language/zh-TW/db_lang.php */

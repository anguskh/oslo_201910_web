<?php

$lang['migration_none_found'] = "没有发现任何 Migrations";
$lang['migration_not_found'] = "此 Migration 不存在";
$lang['migration_multiple_version'] = "多重 migrations 拥有相同的版本号码: %d";
$lang['migration_class_doesnt_exist'] = "Migration 类别 \"%s\" 不存在";
$lang['migration_missing_up_method'] = "Migration 类别 \"%s\" i缺少 'up' 方法";
$lang['migration_missing_down_method'] = "Migration 类别 \"%s\" 缺少 'down' 方法";
$lang['migration_invalid_filename'] = "Migration \"%s\" 包含无效的栏位名称";


/* End of file migration_lang.php */
/* Location: ./system/language/zh-TW/migration_lang.php */

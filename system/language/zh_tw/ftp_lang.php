<?php

$lang['ftp_no_connection'] = '无法定位有效连结ID. 请先确定已经连线成功.';
$lang['ftp_unable_to_connect'] = '无法连线到FTP.';
$lang['ftp_unable_to_login'] = '无法登入到FTP. 请检查帐号及密码.';
$lang['ftp_unable_to_makdir'] = '无法建立资料夹.';
$lang['ftp_unable_to_changedir'] = '无法切换目录.';
$lang['ftp_unable_to_chmod'] = '无法设定权限. 请检查路径. 注意: 此功能只支援 PHP 5 或更高.';
$lang['ftp_unable_to_upload'] = '无法上传档案. 请检查路径.';
$lang['ftp_unable_to_download'] = '无法下载档案. 请检查路径.';
$lang['ftp_no_source_file'] = '无法定位原始档案. 请检查路径.';
$lang['ftp_unable_to_rename'] = '无法更改档案名称.';
$lang['ftp_unable_to_delete'] = '无法删除档案.';
$lang['ftp_unable_to_move'] = '无法移动档案. 请确定目的地资料夹是否存在.';

/* End of file ftp_lang.php */
/* Location: ./system/language/zh-TW/ftp_lang.php */

<?php

$lang['email_must_be_array'] = 'Email检验方法必须通过阵列.';
$lang['email_invalid_address'] = '无效Email信箱: %s';
$lang['email_attachment_missing'] = '无法确认下Email附夹档案位址: %s';
$lang['email_attachment_unreadable'] = '无法开启附夹档案: %s';
$lang['email_no_recipients'] = '必须包含: 收件人(To)、副本(Cc)或密件副本(Bcc)';
$lang['email_send_failure_phpmail'] = '无法使用 mail() 函式发送信件. 主机可能未设定此方法发送信件.';
$lang['email_send_failure_sendmail'] = '无法使用 Sendmail 发送信件. 主机可能未设定此方法发送信件.';
$lang['email_send_failure_smtp'] = '无法使用 SMTP 发送信件. 主机可能未设定此方法发送信件.';
$lang['email_sent'] = '讯息已经发送成功，使用以下协定: %s';
$lang['email_no_socket'] = '无法开启 Sendmail 端口. 请检查设定.';
$lang['email_no_hostname'] = '尚未指定 SMTP 主机.';
$lang['email_smtp_error'] = 'SMTP 发生错误: %s';
$lang['email_no_smtp_unpw'] = '错误: 你必须指定 SMTP 使用者名称及密码.';
$lang['email_failed_smtp_login'] = '发送 AUTH LOGIN 指令错误. 错误讯息: %s';
$lang['email_smtp_auth_un'] = '认证使用者名称失败. 错误讯息: %s';
$lang['email_smtp_auth_pw'] = '认证使用者密码失败. 错误讯息: %s';
$lang['email_smtp_data_failure'] = '无法发送资料: %s';
$lang['email_exit_status'] = '离开状态代码: %s';


/* End of file email_lang.php */
/* Location: ./system/language/zh-TW/email_lang.php */

<?php

$lang['ut_test_name'] = '测试名称';
$lang['ut_test_datatype'] = '测试类型';
$lang['ut_res_datatype'] = '预期类型';
$lang['ut_result'] = '结果';
$lang['ut_undefined'] = '未定义测试名称';
$lang['ut_file'] = '档案名称';
$lang['ut_line'] = '行数';
$lang['ut_passed'] = '通过';
$lang['ut_failed'] = '未通过';
$lang['ut_boolean'] = '布林值';
$lang['ut_integer'] = '整数';
$lang['ut_float'] = '浮点数';
$lang['ut_double'] = '倍精度浮点数';
$lang['ut_string'] = '字串';
$lang['ut_array'] = '阵列';
$lang['ut_object'] = '物件';
$lang['ut_resource'] = '资源';
$lang['ut_null'] = '无';
$lang['ut_notes'] = '备注';


/* End of file unit_test_lang.php */
/* Location: ./system/language/zh-TW/unit_test_lang.php */
